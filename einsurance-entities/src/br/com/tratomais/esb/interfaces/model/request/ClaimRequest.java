package br.com.tratomais.esb.interfaces.model.request;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

public class ClaimRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1821785493535003275L;
	private Long claimNumber;
	private Date claimDate;
	private Date claimNotification;
	private BigInteger policyNumber;
	private Long certificateNumber;
	private Integer claimCauseID;
	private Integer claimEffectID;
	private Integer coverageId;
	private Integer itemId;
	private Integer riskGroupId;
	private Integer claimStatus;
	private int claimActionType;
	private Integer lotID;
	private Integer fileLotID;
	private Integer fileLotDetailsID;
	
	public Long getClaimNumber() {
		return claimNumber;
	}
	public void setClaimNumber(Long claimNumber) {
		this.claimNumber = claimNumber;
	}
	public Date getClaimDate() {
		return claimDate;
	}
	public void setClaimDate(Date claimDate) {
		this.claimDate = claimDate;
	}
	public Date getClaimNotification() {
		return claimNotification;
	}
	public void setClaimNotification(Date claimNotification) {
		this.claimNotification = claimNotification;
	}
	public BigInteger getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(BigInteger policyNumber) {
		this.policyNumber = policyNumber;
	}
	public Long getCertificateNumber() {
		return certificateNumber;
	}
	public void setCertificateNumber(Long certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	public Integer getClaimCauseID() {
		return claimCauseID;
	}
	public void setClaimCauseID(Integer claimCauseID) {
		this.claimCauseID = claimCauseID;
	}
	public Integer getClaimEffectID() {
		return claimEffectID;
	}
	public void setClaimEffectID(Integer claimEffectID) {
		this.claimEffectID = claimEffectID;
	}
	public Integer getCoverageId() {
		return coverageId;
	}
	public void setCoverageId(Integer coverageId) {
		this.coverageId = coverageId;
	}
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	public void setRiskGroupId(Integer riskGroupId) {
		this.riskGroupId = riskGroupId;
	}
	public Integer getRiskGroupId() {
		return riskGroupId;
	}
	public Integer getClaimStatus() {
		return claimStatus;
	}
	public void setClaimStatus(Integer claimStatus) {
		this.claimStatus = claimStatus;
	}
	public int getClaimActionType() {
		return claimActionType;
	}
	public void setClaimActionType(int claimActionType) {
		this.claimActionType = claimActionType;
	}
	public void setLotID(Integer lotID) {
		this.lotID = lotID;
	}
	public Integer getLotID() {
		return lotID;
	}
	public Integer getFileLotID() {
		return fileLotID;
	}
	public void setFileLotID(Integer fileLotID) {
		this.fileLotID = fileLotID;
	}
	public Integer getFileLotDetailsID() {
		return fileLotDetailsID;
	}
	public void setFileLotDetailsID(Integer fileLotDetailsID) {
		this.fileLotDetailsID = fileLotDetailsID;
	}
}