package br.com.tratomais.esb.interfaces.model.response;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import br.com.tratomais.esb.interfaces.model.request.ClaimRequest;

public class ClaimResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long claimNumber; // numero siniestro
	private Date claimDate; // fecha siniestro
	private Date claimNotification; // fecha notificacioin
	private BigInteger policyNumber; //poliza
	private Long certificateNumber;  //ceritificado
	private String channelCode;// agencia
	private Integer claimCauseID; //causa
	private Integer claimEffectID; //efecto
	private Integer coverageId; // Bien e Cobertura
	private Integer itemId; //item_afectado
	private Integer riskGroupId; //item_afectado
	private Integer claimStatus; // estatus
	private int claimActionType;
	private Double claimValue; // monto imndenizacion
	private Double premiumPending;
	private Integer lotID;
	private Integer fileLotID;
	private Integer fileLotDetailsID;
	private Integer errorID;
	private String errorMessage;
	private String responseCode;
	private String responseDescription;
	
	
	public ClaimResponse() {
	}
	
	public void copyFromClaimRequest(ClaimRequest claimRequest){
		this.claimNumber = claimRequest.getClaimNumber();
		this.claimDate = claimRequest.getClaimDate();
		this.claimNotification = claimRequest.getClaimNotification();
		this.policyNumber = claimRequest.getPolicyNumber();
		this.certificateNumber = claimRequest.getCertificateNumber();
//		this.channelCode = claimRequest.getChannelCode();
		this.claimCauseID = claimRequest.getClaimCauseID();
		this.claimEffectID = claimRequest.getClaimEffectID();
		this.coverageId = claimRequest.getCoverageId();
		this.itemId = claimRequest.getItemId();
		this.riskGroupId = claimRequest.getRiskGroupId();
		this.claimStatus = claimRequest.getClaimStatus();
		this.claimActionType = claimRequest.getClaimActionType();
//		this.claimValue = claimRequest.getClaimValue();
//		this.installmentPending = claimRequest.getInstallmentPending();
		this.lotID = claimRequest.getLotID();
		this.fileLotID = claimRequest.getFileLotID();
		this.fileLotDetailsID = claimRequest.getFileLotDetailsID();
//		this.error = claimRequest.getError();		
		
	}
	public ClaimResponse(ClaimRequest claimRequest) {
		this.copyFromClaimRequest(claimRequest);
	}
	
	public Long getClaimNumber() {
		return claimNumber;
	}
	public void setClaimNumber(Long claimNumber) {
		this.claimNumber = claimNumber;
	}
	public Date getClaimDate() {
		return claimDate;
	}
	public void setClaimDate(Date claimDate) {
		this.claimDate = claimDate;
	}
	public Date getClaimNotification() {
		return claimNotification;
	}
	public void setClaimNotification(Date claimNotification) {
		this.claimNotification = claimNotification;
	}
	public BigInteger getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(BigInteger policyNumber) {
		this.policyNumber = policyNumber;
	}
	public Long getCertificateNumber() {
		return certificateNumber;
	}
	public void setCertificateNumber(Long certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	public String getChannelCode() {
		return channelCode;
	}
	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}
	public Integer getClaimCauseID() {
		return claimCauseID;
	}
	public void setClaimCauseID(Integer claimCauseID) {
		this.claimCauseID = claimCauseID;
	}
	public Integer getClaimEffectID() {
		return claimEffectID;
	}
	public void setClaimEffectID(Integer claimEffectID) {
		this.claimEffectID = claimEffectID;
	}
	public Integer getCoverageId() {
		return coverageId;
	}
	public void setCoverageId(Integer coverageId) {
		this.coverageId = coverageId;
	}
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	public void setRiskGroupId(Integer riskGroupId) {
		this.riskGroupId = riskGroupId;
	}
	public Integer getRiskGroupId() {
		return riskGroupId;
	}
	public Integer getClaimStatus() {
		return claimStatus;
	}
	public void setClaimStatus(Integer claimStatus) {
		this.claimStatus = claimStatus;
	}
	public int getClaimActionType() {
		return claimActionType;
	}
	public void setClaimActionType(int claimActionType) {
		this.claimActionType = claimActionType;
	}
	public Double getClaimValue() {
		return claimValue;
	}
	public void setClaimValue(Double claimValue) {
		this.claimValue = claimValue;
	}
	public Double getPremiumPending() {
		return premiumPending;
	}
	public void setPremiumPending(Double premiumPending) {
		this.premiumPending = premiumPending;
	}
	public void setLotID(Integer lotID) {
		this.lotID = lotID;
	}
	public Integer getLotID() {
		return lotID;
	}
	public Integer getFileLotID() {
		return fileLotID;
	}
	public void setFileLotID(Integer fileLotID) {
		this.fileLotID = fileLotID;
	}
	public Integer getFileLotDetailsID() {
		return fileLotDetailsID;
	}
	public void setFileLotDetailsID(Integer fileLotDetailsID) {
		this.fileLotDetailsID = fileLotDetailsID;
	}

	public Integer getErrorID() {
		return errorID;
	}

	public void setErrorID(Integer errorID) {
		this.errorID = errorID;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDescription() {
		return responseDescription;
	}

	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((policyNumber == null) ? 0 : policyNumber.hashCode());
		result = prime * result
				+ ((certificateNumber == null) ? 0 : certificateNumber.hashCode());
		result = prime * result
				+ ((claimNumber == null) ? 0 : claimNumber.hashCode());
		result = prime * result 
				+ ((itemId == null) ? 0 : itemId.hashCode());
		result = prime * result
				+ ((riskGroupId == null) ? 0 : riskGroupId.hashCode());
		result = prime * result
				+ ((coverageId == null) ? 0 : coverageId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClaimResponse other = (ClaimResponse) obj;
		if (policyNumber == null) {
			if (other.policyNumber != null)
				return false;
		} else if (!policyNumber.equals(other.policyNumber))
			return false;
		if (certificateNumber == null) {
			if (other.certificateNumber != null)
				return false;
		} else if (!certificateNumber.equals(other.certificateNumber))
			return false;
		if (claimNumber == null) {
			if (other.claimNumber != null)
				return false;
		} else if (!claimNumber.equals(other.claimNumber))
			return false;
		if (itemId == null) {
			if (other.itemId != null)
				return false;
		} else if (!itemId.equals(other.itemId))
			return false;
		if (riskGroupId == null) {
			if (other.riskGroupId != null)
				return false;
		} else if (!riskGroupId.equals(other.riskGroupId))
			return false;
		if (coverageId == null) {
			if (other.coverageId != null)
				return false;
		} else if (!coverageId.equals(other.coverageId))
			return false;
		return true;
	}
}
