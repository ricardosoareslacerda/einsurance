package br.com.tratomais.core.info;

import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.general.utilities.NumberUtilities;

public class Calculus {
	public static final int STEP_DECIMAL_PLACES = 4;
	public static final int RESULT_DECIMAL_PLACES = 2;
	public static final int RATE_DECIMAL_PLACES = 6;

	/**
	 * Converts to Foreign currency
	 * @param value Value to be converted
	 * @return Local Currency
	 */
	public static Double toForeignCurrency(Double value, Double conversionRate){
		value = NumberUtilities.convertValueDefault(value, null);
		conversionRate = NumberUtilities.convertValueDefault(conversionRate, 1.0);
		conversionRate = (conversionRate.equals(0.0) ? 1.0 : conversionRate);
		if (value != null && conversionRate != null)
			return NumberUtilities.roundDecimalPlaces(value / conversionRate, Endorsement.DECIMAL_PLACES);
		
		return null;
	}

	/**
	 * Converts to local currency
	 * @param value value to be converted
	 * @return Foreign currency
	 */
	public static Double toLocalCurrency(Double value, Double conversionRate){
		value = NumberUtilities.convertValueDefault(value, null);
		conversionRate = NumberUtilities.convertValueDefault(conversionRate, 1.0);
		conversionRate = (conversionRate.equals(0.0) ? 1.0 : conversionRate);
		if (value != null && conversionRate != null)
			return NumberUtilities.roundDecimalPlaces(value * conversionRate, Endorsement.DECIMAL_PLACES);
		
		return null;
	}}
