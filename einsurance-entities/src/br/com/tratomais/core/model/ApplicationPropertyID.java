package br.com.tratomais.core.model;

import br.com.tratomais.core.dao.PersistentEntityId;

public class ApplicationPropertyID extends PersistentEntityId {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer applicationId;
	private String name;
	
	public ApplicationPropertyID() {
	}
	
	public ApplicationPropertyID(Integer applicationId, String name) {
		super();
		this.applicationId = applicationId;
		this.name = name;
	}
	/**
	 * @return the applicationID
	 */
	public Integer getApplicationId() {
		return applicationId;
	}
	/**
	 * @param applicationID the applicationID to set
	 */
	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}
