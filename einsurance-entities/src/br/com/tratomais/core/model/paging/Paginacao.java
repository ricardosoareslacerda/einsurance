package br.com.tratomais.core.model.paging;

import java.util.List;

public class Paginacao {

	private int totalDados;
	private List<? extends Object> listaDados;
	
	public Paginacao(){
		
	}

	/**
	 * @return the totalDados
	 */
	public int getTotalDados() {
		return totalDados;
	}
	/**
	 * @param totalDados the totalDados to set
	 */
	public void setTotalDados(int totalDados) {
		this.totalDados = totalDados;
	}
	
	/**
	 * @return the listaDados
	 */
	public List<? extends Object> getListaDados() {
		return listaDados;
	}
	/**
	 * @param listaDados the listaDados to set
	 */
	public void setListaDados(List<? extends Object> listaDados) {
		this.listaDados = listaDados;
	}
	
	
}
