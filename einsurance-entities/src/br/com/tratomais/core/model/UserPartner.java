package br.com.tratomais.core.model;

import br.com.tratomais.core.dao.PersistentEntity;
import br.com.tratomais.core.model.customer.Partner;

public class UserPartner implements PersistentEntity {

	private static final long serialVersionUID = -6250486988514342833L;
	private UserPartnerId id;
	private Partner partner;
	private User user;
	private boolean administrator;
	private boolean partnerDefault;
	private boolean allChannel;

	private boolean transientSelected;

	public UserPartner() {
	}

	public UserPartner(UserPartnerId id, Partner partner, User user, boolean administrator, boolean allChannel) {
		this.id = id;
		this.partner = partner;
		this.user = user;
		this.administrator = administrator;
		this.allChannel = allChannel;
	}

	public UserPartnerId getId() {
		return this.id;
	}

	public void setId(UserPartnerId id) {
		this.id = id;
	}

	public Partner getPartner() {
		return this.partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isAdministrator() {
		return this.administrator;
	}

	public void setAdministrator(boolean administrator) {
		this.administrator = administrator;
	}

	public boolean isAllChannel() {
		return this.allChannel;
	}

	public void setAllChannel(boolean allChannel) {
		this.allChannel = allChannel;
	}

	public boolean isTransientSelected() {
		return transientSelected;
	}

	public void setTransientSelected(boolean transientSelected) {
		this.transientSelected = transientSelected;
	}

	public boolean isPartnerDefault() {
		return partnerDefault;
	}

	public void setPartnerDefault(boolean partnerDefault) {
		this.partnerDefault = partnerDefault;
	}
}
