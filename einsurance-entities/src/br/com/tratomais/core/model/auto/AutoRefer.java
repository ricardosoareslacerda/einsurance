package br.com.tratomais.core.model.auto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.tratomais.core.dao.PersistentEntity;

@Entity
@Table(name="AutoRefer")
public class AutoRefer implements PersistentEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int AUTO_TABLE_TYPE_MAIN = 340;
	
	@Id
	@Column(name="autoReferId", columnDefinition="Integer", nullable=false)
	private Integer id;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="autoTableType", columnDefinition="Integer", nullable=false)
	private Integer autoTableType;
	public Integer getAutoTableType() {
		return autoTableType;
	}
	public void setAutoTableType(Integer autoTableType) {
		this.autoTableType = autoTableType;
	}
	
	@Column(name="markCode", columnDefinition="char", length=5, nullable=true)
	private String markCode;
	public String getMarkCode() {
		return markCode;
	}
	public void setMarkCode(String markCode) {
		this.markCode = markCode;
	}
	
	@Column(name="markName", columnDefinition="varchar", length=50, nullable=false)
	private String markName;
	public String getMarkName() {
		return markName;
	}
	public void setMarkName(String markName) {
		this.markName = markName;
	}
	
	@Column(name="modelCode", columnDefinition="char", length=10, nullable=false)
	private String modelCode;
	public String getModelCode() {
		return modelCode;
	}
	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}
	
	@Column(name="modelName", columnDefinition="varchar", length=100, nullable=true)
	private String modelName;
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	
	@Column(name="versionCode", columnDefinition="char", length=10, nullable=false)
	private String versionCode;
	public String getVersionCode() {
		return versionCode;
	}
	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}
	
	@Column(name="versionName", columnDefinition="varchar", length=100, nullable=true)
	private String versionName;
	public String getVersionName() {
		return versionName;
	}
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
	
	@Column(name="MotorDescription", columnDefinition="varchar", length=50, nullable=true)
	private String motorDescription;
	public String getMotorDescription() {
		return motorDescription;
	}
	public void setMotorDescription(String motorDescription) {
		this.motorDescription = motorDescription;
	}
	
	@Column(name="transmissionCode", columnDefinition="char", length=3, nullable=true)
	private String transmissionCode;
	public String getTransmissionCode() {
		return transmissionCode;
	}
	public void setTransmissionCode(String transmissionCode) {
		this.transmissionCode = transmissionCode;
	}
	
	@Column(name="fuelCode", columnDefinition="char", length=2, nullable=true)
	private String fuelCode;
	public String getFuelCode() {
		return fuelCode;
	}
	public void setFuelCode(String fuelCode) {
		this.fuelCode = fuelCode;
	}
	
	@Column(name="originCode", columnDefinition="char", length=2, nullable=true)
	private String originCode;
	public String getOriginCode() {
		return originCode;
	}
	public void setOriginCode(String originCode) {
		this.originCode = originCode;
	}
	
	@Column(name="groupCode", columnDefinition="char", length=3, nullable=true)
	private String groupCode;
	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	
	@Column(name="classCode", columnDefinition="char", length=3, nullable=true)
	private String classCode;
	public String getClassCode() {
		return classCode;
	}
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}
	
	@Column(name="DoorNumber", columnDefinition="smallint", nullable=true)
	private Short doorNumber;
	public Short getDoorNumber() {
		return doorNumber;
	}
	public void setDoorNumber(Short doorNumber) {
		this.doorNumber = doorNumber;
	}
	
	@Column(name="PassengerNumber", columnDefinition="smallint", nullable=true)
	private Short passengerNumber;
	public Short getPassengerNumber() {
		return passengerNumber;
	}
	public void setPassengerNumber(Short passengerNumber) {
		this.passengerNumber = passengerNumber;
	}
	
	@Column(name="InitialYear", columnDefinition="smallint", nullable=true)
	private Short initialYear;
	public Short getInitialYear() {
		return initialYear;
	}
	public void setInitialYear(Short initialYear) {
		this.initialYear = initialYear;
	}
	
	@Column(name="FinalYear", columnDefinition="smallint", nullable=true)
	private Short finalYear;
	public Short getFinalYear() {
		return finalYear;
	}
	public void setFinalYear(Short finalYear) {
		this.finalYear = finalYear;
	}
	
	@Column(name="Registred", columnDefinition="datetime", nullable=false)
	private Date registred;
	public Date getRegistred() {
		return registred;
	}
	public void setRegistred(Date registred) {
		this.registred = registred;
	}
	
	@Column(name="Updated", columnDefinition="datetime", nullable=true)
	private Date updated;
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
}
