package br.com.tratomais.core.model.auto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.tratomais.core.dao.PersistentEntity;

@Entity
@Table(name="AutoVersion")
public class AutoVersion implements PersistentEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	private AutoVersionID id;
	public AutoVersionID getId() {
		return id;
	}
	public void setId(AutoVersionID id) {
		this.id = id;
	}
	
	@Column(name="ExpiryDate", columnDefinition="smalldatetime", nullable=false )
	private Date expiryDate;
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	@Column(name="AutoClassType", columnDefinition="int", nullable=true)
	private Integer autoClassType;
	public Integer getAutoClassType() {
		return autoClassType;
	}
	public void setAutoClassType(Integer autoClassType) {
		this.autoClassType = autoClassType;
	}
	
	@Column(name="AutoTariffType", columnDefinition="int", nullable=true)
	private Integer autoTariffType;
	public Integer getAutoTariffType() {
		return autoTariffType;
	}
	public void setAutoTariffType(Integer autoTariffType) {
		this.autoTariffType = autoTariffType;
	}
	
	@Column(name="AutoCategoryType", columnDefinition="int", nullable=true)
	private Integer autoCategoryType;
	public Integer getAutoCategoryType() {
		return autoCategoryType;
	}
	public void setAutoCategoryType(Integer autoCategoryType) {
		this.autoCategoryType = autoCategoryType;
	}
	
	@Column(name="AutoGroupType", columnDefinition="int", nullable=true)
	private Integer autoGroupType;
	public Integer getAutoGroupType() {
		return autoGroupType;
	}
	public void setAutoGroupType(Integer autoGroupType) {
		this.autoGroupType = autoGroupType;
	}
	
	@Column(name="AutoUseType", columnDefinition="int", nullable=true)
	private Integer autoUseType;
	public Integer getAutoUseType() {
		return autoUseType;
	}
	public void setAutoUseType(Integer autoUseType) {
		this.autoUseType = autoUseType;
	}
	
	@Column(name="Registred", columnDefinition="datetime", nullable=false)
	private Date registred;
	public Date getRegistred() {
		return registred;
	}
	public void setRegistred(Date registred) {
		this.registred = registred;
	}
	
	@Column(name="Updated", columnDefinition="datetime", nullable=true)
	private Date updated;
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
}
