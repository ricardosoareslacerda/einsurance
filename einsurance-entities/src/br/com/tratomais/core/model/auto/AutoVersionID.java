package br.com.tratomais.core.model.auto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import br.com.tratomais.core.dao.PersistentEntityId;

@Embeddable
public class AutoVersionID extends PersistentEntityId implements Comparable<AutoVersionID>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="AutoModelID", columnDefinition="Integer", nullable=false)
	private Integer autoModelID;
	public Integer getAutoModelID() {
		return autoModelID;
	}
	public void setAutoModelID(Integer autoModelID) {
		this.autoModelID = autoModelID;
	}
	
	@Column(name="EffectiveDate", columnDefinition="smalldatetime", nullable=false)
	private Date effectiveDate;
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	/** {@inheritDoc} */
	@Override
	public int compareTo(AutoVersionID o) {
		int retorno = 0;
		if (this.autoModelID != null)
			retorno = autoModelID.compareTo(o.autoModelID);
		if ( (retorno == 0)&&(this.effectiveDate != null) )
			retorno = this.effectiveDate.compareTo(o.effectiveDate);
		return retorno;
	}
}
