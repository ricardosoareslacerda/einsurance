package br.com.tratomais.core.model.auto;

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

public class AutoModel implements PersistentEntity {

	private static final long serialVersionUID = -2645804242744015271L;

	private Integer autoModelId;
	private AutoBrand autoMark;
	private String autoModelCode;
	private String modelName;
	private Integer autoFuelType;
	private Integer autoTransmissionType;
	private Short doorNumber;
	private Short passengerNumber;
	private boolean active;
	private Date registred;
	private Date updated;

	public AutoModel() {
	}

	public AutoModel(Integer autoModelId, AutoBrand autoMark, String modelName,
			Date registred) {
		this.autoModelId = autoModelId;
		this.autoMark = autoMark;
		this.modelName = modelName;
		this.registred = registred;
	}

	public AutoModel(Integer autoModelId, AutoBrand autoMark, String autoModelCode,
			String modelName, Integer autoFuelType,
			Integer autoTransmissionType, Short doorNumber,
			Short passengerNumber, Date registred, Date updated) {
		this.autoModelId = autoModelId;
		this.autoMark = autoMark;
		this.autoModelCode = autoModelCode;
		this.modelName = modelName;
		this.autoFuelType = autoFuelType;
		this.autoTransmissionType = autoTransmissionType;
		this.doorNumber = doorNumber;
		this.passengerNumber = passengerNumber;
		this.registred = registred;
		this.updated = updated;
	}

	public Integer getAutoModelId() {
		return this.autoModelId;
	}

	public void setAutoModelId(Integer autoModelId) {
		this.autoModelId = autoModelId;
	}

	public AutoBrand getAutoMark() {
		return this.autoMark;
	}

	public void setAutoMark(AutoBrand autoMark) {
		this.autoMark = autoMark;
	}

	public String getAutoModelCode() {
		return this.autoModelCode;
	}

	public void setAutoModelCode(String autoModelCode) {
		this.autoModelCode = autoModelCode;
	}

	public String getModelName() {
		return this.modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public Integer getAutoFuelType() {
		return this.autoFuelType;
	}

	public void setAutoFuelType(Integer autoFuelType) {
		this.autoFuelType = autoFuelType;
	}

	public Integer getAutoTransmissionType() {
		return this.autoTransmissionType;
	}

	public void setAutoTransmissionType(Integer autoTransmissionType) {
		this.autoTransmissionType = autoTransmissionType;
	}

	public Short getDoorNumber() {
		return this.doorNumber;
	}

	public void setDoorNumber(Short doorNumber) {
		this.doorNumber = doorNumber;
	}

	public Short getPassengerNumber() {
		return this.passengerNumber;
	}

	public void setPassengerNumber(Short passengerNumber) {
		this.passengerNumber = passengerNumber;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getRegistred() {
		return this.registred;
	}

	public void setRegistred(Date registred) {
		this.registred = registred;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
}
