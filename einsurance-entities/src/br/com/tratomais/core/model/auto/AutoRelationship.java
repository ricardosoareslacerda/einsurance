package br.com.tratomais.core.model.auto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="AutoRelationship")
public class AutoRelationship {
	@Id
	private AutoRelationshipId id;
	public AutoRelationshipId getId() {
		return id;
	}
	public void setId(AutoRelationshipId id) {
		this.id = id;
	}
	
	@Column(name="expiryDate", columnDefinition="smallDateTime", nullable=false)
	private Date expiryDate;
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	@Column(name="registred", columnDefinition="smallDateTime", nullable=false)
	private Date registred;
	public Date getRegistred() {
		return registred;
	}
	public void setRegistred(Date registred) {
		this.registred = registred;
	}

	@Column(name="Updated", columnDefinition="smallDateTime", nullable=true)
	public Date updated;
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}	
}
