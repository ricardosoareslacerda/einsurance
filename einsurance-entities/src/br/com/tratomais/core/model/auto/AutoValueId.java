package br.com.tratomais.core.model.auto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import br.com.tratomais.core.dao.PersistentEntityId;

@Embeddable
public class AutoValueId extends PersistentEntityId{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name="autoReferId", columnDefinition="Integer", nullable=false)
	private Integer autoReferId;
	public Integer getAutoReferId() {
		return autoReferId;
	}
	public void setAutoReferId(Integer autoReferId) {
		this.autoReferId = autoReferId;
	}
	
	@Column(name="yearRefer", columnDefinition="Integer", nullable=false)
	private Integer yearRefer;
	public Integer getYearRefer() {
		return yearRefer;
	}
	public void setYearRefer(Integer yearRefer) {
		this.yearRefer = yearRefer;
	}
	
	@Column(name="effectiveDate", columnDefinition="smalldatetime", nullable=false)
	private Date effectiveDate;
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
}
