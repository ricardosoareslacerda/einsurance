package br.com.tratomais.core.model.auto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.tratomais.core.dao.PersistentEntity;

@Entity
@Table(name="AutoValue")
public class AutoValue implements PersistentEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private AutoValueId id;
	public AutoValueId getId() {
		return id;
	}
	public void setId(AutoValueId id) {
		this.id = id;
	}
	
	@Column(name="expiryDate", columnDefinition="smalldatetime", nullable=false)
	private Date expiryDate;
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	@Column(name="valueRefer", columnDefinition="numeric(18,2)", length=18, precision=2, nullable=false)
	private Double valueRefer;
	public Double getValueRefer() {
		return valueRefer;
	}
	public void setValueRefer(Double valueRefer) {
		this.valueRefer = valueRefer;
	}
	
	@Column(name="registred", columnDefinition="datetime", nullable=false)
	private Date registred;
	public Date getRegistred() {
		return registred;
	}
	public void setRegistred(Date registred) {
		this.registred = registred;
	}
	
	@Column(name="updated", columnDefinition="datetime", nullable=true)
	private Date updated;
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
}
