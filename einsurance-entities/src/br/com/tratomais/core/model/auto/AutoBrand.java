package br.com.tratomais.core.model.auto;

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

public class AutoBrand implements PersistentEntity {
	private static final long serialVersionUID = 1336997366220997676L;
	private Integer autoMarkId;
	private String autoMarkCode;
	private String markName;
	private boolean active;
	private Date registred;
	private Date updated;

	public AutoBrand() {
	}

	public AutoBrand(Integer autoMarkId, String markName, Date registred) {
		this.autoMarkId = autoMarkId;
		this.markName = markName;
		this.registred = registred;
	}

	public AutoBrand(Integer autoMarkId, String autoMarkCode, String markName,
			Date registred, Date updated) {
		this.autoMarkId = autoMarkId;
		this.autoMarkCode = autoMarkCode;
		this.markName = markName;
		this.registred = registred;
		this.updated = updated;
	}

	public Integer getAutoMarkId() {
		return this.autoMarkId;
	}

	public void setAutoMarkId(Integer autoMarkId) {
		this.autoMarkId = autoMarkId;
	}

	public String getAutoMarkCode() {
		return this.autoMarkCode;
	}

	public void setAutoMarkCode(String autoMarkCode) {
		this.autoMarkCode = autoMarkCode;
	}

	public String getMarkName() {
		return this.markName;
	}

	public void setMarkName(String markName) {
		this.markName = markName;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getRegistred() {
		return this.registred;
	}

	public void setRegistred(Date registred) {
		this.registred = registred;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
}
