package br.com.tratomais.core.model.auto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import br.com.tratomais.core.dao.PersistentEntityId;

@Embeddable
public class AutoRelationshipId  extends PersistentEntityId{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="AutoModelId", columnDefinition="Integer", nullable=false)
	private Integer autoModelId;
	public Integer getAutoModelId() {
		return autoModelId;
	}
	public void setAutoModelId(Integer autoModelId) {
		this.autoModelId = autoModelId;
	}

	@Column(name="AutoReferId", columnDefinition="Integer", nullable=false)
	private Integer autoReferId;
	public Integer getAutoReferId() {
		return autoReferId;
	}
	public void setAutoReferId(Integer autoReferId) {
		this.autoReferId = autoReferId;
	}
	
	@Column(name="EffectiveDate", columnDefinition="SmallDatetime", nullable=false)
	private Date effectiveDate;
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
}
