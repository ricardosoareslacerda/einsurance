package br.com.tratomais.core.model.report;

import java.util.List;

import br.com.tratomais.core.model.interfaces.Exchange;
import br.com.tratomais.core.model.product.DataOption;

public class DataViewInterface {

	private List<DataOption> listExchangeType;
	private List<Exchange> listExchange;
	private List<DataOption> listLotStatus;
	
	public List<DataOption> getListExchangeType() {
		return listExchangeType;
	}
	
	public void setListExchangeType(List<DataOption> listExchangeType) {
		this.listExchangeType = listExchangeType;
	}
	
	public List<Exchange> getListExchange() {
		return listExchange;
	}
	
	public void setListExchange(List<Exchange> listExchange) {
		this.listExchange = listExchange;
	}

	public List<DataOption> getListLotStatus() {
		return listLotStatus;
	}

	public void setListLotStatus(List<DataOption> listLotStatus) {
		this.listLotStatus = listLotStatus;
	}
}
