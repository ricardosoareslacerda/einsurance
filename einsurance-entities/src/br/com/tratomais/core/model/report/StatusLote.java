package br.com.tratomais.core.model.report;

import br.com.tratomais.core.dao.PersistentEntity;

public class StatusLote implements PersistentEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int statusId;
	private String fieldDescription;
	
	public int getStatusId() {
		return statusId;
	}
	
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	
	public String getFieldDescription() {
		return fieldDescription;
	}
	
	public void setFieldDescription(String fieldDescription) {
		this.fieldDescription = fieldDescription;
	}

}
