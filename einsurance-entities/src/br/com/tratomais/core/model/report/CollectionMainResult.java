package br.com.tratomais.core.model.report;

import java.math.BigDecimal;
import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

public class CollectionMainResult implements PersistentEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int lotId;
	private int lotStatus;
	private String fieldDescription;
	private String lotCode;
	private String fileName;
	private Date sendDate;
	private Date recieveDate;
	private int quantity;
	private BigDecimal amount;
	private String errorMessage;
	
	public int getLotId() {
		return lotId;
	}

	public void setLotId(int lotId) {
		this.lotId = lotId;
	}
	
	public int getLotStatus() {
		return lotStatus;
	}
	
	public void setLotStatus(int lotStatus) {
		this.lotStatus = lotStatus;
	}
	
	public String getFieldDescription() {
		return fieldDescription;
	}
	
	public void setFieldDescription(String fieldDescription) {
		this.fieldDescription = fieldDescription;
	}
	
	public String getLotCode() {
		return lotCode;
	}
	
	public void setLotCode(String lotCode) {
		this.lotCode = lotCode;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public Date getSendDate() {
		return sendDate;
	}
	
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	
	public Date getRecieveDate() {
		return recieveDate;
	}
	
	public void setRecieveDate(Date recieveDate) {
		this.recieveDate = recieveDate;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
