package br.com.tratomais.core.model.report;

import br.com.tratomais.core.dao.PersistentEntity;

public class CollectionDetailsMainParameters implements PersistentEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int lotId;
	private int page;
	private int totalPage;
	private int currentPage;
	
	public int getLotId() {
		return lotId;
	}
	
	public void setLotId(int lotId) {
		this.lotId = lotId;
	}
	
	public int getPage() {
		return page;
	}
	
	public void setPage(int page) {
		this.page = page;
	}
	
	public int getTotalPage() {
		return totalPage;
	}
	
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	
	public int getCurrentPage() {
		return currentPage;
	}
	
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}	
}
