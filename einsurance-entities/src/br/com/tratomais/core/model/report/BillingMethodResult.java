package br.com.tratomais.core.model.report;

import br.com.tratomais.core.dao.PersistentEntity;

public class BillingMethodResult implements PersistentEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int exchangeId;
	private int exchangeType;
	private Integer chartAccountId;
	private String name;
	
	public int getExchangeId() {
		return exchangeId;
	}
	
	public void setExchangeId(int exchangeId) {
		this.exchangeId = exchangeId;
	}
	
	public int getExchangeType() {
		return exchangeType;
	}
	
	public void setExchangeType(int exchangeType) {
		this.exchangeType = exchangeType;
	}
	
	public Integer getChartAccountId() {
		return chartAccountId;
	}
	
	public void setChartAccountId(Integer chartAccountId) {
		this.chartAccountId = chartAccountId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
