package br.com.tratomais.core.model.report;

import java.util.Collection;

@SuppressWarnings("unchecked")

public class ReportListData {

	public ReportListData() {}

	private Collection listChannel;
	private Collection listIssuingStatus;
	private Collection listInstallmentStatus;
	private Collection listUserPerPartner;
	private Collection listProductsPerPartner;
	private Collection listAnnulmentType;
	private Collection listPartnerPerProductOrBroker;
	private Collection listBrokerPerProductOrPartner;
	private Collection listProductPerPartnerOrBroker;
	
	public Collection getListChannel() {
		return listChannel;
	}
	public void setListChannel(Collection listChannel) {
		this.listChannel = listChannel;
	}
	public Collection getListIssuingStatus() {
		return listIssuingStatus;
	}
	public void setListIssuingStatus(Collection listIssuingStatus) {
		this.listIssuingStatus = listIssuingStatus;
	}
	public Collection getListInstallmentStatus() {
		return listInstallmentStatus;
	}
	public void setListInstallmentStatus(Collection listInstallmentStatus) {
		this.listInstallmentStatus = listInstallmentStatus;
	}
	public Collection getListUserPerPartner() {
		return listUserPerPartner;
	}
	public void setListUserPerPartner(Collection listUserPerPartner) {
		this.listUserPerPartner = listUserPerPartner;
	}
	public Collection getListProductsPerPartner() {
		return listProductsPerPartner;
	}
	public void setListProductsPerPartner(Collection listProductsPerPartner) {
		this.listProductsPerPartner = listProductsPerPartner;
	}
	public Collection getListAnnulmentType() {
		return listAnnulmentType;
	}
	public void setListAnnulmentType(Collection listAnnulmentType) {
		this.listAnnulmentType = listAnnulmentType;
	}
	public Collection getListPartnerPerProductOrBroker() {
		return listPartnerPerProductOrBroker;
	}
	public void setListPartnerPerProductOrBroker(
			Collection listPartnerPerProductOrBroker) {
		this.listPartnerPerProductOrBroker = listPartnerPerProductOrBroker;
	}
	public Collection getListBrokerPerProductOrPartner() {
		return listBrokerPerProductOrPartner;
	}
	public void setListBrokerPerProductOrPartner(
			Collection listBrokerPerProductOrPartner) {
		this.listBrokerPerProductOrPartner = listBrokerPerProductOrPartner;
	}
	public Collection getListProductPerPartnerOrBroker() {
		return listProductPerPartnerOrBroker;
	}
	public void setListProductPerPartnerOrBroker(
			Collection listProductPerPartnerOrBroker) {
		this.listProductPerPartnerOrBroker = listProductPerPartnerOrBroker;
	}
}
