package br.com.tratomais.core.model.report;

import java.sql.Timestamp;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.tratomais.core.dao.PersistentEntity;

@Entity
@Table(name = "Report") 
public class Report implements PersistentEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 908935990852608797L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO) 
    @Column(name="ID")
	private Long id;
	
	@Transient
	private Integer reportId;
	
	/** The Name of Report */
	@Column(name="NAME")
    private String name;
    
    /** Type of report (diarily, monthly, weekly) */
	@Column(name="TYPE")
    private Integer type;
    
    /** BSC Report Perspective id: Financial, Process, Customer or Training */
	@Column(name="REPORT_PERSPECTIVE_ID")
    private String reportPerspectiveId;
    
    /** SQL Statement to be executed */
	@Column(name="SQL_STEMENT", columnDefinition="TEXT")
    private String sqlStement;
    
    /** Hour of day (0-23) that report must be executed */
	@Column(name="EXECUTION_HOUR")
    private Integer executionHour;
    
    /** Timestamp of last execution of report */
	@Column(name="LAST_EXECUTION")
    private Timestamp lastExecution;
    
    /** Project related */
    //private ProjectTO project;
    
    /** Timestamp of report ending */
	@Column(name="FINAL_DATE")
    private Timestamp finalDate;
    
    /** DateType of report result (date, number, etc) */
	@Column(name="DATA_TYPE")
    private Integer dataType;
    
    /** The path (absolute path) of Jasper file */
	@Column(name="REPORT_FILE_NAME")
    private String reportFileName;
    
    /** The locale of user that is performing the report.*/
	@Column(name="LACALE")
    private Locale locale;

    //private UserTO handler;    
	@Column(name="PROFILE")
    private String profile;
	
	@Column(name="EXPORT_REPORT_FORMAT")
    private String exportReportFormat; //transient attribute
	
	@Column(name="GOAL")
    private String goal;
	
	@Column(name="TOLERANCE")
    private String tolerance;
	
	@Column(name="TOLERANCE_TYPE")
    private String toleranceType;

	@Column(name="REPORT_CONNECTION")
    private String reportConnection;	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getReportId() {
		return reportId;
	}
	public void setReportId(Integer reportId) {
		this.reportId = reportId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getReportPerspectiveId() {
		return reportPerspectiveId;
	}
	public void setReportPerspectiveId(String reportPerspectiveId) {
		this.reportPerspectiveId = reportPerspectiveId;
	}
	public String getSqlStement() {
		return sqlStement;
	}
	public void setSqlStement(String sqlStement) {
		this.sqlStement = sqlStement;
	}
	public Integer getExecutionHour() {
		return executionHour;
	}
	public void setExecutionHour(Integer executionHour) {
		this.executionHour = executionHour;
	}
	public Timestamp getLastExecution() {
		return lastExecution;
	}
	public void setLastExecution(Timestamp lastExecution) {
		this.lastExecution = lastExecution;
	}
	public Timestamp getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(Timestamp finalDate) {
		this.finalDate = finalDate;
	}
	public Integer getDataType() {
		return dataType;
	}
	public void setDataType(Integer dataType) {
		this.dataType = dataType;
	}
	public String getReportFileName() {
		return reportFileName;
	}
	public void setReportFileName(String reportFileName) {
		this.reportFileName = reportFileName;
	}
	public Locale getLocale() {
		return locale;
	}
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public String getExportReportFormat() {
		return exportReportFormat;
	}
	public void setExportReportFormat(String exportReportFormat) {
		this.exportReportFormat = exportReportFormat;
	}
	public String getGoal() {
		return goal;
	}
	public void setGoal(String goal) {
		this.goal = goal;
	}
	public String getTolerance() {
		return tolerance;
	}
	public void setTolerance(String tolerance) {
		this.tolerance = tolerance;
	}
	public String getToleranceType() {
		return toleranceType;
	}
	public void setToleranceType(String toleranceType) {
		this.toleranceType = toleranceType;
	}
    public String getReportConnection() {
        return reportConnection;
    }
    public void setReportConnection(String reportConnection) {
        this.reportConnection = reportConnection;
    }	
}
