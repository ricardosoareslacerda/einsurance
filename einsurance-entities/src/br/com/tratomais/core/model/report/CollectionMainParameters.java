package br.com.tratomais.core.model.report;



import java.sql.Date;

import br.com.tratomais.core.dao.PersistentEntity;

public class CollectionMainParameters implements PersistentEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int billingMethodId;
	private Date effectiveDate;
	private Date expiryDate;
	private String lotCode;
	private String clientReference;
	private String billingReference;
	private int recordType;
	private int page;
	private int totalPage;
	private int currentPage;
	private int lotType;
	private int exchangeId;
	private int lotId;
	private int statusId;
	
	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public int getBillingMethodId() {
		return billingMethodId;
	}

	public void setBillingMethodId(int billingMethodId) {
		this.billingMethodId = billingMethodId;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}
	
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	public Date getExpiryDate() {
		return expiryDate;
	}
	
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	public String getLotCode() {
		return lotCode;
	}
	public void setLotCode(String lotCode) {
		this.lotCode = lotCode;
	}
	public String getClientReference() {
		return clientReference;
	}
	public void setClientReference(String clientReference) {
		this.clientReference = clientReference;
	}
	public String getBillingReference() {
		return billingReference;
	}
	public void setBillingReference(String billingReference) {
		this.billingReference = billingReference;
	}
	
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	
	public int getRecordType() {
		return recordType;
	}

	public void setRecordType(int recordType) {
		this.recordType = recordType;
	}	
	
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}	
	
	public int getLotType() {
		return lotType;
	}

	public void setLotType(int lotType) {
		this.lotType = lotType;
	}

	public int getExchangeId() {
		return exchangeId;
	}

	public void setExchangeId(int exchangeId) {
		this.exchangeId = exchangeId;
	}

	public int getLotId() {
		return lotId;
	}

	public void setLotId(int lotId) {
		this.lotId = lotId;
	}	
}
