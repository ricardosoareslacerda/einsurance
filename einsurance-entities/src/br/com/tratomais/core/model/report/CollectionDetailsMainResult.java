package br.com.tratomais.core.model.report;

import java.math.BigDecimal;

import br.com.tratomais.core.dao.PersistentEntity;

public class CollectionDetailsMainResult implements PersistentEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String detailsStatus;
	private int lineNumber;
	private String clientReference;
	private String billingReference;
	private BigDecimal installmentValue;
	private String recordType;
	private String responseCode;
	private String responseDescription;
	private String textLine;
	private String errorMessage;
	private int fileLotDetailsStatus;
	
	public CollectionDetailsMainResult(){}
	
	public CollectionDetailsMainResult(String detailsStatus, int lineNumber,
											String clientReference, String billingReference,
											BigDecimal installmentValue, String recordType,
											String responseCode, String responseDescription,
											String textLine,String errorMessage, int fileLotDetailsStatus){
		this.detailsStatus = detailsStatus;
		this.lineNumber = lineNumber;
		this.clientReference = clientReference;
		this.billingReference = billingReference;
		this.installmentValue = installmentValue;
		this.recordType = recordType;
		this.responseCode = responseCode;
		this.responseDescription = responseDescription;
		this.textLine = textLine;
		this.errorMessage = errorMessage;
		this.fileLotDetailsStatus = fileLotDetailsStatus;
	}
	
	public String getDetailsStatus() {
		return detailsStatus;
	}
	
	public void setDetailsStatus(String detailsStatus) {
		this.detailsStatus = detailsStatus;
	}
	
	public int getLineNumber() {
		return lineNumber;
	}
	
	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}
	
	public String getClientReference() {
		return clientReference;
	}
	
	public void setClientReference(String clientReference) {
		this.clientReference = clientReference;
	}
	
	public String getBillingReference() {
		return billingReference;
	}
	
	public void setBillingReference(String billingReference) {
		this.billingReference = billingReference;
	}
	
	public BigDecimal getInstallmentValue() {
		return installmentValue;
	}
	
	public void setInstallmentValue(BigDecimal installmentValue) {
		this.installmentValue = installmentValue;
	}
	
	public String getRecordType() {
		return recordType;
	}
	
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	
	public String getResponseCode() {
		return responseCode;
	}
	
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	
	public String getResponseDescription() {
		return responseDescription;
	}
	
	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}
	
	public String getTextLine() {
		return textLine;
	}
	
	public void setTextLine(String textLine) {
		this.textLine = textLine;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public int getFileLotDetailsStatus() {
		return fileLotDetailsStatus;
	}

	public void setFileLotDetailsStatus(int fileLotDetailsStatus) {
		this.fileLotDetailsStatus = fileLotDetailsStatus;
	}
}
