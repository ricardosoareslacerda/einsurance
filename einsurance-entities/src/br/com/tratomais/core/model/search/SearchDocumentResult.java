package br.com.tratomais.core.model.search;

import java.math.BigInteger;
import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

public class SearchDocumentResult implements PersistentEntity{

	private int endorsementId;	
	private int contractId;
	private BigInteger policyNumber;
	private String channelExternalCode;
	private BigInteger certificateNumber;
	private String productName;
	private String insuredName;
	private String paymentTerm; 
	private String issuingStatus;
	private static final long serialVersionUID = 1L;
	private int productId;
	private int objectId;
	private Boolean changeLocked;
	private int issuingStatusId;
	private String issuanceType;
	private int issuanceTypeId;
	private Integer endorsementType;
	private int contractStatusId;
	private String contractStatus;
	private Date effectiveDate;
	private int lastEndorsementId;
	private Boolean renewed;
	private Boolean existsRenewalAlert;
	private String brokerCode;
	private Integer brokerId;
	private String brokerNickName;
	private Integer channelId;
	private String channelNickName;
	private String contractStatusCode;
	private Date expiryDate;
	private String externalKey;
	private String insuredDocumentNumber;
	private Integer insuredDocumentTypeId;
	private Integer insuredId;
	private String insurerCode;
	private Integer insurerId;
	private String insurerNickName;
	private String issuanceStatusCode;
	private String issuanceTypeCode;
	private String partnerCode;
	private Integer partnerId;
	private String partnerNickName;
	private String productCode;
	private String subsidiaryCode;
	private Integer subsidiaryId;
	private String subsidiaryNickName;
	private String login;


	public SearchDocumentResult(){
		
	}
	
	public BigInteger getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(BigInteger policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getChannelExternalCode() {
		return channelExternalCode;
	}

	public void setChannelExternalCode(String channelExternalCode) {
		this.channelExternalCode = channelExternalCode;
	}	
	
	public BigInteger getCertificateNumber() {
		return certificateNumber;
	}

	public void setCertificateNumber(BigInteger certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	public String getInsuredName() {
		return insuredName;
	}

	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	public int getContractId() {
		return contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getEndorsementId() {
		return endorsementId;
	}

	public void setEndorsementId(int endorsementId) {
		this.endorsementId = endorsementId;
	}

	public String getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public String getIssuingStatus() {
		return issuingStatus;
	}

	public void setIssuingStatus(String issuingStatus) {
		this.issuingStatus = issuingStatus;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getProductId() {
		return productId;
	}

	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}

	public int getObjectId() {
		return objectId;
	}

	public Boolean getChangeLocked() {
		return changeLocked;
	}

	public void setChangeLocked(Boolean changeLocked) {
		this.changeLocked = changeLocked;
	}

	public int getIssuingStatusId() {
		return issuingStatusId;
	}

	public void setIssuingStatusId(int issuingStatusId) {
		this.issuingStatusId = issuingStatusId;
	}

	public String getIssuanceType() {
		return issuanceType;
	}

	public void setIssuanceType(String issuanceType) {
		this.issuanceType = issuanceType;
	}

	public int getIssuanceTypeId() {
		return issuanceTypeId;
	}

	public void setIssuanceTypeId(int issuanceTypeId) {
		this.issuanceTypeId = issuanceTypeId;
	}

	public Integer getEndorsementType() {
		return endorsementType;
	}

	public void setEndorsementType(Integer endorsementType) {
		this.endorsementType = endorsementType;
	}

	public int getContractStatusId() {
		return contractStatusId;
	}

	public void setContractStatusId(int contractStatusId) {
		this.contractStatusId = contractStatusId;
	}

	public String getContractStatus() {
		return contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	public int getLastEndorsementId(){
		return lastEndorsementId;
	}
	
	public void setLastEndorsementId(int lastEndorsementId){
		this.lastEndorsementId = lastEndorsementId;
	}

	public void setRenewed(Boolean renewed) {
		this.renewed = renewed;
	}

	public boolean isRenewed() {
		if (renewed == null)
			return false;
		return renewed;
	}

	public void setExistsRenewalAlert(Boolean existsRenewalAlert) {
		this.existsRenewalAlert = existsRenewalAlert;
	}

	public boolean isExistsRenewalAlert() {
		if (existsRenewalAlert == null)
			return false;
		return existsRenewalAlert;
	}

	public void setBrokerCode(String brokerCode) {
		this.brokerCode = brokerCode;
	}

	public String getBrokerCode() {
		return brokerCode;
	}

	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	public String getBrokerNickName() {
		return brokerNickName;
	}

	public void setBrokerNickName(String brokerNickName) {
		this.brokerNickName = brokerNickName;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public String getChannelNickName() {
		return channelNickName;
	}

	public void setChannelNickName(String channelNickName) {
		this.channelNickName = channelNickName;
	}

	public String getContractStatusCode() {
		return contractStatusCode;
	}

	public void setContractStatusCode(String contractStatusCode) {
		this.contractStatusCode = contractStatusCode;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getExternalKey() {
		return externalKey;
	}

	public void setExternalKey(String externalKey) {
		this.externalKey = externalKey;
	}

	public String getInsuredDocumentNumber() {
		return insuredDocumentNumber;
	}

	public void setInsuredDocumentNumber(String insuredDocumentNumber) {
		this.insuredDocumentNumber = insuredDocumentNumber;
	}

	public Integer getInsuredDocumentTypeId() {
		return insuredDocumentTypeId;
	}

	public void setInsuredDocumentTypeId(Integer insuredDocumentTypeId) {
		this.insuredDocumentTypeId = insuredDocumentTypeId;
	}

	public Integer getInsuredId() {
		return insuredId;
	}

	public void setInsuredId(Integer insuredId) {
		this.insuredId = insuredId;
	}

	public String getInsurerCode() {
		return insurerCode;
	}

	public void setInsurerCode(String insurerCode) {
		this.insurerCode = insurerCode;
	}

	public Integer getInsurerId() {
		return insurerId;
	}

	public void setInsurerId(Integer insurerId) {
		this.insurerId = insurerId;
	}

	public String getInsurerNickName() {
		return insurerNickName;
	}

	public void setInsurerNickName(String insurerNickName) {
		this.insurerNickName = insurerNickName;
	}

	public String getIssuanceStatusCode() {
		return issuanceStatusCode;
	}

	public void setIssuanceStatusCode(String issuanceStatusCode) {
		this.issuanceStatusCode = issuanceStatusCode;
	}

	public String getIssuanceTypeCode() {
		return issuanceTypeCode;
	}

	public void setIssuanceTypeCode(String issuanceTypeCode) {
		this.issuanceTypeCode = issuanceTypeCode;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public Integer getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(Integer partnerId) {
		this.partnerId = partnerId;
	}

	public String getPartnerNickName() {
		return partnerNickName;
	}

	public void setPartnerNickName(String partnerNickName) {
		this.partnerNickName = partnerNickName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getSubsidiaryCode() {
		return subsidiaryCode;
	}

	public void setSubsidiaryCode(String subsidiaryCode) {
		this.subsidiaryCode = subsidiaryCode;
	}

	public Integer getSubsidiaryId() {
		return subsidiaryId;
	}

	public void setSubsidiaryId(Integer subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}

	public String getSubsidiaryNickName() {
		return subsidiaryNickName;
	}

	public void setSubsidiaryNickName(String subsidiaryNickName) {
		this.subsidiaryNickName = subsidiaryNickName;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getLogin() {
		return login;
	}
}
