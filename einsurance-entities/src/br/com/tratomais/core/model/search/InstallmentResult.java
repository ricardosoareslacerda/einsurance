package br.com.tratomais.core.model.search;

import java.math.BigDecimal;
import java.util.Date;

public class InstallmentResult {

	private BigDecimal policyNumber;
	private BigDecimal certificateNumber;
	private Integer contractId;
	private Integer endorsementId;
	private Integer installmentId;
	private Integer holderId;
	private Integer holderDocumentTypeID;
	private String holderDocumentNumber;
	private String holderName;
	private Integer billingMethodId;
	private Integer paymentTypeId;
	private Integer paymentTermId;
	private String paymentDescription;
	private Date effectiveDate;
	private Date expiryDate;
	private Date dueDate;
	private Date limitDate;
	private Date paymentDate;
	private Date cancelDate;
	private BigDecimal installmentValue;
	private BigDecimal paidValue;
	private Boolean paymentNotified;
	private Integer statusId;
	private String statusDescription;

	public BigDecimal getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(BigDecimal policyNumber) {
		this.policyNumber = policyNumber;
	}

	public BigDecimal getCertificateNumber() {
		return certificateNumber;
	}

	public void setCertificateNumber(BigDecimal certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	public Integer getContractId() {
		return contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public Integer getEndorsementId() {
		return endorsementId;
	}

	public void setEndorsementId(Integer endorsementId) {
		this.endorsementId = endorsementId;
	}

	public Integer getInstallmentId() {
		return installmentId;
	}

	public void setInstallmentId(Integer installmentId) {
		this.installmentId = installmentId;
	}

	public Integer getHolderId() {
		return holderId;
	}

	public void setHolderId(Integer holderId) {
		this.holderId = holderId;
	}

	public Integer getHolderDocumentTypeID() {
		return holderDocumentTypeID;
	}

	public void setHolderDocumentTypeID(Integer holderDocumentTypeID) {
		this.holderDocumentTypeID = holderDocumentTypeID;
	}

	public String getHolderDocumentNumber() {
		return holderDocumentNumber;
	}

	public void setHolderDocumentNumber(String holderDocumentNumber) {
		this.holderDocumentNumber = holderDocumentNumber;
	}

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public Integer getBillingMethodId() {
		return billingMethodId;
	}

	public void setBillingMethodId(Integer billingMethodId) {
		this.billingMethodId = billingMethodId;
	}

	public Integer getPaymentTypeId() {
		return paymentTypeId;
	}

	public void setPaymentTypeId(Integer paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}

	public Integer getPaymentTermId() {
		return paymentTermId;
	}

	public void setPaymentTermId(Integer paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public String getPaymentDescription() {
		return paymentDescription;
	}

	public void setPaymentDescription(String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getLimitDate() {
		return limitDate;
	}

	public void setLimitDate(Date limitDate) {
		this.limitDate = limitDate;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public BigDecimal getInstallmentValue() {
		return installmentValue;
	}

	public void setInstallmentValue(BigDecimal installmentValue) {
		this.installmentValue = installmentValue;
	}

	public BigDecimal getPaidValue() {
		return paidValue;
	}

	public void setPaidValue(BigDecimal paidValue) {
		this.paidValue = paidValue;
	}

	public Boolean getPaymentNotified() {
		return paymentNotified;
	}

	public void setPaymentNotified(Boolean paymentNotified) {
		this.paymentNotified = paymentNotified;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
}
