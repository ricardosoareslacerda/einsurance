package br.com.tratomais.core.model.search;

public class InstallmentParameters {
	
	private int contractId;
	private String policyNumber;
	private String certificateNumber;
	private int documentTypeID;
	private String documentNumber;
	private int statusId;
	private boolean inArrears;
	
	private int totalPage;
	private int page;
	
	public int getContractId() {
		return contractId;
	}
	
	public void setContractId(int contractId) {
		this.contractId = contractId;
	}
	
	public String getPolicyNumber() {
		return policyNumber;
	}
	
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	
	public String getCertificateNumber() {
		return certificateNumber;
	}
	
	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	
	public int getDocumentTypeID() {
		return documentTypeID;
	}
	
	public void setDocumentTypeID(int documentTypeID) {
		this.documentTypeID = documentTypeID;
	}
	
	public String getDocumentNumber() {
		return documentNumber;
	}
	
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	
	public int getStatusId() {
		return statusId;
	}
	
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	
	public boolean isInArrears() {
		return inArrears;
	}
	
	public void setInArrears(boolean inArrears) {
		this.inArrears = inArrears;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}
}
