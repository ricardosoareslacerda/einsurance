package br.com.tratomais.core.model.search;

import br.com.tratomais.core.dao.PersistentEntity;

public class SearchDocumentParameters implements PersistentEntity{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int partnerId;
	private int contractId;	
	private String certificateNumber;
	private String policyNumber;
	private String name;
	private Integer documentType;
	private String documentNumber;
	private Boolean renewedOnly;
	private int page;
	private int totalPage;
	private int currentPage;
	
	public SearchDocumentParameters(int partnerId, 
									   int contractId, 
									   String certificateNumber, 
									   String name, 
									   Integer documentType, 
									   String documentNumber){
		this.partnerId = partnerId;
		this.contractId = contractId ;
		this.certificateNumber = certificateNumber;
		this.name = name;
		this.documentType = documentType;
		this.documentNumber = documentNumber;
	}
	
	public SearchDocumentParameters(){
	}
	
	public int getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(int partnerId) {
		this.partnerId = partnerId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public int getContractId() {
		return contractId;
	}	
	public String getCertificateNumber() {
		return certificateNumber;
	}
	
	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	
	public String getPolicyNumber() {
		return policyNumber;
	}
	
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getDocumentType() {
		return documentType;
	}
	
	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}
	
	public String getDocumentNumber() {
		return documentNumber;
	}
	
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	
	public void setRenewedOnly(Boolean renewedOnly) {
		this.renewedOnly = renewedOnly;
	}

	public Boolean getRenewedOnly() {
		return renewedOnly;
	}
	
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
}
