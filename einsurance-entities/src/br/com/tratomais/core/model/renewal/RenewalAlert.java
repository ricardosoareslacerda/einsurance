package br.com.tratomais.core.model.renewal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.tratomais.core.dao.PersistentEntity;
import br.com.tratomais.core.model.policy.Endorsement;

@Entity
@Table(name="RenewalAlert")
public class RenewalAlert  implements PersistentEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public final static int ALERT_TYPE_WARNING=626;
	public final static int ALERT_TYPE_CRITICAL=627;
	public final static int ALERT_TYPE_EXCEPTION=628;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="RenewalAlertID")
	private Integer sequenceId;
	public Integer getSequenceId() {
		return sequenceId;
	}
	public void setSequenceId(Integer sequenceId) {
		this.sequenceId = sequenceId;
	}
	
	@Column(name="ContractID", columnDefinition="integer")
	private Integer contractId;
	public Integer getContractId() {
		return contractId;
	}
	public void setContractId(Integer contract) {
		this.contractId = contract;
	}
	
	@Column(name="EndorsementID", columnDefinition="integer")
	private Integer endorsementId;
	public Integer getEndorsementId() {
		return endorsementId;
	}
	public void setEndorsementId(Integer endorsement) {
		this.endorsementId = endorsement;
	}
	
	@Column(name="ItemID", columnDefinition="integer")
	private Integer itemId;
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer item) {
		this.itemId = item;
	}
	
	@Column(name="RiskGroupID", columnDefinition="integer")
	private Integer riskGroupId;
	public Integer getRiskGroupId() {
		return riskGroupId;
	}
	public void setRiskGroupId(Integer riskGroupId) {
		this.riskGroupId = riskGroupId;
	}
	
	@Column(name="CoverageID", columnDefinition="integer")
	private Integer coverageId;
	public Integer getCoverageId() {
		return coverageId;
	}
	public void setCoverageId(Integer coverage) {
		this.coverageId = coverage;
	}
	
	@Column(name="AlertType", columnDefinition="integer")
	private Integer alertType;
	public Integer getAlertType() {
		return alertType;
	}
	public void setAlertType(Integer alertType) {
		this.alertType = alertType;
	}
	
	@Column(name="AlertCode", columnDefinition="integer")
	private Long alertCode;
	public Long getAlertCode() {
		return alertCode;
	}
	public void setAlertCode(Long alertCode) {
		this.alertCode = alertCode;
	}
	
	@Column(name="Description", columnDefinition="string", length=250)
	private String description;
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name="Resolved", columnDefinition="bit")
	private Boolean resolved;
	public Boolean getResolved() {
		return resolved;
	}
	public void setResolved(Boolean resolved) {
		this.resolved = resolved;
	}
	
	public void updateKey(Endorsement parent) {
		if ( parent.getId() == null ) {
			throw new IllegalArgumentException();
		}
		else {
			this.setContractId( parent.getId().getContractId() );
			this.setEndorsementId( parent.getId().getEndorsementId() );
		}
	}
}
