package br.com.tratomais.core.model;

import br.com.tratomais.core.dao.PersistentEntity;

public class Login implements PersistentEntity{

	private static final long serialVersionUID = -3418201378676476801L;
	private int loginId;
	private String domain;
	private String login;
	private String passwordKey;
	private boolean expirePassword;

	public Login() {
	}

	public Login(int loginId, String domain, String login, boolean expirePassword) {
		this.loginId = loginId;
		this.domain = domain;
		this.login = login;
		this.expirePassword = expirePassword;
	}

	public Login(int loginId, String domain, String login, String passwordKey, boolean expirePassword) {
		this.loginId = loginId;
		this.domain = domain;
		this.login = login;
		this.passwordKey = passwordKey;
		this.expirePassword = expirePassword;
	}

	public int getLoginId() {
		return this.loginId;
	}

	public void setLoginId(int loginId) {
		this.loginId = loginId;
	}

	public String getDomain() {
		return this.domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPasswordKey() {
		return this.passwordKey;
	}

	public void setPasswordKey(String passwordKey) {
		this.passwordKey = passwordKey;
	}

	public boolean isExpirePassword() {
		return this.expirePassword;
	}

	public void setExpirePassword(boolean expirePassword) {
		this.expirePassword = expirePassword;
	}

}
