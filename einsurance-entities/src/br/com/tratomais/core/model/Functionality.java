package br.com.tratomais.core.model;

import java.util.HashSet;
import java.util.Set;

import br.com.tratomais.core.dao.PersistentEntity;

public class Functionality implements PersistentEntity, Comparable<Functionality> {
	/**
	 * Gerado pelo Eclipse
	 */
	private static final long serialVersionUID = -325961253406208452L;
	private int functionalityId;
	private Functionality functionality;
	private String name;
	private String description;
	private String eventAction;
	private String messageProperty;
	private int displayOrder;
	private boolean active;
	private Set<Role> roles = new HashSet<Role>(0);
	private Set<Functionality> functionalities = new HashSet<Functionality>(0);
	private Set<Module> modules = new HashSet<Module>(0);
	private int checked;

	public Functionality() {
	}

	public Functionality(int functionalityId, String name, String eventAction,
			int displayOrder, boolean active) {
		this.functionalityId = functionalityId;
		this.name = name;
		this.eventAction = eventAction;
		this.displayOrder = displayOrder;
		this.active = active;
	}

	public Functionality(int functionalityId, Functionality functionality,
			String name, String description, String eventAction,
			String messageProperty, int displayOrder, boolean active,
			Set<Role> roles, Set<Functionality> functionalities,
			Set<Module> modules) {
		this.functionalityId = functionalityId;
		this.functionality = functionality;
		this.name = name;
		this.description = description;
		this.eventAction = eventAction;
		this.messageProperty = messageProperty;
		this.displayOrder = displayOrder;
		this.active = active;
		this.roles = roles;
		this.functionalities = functionalities;
		this.modules = modules;
	}

	public int getFunctionalityId() {
		return this.functionalityId;
	}

	public void setFunctionalityId(int functionalityId) {
		this.functionalityId = functionalityId;
	}

	public Functionality getFunctionality() {
		return this.functionality;
	}

	public void setFunctionality(Functionality functionality) {
		this.functionality = functionality;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEventAction() {
		return this.eventAction;
	}

	public void setEventAction(String eventAction) {
		this.eventAction = eventAction;
	}

	public String getMessageProperty() {
		return this.messageProperty;
	}

	public void setMessageProperty(String messageProperty) {
		this.messageProperty = messageProperty;
	}

	public int getDisplayOrder() {
		return this.displayOrder;
	}

	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Set<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Set<Functionality> getFunctionalities() {
		return this.functionalities;
	}

	public void setFunctionalities(Set<Functionality> functionalities) {
		this.functionalities = functionalities;
	}

	public Set<Module> getModules() {
		return this.modules;
	}

	public void setModules(Set<Module> modules) {
		this.modules = modules;
	}

	public String getLabel() {
		return this.name;
	}

	public void setLabel(String label) {
		// this.name = label;
	}

	public void setChildren(Set<Module> childrens) {
		// this.modules = childrens;
	}

	public Set<Functionality> getChildren() {
		return functionalities;
	}

	public Boolean hasChildren() {
		return !this.functionalities.isEmpty();
	}

	public int getChecked() {
		return checked;
	}

	public void setChecked(int checked) {
		this.checked = checked;
	}
	
	@Override
	public int compareTo(Functionality other) {
		return new Integer(displayOrder).compareTo(other.getDisplayOrder());
	}

}
