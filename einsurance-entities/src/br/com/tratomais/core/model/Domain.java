package br.com.tratomais.core.model;

import br.com.tratomais.core.dao.PersistentEntity;

// Generated 06/01/2010 19:59:11 by Hibernate Tools 3.2.4.GA

/**
 * Domain generated by hbm2java
 */
public class Domain implements PersistentEntity {

	private static final long serialVersionUID = 8036719390047573307L;
	private int domainId;
	private String name;
	private int autenticationType;

	public Domain() {
	}

	public Domain(int domainId, String name, int autenticationType) {
		this.domainId = domainId;
		this.name = name;
		this.autenticationType = autenticationType;
	}

	public int getDomainId() {
		return this.domainId;
	}

	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAutenticationType() {
		return this.autenticationType;
	}

	public void setAutenticationType(int autenticationType) {
		this.autenticationType = autenticationType;
	}

}
