package br.com.tratomais.core.model.customer;

// Generated 06/01/2010 16:24:25 by Hibernate Tools 3.2.4.GA

import java.util.HashSet;
import java.util.Set;

import br.com.tratomais.core.dao.PersistentEntity;
/**
 * Subsidiary generated by hbm2java
 */
public class Subsidiary implements PersistentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4326529616995627506L;
	private int subsidiaryId;
	private int insurerId;
	private String name;
	private String nickName;
	private String externalCode;
	private String addressStreet;
	private String districtName;
	private Integer cityId;
	private String cityName;
	private String regionName;
	private Integer stateId;
	private String stateName;
	private String zipCode;
	private Integer countryId;
	private String countryName;
	private String emailAddress;
	private String phoneNumber;
	private String faxNumber;
	private boolean active;
	private Set< Channel > channels = new HashSet< Channel >( 0 );

	public Subsidiary() {
	}

	public Subsidiary(int subsidiaryId, int insurerId, String name, boolean active) {
		this.subsidiaryId = subsidiaryId;
		this.insurerId = insurerId;
		this.name = name;
		this.active = active;
	}

	public Subsidiary(int subsidiaryId, int insurerId, String name, String nickName, String externalCode, String addressStreet, String districtName,
			Integer cityId, String cityName, String regionName, Integer stateId, String stateName, String zipCode, Integer countryId,
			String countryName, String emailAddress, String phoneNumber, String faxNumber, boolean active, Set< Channel > channels) {
		this.subsidiaryId = subsidiaryId;
		this.insurerId = insurerId;
		this.name = name;
		this.nickName = nickName;
		this.externalCode = externalCode;
		this.addressStreet = addressStreet;
		this.districtName = districtName;
		this.cityId = cityId;
		this.cityName = cityName;
		this.regionName = regionName;
		this.stateId = stateId;
		this.stateName = stateName;
		this.zipCode = zipCode;
		this.countryId = countryId;
		this.countryName = countryName;
		this.emailAddress = emailAddress;
		this.phoneNumber = phoneNumber;
		this.faxNumber = faxNumber;
		this.active = active;
		this.channels = channels;
	}

	public int getSubsidiaryId() {
		return this.subsidiaryId;
	}

	public void setSubsidiaryId(int subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}

	public int getInsurerId() {
		return this.insurerId;
	}

	public void setInsurerId(int insurerId) {
		this.insurerId = insurerId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNickName() {
		return this.nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getExternalCode() {
		return this.externalCode;
	}

	public void setExternalCode(String externalCode) {
		this.externalCode = externalCode;
	}

	public String getAddressStreet() {
		return this.addressStreet;
	}

	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}

	public String getDistrictName() {
		return this.districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public Integer getCityId() {
		return this.cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getRegionName() {
		return this.regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public Integer getStateId() {
		return this.stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return this.stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public Integer getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getFaxNumber() {
		return this.faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Set< Channel > getChannels() {
		return this.channels;
	}

	public void setChannels(Set< Channel > channels) {
		this.channels = channels;
	}

}
