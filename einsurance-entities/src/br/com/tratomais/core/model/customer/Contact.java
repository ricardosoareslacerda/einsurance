package br.com.tratomais.core.model.customer;

// Generated 15/01/2010 14:13:11 by Hibernate Tools 3.2.4.GA

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

/**
 * Contact generated by hbm2java
 */
public class Contact implements PersistentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5344641740531556603L;
	private int contactId;
	private Customer customer;
	private int contactType;
	private String name;
	private String lastName;
	private String firstName;
	private String title;
	private Integer documentType;
	private String documentNumber;
	private String documentIssuer;
	private Date documentValidity;
	private String emailAddress;
	private String phoneNumber;
	private String faxNumber;
	private boolean active;

	public Contact() {
	}

	public Contact(int contactId, Customer customer, int contactType, String name, boolean active) {
		this.contactId = contactId;
		this.customer = customer;
		this.contactType = contactType;
		this.name = name;
		this.active = active;
	}

	public Contact(int contactId, Customer customer, int contactType, String name, String lastName, String firstName, String title,
			Integer documentType, String documentNumber, String documentIssuer, Date documentValidity, String emailAddress, String phoneNumber,
			String faxNumber, boolean active) {
		this.contactId = contactId;
		this.customer = customer;
		this.contactType = contactType;
		this.name = name;
		this.lastName = lastName;
		this.firstName = firstName;
		this.title = title;
		this.documentType = documentType;
		this.documentNumber = documentNumber;
		this.documentIssuer = documentIssuer;
		this.documentValidity = documentValidity;
		this.emailAddress = emailAddress;
		this.phoneNumber = phoneNumber;
		this.faxNumber = faxNumber;
		this.active = active;
	}

	public int getContactId() {
		return this.contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public int getContactType() {
		return this.contactType;
	}

	public void setContactType(int contactType) {
		this.contactType = contactType;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getDocumentType() {
		return this.documentType;
	}

	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	public String getDocumentNumber() {
		return this.documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getDocumentIssuer() {
		return this.documentIssuer;
	}

	public void setDocumentIssuer(String documentIssuer) {
		this.documentIssuer = documentIssuer;
	}

	public Date getDocumentValidity() {
		return this.documentValidity;
	}

	public void setDocumentValidity(Date documentValidity) {
		this.documentValidity = documentValidity;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getFaxNumber() {
		return this.faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
