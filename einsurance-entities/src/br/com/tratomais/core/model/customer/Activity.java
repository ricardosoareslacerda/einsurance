package br.com.tratomais.core.model.customer;

import br.com.tratomais.core.dao.PersistentEntity;

public class Activity implements PersistentEntity {

	private static final long serialVersionUID = -7743047802436269248L;
	private int activityId;
	private String name;
	private String externalCode;
//	private Set<Customer> customers = new HashSet<Customer>(0);

	public Activity() {
	}

	public Activity(int activityId, String name) {
		this.activityId = activityId;
		this.name = name;
	}

	public Activity(int activityId, String name, String externalCode
//			, Set<Customer> customers
			) {
		this.activityId = activityId;
		this.name = name;
		this.externalCode = externalCode;
//		this.customers = customers;
	}

	public int getActivityId() {
		return this.activityId;
	}

	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExternalCode() {
		return this.externalCode;
	}

	public void setExternalCode(String externalCode) {
		this.externalCode = externalCode;
	}

//	public Set<Customer> getCustomers() {
//		return this.customers;
//	}
//
//	public void setCustomers(Set<Customer> customers) {
//		this.customers = customers;
//	}

}
