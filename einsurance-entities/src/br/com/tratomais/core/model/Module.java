package br.com.tratomais.core.model;

// Generated 06/01/2010 20:25:39 by Hibernate Tools 3.2.4.GA

import java.util.HashSet;
import java.util.Set;

import br.com.tratomais.core.dao.PersistentEntity;

/**
 * Module generated by hbm2java
 */
public class Module implements PersistentEntity, Comparable<Module> {

	private static final long serialVersionUID = -7797211428442062272L;
	private int moduleId;
	private Module module;
	private Functionality functionality;
	private int applicationId;
	private String name;
	private String description;
	private String messageProperty;
	private String imageProperty;
	private int displayOrder;
	private boolean displayShow;
	private String shortKey;
	private boolean active;
	private Set<Module> modules = new HashSet<Module>(0);
	private int checked;

	public Module() {
	}

	public Module(int moduleId, int applicationId, String name, int displayOrder, boolean displayShow, boolean active) {
		this.moduleId = moduleId;
		this.applicationId = applicationId;
		this.name = name;
		this.displayOrder = displayOrder;
		this.displayShow = displayShow;
		this.active = active;
	}

	public Module(int moduleId, Module module, Functionality functionality, int applicationId, String name, String description, String messageProperty, String imageProperty, 
			int displayOrder, boolean displayShow, String shortKey, boolean active, Set<Module> modules) {
		this.moduleId = moduleId;
		this.module = module;
		this.functionality = functionality;
		this.applicationId = applicationId;
		this.name = name;
		this.description = description;
		this.messageProperty = messageProperty;
		this.imageProperty = imageProperty;
		this.displayOrder = displayOrder;
		this.displayShow = displayShow;
		this.shortKey = shortKey;
		this.active = active;
		this.modules = modules;
	}

	public int getModuleId() {
		return this.moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public Module getModule() {
		return this.module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	public Functionality getFunctionality() {
		return this.functionality;
	}

	public void setFunctionality(Functionality functionality) {
		this.functionality = functionality;
	}

	public int getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMessageProperty() {
		return this.messageProperty;
	}

	public void setMessageProperty(String messageProperty) {
		this.messageProperty = messageProperty;
	}

	public String getImageProperty() {
		return this.imageProperty;
	}

	public void setImageProperty(String imageProperty) {
		this.imageProperty = imageProperty;
	}

	public int getDisplayOrder() {
		return this.displayOrder;
	}

	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}

	public boolean getDisplayShow() {
		return this.displayShow;
	}

	public void setDisplayShow(boolean displayShow) {
		this.displayShow = displayShow;
	}
	
	public String getShortKey() {
		return this.shortKey;
	}

	public void setShortKey(String shortKey) {
		this.shortKey = shortKey;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Set<Module> getModules() {
		return this.modules;
	}

	public void setModules(Set<Module> modules) {
		this.modules = modules;
	}

	/**
	 * Codigo para integração com o componente de menu
	 */

	public String getLabel() {
		return this.name;
	}

	public void setLabel(String label) {
		// this.name = label;
	}

	public void setChildren(Set<Module> childrens) {
		// this.modules = childrens;
	}

	public Set<Module> getChildren() {
		return modules;
	}

	public Boolean hasChildren() {
		return !this.modules.isEmpty();
	}

	public int getChecked() {
		return checked;
	}

	public void setChecked(int checked) {
		this.checked = checked;
	}

	public int compareTo(Module other) {
		return new Integer(displayOrder).compareTo(other.getDisplayOrder());
	}
}
