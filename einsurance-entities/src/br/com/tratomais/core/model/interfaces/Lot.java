package br.com.tratomais.core.model.interfaces;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import br.com.tratomais.core.dao.PersistentEntity;

/** Represents the Lot Table */
public class Lot implements PersistentEntity {
	
	public static int LOT_TYPE_CLAIM = 621;
	public static int LOT_TYPE_TREASURY = 631;
	public static int LOT_TYPE_INTERFACE = 755;
	public static int LOT_TYPE_MIGRATION = 757;
	
	public static int LOT_STATUS_PENDING = 303;
	public static int LOT_STATUS_PROCESSED = 304;
	public static int LOT_STATUS_ERROR = 305;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private transient String statusDescription;
	
	private int lotID;
	private Integer parentLotID;
	private Exchange exchange;
	private int insurerID;
	private Integer partnerID;
	private String lotCode;
	private int lotType;
	private String fileName;
	private String source;
	private String destination;
	private Date sendDate;
	private Date recieveDate;
	private Date processDate;
	private int recordsQuantity;
	private Double recordsAmount;
	private Integer errorID;
	private String errorMessage;
	private int lotStatus;

	private Set<FileLot> fileLots = new HashSet<FileLot>(0);

	/**
	 * @return the lotId
	 */
	public int getLotID() {
		return lotID;
	}

	/**
	 * @param lotId the lotId to set
	 */
	public void setLotID(int lotID) {
		this.lotID = lotID;
	}

	/**
	 * @return the parent
	 */
	public Integer getParentLotID() {
		return parentLotID;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParentLotID(Integer parentLotID) {
		this.parentLotID = parentLotID;
	}

	/**
	 * @return the exchangeID
	 */
	public Exchange getExchange() {
		return exchange;
	}

	/**
	 * @param exchangeID the exchangeID to set
	 */
	public void setExchange(Exchange exchange) {
		this.exchange = exchange;
	}

	/**
	 * @return the insurerID
	 */
	public int getInsurerID() {
		return insurerID;
	}

	/**
	 * @param insurerID the insurerID to set
	 */
	public void setInsurerID(int insurerID) {
		this.insurerID = insurerID;
	}

	/**
	 * @return the partnerID
	 */
	public Integer getPartnerID() {
		return partnerID;
	}

	/**
	 * @param partnerID the partnerID to set
	 */
	public void setPartnerID(Integer partnerID) {
		this.partnerID = partnerID;
	}

	/**
	 * @return the lotCode
	 */
	public String getLotCode() {
		return lotCode;
	}

	/**
	 * @param lotCode the lotCode to set
	 */
	public void setLotCode(String lotCode) {
		this.lotCode = lotCode;
	}

	/**
	 * @return the lotType
	 */
	public int getLotType() {
		return lotType;
	}

	/**
	 * @param lotType the lotType to set
	 */
	public void setLotType(int lotType) {
		this.lotType = lotType;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * @param destination the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * @return the sendDate
	 */
	public Date getSendDate() {
		return sendDate;
	}

	/**
	 * @param sendDate the sendDate to set
	 */
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	/**
	 * @return the recieveDate
	 */
	public Date getRecieveDate() {
		return recieveDate;
	}

	/**
	 * @param recieveDate the recieveDate to set
	 */
	public void setRecieveDate(Date recieveDate) {
		this.recieveDate = recieveDate;
	}

	/**
	 * @return the processDate
	 */
	public Date getProcessDate() {
		return processDate;
	}

	/**
	 * @param processDate the processDate to set
	 */
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	/**
	 * @return the recordsQuantity
	 */
	public int getRecordsQuantity() {
		return recordsQuantity;
	}

	/**
	 * @param recordsQuantity the recordsQuantity to set
	 */
	public void setRecordsQuantity(int recordsQuantity) {
		this.recordsQuantity = recordsQuantity;
	}

	/**
	 * @return the recordAmount
	 */
	public Double getRecordsAmount() {
		return recordsAmount;
	}

	/**
	 * @param recordAmount the recordAmount to set
	 */
	public void setRecordsAmount(Double recordAmount) {
		this.recordsAmount = recordAmount;
	}

	/**
	 * @return the errorID
	 */
	public Integer getErrorID() {
		return errorID;
	}

	/**
	 * @param errorID the errorID to set
	 */
	public void setErrorID(Integer errorID) {
		this.errorID = errorID;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the lotStatus
	 */
	public int getLotStatus() {
		return lotStatus;
	}

	/**
	 * @param lotStatus the lotStatus to set
	 */
	public void setLotStatus(int lotStatus) {
		this.lotStatus = lotStatus;
	}

	/**
	 * @return the fileLots
	 */
	public Set<FileLot> getFileLots() {
		return fileLots;
	}

	/**
	 * @param fileLots the fileLots to set
	 */
	public void setFileLots(Set<FileLot> fileLots) {
		this.fileLots = fileLots;
	}
	
	public void addFileLot(FileLot fileLot){
		this.fileLots.add(fileLot);
		fileLot.setLot(this);
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
}
