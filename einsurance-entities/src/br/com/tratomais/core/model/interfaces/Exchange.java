package br.com.tratomais.core.model.interfaces;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import br.com.tratomais.core.dao.PersistentEntity;

public class Exchange implements PersistentEntity{

	public static int EXCHANGE_TYPE_SEND = 596;
	public static int EXCHANGE_TYPE_RECEIVE = 597;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int exchangeID;
	private int exchangeType;
	private Integer partnerID;
	private String name;
	private String description;
	private String source;
	private String destination;
	private String sendClassMethod;
	private String recieveClassMethod;
	private String processClassMethod;
	private Integer sendSequenceNumber;
	private Integer recieveSequenceNumber;
	private Date lastSendDate;
	private Date lastRecieveDate;
	private Date lastProcessDate;
	private String exchangeGroup;
	private Date registred;
	private Date updated;
	private boolean active;
	private Set<Lot> lots = new HashSet<Lot>(0);
	/**
	 * @return the exchangeId
	 */
	public int getExchangeID() {
		return exchangeID;
	}
	/**
	 * @param exchangeId the exchangeId to set
	 */
	public void setExchangeID(int exchangeID) {
		this.exchangeID = exchangeID;
	}
	/**
	 * @return the exchangeType
	 */
	public int getExchangeType() {
		return exchangeType;
	}
	/**
	 * @param exchangeType the exchangeType to set
	 */
	public void setExchangeType(int exchangeType) {
		this.exchangeType = exchangeType;
	}
	/**
	 * @return the partnerID
	 */
	public Integer getPartnerID() {
		return partnerID;
	}
	/**
	 * @param partnerID the partnerID to set
	 */
	public void setPartnerID(Integer partnerID) {
		this.partnerID = partnerID;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}
	/**
	 * @param destination the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}
	/**
	 * @return the sendClassMethod
	 */
	public String getSendClassMethod() {
		return sendClassMethod;
	}
	/**
	 * @param sendClassMethod the sendClassMethod to set
	 */
	public void setSendClassMethod(String sendClassMethod) {
		this.sendClassMethod = sendClassMethod;
	}
	/**
	 * @return the recieveClassMethod
	 */
	public String getRecieveClassMethod() {
		return recieveClassMethod;
	}
	/**
	 * @param recieveClassMethod the recieveClassMethod to set
	 */
	public void setRecieveClassMethod(String recieveClassMethod) {
		this.recieveClassMethod = recieveClassMethod;
	}
	/**
	 * @return the processClassMethod
	 */
	public String getProcessClassMethod() {
		return processClassMethod;
	}
	/**
	 * @param processClassMethod the processClassMethod to set
	 */
	public void setProcessClassMethod(String processClassMethod) {
		this.processClassMethod = processClassMethod;
	}
	/**
	 * @return the sendSequenceNumber
	 */
	public Integer getSendSequenceNumber() {
		return sendSequenceNumber;
	}
	/**
	 * @param sendSequenceNumber the sendSequenceNumber to set
	 */
	public void setSendSequenceNumber(Integer sendSequenceNumber) {
		this.sendSequenceNumber = sendSequenceNumber;
	}
	/**
	 * @return the recieveSequenceNumber
	 */
	public Integer getRecieveSequenceNumber() {
		return recieveSequenceNumber;
	}
	/**
	 * @param recieveSequenceNumber the recieveSequenceNumber to set
	 */
	public void setRecieveSequenceNumber(Integer recieveSequenceNumber) {
		this.recieveSequenceNumber = recieveSequenceNumber;
	}
	/**
	 * @return the lastSendDate
	 */
	public Date getLastSendDate() {
		return lastSendDate;
	}
	/**
	 * @param lastSendDate the lastSendDate to set
	 */
	public void setLastSendDate(Date lastSendDate) {
		this.lastSendDate = lastSendDate;
	}
	/**
	 * @return the lastRecieveDate
	 */
	public Date getLastRecieveDate() {
		return lastRecieveDate;
	}
	/**
	 * @param lastRecieveDate the lastRecieveDate to set
	 */
	public void setLastRecieveDate(Date lastRecieveDate) {
		this.lastRecieveDate = lastRecieveDate;
	}
	/**
	 * @return the lastProcessDate
	 */
	public Date getLastProcessDate() {
		return lastProcessDate;
	}
	/**
	 * @param lastProcessDate the lastProcessDate to set
	 */
	public void setLastProcessDate(Date lastProcessDate) {
		this.lastProcessDate = lastProcessDate;
	}
	/**
	 * @return the exchangeGroup
	 */
	public String getExchangeGroup() {
		return exchangeGroup;
	}
	/**
	 * @param exchangeGroup the exchangeGroup to set
	 */
	public void setExchangeGroup(String exchangeGroup) {
		this.exchangeGroup = exchangeGroup;
	}
	/**
	 * @return the registred
	 */
	public Date getRegistred() {
		return registred;
	}
	/**
	 * @param registred the registred to set
	 */
	public void setRegistred(Date registred) {
		this.registred = registred;
	}
	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}
	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	/**
	 * @return the lots
	 */
	public Set<Lot> getLots() {
		return lots;
	}
	/**
	 * @param lots the lots to set
	 */
	public void setLots(Set<Lot> lots) {
		this.lots = lots;
	}
}
