package br.com.tratomais.core.model.interfaces;

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

/** Represents the FileLotDetails Table */
public class FileLotDetails implements PersistentEntity {
	
	public static int RECORD_TYPE_UNDEFINED = 598;
	public static int RECORD_TYPE_HEADER 	= 324;
	public static int RECORD_TYPE_DETAIL 	= 325;
	public static int RECORD_TYPE_TAIL 		= 326;
	
	public static int FILE_LOT_DETAIL_STATUS_PENDING = 321;
	public static int FILE_LOT_DETAIL_STATUS_PROCESSED = 322;
	public static int FILE_LOT_DETAIL_STATUS_ERROR = 323;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private transient String policyNumber;
	private transient String certificateNumber;
	private transient Integer subsidiaryID;
	private transient String statusDescription;
	
	private int fileLotDetailsID;
	private FileLot fileLot;
	private int recordType;
	private String operation;
	private Integer masterPolicyID;
	private Integer contractID;
	private Integer endorsementID;
	private Integer installmentID;
	private Integer itemID;
	private Integer lineNumber;
	private String textLine;
	private Date registryDate;
	private Date processDate;
	private Integer errorID;
	private String errorMessage;
	private String responseCode;
	private String responseDescription;
	private int fileLotDetailsStatus;
	/**
	 * @return the fileLotDetailsId
	 */
	public int getFileLotDetailsID() {
		return fileLotDetailsID;
	}
	/**
	 * @param fileLotDetailsID the fileLotDetailsId to set
	 */
	public void setFileLotDetailsID(int fileLotDetailsID) {
		this.fileLotDetailsID = fileLotDetailsID;
	}
	/**
	 * @return the fileLot
	 */
	public FileLot getFileLot() {
		return fileLot;
	}
	/**
	 * @param fileLot the fileLot to set
	 */
	public void setFileLot(FileLot fileLot) {
		this.fileLot = fileLot;
	}
	/**
	 * @return the recordType
	 */
	public int getRecordType() {
		return recordType;
	}
	/**
	 * @param recordType the recordType to set
	 */
	public void setRecordType(int recordType) {
		this.recordType = recordType;
	}
	/**
	 * @return the operation
	 */
	public String getOperation() {
		return operation;
	}
	/**
	 * @param operation the operation to set
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}
	/**
	 * @return the masterPolicyID
	 */
	public Integer getMasterPolicyID() {
		return masterPolicyID;
	}
	/**
	 * @param masterPolicyID the masterPolicyID to set
	 */
	public void setMasterPolicyID(Integer masterPolicyID) {
		this.masterPolicyID = masterPolicyID;
	}
	/**
	 * @return the contractID
	 */
	public Integer getContractID() {
		return contractID;
	}
	/**
	 * @param contractID the contractID to set
	 */
	public void setContractID(Integer contractID) {
		this.contractID = contractID;
	}
	/**
	 * @return the endorsementID
	 */
	public Integer getEndorsementID() {
		return endorsementID;
	}
	/**
	 * @param endorsementID the endorsementID to set
	 */
	public void setEndorsementID(Integer endorsementID) {
		this.endorsementID = endorsementID;
	}
	/**
	 * @return the installmentID
	 */
	public Integer getInstallmentID() {
		return installmentID;
	}
	/**
	 * @param installmentID the installmentID to set
	 */
	public void setInstallmentID(Integer installmentID) {
		this.installmentID = installmentID;
	}
	/**
	 * @return the itemID
	 */
	public Integer getItemID() {
		return itemID;
	}
	/**
	 * @param itemID the itemID to set
	 */
	public void setItemID(Integer itemID) {
		this.itemID = itemID;
	}
	/**
	 * @return the lineNumber
	 */
	public Integer getLineNumber() {
		return lineNumber;
	}
	/**
	 * @param lineNumber the lineNumber to set
	 */
	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}
	/**
	 * @return the textLine
	 */
	public String getTextLine() {
		return textLine;
	}
	/**
	 * @param textLine the textLine to set
	 */
	public void setTextLine(String textLine) {
		this.textLine = textLine;
	}
	/**
	 * @return the registryDate
	 */
	public Date getRegistryDate() {
		return registryDate;
	}
	/**
	 * @param registryDate the registryDate to set
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	/**
	 * @return the processDate
	 */
	public Date getProcessDate() {
		return processDate;
	}
	/**
	 * @param processDate the processDate to set
	 */
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
	/**
	 * @return the errorId
	 */
	public Integer getErrorID() {
		return errorID;
	}
	/**
	 * @param errorID the errorId to set
	 */
	public void setErrorID(Integer errorID) {
		this.errorID = errorID;
	}
	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}
	/**
	 * @param responseCode the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	/**
	 * @return the responseDescription
	 */
	public String getResponseDescription() {
		return responseDescription;
	}
	/**
	 * @param responseDescription the responseDescription to set
	 */
	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}
	/**
	 * @return the fileLotDetailsStatus
	 */
	public int getFileLotDetailsStatus() {
		return fileLotDetailsStatus;
	}
	/**
	 * @param fileLotDetailsStatus the fileLotDetailsStatus to set
	 */
	public void setFileLotDetailsStatus(int fileLotDetailsStatus) {
		this.fileLotDetailsStatus = fileLotDetailsStatus;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getCertificateNumber() {
		return certificateNumber;
	}
	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	public Integer getSubsidiaryID() {
		return subsidiaryID;
	}
	public void setSubsidiaryID(Integer subsidiaryID) {
		this.subsidiaryID = subsidiaryID;
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
}