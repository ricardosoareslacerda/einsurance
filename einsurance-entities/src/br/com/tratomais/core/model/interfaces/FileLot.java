package br.com.tratomais.core.model.interfaces;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import br.com.tratomais.core.dao.PersistentEntity;

/** Represents the FileLot Table */
public class FileLot implements PersistentEntity  {
	
	public static int FILE_LOT_TYPE_CLAIM_INPUT = 313;
	public static int FILE_LOT_TYPE_CLAIM_OUTPUT = 609;
	public static int FILE_LOT_TYPE_TREASURY_INPUT = 642;
	public static int FILE_LOT_TYPE_INMA_INPUT = 756;
	public static int FILE_LOT_TYPE_ISSUANCE = 762;
	
	public static int FILE_LOT_STATUS_PENDING = 318;
	public static int FILE_LOT_STATUS_PROCESSED = 319;
	public static int FILE_LOT_STATUS_ERROR = 320;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private transient String statusDescription;
	private transient String typeDescription;
	
	private int fileLotID;
	private Lot lot;
	private int fileLotType;
	private String fileName;
	private String source;
	private String destination;
	private Date sendDate;
	private Date recieveDate;
	private Date registryDate;
	private Date processDate;
	private int recordsQuantity;
	private Double recordsAmount;
	private Integer errorID;
	private String errorMessage;
	private int fileLotStatus;
	private Set<FileLotDetails> fileLotDetailses = new HashSet<FileLotDetails>(0);
	
	/**
	 * @return the fileLotId
	 */
	public int getFileLotID() {
		return fileLotID;
	}
	/**
	 * @param fileLotId the fileLotId to set
	 */
	public void setFileLotID(int fileLotID) {
		this.fileLotID = fileLotID;
	}
	/**
	 * @return the lot
	 */
	public Lot getLot() {
		return lot;
	}
	/**
	 * @param lot the lot to set
	 */
	public void setLot(Lot lot) {
		this.lot = lot;
	}
	/**
	 * @return the fileLotType
	 */
	public int getFileLotType() {
		return fileLotType;
	}
	/**
	 * @param fileLotType the fileLotType to set
	 */
	public void setFileLotType(int fileLotType) {
		this.fileLotType = fileLotType;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}
	/**
	 * @param destination the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}
	/**
	 * @return the sendDate
	 */
	public Date getSendDate() {
		return sendDate;
	}
	/**
	 * @param sendDate the sendDate to set
	 */
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	/**
	 * @return the recieveDate
	 */
	public Date getRecieveDate() {
		return recieveDate;
	}
	/**
	 * @param recieveDate the recieveDate to set
	 */
	public void setRecieveDate(Date recieveDate) {
		this.recieveDate = recieveDate;
	}
	/**
	 * @return the registryDate
	 */
	public Date getRegistryDate() {
		return registryDate;
	}
	/**
	 * @param registryDate the registryDate to set
	 */
	public void setRegistryDate(Date registryDate) {
		this.registryDate = registryDate;
	}
	/**
	 * @return the processDate
	 */
	public Date getProcessDate() {
		return processDate;
	}
	/**
	 * @param processDate the processDate to set
	 */
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
	/**
	 * @return the recordsQuantity
	 */
	public int getRecordsQuantity() {
		return recordsQuantity;
	}
	/**
	 * @param recordsQuantity the recordsQuantity to set
	 */
	public void setRecordsQuantity(int recordsQuantity) {
		this.recordsQuantity = recordsQuantity;
	}
	/**
	 * @return the recordsAmount
	 */
	public Double getRecordsAmount() {
		return recordsAmount;
	}
	/**
	 * @param recordsAmount the recordsAmount to set
	 */
	public void setRecordsAmount(Double recordsAmount) {
		this.recordsAmount = recordsAmount;
	}
	/**
	 * @return the errorId
	 */
	public Integer getErrorID() {
		return errorID;
	}
	/**
	 * @param errorID the errorId to set
	 */
	public void setErrorID(Integer errorID) {
		this.errorID = errorID;
	}
	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	/**
	 * @return the fileLotStatus
	 */
	public int getFileLotStatus() {
		return fileLotStatus;
	}
	/**
	 * @param fileLotStatus the fileLotStatus to set
	 */
	public void setFileLotStatus(int fileLotStatus) {
		this.fileLotStatus = fileLotStatus;
	}
	/**
	 * @return the fileLotDetailses
	 */
	public Set<FileLotDetails> getFileLotDetailses() {
		return fileLotDetailses;
	}
	/**
	 * @param fileLotDetailses the fileLotDetailses to set
	 */
	public void setFileLotDetailses(Set<FileLotDetails> fileLotDetailses) {
		this.fileLotDetailses = fileLotDetailses;
	}
	
	/**
	 * @param fileLotDetails To be added to the internal collection
	 */
	public void addFileLotDetail(FileLotDetails fileLotDetails){
		this.fileLotDetailses.add(fileLotDetails);
		fileLotDetails.setFileLot(this);
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
	public String getTypeDescription() {
		return typeDescription;
	}
	public void setTypeDescription(String typeDescription) {
		this.typeDescription = typeDescription;
	}
}
