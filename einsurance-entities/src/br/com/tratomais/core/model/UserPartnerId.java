package br.com.tratomais.core.model;

import br.com.tratomais.core.dao.PersistentEntity;

public class UserPartnerId implements PersistentEntity {

	private static final long serialVersionUID = -991048110303056953L;
	private int userId;
	private int partnerId;

	public UserPartnerId() {
	}

	public UserPartnerId(int userId, int partnerId) {
		this.userId = userId;
		this.partnerId = partnerId;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getPartnerId() {
		return this.partnerId;
	}

	public void setPartnerId(int partnerId) {
		this.partnerId = partnerId;
	}

	public boolean equals(Object other) {
		if ( ( this == other ) )
			return true;
		if ( ( other == null ) )
			return false;
		if ( !( other instanceof UserPartnerId ) )
			return false;
		UserPartnerId castOther = ( UserPartnerId ) other;

		return ( this.getUserId() == castOther.getUserId() ) && ( this.getPartnerId() == castOther.getPartnerId() );
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getUserId();
		result = 37 * result + this.getPartnerId();
		return result;
	}

}
