package br.com.tratomais.core.model.policy;

import br.com.tratomais.core.dao.PersistentEntityId;

public class ItemSequenceId extends PersistentEntityId {

	/**
	 * 
	 */
	private static final long serialVersionUID = -397157689208017776L;
	private int contractID;
	private int sequenceCode;

	public ItemSequenceId() {
	}

	public ItemSequenceId(int contractID, int sequenceCode) {
		this.contractID = contractID;
		this.sequenceCode = sequenceCode;
	}

	public int getContractID() {
		return contractID;
	}

	public void setContractID(int contractID) {
		this.contractID = contractID;
	}

	public int getSequenceCode() {
		return sequenceCode;
	}

	public void setSequenceCode(int sequenceCode) {
		this.sequenceCode = sequenceCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + contractID;
		result = prime * result + sequenceCode;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemSequenceId other = (ItemSequenceId) obj;
		if (contractID != other.contractID)
			return false;
		if (sequenceCode != other.sequenceCode)
			return false;
		return true;
	}
}
