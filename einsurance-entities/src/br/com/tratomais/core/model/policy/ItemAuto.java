package br.com.tratomais.core.model.policy;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.com.tratomais.core.info.Calculus;
import br.com.tratomais.general.utilities.DateUtilities;

public class ItemAuto extends Item {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int PERSONAL_RISK_TYPE_MOTORIST = 496;
	public static final int PERSONAL_KINSHIP_TYPE_OWN = 294;
	
	private static final int VALUE_REFER = 55;
	private static final int AGE_AUTO = 54;
	private static final int ZERO_KM = 53;
	private static final int DRIVER_GENDER = 30;
	private static final int DRIVER_AGE = 32;
	private static final int DRIVER_MARITAL_STATUS = 33;
	private static final int AUTO_MARK_ID = 37;
	private static final int AUTO_MODEL_ID = 38;
	private static final int AUTO_FUEL_TYPE = 39;
	private static final int AUTO_TRANSMISSION_TYPE = 40;
	private static final int AUTO_CLASS_TYPE = 41;
	private static final int AUTO_TARIFF_TYPE = 42;
	private static final int AUTO_CATEGORY_TYPE = 43;
	private static final int AUTO_GROUP_TYPE = 44;
	private static final int AUTO_USE_TYPE = 45;
	private static final int COUNTRY_ID = 46;
	private static final int STATE_ID = 47;
	private static final int CITY_ID = 48;
	private static final int LOCALITY_CODE = 49;
	private static final int PASSENGER_NUMBER = 50;
	private static final int YEAR_FABRICATION = 51;
	private static final int YEAR_MODEL = 52;
	private static final int COVERAGE_ID = 59;
	
	private Integer autoTableType;
	private String brandName;
	private String modelName;
	private Integer autoModelId;
	private Integer autoBrandId;
	private Integer autoFuelType;
	private Integer autoTransmissionType;
	private Integer autoClassType;
	private Integer autoTariffType;
	private Integer autoCategoryType;
	private Integer autoGroupType;
	private Integer autoUseType;
	private Integer countryID;
	private Integer stateID;
	private Integer cityID;
	private Integer localityCode;
	private Integer doorNumber;
	private Integer passengerNumber;
	private Integer yearFabrication;
	private Integer yearModel;
	private Boolean zeroKm;
	/** Value refer amount, expressed in local currency (conversions to foreign done by getter and setter) */
	private Double valueRefer;
	private Integer modalityType;
	private Double variationFactor;
	private Integer tonneQuantity;
	private Integer kmQuantity;
	private Integer colorType;
	private String licensePlate;
	private String chassisSerial;
	private String engineSerial;
	private String licenseCode;
	private Integer invoiceNumber;
	private Date invoiceDate;
	private Integer inspectionNumber;
	private Date inspectionDate;
	private Set<Motorist> motorists = new HashSet<Motorist>( 0 );
	
	@Override
	public Integer getAttributeValue(int attrNumber) {
		Motorist motorist = getMainMotorist();
		
		switch (attrNumber){
		case VALUE_REFER:
			return this.valueRefer.intValue();
		case AGE_AUTO:
			return DateUtilities.extractYear(new Date()) -  this.yearFabrication;
		case ZERO_KM:
			return this.zeroKm?1:0;
		case YEAR_MODEL:
			return this.yearModel;
		case YEAR_FABRICATION:
			return this.yearFabrication;
		case PASSENGER_NUMBER:
			return this.passengerNumber;
		case LOCALITY_CODE:
			return this.localityCode;
		case CITY_ID:
			return this.cityID;
		case STATE_ID:
			return this.stateID;
		case COUNTRY_ID:
			return this.countryID;
		case AUTO_USE_TYPE:
			return this.autoUseType;
		case AUTO_GROUP_TYPE:
			return this.autoGroupType;
		case AUTO_CATEGORY_TYPE:
			return this.autoCategoryType;
		case AUTO_TARIFF_TYPE:
			return this.autoTariffType;
		case AUTO_CLASS_TYPE:
			return this.autoClassType;
		case AUTO_TRANSMISSION_TYPE:
			return this.autoTransmissionType;
		case AUTO_FUEL_TYPE:
			return this.autoFuelType;
		case AUTO_MODEL_ID:
			return this.autoModelId;
		case AUTO_MARK_ID:
			return this.autoBrandId;
		case DRIVER_MARITAL_STATUS:
			if (motorist != null && motorist.getMaritalStatus() != null)
				return motorist.getMaritalStatus();
			return null;
		case DRIVER_GENDER:
			if (motorist != null && motorist.getGender() != null)
				return motorist.getGender();
			return null;
		case DRIVER_AGE:
			/*if (motorist != null && motorist.getBirthDate() != null)
				return DateUtilities.calculateAge(motorist.getBirthDate());*/
			return null;
		case COVERAGE_ID:
			return this.getTCoverageId();			
		}
		return null;
	}
	
	public Integer getAutoTableType() {
		return autoTableType;
	}

	public void setAutoTableType(Integer autoTableType) {
		this.autoTableType = autoTableType;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public Integer getAutoFuelType() {
		return autoFuelType;
	}

	public void setAutoFuelType(Integer autoFuelType) {
		this.autoFuelType = autoFuelType;
	}

	public Integer getAutoTransmissionType() {
		return autoTransmissionType;
	}

	public void setAutoTransmissionType(Integer autoTransmissionType) {
		this.autoTransmissionType = autoTransmissionType;
	}

	public Integer getAutoClassType() {
		return autoClassType;
	}

	public void setAutoClassType(Integer autoClassType) {
		this.autoClassType = autoClassType;
	}

	public Integer getAutoTariffType() {
		return autoTariffType;
	}

	public void setAutoTariffType(Integer autoTariffType) {
		this.autoTariffType = autoTariffType;
	}

	public Integer getAutoCategoryType() {
		return autoCategoryType;
	}

	public void setAutoCategoryType(Integer autoCategoryType) {
		this.autoCategoryType = autoCategoryType;
	}

	public Integer getAutoGroupType() {
		return autoGroupType;
	}

	public void setAutoGroupType(Integer autoGroupType) {
		this.autoGroupType = autoGroupType;
	}

	public Integer getAutoUseType() {
		return autoUseType;
	}

	public void setAutoUseType(Integer autoUseType) {
		this.autoUseType = autoUseType;
	}

	public Integer getCountryID() {
		return countryID;
	}

	public void setCountryID(Integer countryID) {
		this.countryID = countryID;
	}

	public Integer getStateID() {
		return stateID;
	}

	public void setStateID(Integer stateID) {
		this.stateID = stateID;
	}

	public Integer getCityID() {
		return cityID;
	}

	public void setCityID(Integer cityID) {
		this.cityID = cityID;
	}

	public Integer getLocalityCode() {
		return localityCode;
	}

	public void setLocalityCode(Integer localityCode) {
		this.localityCode = localityCode;
	}

	public Integer getDoorNumber() {
		return doorNumber;
	}

	public void setDoorNumber(Integer doorNumber) {
		this.doorNumber = doorNumber;
	}

	public Integer getPassengerNumber() {
		return passengerNumber;
	}

	public void setPassengerNumber(Integer passengerNumber) {
		this.passengerNumber = passengerNumber;
	}

	public Integer getYearFabrication() {
		return yearFabrication;
	}

	public void setYearFabrication(Integer yearFabrication) {
		this.yearFabrication = yearFabrication;
	}

	public Integer getYearModel() {
		return yearModel;
	}

	public void setYearModel(Integer yearModel) {
		this.yearModel = yearModel;
	}

	public Boolean getZeroKm() {
		return zeroKm;
	}

	public void setZeroKm(Boolean zeroKm) {
		this.zeroKm = zeroKm;
	}

	/**
	 * Retrieve the Value refer amount
	 * @return Value refer amount, expressed in foreing currency
	 */
	public Double getValueRefer() {
		return Calculus.toForeignCurrency(this.valueRefer, this.conversionRate());
	}

	/**
	 * Sets the Value refer amount
	 * @param valueRefer Value refer amount, expressed in foreing currency
	 */
	public void setValueRefer(Double valueRefer) {
		this.valueRefer = Calculus.toLocalCurrency(valueRefer, this.conversionRate());
	}

	public Integer getModalityType() {
		return modalityType;
	}

	public void setModalityType(Integer modalityType) {
		this.modalityType = modalityType;
	}

	public Double getVariationFactor() {
		return variationFactor;
	}

	public void setVariationFactor(Double variationFactor) {
		this.variationFactor = variationFactor;
	}

	public Integer getTonneQuantity() {
		return tonneQuantity;
	}

	public void setTonneQuantity(Integer tonneQuantity) {
		this.tonneQuantity = tonneQuantity;
	}

	public Integer getKmQuantity() {
		return kmQuantity;
	}

	public void setKmQuantity(Integer kmQuantity) {
		this.kmQuantity = kmQuantity;
	}

	public Integer getColorType() {
		return colorType;
	}

	public void setColorType(Integer colorType) {
		this.colorType = colorType;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public String getChassisSerial() {
		return chassisSerial;
	}

	public void setChassisSerial(String chassisSerial) {
		this.chassisSerial = chassisSerial;
	}

	public String getEngineSerial() {
		return engineSerial;
	}

	public void setEngineSerial(String engineSerial) {
		this.engineSerial = engineSerial;
	}

	public String getLicenseCode() {
		return licenseCode;
	}

	public void setLicenseCode(String licenseCode) {
		this.licenseCode = licenseCode;
	}

	public Integer getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(Integer invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Integer getInspectionNumber() {
		return inspectionNumber;
	}

	public void setInspectionNumber(Integer inspectionNumber) {
		this.inspectionNumber = inspectionNumber;
	}

	public Date getInspectionDate() {
		return inspectionDate;
	}

	public void setInspectionDate(Date inspectionDate) {
		this.inspectionDate = inspectionDate;
	}

	@Override
	public List<Integer> getAttributeListValue(int attrNumber) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Set<Motorist> getMotorists() {
		return motorists;
	}

	public void setMotorists(Set<Motorist> motorists) {
		this.motorists = motorists;
	}

	public Integer getAutoModelId() {
		return autoModelId;
	}

	public void setAutoModelId(Integer autoModelId) {
		this.autoModelId = autoModelId;
	}

	public Integer getAutoBrandId() {
		return autoBrandId;
	}

	public void setAutoBrandId(Integer autoBrandId) {
		this.autoBrandId = autoBrandId;
	}
	
	private Motorist getMainMotorist() {
		if (motorists.size() > 0) {
			for (Motorist curMotorist : motorists) {
				if (curMotorist.getMotoristType() == Motorist.MOTORIST_TYPE_MAIN) {
					return curMotorist;
				}
			}
		}
		return null;
	}
	
	public void updateKey(Endorsement parent){
		if ( parent.getId() == null ) {
			throw new IllegalArgumentException();
		}
		
		super.updateKey( parent );
		
		for ( Motorist workMotorist : this.getMotorists() ) {
			workMotorist.updateKey( this );
		}
	}
	
	public final Motorist addMotorist() {
		Motorist newMotorist = new Motorist();
		MotoristId newMotoristId = new MotoristId();

		if ( this.getId() != null  ) {
			newMotoristId.setItemId( this.getId().getItemId());
			newMotoristId.setEndorsementId( this.getId().getEndorsementId() );
			newMotoristId.setContractId( this.getId().getContractId() );
		}
		
		newMotoristId.setMotoristId( getNextMotoristId() );
		newMotorist.setId( newMotoristId );
		
		newMotorist.setItemAuto( this );
		this.getMotorists().add( newMotorist );
		
		return newMotorist;
	}
	
	public final Motorist attachMotorist(Motorist newMotorist){
		this.getMotorists().add( newMotorist );
		newMotorist.updateKey( this );
		return newMotorist;
	}
	
	public int getNextMotoristId() {
		int maxSequenceId = 0;
		
		for ( Motorist tmpMotorist : this.getMotorists() ) {
			if ( tmpMotorist.getId() != null && tmpMotorist.getId().getMotoristId() > maxSequenceId ) {
				maxSequenceId = tmpMotorist.getId().getMotoristId();
			}
		}
		
		maxSequenceId++;
		return maxSequenceId;
	}
	
	@Override
	public Object clone(){
		return this.copyTo(new ItemAuto());
	}
	
	protected ItemAuto copyTo(ItemAuto item) {
		item.autoTableType = this.autoTableType;
		item.brandName = this.brandName;
		item.modelName = this.modelName;
		item.autoModelId = this.autoModelId;
		item.autoBrandId = this.autoBrandId;
		item.autoFuelType = this.autoFuelType;
		item.autoTransmissionType = this.autoTransmissionType;
		item.autoClassType = this.autoClassType;
		item.autoTariffType = this.autoTariffType;
		item.autoCategoryType = this.autoCategoryType;
		item.autoGroupType = this.autoGroupType;
		item.autoUseType = this.autoUseType;
		item.countryID = this.countryID;
		item.stateID = this.stateID;
		item.cityID = this.cityID;
		item.localityCode = this.localityCode;
		item.doorNumber = this.doorNumber;
		item.passengerNumber = this.passengerNumber;
		item.yearFabrication = this.yearFabrication;
		item.yearModel = this.yearModel;
		item.zeroKm = this.zeroKm;
		item.valueRefer = this.valueRefer;
		item.modalityType = this.modalityType;
		item.variationFactor = this.variationFactor;
		item.tonneQuantity = this.tonneQuantity;
		item.kmQuantity = this.kmQuantity;
		item.colorType = this.colorType;
		item.licensePlate = this.licensePlate;
		item.chassisSerial = this.chassisSerial;
		item.engineSerial = this.engineSerial;
		item.licenseCode = this.licenseCode;
		item.invoiceNumber = this.invoiceNumber;
		item.invoiceDate = this.invoiceDate;
		item.inspectionNumber = this.inspectionNumber;
		item.inspectionDate = this.inspectionDate;
		item.motorists = this.motorists;
		super.copyTo(item);
		return item;
	}

	/** {@inheritDoc} */
	@Override
	void refreshConversionRate(Double conversionRateOld, Double conversionRateNew) {
		
		super.refreshConversionRate(conversionRateOld, conversionRateNew);
		
		Double 	valueReferForeign = Calculus.toForeignCurrency(this.valueRefer, conversionRateOld);
		
		this.valueRefer = Calculus.toLocalCurrency(valueReferForeign, conversionRateNew);
	}
}