package br.com.tratomais.core.model.policy;

import br.com.tratomais.core.model.product.PlanPersonalRisk;
import br.com.tratomais.core.model.product.ProductOption;

public class EndorsementOption {

	private Endorsement endorsement;
	private int action;
	private ProductOption productOption;
	private String partnerName;
	private String channelName;
	private PlanPersonalRisk planPersonalRisk;
	
	public static int ACTION_EFFECTIVE = 1;
	public static int ACTION_UNLOCK = 2;

	public Endorsement getEndorsement() {
		return endorsement;
	}

	public void setEndorsement(Endorsement endorsement) {
		this.endorsement = endorsement;
	}

	public int getAction() {
		return action;
	}

	public void setAction(int action) {
		this.action = action;
	}

	public ProductOption getProductOption() {
		return productOption;
	}

	public void setProductOption(ProductOption productOption) {
		this.productOption = productOption;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public PlanPersonalRisk getPlanPersonalRisk() {
		return planPersonalRisk;
	}

	public void setPlanPersonalRisk(PlanPersonalRisk planPersonalRisk) {
		this.planPersonalRisk = planPersonalRisk;
	}

}
