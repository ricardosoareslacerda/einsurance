package br.com.tratomais.core.model.policy;

import br.com.tratomais.core.dao.PersistentEntity;

public class ItemSequence implements PersistentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -65672064534111910L;
	private ItemSequenceId id;
	private Contract contract;
	private int itemID;
	private int riskGroupID;

	public ItemSequence() {
	}

	public ItemSequence(ItemSequenceId id, int itemID, int riskGroupID) {
		this.setId(id);
		this.itemID = itemID;
		this.riskGroupID = riskGroupID;
	}
	public ItemSequence(Contract contract, int itemID, int riskGroupID, int sequenceCode) {
		this.contract = contract;
		this.itemID = itemID;
		this.riskGroupID = riskGroupID;
		this.id.setSequenceCode(sequenceCode);
	}

	public ItemSequence(int contractId, int itemID, int riskGroupID, int sequenceCode) {
		this.id.setContractID(contractId);
		this.itemID = itemID;
		this.riskGroupID = riskGroupID;
		this.id.setSequenceCode(sequenceCode);
	}
	
	public void setId(ItemSequenceId id) {
		this.id = id;
	}

	public ItemSequenceId getId() {
		return id;
	}
	
	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}
	
	public int getItemID() {
		return itemID;
	}
	
	public void setItemID(int itemID) {
		this.itemID = itemID;
	}
	
	public int getRiskGroupID() {
		return riskGroupID;
	}
	
	public void setRiskGroupID(int riskGroupID) {
		this.riskGroupID = riskGroupID;
	}
}
