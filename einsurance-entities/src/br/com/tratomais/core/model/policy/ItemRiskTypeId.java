package br.com.tratomais.core.model.policy;

import br.com.tratomais.core.dao.PersistentEntity;

/**
 * Identificador para class ItemRiskType
 * @author luiz.alberoni
 */
public class ItemRiskTypeId implements PersistentEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int contractId;
	private int endorsementId;
	private int itemId;
	private int riskType;

	public ItemRiskTypeId() {
	}
	
	public ItemRiskTypeId(int riskType, int itemId, int endorsementId, int contractId) {
		this.riskType = riskType;
		this.itemId = itemId;
		this.endorsementId = endorsementId;
		this.contractId = contractId;
	}
	
	/** 
	 * Retorna o identificador do contrato
	 * @return identificador do contrato
	 */
	public int getContractId() {
		return contractId;
	}
	/**
	 * Ajusta o identificador do contrato
	 * @param contractId Identificador do contrato
	 */
	public void setContractId(int contractId) {
		this.contractId = contractId;
	}
	
	/** 
	 * Retorna o c�digo do endorsement
	 * @return c�digo do endorsement
	 */
	public int getEndorsementId() {
		return endorsementId;
	}
	/**
	 * Ajusta o identificador do endorsement 
	 * @param endorsementId identificador do endorsement
	 */
	public void setEndorsementId(int endorsementId) {
		this.endorsementId = endorsementId;
	}
	
	/** 
	 * Retorna o identificador do item
	 * @return identificador do item
	 */
	public int getItemId() {
		return itemId;
	}
	/**
	 * Ajusta o identificador do item
	 * @param itemId identificador do item
	 */
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	
	/** 
	 * Retorna o tipo de risco
	 * @return tipo de risco
	 */
	public int getRiskType() {
		return riskType;
	}
	/**
	 * Ajusta o tipo de risco
	 * @param riskType tipo de risco
	 */
	public void setRiskType(int riskType) {
		this.riskType = riskType;
	}
	
	public boolean equals(Object other) {
		if ( ( this == other ) )
			return true;
		if ( ( other == null ) )
			return false;
		if ( !( other instanceof ItemRiskTypeId ) )
			return false;
		
		ItemRiskTypeId castOther = ( ItemRiskTypeId ) other;

		return ( this.getRiskType() == castOther.getRiskType() ) && 
			   ( this.getItemId() == castOther.getItemId() ) && 
			   ( this.getEndorsementId() == castOther.getEndorsementId() ) && 
			   ( this.getContractId() == castOther.getContractId() );
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getRiskType();
		result = 37 * result + this.getItemId();
		result = 37 * result + this.getEndorsementId();
		result = 37 * result + this.getContractId();
		return result;
	}
}
