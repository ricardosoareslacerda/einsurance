package br.com.tratomais.core.model.policy;

// Generated 28/12/2009 16:05:24 by Hibernate Tools 3.2.4.GA

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import br.com.tratomais.core.info.Calculus;

/**
 * ItemProperty generated by hbm2java
 */
public class ItemProperty extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7724854522539976156L;

	private int propertyType;
	private Integer activityType;
	private String addressStreet;
	private String districtName;
	private Integer cityId;
	private String cityName;
	private Integer regionCode;
	private String regionName;
	private Integer stateId;
	private String stateName;
	private String zipCode;
	private Integer countryId;
	private String countryName;
	private int propertyRiskType;
	/** Content Value, expressed in local currency (conversions to foreign done by getter and setter) */
	private Double contentValue;
	/** Structure Value, expressed in local currency (conversions to foreign done by getter and setter) */
	private Double structureValue;
	/** Property Value, expressed in local currency (conversions to foreign done by getter and setter) */
	private Double propertyValue;
	private Date acquisitionDate;
	private Date registrationDate;
	private String registrationName;
	private String registrationTome;
	private String registrationNumber;
	private String registrationProtocol;
	private String boundaryNorth;
	private String boundarySouth;
	private String boundaryEast;
	private String boundaryWest;
	private Boolean singleAddress;

	public ItemProperty() {
	}

	public ItemProperty(int propertyType, String addressStreet, String cityName, String stateName, int propertyRiskType,
			Double propertyValue) {
		this.propertyType = propertyType;
		this.addressStreet = addressStreet;
		this.cityName = cityName;
		this.stateName = stateName;
		this.propertyRiskType = propertyRiskType;
		this.propertyValue = propertyValue;
	}

	public ItemProperty(int propertyType, String addressStreet, String districtName, Integer cityId, String cityName, Integer regionCode, String regionName,
			Integer stateId, String stateName, String zipCode, Integer countryId, String countryName, int propertyRiskType, Double contentValue,
			Double structureValue, Double propertyValue, Date acquisitionDate, Date registrationDate, String registrationName,
			String registrationTome, String registrationNumber, String registrationProtocol, String boundaryNorth, String boundarySouth,
			String boundaryEast, String boundaryWest, Boolean singleAddress) {
		this.propertyType = propertyType;
		this.addressStreet = addressStreet;
		this.districtName = districtName;
		this.cityId = cityId;
		this.cityName = cityName;
		this.regionCode = regionCode;
		this.regionName = regionName;
		this.stateId = stateId;
		this.stateName = stateName;
		this.zipCode = zipCode;
		this.countryId = countryId;
		this.countryName = countryName;
		this.propertyRiskType = propertyRiskType;
		this.contentValue = contentValue;
		this.structureValue = structureValue;
		this.propertyValue = propertyValue;
		this.acquisitionDate = acquisitionDate;
		this.registrationDate = registrationDate;
		this.registrationName = registrationName;
		this.registrationTome = registrationTome;
		this.registrationNumber = registrationNumber;
		this.registrationProtocol = registrationProtocol;
		this.boundaryNorth = boundaryNorth;
		this.boundarySouth = boundarySouth;
		this.boundaryEast = boundaryEast;
		this.boundaryWest = boundaryWest;
		this.singleAddress = singleAddress;
	}

	public int getPropertyType() {
		return this.propertyType;
	}

	public void setPropertyType(int propertyType) {
		this.propertyType = propertyType;
	}

	/**
	 * Ajusta o tipo de Atividade
	 * @return tipo de Atividade
	 */
	public Integer getActivityType() {
		return activityType;
	}

	/**
	 * Ajusta o tipo de Atividade
	 * @param activityType tipo de Atividade
	 */
	public void setActivityType(Integer activityType) {
		this.activityType = activityType;
	}
	
	public String getAddressStreet() {
		return this.addressStreet;
	}

	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}

	public String getDistrictName() {
		return this.districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public Integer getCityId() {
		return this.cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Integer getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(Integer regionCode) {
		this.regionCode = regionCode;
	}

	public String getRegionName() {
		return this.regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public Integer getStateId() {
		return this.stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return this.stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public Integer getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public int getPropertyRiskType() {
		return this.propertyRiskType;
	}

	public void setPropertyRiskType(int propertyRiskType) {
		this.propertyRiskType = propertyRiskType;
	}

	/**
	 * Retrieve the Content Value
	 * @return Content Value, expressed in foreign currency
	 */
	public Double getContentValue() {
		return Calculus.toForeignCurrency(this.contentValue, conversionRate());
	}

	/**
	 * Sets the Content Value
	 * @param contentValue Content Value, expressed in foreign currency
	 */
	public void setContentValue(Double contentValue) {
		this.contentValue = Calculus.toLocalCurrency(contentValue, conversionRate());
	}

	/**
	 * Retrieves the Structure Value
	 * @return Structure Value, expressed in foreign currency
	 */
	public Double getStructureValue() {
		return Calculus.toForeignCurrency(this.structureValue, conversionRate());
	}

	/**
	 * Sets the Structure Value
	 * @param structureValue Structure Value, expressed in foreign currency
	 */
	public void setStructureValue(Double structureValue) {
		this.structureValue = Calculus.toLocalCurrency(structureValue, conversionRate());
	}

	/**
	 * Retrieves the Property Value
	 * @return Property Value, expressed in foreign currency
	 */
	public Double getPropertyValue() {
		return Calculus.toForeignCurrency(this.propertyValue, conversionRate());
	}

	/**
	 * Sets the Property Value
	 * @param propertyValue Property Value, expressed in foreign currency
	 */
	public void setPropertyValue(Double propertyValue) {
		this.propertyValue = Calculus.toLocalCurrency(propertyValue, conversionRate());
	}

	public Date getAcquisitionDate() {
		return this.acquisitionDate;
	}

	public void setAcquisitionDate(Date acquisitionDate) {
		this.acquisitionDate = acquisitionDate;
	}

	public Date getRegistrationDate() {
		return this.registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getRegistrationName() {
		return this.registrationName;
	}

	public void setRegistrationName(String registrationName) {
		this.registrationName = registrationName;
	}

	public String getRegistrationTome() {
		return this.registrationTome;
	}

	public void setRegistrationTome(String registrationTome) {
		this.registrationTome = registrationTome;
	}

	public String getRegistrationNumber() {
		return this.registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getRegistrationProtocol() {
		return this.registrationProtocol;
	}

	public void setRegistrationProtocol(String registrationProtocol) {
		this.registrationProtocol = registrationProtocol;
	}

	public String getBoundaryNorth() {
		return this.boundaryNorth;
	}

	public void setBoundaryNorth(String boundaryNorth) {
		this.boundaryNorth = boundaryNorth;
	}

	public String getBoundarySouth() {
		return this.boundarySouth;
	}

	public void setBoundarySouth(String boundarySouth) {
		this.boundarySouth = boundarySouth;
	}

	public String getBoundaryEast() {
		return this.boundaryEast;
	}

	public void setBoundaryEast(String boundaryEast) {
		this.boundaryEast = boundaryEast;
	}

	public String getBoundaryWest() {
		return this.boundaryWest;
	}

	public void setBoundaryWest(String boundaryWest) {
		this.boundaryWest = boundaryWest;
	}

	public Boolean getSingleAddress() {
		return this.singleAddress;
	}

	public void setSingleAddress(Boolean singleAddress) {
		this.singleAddress = singleAddress;
	}	
	
	@Override
	public Integer getAttributeValue(int attrNumber) {
		switch(attrNumber){
		case 17: // RiskPlan
		case 81:
			return this.propertyRiskType;
		case 18: // PropertyType
		case 83:
			return this.propertyType;
		case 84: //ActivityType
			return this.getActivityType();
		case 19: // StructureValue
			return this.structureValue.intValue();
		case 20: // ContentValue
			return this.contentValue.intValue();
		case 21: // AddressStreet
			return 0;//this.addressStreet;
		case 22: // CityID
		case 85:
			return this.cityId;
		case 23: // StateID
		case 86:
			return this.stateId;
		case 24: // ConverageID
			return this.getTCoverageId();
		case 25: // RangeValueID
		case 89:
			return this.getTRangeValueId();
		case 61: // PropertyType
			return this.getPropertyType();
		case 62: // RegionCode
		case 87:
			return this.getRegionCode();
		case 64: // ItemConverages
			return this.getItemCoverages().size();
		case 65: // RenewalInsurerID
			return this.getRenewalInsurerId();
		case 66: // RenewalType
			return this.getRenewalType();
		case 69: // CoverageID
		case 88:
			return this.getTCoverageId();
		case 71: // DeductibleID
			return this.getTDeductibleId();
		case 82:
			return this.getTRiskType();
		case 90: //TariffType
			return this.getTTariffType();
		case 91: //RangeFractionID
			return this.getTRangeFractionId();
		}
		
		throw new IllegalArgumentException("" + attrNumber + " - Atributo n�o encontrado");
	}

	/**
	 * M�todo que retorna uma lista de valores para o atributo informado
	 * @param attrNumber
	 * @return
	 */
	public List<Integer> getAttributeListValue(int attrNumber) {
		
		List<Integer> listValues = new ArrayList<Integer>();
		
		switch(attrNumber){
			case 67: // ItemProfile.QuestionID
			{
				Iterator<ItemProfile> iteratorProfiles = this.getItemProfiles().iterator();
				while(iteratorProfiles.hasNext()){
					listValues.add(iteratorProfiles.next().getId().getQuestionId());
				}
				return listValues;	
			}
			case 68: // ItemProfile.ResponseID
			{
				Iterator<ItemProfile> iteratorProfiles = this.getItemProfiles().iterator();
				while(iteratorProfiles.hasNext()){
					listValues.add(iteratorProfiles.next().getId().getResponseId());
				}
				return listValues;	
			}
			case 69: // ItemCoverage.CoverageID
			{
				Iterator<ItemCoverage> iteratorCoverages = this.getItemCoverages().iterator();
				while(iteratorCoverages.hasNext()){
					listValues.add(iteratorCoverages.next().getId().getCoverageId());
				}
				return listValues;	
			}
			case 71: // ItemCoverage.DeductibleId
			{
				Iterator<ItemCoverage> iteratorCoverages = this.getItemCoverages().iterator();
				while(iteratorCoverages.hasNext()){
					ItemCoverage itemCoverage = iteratorCoverages.next(); 
					if (itemCoverage.getDeductibleId() != null) {
						listValues.add(itemCoverage.getDeductibleId());
					}
				}
				return listValues;	
			}
			case 72: // Endorsement.Installment
			{
				Iterator<Installment> iteratorInstallment = this.getEndorsement().getInstallments().iterator();
				while(iteratorInstallment.hasNext()){
					Installment installment = iteratorInstallment.next();
					if (installment.getPaymentType() != 0) {
						listValues.add(installment.getPaymentType());
					}
				}
				return listValues;	
			}
			case 73: // Coverage.RangeID
			{
				Iterator<ItemCoverage> iteratorCoverages = this.getItemCoverages().iterator();
				while(iteratorCoverages.hasNext()){
					listValues.add(iteratorCoverages.next().getRangeValueId());
				}
				return listValues;
			}
			case 74: // Clause.ClauseID
			{
				Iterator<ItemClause> iteratorClauses = this.getItemClauses().iterator();
				while(iteratorClauses.hasNext()){
					listValues.add(iteratorClauses.next().getId().getClauseId());
				}
				return listValues;	
			}
		}
		
		throw new IllegalArgumentException("" + attrNumber + " - Atributo n�o encontrado");
	}
	
	@Override
	public Object clone(){
		return copyTo(new ItemProperty());
	}
	
	protected ItemProperty copyTo(ItemProperty item) {
		item.propertyType = this.propertyType;
		item.addressStreet = this.addressStreet;
		item.districtName = this.districtName;
		item.cityId = this.cityId;
		item.cityName = this.cityName;
		item.regionCode = this.regionCode;
		item.regionName = this.regionName;
		item.stateId = this.stateId;
		item.stateName = this.stateName;
		item.zipCode = this.zipCode;
		item.countryId = this.countryId;
		item.countryName = this.countryName;
		item.propertyRiskType = this.propertyRiskType;
		item.contentValue = this.contentValue;
		item.structureValue = this.structureValue;
		item.propertyValue = this.propertyValue;
		item.acquisitionDate = this.acquisitionDate;
		item.registrationDate = this.registrationDate;
		item.registrationName = this.registrationName;
		item.registrationTome = this.registrationTome;
		item.registrationNumber = this.registrationNumber;
		item.registrationProtocol = this.registrationProtocol;
		item.boundaryNorth = this.boundaryNorth;
		item.boundarySouth = this.boundarySouth;
		item.boundaryEast = this.boundaryEast;
		item.boundaryWest = this.boundaryWest;
		item.singleAddress = this.singleAddress;
		super.copyTo(item);
		return item;
	}

	/** {@inheritDoc} */
	@Override
	void refreshConversionRate(Double conversionRateOld, Double conversionRateNew) {
		
		super.refreshConversionRate(conversionRateOld, conversionRateNew);
		
		Double 	contentValueForeign = Calculus.toForeignCurrency(this.contentValue, conversionRateOld),
				structureValueForeign = Calculus.toForeignCurrency(this.structureValue, conversionRateOld),
				propertyValueForeign = Calculus.toForeignCurrency(this.propertyValue, conversionRateOld);
		
		this.contentValue = Calculus.toLocalCurrency(contentValueForeign, conversionRateNew);		
		this.structureValue = Calculus.toLocalCurrency(structureValueForeign, conversionRateNew);		
		this.propertyValue = Calculus.toLocalCurrency(propertyValueForeign, conversionRateNew);		
	}
}
