package br.com.tratomais.core.model.policy;

import java.io.Serializable;
import java.util.Date;

public class ValueAndRate implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5893871116895712992L;
	private double inputValue;
	private double rate; 
	private double fixedValue;
	private double minimumValue;
	private double finalValue;
	private int rateId;
	
	//variáveis para Endosso
	private int endorsedId;
	private int endorsedTermId;
	private Date endorsedEffectiveDate;
	private Date endorsedExpiryDate;
	private double oldPremium;
	private double paidPremium;
	private double paidFactor;
	private double unPaidFactor;
	private double refundFactor;
	private double unEarnedFactor;
	private double residualValue;
	private int daysRetained;
	private int daysEarned;
	private int daysUnearned;

	public ValueAndRate(int endorsedId, int endorsedTermId, Date endorsedEffectiveDate, Date endorsedExpiryDate, double oldPremium, double paidPremium, double paidFactor, double unPaidFactor, double refundFactor, double unEarnedFactor, double residualValue, int daysRetained, int daysEarned, int daysUnearned) {
		super();
		this.endorsedId = endorsedId;
		this.endorsedTermId = endorsedTermId; 
		this.endorsedEffectiveDate = endorsedEffectiveDate;
		this.endorsedExpiryDate = endorsedExpiryDate;
		this.oldPremium = oldPremium;
		this.paidPremium = paidPremium;
		this.paidFactor = paidFactor;
		this.unPaidFactor = unPaidFactor;
		this.refundFactor = refundFactor;
		this.unEarnedFactor = unEarnedFactor;
		this.residualValue = residualValue;
		this.daysRetained = daysRetained;
		this.daysEarned = daysEarned;
		this.daysUnearned = daysUnearned;
	}
	
	public ValueAndRate(double inputValue, double rate, double fixedValue, double finalValue) {
		super();
		this.inputValue = inputValue;
		this.rate = rate;
		this.fixedValue = fixedValue;
		this.finalValue = finalValue;
	}

	public ValueAndRate(double inputValue, double rate, double fixedValue, double minimumValue, double finalValue) {
		super();
		this.inputValue = inputValue;
		this.rate = rate;
		this.fixedValue = fixedValue;
		this.minimumValue = minimumValue;
		this.finalValue = finalValue;
	}	
	
	public ValueAndRate(double inputValue, double rate, double fixedValue, double finalValue, int rateId) {
		super();
		this.inputValue = inputValue;
		this.rate = rate;
		this.fixedValue = fixedValue;
		this.finalValue = finalValue;
		this.rateId = rateId;
	}
	
	public double getInputValue() {
		return inputValue;
	}
	
	public double getRate() {
		return rate;
	}

	public double getFixedValue() {
		return fixedValue;
	}
	
	public double getMinimumValue() {
		return minimumValue;
	}
	
	public double getFinalValue() {
		return finalValue;
	}

	public int getRateId() {
		return rateId;
	}
	
	public double getPaidFactor() {
		return paidFactor;
	}
	
	public double getUnPaidFactor() {
		return unPaidFactor;
	}
	
	public double getRefundFactor() {
		return refundFactor;
	}
	
	public double getUnEarnedFactor() {
		return unEarnedFactor;
	}
	
	public double getOldPremium() {
		return oldPremium;
	}
	
	public double getPaidPremium() {
		return paidPremium;
	}
	
	public int getEndorsedId() {
		return endorsedId;
	}
	
	public int getEndorsedTermId() {
		return endorsedTermId;
	}
	
	public Date getEndorsedEffectiveDate() {
		return endorsedEffectiveDate;
	}

	public Date getEndorsedExpiryDate() {
		return endorsedExpiryDate;
	}
	
	public double getResidualValue() {
		return residualValue;
	}
	
	public void setResidualValue(double residualValue) {
		this.residualValue = residualValue;
	}
	
	public int getDaysRetained() {
		return daysRetained;
	}
	
	public int getDaysEarned() {
		return daysEarned;
	}
	
	public int getDaysUnearned() {
		return daysUnearned;
	}
}
