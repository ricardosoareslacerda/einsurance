package br.com.tratomais.core.model.policy;

import br.com.tratomais.core.dao.PersistentEntity;
import br.com.tratomais.core.info.Calculus;

/**
 * Representa os tipos de risco
 * @author luiz.alberoni
 */
public class ItemRiskType implements PersistentEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ItemRiskTypeId id;
	private Double riskTypeValue;
 	private String riskTypeName;
 	private Item item;
	
	/** 
	 * Retorna o identificador de registro
	 * @return identificador de registro
	 */
	public ItemRiskTypeId getId() {
		return id;
	}
	/**
	 * Ajusta o identificador de registro
	 * @param id identificador de registro
	 */
	public void setId(ItemRiskTypeId id) {
		this.id = id;
	}
	
	/**
	 * Retorna o tipo de risco
	 * @return tipo de risco
	 */
	public Double getRiskTypeValue() {
		return Calculus.toForeignCurrency(this.riskTypeValue, this.conversionRate());
	}
	/**
	 * Ajusta o tipo de risco 
	 * @param riskTypeValue Novo tipo de risco
	 */
	public void setRiskTypeValue(Double riskTypeValue) {
		this.riskTypeValue = Calculus.toLocalCurrency(riskTypeValue, this.conversionRate());
	}
	
	/** 
	 * Retorna o nome do tipo de risco
	 * @return nome do tipo de risco
	 */
 	public String getRiskTypeName() {
		return riskTypeName;
	}
 	/**
 	 * Ajusta o nome do tipo de risco 
 	 * @param riskTypeName Nome do tipo de risco
 	 */
 	public void setRiskTypeName(String riskTypeName) {
		this.riskTypeName = riskTypeName;
	}
 	
 	/**
 	 * Retorna o item ao qual est� ligado
 	 * @return item ao qual est� ligado
 	 */
 	public Item getItem() {
		return item;
	}
 	/**
 	 * Ajusta o item ao qual o ItemRiskType est� ligado
 	 * @param item Item ao qual o ItemRiskType est� ligado
 	 */
 	public void setItem(Item item) {
		if ( this.item == null) {
			this.refreshConversionRate(1d, item.conversionRate());
		} else {
			this.refreshConversionRate(this.item.conversionRate(), item.conversionRate());
		}
		this.item = item;
	}
 	
	public void updateKey(Item parent){
		if ( parent.getId() == null ) {
			throw new IllegalArgumentException();
		}
		this.setItem( parent );
		if ( this.getId() == null ) {
			this.setId( new ItemRiskTypeId(0, parent.getId().getItemId(), parent.getId().getEndorsementId(), parent.getId().getContractId()) );
		}else{
			this.getId().setItemId( parent.getId().getItemId());
			this.getId().setEndorsementId( parent.getId().getEndorsementId() );
			this.getId().setContractId( parent.getId().getContractId() );
		}
	}

	/**
	 * A kind of callback, used by endorsement to notify changes in the conversion rate value.
	 * @param oldValue Value used in a previous conversion
	 * @param newValue New value of the conversion rate
	 */
	final void refreshConversionRate(double conversionRateOld, double conversionRateNew) {
		Double 	riskTypeValueForeign = Calculus.toForeignCurrency(this.riskTypeValue, conversionRateOld);
		
		this.riskTypeValue = Calculus.toLocalCurrency(riskTypeValueForeign, conversionRateNew);
	}

	/**
	 * Retrives the conversion rate used by the endorsement, 1.0 if there is no endorsement avaliable
	 * @return
	 */
	protected Double conversionRate() {
		return this.item == null ? 1d : this.item.conversionRate();
	}
}
