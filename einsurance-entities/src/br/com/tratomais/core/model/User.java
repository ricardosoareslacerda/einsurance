package br.com.tratomais.core.model;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Transient;

import org.springframework.security.GrantedAuthority;
import org.springframework.security.GrantedAuthorityImpl;
import org.springframework.security.userdetails.UserDetails;

import br.com.tratomais.core.dao.PersistentEntity;
import br.com.tratomais.core.model.customer.Channel;


public class User implements PersistentEntity, UserDetails {
	/**
	 * Gerado pelo Eclipse
	 */
	private static final long serialVersionUID = -3697020028227320480L;
	
	/**
	 * Authorization Type
	 */
	public static final int AUTHORIZATION_TYPE_INSURER = 181;		// Matriz
	public static final int AUTHORIZATION_TYPE_SUBSIDIARY = 182; 	// Sucursal
	public static final int AUTHORIZATION_TYPE_BROKER = 183; 		// Corretor
	
	private int userId;
	private Domain domain;
	private String login;
	private String name;
	private String emailAddress;
	private String phoneNumber;
	private String employeeCode;
	private int hierarchyType;
	private int authorizationType;
	private boolean administrator;
	private boolean active;
	private Set<UserPartner> userPartners = new HashSet<UserPartner>(0);
	private Set< Channel > channels = new HashSet< Channel >( 0 );
	private Set<Role> roles = new HashSet<Role>(0);
//	private Set<Endorse> endorses = new HashSet<Endorse>(0);
	
	private String password;
	private Timestamp timeValidation;
	private int timeLimit = 15;

	private int transientSelectedPartner;
	private Boolean transientSelected;

	
	public User() {
	}

	public User(int userId, Domain domain, String login, String name, boolean administrator, boolean active) {
		this.userId = userId;
		this.domain = domain;
		this.login = login;
		this.name = name;
		this.administrator = administrator;
		this.active = active;
	}

	public User(int userId, Domain domain, String login, String name, String emailAddress, String phoneNumber, String employeeCode,
			boolean administrator, boolean active, Set< UserPartner > userPartners, Set< Channel > channels, Set< Role > roles) {
		this.userId = userId;
		this.domain = domain;
		this.login = login;
		this.name = name;
		this.emailAddress = emailAddress;
		this.phoneNumber = phoneNumber;
		this.employeeCode = employeeCode;
		this.administrator = administrator;
		this.active = active;
		this.userPartners = userPartners;
		this.channels = channels;
		this.roles = roles;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Domain getDomain() {
		return this.domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmployeeCode() {
		return this.employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public int getHierarchyType() {
		return this.hierarchyType;
	}

	public void setHierarchyType(int hierarchyType) {
		this.hierarchyType = hierarchyType;
	}

	public int getAuthorizationType() {
		return this.authorizationType;
	}

	public void setAuthorizationType(int authorizationType) {
		this.authorizationType = authorizationType;
	}
	
	public boolean isAdministrator() {
		return this.administrator;
	}

	public void setAdministrator(boolean administrator) {
		this.administrator = administrator;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Set<UserPartner> getUserPartners() {
		return this.userPartners;
	}

	public void setUserPartners(Set<UserPartner> userPartners) {
		this.userPartners = userPartners;
	}

	public Set< Channel > getChannels() {
		return this.channels;
	}

	public void setChannels(Set< Channel > channels) {
		this.channels = channels;
	}

	public Set<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
//	public Set<Endorse> getEndorses() {
//		return this.endorses;
//	}
//
//	public void setEndorses(Set<Endorse> endorses) {
//		this.endorses = endorses;
//	}

	/**
	 * Security
	 **/
	@Transient
	public GrantedAuthority[] getAuthorities() {
		Set<GrantedAuthorityImpl> list;
		try {
			list = new HashSet<GrantedAuthorityImpl>();
			for (Role role : this.getRoles()) {
				list.add(new GrantedAuthorityImpl(role.getAuthority()));
			}
			return (GrantedAuthority[]) list.toArray(new GrantedAuthority[0]);
		
		} catch ( Exception e ) {
			return null;
		}
	}

	@Transient
	public void setAuthorities(GrantedAuthority[] combinedAuthorities) {
 	}

	@Transient
	public String getPassword() {
		return this.password;
	}

	@Transient
	public void setPassword(String password) {
		this.password = password;
	}

	@Transient
	public String getUsername() {
		return this.getLogin();
	}

	@Transient
	public boolean isAccountNonExpired() {
		return true;
	}

	@Transient
	public boolean isAccountNonLocked() {
		return true;
	}

	@Transient
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Transient
	public boolean isEnabled() {
		return this.isActive();
	}

	public Timestamp getTimeValidation() {
		return timeValidation;
	}

	public void setTimeValidation(Timestamp timeValidation) {
		this.timeValidation = timeValidation;
	}

	public int getTimeLimit() {
		return timeLimit;
	}

	public void setTimeLimit(int timeLimit) {
		this.timeLimit = timeLimit;
	}

	public void setTransientSelectedPartner(int partnerId) {
		this.transientSelectedPartner = partnerId;		
	}

	public int getTransientSelectedPartner() {
		return this.transientSelectedPartner;		
	}

	public Boolean getTransientSelected() {
		return transientSelected;
	}

	public void setTransientSelected(Boolean transientSelected) {
		this.transientSelected = transientSelected;
	}
}
