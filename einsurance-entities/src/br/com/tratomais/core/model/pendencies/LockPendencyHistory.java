package br.com.tratomais.core.model.pendencies;

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

public class LockPendencyHistory implements PersistentEntity{

	public static final int HISTORY_TYPE_STATUS_CHANGE = 589;
	/**
	 * 
	 */
	private static final long serialVersionUID = -7963833776419811807L;
	private Integer id;
	private Integer historyType;
	private Date issueDate;
	private Integer contractID;
	private Integer endorsementID;
	private Integer lockID;	
	private int lockPendencyStatus; 
	private Integer userID;
	private String description;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the historyType
	 */
	public Integer getHistoryType() {
		return historyType;
	}
	/**
	 * @param historyType the historyType to set
	 */
	public void setHistoryType(Integer historyType) {
		this.historyType = historyType;
	}
	/**
	 * @return the issueDate
	 */
	public Date getIssueDate() {
		return issueDate;
	}
	/**
	 * @param issueDate the issueDate to set
	 */
	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}
	/**
	 * @return the contractID
	 */
	public Integer getContractID() {
		return contractID;
	}
	/**
	 * @param contractID the contractID to set
	 */
	public void setContractID(Integer contractID) {
		this.contractID = contractID;
	}
	/**
	 * @return the endorsementID
	 */
	public Integer getEndorsementID() {
		return endorsementID;
	}
	/**
	 * @param endorsementID the endorsementID to set
	 */
	public void setEndorsementID(Integer endorsementID) {
		this.endorsementID = endorsementID;
	}
	/**
	 * @return the lockID
	 */
	public Integer getLockID() {
		return lockID;
	}
	/**
	 * @param lockID the lockID to set
	 */
	public void setLockID(Integer lockID) {
		this.lockID = lockID;
	}
	/**
	 * @return the lockPendencyStatus
	 */
	public int getLockPendencyStatus() {
		return lockPendencyStatus;
	}
	/**
	 * @param lockPendencyStatus the lockPendencyStatus to set
	 */
	public void setLockPendencyStatus(int lockPendencyStatus) {
		this.lockPendencyStatus = lockPendencyStatus;
	}
	/**
	 * @return the userID
	 */
	public Integer getUserID() {
		return userID;
	}
	/**
	 * @param userID the userID to set
	 */
	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}	
}
