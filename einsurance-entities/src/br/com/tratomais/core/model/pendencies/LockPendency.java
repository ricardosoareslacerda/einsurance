package br.com.tratomais.core.model.pendencies;

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

public class LockPendency implements PersistentEntity{
	public static final int LOCK_PENDENCY_STATUS_PENDENT 	= 574;
	public static final int LOCK_PENDENCY_STATUS_APROVED 	= 575;
	public static final int LOCK_PENDENCY_STATUS_REJECTED 	= 576;
	public static final int LOCK_PENENCY_STATUS_IN_ANALISIS = 577;
	public static final int LOCK_PENDENCY_STATUS_PREVENTED 	= 578;
	public static final int LOCK_PENDENCY_STATUS_BLOCKED 	= 579;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4587087015907576223L;
	private LockPendencyId id;
	private Integer itemID;
	private Integer pendencyID;
	private Date issueDate;
	private Date unlockDate;
	private Integer userID;
	private int lockPendencyStatus; 
	/**
	 * @return the id
	 */
	public LockPendencyId getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(LockPendencyId id) {
		this.id = id;
	}
	/**
	 * @return the itemID
	 */
	public Integer getItemID() {
		return itemID;
	}
	/**
	 * @param itemID the itemID to set
	 */
	public void setItemID(Integer itemID) {
		this.itemID = itemID;
	}
	/**
	 * @return the pendencyID
	 */
	public Integer getPendencyID() {
		return pendencyID;
	}
	/**
	 * @param pendencyID the pendencyID to set
	 */
	public void setPendencyID(Integer pendencyID) {
		this.pendencyID = pendencyID;
	}
	/**
	 * @return the issueDate
	 */
	public Date getIssueDate() {
		return issueDate;
	}
	/**
	 * @param issueDate the issueDate to set
	 */
	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}
	/**
	 * @return the unlockDate
	 */
	public Date getUnlockDate() {
		return unlockDate;
	}
	/**
	 * @param unlockDate the unlockDate to set
	 */
	public void setUnlockDate(Date unlockDate) {
		this.unlockDate = unlockDate;
	}
	/**
	 * @return the userID
	 */
	public Integer getUserID() {
		return userID;
	}
	/**
	 * @param userID the userID to set
	 */
	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	/**
	 * @return the lockPendencyStatus
	 */
	public int getLockPendencyStatus() {
		return lockPendencyStatus;
	}
	/**
	 * @param lockPendencyStatus the lockPendencyStatus to set
	 */
	public void setLockPendencyStatus(int lockPendencyStatus) {
		this.lockPendencyStatus = lockPendencyStatus;
	}
	
	public boolean same(LockPendency lockPendency){
		return 
			( ( ( id.getContractID() == null ) && ( lockPendency.getId().getContractID() == null )) ||
			  ( ( id.getContractID() != null ) && (id.getContractID().equals(lockPendency.getId().getContractID())))) && 
			( ( ( id.getEndorsementID() == null ) && ( lockPendency.getId().getEndorsementID() == null )) ||
			  ( ( id.getEndorsementID() != null ) && (id.getEndorsementID().equals(lockPendency.getId().getEndorsementID())))) && 
			( ( ( itemID == null ) && ( lockPendency.getItemID() == null )) ||
			  ( ( itemID != null ) && ( itemID.equals(lockPendency.getItemID()))))&& 
			( ( ( pendencyID == null ) && ( lockPendency.getPendencyID() == null )) ||
			  ( ( pendencyID != null ) && ( pendencyID.equals(lockPendency.getPendencyID())))); 
	}
}
