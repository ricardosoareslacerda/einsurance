package br.com.tratomais.core.model.pendencies;

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

/**
 * Represents Pendency record
 */
public class Pendency implements PersistentEntity{
	/**
	 * Eclipse Generated 
	 */
	private static final long serialVersionUID = -6826982636304141609L;
	
	public final static int PENDENCY_LEVEL_ENDORSEMENT = 730;
	public final static int PENDENCY_LEVEL_ITEM = 731;
	public final static int PENDENCY_LEVEL_AUTOMATIC = 732;
	
	/**
	 * Pendency record Identificator
	 */
	private int id;
	/**
	 * Pendency code
	 */
	private String pendencyCode;
	/**
	 * Pendency type
	 */
	private int pendencyType;
	/**
	 * P
	 */
	private String description;
	private Integer severityType;
	private Integer priorityType;
	private Integer pendencyLevel;
	private Integer objectID;
	private Integer processOwnerID;
	private Integer displayOrder;
	private Date registred;
	private Date updated;
	private boolean active;
	/**
	 * @return the pendencyID
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param pendencyID the pendencyID to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the pendencyCode
	 */
	public String getPendencyCode() {
		return pendencyCode;
	}
	/**
	 * @param pendencyCode the pendencyCode to set
	 */
	public void setPendencyCode(String pendencyCode) {
		this.pendencyCode = pendencyCode;
	}
	/**
	 * @return the pendencyType
	 */
	public int getPendencyType() {
		return pendencyType;
	}
	/**
	 * @param pendencyType the pendencyType to set
	 */
	public void setPendencyType(int pendencyType) {
		this.pendencyType = pendencyType;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the severityType
	 */
	public Integer getSeverityType() {
		return severityType;
	}
	/**
	 * @param severityType the severityType to set
	 */
	public void setSeverityType(Integer severityType) {
		this.severityType = severityType;
	}
	/**
	 * @return the priorityType
	 */
	public Integer getPriorityType() {
		return priorityType;
	}
	/**
	 * @param priorityType the priorityType to set
	 */
	public void setPriorityType(Integer priorityType) {
		this.priorityType = priorityType;
	}
	/**
	 * @return the pendencyLevel
	 */
	public Integer getPendencyLevel() {
		return pendencyLevel;
	}
	/**
	 * @param pendencyLevel the pendencyLevel to set
	 */
	public void setPendencyLevel(Integer pendencyLevel) {
		this.pendencyLevel = pendencyLevel;
	}
	/**
	 * @return the objectID
	 */
	public Integer getObjectID() {
		return objectID;
	}
	/**
	 * @param objectID the objectID to set
	 */
	public void setObjectID(Integer objectID) {
		this.objectID = objectID;
	}
	/**
	 * @return the processOwnerID
	 */
	public Integer getProcessOwnerID() {
		return processOwnerID;
	}
	/**
	 * @param processOwnerID the processOwnerID to set
	 */
	public void setProcessOwnerID(Integer processOwnerID) {
		this.processOwnerID = processOwnerID;
	}
	/**
	 * @return the displayOrder
	 */
	public Integer getDisplayOrder() {
		return displayOrder;
	}
	/**
	 * @param displayOrder the displayOrder to set
	 */
	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}
	/**
	 * @return the registred
	 */
	public Date getRegistred() {
		return registred;
	}
	/**
	 * @param registred the registred to set
	 */
	public void setRegistred(Date registred) {
		this.registred = registred;
	}
	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}
	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
