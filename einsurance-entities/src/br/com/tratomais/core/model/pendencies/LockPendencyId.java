package br.com.tratomais.core.model.pendencies;

import br.com.tratomais.core.dao.PersistentEntityId;

public class LockPendencyId extends PersistentEntityId {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4708493645507623977L;
	private Integer lockID;
	private Integer endorsementID;
	private Integer contractID;
	/**
	 * @return the lockID
	 */
	public Integer getLockID() {
		return lockID;
	}
	/**
	 * @param lockID the lockID to set
	 */
	public void setLockID(Integer lockID) {
		this.lockID = lockID;
	}
	/**
	 * @return the endorsementID
	 */
	public Integer getEndorsementID() {
		return endorsementID;
	}
	/**
	 * @param endorsementID the endorsementID to set
	 */
	public void setEndorsementID(Integer endorsementID) {
		this.endorsementID = endorsementID;
	}
	/**
	 * @return the contractID
	 */
	public Integer getContractID() {
		return contractID;
	}
	/**
	 * @param contractID the contractID to set
	 */
	public void setContractID(Integer contractID) {
		this.contractID = contractID;
	}	
}
