package br.com.tratomais.core.model;

import br.com.tratomais.core.dao.PersistentEntity;

public class ApplicationProperty implements PersistentEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String BILLING_METHOD_ID_FOR_TREASURY = "BillingMethodIdForTreasury";

	public static final String DAYS_TO_BILLET_LOOSE = "DaysToBilletLoose";
	public static final String DAYS_TOLERANCE_INSTALLMENT = "DaysToleranceInstallment";
	
	public static final String PAYMENT_TERM_ID_FOR_TREASURY = "PaymentTermIdForTreasury";

	private ApplicationPropertyID id;
	private String value;
	
	public ApplicationPropertyID getId() {
		return id;
	}
	public void setId(ApplicationPropertyID id) {
		this.id = id;
	}
	
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
