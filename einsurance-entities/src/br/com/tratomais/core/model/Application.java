package br.com.tratomais.core.model;

import java.util.HashSet;
import java.util.Set;

import br.com.tratomais.core.dao.PersistentEntity;

public class Application implements PersistentEntity {
	private static final long serialVersionUID = 8171782431711217942L;

	public static final int DEFAULT = 1;
	
	private int applicationId;
	private String name;
	private boolean active;
	private Set< Module > modules = new HashSet< Module >( 0 );

	public Application() {
	}

	public Application(int applicationId, String name, boolean active) {
		this.applicationId = applicationId;
		this.name = name;
		this.active = active;
	}

	public Application(int applicationId, String name, boolean active, Set< Module > modules) {
		this.applicationId = applicationId;
		this.name = name;
		this.active = active;
		this.modules = modules;
	}

	public int getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Set<Module> getModules() {
		return this.modules;
	}

	public void setModules(Set<Module> modules) {
		this.modules = modules;
	}

}
