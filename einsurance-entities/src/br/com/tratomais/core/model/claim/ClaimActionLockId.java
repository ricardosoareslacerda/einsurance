package br.com.tratomais.core.model.claim;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import br.com.tratomais.core.dao.PersistentEntityId;

@Embeddable
public class ClaimActionLockId extends PersistentEntityId{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name="ClaimEffectID", columnDefinition="int", nullable=false)
	private Integer claimEffectId;
	@Column(name="CoverageID", columnDefinition="int", nullable=false)
	private Integer coverageId;
	@Column(name="InsuredMain", columnDefinition="bit", nullable=false)
	private Boolean insuredMain;
	@Column(name="ClaimStatus", columnDefinition="int", nullable=false)
	private Integer claimStatus;
	@Column(name="IssuanceType", columnDefinition="int", nullable=false)
	private Integer issuanceType;
	@Column(name="EffectiveDate", columnDefinition="datetime", nullable=false)
	private Date effectiveDate;
	/**
	 * @return the claimEffectId
	 */
	public Integer getClaimEffectId() {
		return claimEffectId;
	}
	/**
	 * @param claimEffectId the claimEffectId to set
	 */
	public void setClaimEffectId(Integer claimEffectId) {
		this.claimEffectId = claimEffectId;
	}
	/**
	 * @return the coverageId
	 */
	public Integer getCoverageId() {
		return coverageId;
	}
	/**
	 * @param coverageId the coverageId to set
	 */
	public void setCoverageId(Integer coverageId) {
		this.coverageId = coverageId;
	}
	/**
	 * @return the insuredMain
	 */
	public Boolean getInsuredMain() {
		return insuredMain;
	}
	/**
	 * @param insuredMain the insuredMain to set
	 */
	public void setInsuredMain(Boolean insuredMain) {
		this.insuredMain = insuredMain;
	}
	/**
	 * @return the claimStatus
	 */
	public Integer getClaimStatus() {
		return claimStatus;
	}
	/**
	 * @param claimStatus the claimStatus to set
	 */
	public void setClaimStatus(Integer claimStatus) {
		this.claimStatus = claimStatus;
	}
	/**
	 * @return the issuanceType
	 */
	public Integer getIssuanceType() {
		return issuanceType;
	}
	/**
	 * @param issuanceType the issuanceType to set
	 */
	public void setIssuanceType(Integer issuanceType) {
		this.issuanceType = issuanceType;
	}
	/**
	 * @return the effectiveDate
	 */
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
}
