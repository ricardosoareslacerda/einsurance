package br.com.tratomais.core.model.claim;

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

public class ClaimAction implements PersistentEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public final static int ACTION_TOTAL_LOSS = 610;
	public final static int ACTION_PARTIAL_LOSS = 611;
	public final static int ACTION_AFFECTED_ITEM = 612;
	public final static int ACTION_FALSE_DECLARATION = 613;
	
	private ClaimActionId id;
	private int claimActionType;
	private Date expiryDate;
	private int objectID;
	private String description;
	private Date registred;
	private Date updated;

	/**
	 * @return the id
	 */
	public ClaimActionId getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(ClaimActionId id) {
		this.id = id;
	}
	/**
	 * @return the claimActionType
	 */
	public int getClaimActionType() {
		return claimActionType;
	}
	/**
	 * @param claimActionType the claimActionType to set
	 */
	public void setClaimActionType(int claimActionType) {
		this.claimActionType = claimActionType;
	}
	/**
	 * @return the expiryDate
	 */
	public Date getExpiryDate() {
		return expiryDate;
	}
	/**
	 * @param expiryDate the expiryDate to set
	 */
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	/**
	 * @return the objectID
	 */
	public int getObjectID() {
		return objectID;
	}
	/**
	 * @param objectID the objectID to set
	 */
	public void setObjectID(int objectID) {
		this.objectID = objectID;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the registred
	 */
	public Date getRegistred() {
		return registred;
	}
	/**
	 * @param registred the registred to set
	 */
	public void setRegistred(Date registred) {
		this.registred = registred;
	}
	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}
	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}

}
