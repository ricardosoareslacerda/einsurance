package br.com.tratomais.core.model.claim;

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

public class ClaimEffect implements PersistentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int claimEffectID;
	private String claimEffectCode;
	private String description;
	private int objectID;
	private int displayOrder;
	private Date registred;
	private Date updated;
	/**
	 * @return the claimEffectID
	 */
	public int getClaimEffectID() {
		return claimEffectID;
	}
	/**
	 * @param claimEffectID the claimEffectID to set
	 */
	public void setClaimEffectID(int claimEffectID) {
		this.claimEffectID = claimEffectID;
	}
	/**
	 * @return the claimEffectCode
	 */
	public String getClaimEffectCode() {
		return claimEffectCode;
	}
	/**
	 * @param claimEffectCode the claimEffectCode to set
	 */
	public void setClaimEffectCode(String claimEffectCode) {
		this.claimEffectCode = claimEffectCode;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the objectID
	 */
	public int getObjectID() {
		return objectID;
	}
	/**
	 * @param objectID the objectID to set
	 */
	public void setObjectID(int objectID) {
		this.objectID = objectID;
	}
	/**
	 * @return the displayOrder
	 */
	public int getDisplayOrder() {
		return displayOrder;
	}
	/**
	 * @param displayOrder the displayOrder to set
	 */
	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}
	/**
	 * @return the registred
	 */
	public Date getRegistred() {
		return registred;
	}
	/**
	 * @param registred the registred to set
	 */
	public void setRegistred(Date registred) {
		this.registred = registred;
	}
	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}
	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
