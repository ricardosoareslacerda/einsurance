package br.com.tratomais.core.model.claim;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import br.com.tratomais.core.dao.PersistentEntity;

@Entity
public class ClaimActionLock implements PersistentEntity {
	public static final Integer LOCK_TYPE_TOTAL_BLOCK = 622;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private ClaimActionLockId clainActionLockId;
	@Column(name="ClaimActionLockType", columnDefinition="int", nullable=false)
	private Integer claimActionLockType;
	@Column(name="ExpiryDate", columnDefinition="datetime", nullable=false)
	private Date expiryDate;
	@Column(name="ObjectId", columnDefinition="int", nullable=false)
	private Integer objectId;
	@Column(name="Description", columnDefinition="varchar", length=100)
	private String description;
	@Column(name="Registred", columnDefinition="datetime", nullable=false)
	private Date registred;
	@Column(name="Updated", columnDefinition="datetime")
	private Date updated;
	/**
	 * @return the clainActionLockId
	 */
	public ClaimActionLockId getClainActionLockId() {
		return clainActionLockId;
	}
	/**
	 * @param clainActionLockId the clainActionLockId to set
	 */
	public void setClainActionLockId(ClaimActionLockId clainActionLockId) {
		this.clainActionLockId = clainActionLockId;
	}
	/**
	 * @return the claimActionLockType
	 */
	public Integer getClaimActionLockType() {
		return claimActionLockType;
	}
	/**
	 * @param claimActionLockType the claimActionLockType to set
	 */
	public void setClaimActionLockType(Integer claimActionLockType) {
		this.claimActionLockType = claimActionLockType;
	}
	/**
	 * @return the expiryDate
	 */
	public Date getExpiryDate() {
		return expiryDate;
	}
	/**
	 * @param expiryDate the expiryDate to set
	 */
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	/**
	 * @return the objectId
	 */
	public Integer getObjectId() {
		return objectId;
	}
	/**
	 * @param objectId the objectId to set
	 */
	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the registred
	 */
	public Date getRegistred() {
		return registred;
	}
	/**
	 * @param registred the registred to set
	 */
	public void setRegistred(Date registred) {
		this.registred = registred;
	}
	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}
	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
}
