package br.com.tratomais.core.model.claim;

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntityId;

public class ClaimActionId extends PersistentEntityId {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	
	private Integer claimEffectID;
	private Integer coverageID;
	private Boolean insuredMain;
	private Integer claimStatus;
	private Date effectiveDate;
	/**
	 * @return the claimEffectID
	 */
	public Integer getClaimEffectID() {
		return claimEffectID;
	}
	/**
	 * @param claimEffectID the claimEffectID to set
	 */
	public void setClaimEffectID(Integer claimEffectID) {
		this.claimEffectID = claimEffectID;
	}
	/**
	 * @return the coverageID
	 */
	public Integer getCoverageID() {
		return coverageID;
	}
	/**
	 * @param coverageID the coverageID to set
	 */
	public void setCoverageID(Integer coverageID) {
		this.coverageID = coverageID;
	}
	/**
	 * @return the insuredMain
	 */
	public Boolean getInsuredMain() {
		return insuredMain;
	}
	/**
	 * @param insuredMain the insuredMain to set
	 */
	public void setInsuredMain(Boolean insuredMain) {
		this.insuredMain = insuredMain;
	}
	/**
	 * @return the claimStatus
	 */
	public Integer getClaimStatus() {
		return claimStatus;
	}
	/**
	 * @param claimStatus the claimStatus to set
	 */
	public void setClaimStatus(Integer claimStatus) {
		this.claimStatus = claimStatus;
	}
	/**
	 * @return the effectiveDate
	 */
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
}
