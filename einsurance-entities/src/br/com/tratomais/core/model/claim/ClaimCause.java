package br.com.tratomais.core.model.claim;

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

public class ClaimCause implements PersistentEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int claimCauseID;
	private String claimCauseCode;
	private String description;
	private int objectID;
	private int displayOrder;
	private Date registred;
	private Date updated;
	/**
	 * @return the claimCauseID
	 */
	public int getClaimCauseID() {
		return claimCauseID;
	}
	/**
	 * @param claimCauseID the claimCauseID to set
	 */
	public void setClaimCauseID(int claimCauseID) {
		this.claimCauseID = claimCauseID;
	}
	/**
	 * @return the claimCauseCode
	 */
	public String getClaimCauseCode() {
		return claimCauseCode;
	}
	/**
	 * @param claimCauseCode the claimCauseCode to set
	 */
	public void setClaimCauseCode(String claimCauseCode) {
		this.claimCauseCode = claimCauseCode;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the objectID
	 */
	public int getObjectID() {
		return objectID;
	}
	/**
	 * @param objectID the objectID to set
	 */
	public void setObjectID(int objectID) {
		this.objectID = objectID;
	}
	/**
	 * @return the displayOrder
	 */
	public int getDisplayOrder() {
		return displayOrder;
	}
	/**
	 * @param displayOrder the displayOrder to set
	 */
	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}
	/**
	 * @return the registred
	 */
	public Date getRegistred() {
		return registred;
	}
	/**
	 * @param registred the registred to set
	 */
	public void setRegistred(Date registred) {
		this.registred = registred;
	}
	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}
	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
}
