package br.com.tratomais.core.model.product;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.tratomais.core.dao.PersistentEntity;

@Entity
@Table(name="Rule")
public class Rule implements PersistentEntity {
	
	private static final long serialVersionUID = 2831912939458917970L;

	@Id
	@Column(name="RuleID", columnDefinition="Integer", nullable=false)
	private Integer ruleId;

	@Column(name="RuleType", columnDefinition="Integer", nullable=true)
	private Integer ruleType;

	@Column(name="Description", columnDefinition="String", nullable=false)
	private String description;
	
	@Column(name="ObjectID", columnDefinition="Integer", nullable=false)
	private Integer ObjectId;
	
	@Column(name="AttributeID_1", columnDefinition="Integer", nullable=true)
	private Integer attributeId_1;

	@Column(name="AttributeID_2", columnDefinition="Integer", nullable=true)
	private Integer attributeId_2;
	
	@Column(name="AttributeID_3", columnDefinition="Integer", nullable=true)
	private Integer attributeId_3;
	
	@Column(name="AttributeID_4", columnDefinition="Integer", nullable=true)
	private Integer attributeId_4;
	
	@Column(name="AttributeID_5", columnDefinition="Integer", nullable=true)
	private Integer attributeId_5;
	
	@Column(name="DisplayOrder", columnDefinition="Integer", nullable=true)
	private Integer displayOrder;

	@Column(name="Registred", columnDefinition="datetime", nullable=true)
	private Date registred;
	
	@Column(name="Updated", columnDefinition="datetime", nullable=true)
	private Date updated;

	/**
	 * @return the ruleId
	 */
	public Integer getRuleId() {
		return ruleId;
	}

	/**
	 * @param ruleId the ruleId to set
	 */
	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}

	/**
	 * @return the ruleType
	 */
	public Integer getRuleType() {
		return ruleType;
	}

	/**
	 * @param ruleType the ruleType to set
	 */
	public void setRuleType(Integer ruleType) {
		this.ruleType = ruleType;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the objectId
	 */
	public Integer getObjectId() {
		return ObjectId;
	}

	/**
	 * @param objectId the objectId to set
	 */
	public void setObjectId(Integer objectId) {
		this.ObjectId = objectId;
	}

	/**
	 * @return the attributeId_1
	 */
	public Integer getAttributeId_1() {
		return attributeId_1;
	}

	/**
	 * @param attributeId_1 the attributeId_1 to set
	 */
	public void setAttributeId_1(Integer attributeId_1) {
		this.attributeId_1 = attributeId_1;
	}

	/**
	 * @return the attributeId_2
	 */
	public Integer getAttributeId_2() {
		return attributeId_2;
	}

	/**
	 * @param attributeId_2 the attributeId_2 to set
	 */
	public void setAttributeId_2(Integer attributeId_2) {
		this.attributeId_2 = attributeId_2;
	}

	/**
	 * @return the attributeId_3
	 */
	public Integer getAttributeId_3() {
		return attributeId_3;
	}

	/**
	 * @param attributeId_3 the attributeId_3 to set
	 */
	public void setAttributeId_3(Integer attributeId_3) {
		this.attributeId_3 = attributeId_3;
	}

	/**
	 * @return the attributeId_4
	 */
	public Integer getAttributeId_4() {
		return attributeId_4;
	}

	/**
	 * @param attributeId_4 the attributeId_4 to set
	 */
	public void setAttributeId_4(Integer attributeId_4) {
		this.attributeId_4 = attributeId_4;
	}

	/**
	 * @return the attributeId_5
	 */
	public Integer getAttributeId_5() {
		return attributeId_5;
	}

	/**
	 * @param attributeId_5 the attributeId_5 to set
	 */
	public void setAttributeId_5(Integer attributeId_5) {
		this.attributeId_5 = attributeId_5;
	}

	/**
	 * @return the displayOrder
	 */
	public Integer getDisplayOrder() {
		return displayOrder;
	}

	/**
	 * @param displayOrder the displayOrder to set
	 */
	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	/**
	 * @return the registred
	 */
	public Date getRegistred() {
		return registred;
	}

	/**
	 * @param registred the registred to set
	 */
	public void setRegistred(Date registred) {
		this.registred = registred;
	}

	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}

	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}

}
