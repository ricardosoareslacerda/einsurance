package br.com.tratomais.core.model.product;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.tratomais.core.dao.PersistentEntity;

@Entity
@Table(name="ProductClause")
public class ProductClause implements PersistentEntity {
	
	private static final long serialVersionUID = 7243081523214624403L;

	@Id
	private ProductClauseId id;

	@Column(name="ExpiryDate", columnDefinition="datetime", nullable=false)
	private Date expiryDate;

	@Column(name="Name", columnDefinition="String", nullable=true)
	private String name;
	
	@Column(name="Description", columnDefinition="String", nullable=true)
	private String description;
	
	@Column(name="Printed", columnDefinition="Boolean", nullable=true)
	private Boolean printed;
	
	@Column(name="Displayed", columnDefinition="Boolean", nullable=true)
	private Boolean displayed;

	@Column(name="Registred", columnDefinition="datetime", nullable=false)
	private Date registred;

	@Column(name="Updated", columnDefinition="datetime", nullable=true)
	private Date updated;

	/**
	 * @return the id
	 */
	public ProductClauseId getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(ProductClauseId id) {
		this.id = id;
	}

	/**
	 * @return the expiryDate
	 */
	public Date getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expiryDate the expiryDate to set
	 */
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the printed
	 */
	public Boolean getPrinted() {
		return printed;
	}

	/**
	 * @param printed the printed to set
	 */
	public void setPrinted(Boolean printed) {
		this.printed = printed;
	}

	/**
	 * @return the displayed
	 */
	public Boolean getDisplayed() {
		return displayed;
	}

	/**
	 * @param displayed the displayed to set
	 */
	public void setDisplayed(Boolean displayed) {
		this.displayed = displayed;
	}

	/**
	 * @return the registred
	 */
	public Date getRegistred() {
		return registred;
	}

	/**
	 * @param registred the registred to set
	 */
	public void setRegistred(Date registred) {
		this.registred = registred;
	}

	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}

	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}

}
