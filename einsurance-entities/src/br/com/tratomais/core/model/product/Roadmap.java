package br.com.tratomais.core.model.product;

// Generated 30/12/2009 20:49:46 by Hibernate Tools 3.2.4.GA

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

/**
 * Roadmap generated by hbm2java
 */
public class Roadmap implements PersistentEntity {

	public static final int ROADMAP_TYPE_START = 332;
	public static final int ROADMAP_TYPE_PURE_PREMIUM = 278;
	public static final int ROADMAP_TYPE_TARIFF_PREMIUM = 279;
	public static final int ROADMAP_TYPE_COMMERCIAL_PREMIUM = 280;
	public static final int ROADMAP_TYPE_PROFIT_PREMIUM = 281;
	public static final int ROADMAP_TYPE_NET_PREMIUM = 282;
	public static final int ROADMAP_TYPE_TOTAL_PREMIUM = 283;
	public static final int ROADMAP_TYPE_RENEWAL = 633;

	/**
	 * 
	 */
	private static final long serialVersionUID = -4925792943666291914L;
	private int roadmapId;
	private int algorithmId;
	private int roadmapCode;
	private int roadmapType;
	private int productId;
	private int coverageId;
	private int factorId;
	private Date effectiveDate;
	private Date expiryDate;
	private boolean compulsory;
	private int applyOrder;
	private Date registred;
	private Date updated;

	public Roadmap() {
	}

	public Roadmap(int roadmapId, int algorithmId, int roadmapCode, int roadmapType, int productId, int coverageId, int factorId, Date effectiveDate,
			Date expiryDate, int applyOrder, Date registred) {
		this.roadmapId = roadmapId;
		this.algorithmId = algorithmId;
		this.roadmapCode = roadmapCode;
		this.roadmapType = roadmapType;
		this.productId = productId;
		this.coverageId = coverageId;
		this.factorId = factorId;
		this.effectiveDate = effectiveDate;
		this.expiryDate = expiryDate;
		this.applyOrder = applyOrder;
		this.registred = registred;
	}

	public Roadmap(int roadmapId, int algorithmId, int roadmapCode, int roadmapType, int productId, int coverageId, int factorId, Date effectiveDate,
			Date expiryDate, int applyOrder, Date registred, Date updated) {
		this.roadmapId = roadmapId;
		this.algorithmId = algorithmId;
		this.roadmapCode = roadmapCode;
		this.roadmapType = roadmapType;
		this.productId = productId;
		this.coverageId = coverageId;
		this.factorId = factorId;
		this.effectiveDate = effectiveDate;
		this.expiryDate = expiryDate;
		this.applyOrder = applyOrder;
		this.registred = registred;
		this.updated = updated;
	}

	public int getRoadmapId() {
		return this.roadmapId;
	}

	public void setRoadmapId(int roadmapId) {
		this.roadmapId = roadmapId;
	}

	public int getAlgorithmId() {
		return this.algorithmId;
	}

	public void setAlgorithmId(int algorithmId) {
		this.algorithmId = algorithmId;
	}

	public int getRoadmapCode() {
		return this.roadmapCode;
	}

	public void setRoadmapCode(int roadmapCode) {
		this.roadmapCode = roadmapCode;
	}

	public int getRoadmapType() {
		return this.roadmapType;
	}

	public void setRoadmapType(int roadmapType) {
		this.roadmapType = roadmapType;
	}

	public int getProductId() {
		return this.productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getCoverageId() {
		return this.coverageId;
	}

	public void setCoverageId(int coverageId) {
		this.coverageId = coverageId;
	}

	public int getFactorId() {
		return this.factorId;
	}

	public void setFactorId(int factorId) {
		this.factorId = factorId;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public boolean isCompulsory() {
		return compulsory;
	}

	public void setCompulsory(boolean compulsory) {
		this.compulsory = compulsory;
	}

	public int getApplyOrder() {
		return this.applyOrder;
	}

	public void setApplyOrder(int applyOrder) {
		this.applyOrder = applyOrder;
	}

	public Date getRegistred() {
		return this.registred;
	}

	public void setRegistred(Date registred) {
		this.registred = registred;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

}
