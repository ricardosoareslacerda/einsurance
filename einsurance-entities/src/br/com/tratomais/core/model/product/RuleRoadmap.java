package br.com.tratomais.core.model.product;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.tratomais.core.dao.PersistentEntity;

@Entity
@Table(name="RuleRoadmap")
public class RuleRoadmap implements PersistentEntity {
	
	private static final long serialVersionUID = -5663799558910523562L;

	@Id
	RuleRoadmapId id;
	
	@Column(name="ExpiryDate", columnDefinition="Integer", nullable=true)
	private Date expiryDate;
	
	@Column(name="Registred", columnDefinition="datetime", nullable=true)
	private Date registred;
	
	@Column(name="Updated", columnDefinition="datetime", nullable=true)
	private Date updated;
	
	@Column(name="ApplyOrder", columnDefinition="Integer", nullable=false)
	private Integer applyOrder;

	/**
	 * @return the id
	 */
	public RuleRoadmapId getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(RuleRoadmapId id) {
		this.id = id;
	}

	/**
	 * @return the expiryDate
	 */
	public Date getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expiryDate the expiryDate to set
	 */
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * @return the registred
	 */
	public Date getRegistred() {
		return registred;
	}

	/**
	 * @param registred the registred to set
	 */
	public void setRegistred(Date registred) {
		this.registred = registred;
	}

	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}

	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	/**
	 * @return the applyOrder
	 */
	public Integer getApplyOrder() {
		return applyOrder;
	}

	/**
	 * @param applyOrder the applyOrder to set
	 */
	public void setApplyOrder(Integer applyOrder) {
		this.applyOrder = applyOrder;
	}


}
