package br.com.tratomais.core.model.product;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import br.com.tratomais.core.dao.PersistentEntity;

@Embeddable
public class RuleTargetId implements PersistentEntity {
	
	private static final long serialVersionUID = 5243461809757882209L;

	@Column(name="RuleTargetId", columnDefinition="Integer", nullable=false)
	private Integer ruleTargeId;

	/**
	 * @return the ruleTargeId
	 */
	public Integer getRuleTargeId() {
		return ruleTargeId;
	}

	/**
	 * @param ruleTargeId the ruleTargeId to set
	 */
	public void setRuleTargeId(Integer ruleTargeId) {
		this.ruleTargeId = ruleTargeId;
	}

}