package br.com.tratomais.core.model.product;

// Generated Jul 29, 2011 12:15:37 PM by Hibernate Tools 3.2.4.GA

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

/**
 * CoverageRuleValue generated by hbm2java
 */
public class CoverageRuleValue implements PersistentEntity {

	private static final long serialVersionUID = 2309959587219000510L;
	private CoverageRuleValueId id;
	private CoverageRule coverageRule;
	private Double ruleValue;
	private Double ruleFactor;
	private Date expiryDate;
	private Date registred;
	private Date updated;

	public CoverageRuleValue() {
	}

	public CoverageRuleValue(CoverageRuleValueId id, CoverageRule coverageRule,
			Date expiryDate, Date registred) {
		this.id = id;
		this.coverageRule = coverageRule;
		this.expiryDate = expiryDate;
		this.registred = registred;
	}

	public CoverageRuleValue(CoverageRuleValueId id, CoverageRule coverageRule,
			Double ruleValue, Double ruleFactor, Date expiryDate,
			Date registred, Date updated) {
		this.id = id;
		this.coverageRule = coverageRule;
		this.ruleValue = ruleValue;
		this.ruleFactor = ruleFactor;
		this.expiryDate = expiryDate;
		this.registred = registred;
		this.updated = updated;
	}

	public CoverageRuleValueId getId() {
		return this.id;
	}

	public void setId(CoverageRuleValueId id) {
		this.id = id;
	}

	public CoverageRule getCoverageRule() {
		return this.coverageRule;
	}

	public void setCoverageRule(CoverageRule coverageRule) {
		this.coverageRule = coverageRule;
	}

	public Double getRuleValue() {
		return this.ruleValue;
	}

	public void setRuleValue(Double ruleValue) {
		this.ruleValue = ruleValue;
	}

	public Double getRuleFactor() {
		return this.ruleFactor;
	}

	public void setRuleFactor(Double ruleFactor) {
		this.ruleFactor = ruleFactor;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Date getRegistred() {
		return this.registred;
	}

	public void setRegistred(Date registred) {
		this.registred = registred;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
}
