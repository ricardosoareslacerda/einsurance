package br.com.tratomais.core.model.product;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import br.com.tratomais.core.dao.PersistentEntity;

@Embeddable
public class ProductClauseId implements PersistentEntity {
	
	private static final long serialVersionUID = 7512594351492918126L;
	
	@Column(name="ProductId", columnDefinition="Integer", nullable=false)
	private Integer productId;

	@Column(name="ClauseId", columnDefinition="Integer", nullable=false)
	private Integer clauseId;
	
	@Column(name="EffectiveDate", columnDefinition="datetime", nullable=false)
	private Date effectiveDate;

	/**
	 * @return the productId
	 */
	public Integer getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	/**
	 * @return the clauseId
	 */
	public Integer getClauseId() {
		return clauseId;
	}

	/**
	 * @param clauseId the clauseId to set
	 */
	public void setClauseId(Integer clauseId) {
		this.clauseId = clauseId;
	}

	/**
	 * @return the effectiveDate
	 */
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

}
