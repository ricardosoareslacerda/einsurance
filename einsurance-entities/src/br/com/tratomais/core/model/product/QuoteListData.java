package br.com.tratomais.core.model.product;

import java.util.Collection;

@SuppressWarnings("unchecked")
public class QuoteListData {
	
	public QuoteListData(){
		
	}
	
	private Collection listGender;
	private Collection listOccupation;
	private Collection listHousing;
	private Collection listPlanRisk;
	private Collection listCoverageStructure;
	private Collection listCoverageContent;
	private Collection listPlanRiskType;
	private Collection listPersonalRiskPlan;
	private Collection listProductPlan;
	private ProductVersion productVersion;
	private ProductPlan productPlan;
	
	public Collection getListGender() {
		return listGender;
	}
	
	public void setListGender(Collection listGender) {
		this.listGender = listGender;
	}
	
	public Collection getListOccupation() {
		return listOccupation;
	}
	
	public void setListOccupation(Collection listOccupation) {
		this.listOccupation = listOccupation;
	}
	
	public Collection getListHousing() {
		return listHousing;
	}
	
	public void setListHousing(Collection listHousing) {
		this.listHousing = listHousing;
	}
	
	public Collection getListPlanRisk() {
		return listPlanRisk;
	}
	
	public void setListPlanRisk(Collection listPlanRisk) {
		this.listPlanRisk = listPlanRisk;
	}
	
	public Collection getListCoverageStructure() {
		return listCoverageStructure;
	}
	
	public void setListCoverageStructure(Collection listCoverageStructure) {
		this.listCoverageStructure = listCoverageStructure;
	}
	
	public Collection getListCoverageContent() {
		return listCoverageContent;
	}
	
	public void setListCoverageContent(Collection listCoverageContent) {
		this.listCoverageContent = listCoverageContent;
	}

	public Collection getListPlanRiskType() {
		return listPlanRiskType;
	}

	public void setListPlanRiskType(Collection listPlanRiskType) {
		this.listPlanRiskType = listPlanRiskType;
	}

	public Collection getListPersonalRiskPlan() {
		return listPersonalRiskPlan;
	}

	public void setListPersonalRiskPlan(Collection listPersonalRiskPlan) {
		this.listPersonalRiskPlan = listPersonalRiskPlan;
	}

	public Collection getListProductPlan() {
		return listProductPlan;
	}

	public void setListProductPlan(Collection listProductPlan) {
		this.listProductPlan = listProductPlan;
	}

	public ProductVersion getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(ProductVersion productVersion) {
		this.productVersion = productVersion;
	}

	public ProductPlan getProductPlan() {
		return productPlan;
	}

	public void setProductPlan(ProductPlan productPlan) {
		this.productPlan = productPlan;
	}
}
