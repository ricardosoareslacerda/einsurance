package br.com.tratomais.core.model.product;

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

public class Clause implements PersistentEntity, Cloneable {
	
	private static final long serialVersionUID = -2592399858204387847L;

	private ClauseId id;
	private Integer clauseType;
	private String clauseCode;
	private Integer objectId;
	private String name;
	private String description;
	private Date registred;
	private Date updated;
	
	/**
	 * @return the id
	 */
	public ClauseId getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(ClauseId id) {
		this.id = id;
	}
	/**
	 * @return the clauseType
	 */
	public Integer getClauseType() {
		return clauseType;
	}
	/**
	 * @param clauseType the clauseType to set
	 */
	public void setClauseType(Integer clauseType) {
		this.clauseType = clauseType;
	}
	/**
	 * @return the clauseCode
	 */
	public String getClauseCode() {
		return clauseCode;
	}
	/**
	 * @param clauseCode the clauseCode to set
	 */
	public void setClauseCode(String clauseCode) {
		this.clauseCode = clauseCode;
	}
	/**
	 * @return the objectId
	 */
	public Integer getObjectId() {
		return objectId;
	}
	/**
	 * @param objectId the objectId to set
	 */
	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the registred
	 */
	public Date getRegistred() {
		return registred;
	}
	/**
	 * @param registred the registred to set
	 */
	public void setRegistred(Date registred) {
		this.registred = registred;
	}
	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}
	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}	

}
