package br.com.tratomais.core.model.product;

// Generated Jul 29, 2011 12:15:37 PM by Hibernate Tools 3.2.4.GA

import java.util.Date;

/**
 * CoverageRuleValueId generated by hbm2java
 */
public class CoverageRuleValueId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4274536898906462663L;
	private int ruleId;
	private int attributeValue1;
	private int attributeValue2;
	private int attributeValue3;
	private int attributeValue4;
	private int attributeValue5;
	private Date effectiveDate;

	public CoverageRuleValueId() {
	}

	public CoverageRuleValueId(int ruleId,
			int attributeValue1, int attributeValue2, int attributeValue3,
			int attributeValue4, int attributeValue5, Date effectiveDate) {
		this.ruleId = ruleId;
		this.attributeValue1 = attributeValue1;
		this.attributeValue2 = attributeValue2;
		this.attributeValue3 = attributeValue3;
		this.attributeValue4 = attributeValue4;
		this.attributeValue5 = attributeValue5;
		this.effectiveDate = effectiveDate;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public int getRuleId() {
		return this.ruleId;
	}

	public void setRuleId(int ruleId) {
		this.ruleId = ruleId;
	}

	public int getAttributeValue1() {
		return this.attributeValue1;
	}

	public void setAttributeValue1(int attributeValue1) {
		this.attributeValue1 = attributeValue1;
	}

	public int getAttributeValue2() {
		return this.attributeValue2;
	}

	public void setAttributeValue2(int attributeValue2) {
		this.attributeValue2 = attributeValue2;
	}

	public int getAttributeValue3() {
		return this.attributeValue3;
	}

	public void setAttributeValue3(int attributeValue3) {
		this.attributeValue3 = attributeValue3;
	}

	public int getAttributeValue4() {
		return this.attributeValue4;
	}

	public void setAttributeValue4(int attributeValue4) {
		this.attributeValue4 = attributeValue4;
	}

	public int getAttributeValue5() {
		return this.attributeValue5;
	}

	public void setAttributeValue5(int attributeValue5) {
		this.attributeValue5 = attributeValue5;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CoverageRuleValueId))
			return false;
		CoverageRuleValueId castOther = (CoverageRuleValueId) other;

		return ((this.getEffectiveDate() == castOther.getEffectiveDate()) || (this
				.getEffectiveDate() != null
				&& castOther.getEffectiveDate() != null && this
				.getEffectiveDate().equals(castOther.getEffectiveDate())))
				&& (this.getRuleId() == castOther.getRuleId())
				&& (this.getAttributeValue1() == castOther.getAttributeValue1())
				&& (this.getAttributeValue2() == castOther.getAttributeValue2())
				&& (this.getAttributeValue3() == castOther.getAttributeValue3())
				&& (this.getAttributeValue4() == castOther.getAttributeValue4())
				&& (this.getAttributeValue5() == castOther.getAttributeValue5());
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getEffectiveDate() == null ? 0 : this.getEffectiveDate()
						.hashCode());
		result = 37 * result + this.getRuleId();
		result = 37 * result + this.getAttributeValue1();
		result = 37 * result + this.getAttributeValue2();
		result = 37 * result + this.getAttributeValue3();
		result = 37 * result + this.getAttributeValue4();
		result = 37 * result + this.getAttributeValue5();
		return result;
	}

}
