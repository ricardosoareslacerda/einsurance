package br.com.tratomais.core.model.product;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.tratomais.core.dao.PersistentEntity;

@Entity
@Table(name="RuleType")
public class RuleType implements PersistentEntity {
	
	private static final long serialVersionUID = -5437827525976248490L;

	@Id
	private RuleTypeId id;

	@Column(name="ObjectID", columnDefinition="Integer", nullable=false)
	private Integer ObjectId;
	
	@Column(name="AttributeID_1", columnDefinition="Integer", nullable=true)
	private Integer AttributeId1;
	
	@Column(name="AttributeID_2", columnDefinition="Integer", nullable=true)
	private Integer AttributeId2;
	
	@Column(name="AttributeID_3", columnDefinition="Integer", nullable=true)
	private Integer AttributeId3;
	
	@Column(name="AttributeID_4", columnDefinition="Integer", nullable=true)
	private Integer AttributeId4;
	
	@Column(name="AttributeID_5", columnDefinition="Integer", nullable=true)
	private Integer AttributeId5;
	
	@Column(name="Registred", columnDefinition="datetime", nullable=false)
	private Date Registred;
	
	@Column(name="Updated", columnDefinition="datetime", nullable=true)
	private Date updated;

	/**
	 * @return the id
	 */
	public RuleTypeId getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(RuleTypeId id) {
		this.id = id;
	}

	/**
	 * @return the objectId
	 */
	public Integer getObjectId() {
		return ObjectId;
	}

	/**
	 * @param objectId the objectId to set
	 */
	public void setObjectId(Integer ObjectId) {
		this.ObjectId = ObjectId;
	}

	/**
	 * @return the attributeId1
	 */
	public Integer getAttributeId1() {
		return AttributeId1;
	}

	/**
	 * @param attributeId1 the attributeId1 to set
	 */
	public void setAttributeId1(Integer attributeId1) {
		AttributeId1 = attributeId1;
	}

	/**
	 * @return the attributeId2
	 */
	public Integer getAttributeId2() {
		return AttributeId2;
	}

	/**
	 * @param attributeId2 the attributeId2 to set
	 */
	public void setAttributeId2(Integer attributeId2) {
		AttributeId2 = attributeId2;
	}

	/**
	 * @return the attributeId3
	 */
	public Integer getAttributeId3() {
		return AttributeId3;
	}

	/**
	 * @param attributeId3 the attributeId3 to set
	 */
	public void setAttributeId3(Integer attributeId3) {
		AttributeId3 = attributeId3;
	}

	/**
	 * @return the attributeId4
	 */
	public Integer getAttributeId4() {
		return AttributeId4;
	}

	/**
	 * @param attributeId4 the attributeId4 to set
	 */
	public void setAttributeId4(Integer attributeId4) {
		AttributeId4 = attributeId4;
	}

	/**
	 * @return the attributeId5
	 */
	public Integer getAttributeId5() {
		return AttributeId5;
	}

	/**
	 * @param attributeId5 the attributeId5 to set
	 */
	public void setAttributeId5(Integer attributeId5) {
		AttributeId5 = attributeId5;
	}

	/**
	 * @return the registred
	 */
	public Date getRegistred() {
		return Registred;
	}

	/**
	 * @param registred the registred to set
	 */
	public void setRegistred(Date registred) {
		Registred = registred;
	}

	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}

	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
}
