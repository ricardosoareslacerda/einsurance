package br.com.tratomais.core.model.product;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import br.com.tratomais.core.dao.PersistentEntity;

@Embeddable
public class RuleTypeId implements PersistentEntity {
	
	private static final long serialVersionUID = -7836364982418210965L;

	@Column(name="RuleType", columnDefinition="Integer", nullable=false)
	private Integer ruleType;
	
	/**
	 * @return the ruleType
	 */
	public Integer getRuleType() {
		return ruleType;
	}
	
	/**
	 * @param ruleType the ruleType to set
	 */
	public void setRuleType(Integer ruleType) {
		this.ruleType = ruleType;
	}
}