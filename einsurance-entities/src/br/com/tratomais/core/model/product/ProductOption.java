package br.com.tratomais.core.model.product;

import java.util.Date;

public class ProductOption {

	private int productId;
	private String policyNumber;
	private int masterPolicyId;
	private Double commissionFactory;
	private int currencyId;
	private int objectId;
	private int objectType;
	private String nickName;
	private String name;
	private String imageProperty;
	private Date referenceDate;
	private Date effectiveDate;
	private Date expiryDate;
	private Integer brokerId;
	private Boolean beneficiaryRequired;
	private Boolean profileRequired;
	private Integer riskValueType;
	
	/** Maximum of occurrences permitted */ 
	private Integer maxOccurrences;
	
	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}

	public int getObjectId() {
		return objectId;
	}

	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}

	public int getObjectType() {
		return objectType;
	}

	public void setObjectType(int objectType) {
		this.objectType = objectType;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getImageProperty() {
		return imageProperty;
	}

	public void setImageProperty(String imageProperty) {
		this.imageProperty = imageProperty;
	}

	public Date getReferenceDate() {
		return referenceDate;
	}

	public void setReferenceDate(Date referenceDate) {
		this.referenceDate = referenceDate;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public Double getCommissionFactory() {
		return commissionFactory;
	}

	public void setCommissionFactory(Double commissionFactory) {
		this.commissionFactory = commissionFactory;
	}

	public int getMasterPolicyId() {
		return masterPolicyId;
	}

	public void setMasterPolicyId(int masterPolicyId) {
		this.masterPolicyId = masterPolicyId;
	}

	/**
	 * @param brokerId the brokerId to set
	 */
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	/**
	 * @return the brokerId
	 */
	public Integer getBrokerId() {
		return brokerId;
	}

	public Boolean getProfileRequired() {
		return profileRequired;
	}

	public void setProfileRequired(Boolean profileRequired) {
		this.profileRequired = profileRequired;
	}

	public Boolean getBeneficiaryRequired() {
		return beneficiaryRequired;
	}

	public void setBeneficiaryRequired(Boolean beneficiaryRequired) {
		this.beneficiaryRequired = beneficiaryRequired;
	}

	public Integer getRiskValueType() {
		return this.riskValueType;
	}

	public void setRiskValueType(Integer riskValueType) {
		this.riskValueType = riskValueType;
	}

	/**
	 * @return the maxOccurrences
	 */
	public Integer getMaxOccurrences() {
		return maxOccurrences;
	}

	/**
	 * @param maxOccurrences the maxOccurrences to set
	 */
	public void setMaxOccurrences(Integer maxOccurrences) {
		this.maxOccurrences = maxOccurrences;
	}
}
