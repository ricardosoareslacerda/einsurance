package br.com.tratomais.core.model.product;

import java.util.List;

import br.com.tratomais.core.dao.PersistentEntity;

@SuppressWarnings("unchecked")
public class IdentifiedList implements PersistentEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IdentifiedList(){}
	
	public IdentifiedList(String groupId, List resultArray) {
		super();
		this.groupId = groupId;
		this.resultArray = resultArray;
	}
	
	private String groupId;
	private List resultArray;
	private String nameComponent;
	private Object value;
	
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public List getResultArray() {
		return resultArray;
	}
	public void setResultArray(List resultArray) {
		this.resultArray = resultArray;
	}
	public String getNameComponent() {
		return nameComponent;
	}
	public void setNameComponent(String nameComponent) {
		this.nameComponent = nameComponent;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
}
