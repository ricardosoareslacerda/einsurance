package br.com.tratomais.core.model.product;

// Generated 13/02/2010 12:28:57 by Hibernate Tools 3.2.4.GA

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntityId;

/**
 * CoveragePersonalRiskId generated by hbm2java
 */
public class CoveragePersonalRiskId extends PersistentEntityId {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4836952736653116084L;
	private Date effectiveDate;
	private int personalRiskType;
	private int coverageId;
	private int planId;
	private int productId;

	public CoveragePersonalRiskId() {
	}

	public CoveragePersonalRiskId(Date effectiveDate, int personalRiskType, int coverageId, int planId, int productId) {
		this.effectiveDate = effectiveDate;
		this.personalRiskType = personalRiskType;
		this.coverageId = coverageId;
		this.planId = planId;
		this.productId = productId;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public int getPersonalRiskType() {
		return this.personalRiskType;
	}

	public void setPersonalRiskType(int personalRiskType) {
		this.personalRiskType = personalRiskType;
	}

	public int getCoverageId() {
		return this.coverageId;
	}

	public void setCoverageId(int coverageId) {
		this.coverageId = coverageId;
	}

	public int getPlanId() {
		return this.planId;
	}

	public void setPlanId(int planId) {
		this.planId = planId;
	}

	public int getProductId() {
		return this.productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public boolean equals(Object other) {
		if ( ( this == other ) )
			return true;
		if ( ( other == null ) )
			return false;
		if ( !( other instanceof CoveragePersonalRiskId ) )
			return false;
		CoveragePersonalRiskId castOther = ( CoveragePersonalRiskId ) other;

		return ( ( this.getEffectiveDate() == castOther.getEffectiveDate() ) || ( this.getEffectiveDate() != null
				&& castOther.getEffectiveDate() != null && this.getEffectiveDate().equals( castOther.getEffectiveDate() ) ) )
				&& ( this.getPersonalRiskType() == castOther.getPersonalRiskType() )
				&& ( this.getCoverageId() == castOther.getCoverageId() )
				&& ( this.getPlanId() == castOther.getPlanId() ) && ( this.getProductId() == castOther.getProductId() );
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + ( getEffectiveDate() == null ? 0 : this.getEffectiveDate().hashCode() );
		result = 37 * result + this.getPersonalRiskType();
		result = 37 * result + this.getCoverageId();
		result = 37 * result + this.getPlanId();
		result = 37 * result + this.getProductId();
		return result;
	}

}
