package br.com.tratomais.core.model.product;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import br.com.tratomais.core.dao.PersistentEntity;

@Embeddable
public class RuleRoadmapId implements PersistentEntity {
	
	private static final long serialVersionUID = -4847952024467312460L;

	@Column(name="RuleTargetId", columnDefinition="Integer", nullable=false)
	private Integer ruleTargeId;

	@Column(name="RuleId", columnDefinition="Integer", nullable=true)
	private Integer ruleId;

	@Column(name="EffectiveDate", columnDefinition="Integer", nullable=true)
	private Date effectiveDate;

	/**
	 * @return the ruleTargeId
	 */
	public Integer getRuleTargeId() {
		return ruleTargeId;
	}

	/**
	 * @param ruleTargeId the ruleTargeId to set
	 */
	public void setRuleTargeId(Integer ruleTargeId) {
		this.ruleTargeId = ruleTargeId;
	}

	/**
	 * @return the ruleId
	 */
	public Integer getRuleId() {
		return ruleId;
	}

	/**
	 * @param ruleId the ruleId to set
	 */
	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}

	/**
	 * @return the effectiveDate
	 */
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

}
