package br.com.tratomais.core.model.product;

import br.com.tratomais.core.dao.PersistentEntityId;

public class ClauseId extends PersistentEntityId {
	
	private static final long serialVersionUID = -7836242378328405167L;
	
	private Integer clauseId;

	/**
	 * @return the clauseId
	 */
	public Integer getClauseId() {
		return clauseId;
	}

	/**
	 * @param clauseId the clauseId to set
	 */
	public void setClauseId(Integer clauseId) {
		this.clauseId = clauseId;
	}

	
}
