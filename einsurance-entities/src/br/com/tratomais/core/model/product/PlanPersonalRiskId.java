package br.com.tratomais.core.model.product;

// Generated 19/01/2010 16:16:00 by Hibernate Tools 3.2.4.GA

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntityId;

/**
 * PlanPersonalRiskId generated by hbm2java
 */
public class PlanPersonalRiskId extends PersistentEntityId {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4340352443598366389L;
	private Date effectiveDate;
	private int personType;
	private int planId;
	private int productId;

	public PlanPersonalRiskId() {
	}

	public PlanPersonalRiskId(Date effectiveDate, int personType, int planId, int productId) {
		this.effectiveDate = effectiveDate;
		this.personType = personType;
		this.planId = planId;
		this.productId = productId;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public int getPersonType() {
		return this.personType;
	}

	public void setPersonType(int personType) {
		this.personType = personType;
	}

	public int getPlanId() {
		return this.planId;
	}

	public void setPlanId(int planId) {
		this.planId = planId;
	}

	public int getProductId() {
		return this.productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public boolean equals(Object other) {
		if ( ( this == other ) )
			return true;
		if ( ( other == null ) )
			return false;
		if ( !( other instanceof PlanPersonalRiskId ) )
			return false;
		PlanPersonalRiskId castOther = ( PlanPersonalRiskId ) other;

		return ( ( this.getEffectiveDate() == castOther.getEffectiveDate() ) || ( this.getEffectiveDate() != null
				&& castOther.getEffectiveDate() != null && this.getEffectiveDate().equals( castOther.getEffectiveDate() ) ) )
				&& ( this.getPersonType() == castOther.getPersonType() )
				&& ( this.getPlanId() == castOther.getPlanId() )
				&& ( this.getProductId() == castOther.getProductId() );
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + ( getEffectiveDate() == null ? 0 : this.getEffectiveDate().hashCode() );
		result = 37 * result + this.getPersonType();
		result = 37 * result + this.getPlanId();
		result = 37 * result + this.getProductId();
		return result;
	}

}
