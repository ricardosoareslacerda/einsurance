package br.com.tratomais.core.model.product;

// Generated 03/03/2010 16:36:47 by Hibernate Tools 3.2.4.GA

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

/**
 * PlanPersonalRisk generated by hbm2java
 */
public class PlanPersonalRisk implements PersistentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9020506755564983785L;
	private PlanPersonalRiskId id;
	private Plan plan;
	private Date expiryDate;
	private short quantityPersonalRisk;
	private short quantityAdditional;
	private boolean additionalNewborn;
	private Date registred;
	private Date updated;

	public PlanPersonalRisk() {
	}

	public PlanPersonalRisk(PlanPersonalRiskId id, Plan plan, Date expiryDate, short quantityPersonalRisk, short quantityAdditional,
			boolean additionalNewborn, Date registred) {
		this.id = id;
		this.plan = plan;
		this.expiryDate = expiryDate;
		this.quantityPersonalRisk = quantityPersonalRisk;
		this.quantityAdditional = quantityAdditional;
		this.additionalNewborn = additionalNewborn;
		this.registred = registred;
	}

	public PlanPersonalRisk(PlanPersonalRiskId id, Plan plan, Date expiryDate, short quantityPersonalRisk, short quantityAdditional,
			boolean additionalNewborn, Date registred, Date updated) {
		this.id = id;
		this.plan = plan;
		this.expiryDate = expiryDate;
		this.quantityPersonalRisk = quantityPersonalRisk;
		this.quantityAdditional = quantityAdditional;
		this.additionalNewborn = additionalNewborn;
		this.registred = registred;
		this.updated = updated;
	}

	public PlanPersonalRiskId getId() {
		return this.id;
	}

	public void setId(PlanPersonalRiskId id) {
		this.id = id;
	}

	public Plan getPlan() {
		return this.plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public short getQuantityPersonalRisk() {
		return this.quantityPersonalRisk;
	}

	public void setQuantityPersonalRisk(short quantityPersonalRisk) {
		this.quantityPersonalRisk = quantityPersonalRisk;
	}

	public short getQuantityAdditional() {
		return this.quantityAdditional;
	}

	public void setQuantityAdditional(short quantityAdditional) {
		this.quantityAdditional = quantityAdditional;
	}

	public boolean isAdditionalNewborn() {
		return this.additionalNewborn;
	}

	public void setAdditionalNewborn(boolean additionalNewborn) {
		this.additionalNewborn = additionalNewborn;
	}

	public Date getRegistred() {
		return this.registred;
	}

	public void setRegistred(Date registred) {
		this.registred = registred;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public void updateKey(ProductPlan parent){
		if ( parent.getId() == null ) {
			throw new IllegalArgumentException();
		}
		
		this.setPlan( parent.getPlan() );

		if ( this.getId() == null ) {
			this.setId( new PlanPersonalRiskId( new Date(), 0, parent.getId().getPlanId(), parent.getId().getProductId()) );
		}else{
			this.getId().setPlanId( parent.getId().getPlanId() );
			this.getId().setProductId( parent.getId().getProductId() );
		}

	}

	
}
