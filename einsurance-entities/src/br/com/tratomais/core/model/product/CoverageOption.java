package br.com.tratomais.core.model.product;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.com.tratomais.core.dao.PersistentEntity;
import br.com.tratomais.core.model.policy.CalcStep;

public class CoverageOption implements PersistentEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean contractCoverage;
	private boolean contractPlan;
	private CoveragePlan coveragePlan;
	private boolean enableSelectionCoverage;
	private boolean enableSelectionPlan;
	private boolean middle;
	private Double percentage;
	private int rangeValueId;	
	private	List< CoverageRangeValue > rangeValueList = new ArrayList< CoverageRangeValue >();
	private	List< Deductible > deductibleList = new ArrayList< Deductible >();
	private	Set< CalcStep > calcStepList = new HashSet< CalcStep >( 0 );
	private Double value;
	private Double valueTotal;
	private Double totalPremium;
	private Deductible deductible;
	private boolean modified;
	
	public boolean isContractCoverage() {
		return contractCoverage;
	}

	public void setContractCoverage(boolean checkedCoverage) {
		this.contractCoverage = checkedCoverage;
	}

	public boolean isContractPlan() {
		return contractPlan;
	}
	
	public void setContractPlan(boolean checked) {
		this.contractPlan = checked;
	}
	
	public CoveragePlan getCoveragePlan() {
		return coveragePlan;
	}

	public void setCoveragePlan(CoveragePlan coveragePlan) {
		this.coveragePlan = coveragePlan;
	}
	
	public boolean isEnableSelectionCoverage() {
		return enableSelectionCoverage;
	}

	public void setEnableSelectionCoverage(boolean enableCoverage) {
		this.enableSelectionCoverage = enableCoverage;
	}

	public boolean isEnableSelectionPlan() {
		return enableSelectionPlan;
	}
	
	public void setEnableSelectionPlan(boolean enable) {
		this.enableSelectionPlan = enable;
	}
	
	public boolean getMiddle() {
		return middle;
	}
	
	public void setMiddle(boolean middle) {
		this.middle = middle;
	}
	
	public String getNickName() {
		return coveragePlan.getNickName();
	}
	
	public Double getPercentage() {
		return percentage;
	}
	
	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}
	
	public int getRangeValueId() {
		return rangeValueId;
	}

	public void setRangeValueId(int rangeValueId) {
		this.rangeValueId = rangeValueId;
	}

	public List<CoverageRangeValue> getRangeValueList() {
		return rangeValueList;
	}

	public void setRangeValueList(List<CoverageRangeValue> rangeValueList) {
		this.rangeValueList = rangeValueList;
	}
	
	public Double getValue() {
		return value;
	}
	
	public void setValue(Double value) {
		this.value = value;
	}
	
	public Double getValueTotal() {
		return valueTotal;
	}
	
	public void setValueTotal(Double valueTotal) {
		this.valueTotal = valueTotal;
	}

	public List<Deductible> getDeductibleList() {
		return deductibleList;
	}

	public void setDeductibleList(List<Deductible> deductibleList) {
		this.deductibleList = deductibleList;
	}

	public Deductible getDeductible() {
		return deductible;
	}

	public void setDeductible(Deductible deductible) {
		this.deductible = deductible;
	}

	public Double getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(Double totalPremium) {
		this.totalPremium = totalPremium;
	}

	public Set<CalcStep> getCalcStepList() {
		return calcStepList;
	}

	public void setModified(boolean modified) {
		this.modified = modified;
	}

	public boolean isModified() {
		return modified;
	}

	public void setCalcStepList(Set<CalcStep> calcStepList) {
		this.calcStepList = calcStepList;
	}
}
