package br.com.tratomais.core.model.product;

import java.util.ArrayList;
import java.util.List;

import br.com.tratomais.core.dao.PersistentEntity;

public class CoverageDataGridOption implements PersistentEntity{
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	/*
	@Override
	public String toString() {
		return "CoverageDataGridOption [compulsory=" + compulsory
				+ ", coverageCode=" + coverageCode + ", coveragePlan1="
				+ coveragePlan1 + ", coveragePlan2=" + coveragePlan2
				+ ", coveragePlan3=" + coveragePlan3 + ", displayOrder="
				+ displayOrder + ", insuredValue1=" + insuredValue1
				+ ", insuredValue2=" + insuredValue2 + ", insuredValue3="
				+ insuredValue3 + ", insuredValueType1=" + insuredValueType1
				+ ", insuredValueType2=" + insuredValueType2
				+ ", insuredValueType3=" + insuredValueType3 + ", nickName="
				+ nickName + ", rangeValueId1=" + rangeValueId1
				+ ", rangeValueId2=" + rangeValueId2 + ", rangeValueId3="
				+ rangeValueId3 + ", rangeValueList1=" + rangeValueList1
				+ ", rangeValueList2=" + rangeValueList2 + ", rangeValueList3="
				+ rangeValueList3 + ", riskType1=" + riskType1 + ", riskType2="
				+ riskType2 + ", riskType3=" + riskType3
				+ ", selectedCoverage=" + selectedCoverage + "]";
	}
	*/
	/**
	 * 
	 */
	private static final long serialVersionUID = 3395849568446844651L;
	private String coverageCode;
	private String nickName;
	private Short displayOrder;
	private boolean compulsory;
	private boolean selectedCoverage;

	private Integer riskType1;
	private Integer riskType2;
	private Integer riskType3;

	private int insuredValueType1;
	private int insuredValueType2;
	private int insuredValueType3;
	
	private Double insuredValue1;
	private Double insuredValue2;
	private Double insuredValue3;
	
	List< CoverageRangeValue > rangeValueList1 = new ArrayList< CoverageRangeValue >();	
	List< CoverageRangeValue > rangeValueList2 = new ArrayList< CoverageRangeValue >();	
	List< CoverageRangeValue > rangeValueList3 = new ArrayList< CoverageRangeValue >();	
	
	private int rangeValueId1;
	private int rangeValueId2;
	private int rangeValueId3;
	
	private CoveragePlan coveragePlan1;
	private CoveragePlan coveragePlan2;
	private CoveragePlan coveragePlan3;

	public CoverageDataGridOption(){
		
	}

	/**
	 * @return the coverageCode
	 */
	public String getCoverageCode() {
		return coverageCode;
	}

	/**
	 * @param coverageCode the coverageCode to set
	 */
	public void setCoverageCode(String coverageCode) {
		this.coverageCode = coverageCode;
	}

	/**
	 * @return the nickName
	 */
	public String getNickName() {
		return nickName;
	}

	/**
	 * @param nickName the nickName to set
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	/**
	 * @return the displayOrder
	 */
	public Short getDisplayOrder() {
		return displayOrder;
	}

	/**
	 * @param displayOrder the displayOrder to set
	 */
	public void setDisplayOrder(Short displayOrder) {
		this.displayOrder = displayOrder;
	}

	/**
	 * @return the compulsory
	 */
	public boolean isCompulsory() {
		return compulsory;
	}

	/**
	 * @param compulsory the compulsory to set
	 */
	public void setCompulsory(boolean compulsory) {
		this.compulsory = compulsory;
	}

	/**
	 * @return the selectedCoverage
	 */
	public boolean isSelectedCoverage() {
		return selectedCoverage;
	}

	/**
	 * @param selectedCoverage the selectedCoverage to set
	 */
	public void setSelectedCoverage(boolean selectedCoverage) {
		this.selectedCoverage = selectedCoverage;
	}

	/**
	 * @return the riskType1
	 */
	public Integer getRiskType1() {
		return riskType1;
	}

	/**
	 * @param riskType1 the riskType1 to set
	 */
	public void setRiskType1(Integer riskType1) {
		this.riskType1 = riskType1;
	}

	/**
	 * @return the riskType2
	 */
	public Integer getRiskType2() {
		return riskType2;
	}

	/**
	 * @param riskType2 the riskType2 to set
	 */
	public void setRiskType2(Integer riskType2) {
		this.riskType2 = riskType2;
	}

	/**
	 * @return the riskType3
	 */
	public Integer getRiskType3() {
		return riskType3;
	}

	/**
	 * @param riskType3 the riskType3 to set
	 */
	public void setRiskType3(Integer riskType3) {
		this.riskType3 = riskType3;
	}

	/**
	 * @return the insuredValueType1
	 */
	public int getInsuredValueType1() {
		return insuredValueType1;
	}

	/**
	 * @param insuredValueType1 the insuredValueType1 to set
	 */
	public void setInsuredValueType1(int insuredValueType1) {
		this.insuredValueType1 = insuredValueType1;
	}

	/**
	 * @return the insuredValueType2
	 */
	public int getInsuredValueType2() {
		return insuredValueType2;
	}

	/**
	 * @param insuredValueType2 the insuredValueType2 to set
	 */
	public void setInsuredValueType2(int insuredValueType2) {
		this.insuredValueType2 = insuredValueType2;
	}

	/**
	 * @return the insuredValueType3
	 */
	public int getInsuredValueType3() {
		return insuredValueType3;
	}

	/**
	 * @param insuredValueType3 the insuredValueType3 to set
	 */
	public void setInsuredValueType3(int insuredValueType3) {
		this.insuredValueType3 = insuredValueType3;
	}

	/**
	 * @return the insuredValue1
	 */
	public Double getInsuredValue1() {
		return insuredValue1;
	}

	/**
	 * @param insuredValue1 the insuredValue1 to set
	 */
	public void setInsuredValue1(Double insuredValue1) {
		this.insuredValue1 = insuredValue1;
	}

	/**
	 * @return the insuredValue2
	 */
	public Double getInsuredValue2() {
		return insuredValue2;
	}

	/**
	 * @param insuredValue2 the insuredValue2 to set
	 */
	public void setInsuredValue2(Double insuredValue2) {
		this.insuredValue2 = insuredValue2;
	}

	/**
	 * @return the insuredValue3
	 */
	public Double getInsuredValue3() {
		return insuredValue3;
	}

	/**
	 * @param insuredValue3 the insuredValue3 to set
	 */
	public void setInsuredValue3(Double insuredValue3) {
		this.insuredValue3 = insuredValue3;
	}

	/**
	 * @return the rangeValueList1
	 */
	public List< CoverageRangeValue > getRangeValueList1() {
		return rangeValueList1;
	}

	/**
	 * @param rangeValueList1 the rangeValueList1 to set
	 */
	public void setRangeValueList1(List< CoverageRangeValue > rangeValueList1) {
		this.rangeValueList1 = rangeValueList1;
	}

	/**
	 * @return the rangeValueList2
	 */
	public List< CoverageRangeValue > getRangeValueList2() {
		return rangeValueList2;
	}

	/**
	 * @param rangeValueList2 the rangeValueList2 to set
	 */
	public void setRangeValueList2(List< CoverageRangeValue > rangeValueList2) {
		this.rangeValueList2 = rangeValueList2;
	}

	/**
	 * @return the rangeValueList3
	 */
	public List< CoverageRangeValue > getRangeValueList3() {
		return rangeValueList3;
	}

	/**
	 * @param rangeValueList3 the rangeValueList3 to set
	 */
	public void setRangeValueList3(List< CoverageRangeValue > rangeValueList3) {
		this.rangeValueList3 = rangeValueList3;
	}

	/**
	 * @return the rangeValueId1
	 */
	public int getRangeValueId1() {
		return rangeValueId1;
	}

	/**
	 * @param rangeValueId1 the rangeValueId1 to set
	 */
	public void setRangeValueId1(int rangeValueId1) {
		this.rangeValueId1 = rangeValueId1;
	}

	/**
	 * @return the rangeValueId2
	 */
	public int getRangeValueId2() {
		return rangeValueId2;
	}

	/**
	 * @param rangeValueId2 the rangeValueId2 to set
	 */
	public void setRangeValueId2(int rangeValueId2) {
		this.rangeValueId2 = rangeValueId2;
	}

	/**
	 * @return the rangeValueId3
	 */
	public int getRangeValueId3() {
		return rangeValueId3;
	}

	/**
	 * @param rangeValueId3 the rangeValueId3 to set
	 */
	public void setRangeValueId3(int rangeValueId3) {
		this.rangeValueId3 = rangeValueId3;
	}

	/**
	 * @return the coveragePlan1
	 */
	public CoveragePlan getCoveragePlan1() {
		return coveragePlan1;
	}

	/**
	 * @param coveragePlan1 the coveragePlan1 to set
	 */
	public void setCoveragePlan1(CoveragePlan coveragePlan1) {
		this.coveragePlan1 = coveragePlan1;
	}

	/**
	 * @return the coveragePlan2
	 */
	public CoveragePlan getCoveragePlan2() {
		return coveragePlan2;
	}

	/**
	 * @param coveragePlan2 the coveragePlan2 to set
	 */
	public void setCoveragePlan2(CoveragePlan coveragePlan2) {
		this.coveragePlan2 = coveragePlan2;
	}

	/**
	 * @return the coveragePlan3
	 */
	public CoveragePlan getCoveragePlan3() {
		return coveragePlan3;
	}

	/**
	 * @param coveragePlan3 the coveragePlan3 to set
	 */
	public void setCoveragePlan3(CoveragePlan coveragePlan3) {
		this.coveragePlan3 = coveragePlan3;
	}
	
}
