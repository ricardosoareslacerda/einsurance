package br.com.tratomais.core.model.product;

// Generated 11/02/2010 18:15:27 by Hibernate Tools 3.2.4.GA

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntityId;

/**
 * Represents PlanKinship ID
 * @author luiz.alberoni
 */
public class PlanKinshipId extends PersistentEntityId {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5702364359157924855L;
	private Date effectiveDate;
	private int renewalType;
	private int kinshipType;
	private int personalRiskType;
	private int planId;
	private int productId;

	public PlanKinshipId() {
	}

	public PlanKinshipId(Date effectiveDate, int renewalType, int kinshipType, int personalRiskType, int planId, int productId) {
		this.effectiveDate = effectiveDate;
		this.renewalType = renewalType;
		this.kinshipType = kinshipType;
		this.personalRiskType = personalRiskType;
		this.planId = planId;
		this.productId = productId;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public int getRenewalType() {
		return this.renewalType;
	}

	public void setRenewalType(int renewalType) {
		this.renewalType = renewalType;
	}

	public int getKinshipType() {
		return this.kinshipType;
	}

	public void setKinshipType(int kinshipType) {
		this.kinshipType = kinshipType;
	}

	public int getPersonalRiskType() {
		return this.personalRiskType;
	}

	public void setPersonalRiskType(int personalRiskType) {
		this.personalRiskType = personalRiskType;
	}

	public int getPlanId() {
		return this.planId;
	}

	public void setPlanId(int planId) {
		this.planId = planId;
	}

	public int getProductId() {
		return this.productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public boolean equals(Object other) {
		if ( ( this == other ) )
			return true;
		if ( ( other == null ) )
			return false;
		if ( !( other instanceof PlanKinshipId ) )
			return false;
		PlanKinshipId castOther = ( PlanKinshipId ) other;

		return ( ( this.getEffectiveDate() == castOther.getEffectiveDate() ) || ( this.getEffectiveDate() != null
				&& castOther.getEffectiveDate() != null && this.getEffectiveDate().equals( castOther.getEffectiveDate() ) ) )
				&& ( this.getRenewalType() == castOther.getRenewalType() )
				&& ( this.getKinshipType() == castOther.getKinshipType() )
				&& ( this.getPersonalRiskType() == castOther.getPersonalRiskType() )
				&& ( this.getPlanId() == castOther.getPlanId() )
				&& ( this.getProductId() == castOther.getProductId() );
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + ( getEffectiveDate() == null ? 0 : this.getEffectiveDate().hashCode() );
		result = 37 * result + this.getRenewalType();
		result = 37 * result + this.getKinshipType();
		result = 37 * result + this.getPersonalRiskType();
		result = 37 * result + this.getPlanId();
		result = 37 * result + this.getProductId();
		return result;
	}

}
