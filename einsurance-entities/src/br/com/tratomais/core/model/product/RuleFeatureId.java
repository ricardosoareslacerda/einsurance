package br.com.tratomais.core.model.product;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import br.com.tratomais.core.dao.PersistentEntity;

@Embeddable
public class RuleFeatureId implements PersistentEntity {
	
	private static final long serialVersionUID = 7071478073942340308L;

	@Column(name="RuleID", columnDefinition="Integer", nullable=false)
	private Integer ruleId;

	@Column(name="AttributeValue_1", columnDefinition="Integer", nullable=true)
	private Integer attributeValue_1;

	@Column(name="AttributeValue_2", columnDefinition="Integer", nullable=true)
	private Integer attributeValue_2;
	
	@Column(name="AttributeValue_3", columnDefinition="Integer", nullable=true)
	private Integer attributeValue_3;
	
	@Column(name="AttributeValue_4", columnDefinition="Integer", nullable=true)
	private Integer attributeValue_4;
	
	@Column(name="AttributeValue_5", columnDefinition="Integer", nullable=true)
	private Integer attributeValue_5;
	
	@Column(name="EffectiveDate", columnDefinition="Integer", nullable=true)
	private Date effectiveDate;

	/**
	 * @return the ruleId
	 */
	public Integer getRuleId() {
		return ruleId;
	}

	/**
	 * @param ruleId the ruleId to set
	 */
	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}

	/**
	 * @return the attributeValue_1
	 */
	public Integer getAttributeValue_1() {
		return attributeValue_1;
	}

	/**
	 * @param attributeValue_1 the attributeValue_1 to set
	 */
	public void setAttributeValue_1(Integer attributeValue_1) {
		this.attributeValue_1 = attributeValue_1;
	}

	/**
	 * @return the attributeValue_2
	 */
	public Integer getAttributeValue_2() {
		return attributeValue_2;
	}

	/**
	 * @param attributeValue_2 the attributeValue_2 to set
	 */
	public void setAttributeValue_2(Integer attributeValue_2) {
		this.attributeValue_2 = attributeValue_2;
	}

	/**
	 * @return the attributeValue_3
	 */
	public Integer getAttributeValue_3() {
		return attributeValue_3;
	}

	/**
	 * @param attributeValue_3 the attributeValue_3 to set
	 */
	public void setAttributeValue_3(Integer attributeValue_3) {
		this.attributeValue_3 = attributeValue_3;
	}

	/**
	 * @return the attributeValue_4
	 */
	public Integer getAttributeValue_4() {
		return attributeValue_4;
	}

	/**
	 * @param attributeValue_4 the attributeValue_4 to set
	 */
	public void setAttributeValue_4(Integer attributeValue_4) {
		this.attributeValue_4 = attributeValue_4;
	}

	/**
	 * @return the attributeValue_5
	 */
	public Integer getAttributeValue_5() {
		return attributeValue_5;
	}

	/**
	 * @param attributeValue_5 the attributeValue_5 to set
	 */
	public void setAttributeValue_5(Integer attributeValue_5) {
		this.attributeValue_5 = attributeValue_5;
	}

	/**
	 * @return the effectiveDate
	 */
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
}
