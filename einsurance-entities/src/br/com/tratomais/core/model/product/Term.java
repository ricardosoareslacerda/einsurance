package br.com.tratomais.core.model.product;

// Generated 30/12/2009 20:05:30 by Hibernate Tools 3.2.4.GA

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

/**
 * Term generated by hbm2java
 */
public class Term implements PersistentEntity {

	public static final int MULTIPLE_TYPE_YEAR = 60;
	public static final int MULTIPLE_TYPE_MONTH = 61;
	public static final int MULTIPLE_TYPE_DAY = 62;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8567143141208677456L;
	
	public static final int INVOICE_TYPE_FLEXIBLE = 824;
	
	private int termId;
	private String termCode;
	private String name;
	private int multipleType;
	private short multipleQuantity;
	private Boolean modifyPermit;
	private Date registred;
	private Date updated;

	public Term() {
	}

	public Term(int termId, String termCode, String name, int multipleType, short multipleQuantity, Date registred) {
		this.termId = termId;
		this.termCode = termCode;
		this.name = name;
		this.multipleType = multipleType;
		this.multipleQuantity = multipleQuantity;
		this.registred = registred;
	}

	public Term(int termId, String termCode, String name, int multipleType, short multipleQuantity, Boolean modifyPermit, Date registred, Date updated) {
		this.termId = termId;
		this.termCode = termCode;
		this.name = name;
		this.multipleType = multipleType;
		this.multipleQuantity = multipleQuantity;
		this.modifyPermit = modifyPermit;
		this.registred = registred;
		this.updated = updated;
	}

	public int getTermId() {
		return this.termId;
	}

	public void setTermId(int termId) {
		this.termId = termId;
	}

	public String getTermCode() {
		return this.termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMultipleType() {
		return this.multipleType;
	}

	public void setMultipleType(int multipleType) {
		this.multipleType = multipleType;
	}

	public short getMultipleQuantity() {
		return this.multipleQuantity;
	}

	public void setMultipleQuantity(short multipleQuantity) {
		this.multipleQuantity = multipleQuantity;
	}

	public Boolean getModifyPermit() {
		return this.modifyPermit;
	}

	public void setModifyPermit(Boolean modifyPermit) {
		this.modifyPermit = modifyPermit;
	}

	public Date getRegistred() {
		return this.registred;
	}

	public void setRegistred(Date registred) {
		this.registred = registred;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}
}
