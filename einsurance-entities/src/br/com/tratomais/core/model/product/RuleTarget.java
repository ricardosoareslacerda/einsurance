package br.com.tratomais.core.model.product;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.tratomais.core.dao.PersistentEntity;

@Entity
@Table(name="RuleTarget")
public class RuleTarget implements PersistentEntity {
	
	private static final long serialVersionUID = 5243461809757882209L;
	
	public static final int RULE_TYPE_CLAUSE = 635; 
	public static final int RULE_TYPE_COVERAGE = 727;
	public static final int RULE_TYPE_DEDUCTIBLE = 492;
	public static final int RULE_TYPE_PROFILE = 726;
	public static final int RULE_TYPE_PENDENCY = 653;
	public static final int RULE_TYPE_INSURED_VALUE = 491;
	public static final int RULE_TYPE_PAYMENT_TERM = 634;
	public static final int RULE_TYPE_PAYMENT_TYPE = 725;
	public static final int RULE_TYPE_CLAUSE_AUTOMATIC = 636; 
	
	public static final int RULE_ACTION_TYPE_BLOCKADE = 637;
	public static final int RULE_ACTION_TYPE_SUBSCRIBED = 638; 
	public static final int RULE_ACTION_TYPE_CLAUSE_INCLUDE = 639; 
	public static final int RULE_ACTION_TYPE_PENDENCY_INCLUDE = 640;

	@Id
	private RuleTargetId id;

	@Column(name="RuleType", columnDefinition="Integer", nullable=true)
	private Integer ruleType;

	@Column(name="ProductId", columnDefinition="Integer", nullable=false)
	private Integer productId;

	@Column(name="AttributeValue_1", columnDefinition="Integer", nullable=true)
	private Integer attributeValue_1;

	@Column(name="AttributeValue_2", columnDefinition="Integer", nullable=true)
	private Integer attributeValue_2;
	
	@Column(name="AttributeValue_3", columnDefinition="Integer", nullable=true)
	private Integer attributeValue_3;
	
	@Column(name="AttributeValue_4", columnDefinition="Integer", nullable=true)
	private Integer attributeValue_4;
	
	@Column(name="AttributeValue_5", columnDefinition="Integer", nullable=true)
	private Integer attributeValue_5;
	
	@Column(name="RuleActionType", columnDefinition="Integer", nullable=true)
	private Integer ruleActionType;
	
	@Column(name="Registred", columnDefinition="datetime", nullable=true)
	private Date registred;
	
	@Column(name="Updated", columnDefinition="datetime", nullable=true)
	private Date updated;

	/**
	 * @return the id
	 */
	public RuleTargetId getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(RuleTargetId id) {
		this.id = id;
	}

	/**
	 * @return the ruleType
	 */
	public Integer getRuleType() {
		return ruleType;
	}

	/**
	 * @param ruleType the ruleType to set
	 */
	public void setRuleType(Integer ruleType) {
		this.ruleType = ruleType;
	}

	/**
	 * @return the productId
	 */
	public Integer getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	/**
	 * @return the attributeValue_1
	 */
	public Integer getAttributeValue_1() {
		return attributeValue_1;
	}

	/**
	 * @param attributeValue_1 the attributeValue_1 to set
	 */
	public void setAttributeValue_1(Integer attributeValue_1) {
		this.attributeValue_1 = attributeValue_1;
	}

	/**
	 * @return the attributeValue_2
	 */
	public Integer getAttributeValue_2() {
		return attributeValue_2;
	}

	/**
	 * @param attributeValue_2 the attributeValue_2 to set
	 */
	public void setAttributeValue_2(Integer attributeValue_2) {
		this.attributeValue_2 = attributeValue_2;
	}

	/**
	 * @return the attributeValue_3
	 */
	public Integer getAttributeValue_3() {
		return attributeValue_3;
	}

	/**
	 * @param attributeValue_3 the attributeValue_3 to set
	 */
	public void setAttributeValue_3(Integer attributeValue_3) {
		this.attributeValue_3 = attributeValue_3;
	}

	/**
	 * @return the attributeValue_4
	 */
	public Integer getAttributeValue_4() {
		return attributeValue_4;
	}

	/**
	 * @param attributeValue_4 the attributeValue_4 to set
	 */
	public void setAttributeValue_4(Integer attributeValue_4) {
		this.attributeValue_4 = attributeValue_4;
	}

	/**
	 * @return the attributeValue_5
	 */
	public Integer getAttributeValue_5() {
		return attributeValue_5;
	}

	/**
	 * @param attributeValue_5 the attributeValue_5 to set
	 */
	public void setAttributeValue_5(Integer attributeValue_5) {
		this.attributeValue_5 = attributeValue_5;
	}

	/**
	 * @return the ruleActionType
	 */
	public Integer getRuleActionType() {
		return ruleActionType;
	}

	/**
	 * @param ruleActionType the ruleActionType to set
	 */
	public void setRuleActionType(Integer ruleActionType) {
		this.ruleActionType = ruleActionType;
	}

	/**
	 * @return the registred
	 */
	public Date getRegistred() {
		return registred;
	}

	/**
	 * @param registred the registred to set
	 */
	public void setRegistred(Date registred) {
		this.registred = registred;
	}

	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}

	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}

}
