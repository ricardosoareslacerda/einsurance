package br.com.tratomais.core.model.product;

import java.util.Collection;

@SuppressWarnings("unchecked")
public class ProposalListData {

	public ProposalListData() {

	}

	private Collection listPersonType;
	private Collection listGender;
	private Collection listMaritalStatus;
	private Collection listDocumentTypeNatural;
	private Collection listDocumentTypeLegally;
	private Collection listAddressType;
	private Collection listPhoneType;
	private Collection listProfessionType;
	private Collection listProfession;
	private Collection listKinshipType;
	private Collection listActivity;
	private Collection listOccupation;

	public Collection getListPersonType() {
		return listPersonType;
	}

	public void setListPersonType(Collection listPersonType) {
		this.listPersonType = listPersonType;
	}

	public Collection getListGender() {
		return listGender;
	}

	public void setListGender(Collection listGender) {
		this.listGender = listGender;
	}

	public Collection getListMaritalStatus() {
		return listMaritalStatus;
	}

	public void setListMaritalStatus(Collection listMaritalStatus) {
		this.listMaritalStatus = listMaritalStatus;
	}

	public Collection getListAddressType() {
		return listAddressType;
	}

	public void setListAddressType(Collection listAddressType) {
		this.listAddressType = listAddressType;
	}

	public Collection getListPhoneType() {
		return listPhoneType;
	}

	public void setListPhoneType(Collection listPhoneType) {
		this.listPhoneType = listPhoneType;
	}

	public Collection getListProfessionType() {
		return listProfessionType;
	}

	public void setListProfessionType(Collection listProfessionType) {
		this.listProfessionType = listProfessionType;
	}

	public Collection getListProfession() {
		return listProfession;
	}

	public void setListProfession(Collection listProfession) {
		this.listProfession = listProfession;
	}

	public Collection getListKinshipType() {
		return listKinshipType;
	}

	public void setListKinshipType(Collection listKinshipType) {
		this.listKinshipType = listKinshipType;
	}

	public Collection getListActivity() {
		return listActivity;
	}

	public void setListActivity(Collection listActivity) {
		this.listActivity = listActivity;
	}

	public Collection getListOccupation() {
		return listOccupation;
	}

	public void setListOccupation(Collection listOccupation) {
		this.listOccupation = listOccupation;
	}

	public Collection getListDocumentTypeNatural() {
		return listDocumentTypeNatural;
	}

	public void setListDocumentTypeNatural(Collection listDocumentTypeNatural) {
		this.listDocumentTypeNatural = listDocumentTypeNatural;
	}

	public Collection getListDocumentTypeLegally() {
		return listDocumentTypeLegally;
	}

	public void setListDocumentTypeLegally(Collection listDocumentTypeLegally) {
		this.listDocumentTypeLegally = listDocumentTypeLegally;
	}
}
