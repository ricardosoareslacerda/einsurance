package br.com.tratomais.core.model.product;

import br.com.tratomais.core.dao.PersistentEntity;

public class PersonalRiskOption implements PersistentEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int personType;
	private String personTypeValue;
	private String personTypeDescription;
	private short quantityPersonalRisk;
	private short quantityAdditional;
	private boolean additionalNewborn;

	public PersonalRiskOption(int personType,
							  String personTypeValue,
							  String personTypeDescription,
							  short quantityPersonalRisk,
							  short quantityAdditional,
							  boolean additionalNewborn) {
		super();
		this.personType = personType;
		this.personTypeValue = personTypeValue;
		this.personTypeDescription = personTypeDescription;
		this.quantityPersonalRisk = quantityPersonalRisk;
		this.quantityAdditional = quantityAdditional;
		this.additionalNewborn = additionalNewborn;
	}
	
	public PersonalRiskOption() {
		// TODO Auto-generated constructor stub
	}

	public int getPersonType() {
		return personType;
	}
	
	public void setPersonType(int personType) {
		this.personType = personType;
	}
	
	public String getPersonTypeValue() {
		return personTypeValue;
	}

	public void setPersonTypeValue(String personTypeValue) {
		this.personTypeValue = personTypeValue;
	}

	public String getPersonTypeDescription() {
		return personTypeDescription;
	}
	
	public void setPersonTypeDescription(String personTypeDescription) {
		this.personTypeDescription = personTypeDescription;
	}
	
	public short getQuantityPersonalRisk() {
		return this.quantityPersonalRisk;
	}

	public void setQuantityPersonalRisk(short quantityPersonalRisk) {
		this.quantityPersonalRisk = quantityPersonalRisk;
	}

	public short getQuantityAdditional() {
		return this.quantityAdditional;
	}

	public void setQuantityAdditional(short quantityAdditional) {
		this.quantityAdditional = quantityAdditional;
	}

	public boolean isAdditionalNewborn() {
		return this.additionalNewborn;
	}

	public void setAdditionalNewborn(boolean additionalNewborn) {
		this.additionalNewborn = additionalNewborn;
	}

}
