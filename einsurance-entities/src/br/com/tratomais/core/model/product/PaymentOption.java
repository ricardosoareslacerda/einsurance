package br.com.tratomais.core.model.product;

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

public class PaymentOption implements PersistentEntity {

	private static final long serialVersionUID = 7584375132198391065L;
	
	/**
	 * Tipo de prazo de pagamento
	 */
	public static final int PAYMENT_TERM_TYPE_POLICY		= 174;	// Ap�lice
	public static final int PAYMENT_TERM_TYPE_ENDORSEMENT 	= 175;	// Endosso
	public static final int PAYMENT_TERM_TYPE_ALL 			= 176;	// Todos
	public static final int PAYMENT_TERM_TYPE_INVOICE 		= 781;	// Fatura

	private String paymentTermName;
	private String paymentDescription;
	private double firstInstallmentValue;
	private double nextInstallmentValue;
	private double totalPremium;
	private double netPremium;
	private double policyCost;
	private double fractioningAdditional;
	private double taxRate;
	private double taxValue;
	private boolean isDefault;
	
	private int billingMethodId;
	private int paymentType;
	
	private Integer bankNumber;
	private String bankAgencyNumber;
	private String bankAccountNumber;
	private String bankCheckNumber;
	private Integer cardType;
	private Integer cardBrandType;
	private String cardNumber;
	private Integer cardExpirationDate;
	private String cardHolderName;
	private Integer invoiceNumber;
	private String otherAccountNumber;
	private Long debitAuthorizationCode;
	private Long creditAuthorizationCode;
	
	private BillingMethod billingMethod;
	
	private int paymentId;
	
	private int quantityInstallment;
	private Short nextInstallment;
	private boolean selected;
	private Date nextPaymentDate;
	private Short dueDay;
	private Short displayOrder;

	public String getPaymentTermName() {
		return paymentTermName;
	}

	public void setPaymentTermName(String paymentTermName) {
		this.paymentTermName = paymentTermName;
	}

	public String getPaymentDescription() {
		return paymentDescription;
	}

	public void setPaymentDescription(String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	public double getFirstInstallmentValue() {
		return firstInstallmentValue;
	}

	public void setFirstInstallmentValue(double firstInstallmentValue) {
		this.firstInstallmentValue = firstInstallmentValue;
	}

	public double getNextInstallmentValue() {
		return nextInstallmentValue;
	}

	public void setNextInstallmentValue(double nextInstallmentValue) {
		this.nextInstallmentValue = nextInstallmentValue;
	}

	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	public int getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public double getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(double totalPremium) {
		this.totalPremium = totalPremium;
	}

	public double getNetPremium() {
		return netPremium;
	}

	public void setNetPremium(double netPremium) {
		this.netPremium = netPremium;
	}

	public void setPolicyCost(double policyCost) {
		this.policyCost = policyCost;
	}

	public double getPolicyCost() {
		return policyCost;
	}

	public double getFractioningAdditional() {
		return fractioningAdditional;
	}

	public void setFractioningAdditional(double fractioningAdditional) {
		this.fractioningAdditional = fractioningAdditional;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public void setTaxValue(double taxValue) {
		this.taxValue = taxValue;
	}

	public double getTaxValue() {
		return taxValue;
	}

	public int getBillingMethodId() {
		return billingMethodId;
	}

	public void setBillingMethodId(int billingMethodId) {
		this.billingMethodId = billingMethodId;
	}

	public int getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(int paymentType) {
		this.paymentType = paymentType;
	}

	public Integer getBankNumber() {
		return bankNumber;
	}

	public void setBankNumber(Integer bankNumber) {
		this.bankNumber = bankNumber;
	}

	public String getBankAgencyNumber() {
		return bankAgencyNumber;
	}

	public void setBankAgencyNumber(String bankAgencyNumber) {
		this.bankAgencyNumber = bankAgencyNumber;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getBankCheckNumber() {
		return bankCheckNumber;
	}

	public void setBankCheckNumber(String bankCheckNumber) {
		this.bankCheckNumber = bankCheckNumber;
	}

	public Integer getCardType() {
		return cardType;
	}

	public void setCardType(Integer cardType) {
		this.cardType = cardType;
	}

	public Integer getCardBrandType() {
		return cardBrandType;
	}

	public void setCardBrandType(Integer cardBrandType) {
		this.cardBrandType = cardBrandType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Integer getCardExpirationDate() {
		return cardExpirationDate;
	}

	public void setCardExpirationDate(Integer cardExpirationDate) {
		this.cardExpirationDate = cardExpirationDate;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public Integer getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(Integer invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getOtherAccountNumber() {
		return otherAccountNumber;
	}

	public void setOtherAccountNumber(String otherAccountNumber) {
		this.otherAccountNumber = otherAccountNumber;
	}

	public Long getDebitAuthorizationCode() {
		return debitAuthorizationCode;
	}

	public void setDebitAuthorizationCode(Long debitAuthorizationCode) {
		this.debitAuthorizationCode = debitAuthorizationCode;
	}

	public Long getCreditAuthorizationCode() {
		return creditAuthorizationCode;
	}

	public void setCreditAuthorizationCode(Long creditAuthorizationCode) {
		this.creditAuthorizationCode = creditAuthorizationCode;
	}

	public BillingMethod getBillingMethod() {
		return billingMethod;
	}

	public void setBillingMethod(BillingMethod billingMethod) {
		this.billingMethod = billingMethod;
	}

	/**
	 * @return the paymentTimes
	 */
	public int getQuantityInstallment() {
		return quantityInstallment;
	}

	/**
	 * @param paymentTimes the paymentTimes to set
	 */
	public void setQuantityInstallment(int paymentTimes) {
		this.quantityInstallment = paymentTimes;
	}

	/**
	 * @return the nextInstallment
	 */
	public Short getNextInstallment() {
		return nextInstallment;
	}

	/**
	 * @param nextInstallment the nextInstallment to set
	 */
	public void setNextInstallment(Short nextInstallment) {
		this.nextInstallment = nextInstallment;
	}

	public void setNextPaymentDate(Date nextPaymentDate) {
		this.nextPaymentDate = nextPaymentDate;
	}

	public Date getNextPaymentDate() {
		return nextPaymentDate;
	}

	public void setDueDay(Short dueDay) {
		this.dueDay = dueDay;
	}

	public Short getDueDay() {
		return dueDay;
	}

	public Short getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Short displayOrder) {
		this.displayOrder = displayOrder;
	}
}
