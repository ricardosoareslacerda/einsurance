package br.com.tratomais.core.model.product;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.tratomais.core.dao.PersistentEntity;

@Entity
@Table(name="RuleFeature")
public class RuleFeature implements PersistentEntity {
	
	private static final long serialVersionUID = 7071478073942340308L;

	@Id
	private RuleFeatureId id;

	@Column(name="ExpiryDate", columnDefinition="Integer", nullable=true)
	private Date expiryDate;
	
	@Column(name="Registred", columnDefinition="datetime", nullable=true)
	private Date registred;
	
	@Column(name="Updated", columnDefinition="datetime", nullable=true)
	private Date updated;

	/**
	 * @return the id
	 */
	public RuleFeatureId getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(RuleFeatureId id) {
		this.id = id;
	}

	/**
	 * @return the expiryDate
	 */
	public Date getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expiryDate the expiryDate to set
	 */
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * @return the registred
	 */
	public Date getRegistred() {
		return registred;
	}

	/**
	 * @param registred the registred to set
	 */
	public void setRegistred(Date registred) {
		this.registred = registred;
	}

	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}

	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}

}
