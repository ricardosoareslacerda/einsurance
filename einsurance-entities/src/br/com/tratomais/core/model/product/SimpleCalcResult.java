package br.com.tratomais.core.model.product;

import java.util.ArrayList;
import java.util.List;

import br.com.tratomais.core.model.policy.Endorsement;

public class SimpleCalcResult {

	private Endorsement endorsement;
	private List<PaymentOption> paymentOptions = new ArrayList<PaymentOption>();
	private List<String> erros = new ArrayList<String>();

	public SimpleCalcResult(Endorsement endorsement, List<PaymentOption> paymentOptions, List<String> erros) {
		this.endorsement = endorsement;
		this.paymentOptions = paymentOptions;
		this.erros = erros;
	}

	public SimpleCalcResult() {
	}

	public Endorsement getEndorsement() {
		return endorsement;
	}

	public void setEndorsement(Endorsement endorsement) {
		this.endorsement = endorsement;
	}

	public List<String> getErros() {
		return erros;
	}

	public void setErros(List<String> erros) {
		this.erros = erros;
	}
	
	public void addStackTraceToErroList(StackTraceElement [] stackTraceElements) {
		if (this.erros == null)
			erros = new ArrayList<String>();
		for (StackTraceElement stackTraceElement : stackTraceElements)
			erros.add(stackTraceElement.toString() );
	}
	
	

	public List<PaymentOption> getPaymentOptions() {
		return paymentOptions;
	}

	public void setPaymentOptions(List<PaymentOption> paymentOptions) {
		this.paymentOptions = paymentOptions;
	}
}
