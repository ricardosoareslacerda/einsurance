package br.com.tratomais.web.listeners;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.springframework.web.context.support.WebApplicationContextUtils;

import br.com.tratomais.core.calculus.chron.Chron;
import br.com.tratomais.core.calculus.chron.ParseException;

public class CronListener implements ServletContextListener{
	private Logger logger = Logger.getLogger(CronListener.class);
	
	/** {@inheritDoc} */
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	/** {@inheritDoc} */
	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		String arquivoConfiguracao = servletContextEvent.getServletContext().getInitParameter("cron-configuration-file");
		InputStream inputStream = servletContextEvent.getServletContext().getResourceAsStream(arquivoConfiguracao);
		Chron chron = Chron.getSingleton(WebApplicationContextUtils.getRequiredWebApplicationContext(servletContextEvent.getServletContext()));
		try {
			chron.loadConfiguration(inputStream);
			chron.start();
		} catch (IOException e) {
			String msg = "Error en la configuración del Cron"; 
//				SpringSessionUtil.getMessage("CRON_CONFIGURATION_FILE_NOT_FOUND", new Object[] {arquivoConfiguracao});
			if (msg == null)
				msg = "Error reading file " + arquivoConfiguracao;
			logger.error(msg, e);
		} catch (ParseException e) {
			String msg = "Error durante el parsing del archivo de configuración"; 
//				SpringSessionUtil.getMessage("CRON_CONFIGURATION_FILE_PARSING", new Object[] {arquivoConfiguracao});
			if (msg == null)
				msg = "Error parsing Cron configuration file: " + arquivoConfiguracao;
			logger.error(msg, e);
		} catch (ClassNotFoundException e) {
			String msg = "Error - Clase no encontrada";
//				SpringSessionUtil.getMessage("CRON_CONFIGURATION_CLASS_NOT_FOUND", new Object[] {arquivoConfiguracao});
			if (msg == null)
				msg = "Class not found exception: " + arquivoConfiguracao;
			logger.error(msg, e);
		}
	}

}
