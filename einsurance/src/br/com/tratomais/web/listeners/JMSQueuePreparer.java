package br.com.tratomais.web.listeners;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import br.com.tratomais.core.dao.IDaoChannel;
import br.com.tratomais.core.dao.IDaoDataOption;
import br.com.tratomais.core.dao.IDaoPartner;
import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.dao.IDaoProduct;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Customer;
import br.com.tratomais.core.model.customer.Phone;
import br.com.tratomais.core.model.policy.Contract;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.Installment;
import br.com.tratomais.core.model.product.Product;
import br.com.tratomais.core.util.SpringSessionUtil;
import br.com.tratomais.core.util.datatrans.ECollectionJMSTransmitter;
import br.com.tratomais.core.util.jms.EInsuranceJMSException;

/**
 * Inicializa os objetos que devem ser enviados
 * @author luiz.alberoni
 */
public class JMSQueuePreparer implements ServletContextListener{
	/** Transmite as mensagens JMS */
	private ECollectionJMSTransmitter collectionJMSTransmitter;

	private IDaoProduct daoProduct;
	private IDaoChannel daoChannel;
	private IDaoDataOption daoDataOption;
	private IDaoPartner daoPartner;
	private IDaoPolicy daoPolicy;

	/** Logger */
	private Logger logger = Logger.getLogger(JMSQueuePreparer.class);

	/** {@inheritDoc} */
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	/** Inicializa os daos a serem utilizados */
	private void initialize(ServletContext servletContext){
		/* Usamos os daos diretamente do spring */
		daoProduct = (IDaoProduct) SpringSessionUtil.getBean(servletContext, "daoProduct");
		daoChannel = (IDaoChannel) SpringSessionUtil.getBean(servletContext, "daoChannel");
		daoDataOption = (IDaoDataOption) SpringSessionUtil.getBean(servletContext, "daoDataOption");
		daoPartner = (IDaoPartner) SpringSessionUtil.getBean(servletContext, "daoPartner");
		daoPolicy = (IDaoPolicy) SpringSessionUtil.getBean(servletContext, "daoPolicy");
	}
	
	/** {@inheritDoc} */
	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		initialize(servletContextEvent.getServletContext());
		
		List<Endorsement> dados = daoPolicy.listEndorsementForEmission();

		if (dados != null){
			try {
				collectionJMSTransmitter = ECollectionJMSTransmitter.getInstance();
			} catch (EInsuranceJMSException e) {
				logger.error("Problem preparing Collection transmition Step - Objects could not be send ",e);
				e.printStackTrace();
			}
			
			for(Endorsement endorsement : dados){
				colocaListaEnvio(endorsement);
			}
		}
		collectionJMSTransmitter.start();
	}

	/** Prepara lista para envio */
	private void colocaListaEnvio(Endorsement endorsement) {
		Contract contract = endorsement.getContract();
		LinkedList<Installment> listRegister = new LinkedList<Installment>();
		LinkedList<Installment> listCancellation = new LinkedList<Installment>();
		Customer policyHolder = endorsement.getCustomerByPolicyHolderId();
		Phone phone = policyHolder.getFirstAddress().getFirstPhone();
		Channel channel = daoChannel.findById(endorsement.getChannelId());
		Product product = daoProduct.findById(contract.getProductId());
		for(Installment ins : endorsement.getInstallments()){
			if (! ins.isShippingCollection()){
				listRegister.add(ins);
				if (ins.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_ANNULMENT){
					listCancellation.add(ins);
				}
			}
		}
		if (listRegister.size() > 0){
			this.collectionJMSTransmitter.fillAndAdd(
					listRegister, 
					daoPartner.findById(contract.getPartnerId()), 
					daoDataOption.findById(policyHolder.getPersonType()), 
					daoDataOption.findById(phone.getPhoneType()), 
					channel, 
					product,
					false);
		}
		if (listCancellation.size() > 0){
			this.collectionJMSTransmitter.fillAndAdd(
					listCancellation, 
					daoPartner.findById(contract.getPartnerId()), 
					daoDataOption.findById(policyHolder.getPersonType()), 
					daoDataOption.findById(phone.getPhoneType()), 
					channel, 
					product,
					true);
		}
	}
}
