package br.com.tratomais.core.report;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.tratomais.core.service.impl.ReportService;

@Controller
public class ReportEinsurance {
	private static final Logger logger = Logger.getLogger( ReportEinsurance.class );

	@Resource(name="reportService")
	public ReportService serviceReport;
	
	//public void setServiceReport(ReportService reportService) {
	//	this.serviceReport = reportService;
	//}
	/*HttpServletRequest request,
	HttpServletResponse response,
	@RequestParam("reportOutput") String reportOutput,
	@RequestParam("exportReportFormat") String exportReportFormat,
	*/
	@SuppressWarnings({ "unchecked", "deprecation" })
	@RequestMapping("/viewReport")
	public String report(@RequestParam("id") Long id, 
			HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam("reportOutput") String reportOutput,
			ModelMap model) {

		
		boolean showReport = true;
		if (id == null) return "Problema id";
		if (request == null) return "Problema request";
		if (response == null) return "Problema response";
		//if (exportReportFormat == null) return "Problema exportReportFormat";

		ReportTO rto = serviceReport.getReport(id);
	    String fileName = rto.getReportFileName();
	    //find the absolute path using the relative reference... 
	    if (fileName != null && fileName.indexOf("#CLASS_PATH#")>=0) {
	        fileName = fileName.replaceAll("#CLASS_PATH#", "");
	        rto.setReportFileName(request.getRealPath(fileName));
	    }
	    
		if (reportOutput != null){
			rto.setExportReportFormat(reportOutput);
		} else {
			return "Problema reportOutput";
		}	    
	    
	  //create a list of fields from form with selected values and set into ReportTO
        List formFields = new ArrayList();
	    List fields = serviceReport.getReportFields(rto.getSqlStement());
	    if (fields!=null) {
            Iterator i = fields.iterator();
            while(i.hasNext()) {
                ReportFieldTO fieldTO = (ReportFieldTO)i.next();
                String value = request.getParameter(fieldTO.getFieldToHtml(rto));
                //if (value!=null) { 
                    if (!value.equals("") || fieldTO.getReportFieldType().equals(ReportFieldTO.TYPE_OBJECT)) {
		                fieldTO.setValue(value);
		                formFields.add(fieldTO);	                    
	              //  } else {
	                	//this.setErrorFormSession(request, "error.viewReport.mandatoryFields", null);
	                //	showReport = false;
	                //}
                }
            }
            rto.setFormFieldsValues(formFields);
        }
	    rto.setLocale(request.getLocale());
        rto.setHandler("einsurance");      

        if (showReport) {
        	try {
        		String pathReports = request.getRealPath(rto.getProfile());
        		byte[] reportStream = serviceReport.performJasperReport(rto, pathReports);
        		if (reportStream!=null) {
        			if (reportOutput.equals(ReportTO.REPORT_EXPORT_PDF)) {
        				response.setContentType("application/pdf");
        				response.setHeader("content-disposition", "inline;filename=report.pdf");
        			} else if (reportOutput.equals(ReportTO.REPORT_EXPORT_RTF)) {
        				response.setContentType("application/rtf");
        				response.setHeader("content-disposition", "inline;filename=report.rtf");
        			} else if (reportOutput.equals(ReportTO.REPORT_EXPORT_ODT)) {	
        				response.setContentType("application/vnd.oasis.opendocument.text");
        				response.setHeader("content-disposition", "inline;filename=report.odt");
        			} else if (reportOutput.equals(ReportTO.REPORT_EXPORT_EXCEL)) {	
        				response.setContentType("application/vnd.ms-excel");  
        				response.setHeader("content-disposition", "inline;filename=report.xls");
        			}	    		
        			response.setContentLength(reportStream.length); 
        			OutputStream ouputStream = response.getOutputStream(); 
        			ouputStream.write(reportStream);
        			ouputStream.flush(); 
        			ouputStream.close();
        		}
	        } catch (Exception e) {
	        	e.printStackTrace();
	        }
        }
		return null;
	}
}
