<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="CoverageProperty" pageWidth="612" pageHeight="792" columnWidth="525" leftMargin="0" rightMargin="0" topMargin="5" bottomMargin="0">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="css - default" isDefault="true" forecolor="#000000" backcolor="#FFFFFF" hAlign="Left" vAlign="Middle" isBlankWhenNull="true" fontName="Arial" fontSize="11" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false"/>
	<style name="css - title" isDefault="false" forecolor="#000000" backcolor="#FFFFFF" hAlign="Center" vAlign="Middle" isBlankWhenNull="true" fontName="Arial" fontSize="11" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false"/>
	<parameter name="EndorsementID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="ContractID" class="java.lang.Integer"/>
	<parameter name="ItemID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="CurrencySymbol" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT CoveragePlan.NickName as CoveragePlanNickName
     , (ItemCoverage.InsuredValue / isnull(Endorsement.ConversionRate, 1.0)) as InsuredValue
     , (ItemCoverage.TotalPremium / isnull(Endorsement.ConversionRate, 1.0)) as TotalPremium
     , CoveragePlan.CoverageID
     , CoveragePlan.DisplayOrder
     , CoveragePlan.InsuredValueType
     , isnull(CoveragePlan.PrintGroup, 999999) as CoverageRiskType
     , isnull(RiskType.Description, 'Adicional') as RiskType
     , CoverageRangeValue.Description as CoverageRangeDescription
     FROM Item
LEFT JOIN Contract
       ON item.ContractID = Contract.ContractID
LEFT JOIN Endorsement
       ON Contract.ContractID = Endorsement.ContractID
      AND Item.EndorsementID  = Endorsement.EndorsementID
LEFT JOIN ItemCoverage
       ON Item.ContractID = ItemCoverage.ContractID
      AND Item.EndorsementID  = ItemCoverage.EndorsementID
      AND Item.ItemID = ItemCoverage.ItemID
LEFT JOIN CoveragePlan
       ON Contract.ProductID = CoveragePlan.ProductID
      AND Item.CoveragePlanID = CoveragePlan.PlanID
      AND ItemCoverage.CoverageID = CoveragePlan.CoverageID
      AND Endorsement.ReferenceDate BETWEEN CoveragePlan.EffectiveDate and CoveragePlan.ExpiryDate
LEFT JOIN CoverageRangeValue
       ON Contract.ProductID = CoverageRangeValue.ProductID
      AND Item.CoveragePlanID = CoverageRangeValue.PlanID
      AND ItemCoverage.CoverageID = CoverageRangeValue.CoverageID
      AND ItemCoverage.RangeValueID = CoverageRangeValue.RangeID
      AND Endorsement.ReferenceDate BETWEEN CoverageRangeValue.EffectiveDate and CoverageRangeValue.ExpiryDate
LEFT JOIN DataOption AS RiskType
       ON CoveragePlan.PrintGroup = RiskType.DataOptionID
    WHERE Item.ContractID = $P{ContractID}
      AND Item.EndorsementID = $P{EndorsementID}
      AND Item.ItemID = $P{ItemID}
 ORDER BY CoverageRiskType, CoveragePlan.DisplayOrder]]>
	</queryString>
	<field name="CoveragePlanNickName" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="InsuredValue" class="java.lang.Double">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="TotalPremium" class="java.lang.Double">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="CoverageID" class="java.lang.Integer">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="DisplayOrder" class="java.lang.Integer">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="InsuredValueType" class="java.lang.Integer">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="CoverageRiskType" class="java.lang.Integer">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="RiskType" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="CoverageRangeDescription" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<variable name="RiskTypeGroup" class="java.lang.Integer" resetType="Column">
		<variableExpression><![CDATA[$F{CoverageRiskType}]]></variableExpression>
	</variable>
	<group name="RiskGroup">
		<groupExpression><![CDATA[$F{CoverageRiskType}]]></groupExpression>
		<groupHeader>
			<band height="13">
				<textField>
					<reportElement style="css - default" x="2" y="0" width="100" height="13"/>
					<textElement>
						<font isBold="true"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{RiskType}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="30">
			<textField>
				<reportElement style="css - title" x="0" y="0" width="254" height="13"/>
				<textElement textAlignment="Left">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[($V{RiskGroup_COUNT}.intValue() != 0)? "COBERTURAS (continuação)" : "COBERTURAS"]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="14" width="525" height="1"/>
			</line>
			<staticText>
				<reportElement style="css - default" x="0" y="16" width="80" height="13"/>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Descrição]]></text>
			</staticText>
			<textField>
				<reportElement style="css - default" x="299" y="18" width="155" height="13"/>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[($P{CurrencySymbol} != null)? "LMI (" + $P{CurrencySymbol} + ")" : "LMI"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="css - default" x="454" y="16" width="70" height="13"/>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[($P{CurrencySymbol} != null)? "Prêmio (" + $P{CurrencySymbol} + ")" : "Prêmio"]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="-1" y="29" width="525" height="1"/>
			</line>
		</band>
	</pageHeader>
	<detail>
		<band height="13" splitType="Stretch">
			<textField isStretchWithOverflow="true">
				<reportElement style="css - default" x="6" y="0" width="240" height="13"/>
				<textElement/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{CoveragePlanNickName}]]></textFieldExpression>
			</textField>
			<textField pattern="">
				<reportElement style="css - default" positionType="Float" x="454" y="0" width="70" height="13"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.lang.String"><![CDATA[new DecimalFormat("#,##0.00").format($F{TotalPremium})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="css - default" positionType="Float" x="246" y="0" width="208" height="13"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.lang.String"><![CDATA[($F{InsuredValueType}.intValue() == 166)? "Contratada" :
($F{InsuredValueType}.intValue() == 207)? $F{CoverageRangeDescription} : new DecimalFormat("#,##0.00").format($F{InsuredValue})]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
