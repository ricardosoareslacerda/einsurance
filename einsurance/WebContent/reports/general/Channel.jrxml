<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Channel" pageWidth="792" pageHeight="612" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="708" leftMargin="42" rightMargin="42" topMargin="14" bottomMargin="28">
	<property name="ireport.zoom" value="2.5937424601000023"/>
	<property name="ireport.x" value="1316"/>
	<property name="ireport.y" value="0"/>
	<queryString>
		<![CDATA[SELECT Product.ProductID
      , Product.Name AS ProductName
      , Endorsement.EndorsedID
      , Endorsement.EndorsementID
      , Endorsement.EffectiveDate
      , Endorsement.ExpiryDate
      , Customer.Name AS CustomerName
      , Channel.ChannelID
      , Channel.Name as ChannelName
      , IssuingStatus.FieldDescription AS IssuingStatus
      , Installment.InstallmentValue
      , Currency.Symbol AS CurrencySymbol
      , Endorsement.TotalPremium
      , Contract.ContractID
      , Contract.CertificateNumber
    FROM Contract
    JOIN Product
      ON Contract.ProductID = Product.ProductID
    JOIN Endorsement
      ON Contract.ContractID = Endorsement.ContractID
    JOIN Channel
      ON Endorsement.ChannelID = Channel.ChannelID
    JOIN Installment
      ON Endorsement.EndorsementID = Installment.EndorsementID
     AND Endorsement.ContractID = Installment.ContractID
    JOIN Customer
      ON Endorsement.InsuredID = Customer.CustomerID
    JOIN Currency
      ON Product.CurrencyID = Currency.CurrencyID
    JOIN vw_DataOption AS IssuingStatus
      ON Endorsement.IssuingStatus = IssuingStatus.DataOptionID
   WHERE Contract.PartnerID = 1
     AND Endorsement.ChannelID = 2
     AND Endorsement.IssuingStatus = 106
     AND Endorsement.EffectiveDate BETWEEN '2009-01-01' AND '2010-01-31'
     AND Installment.InstallmentID = 1
ORDER BY Channel.ChannelID, Contract.CertificateNumber, Contract.ContractID, Endorsement.EndorsementID]]>
	</queryString>
	<field name="ProductName" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="EndorsementID" class="java.lang.Integer">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="EffectiveDate" class="java.sql.Timestamp">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="ExpiryDate" class="java.sql.Timestamp">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="CustomerName" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="ChannelID" class="java.lang.Integer">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="ChannelName" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="IssuingStatus" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="InstallmentValue" class="java.lang.Double">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="CurrencySymbol" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="TotalPremium" class="java.lang.Double">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="ContractID" class="java.lang.Integer"/>
	<field name="CertificateNumber" class="java.lang.Double"/>
	<field name="ParamChannelID" class="java.lang.Integer"/>
	<field name="ParamEffectiveDateIni" class="java.sql.Timestamp"/>
	<field name="ParamEffectiveDateEnd" class="java.sql.Timestamp"/>
	<field name="ParamIssuingStatus" class="java.lang.Integer"/>
	<field name="PolicyNumber" class="java.lang.Double"/>
	<field name="ChannelCode" class="java.lang.String"/>
	<variable name="totalPrima" class="java.lang.Double" resetType="Group" resetGroup="totalgroup" calculation="Sum">
		<variableExpression><![CDATA[$F{TotalPremium}]]></variableExpression>
		<initialValueExpression><![CDATA[( $V{totalPrima} != null ? $V{totalPrima} : new Double(0))]]></initialValueExpression>
	</variable>
	<variable name="totalCota" class="java.lang.Double" resetType="Group" resetGroup="totalgroup" calculation="Sum">
		<variableExpression><![CDATA[$F{InstallmentValue}]]></variableExpression>
		<initialValueExpression><![CDATA[( $V{totalCota} != null ? $V{totalCota} : new Double(0))]]></initialValueExpression>
	</variable>
	<variable name="totalGeralCota" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[$F{InstallmentValue}]]></variableExpression>
		<initialValueExpression><![CDATA[( $V{totalGeralCota} != null ? $V{totalGeralCota} : new Double(0))]]></initialValueExpression>
	</variable>
	<variable name="totalGeralPrima" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[$F{TotalPremium}]]></variableExpression>
		<initialValueExpression><![CDATA[( $V{totalGeralPrima} != null ? $V{totalGeralPrima} : new Double(0))]]></initialValueExpression>
	</variable>
	<group name="totalgroup">
		<groupExpression><![CDATA[$F{ChannelID}]]></groupExpression>
		<groupHeader>
			<band height="11">
				<textField isBlankWhenNull="true">
					<reportElement mode="Transparent" x="2" y="0" width="180" height="10" backcolor="#D9DFEB"/>
					<textElement verticalAlignment="Middle">
						<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{ChannelName}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="29">
				<printWhenExpression><![CDATA[new Boolean($V{totalgroup_COUNT}.intValue() != 0)]]></printWhenExpression>
				<textField>
					<reportElement x="520" y="2" width="85" height="9"/>
					<textElement textAlignment="Right" markup="none">
						<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[($V{totalgroup_COUNT} != null)? "Cantidad:" : ""]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="520" y="11" width="85" height="9"/>
					<textElement textAlignment="Right" markup="none">
						<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA["Total 1ra Cuota (Bs):"]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="520" y="20" width="85" height="9"/>
					<textElement textAlignment="Right" markup="none">
						<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA["Total Prima (Bs):"]]></textFieldExpression>
				</textField>
				<textField evaluationTime="Group" evaluationGroup="totalgroup" pattern="###0" isBlankWhenNull="true">
					<reportElement x="605" y="2" width="100" height="9"/>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
					</textElement>
					<textFieldExpression class="java.lang.Integer"><![CDATA[$V{totalgroup_COUNT}]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement x="605" y="11" width="100" height="9"/>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[new DecimalFormat("#,##0.00").format($V{totalCota})]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement x="605" y="20" width="100" height="9"/>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[new DecimalFormat("#,##0.00").format($V{totalPrima})]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="14">
			<staticText>
				<reportElement x="0" y="0" width="708" height="10"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Times New Roman" size="8" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<text><![CDATA[Reporte - Canal de Venta]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="11">
			<staticText>
				<reportElement mode="Opaque" x="133" y="0" width="35" height="10" forecolor="#FFFFFF" backcolor="#30579F"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<text><![CDATA[Contrato]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="200" y="0" width="140" height="10" forecolor="#FFFFFF" backcolor="#30579F"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<text><![CDATA[Asegurado]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="341" y="0" width="95" height="10" forecolor="#FFFFFF" backcolor="#30579F"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<text><![CDATA[Producto]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="518" y="0" width="55" height="10" forecolor="#FFFFFF" backcolor="#30579F"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<text><![CDATA[Vigencia desde]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement mode="Opaque" x="574" y="0" width="65" height="10" forecolor="#FFFFFF" backcolor="#30579F"/>
				<textElement textAlignment="Center" verticalAlignment="Middle" markup="none">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["1ra Cuota (Bs)"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Opaque" x="437" y="0" width="80" height="10" forecolor="#FFFFFF" backcolor="#30579F"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<text><![CDATA[Situación]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="169" y="0" width="30" height="10" forecolor="#FFFFFF" backcolor="#30579F"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<text><![CDATA[Emisión]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement mode="Opaque" x="640" y="0" width="65" height="10" forecolor="#FFFFFF" backcolor="#30579F"/>
				<textElement textAlignment="Center" verticalAlignment="Middle" markup="none">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[("Prima (Bs)")]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Opaque" x="92" y="0" width="40" height="10" forecolor="#FFFFFF" backcolor="#30579F"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<text><![CDATA[Certificado]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="2" y="0" width="50" height="10" forecolor="#FFFFFF" backcolor="#30579F"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<text><![CDATA[Poliza]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="53" y="0" width="38" height="10" isRemoveLineWhenBlank="true" forecolor="#FFFFFF" backcolor="#30579F"/>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<text><![CDATA[Oficina]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="12" splitType="Stretch">
			<textField isStretchWithOverflow="true" pattern="000000" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="133" y="1" width="35" height="10" backcolor="#FFFFFF"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.Integer"><![CDATA[$F{ContractID}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="200" y="1" width="140" height="10" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{CustomerName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="341" y="1" width="95" height="10" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{ProductName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="518" y="1" width="55" height="10" backcolor="#FFFFFF"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.sql.Timestamp"><![CDATA[$F{EffectiveDate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="574" y="1" width="65" height="10" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.Double"><![CDATA[$F{InstallmentValue}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="640" y="1" width="65" height="10" backcolor="#FFFFFF"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.Double"><![CDATA[$F{TotalPremium}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="437" y="1" width="80" height="10" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{IssuingStatus}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="169" y="1" width="30" height="10" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.Integer"><![CDATA[$F{EndorsementID}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="000000000" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="92" y="1" width="40" height="10" backcolor="#FFFFFF"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.Double"><![CDATA[$F{CertificateNumber}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="0" width="708" height="1"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<textField isStretchWithOverflow="true" pattern="0000000000000" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="2" y="1" width="50" height="10" backcolor="#FFFFFF"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.Double"><![CDATA[$F{PolicyNumber}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement positionType="Float" x="53" y="1" width="38" height="10"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{ChannelCode}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="10">
			<textField pattern="dd/MM/yyyy">
				<reportElement x="2" y="1" width="137" height="8"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Times New Roman" size="6" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement x="695" y="1" width="10" height="8"/>
				<textElement textAlignment="Right">
					<font fontName="Times New Roman" size="6" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="615" y="1" width="80" height="8"/>
				<textElement textAlignment="Right">
					<font fontName="Times New Roman" size="6" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Página "+$V{PAGE_NUMBER}+" de"]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="0" width="708" height="1"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
		</band>
	</pageFooter>
	<summary>
		<band height="41" splitType="Stretch">
			<printWhenExpression><![CDATA[new Boolean($V{totalgroup_COUNT}.intValue() != 0)]]></printWhenExpression>
			<textField isStretchWithOverflow="true">
				<reportElement x="519" y="14" width="85" height="9"/>
				<textElement textAlignment="Right" markup="none">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[($V{REPORT_COUNT} != null)? "Cantidad Total:" : ""]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement x="498" y="23" width="106" height="9"/>
				<textElement textAlignment="Right" markup="none">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[("Total General 1ra Cuota (Bs):")]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement x="498" y="32" width="106" height="9"/>
				<textElement textAlignment="Right" markup="none">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[("Total General Prima (Bs)")]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Group" evaluationGroup="totalgroup" pattern="###0" isBlankWhenNull="true">
				<reportElement x="604" y="14" width="100" height="9"/>
				<textElement textAlignment="Right">
					<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.Integer"><![CDATA[$V{REPORT_COUNT}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement x="604" y="23" width="100" height="9"/>
				<textElement textAlignment="Right">
					<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[new DecimalFormat("#,##0.00").format($V{totalGeralCota})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement x="604" y="32" width="100" height="9"/>
				<textElement textAlignment="Right">
					<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[new DecimalFormat("#,##0.00").format($V{totalGeralPrima})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="2" y="14" width="55" height="9"/>
				<textElement verticalAlignment="Middle" markup="none">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<text><![CDATA[Canal de Venta:]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement x="57" y="32" width="200" height="9"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[new SimpleDateFormat("dd/MM/yyyy").format($F{ParamEffectiveDateIni}) + " hasta " + new SimpleDateFormat("dd/MM/yyyy").format($F{ParamEffectiveDateEnd}) + " (vigencia desde)"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="2" y="23" width="42" height="9"/>
				<textElement verticalAlignment="Middle" markup="none">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<text><![CDATA[Situación:]]></text>
			</staticText>
			<staticText>
				<reportElement x="2" y="32" width="55" height="9"/>
				<textElement verticalAlignment="Middle" markup="none">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<text><![CDATA[Rango de fecha:]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="57" y="14" width="200" height="9"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[($F{ParamChannelID} != null)? $F{ChannelName} : "Todos los canales de venta"]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement x="57" y="23" width="200" height="9"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" pdfFontName="Times-Roman"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[($F{ParamIssuingStatus} != null)? $F{IssuingStatus} : "Todas las situaciones"]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="9" width="708" height="1"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement x="2" y="0" width="42" height="9"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Times New Roman" size="7" isBold="true" pdfFontName="Times-Roman"/>
				</textElement>
				<text><![CDATA[Resumen]]></text>
			</staticText>
		</band>
	</summary>
</jasperReport>
