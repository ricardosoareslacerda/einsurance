﻿var tree_data = [
	['Bienvenido', 'Files/9.gif', '', 'Topics/Topic20.htm', null],
	['Introducción', 'Files/1.gif', 'Files/2.gif', '',
		['El Objetivo', 'Files/9.gif', '', 'Topics/Topic30.htm', null],
		['Lo que hay de nuevo', 'Files/9.gif', '', 'Topics/Topic40.htm', null],
		['Lo que es el eInsurance', 'Files/9.gif', '', 'Topics/Topic50.htm', null],
		['Entrar en el eInsurance', 'Files/9.gif', '', 'Topics/Topic60.htm', null],
		['Presentación del Ecrã', 'Files/9.gif', '', 'Topics/Topic70.htm', null],
		['La barra de Menú', 'Files/9.gif', '', 'Topics/Topic100.htm', null],
		['La barra de Aliados', 'Files/9.gif', '', 'Topics/Topic110.htm', null]
	],
	['Configuración', 'Files/1.gif', 'Files/2.gif', '',
		['Seguridad', 'Files/1.gif', 'Files/2.gif', '',
			['Usuario', 'Files/1.gif', 'Files/2.gif', '',
				['Como registrar un Usuario', 'Files/9.gif', '', 'Topics/Topic80.htm', null],
				['Pantalla de Registro de usuario', 'Files/9.gif', '', 'Topics/Topic90.htm', null],
				['El cambio de contraseña', 'Files/9.gif', '', 'Topics/Topic840.htm', null],
				['Registro', 'Files/1.gif', 'Files/2.gif', '',
					['Pantalla de Detalles del usuario', 'Files/9.gif', '', 'Topics/Topic120.htm', null],
					['Pantalla de Login del usuario', 'Files/9.gif', '', 'Topics/Topic130.htm', null],
					['Pantalla de Rol del usuario', 'Files/9.gif', '', 'Topics/Topic140.htm', null],
					['Pantalla de Aliado del usuario', 'Files/9.gif', '', 'Topics/Topic150.htm', null],
					['Pantalla de Canal de Venta del usuario', 'Files/9.gif', '', 'Topics/Topic160.htm', null]
				]
			],
			['Rol', 'Files/1.gif', 'Files/2.gif', '',
				['Como registrar un Rol', 'Files/9.gif', '', 'Topics/Topic170.htm', null],
				['Pantalla de Registro de rol', 'Files/9.gif', '', 'Topics/Topic180.htm', null],
				['Registro', 'Files/1.gif', 'Files/2.gif', '',
					['Pantalla de Detalles del rol', 'Files/9.gif', '', 'Topics/Topic190.htm', null]
				]
			]
		]
	],
	['Datos Maestros', 'Files/1.gif', 'Files/2.gif', '',
		['Entidad', 'Files/1.gif', 'Files/2.gif', '',
			['Sucursal', 'Files/1.gif', 'Files/2.gif', '',
				['Como registrar una Sucursal', 'Files/9.gif', '', 'Topics/Topic220.htm', null],
				['Pantalla de Registro de sucursal', 'Files/9.gif', '', 'Topics/Topic230.htm', null],
				['Registro', 'Files/1.gif', 'Files/2.gif', '',
					['Pantalla de Detalles de la sucursal', 'Files/9.gif', '', 'Topics/Topic240.htm', null],
					['Pantalla de Dirección de la sucursal', 'Files/9.gif', '', 'Topics/Topic250.htm', null]
				]
			],
			['Corredor', 'Files/1.gif', 'Files/2.gif', '',
				['Como registrar un Corredor', 'Files/9.gif', '', 'Topics/Topic260.htm', null],
				['Pantalla de Registro de corredor', 'Files/9.gif', '', 'Topics/Topic270.htm', null],
				['Registro', 'Files/1.gif', 'Files/2.gif', '',
					['Pantalla de Detalles del corredor', 'Files/9.gif', '', 'Topics/Topic280.htm', null],
					['Pantalla de Dirección del corredor', 'Files/9.gif', '', 'Topics/Topic290.htm', null]
				]
			],
			['Aliado', 'Files/1.gif', 'Files/2.gif', '',
				['Aliado', 'Files/1.gif', 'Files/2.gif', '',
					['Como registrar un Aliado en el eInsurance', 'Files/9.gif', '', 'Topics/Topic300.htm', null],
					['Pantalla de Registro de aliado', 'Files/9.gif', '', 'Topics/Topic310.htm', null],
					['Registro', 'Files/1.gif', 'Files/2.gif', '',
						['Pantalla de Detalles del aliado', 'Files/9.gif', '', 'Topics/Topic320.htm', null],
						['Pantalla de Dirección del aliado', 'Files/9.gif', '', 'Topics/Topic330.htm', null],
						['Pantalla de Canal de venta del aliado', 'Files/9.gif', '', 'Topics/Topic340.htm', null]
					]
				],
				['Canal de Venta', 'Files/1.gif', 'Files/2.gif', '',
					['Como registrar un Canal de Venta', 'Files/9.gif', '', 'Topics/Topic350.htm', null],
					['Pantalla de Registro de canal de venta', 'Files/9.gif', '', 'Topics/Topic360.htm', null],
					['Registro', 'Files/1.gif', 'Files/2.gif', '',
						['Pantalla de Detalles del canal de venta', 'Files/9.gif', '', 'Topics/Topic370.htm', null],
						['Pantalla de Dirección del canal de venta', 'Files/9.gif', '', 'Topics/Topic380.htm', null],
						['Pantalla de Usuario del canal de venta', 'Files/9.gif', '', 'Topics/Topic390.htm', null]
					]
				]
			]
		],
		['Producto', 'Files/1.gif', 'Files/2.gif', '',
			['Contrato', 'Files/1.gif', 'Files/2.gif', '',
				['Como registrar un producto del contrato', 'Files/9.gif', '', 'Topics/Topic790.htm', null],
				['Pantalla de Registro de producto de contrato', 'Files/9.gif', '', 'Topics/Topic780.htm', null]
			]
		],
		['Póliza', 'Files/1.gif', 'Files/2.gif', '',
			['Maestra', 'Files/1.gif', 'Files/2.gif', '',
				['Como registrar una Póliza Maestra', 'Files/9.gif', '', 'Topics/Topic400.htm', null],
				['Pantalla de Registro de la póliza maestra', 'Files/9.gif', '', 'Topics/Topic410.htm', null],
				['Registro', 'Files/1.gif', 'Files/2.gif', '',
					['Pantalla de Detalles de la póliza maestra', 'Files/9.gif', '', 'Topics/Topic420.htm', null]
				]
			]
		]
	],
	['Comercialización', 'Files/1.gif', 'Files/2.gif', '',
		['Emisión', 'Files/1.gif', 'Files/2.gif', '',
			['Como cotizar un Seguro', 'Files/9.gif', '', 'Topics/Topic200.htm', null],
			['Pantalla de selección de Producto', 'Files/9.gif', '', 'Topics/Topic210.htm', null],
			['Cotización', 'Files/1.gif', 'Files/2.gif', '',
				['Pantalla de Cotización del seguro', 'Files/9.gif', '', 'Topics/Topic430.htm', null],
				['Pantalla de Datos Generales del seguro', 'Files/9.gif', '', 'Topics/Topic440.htm', null],
				['Pantalla de Datos del Asegurado del seguro', 'Files/9.gif', '', 'Topics/Topic450.htm', null],
				['Datos del Riesgo', 'Files/1.gif', 'Files/2.gif', '',
					['Vida', 'Files/1.gif', 'Files/2.gif', '',
						['Pantalla de Datos del Riesgo de Vida', 'Files/9.gif', '', 'Topics/Topic460.htm', null]
					],
					['Accidentes Personales', 'Files/1.gif', 'Files/2.gif', '',
						['Pantalla de Datos del Riesgo de Accidentes Personales', 'Files/9.gif', '', 'Topics/Topic470.htm', null]
					],
					['Ciclo Vital', 'Files/1.gif', 'Files/2.gif', '',
						['Pantalla de Datos del Riesgo de Ciclo Vital', 'Files/9.gif', '', 'Topics/Topic800.htm', null]
					],
					['Hábitat', 'Files/1.gif', 'Files/2.gif', '',
						['Pantalla de Datos del Riesgo de Hábitat', 'Files/9.gif', '', 'Topics/Topic810.htm', null]
					],
					['Saldo Deudor', 'Files/1.gif', 'Files/2.gif', '',
						['Pantalla de Datos del Riesgo de Saldo Deudor', 'Files/9.gif', '', 'Topics/Topic820.htm', null]
					]
				],
				['Pantalla de Coberturas del seguro', 'Files/9.gif', '', 'Topics/Topic480.htm', null],
				['Pantalla de Frecuencia de Pago del seguro', 'Files/9.gif', '', 'Topics/Topic490.htm', null]
			],
			['Propuesta', 'Files/1.gif', 'Files/2.gif', '',
				['Como contratar un Seguro', 'Files/9.gif', '', 'Topics/Topic500.htm', null],
				['Asegurado', 'Files/1.gif', 'Files/2.gif', '',
					['Pantalla de datos del Asegurado del seguro', 'Files/9.gif', '', 'Topics/Topic510.htm', null],
					['Pantalla de Dirección del asegurado', 'Files/9.gif', '', 'Topics/Topic520.htm', null]
				],
				['Contratante', 'Files/1.gif', 'Files/2.gif', '',
					['Pantalla de datos del Tomador del seguro', 'Files/9.gif', '', 'Topics/Topic530.htm', null],
					['Pantalla de Dirección del contratante', 'Files/9.gif', '', 'Topics/Topic540.htm', null]
				],
				['Beneficiario', 'Files/1.gif', 'Files/2.gif', '',
					['Pantalla de listado de Beneficiario del seguro', 'Files/9.gif', '', 'Topics/Topic550.htm', null],
					['Pantalla de Registro de beneficiario del seguro', 'Files/9.gif', '', 'Topics/Topic560.htm', null]
				],
				['Datos del Riesgo', 'Files/1.gif', 'Files/2.gif', '',
					['Hábitat', 'Files/1.gif', 'Files/2.gif', '',
						['Pantalla de Datos del Riesgo de Hábitat', 'Files/9.gif', '', 'Topics/Topic830.htm', null]
					]
				],
				['Grupo Familiar', 'Files/1.gif', 'Files/2.gif', '',
					['Pantalla de listado de Grupo Familiar del seguro', 'Files/9.gif', '', 'Topics/Topic930.htm', null],
					['Pantalla de Registro de grupo familiar del seguro', 'Files/9.gif', '', 'Topics/Topic940.htm', null]
				],
				['Adicionales', 'Files/1.gif', 'Files/2.gif', '',
					['Pantalla de listado de Adicionales del seguro', 'Files/9.gif', '', 'Topics/Topic950.htm', null],
					['Pantalla de Registro de adicioanles del seguro', 'Files/9.gif', '', 'Topics/Topic960.htm', null]
				],
				['Forma de Pago', 'Files/1.gif', 'Files/2.gif', '',
					['Pantalla de Forma de Pago del seguro', 'Files/9.gif', '', 'Topics/Topic570.htm', null]
				],
				['Pantalla de Impresión del seguro', 'Files/9.gif', '', 'Topics/Topic600.htm', null]
			]
		]
	],
	['Información', 'Files/1.gif', 'Files/2.gif', '',
		['Consulta', 'Files/1.gif', 'Files/2.gif', '',
			['Documentos', 'Files/1.gif', 'Files/2.gif', '',
				['Como consultar un Documento', 'Files/9.gif', '', 'Topics/Topic580.htm', null],
				['Pantalla de Consulta de documentos', 'Files/9.gif', '', 'Topics/Topic590.htm', null],
				['Reporte', 'Files/1.gif', 'Files/2.gif', '',
					['Reporte de Certificado', 'Files/9.gif', '', 'Topics/Topic610.htm', null],
					['Reporte de estatus de Cuota', 'Files/9.gif', '', 'Topics/Topic620.htm', null]
				],
				['Anulacion', 'Files/1.gif', 'Files/2.gif', '',
					['Propuesta', 'Files/1.gif', 'Files/2.gif', '',
						['Pantalla de Anulación de propuesta', 'Files/9.gif', '', 'Topics/Topic880.htm', null],
						['Pantalla de Confirmación y motivo de anulación', 'Files/9.gif', '', 'Topics/Topic890.htm', null]
					],
					['Certificado', 'Files/1.gif', 'Files/2.gif', '',
						['Pantalla de Anulación de certificado', 'Files/9.gif', '', 'Topics/Topic850.htm', null]
					]
				],
				['Desbloqueo', 'Files/1.gif', 'Files/2.gif', '',
					['Propuesta', 'Files/1.gif', 'Files/2.gif', '',
						['Pantalla de Desbloqueo de propuesta', 'Files/9.gif', '', 'Topics/Topic910.htm', null],
						['Pantalla de Confirmación del desbloqueo', 'Files/9.gif', '', 'Topics/Topic920.htm', null]
					]
				],
				['Alteracion', 'Files/1.gif', 'Files/2.gif', '',
					['Cotización', 'Files/1.gif', 'Files/2.gif', '',
						['Pantalla de Recuperación de Cotización', 'Files/9.gif', '', 'Topics/Topic1130.htm', null]
					],
					['Registral', 'Files/1.gif', 'Files/2.gif', '',
						['Pantalla de Alteración Registral', 'Files/9.gif', '', 'Topics/Topic860.htm', null],
						['Pantalla de Confirmación de la alteración', 'Files/9.gif', '', 'Topics/Topic870.htm', null]
					],
					['Técnica', 'Files/1.gif', 'Files/2.gif', '',
						['Pantalla de Alteración Técnica', 'Files/9.gif', '', 'Topics/Topic900.htm', null]
					]
				]
			]
		],
		['Reporte', 'Files/1.gif', 'Files/2.gif', '',
			['Operacional', 'Files/1.gif', 'Files/2.gif', '',
				['Canal de Venta', 'Files/1.gif', 'Files/2.gif', '',
					['Como sacar un reporte por Canal de Venta', 'Files/9.gif', '', 'Topics/Topic630.htm', null],
					['Pantalla del Reporte de canal de venta', 'Files/9.gif', '', 'Topics/Topic640.htm', null],
					['Reporte', 'Files/1.gif', 'Files/2.gif', '',
						['Reporte de Canal de Venta en .PDF', 'Files/9.gif', '', 'Topics/Topic650.htm', null],
						['Reporte de Canal de Venta en .XLS', 'Files/9.gif', '', 'Topics/Topic1040.htm', null]
					]
				],
				['Producto', 'Files/1.gif', 'Files/2.gif', '',
					['Como sacar un reporte por Producto', 'Files/9.gif', '', 'Topics/Topic660.htm', null],
					['Pantalla del Reporte de producto', 'Files/9.gif', '', 'Topics/Topic670.htm', null],
					['Reporte', 'Files/1.gif', 'Files/2.gif', '',
						['Reporte de Producto en .PDF', 'Files/9.gif', '', 'Topics/Topic680.htm', null],
						['Reporte de Producto en .XLS', 'Files/9.gif', '', 'Topics/Topic1050.htm', null]
					]
				],
				['Vendedor', 'Files/1.gif', 'Files/2.gif', '',
					['Como emitir un reporte por Vendedor', 'Files/9.gif', '', 'Topics/Topic690.htm', null],
					['Pantalla del Reporte de vendedor', 'Files/9.gif', '', 'Topics/Topic700.htm', null],
					['Reporte', 'Files/1.gif', 'Files/2.gif', '',
						['Reporte de Vendedor en .PDF', 'Files/9.gif', '', 'Topics/Topic710.htm', null],
						['Reporte de Vendedor en .XLS', 'Files/9.gif', '', 'Topics/Topic1060.htm', null]
					]
				],
				['Cobranza', 'Files/1.gif', 'Files/2.gif', '',
					['Como sacar un reporte por Cobranza', 'Files/9.gif', '', 'Topics/Topic720.htm', null],
					['Pantalla del Reporte de cobranza', 'Files/9.gif', '', 'Topics/Topic730.htm', null],
					['Reporte', 'Files/1.gif', 'Files/2.gif', '',
						['Reporte de Cobranza en .PDF', 'Files/9.gif', '', 'Topics/Topic740.htm', null],
						['Reporte de Cobranza en .XLS', 'Files/9.gif', '', 'Topics/Topic1070.htm', null]
					]
				],
				['Comision', 'Files/1.gif', 'Files/2.gif', '',
					['Como emitir un reporte por Comisión', 'Files/9.gif', '', 'Topics/Topic750.htm', null],
					['Pantalla del Reporte de comisión', 'Files/9.gif', '', 'Topics/Topic760.htm', null],
					['Reporte', 'Files/1.gif', 'Files/2.gif', '',
						['Reporte de Comisión en .PDF', 'Files/9.gif', '', 'Topics/Topic770.htm', null],
						['Reporte de Comisión en .XLS', 'Files/9.gif', '', 'Topics/Topic1080.htm', null]
					]
				],
				['Remuneracion', 'Files/1.gif', 'Files/2.gif', '',
					['Como emitir un reporte por Remuneración', 'Files/9.gif', '', 'Topics/Topic1090.htm', null],
					['Pantalla del Reporte de remuneración', 'Files/9.gif', '', 'Topics/Topic1100.htm', null],
					['Reporte', 'Files/1.gif', 'Files/2.gif', '',
						['Reporte de Remuneración en .PDF', 'Files/9.gif', '', 'Topics/Topic1110.htm', null],
						['Reporte de Remuneración en .XLS', 'Files/9.gif', '', 'Topics/Topic1120.htm', null]
					]
				]
			],
			['Cobranza', 'Files/1.gif', 'Files/2.gif', '',
				['Lotes', 'Files/1.gif', 'Files/2.gif', '',
					['Como emitir un reporte por Lotes', 'Files/9.gif', '', 'Topics/Topic970.htm', null],
					['Pantalla del Reporte de lotes', 'Files/9.gif', '', 'Topics/Topic980.htm', null],
					['Reporte', 'Files/1.gif', 'Files/2.gif', '',
						['Reporte de Lotes en .PDF', 'Files/9.gif', '', 'Topics/Topic990.htm', null]
					]
				],
				['Cuotas', 'Files/1.gif', 'Files/2.gif', '',
					['Como emitir un reporte por Cuotas', 'Files/9.gif', '', 'Topics/Topic1000.htm', null],
					['Pantalla del Reporte de cuotas', 'Files/9.gif', '', 'Topics/Topic1010.htm', null],
					['Reporte', 'Files/1.gif', 'Files/2.gif', '',
						['Reporte de Cuotas en .PDF', 'Files/9.gif', '', 'Topics/Topic1020.htm', null],
						['Reporte de Cuotas en .XLS', 'Files/9.gif', '', 'Topics/Topic1030.htm', null]
					]
				]
			]
		]
	],
	['Ayuda', 'Files/1.gif', 'Files/2.gif', '',
		['Manual Operacional', 'Files/9.gif', '', 'Topics/Topic30.htm', null]
	]
];