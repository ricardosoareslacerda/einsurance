﻿var tree_data = [
	['Bem vindo', 'Files/9.gif', '', 'Topics/Welcome.htm', null],
	['Introdução', 'Files/1.gif', 'Files/2.gif', '',
		['Capa', 'Files/9.gif', '', 'Topics/Topic90.htm', null],
		['Finalidade', 'Files/9.gif', '', 'Topics/Topic250.htm', null],
		['Referências', 'Files/9.gif', '', 'Topics/Table_of_Contents.htm', null]
	],
	['Representação Arquitetura', 'Files/1.gif', 'Files/2.gif', '',
		['Representação da Arquitetura', 'Files/9.gif', '', 'Topics/Using_the_Word_Processor.htm', null]
	],
	['Tecnologias Envolvidas', 'Files/1.gif', 'Files/2.gif', '',
		['Linguagem Java', 'Files/9.gif', '', 'Topics/Creating_Printed_Manual.htm', null],
		['Spring Security', 'Files/9.gif', '', 'Topics/Printed_Manual_Front_Page_Project_Options.htm', null],
		['Adobe BlazeDS', 'Files/9.gif', '', 'Topics/Printed_Manual_Contents_Project_Options.htm', null],
		['Spring Framework', 'Files/9.gif', '', 'Topics/Web_Help_Appearance_Project_Options.htm', null],
		['Adobe Flex', 'Files/9.gif', '', 'Topics/Web_Help_General_Project_Options.htm', null],
		['Pure MVC', 'Files/9.gif', '', 'Topics/Web_Help_Layout_Project_Options.htm', null],
		['Hibernate', 'Files/9.gif', '', 'Topics/Importing_HTML_Help_Workshop_Projects.htm', null],
		['JBoss ESB', 'Files/9.gif', '', 'Topics/Topic240.htm', null]
	]
];