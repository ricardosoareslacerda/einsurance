﻿var topic_data = [
	[10, 'Welcome', 'Topics/Welcome.htm'],
	[50, 'Table_of_Contents', 'Topics/Table_of_Contents.htm'],
	[70, 'Using_the_Word_Processor', 'Topics/Using_the_Word_Processor.htm'],
	[90, 'Topic90', 'Topics/Topic90.htm'],
	[240, 'Topic240', 'Topics/Topic240.htm'],
	[250, 'Topic250', 'Topics/Topic250.htm'],
	[290, 'Creating_Printed_Manual', 'Topics/Creating_Printed_Manual.htm'],
	[300, 'Printed_Manual_Front_Page_Project_Options', 'Topics/Printed_Manual_Front_Page_Project_Options.htm'],
	[310, 'Printed_Manual_Contents_Project_Options', 'Topics/Printed_Manual_Contents_Project_Options.htm'],
	[340, 'Importing_HTML_Help_Workshop_Projects', 'Topics/Importing_HTML_Help_Workshop_Projects.htm'],
	[390, 'Web_Help_General_Project_Options', 'Topics/Web_Help_General_Project_Options.htm'],
	[400, 'Web_Help_Layout_Project_Options', 'Topics/Web_Help_Layout_Project_Options.htm'],
	[410, 'Web_Help_Appearance_Project_Options', 'Topics/Web_Help_Appearance_Project_Options.htm']
];