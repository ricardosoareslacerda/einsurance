package br.com.tratomais.einsurance.useradm.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	import br.com.tratomais.einsurance.useradm.model.vo.User;
	
	import mx.rpc.IResponder;
    
	public class ModuleDelegateProxy
	{
		private var responder : IResponder;
		private var service : Object;
		
		public function ModuleDelegateProxy(pResponder : IResponder )
		{
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_MODULE);
			responder = pResponder;
		}   

		public function findByName(name:String):void
		{
			var call:Object = service.findByName(name);
			call.addResponder(responder);
		}
		
		public function listAllowedModulesByUserLogged():void
		{
			var call:Object = service.listAllowedModulesByUserLogged();
			call.addResponder(responder);
		}


		public function listAll():void
		{
			var call:Object = service.listAllModule();
			call.addResponder(responder);
		}
		
		public function listAllByUser(user : User):void
		{
			var call:Object = service.listAllModuleByUser(user);
			call.addResponder(responder);
		}

		public function findById(id:Number):void
		{
			var call:Object = service.findModuleById(id);
			call.addResponder(responder);
		}
	}
}	