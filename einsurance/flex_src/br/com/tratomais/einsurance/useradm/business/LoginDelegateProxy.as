package br.com.tratomais.einsurance.useradm.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	import br.com.tratomais.einsurance.useradm.model.vo.Login;
	import br.com.tratomais.einsurance.useradm.model.vo.User;
	
	import mx.rpc.IResponder;
    
	public class LoginDelegateProxy
	{
		private var responder : IResponder;
		private var service : Object;
		
		public function LoginDelegateProxy(pResponder : IResponder )
		{
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_LOGIN);
			responder = pResponder;
		}   

		public function validateLogin(userName:String, passwordKey:String):void
		{
			var call:Object = service.validateLogin(userName, passwordKey);
			call.addResponder(responder);
		}

		public function save(user:User):void
		{
			var call:Object = service.saveLoginByUser(user);
			call.addResponder(responder);
		}

		public function loginExistence(login:String):void
		{
			var call:Object = service.loginExistence(login);
			call.addResponder(responder);
		}

		public function listAll():void
		{
			var call:Object = service.listAllLogin();
			call.addResponder(responder);
		}

		public function findById(id:Number):void
		{
			var call:Object = service.findById(id);
			call.addResponder(responder);
		}

		public function findByName(domain:String):void
		{
			var call:Object = service.findByName(domain);
			call.addResponder(responder);
		}

		public function findByLogin(login:String):void
		{
			var call:Object = service.findByLogin(login);
			call.addResponder(responder);
		}
		
		public function findByUser(user:User):void
		{
			var call:Object = service.findLoginByUser(user);
			call.addResponder(responder);
		}


		public function saveObject(login:Login):void
		{
			var call:Object = service.saveLogin(login);
			call.addResponder(responder);
		}

		public function deleteObject(login:Login):void
		{
			var call:Object = service.deleteLogin(login);
			call.addResponder(responder);
    	}
    	
    	public function deleteObjectByLoginName(loginName : String, domain: String):void
		{
			var call:Object = service.deleteLoginByLoginName(loginName, domain);
			call.addResponder(responder);
    	}
    	
    	public function logout():void
		{
			service.logout();
    	}
    	
		public function auditLogger(message:String):void{
			var call:Object = service.logAuditAction(message);
			call.addResponder(responder);
		}    	
    	
    	/**
    	 * Change password from user
    	 * @param user User instance to have the password changed
    	 * @param oldPassword Old password, used to authenticate
    	 * @param newPassword New password
    	 */
    	public function changeUserPassword( user: User, oldPassword: String , newPassword: String ): void {
    		var call:Object = service.changeUserPassword( user.login, user.domain.name, oldPassword, newPassword);
			call.addResponder(responder);
    	}
	}

}	