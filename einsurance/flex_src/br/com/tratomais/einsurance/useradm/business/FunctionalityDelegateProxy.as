package br.com.tratomais.einsurance.useradm.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	
	import mx.rpc.IResponder;
	
	public class FunctionalityDelegateProxy
	{
		
		private var responder : IResponder;
		private var service : Object;
		
		public function FunctionalityDelegateProxy(pResponder : IResponder )
		{
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_MODULE);
			responder = pResponder;
		}
		
		public function listAllowedFunctionalityByUserLogged():void{
			var call:Object = service.listAllowedFunctionalityByUserLogged();
			call.addResponder(responder);
		}

	}
}