package br.com.tratomais.einsurance.useradm.business
{  
    import br.com.tratomais.einsurance.core.UtilitiesService;
    import br.com.tratomais.einsurance.useradm.model.vo.Login;
    import br.com.tratomais.einsurance.useradm.model.vo.User;
    
    import mx.rpc.IResponder;  
      
    public class UserDelegateProxy  
    {  
        private var responder : IResponder;  
        private var service : Object;  
          
        public function UserDelegateProxy(pResponder : IResponder )  
        {  
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_USER);
			responder = pResponder;
        }     
    
        public function loadUserByLogin(userName:String):void  
        {  
            var call:Object = service.loadUserByLogin(userName);  
            call.addResponder(responder);  
        }  
  
        public function saveObject(user:User, login : Login):void  
        {  
            var call:Object = service.saveUser(user, login);  
            call.addResponder(responder);  
        }  
        public function saveObjectInsert(user:User, login : Login):void  
        {  
            var call:Object = service.saveUserInsert(user, login);  
            call.addResponder(responder);  
        }  
  
 	 	public function changePartnerDefaultLoggedUser(partnerId : int) : void {
            var call:Object = service.changePartnerDefaultLoggedUser(partnerId);  
            call.addResponder(responder);  
 	 		
 	 	}

        public function findById(id:Number):void  
        {  
            var call:Object = service.findById(id);  
            call.addResponder(responder);  
        }  
  
        public function findByName(name:String):void  
        {  
            var call:Object = service.findByName(name);  
            call.addResponder(responder);  
        }  
  
        public function deleteObject(user:User):void  
        {  
            var call:Object = service.deleteUser(user);  
            call.addResponder(responder);  
        }  
        
        public function listUserAllowed():void  
        {  
            var call:Object = service.listUserAllowed();  
            call.addResponder(responder);  
        }  
        
        public function listAllDomainActive() : void
        {
        	var call : Object = service.listAllDomainActive();
        	call.addResponder(responder);
        }
        
        public function listAllPartnerChannel(user : User) : void
        {
        	var call : Object = service.listAllPartnerChannel(user);
        	call.addResponder(responder);
        }
        
        public function listUserPerPartner(partner:int) : void
        {
        	var call : Object = service.listUserPerPartner(partner);
        	call.addResponder(responder);
        }
    }  
}