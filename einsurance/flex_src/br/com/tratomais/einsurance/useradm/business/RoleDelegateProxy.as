package br.com.tratomais.einsurance.useradm.business  
{  
    import br.com.tratomais.einsurance.core.UtilitiesService;
    import br.com.tratomais.einsurance.useradm.model.vo.*;
    
    import mx.rpc.IResponder;  
      
    public class RoleDelegateProxy  
    {  
        private var responder : IResponder;  
        private var service : Object;  
          
        public function RoleDelegateProxy(pResponder : IResponder )  
        {  
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_ROLE);
            responder = pResponder;  
        }     
  
        public function listAllActiveRole():void  
        {  
            var call:Object = service.listAllActiveRole();  
            call.addResponder(responder);  
        }   
  
        public function listAll():void  
        {  
            var call:Object = service.listAllRole();  
            call.addResponder(responder);  
        }  
        public function listAllByUser(user:User):void  
        {  
            var call:Object = service.listAllRoleByUser(user);  
            call.addResponder(responder);  
        }  
  
        public function findById(id:int):void  
        {  
            var call:Object = service.findById(id);  
            call.addResponder(responder);  
        }  
  
        public function findByName(name:String):void  
        {  
            var call:Object = service.findByName(name);  
            call.addResponder(responder);  
        } 
          
        public function saveObject(role: Role) : void{
        	var call:Object = service.saveRole(role);
        	call.addResponder(responder);
        }

    }  
  
}     