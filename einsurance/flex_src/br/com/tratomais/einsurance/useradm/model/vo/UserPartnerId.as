package br.com.tratomais.einsurance.useradm.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
   
    [RemoteClass(alias="br.com.tratomais.core.model.UserPartnerId")]      
	[Managed]
    public class UserPartnerId  extends HibernateBean
    {  
  
        private var _userId:int;  
        private var _partnerId:int;  
  
  
        public function UserPartnerId()  
        {  
              
        }  
  
        public function get userId():int{  
            return _userId;  
        }  
  
        public function set userId(pData:int):void{  
            _userId=pData;  
        }  
  
        public function get partnerId():int{  
            return _partnerId;  
        }  
  
        public function set partnerId(pData:int):void{  
            _partnerId=pData;  
        }         
    }  
}  