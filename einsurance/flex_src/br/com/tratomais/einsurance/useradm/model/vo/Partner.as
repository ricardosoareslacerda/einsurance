package br.com.tratomais.einsurance.useradm.model.vo  
{
	import mx.collections.ArrayCollection;
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	 
    [RemoteClass(alias="br.com.tratomais.core.model.customer.Partner")]    
	[Managed]
    public class Partner extends HibernateBean
    {
        private var _partnerId:int;  
        private var _domain:Domain;  
        private var _name:String;  
        private var _nickName:String;  
        private var _partnerType:int;  
        private var _externalCode:String;  
        private var _documentType:String;  
        private var _documentNumber:String;  
        private var _documentIssuer:String;  
        private var _documentValidity:Date;  
        private var _addressStreet:String;  
        private var _districtName:String;  
        private var _cityId:String;  
        private var _cityName:String;  
        private var _regionName:String;  
        private var _stateId:String;  
        private var _stateName:String;  
        private var _zipCode:String;  
        private var _countryId:String;  
        private var _countryName:String;  
        private var _emailAddress:String;  
        private var _phoneNumber:String;  
        private var _faxNumber:String;  
		private var _imageProperty:String;
        private var _active:Boolean=true; 
        private var _channels:ArrayCollection;  
        private var _userPartners:ArrayCollection;  
         

		/*
		 * Campo Transiente para suporte ao check de Listas
		 */
        private var _transientSelected:Boolean;  
        private var _transientAdministrator:Boolean;  
        private var _transientAllChannel:Boolean;  
        private var _transientIsDefault:Boolean;  
        private var _transientEditor:Boolean;    
  
        public function Partner()  
        {  
              
        }  

        public function get partnerId():int{  
            return _partnerId;  
        }  
  
        public function set partnerId(pData:int):void{  
            _partnerId=pData;  
        }  
  
        public function get domain():Domain{  
            return _domain;  
        }  
  
        public function set domain(pData:Domain):void{  
            _domain=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get nickName():String{  
            return _nickName;  
        }  
  
        public function set nickName(pData:String):void{  
            _nickName=pData;  
        }  
  
        public function get partnerType():int{  
            return _partnerType;  
        }  
  
        public function set partnerType(pData:int):void{  
            _partnerType=pData;  
        }  
  
        public function get externalCode():String{  
            return _externalCode;  
        }  
  
        public function set externalCode(pData:String):void{  
            _externalCode=pData;  
        }  
  
        public function get documentType():String{  
            return _documentType;  
        }  
  
        public function set documentType(pData:String):void{  
            _documentType=pData;  
        }  
  
        public function get documentNumber():String{  
            return _documentNumber;  
        }  
  
        public function set documentNumber(pData:String):void{  
            _documentNumber=pData;  
        }  
  
        public function get documentIssuer():String{  
            return _documentIssuer;  
        }  
  
        public function set documentIssuer(pData:String):void{  
            _documentIssuer=pData;  
        }  
  
        public function get documentValidity():Date{  
            return _documentValidity;  
        }  
  
        public function set documentValidity(pData:Date):void{  
            _documentValidity=pData;  
        }  
  
        public function get addressStreet():String{  
            return _addressStreet;  
        }  
  
        public function set addressStreet(pData:String):void{  
            _addressStreet=pData;  
        }  
  
        public function get districtName():String{  
            return _districtName;  
        }  
  
        public function set districtName(pData:String):void{  
            _districtName=pData;  
        }  
  
        public function get cityId():String{  
            return _cityId;  
        }  
  
        public function set cityId(pData:String):void{  
            _cityId=pData;  
        }  
  
        public function get cityName():String{  
            return _cityName;  
        }  
  
        public function set cityName(pData:String):void{  
            _cityName=pData;  
        }  
  
        public function get regionName():String{  
            return _regionName;  
        }  
  
        public function set regionName(pData:String):void{  
            _regionName=pData;  
        }  
  
        public function get stateId():String{  
            return _stateId;  
        }  
  
        public function set stateId(pData:String):void{  
            _stateId=pData;  
        }  
  
        public function get stateName():String{  
            return _stateName;  
        }  
  
        public function set stateName(pData:String):void{  
            _stateName=pData;  
        }  
  
        public function get zipCode():String{  
            return _zipCode;  
        }  
  
        public function set zipCode(pData:String):void{  
            _zipCode=pData;  
        }  
  
        public function get countryId():String{  
            return _countryId;  
        }  
  
        public function set countryId(pData:String):void{  
            _countryId=pData;  
        }  
  
        public function get countryName():String{  
            return _countryName;  
        }  
  
        public function set countryName(pData:String):void{  
            _countryName=pData;  
        }  
  
        public function get emailAddress():String{  
            return _emailAddress;  
        }  
  
        public function set emailAddress(pData:String):void{  
            _emailAddress=pData;  
        }  
  
        public function get phoneNumber():String{  
            return _phoneNumber;  
        }  
  
        public function set phoneNumber(pData:String):void{  
            _phoneNumber=pData;  
        }  
  
        public function get faxNumber():String{  
            return _faxNumber;  
        }  
  
        public function set faxNumber(pData:String):void{  
            _faxNumber=pData;  
        }  
  
        public function get imageProperty():String {
			return _imageProperty;
		}

        public function set imageProperty(pData:String) {
			_imageProperty = pData;
		}

        public function get active():Boolean{  
            return _active;  
        }  
  
        public function set active(pData:Boolean):void{  
            _active=pData;  
        }
            
        public function get channels():ArrayCollection{  
            return _channels;  
        }  
  
        public function set channels(pData:ArrayCollection):void{  
            _channels=pData;  
        }  
  
        public function get userPartners():ArrayCollection{  
            return _userPartners;  
        }  
  
        public function set userPartners(pData:ArrayCollection):void{  
            _userPartners=pData;  
        }
        
        public function get transientSelected():Boolean{  
            return _transientSelected;  
        }  
  
        public function set transientSelected(pData:Boolean):void{  
            _transientSelected=pData;  
        }  
        
           public function get transientAdministrator():Boolean{  
            return _transientAdministrator;  
        }  
  
        public function set transientAdministrator(pData:Boolean):void{  
            _transientAdministrator=pData;  
        }  
        
            public function get transientAllChannel():Boolean{  
            return _transientAllChannel;  
        }  
  
        public function set transientAllChannel(pData:Boolean):void{  
            _transientAllChannel=pData;  
        } 
        
        public function get transientIsDefault():Boolean{  
            return _transientIsDefault;  
        }  
  
        public function set transientIsDefault(pData:Boolean):void{  
            _transientIsDefault=pData;  
        }  
        
        public function get transientEditor():Boolean{  
            return _transientEditor;  
        }  
  
        public function set transientEditor(pData:Boolean):void{  
            _transientEditor=pData;  
        }  

        
    }  
}  