package br.com.tratomais.einsurance.useradm.model.vo  
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
     
    [RemoteClass(alias="br.com.tratomais.core.model.Role")]   
	[Managed]
    public class Role extends HibernateBean
    {  
  
        private var _roleId:int;  
        private var _name:String;  
        private var _description:String;  
        private var _active:Boolean;  
        private var _modules:ArrayCollection;  
        private var _functionalities:ArrayCollection;  
          
		/*
		 * Campo Transiente para suporte ao check de Listas
		 */
        private var _transientSelected:Boolean;  
  		private var _transientEditor:Boolean;    
    
  
        public function Role()  
        {  
              
        }  
  
        public function get roleId():int{  
            return _roleId;  
        }  
  
        public function set roleId(pData:int):void{  
            _roleId=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get description():String{  
            return _description;  
        }  
  
        public function set description(pData:String):void{  
            _description=pData;  
        }  
		  
        public function get active():Boolean{  
            return _active;  
        }  
  
        public function set active(pData:Boolean):void{  
            _active=pData;  
        }  
  
        public function get modules():ArrayCollection{  
            return _modules;  
        }  
  
        public function set modules(pData:ArrayCollection):void{  
            _modules=pData;  
        }  
   
        public function get functionalities():ArrayCollection{  
            return _functionalities;  
        }  
  
        public function set functionalities(pData:ArrayCollection):void{  
            _functionalities=pData;  
        }  
   	  
        public function get transientSelected():Boolean{  
            return _transientSelected;  
        }  
  
        public function set transientSelected(pData:Boolean):void{  
            _transientSelected=pData;  
        }  

  		public function get transientEditor():Boolean{  
            return _transientEditor;  
        }  
  
        public function set transientEditor(pData:Boolean):void{  
            _transientEditor=pData;  
        }  
    }  
}  