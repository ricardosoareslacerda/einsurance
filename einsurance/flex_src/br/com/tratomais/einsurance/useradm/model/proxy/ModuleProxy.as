package br.com.tratomais.einsurance.useradm.model.proxy
{
	
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.useradm.business.ModuleDelegateProxy;
	import br.com.tratomais.einsurance.useradm.model.vo.Module;
	import br.com.tratomais.einsurance.useradm.model.vo.User;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
		
	
	[Bindable]
	public class ModuleProxy extends Proxy implements IProxy 
	{
	
		public static const NAME:String = "ModuleProxy";

		public function ModuleProxy(data:Object= null){
			super(NAME, data );
		}

		public function findByName(name:String):void {
			var delegate:ModuleDelegateProxy = new ModuleDelegateProxy(new Responder(onFindByNameResult, onFault));
			delegate.findByName(name);
		}

		private function onFindByNameResult(pResultEvt:ResultEvent):void {
			var result:Array=pResultEvt.result as Array ;
			//sendNotification();
		}

		public function listAll():void
		{
			var delegate:ModuleDelegateProxy = new ModuleDelegateProxy(new Responder(onListAllResult, onFault));
			delegate.listAll();
		}

		public function listAllEdit():void
		{
			var delegate:ModuleDelegateProxy = new ModuleDelegateProxy(new Responder(onListAllResultEdit, onFault));
			delegate.listAllowedModulesByUserLogged();
		}
		
		public function listAllowedModulesByUserLogged():void
		{
			var delegate:ModuleDelegateProxy = new ModuleDelegateProxy(new Responder(onListAllowedModulesByUserLogged, onFault));
			delegate.listAllowedModulesByUserLogged();
		}

		public function onListAllowedModulesByUserLogged(pResultEvt:ResultEvent):void
		{
			var listResult:ArrayCollection = pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.MODULE_LIST, listResult);
		}
		
		private function onListAllResultEdit(pResultEvt:ResultEvent):void
		{
			var listResult:ArrayCollection = pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.MODULE_LISTED_EDIT, listResult);
		}

		private function onListAllResult(pResultEvt:ResultEvent):void
		{
			var listResult:ArrayCollection = pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.MODULE_LISTED, listResult);
		}

		public function listAllByUser(user : User):void
		{
			var delegate:ModuleDelegateProxy = new ModuleDelegateProxy(new Responder(onListAllByUserResult, onFault));
			delegate.listAllByUser(user);
		}

		private function onListAllByUserResult(pResultEvt:ResultEvent):void
		{
			var listResult:ArrayCollection = pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.MODULE_LISTED_BY_USER, listResult);
		}
		
		public function findById(id:Number):void
		{
			var delegate:ModuleDelegateProxy = new ModuleDelegateProxy(new Responder(onFindByIdResult, onFault));
			delegate.findById(id);
		}

		private function onFindByIdResult(pResultEvt:ResultEvent):void
		{
			var result:Module=pResultEvt.result as Module;
			sendNotification(NotificationList.MODULE_FOUND, result);
		}
		
		private function onFault(pFaultEvt:FaultEvent):void{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
		}
	}
}