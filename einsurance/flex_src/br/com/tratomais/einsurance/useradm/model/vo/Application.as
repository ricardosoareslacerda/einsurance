package br.com.tratomais.einsurance.useradm.model.vo
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
	
	[RemoteClass(alias="br.com.tratomais.core.model.Application")]	
	[Managed]
	public class Application extends HibernateBean
	{
		/**
		 * Constantes
		 */
		public static const DEFAULT:int = 1;

		private var _applicationId:int;
		private var _name:String;
		private var _active:Boolean;
		private var _modules:ArrayCollection;


		public function Application()
		{
			
		}

		public function get applicationId():int{
			return _applicationId;
		}

		public function set applicationId(pData:int):void{
			_applicationId=pData;
		}

		public function get name():String{
			return _name;
		}

		public function set name(pData:String):void{
			_name=pData;
		}

		public function get active():Boolean{
			return _active;
		}

		public function set active(pData:Boolean):void{
			_active=pData;
		}

		public function get modules():ArrayCollection {
			return _modules;
		}

		public function set modules(pData:ArrayCollection):void{
			_modules=pData;
		}
	}
}