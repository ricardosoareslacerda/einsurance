package br.com.tratomais.einsurance.useradm.model.vo  
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
      
  
      
    [RemoteClass(alias="br.com.tratomais.core.model.User")]   
	[Managed]
	   public class User extends HibernateBean
    {  
  
		public static const AUTHORIZATION_TYPE_BROKER:int = 183;		// Corretor
        private var _userId:int;  
        private var _domain:Domain;  
        private var _login:String;  
        private var _name:String;  
        private var _emailAddress:String;  
        private var _phoneNumber:String;  
        private var _employeeCode:String;
        private var _authorizationType:int;
        private var _administrator: Boolean;
        private var _active: Boolean;
        private var _userPartners:ArrayCollection;  
        private var _roles:ArrayCollection;  
        private var _channels:ArrayCollection;  
        private var _endorses:ArrayCollection;  
        private var _password:String;  
        private var _username:String;  
        private var _timeValidation:Date;  
        private var _timeLimit:int;  

	
		private var _transientSelectedPartner:int;
        private var _transientSelected:Boolean;  
        private var _transientEditor:Boolean;  
  
        public function User()  
        {  
              
        }  
  
        public function get userId():int{  
            return _userId;  
        }  
  
        public function set userId(pData:int):void{  
            _userId=pData;  
        }  
  
        public function get domain():Domain{  
            return _domain;  
        }  
  
        public function set domain(pData:Domain):void{  
            _domain=pData;  
        }  
  
        public function get login():String{  
            return _login;  
        }  
  
        public function set login(pData:String):void{  
            _login=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get emailAddress():String{  
            return _emailAddress;  
        }  
  
        public function set emailAddress(pData:String):void{  
            _emailAddress=pData;  
        }  
  
        public function get phoneNumber():String{  
            return _phoneNumber;  
        }  
  
        public function set phoneNumber(pData:String):void{  
            _phoneNumber=pData;  
        }  
  
        public function get employeeCode():String{  
            return _employeeCode;  
        }  
  
        public function set employeeCode(pData:String):void{  
            _employeeCode=pData;  
        }  
		  
        public function get active():Boolean{  
            return _active;  
        }  
  
        public function set active(pData:Boolean):void{  
            _active=pData;  
        }  
	  
		public function get authorizationType():int{
			return _authorizationType;
		}

		public function set authorizationType(pData:int):void{
			_authorizationType = pData;
		}
		
        public function get administrator():Boolean{  
            return _administrator;  
        }  
  
        public function set administrator(pData:Boolean):void{  
            _administrator=pData;  
        }  
  
        public function get userPartners():ArrayCollection{  
            return _userPartners;  
        }  
  
        public function set userPartners(pData:ArrayCollection):void{  
            _userPartners=pData;  
        }  

        public function get channels():ArrayCollection{  
            return _channels;  
        }  
  
        public function set channels(pData:ArrayCollection):void{  
            _channels=pData;  
        }  
  
        public function get roles():ArrayCollection{  
            return _roles;  
        }  
  
        public function set roles(pData:ArrayCollection):void{  
            _roles=pData;  
        }  
  
        public function get endorses():ArrayCollection{  
            return _endorses;  
        }  
  
        public function set endorses(pData:ArrayCollection):void{  
            _endorses=pData;  
        }  
  
        public function get password():String{  
            return _password;  
        }  
  
        public function set password(pData:String):void{  
            _password=pData;  
        }  
  
        public function get username():String{  
            return _username;  
        }  
  
        public function set username(pData:String):void{  
            _username=pData;  
        }  
  
        public function get timeValidation():Date{  
            return _timeValidation;  
        }  
  
        public function set timeValidation(pData:Date):void{  
            _timeValidation=pData;  
        }  
  
        public function get timeLimit():int{  
            return _timeLimit;  
        }  
  
        public function set timeLimit(pData:int):void{  
            _timeLimit=pData;  
        }  

  		public function get transientSelectedPartner():int{  
            return _transientSelectedPartner;  
        }  
  
        public function set transientSelectedPartner(partnerId:int):void{  
            _transientSelectedPartner=partnerId;  
        } 
        
        public function get transientSelected():Boolean{  
            return _transientSelected;  
        }  
  
        public function set transientSelected(pData:Boolean):void{  
            _transientSelected=pData;  
        }  

  		public function get transientEditor():Boolean{  
            return _transientEditor;  
        }  
  
        public function set transientEditor(pData:Boolean):void{  
            _transientEditor=pData;  
        }  




    }  
}  