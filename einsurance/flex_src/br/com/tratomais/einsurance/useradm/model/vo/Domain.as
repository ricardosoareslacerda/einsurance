package br.com.tratomais.einsurance.useradm.model.vo
{
	import mx.collections.ArrayCollection;
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.Domain")]     
    [Managed]  
    public class Domain extends HibernateBean
    {  
        private var _domainId:int;  
        private var _name:String;  
        private var _autenticationType:int;  
        private var _partners:ArrayCollection;  
        private var _users:ArrayCollection;  
  
        public function Domain()  
        {  
        }  
  
        public function get domainId():int{  
            return _domainId;  
        }  
  
        public function set domainId(pData:int):void{  
            _domainId=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get autenticationType():int{  
            return _autenticationType;  
        }  
  
        public function set autenticationType(pData:int):void{  
            _autenticationType=pData;  
        }  
  
        public function get partners():ArrayCollection{  
            return _partners;  
        }  
  
        public function set partners(pData:ArrayCollection):void{  
            _partners=pData;  
        }  
  
        public function get users():ArrayCollection{  
            return _users;  
        }  
  
        public function set users(pData:ArrayCollection):void{  
            _users=pData;  
        }  
    }  
}