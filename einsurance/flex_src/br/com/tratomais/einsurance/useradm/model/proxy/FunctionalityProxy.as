package br.com.tratomais.einsurance.useradm.model.proxy
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.useradm.business.FunctionalityDelegateProxy;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	public class FunctionalityProxy extends Proxy implements IProxy 
	{
	
		public static const NAME:String = "FunctionalityProxy";

		public function FunctionalityProxy(data:Object= null){
			super(NAME, data );
		}
		
		public function listAllEdit():void{
			var delegate:FunctionalityDelegateProxy = new FunctionalityDelegateProxy(new Responder(onListAllResultEdit, onFault));
			delegate.listAllowedFunctionalityByUserLogged();
		}
		
		private function onListAllResultEdit(pResultEvt:ResultEvent):void
		{
			var listResult:ArrayCollection = pResultEvt.result as ArrayCollection;
         	sendNotification(NotificationList.FUNCTIONALITY_LISTED, listResult);
		}
		
		private function onFault(pFaultEvt:FaultEvent):void{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
		}
	}
}