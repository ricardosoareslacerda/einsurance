package br.com.tratomais.einsurance.useradm.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
  
    [RemoteClass(alias="br.com.tratomais.core.model.Login")]      
	[Managed]
    public class Login  extends HibernateBean
    {  
  
        private var _loginId:int;  
        private var _domain:String;  
        private var _login:String;  
        private var _passwordKey:String;  
        private var _expirePassword:Boolean;  
  
       public function Login()  
        {  
        }  
  
        public function get loginId():int{  
            return _loginId;  
        }  
  
        public function set loginId(pData:int):void{  
            _loginId=pData;  
        }  
  
        public function get domain():String{  
            return _domain;  
        }  
  
        public function set domain(pData:String):void{  
            _domain=pData;  
        }  
  
        public function get login():String{  
            return _login;  
        }  
  
        public function set login(pData:String):void{  
            _login=pData;  
        }  
  
        public function get passwordKey():String{  
            return _passwordKey;  
        }  
  
        public function set passwordKey(pData:String):void{  
            _passwordKey=pData;  
        }  
  
        public function get expirePassword():Boolean{  
            return _expirePassword;  
        }  
  
        public function set expirePassword(pData:Boolean):void{  
            _expirePassword=pData;  
        }  
        
    }  
} 