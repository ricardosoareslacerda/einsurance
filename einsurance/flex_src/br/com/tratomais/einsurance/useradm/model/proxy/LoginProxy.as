package br.com.tratomais.einsurance.useradm.model.proxy
{  
      
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.useradm.business.LoginDelegateProxy;
    import br.com.tratomais.einsurance.useradm.model.vo.Login;
    import br.com.tratomais.einsurance.useradm.model.vo.User;
    
    import mx.controls.Alert;
    import mx.resources.ResourceManager;
    import mx.rpc.Responder;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    
    import org.puremvc.as3.interfaces.IProxy;
    import org.puremvc.as3.patterns.proxy.Proxy;
	
    [Bindable]  
    public class LoginProxy extends Proxy implements IProxy   
    {  
		[Embed(source="/assets/images/error.png")]
		public var errorMsg:Class;
		      
        public static const NAME:String = "LoginProxy";  
		private static var BAD_CREDENTIALS:String = "org.springframework.transaction.UnexpectedRollbackException : Transaction rolled back because it has been marked as rollback-only";

   
        public function LoginProxy(data:Object = null) {  
            super(NAME, data );  
        }  
        
        public function validate(userName:String, password:String) : void{
		    var delegate:LoginDelegateProxy = new LoginDelegateProxy(new Responder(onValidateLoginResult, onFault));  
            delegate.validateLogin(userName, password);  
		}

		private function onValidateLoginResult(pResultEvt:ResultEvent):void  {  
            var user:User = pResultEvt.result as User ;
            if(user != null){
            	 sendNotification(NotificationList.LOGIN_VALIDATED, user);
            }else{
            	 sendNotification(NotificationList.LOGIN_NOT_VALIDATED);
            }
        } 

        public function loginExistence(login: String ) : void{
		    var delegate:LoginDelegateProxy = new LoginDelegateProxy(new Responder(onLoginExistence, onFault));  
            delegate.loginExistence(login); 
		}

		private function onLoginExistence(pResultEvt:ResultEvent) : void  {  
        	var result : Boolean = pResultEvt.result as Boolean;
      		sendNotification(NotificationList.USER_LOGIN_EXISTENCE, result);
        } 


        public function findByUser(user: User ) : void{
		    var delegate:LoginDelegateProxy = new LoginDelegateProxy(new Responder(onFindByUserLoginResult, onFault));  
            delegate.findByUser(user); 
		}
		
		public function saveObject(login: Login ) : void{
		    var delegate:LoginDelegateProxy = new LoginDelegateProxy(new Responder(onSaveObjectResult, onFault));  
            delegate.saveObject(login); 
		}
		
		private function onSaveObjectResult(pResultEvt:ResultEvent) : void  {  
            var login : Login = pResultEvt.result as Login ;
		//	return login;
        } 
       
        public function deleteObjectByLoginName(loginName: String, domain: String ) : void{
		    var delegate:LoginDelegateProxy = new LoginDelegateProxy(new Responder(onDeleteObjectByLoginNameResult, onFault));  
            delegate.deleteObjectByLoginName(loginName, domain); 
		}
		
		private function onDeleteObjectByLoginNameResult(pResultEvt:ResultEvent) : void  {  
    		//	return delete;
        } 
        
        private function onFindByUserLoginResult(pResultEvt:ResultEvent):void  {  
            var login:Login=pResultEvt.result as Login ;
         	 sendNotification(NotificationList.USER_LOGIN_VIEW, login);
        }  
   		
        public function logout() : void{
		    var delegate:LoginDelegateProxy = new LoginDelegateProxy(new Responder(onValidateLogoutResult, onFault));  
            delegate.logout();  
		}
		
		// -------------------------------------------------------------------------------------------
		
		/**
		 * Change password from user
		 */
		public function changeUserPassword( user: User, oldPassword:String , newPassword:String): void {
			var delegate:LoginDelegateProxy = new LoginDelegateProxy(new Responder(onPasswordChange, onFault));
			delegate.changeUserPassword(user, oldPassword, newPassword);
		} 

		private function onPasswordChange(pResultEvt:ResultEvent):void  {
        	var result : Boolean = pResultEvt.result as Boolean;
			sendNotification(NotificationList.PASSWORD_CHANGE, result);
        } 
		
		private function onValidateLogoutResult(pResultEvt:ResultEvent):void  {  
        } 

        public function auditLogger(value: String ) : void{
		    var delegate:LoginDelegateProxy = new LoginDelegateProxy(new Responder(onAuditLogger, onFault));  
            delegate.auditLogger(value);
		}
		
		private function onAuditLogger(pResultEvt:ResultEvent):void {
		}
		
   		private function onFault(pFaultEvt:FaultEvent):void  {  
   			
		  	if (pFaultEvt.fault.faultString == BAD_CREDENTIALS) {
	            Alert.show(ResourceManager.getInstance().getString("messages", "invalidLogin"), "Error", Alert.OK, null, null, errorMsg);  
		    } else {
	         	sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);  
       		 }
     	}  
    }  
}  