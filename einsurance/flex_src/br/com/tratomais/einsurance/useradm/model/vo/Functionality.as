package br.com.tratomais.einsurance.useradm.model.vo  
{
	import mx.collections.ArrayCollection;
	import mx.collections.IList;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
      
  
      
    [RemoteClass(alias="br.com.tratomais.core.model.Functionality")]      
	[Managed]
    public class Functionality  extends HibernateBean
    {  
  
        private var _functionalityId:int;  
        private var _functionality:Functionality;  
        private var _name:String;  
        private var _description:String;  
        private var _eventAction:String;
        private var _imageProperty:String;
        private var _messageProperty:String;  
        private var _displayOrder:int;
		private var _displayShow:Boolean;
        private var _active:Boolean;  
        private var _roles:ArrayCollection;  
        private var _functionalities:ArrayCollection;  
        private var _modules:ArrayCollection;
        private var _checked:uint;
        private var populateCompleted:Boolean;
  
  		public static const UNCHECKED : uint = 0;
		public static const CHECKED : uint = 1;
		public static const SEMI_UNCHECKED : uint = 2;
  
        public function Functionality(){}  
  
        public function get functionalityId():int{  
            return _functionalityId;  
        }  
  
        public function set functionalityId(pData:int):void{  
            _functionalityId=pData;  
        }  
  
        public function get functionality():Functionality{  
            return _functionality;  
        }  
  
        public function set functionality(pData:Functionality):void{  
            _functionality=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get description():String{  
            return _description;  
        }  
  
        public function set description(pData:String):void{  
            _description=pData;  
        }  
  
        public function get eventAction():String{  
            return _eventAction;  
        }  
  
        public function set eventAction(pData:String):void{  
            _eventAction=pData;  
        }  
		
		public function get imageProperty():String{
			return _imageProperty;
		}

		public function set imageProperty(pData:String):void{
			_imageProperty=pData;
		}
		
        public function get messageProperty():String{  
            return _messageProperty;  
        }  
  
        public function set messageProperty(pData:String):void{  
            _messageProperty=pData;  
        }  
  
        public function get displayOrder():int{  
            return _displayOrder;  
        }  
  
        public function set displayOrder(pData:int):void{  
            _displayOrder=pData;  
        }  

		public function get displayShow():Boolean{
			return _displayShow;
		}

		public function set displayShow(pData:Boolean):void{
			_displayShow=pData;
		}
		
        public function get active():Boolean{  
            return _active;  
        }  
  
        public function set active(pData:Boolean):void{  
            _active=pData;  
        }  
  
        public function get roles():ArrayCollection{  
            return _roles;  
        }  
  
        public function set roles(pData:ArrayCollection):void{  
            _roles=pData;  
        }  
  
        public function get functionalities():ArrayCollection{  
            return _functionalities;  
        }  
  
        public function set functionalities(pData:ArrayCollection):void{  
            _functionalities=pData;  
        }  
  
        public function get modules():ArrayCollection{  
            return _modules;  
        }  
  
        public function set modules(pData:ArrayCollection):void{  
            _modules=pData;  
        }  
  
		public function get label():String{
			return _name;
		}
		
		public function get icon():String{
			return _imageProperty;
		}
		
		public function set icon(entry:String) : void{
			
		}		

		public function set label(lab : String) : void{
			//this._name = lab;
		}
		
		public function set children(list : IList) : void{
		}
		
		public function get children():IList{
			if(_functionalities!= null && _functionalities.length == 0){
				return null;
			}
			var functionalitiesActive : ArrayCollection = new ArrayCollection();
			for each (var functionalitieChildren: Functionality in (_functionalities  as ArrayCollection)){
				if(functionalitieChildren.active == true){
					functionalitiesActive.addItem(functionalitieChildren);
				}
			}
			return functionalitiesActive;
		}


		public function get checked(): uint{
			return _checked;
		}

		public function set checked(pData : uint):void{
			_checked=pData;
			setChildren();
		}
		
		public function hasChildren():Boolean{
			if(_functionalities!= null &&_functionalities.length > 0 ){
				return true;
			}
			return false;
		}
		
		private function setChildren():void
		{
			populateCompleted = false;
			
			if(functionalities && functionalities.length > 0)
			{
				for each(var workFunctionality:Functionality in functionalities)
				{
					if(workFunctionality.active)
						workFunctionality.checked = this.checked;
				}
				
				populateCompleted = true;
			}
			
			if(functionality)
			{
				functionality.setParent();
			}
		}
		
		private function setParent():void
		{
			if(functionalities && functionalities.length > 0 && populateCompleted)
			{
				var quantChecked:int = 0;
				var isMiddle:Boolean = false;
				var quantActive:int = 0;
				
				for each(var workFunctionality:Functionality in functionalities)
				{
					if(workFunctionality.active)
					{
						quantActive++;
					}
					else
					{
						workFunctionality._checked = UNCHECKED;
					}
					
					if(workFunctionality.checked == CHECKED)
					{
						quantChecked++;
					}
					else if(workFunctionality.checked == SEMI_UNCHECKED)
					{
						isMiddle = true;
						break;
					}
				}
				
				if(quantChecked == quantActive)
				{
					this._checked = CHECKED;
				} 
				else if(isMiddle || quantChecked > 0)
				{
					this._checked = SEMI_UNCHECKED;
				} 
				else 
				{
					this._checked = UNCHECKED;
				}

				if(functionality)
				{
					functionality.setParent();
				}
			}
		}
          
    }  
}  