package br.com.tratomais.einsurance.useradm.model.proxy  
{  
      
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.useradm.business.RoleDelegateProxy;
    import br.com.tratomais.einsurance.useradm.model.vo.Role;
    import br.com.tratomais.einsurance.useradm.model.vo.User;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.rpc.Responder;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    
    import org.puremvc.as3.interfaces.IProxy;
    import org.puremvc.as3.patterns.proxy.Proxy;  
          
      
    [Bindable]  
    public class RoleProxy extends Proxy implements IProxy   
    {  
      
        public static const NAME:String = "RoleProxy";  
  
        public function RoleProxy(proxyName:String=null, data:Object=null){  
            super(NAME, data );  
        }  

        public function listAllActiveRole():void{  
            var delegate:RoleDelegateProxy = new RoleDelegateProxy(new Responder(onlistAllActiveRole, onFault));  
            delegate.listAllActiveRole();  
        }  
  
        private function onlistAllActiveRole(pResultEvt:ResultEvent):void{  
            var result:ArrayCollection=pResultEvt.result as ArrayCollection ;  
            sendNotification(NotificationList.ROLE_LIST_REFRESH,result);  
        } 
  
        public function listAll():void  {  
            var delegate:RoleDelegateProxy = new RoleDelegateProxy(new Responder(onListAllResult, onFault));  
            delegate.listAll();  
        }  
  
        private function onListAllResult(pResultEvt:ResultEvent):void{  
            var result:ArrayCollection=pResultEvt.result as ArrayCollection ;  
            sendNotification(NotificationList.ROLE_LIST_REFRESH,result);  
        } 
        
        public function listAllRoles():void{  
            var delegate:RoleDelegateProxy = new RoleDelegateProxy(new Responder(onListAllRolesResult, onFault));  
            delegate.listAll();  
        }  
  
        private function onListAllRolesResult(pResultEvt:ResultEvent):void  {  
            var result:ArrayCollection=pResultEvt.result as ArrayCollection ;  
            sendNotification(NotificationList.ROLE_MAIN_LIST_REFRESH,result);  
        }  
 
        public function findById(id:int):void  {  
            var delegate:RoleDelegateProxy = new RoleDelegateProxy(new Responder(onFindByIdResult, onFault));  
            delegate.findById(id);  
        }  
  
        private function onFindByIdResult(pResultEvt:ResultEvent):void {  
            var result:Role=pResultEvt.result as Role ;  
        }  
  
        public function findByName(name:String):void {  
            var delegate:RoleDelegateProxy = new RoleDelegateProxy(new Responder(onFindByNameResult, onFault));  
            delegate.findByName(name);  
        }  
  
        private function onFindByNameResult(pResultEvt:ResultEvent):void  {  
            var result:ArrayCollection=pResultEvt.result as ArrayCollection ;  
        }  
          
        public function listAllByUser(user:User):void  {  
            var delegate:RoleDelegateProxy = new RoleDelegateProxy(new Responder(onListAllByUserResult, onFault));  
            delegate.listAllByUser(user);  
        }  
  
        private function onListAllByUserResult(pResultEvt:ResultEvent):void  {  
            var result:ArrayCollection=pResultEvt.result as ArrayCollection ;  
            sendNotification(NotificationList.USER_ROLES_REFRESH,result);  
        }  
  
  		public function saveObject(role: Role) : void{
            var delegate:RoleDelegateProxy = new RoleDelegateProxy(new Responder(onSaveResult, onFault));  
            delegate.saveObject(role);  
  		}
  		
  		private function onSaveResult(pResultEvent : ResultEvent) : void{
            var role:Role =pResultEvent.result as Role;
            sendNotification(NotificationList.ROLE_SAVE_SUCCESS);
  		}
  		
        private function onFault(pFaultEvt:FaultEvent):void{
            sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
        }
    }
}  