package br.com.tratomais.einsurance.useradm.model.proxy
{  
      
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.useradm.business.UserDelegateProxy;
    import br.com.tratomais.einsurance.useradm.model.vo.Login;
    import br.com.tratomais.einsurance.useradm.model.vo.User;
    
    import mx.collections.ArrayCollection;
    import mx.rpc.Responder;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    
    import org.puremvc.as3.interfaces.IProxy;
    import org.puremvc.as3.patterns.proxy.Proxy;  
        
      
    [Bindable]  
    public class UserProxy extends Proxy implements IProxy   
    {  
      
        public static const NAME:String = "UserProxy"; 
		
		[Embed(source="/assets/images/error.png")]
		public var errorMsg:Class;
		    	
        public function UserProxy( data:Object = null)  
        {  
            super(NAME, data );  
        }  
 
 		public function listUserAllowed():void
		{
			var delegate:UserDelegateProxy = new UserDelegateProxy(new Responder(onListResult, onFault));  
			delegate.listUserAllowed();
		} 
		
		private function onListResult(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.USER_LIST_REFRESH, collection);
        }  
        
        public function findByName(name: String):void	{
			var delegate:UserDelegateProxy = new UserDelegateProxy(new Responder(onFindByNameResult, onFault));  
			delegate.findByName(name);
		} 
		
		private function onFindByNameResult(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.USER_LIST_REFRESH, collection);
        }  

        public function saveObject(user : User, login : Login):void	{
			var delegate:UserDelegateProxy = new UserDelegateProxy(new Responder(onSaveObjectResult, onFault));  
			delegate.saveObject(user, login);
			sendNotification(NotificationList.USER_SAVE_SUCCESS, user);
		} 

        public function saveObjectInsert(user : User, login : Login):void	{
			var delegate:UserDelegateProxy = new UserDelegateProxy(new Responder(onSaveObjectResult, onFault));  
			delegate.saveObjectInsert(user, login);
		} 

		private function onSaveObjectResult(pResultEvt:ResultEvent):void  {  
			var user : User = pResultEvt.result as User;
        	sendNotification(NotificationList.USER_SAVE_SUCCESS, user);
        }  
        
        public function changePartnerDefaultLoggedUser(partnerId : int):void
		{
			var delegate:UserDelegateProxy = new UserDelegateProxy(new Responder(onChangePartnerDefaultLoggedUser, onFault));  
			delegate.changePartnerDefaultLoggedUser(partnerId);
		} 
		
		public function listAllDomainActive():void
		{
			var delegate:UserDelegateProxy = new UserDelegateProxy(new Responder(onListDomainResult, onFault));  
			delegate.listAllDomainActive();
		} 
		
		private function onListDomainResult(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.DOMAIN_LIST_REFRESH, collection);
        }  
		
		public function listAllPartnerChannel(user: User):void
		{
			var delegate:UserDelegateProxy = new UserDelegateProxy(new Responder(onListAllPartnerChannel, onFault));  
			delegate.listAllPartnerChannel(user);
		} 
		
		private function onListAllPartnerChannel(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.PARTNER_CHANNEL_LIST_REFRESH, collection);
        }  

   	 	private function onChangePartnerDefaultLoggedUser(pResultEvt:ResultEvent) : void
   	 	{
   	 	}
   	 	
		public function listUserPerPartner(partner:int):void
		{
			var delegate:UserDelegateProxy = new UserDelegateProxy(new Responder(onListUserPerPartner, onFault));  
			delegate.listUserPerPartner(partner);
		} 
		
		private function onListUserPerPartner(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.USER_LIST_PARTNER, collection);
        }  
   	 	
   		private function onFault(pFaultEvt:FaultEvent):void  {  
            sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
        }   	 	
    }  
}  