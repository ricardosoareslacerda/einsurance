////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2009 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 17/12/2009
//
// NOTICE: Trato+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.useradm.view.mediator
{ 
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.useradm.model.vo.Role;
	import br.com.tratomais.einsurance.useradm.view.components.RolesForm;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;  
	
    public class RolesFormMediator extends Mediator implements IMediator  
	    {  
	          
	    public static const NAME:String = 'RolesFormMediator';  
          
          
        public function RolesFormMediator(viewComponent:Object)  
        {  
            super(NAME, viewComponent);  
        }  
          
        public function get roleForm():RolesForm{  
            return viewComponent as RolesForm;  
        }         
       
         public function clearForm( ) : void {
        	if(roleForm!= null){
	        	roleForm.chkActive.selected = false;
	        	roleForm.txtName.text = "";
	        	roleForm.txtDescription.text = "";

	        	roleForm.txtName.errorString = "";
	        	roleForm.txtDescription.errorString = "";
        	}
        }  
        
        public function enabledForm(valor:Boolean) : void {
        	roleForm.chkActive.enabled=valor;
        	roleForm.txtName.enabled=valor;
        	roleForm.txtDescription.enabled=valor;
        }  
                
        public function populateRoleToForm(role : Role) : void {
        	role.active = roleForm.chkActive.selected;
        	role.name = roleForm.txtName.text;
        	role.description = roleForm.txtDescription.text ;
        }  
        
        public function populateFormToRole( role : Role) : void{
        	roleForm.chkActive.selected = role.active;
        	roleForm.txtName.text = role.name;
        	roleForm.txtDescription.text = role.description;
        }

        
        
		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */
		override public function onRegister():void 
		{
			super.onRegister();
		}
		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			this.clearForm();
			setViewComponent(null);

			super.onRemove();
		}
		
			
          
        override public function listNotificationInterests():Array  
        {  
            return [
             ];  
        }  
          
        override public function handleNotification(notification:INotification):void  
        {  
              
            switch ( notification.getName() )  
            {  
		        default : break;  
            }  
                  
        } 
	}
}  