////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2009 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 17/12/2009
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.useradm.view.mediator
{  
    import br.com.tratomais.einsurance.ApplicationFacade;
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.useradm.model.proxy.LoginProxy;
    import br.com.tratomais.einsurance.useradm.view.components.Login;
    
    import flash.events.Event;
    
    import mx.controls.Alert;
    import mx.resources.ResourceManager;
    
    import org.puremvc.as3.interfaces.IMediator;
    import org.puremvc.as3.interfaces.INotification;
    import org.puremvc.as3.patterns.mediator.Mediator;
  	
    public class LoginFormMediator extends Mediator implements IMediator  
    {  
          
        /**
        * 
        */  
        private var loginProxy:LoginProxy;  
	    public static const NAME:String = 'LoginFormMediator';  
    	
    	[Bindable]
		[Embed(source="/assets/images/warning.png")]
		public var warningMsg:Class;
		                    
        public function LoginFormMediator(viewComponent:Object){  
            super(NAME, viewComponent);  
            loginProxy = LoginProxy( facade.retrieveProxy( LoginProxy.NAME ) );
        }  
          
        private function validate(event:Event):void  {  
        	var login : String = loginForm.txtUser.text;
        	if(login.indexOf("@",0) > -1){
	        	loginProxy.validate(login, loginForm.txtPassword.text );
        	}else{
				Alert.show(ResourceManager.getInstance().getString("messages", "invalidLogin"), "Atención", Alert.OK, null, null, warningMsg);
	       	}
       }  
              
	    protected function get loginForm(): Login {
            return viewComponent as Login;
        }
                         
        override public function listNotificationInterests():Array{  
            return [ 
            	 NotificationList.LOGIN_NOT_VALIDATED           	 
             ];  
        }  
        
        public function enabledForm(valor:Boolean):void{
        	loginForm.txtUser.enabled=valor;
        	loginForm.lblPassword.enabled=valor;
        }
        
		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */
		override public function onRegister():void 
		{
			super.onRegister();
			loginForm.addEventListener(Login.EVENT_LOGIN_VALIDATE, validate);  
    	}
		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
			loginForm.removeEventListener(Login.EVENT_LOGIN_VALIDATE, validate);
			loginForm.resetLogin();
			setViewComponent(null);
		}    
		      
		/**
		 * 
		 * **/
        override public function handleNotification(notification:INotification):void {  
       	
       		switch ( notification.getName() ){  
        		case NotificationList.LOGIN_NOT_VALIDATED:
					Alert.show(ResourceManager.getInstance().getString("messages", "invalidLogin"), "Atención", Alert.OK, null, null, warningMsg);
					break;
            	}  
        }  
    }  
    
} 