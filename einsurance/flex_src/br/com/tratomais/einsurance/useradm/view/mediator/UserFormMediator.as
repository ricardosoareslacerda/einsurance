////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 04/01/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.useradm.view.mediator
{  
    import br.com.tratomais.einsurance.useradm.model.proxy.UserProxy;
    import br.com.tratomais.einsurance.useradm.model.vo.Domain;
    import br.com.tratomais.einsurance.useradm.model.vo.User;
    import br.com.tratomais.einsurance.useradm.view.components.UserForm;
    
    import mx.validators.Validator;
   	import mx.collections.ArrayCollection;

    import org.puremvc.as3.interfaces.IMediator;
    import org.puremvc.as3.interfaces.INotification;
    import org.puremvc.as3.patterns.mediator.Mediator;  
  
    public class UserFormMediator extends Mediator implements IMediator  
    {  
          
        private var userProxy:UserProxy;  
  
        public static const NAME:String = 'UserFormMediator';  
                          
        public function UserFormMediator( viewComponent:Object )  
        {  
            super(NAME, viewComponent);  
            userProxy = UserProxy( facade.retrieveProxy( UserProxy.NAME ) );
        } 

       	protected function get userForm():UserForm
		{
            return viewComponent as UserForm;
        }          
        
        public function populateFormToUser(user:User ): void{
			userForm.txtName.text = user.name;
			userForm.txtEmail.text = user.emailAddress;
			userForm.txtPhone.text = user.phoneNumber;
			userForm.txtEmployeeCode.text = user.employeeCode;
			userForm.chkAdministrator.selected = user.administrator;
			userForm.chkActive.selected = user.active;
			if(user.domain!= null){
				userForm.cmbxDomain.selectedValue=user.domain.domainId.toString();
			}
		}	

        public function populateUserToForm(user:User ): void{
			user.name = userForm.txtName.text;
			user.emailAddress = userForm.txtEmail.text;
			user.phoneNumber = userForm.txtPhone.text;
			user.employeeCode = userForm.txtEmployeeCode.text;
			user.administrator = userForm.chkAdministrator.selected;
			user.active = userForm.chkActive.selected;
			if(userForm.cmbxDomain.selectedItem != null){
				user.domain = userForm.cmbxDomain.selectedItem as Domain;
			}
		}	

       public function resetForm():void{
			userForm.txtName.text = "";
			userForm.txtName.errorString = "";
			userForm.txtEmail.text = "";
			userForm.txtEmail.errorString = "";
			userForm.txtPhone.text = "";
			userForm.txtPhone.errorString = "";
		    userForm.txtEmployeeCode.text = "";
		    userForm.txtEmployeeCode.errorString = "";
			userForm.chkAdministrator.errorString = "";
			userForm.chkAdministrator.selected = false;
			userForm.chkActive.selected = false;
			userForm.chkActive.errorString = "";
			userForm.cmbxDomain.selectedIndex = -1;
			userForm.cmbxDomain.errorString = "";
       }
       
		public function enabledForm(valor:Boolean):void{
			userForm.txtName.enabled=valor;
			userForm.txtEmail.enabled=valor;
			userForm.txtPhone.enabled=valor;
			userForm.txtEmployeeCode.enabled=valor;
			userForm.chkAdministrator.enabled=valor;
			userForm.chkActive.enabled=valor;     
			userForm.cmbxDomain.enabled = valor;  	
		}
      
        /**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */
		override public function onRegister():void 
		{
			super.onRegister();
			
    	}
		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
			this.resetForm();		
		}
              
         override public function listNotificationInterests():Array  
        {  
            return [ ];  
        } 
              
        override public function handleNotification(notification:INotification):void  
        {  
               
        }
    }  
} 