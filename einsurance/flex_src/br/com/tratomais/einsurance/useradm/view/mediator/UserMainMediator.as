////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2009 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 17/12/2009
//
// NOTICE: Trato+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.useradm.view.mediator
{  
    import br.com.tratomais.einsurance.ApplicationFacade;
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.core.Utilities;
    import br.com.tratomais.einsurance.core.components.DoubleClickManager;
    import br.com.tratomais.einsurance.core.proxy.UtilProxy;
    import br.com.tratomais.einsurance.customer.model.proxy.PartnerProxy;
    import br.com.tratomais.einsurance.useradm.model.proxy.LoginProxy;
    import br.com.tratomais.einsurance.useradm.model.proxy.RoleProxy;
    import br.com.tratomais.einsurance.useradm.model.proxy.UserProxy;
    import br.com.tratomais.einsurance.useradm.model.vo.Login;
    import br.com.tratomais.einsurance.useradm.model.vo.User;
    import br.com.tratomais.einsurance.useradm.view.components.UserForm;
    import br.com.tratomais.einsurance.useradm.view.components.UserMain;
    
    import flash.events.Event;
    import flash.events.MouseEvent;
    
    import mx.collections.ArrayCollection;
    import mx.resources.ResourceManager;
    import mx.validators.Validator;
    
    import org.puremvc.as3.interfaces.IMediator;
    import org.puremvc.as3.interfaces.INotification;
    import org.puremvc.as3.patterns.mediator.Mediator;  
  
    /**
     *  The UserMainMediator class is a
     */
    public class UserMainMediator extends Mediator implements IMediator  
    {  
    	
        public static const NAME:String = 'UserMainMediator';  
		private var doubleCLickManager:DoubleClickManager = null; 
		private var user:User;  
		//private var isAdd:Boolean = new Boolean(false);
		
		private var userProxy:UserProxy; 
		private var roleProxy:RoleProxy;
		private var partnerProxy:PartnerProxy; 
       	private var loginProxy:LoginProxy;
       	private var utilProxy:UtilProxy;

		private var userLoginMediator:UserLoginMediator;
		private var userPartnerFormMediator:UserPartnerFormMediator;
		private var userRolesMediator:UserRolesMediator;
		private var userFormMediator:UserFormMediator;
				
	    public function UserMainMediator(viewComponent:Object)  
        {  
            super(NAME, viewComponent);  
            userProxy 			= UserProxy(facade.retrieveProxy(UserProxy.NAME));
            roleProxy 			= RoleProxy(facade.retrieveProxy(RoleProxy.NAME));
            loginProxy 			= LoginProxy(facade.retrieveProxy(LoginProxy.NAME));
            utilProxy 			= UtilProxy(facade.retrieveProxy(UtilProxy.NAME));
			partnerProxy 		= PartnerProxy(facade.retrieveProxy(PartnerProxy.NAME));
        }  
          
        public function get userMain():UserMain{  
            return viewComponent as UserMain;  
        }
          
		override public function onRegister():void 
		{
			super.onRegister();
			
			try{
				userLoginMediator = new UserLoginMediator(userMain.userLogin); 
				userPartnerFormMediator = new UserPartnerFormMediator(userMain.userPartner);
				userRolesMediator = new UserRolesMediator(userMain.userRoles); 
				userFormMediator = new UserFormMediator(userMain.userForm);
				
				ApplicationFacade.getInstance().registerOnlyNewMediator(userLoginMediator);
				ApplicationFacade.getInstance().registerOnlyNewMediator(userPartnerFormMediator);
				ApplicationFacade.getInstance().registerOnlyNewMediator(userRolesMediator);
				ApplicationFacade.getInstance().registerOnlyNewMediator(userFormMediator);

				userMain.addEventListener(UserMain.SAVE_ITEM, saveItem);  
				userMain.addEventListener(UserMain.ADD_ITEM, addItem);  
				userMain.addEventListener(UserMain.EDIT_ITEM, editItem);
				userMain.addEventListener(UserMain.CANCEL_ITEM, cancelItem);  
				userMain.userForm.addEventListener(UserForm.ACTIVE, activePartner);  
				userMain.addEventListener(UserMain.INITIALIZE, initializeUser);  
	
				doubleCLickManager = new DoubleClickManager(userMain.dtgUser);
				doubleCLickManager.addEventListener(MouseEvent.CLICK, viewItem);
				doubleCLickManager.addEventListener(MouseEvent.DOUBLE_CLICK, changeItem);
				
				this.onClean();
			}
			catch(error:Error){
				throw error;	
			}
			
    	}      
        
		override public function onRemove():void 
		{
			ApplicationFacade.getInstance().removeMediator(userLoginMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(userPartnerFormMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(userRolesMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(userFormMediator.getMediatorName());

			userMain.removeEventListener(UserMain.SAVE_ITEM, saveItem);  
			userMain.removeEventListener(UserMain.ADD_ITEM, addItem);  
			userMain.removeEventListener(UserMain.EDIT_ITEM, editItem);
			userMain.removeEventListener(UserMain.CANCEL_ITEM, cancelItem);  
			userMain.userForm.removeEventListener(UserForm.ACTIVE, activePartner);  
			userMain.removeEventListener(UserMain.INITIALIZE, initializeUser);  

			doubleCLickManager.removeEventListener(MouseEvent.CLICK, viewItem);
			doubleCLickManager.removeEventListener(MouseEvent.DOUBLE_CLICK, changeItem);			

			super.onRemove();
		}
		
        override public function listNotificationInterests():Array  
        {  
            return [  
   		 	        NotificationList.USER_LIST_REFRESH,
   		 	        NotificationList.ROLE_LIST_REFRESH,
   		 	        NotificationList.USER_MAIN_GENERAL_MESSAGE,
   		 	        NotificationList.USER_SAVE_SUCCESS,
	            	NotificationList.USER_ROLES_REFRESH,
   		 	        NotificationList.ROLE_MAIN_LIST_REFRESH,
               	 	NotificationList.PARTNER_LIST_REFRESH,
               	 	NotificationList.USER_PARTNER_REFRESH,
               	 	NotificationList.DOMAIN_LIST_REFRESH,
               	 	NotificationList.PARTNER_CHANNEL_LIST_REFRESH
   		            ];  
        }  
          
        override public function handleNotification(notification:INotification):void  
        {  
            switch ( notification.getName() )  
            {  
        		case NotificationList.USER_LIST_REFRESH:
        			var usersCollection:ArrayCollection = notification.getBody() as ArrayCollection;
        			userMain.userCollection = usersCollection;
		        	break;  
 				case NotificationList.USER_MAIN_GENERAL_MESSAGE:
 					var message: String = notification.getBody() as String;
					break;
 				case NotificationList.USER_SAVE_SUCCESS:
	             	this.onClean();
	             	this.initializeUser(null);
		      		userMain.dtgUser.enabled = true;
					userMain.accordionContainer.selectedIndex = 0;
					this.enabledForms(false);
					
					var log:String;
					log = ApplicationFacade.getInstance().loggedUser.login;
					log += ":" + ApplicationFacade.getInstance().loggedUser.domain.name;
					log += ":USER";
					log += ":SAVE";
					ApplicationFacade.getInstance().auditLogger = log;					
										
					break;
		        case NotificationList.ROLE_LIST_REFRESH:
            	case NotificationList.USER_ROLES_REFRESH:
					userRolesMediator.populateFormToRole(notification.getBody() as ArrayCollection);
        			break;
	            case NotificationList.PARTNER_LIST_REFRESH:
            	case NotificationList.USER_PARTNER_REFRESH:
					userPartnerFormMediator.populateFormToListPartners(notification.getBody() as ArrayCollection);
                 	break;
               case NotificationList.DOMAIN_LIST_REFRESH:
               		var list:ArrayCollection = notification.getBody() as ArrayCollection;
					userMain.userForm.cmbxDomain.dataProvider = list;
                 	break;  	
               case NotificationList.PARTNER_CHANNEL_LIST_REFRESH:
					var list:ArrayCollection = notification.getBody() as ArrayCollection;
					userMain.userPartnerChannel.dgrListPartnerChannel.dataProvider = list;
 		    }
        }
         
         
        private function initializeUser(event : Event):void{
        	userProxy.listUserAllowed();
      		userProxy.listAllDomainActive();
    		userMain.imgNew.enabled = this.checkAllowed();
        	this.enabledForms(false);
        }  
		
      	private function getListsUser(user:User):void{
        	if(user == null){
	    		var loggedUser : User = ApplicationFacade.getInstance().loggedUser;
 				partnerProxy.listPartnersByUserAll(null);
				userProxy.listAllPartnerChannel(loggedUser);
				if(loggedUser.administrator == true){
   					roleProxy.listAllActiveRole();
        		}else{
	        		userRolesMediator.populateFormToRole(loggedUser.roles);
        		}	
	    	}else{
				partnerProxy.listPartnersByUserAll(user);
				userProxy.listAllPartnerChannel(user); 
    			loginProxy.findByUser( user );
				roleProxy.listAllByUser(user);
        	} 
        }     	
      	
		private function viewItem(event:Event):void  {
			var user: User = this.getObjectSelect();
			if(editorIsEnabled()==false && user != null){
				this.enabledForms(false);	
				userFormMediator.populateFormToUser(user);
				this.getListsUser(user);
			}
       		userPartnerFormMediator.editor = false;		
       		userRolesMediator.editor = false;		
      	}
      	
		private function changeItem(event:Event):void  {
			if(this.checkAllowed()){
	  			var user: User = this.getObjectSelect();
				if(editorIsEnabled()== false && user != null){
					this.enabledForms(true);				
					userMain.imgNew.enabled=false;
					userMain.imgSave.enabled=true;
					userMain.imgUndo.enabled=true;
					userMain.dtgUser.enabled = false;
					userFormMediator.populateFormToUser(user);
					this.getListsUser(user);
	        	}
	        	userMain.userLogin.vldTxtNewPasswordKey.required = false;
	     		userPartnerFormMediator.editor = true;		
	       		userRolesMediator.editor = true;		
			}else{
				Utilities.warningMessage("thisUserNotAreAllowedAsAdministrator");
			}
		}
		
		public function editorIsEnabled() : Boolean{
			var isEnabled:Boolean = false;
			if(userMain.dtgUser.enabled == false){
				isEnabled = true;
			}
			return isEnabled;
		} 
		      	
      	private function checkAllowed() : Boolean{
			var userLogged : User = ApplicationFacade.getInstance().loggedUser;
      		if(userLogged.administrator == false){
      			return false;
      		}
			return true;
      	}

		private function validateChildren() : Boolean{
			var allItens : Array = new Array();
			for each (var validator : Validator in userMain.userForm.validatorForm){
				allItens.push(validator);
			}
			for each (var validator : Validator in userMain.userLogin.validatorForm){
				allItens.push(validator);
			}
			var result : Boolean = Utilities.validateForm(allItens, ResourceManager.getInstance().getString('resources', 'userdetails'));
			return result;
		}

      	private function saveItem(event:Event):void  {
			var isAdd:Boolean = new Boolean(false);
			user = this.getObjectSelect();
			if(user == null){
	  			isAdd = true;
	  			user = new User();
	  		}
			if(this.validateChildren()){
				if(userPartnerFormMediator.validateForm()){
					if(userPartnerFormMediator.validateSelectPartnerDefault()){
			  			if(userLoginMediator.isExistsLogin() == false){
							if(this.checkAllowed()){
								this.executeSaveItem(user, isAdd);
							} else{
								Utilities.warningMessage("thisUserNotAreAllowedAsAdministrator");
							}
			  			} else{
			    			Utilities.warningMessage("existenceLogin");
							userMain.accordionContainer.selectedIndex = 1;
			  			}
		  			} else{
						userMain.accordionContainer.selectedIndex = 3;
						Utilities.warningMessage("pleaseSelectDefaultPartner");
		  			}
	  			} else{
					userMain.accordionContainer.selectedIndex = 3;
					Utilities.warningMessage("pleaseSelectPartner");
	  			}
	  		} else{
				userMain.accordionContainer.selectedIndex = 0;
			 }
		}      	
		
		private function executeSaveItem(user : User, isAdd : Boolean){
			userFormMediator.populateUserToForm(user);
			var login : Login = userLoginMediator.fillLoginUser(user);
			userPartnerFormMediator.populateUserPartnersToForm(user);
			userRolesMediator.populateUserToForm(user);
			if(isAdd){
				userProxy.saveObjectInsert(user, login);	
			}else{
				userProxy.saveObject(user,login);
			}
			userMain.imgNew.enabled=true;
			userMain.imgSave.enabled=false;
			userMain.imgUndo.enabled=true;
			userMain.dtgUser.enabled = true;				
      	}
      	
		/**
		 * Comments
		 */        
        private function cancelItem(event:Event):void{
			this.onClean();	      		
			userMain.dtgUser.enabled = true;
			userMain.accordionContainer.selectedIndex = 0;
			this.enabledForms(false);			
      	}
      	
      	public function enabledForms(valor:Boolean):void{
      		userFormMediator.enabledForm(valor);
      		userLoginMediator.enabledForm(valor);
      		userPartnerFormMediator.enabledForm(valor);
      		userRolesMediator.enabledForm(valor);
      	}
      	
      	private function getObjectSelect() : User {
      		var obj : Object = userMain.dtgUser.selectedItem;
      		var user : User = null;
      		if(obj != null){
      			return obj as User;
      		}else{
      			return null;
      		}
      	}
     	
     	private function onClean():void{
     		userFormMediator.resetForm();
     		userLoginMediator.resetForm();
     		userRolesMediator.resetForm();  
     		userPartnerFormMediator.resetForm();
     		userMain.userPartnerChannel.dgrListPartnerChannel.dataProvider = null;
     	}
     	
      	private function addItem(event:Event):void  {
  			userMain.dtgUser.selectedItem = null;
        	userMain.dtgUser.enabled = false;
			userMain.accordionContainer.selectedIndex = 0;
			this.enabledForms(true);			
			this.onClean();
    		this.getListsUser(null);
    		userMain.userForm.chkActive.selected = true;
        	userMain.userLogin.vldTxtNewPasswordKey.required = true;
      		userPartnerFormMediator.editor = true;	
       		userRolesMediator.editor = true;		
		}
		
		private function editItem(event:Event):void  {
      		var obj:Object = userMain.dtgUser.selectedItem;
			if(obj != null){
				userFormMediator.populateFormToUser(obj as User);
				userMain.dtgUser.enabled = false;
				this.getListsUser(obj as User);
			}

       		userPartnerFormMediator.editor = true;	
       		userRolesMediator.editor = true;		
		}
		
		private function activePartner(event : Event) : void {
			userPartnerFormMediator.checkedPartners(userMain.userForm.chkAdministrator.selected);
		}
    }  
}  