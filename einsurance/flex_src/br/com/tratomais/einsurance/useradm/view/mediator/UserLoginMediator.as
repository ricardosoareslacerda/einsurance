////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2009 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 17/12/2009
//
// NOTICE: Trato+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.useradm.view.mediator 
{  
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.core.Utilities;
    import br.com.tratomais.einsurance.core.proxy.UtilProxy;
    import br.com.tratomais.einsurance.useradm.model.proxy.LoginProxy;
    import br.com.tratomais.einsurance.useradm.model.vo.Login;
    import br.com.tratomais.einsurance.useradm.model.vo.User;
    import br.com.tratomais.einsurance.useradm.view.components.UserLogin;
    
    import flash.events.Event;
    
    import org.puremvc.as3.interfaces.IMediator;
    import org.puremvc.as3.interfaces.INotification;
    import org.puremvc.as3.patterns.mediator.Mediator;  
  
    public class UserLoginMediator extends Mediator implements IMediator  
    {  

        public static const NAME:String = 'UserLoginMediator';  
        private var loginObject : Login;
       	private var loginProxy: LoginProxy;
       	private var utilProxy: UtilProxy;
        private var loginExistenceValue : Boolean;
          
        public function UserLoginMediator(viewComponent:Object)  
        {  
            super(NAME, viewComponent);  
            loginProxy = LoginProxy(facade.retrieveProxy(LoginProxy.NAME));
            utilProxy = UtilProxy(facade.retrieveProxy(UtilProxy.NAME));
        }  
          
        public function get userLogin():UserLogin{  
            return viewComponent as UserLogin;  
        }         
 
      	public function fillLoginUser(user: User ): Login{
     		if(loginObject == null){
     			loginObject = new Login();
     		}
			loginObject.login = userLogin.txtLogin.text;
			if(userLogin.txtNewPasswordKey.text != ""){
				if(user.domain != null){
					loginObject.passwordKey = userLogin.txtNewPasswordKey.text;
		    		user.login = loginObject.login;
					loginObject.domain = user.domain.name;
				}
				return loginObject;
			}
			return null;
		}
    	
    	public function loginExistence(event : Event) : void {
    		var login : String = userLogin.txtLogin.text;
    		if(login != null && login != ""){
				loginProxy.loginExistence(login);			    			
    		}
    	}
   	
    	public function verifyLoginExistence(result : Boolean ) : void{
    		userLogin.lblMessage.label = "";
			if(result == true){
				loginExistenceValue = true;
				sendNotification(NotificationList.USER_MAIN_GENERAL_MESSAGE,"LOGIN JA EXISTE"); 
    		}else{
    			loginExistenceValue = false;
 				sendNotification(NotificationList.USER_MAIN_GENERAL_MESSAGE,"LOGIN Válido"); 
    		}
    	}
    		
		public function resetForm(): void{
			loginObject = null;
			userLogin.txtLogin.text = '';
			userLogin.txtLogin.errorString = "";
			userLogin.txtNewPasswordKey.text = '';
			userLogin.txtNewPasswordKey.errorString = "";
//			userLogin.chkExpirePassword.selected = false;
		}	
		
		public function enabledForm(valor:Boolean): void{
			userLogin.txtLogin.enabled=valor;
			userLogin.txtNewPasswordKey.enabled=valor;
//			userLogin.chkExpirePassword.enabled=valor;
		}	
		     	
		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */
		override public function onRegister():void 
		{
			super.onRegister();
			userLogin.addEventListener("USER_LOGIN_EXISTENCE", loginExistence);
		}
		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
			userLogin.removeEventListener("USER_LOGIN_EXISTENCE", loginExistence);
			this.resetForm();
		}
		    
                  
        override public function listNotificationInterests():Array  
        {  
            return [NotificationList.USER_LOGIN_VIEW,
       				NotificationList.USER_LOGIN_EXISTENCE];  
        }
        
        override public function handleNotification(notification:INotification):void  
        {  
            switch ( notification.getName() )  {
		        case NotificationList.USER_LOGIN_VIEW:
		        	var login: Login = notification.getBody() as Login;
		        	this.fillLogin(login);
		        	this.loginObject = login;
	            	break;
        	     case NotificationList.USER_LOGIN_EXISTENCE:
        	     	var result: Boolean = notification.getBody() as Boolean
					this.verifyLoginExistence(result);
		    		break;
            }
    	}
    	
     	public function fillLogin(login: Login ): void{
   			this.resetForm();
     		if(login != null){
				userLogin.txtLogin.text = login.login;
				userLogin.txtLogin.enabled = false;
	   		}
		}
    	
    	public function isExistsLogin() : Boolean{
    		return loginExistenceValue;
    	}
	}  
}  