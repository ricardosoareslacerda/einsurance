////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2009 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 17/12/2009
//
// NOTICE: Trato+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.useradm.view.mediator {  
    import br.com.tratomais.einsurance.ApplicationFacade;
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.core.Utilities;
    import br.com.tratomais.einsurance.core.components.DoubleClickManager;
    import br.com.tratomais.einsurance.useradm.model.proxy.FunctionalityProxy;
    import br.com.tratomais.einsurance.useradm.model.proxy.ModuleProxy;
    import br.com.tratomais.einsurance.useradm.model.proxy.RoleProxy;
    import br.com.tratomais.einsurance.useradm.model.vo.Role;
    import br.com.tratomais.einsurance.useradm.model.vo.User;
    import br.com.tratomais.einsurance.useradm.view.components.RolesMain;
    
    import flash.events.Event;
    import flash.events.MouseEvent;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.resources.ResourceManager;
    
    import org.puremvc.as3.interfaces.IMediator;
    import org.puremvc.as3.interfaces.INotification;
    import org.puremvc.as3.patterns.mediator.Mediator;  
  
    public class RolesMainMediator extends Mediator implements IMediator {  
        public static const NAME:String = 'RolesMainMediator';  
        private var roleProxy:RoleProxy;
        private var moduleProxy:ModuleProxy;
        private var functionalityProxy:FunctionalityProxy;
        private var rolesFormMediator : RolesFormMediator = null;
        private var doubleClickManager : DoubleClickManager = null;
	
        public function RolesMainMediator(viewComponent:Object){  
            super(NAME, viewComponent);
   			roleProxy = facade.retrieveProxy(RoleProxy.NAME) as RoleProxy;
   			moduleProxy = facade.retrieveProxy(ModuleProxy.NAME) as ModuleProxy;
   			functionalityProxy = facade.retrieveProxy(FunctionalityProxy.NAME) as FunctionalityProxy;
        }

        public function get rolesMain():RolesMain{
            return viewComponent as RolesMain;
        }

        public function initialize():void{
        	roleProxy.listAllByUser(ApplicationFacade.getInstance().loggedUser);			
			this.initializeStatus();
        }

        private function listAllModule():void{
        	moduleProxy.listAllEdit();
        }

		private function listAllFunctionality():void{
			functionalityProxy.listAllEdit();
		}
		
        private function getObjectSelect():Role{
      		var obj:Object = rolesMain.dtRoles.selectedItem;
      		if(obj != null){
      			return obj as Role;
      		}else{
      			return null;
      		}
      	}

		private function viewRole(event:Event):void {
			var role:Role = this.getObjectSelect();
			if(editorIsEnabled() == false && role != null){
				this.enabledView();
				this.listAllModule();
				this.listAllFunctionality();
				rolesFormMediator.populateFormToRole(role);
			}
		}
		
		public function addRole(event:Event):void{
    		this.onClean();
        	this.listAllModule();
        	this.listAllFunctionality();
			this.enabledEditor();
			rolesMain.roleForm.chkActive.selected = true;
			rolesFormMediator.enabledForm(true);
		}

        public function onClean():void{
			rolesMain.dtRoles.selectedItem = null;
			rolesFormMediator.clearForm();
        }

        public function cancel(event:Event):void{
			this.initializeStatus();
        }

		private function initializeStatus():void{
        	this.onClean();
        	rolesMain.dtRoles.enabled = true;
			rolesFormMediator.enabledForm(false);
			rolesMain.onClean();
			rolesMain.imgNew.enabled = ApplicationFacade.getInstance().loggedUser.administrator;
			rolesMain.imgSave.enabled = false;
			rolesMain.imgUndo.enabled = false;
		}

		private function changeRole(event:Event):void{
			var loggedUser:User = ApplicationFacade.getInstance().loggedUser;
			
			if(loggedUser.administrator == false){
				Utilities.warningMessage("thisUserNotAreAllowedAsAdministrator");
			}else{
		  		var role: Role = this.getObjectSelect();
				if(editorIsEnabled() == false && role != null){
					this.enabledEditor();
					rolesMain.imgNew.enabled = false;
					rolesMain.imgSave.enabled = true;
					rolesMain.imgUndo.enabled = true;
					
					this.listAllModule();
					this.listAllFunctionality();
					rolesFormMediator.populateFormToRole(role);
				}
			}
		}
		
		private function saveRole(event:Event):void{
  			var role: Role = this.getObjectSelect();
			
			if(Utilities.validateForm(rolesMain.roleForm.validatorForm, ResourceManager.getInstance().getString('resources', 'rolesDetails'))){
	  			if(role == null){
					role = new Role();
				}
				
				try{
					rolesMain.populateRoleToForm(role);
					this.rolesFormMediator.populateRoleToForm(role);
					roleProxy.saveObject(role);
	  				
					//criar função para tratar os botões
					rolesMain.imgNew.enabled = ApplicationFacade.getInstance().loggedUser.administrator;
					rolesMain.imgSave.enabled = false;
					rolesMain.imgUndo.enabled = true;
				}catch(error:Error){
					Alert.show(error.message, "Error", Alert.OK, null, null, Utilities.errorMsg);
				}
			}
		}

        public function populateFormToRoles(list:ArrayCollection):void{
	    	rolesMain.dtRoles.dataProvider = list;
        }
        
        public function enabledEditor():void{ 
			sendNotification(NotificationList.USER_MAIN_GENERAL_MESSAGE,"");
			rolesMain.dtRoles.enabled = false;
			rolesMain.imgNew.enabled = false;
			rolesMain.imgSave.enabled = true;
			rolesMain.imgUndo.enabled = true;
			rolesFormMediator.enabledForm(true);
		}
		
		public function enabledView():void{
			rolesMain.dtRoles.enabled = true;
			rolesFormMediator.enabledForm(false);
			rolesMain.imgNew.enabled = ApplicationFacade.getInstance().loggedUser.administrator;
			rolesMain.imgSave.enabled = false;
			rolesMain.imgUndo.enabled = true;
		}
		
		public function editorIsEnabled():Boolean{
			var isEnabled:Boolean = false;
			if(rolesMain.dtRoles.enabled == false){
				isEnabled = true;
			}
			return isEnabled;
		}  		
		
		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */
		override public function onRegister():void{
			super.onRegister();

			rolesFormMediator = new RolesFormMediator(rolesMain.roleForm);
			ApplicationFacade.getInstance().registerOnlyNewMediator(rolesFormMediator);

			rolesMain.addEventListener(RolesMain.SAVE_ROLE, saveRole);
			rolesMain.addEventListener(RolesMain.ADD_ROLE, addRole);
			rolesMain.addEventListener(RolesMain.CANCEL_ROLE, cancel);

			doubleClickManager = new DoubleClickManager(rolesMain.dtRoles);
			doubleClickManager.addEventListener(MouseEvent.CLICK, viewRole);
			doubleClickManager.addEventListener(MouseEvent.DOUBLE_CLICK, changeRole);

			this.initialize();
		}
		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void{
			ApplicationFacade.getInstance().removeMediator(rolesFormMediator.getMediatorName());

			rolesMain.removeEventListener(RolesMain.SAVE_ROLE, saveRole);
			rolesMain.removeEventListener(RolesMain.ADD_ROLE, addRole);
			rolesMain.removeEventListener(RolesMain.CANCEL_ROLE, cancel);

			doubleClickManager.removeEventListener(MouseEvent.CLICK, viewRole);
			doubleClickManager.removeEventListener(MouseEvent.DOUBLE_CLICK, changeRole);
			
			this.onClean();
			setViewComponent(null);

			super.onRemove();
		}	

        override public function listNotificationInterests():Array{  
            return [NotificationList.USER_ROLES_REFRESH,
            		NotificationList.MODULE_LISTED_EDIT,
            		NotificationList.ROLE_SAVE_SUCCESS,
            		NotificationList.FUNCTIONALITY_LISTED];  
        }  
          
        override public function handleNotification(notification:INotification):void{  
            switch(notification.getName()){  
           		case NotificationList.USER_ROLES_REFRESH:
           			this.populateFormToRoles(notification.getBody()  as ArrayCollection);
           			break;   
           		case NotificationList.MODULE_LISTED_EDIT:
           			rolesMain.populateFormToModules(notification.getBody()  as ArrayCollection, this.getObjectSelect());
           			break;   
           		case NotificationList.ROLE_LIST_SAVE:
           			roleProxy.listAllRoles();
           			break;
           		case NotificationList.ROLE_SAVE_SUCCESS:
	             	this.onClean();
	             	this.initialize();
		      		rolesMain.dtRoles.enabled = true;
					rolesMain.accordionContainer.selectedIndex = 0;
					rolesFormMediator.enabledForm(false);
					
					var log:String;
					log = ApplicationFacade.getInstance().loggedUser.login;
					log += ":" + ApplicationFacade.getInstance().loggedUser.domain.name;
					log += ":Roles";
					log += ":SAVE";
					ApplicationFacade.getInstance().auditLogger = log;
           			break;
           		case NotificationList.FUNCTIONALITY_LISTED:
           			rolesMain.populateFormToFunctionalitys(notification.getBody()  as ArrayCollection, this.getObjectSelect());
           			break;
            }
        }
    }
}