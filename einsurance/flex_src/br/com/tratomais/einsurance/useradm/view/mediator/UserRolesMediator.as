////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2009 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 17/12/2009
//
// NOTICE: Trato+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.useradm.view.mediator  
{  
    import br.com.tratomais.einsurance.useradm.model.vo.Role;
    import br.com.tratomais.einsurance.useradm.model.vo.User;
    import br.com.tratomais.einsurance.useradm.view.components.UserRoles;
    
    import mx.collections.ArrayCollection;
    
    import org.puremvc.as3.interfaces.IMediator;
    import org.puremvc.as3.interfaces.INotification;
    import org.puremvc.as3.patterns.mediator.Mediator;  
  
    public class UserRolesMediator extends Mediator implements IMediator  
    {  

        public static const NAME:String = 'UserRolesMediator';  
        public var editor: Boolean = false;
      
        public function UserRolesMediator(viewComponent:Object)  
        {  
            super(NAME, viewComponent);  
	    }  
          
        public function get userRoles():UserRoles{  
            return viewComponent as UserRoles;  
        }         
                
         public function populateFormToRole(roles : ArrayCollection):void{
   			userRoles.dgRoles.dataProvider = roles; 
   			for(var i : uint = 0; i < roles.length; i++){
        		var role : Role = roles.getItemAt(i) as Role;
       			role.transientEditor = editor;
        	} 
        }         
        
         public function populateUserToForm(user : User):void{
			var dgCollection : ArrayCollection = userRoles.dgRoles.dataProvider as ArrayCollection; 
			var rolesUser : ArrayCollection = new ArrayCollection();
			
        	for(var i : uint = 0; i < dgCollection.length; i++){
        		var role : Role = dgCollection.getItemAt(i) as Role;
        		if(role.transientSelected == true){
        			rolesUser.addItem(role);
        		}
        	} 
        	
        	user.roles = rolesUser;
        }       
        
       	public function enabledForm(valor:Boolean) : void {
			userRoles.dgRoles.enabled = valor;
	   	}
	   	
	   	public function resetForm() : void {
			userRoles.dgRoles.dataProvider = null;
	   	}
     	
		override public function onRegister():void 
		{
			super.onRegister();
		}
		
		override public function onRemove():void 
		{
			super.onRemove();
			this.resetForm();
		}   
		          
        override public function listNotificationInterests():Array  
        {  
             return [ 
              ];   
        }  
          
        override public function handleNotification(notification:INotification):void  
        {  
        } 
    }  
}  