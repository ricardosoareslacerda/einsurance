package br.com.tratomais.einsurance.useradm.view.components.events
{
	import br.com.tratomais.einsurance.useradm.model.vo.Functionality;
	import br.com.tratomais.einsurance.useradm.model.vo.Module;
	
	import flash.events.Event;
	
	public class RoleEvent extends Event
	{

		public static const ROLE_EVENT_PARENT:String = "roleEventUpdateParent";
		public static const ROLE_EVENT_CHILD:String = "roleEventUpdateChild";

		private var _state : uint;
		private var _module : Module;
		private var _functionality : Functionality;


		public function RoleEvent(type:String, obj:Object, state : uint, bubbles:Boolean= true, cancelable:Boolean= true)
		{
			super(type, bubbles, cancelable);
			
			if(obj is Module)
				this._module = obj as Module;
			else
				this._functionality = obj as Functionality;
				
			this._state = state;		
		}
		
		public function get state():uint{  
            return _state;  
        }  
  
        public function set state(pData:uint):void{  
            _state=pData;  
        }
        
        public function get module():Module{  
            return _module;  
        }  
  
        public function set module(pData:Module):void{  
            _module=pData;  
        }

        public function get functionality():Functionality{  
            return _functionality;  
        }  
  
        public function set functionality(pData:Functionality):void{  
            _functionality=pData;  
        } 
	}
}