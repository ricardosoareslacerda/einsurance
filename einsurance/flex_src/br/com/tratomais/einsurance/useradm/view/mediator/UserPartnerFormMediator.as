////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2009 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 17/12/2009
//
// NOTICE: Trato+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.useradm.view.mediator  
{  
    import br.com.tratomais.einsurance.useradm.model.vo.Partner;
    import br.com.tratomais.einsurance.useradm.model.vo.User;
    import br.com.tratomais.einsurance.useradm.model.vo.UserPartner;
    import br.com.tratomais.einsurance.useradm.view.components.UserPartnerForm;
    
    import mx.collections.ArrayCollection;
    
    import org.puremvc.as3.interfaces.IMediator;
    import org.puremvc.as3.patterns.mediator.Mediator;  
  
    public class UserPartnerFormMediator extends Mediator implements IMediator  
    {  

        public static const NAME:String = 'UserPartnerFormMediator';  
        public var editor: Boolean = false;
        
        public function UserPartnerFormMediator(viewComponent:Object)  
        {  
            super(NAME, viewComponent);
        }  
          
        public function get userPartnerForm():UserPartnerForm{  
            return viewComponent as UserPartnerForm;  
        }         
	
       public function populateFormToListPartners( partners : ArrayCollection ) : void {
       		for(var i : uint = 0 ; i < partners.length; i++){
        		var partner : Partner = partners.getItemAt(i) as Partner;
        		if(partners.length == 1){
        			partner.transientIsDefault = true;
        		}
	       		partner.transientEditor = editor;
        	}
	    	userPartnerForm.dgrListPartner.visible = true;
 			userPartnerForm.dgrListPartner.dataProvider = partners;
        }        
       
	     public function populateUserPartnersToForm(user : User):void{
			var dgCollection : ArrayCollection = userPartnerForm.dgrListPartner.dataProvider as ArrayCollection; 
        	var newUserPartners : ArrayCollection = new ArrayCollection();
        	for(var i : uint = 0 ; i < dgCollection.length; i++){
        		var partner : Partner = dgCollection.getItemAt(i) as Partner;
        		if(partner.transientSelected == true){
	        		var userPartnerObject : UserPartner  = new UserPartner();
	        		userPartnerObject.partner = partner;
	        		userPartnerObject.administrator = partner.transientAdministrator;
	        		userPartnerObject.allChannel = partner.transientAllChannel;
        			if(dgCollection.length == 1){
        				userPartnerObject.partnerDefault = true;
        			}else{
		        		userPartnerObject.partnerDefault = partner.transientIsDefault;
        			}
	        		newUserPartners.addItem(userPartnerObject);
        		}
        	}
        	user.userPartners = newUserPartners;
	    } 
	    
      	public function checkedPartners(value : Boolean) : void {
      		var dgCollection : ArrayCollection = userPartnerForm.dgrListPartner.dataProvider as ArrayCollection; 
           	for(var i : uint = 0 ; i < dgCollection.length; i++){
        		var partner : Partner = dgCollection.getItemAt(i) as Partner;
	       		partner.transientAdministrator = value;
	       		partner.transientSelected = value;
        	}
      	}

		public function validateForm() : Boolean {
      		var dgCollection : ArrayCollection = userPartnerForm.dgrListPartner.dataProvider as ArrayCollection; 
           	for(var i : uint = 0 ; i < dgCollection.length; i++){
        		var partner : Partner = dgCollection.getItemAt(i) as Partner;
	       		if(partner.transientSelected){
        			return true;
        		} 
        	}
        	return false;

		}
		
      	public function validateChecks() : Boolean {
      		var dgCollection : ArrayCollection = userPartnerForm.dgrListPartner.dataProvider as ArrayCollection; 
           	for(var i : uint = 0 ; i < dgCollection.length; i++){
        		var partner : Partner = dgCollection.getItemAt(i) as Partner;
	       		if(partner.transientSelected == false && partner.transientAdministrator == true){
        			return false;
        		} 
        	}
        	return true;
      	}

      	public function validateSelectPartnerDefault() : Boolean {
      		var dgCollection : ArrayCollection = userPartnerForm.dgrListPartner.dataProvider as ArrayCollection; 
      		var countPartnerDefault : int = 0;
      		var countSelected : int = 0;
           	for(var i : uint = 0 ; i < dgCollection.length; i++){
        		var partner : Partner = dgCollection.getItemAt(i) as Partner;
	       		if(partner.transientIsDefault == true){
					countPartnerDefault ++;
        		} 
        		if(partner.transientSelected == true){
        			countSelected ++;
        		}
        	}
			if((countPartnerDefault > 1 || countPartnerDefault < 1) && (countSelected != 1)){
	    	   	return false;
		    }else{
	       		return true;
	     	}
      	}

      	public function enabledForm(valor:Boolean) : void {
			userPartnerForm.dgrListPartner.enabled = valor;
	   	}
	   	
	   	public function resetForm() : void {
			userPartnerForm.dgrListPartner.dataProvider = new ArrayCollection();
	   	}
     	
		override public function onRegister():void 
		{
			super.onRegister();
		}
		
		override public function onRemove():void 
		{
			super.onRemove();
			this.resetForm();			
		}        
    }  
}  