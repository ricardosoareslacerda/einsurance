package br.com.tratomais.einsurance.useradm.view.mediator
{
	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.useradm.model.proxy.LoginProxy;
	import br.com.tratomais.einsurance.useradm.model.vo.User;
	import br.com.tratomais.einsurance.useradm.view.components.PasswordChangeForm;
	
	import flash.events.Event;
	
	import mx.controls.Alert;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	/**
	 * Mediator used with the PasswordChangeForm
	 * @see br.com.tratomais.einsurance.useradm.view.components.PasswordChangeForm
	 */
	[ResourceBundle("messages")]
	[ResourceBundle("resources")]
	public class PasswordChangeMediator extends Mediator implements IMediator
	{
		/**
		 * Mediator name
		 */
		public static const NAME:String = 'PasswordChangeMediator';
		
		/**
		 * Login Proxy
		 */
		private var loginProxy : LoginProxy;
		
		/**
		 * Resource instance used with i18n
		 */
		private var resourceMaganer: IResourceManager;
		
		/**
		 * Constructor
		 * @param viewComponent The PasswordChangeForm instance
		 */
		public function PasswordChangeMediator(viewComponent:PasswordChangeForm)
		{
            super(NAME, viewComponent);
            resourceMaganer = ResourceManager.getInstance();
		}

		/**
		 * Return the password form
		 */		
        protected function get passwordChangeForm():PasswordChangeForm{  
          return viewComponent as PasswordChangeForm;  
        }

		/**
		 * During the Register Phrase, register the Events that the form depends on. Initialize variables needed.
		 */        
		override public function onRegister():void {
            loginProxy = LoginProxy ( facade.retrieveProxy(LoginProxy.NAME));
            passwordChangeForm.addEventListener(PasswordChangeForm.PASS_SAVE_CHANGES, saveChanges);
            passwordChangeForm.addEventListener(PasswordChangeForm.PASS_CANCEL_CHANGES, cancelChanges);

			user = ApplicationFacade.getInstance().loggedUser;
			passwordChangeForm.user = user;
		}
		 
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
            passwordChangeForm.removeEventListener(PasswordChangeForm.PASS_CANCEL_CHANGES, cancelChanges);
            passwordChangeForm.removeEventListener(PasswordChangeForm.PASS_SAVE_CHANGES, saveChanges);
		}
		
		// ----------------------------------------------------------------------------------------------------------
		
		/**
		 * Logged User
		 */
		private var user:User;
		
		/**
		 * Saves changes, validating the field values
		 */
		private function saveChanges(event: Event):void{
			if (! Utilities.validateForm(passwordChangeForm.getValidators(), ResourceManager.getInstance().getString('resources', 'password')) )
				return;
			if ( passwordChangeForm.newPassword == passwordChangeForm.newPasswordRetype ) {
				loginProxy.changeUserPassword(passwordChangeForm.user,passwordChangeForm.oldPassword, passwordChangeForm.newPassword);
			} else {
				Alert.show(
						resourceMaganer.getString("messages","passwordRetypeError"),
						resourceMaganer.getString("resources","attention"), 
						Alert.OK );
			}
		}
		
		/**
		 * Close form without changes
		 * @param event E
		 */
		private function cancelChanges(event: Event):void{
	        passwordChangeForm.close();
		}
		
		/**
		 */
        override public function listNotificationInterests():Array  
        {  
            return [  
            	NotificationList.PASSWORD_CHANGE
             ];
        }
        
		/**
		 * @private 
		 */          
        override public function handleNotification(notification:INotification):void  
        {
            switch ( notification.getName() )  
            {  
           		case NotificationList.PASSWORD_CHANGE:
					var alterou:Boolean = notification.getBody() as Boolean;
					if(alterou) {
						Utilities.successMessage("password.changed");
				        passwordChangeForm.close();
					}
					else
						Utilities.warningMessage("password.not.changed");
					break;			
           	}
        }  
	}
}