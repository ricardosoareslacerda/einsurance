////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2009 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 17/12/2009
//
// NOTICE: Adobe permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance
{
	import br.com.tratomais.einsurance.core.DesktopMediator;
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.useradm.model.vo.User;
	import br.com.tratomais.einsurance.useradm.view.mediator.LoginFormMediator;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
    /**
     * A Mediator for interacting with the top level Application.
     * 
     * <P>
     * In addition to the ordinary responsibilities of a mediator
     * the MXML application (in this case) built all the view components
     * and so has a direct reference to them internally. Therefore
     * we create and register the mediators for each view component
     * with an associated mediator here.</P>
     * 
     * <P>
     * Then, ongoing responsibilities are: 
     * <UL>
     * <LI>listening for events from the viewComponent (the application)</LI>
     * <LI>sending notifications on behalf of or as a result of these 
     * events or other notifications.</LI>
     * <LI>direct manipulating of the viewComponent by method invocation
     * or property setting</LI>
     * </UL>
     */
    public class ApplicationMediator extends Mediator implements IMediator
    {
    	/**
    	 * Cannonical name of the Mediator
    	 */
        public static const NAME:String = "ApplicationMediator";
        
        public static const USER_FORM : Number = 0;
		public static const USER_LIST : Number = 1;
       
        /**
         * Constructor. 
         * 
         * <P>
         * On <code>applicationComplete</code> in the <code>Application</code>,
         * the <code>ApplicationFacade</code> is initialized and the 
         * <code>ApplicationMediator</code> is created and registered.</P>
         * 
         * <P>
         * The <code>ApplicationMediator</code> constructor also registers the 
         * Mediators for the view components created by the application.</P>
         * 
         * @param object the viewComponent einsurance
         */       
        public function ApplicationMediator( viewComponent:Object ) 
        {	
            // pass the viewComponent to the superclass where 
            // it will be stored in the inherited viewComponent property        	
            super(NAME, viewComponent);
            
            // Create and register Mediators for the LoginForm and Desktop
            // components that were instantiated by the mxml application            
            ApplicationFacade.getInstance().registerOnlyNewMediator(new LoginFormMediator(app.login));    
   	        ApplicationFacade.getInstance().registerOnlyNewMediator(new DesktopMediator(app.desktop));    
        }
        
        	
		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */
		override public function onRegister():void 
		{
			super.onRegister();
		}
		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
			ApplicationFacade.getInstance().removeMediator(DesktopMediator.NAME);
		}
        
        /**
         * List all notifications this Mediator is interested in.
         * <P>
         * Automatically called by the framework when the mediator
         * is registered with the view.</P>
         * 
         * @return Array the list of Nofitication names
         */        
        override public function listNotificationInterests():Array 
        {
            
            return [
	            NotificationList.LOGIN_VALIDATED
            ];
        }
        
        /**
         * Cast the viewComponent to its actual type.
         * 
         * <P>
         * This is a useful idiom for mediators. The
         * PureMVC Mediator class defines a viewComponent
         * property of type Object. </P>
         * 
         * <P>
         * Here, we cast the generic viewComponent to 
         * its actual type in a protected mode. This 
         * retains encapsulation, while allowing the instance
         * (and subclassed instance) access to a 
         * strongly typed reference with a meaningful
         * name.</P>
         * 
         * @return app the viewComponent cast to einsurance
         */        
        protected function get app():einsurance{
            return viewComponent as einsurance
        }
        
        /**
        * Inicializa a aplicação geral, deixando a tela de login
        * invisivel e mostrando o desktop
        * 
        * @param objeto User, contendo as informações do usuário logado. 
        */
        public function initialize(user : User) : void {
        	ApplicationFacade.getInstance().loggedUser = user;
	       	app.login.visible = false;
           	app.desktop.visible = true;
    
        }
        
        /**
         * Handle all notifications this Mediator is interested in.
         * <P>
         * Called by the framework when a notification is sent that
         * this mediator expressed an interest in when registered
         * </P>
         * @see listNotificationInterests.
         * @param INotification a notification 
         */
        override public function handleNotification( notification:INotification ):void {
            switch ( notification.getName() ){  
            	case NotificationList.LOGIN_VALIDATED:
            		this.initialize(notification.getBody() as User);
            		break;
 	        }
        }
    }
}
