package br.com.tratomais.einsurance.policy.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.CalcStepId")]      
    [Managed]  
    public class CalcStepId extends HibernateBean 
    {  
        private var _sequenceId:int;  
        private var _coverageId:int;  
        private var _itemId:int;  
        private var _endorsementId:int;  
        private var _contractId:int;  
  
        public function CalcStepId()  
        {  
        }  
  
        public function get sequenceId():int{  
            return _sequenceId;  
        }  
  
        public function set sequenceId(pData:int):void{  
            _sequenceId=pData;  
        }  
  
        public function get coverageId():int{  
            return _coverageId;  
        }  
  
        public function set coverageId(pData:int):void{  
            _coverageId=pData;  
        }  
  
        public function get itemId():int{  
            return _itemId;  
        }  
  
        public function set itemId(pData:int):void{  
            _itemId=pData;  
        }  
  
        public function get endorsementId():int{  
            return _endorsementId;  
        }  
  
        public function set endorsementId(pData:int):void{  
            _endorsementId=pData;  
        }  
  
        public function get contractId():int{  
            return _contractId;  
        }  
  
        public function set contractId(pData:int):void{  
            _contractId=pData;  
        }  
    }  
}