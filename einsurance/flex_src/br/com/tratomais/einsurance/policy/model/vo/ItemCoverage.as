package br.com.tratomais.einsurance.policy.model.vo
{
	
	import br.com.tratomais.einsurance.products.model.vo.Deductible;
	
	import flash.errors.IllegalOperationError;
	
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.ItemCoverage")]    
    [Managed]  
    public class ItemCoverage extends HibernateBean 
    {  
        private var _id:ItemCoverageId;  
        private var _item:Item;  
        private var _coverageCode:String;  
        private var _coverageName:String;  
        private var _basicCoverage:Boolean;  
        private var _groupCoverageId:int;  
        private var _coverageCancel:Boolean;  
        private var _goodsCode:String;  
        private var _branchId:int;  
        private var _insuredValue:Number;  
		private var _rangeValueId:int;
        private var _pureRate:Number;  
        private var _tariffRate:Number;  
        private var _commercialRate:Number;  
        private var _spendingRate:Number;  
        private var _profitRate:Number;  
        private var _netRate:Number;  
        private var _tariffFactor:Number;  
        private var _commercialFactor:Number;  
        private var _purePremium:Number;  
        private var _tariffPremium:Number;  
        private var _commercialPremium:Number;  
        private var _profitPremium:Number;  
        private var _technicalPremium:Number;
        private var _retainedPremium:Number;  
        private var _earnedPremium:Number;
        private var _unearnedPremium:Number;
        private var _netPremium:Number;  
		private var _fractioningAdditional:Number;
        private var _taxIncidence:Boolean;  
        private var _taxValue:Number;  
        private var _totalPremium:Number;  
		private var _deductibleId:String;
        private var _deductibleValue:Number;  
        private var _deductibleValueMinimum:Number;  
        private var _deductibleValueMaximum:Number;  
        private var _deductibleDescription:String;  
        private var _commission:Number;  
        private var _commissionFactor:Number;  
        private var _commissionFactorOriginal:Number;  
		private var _labour:Number;
		private var _labourFactor:Number;
		private var _agency:Number;
		private var _agencyFactor:Number;
		private var _bonus:Number;
		private var _bonusFactor:Number;
		private var _displayOrder:int;
        private var _calcSteps:ArrayCollection = new ArrayCollection();
        private var _calcStep:CalcStep;
		private var _deductible:Deductible;
  
        public function ItemCoverage()  
        {  
        	//_calcSteps = new ArrayCollection();
        }
        
        public function get deductible():Deductible{  
            return _deductible;  
        }
        
        public function set deductible(pData:Deductible):void{  
            _deductible=pData;  
        }
        
        public function get id():ItemCoverageId{  
            return _id;  
        }  
  
        public function set id(pData:ItemCoverageId):void{  
            _id=pData;  
        }  
  
        public function get item():Item{  
            return _item;  
        }  
  
        public function set item(pData:Item):void{  
            _item=pData;  
        }  
  
        public function get coverageCode():String{  
            return _coverageCode;  
        }  
  
        public function set coverageCode(pData:String):void{  
            _coverageCode=pData;  
        }  
  
        public function get coverageName():String{  
            return _coverageName;  
        }  
  
        public function set coverageName(pData:String):void{  
            _coverageName=pData;  
        }  
  
        public function get basicCoverage(): Boolean { 
        	return _basicCoverage;
        }
        
        public function set basicCoverage(pBasicCoverage: Boolean): void {
        	_basicCoverage = pBasicCoverage;
        }
        
		public function get groupCoverageId():int{  
            return _groupCoverageId;  
        }  
  
        public function set groupCoverageId(pData:int):void{  
            _groupCoverageId=pData;  
        }  
  
        public function get coverageCancel():Boolean{  
            return _coverageCancel;  
        }  
  
        public function set coverageCancel(pData:Boolean):void{  
            _coverageCancel=pData;  
        }  
  
        public function get goodsCode():String{  
            return _goodsCode;  
        }  
  
        public function set goodsCode(pData:String):void{  
            _goodsCode=pData;  
        }  
  
        public function get branchId():int{  
            return _branchId;  
        }  
  
        public function set branchId(pData:int):void{  
            _branchId=pData;  
        }  
  
        public function get insuredValue():Number{  
            return _insuredValue;  
        }  
  
        public function set insuredValue(pData:Number):void{  
            _insuredValue=pData;  
        }  
  
		public function get rangeValueId():int {
			return _rangeValueId;
		}
	
		public function set rangeValueId(pData:int):void {
			_rangeValueId = pData;
		}
	
        public function get pureRate():Number{  
            return _pureRate;  
        }  
  
        public function set pureRate(pData:Number):void{  
            _pureRate=pData;  
        }  
  
        public function get tariffRate():Number{  
            return _tariffRate;  
        }  
  
        public function set tariffRate(pData:Number):void{  
            _tariffRate=pData;  
        }  
  
        public function get commercialRate():Number{  
            return _commercialRate;  
        }  
  
        public function set commercialRate(pData:Number):void{  
            _commercialRate=pData;  
        }  
  
        public function get spendingRate():Number{  
            return _spendingRate;  
        }  
  
        public function set spendingRate(pData:Number):void{  
            _spendingRate=pData;  
        }  
  
        public function get profitRate():Number{  
            return _profitRate;  
        }  
  
        public function set profitRate(pData:Number):void{  
            _profitRate=pData;  
        }  

        public function get netRate():Number{  
            return _netRate;  
        }  
  
        public function set netRate(pData:Number):void{  
            _netRate=pData;  
        }  
  
        public function get tariffFactor():Number{  
            return _tariffFactor;  
        }  
  
        public function set tariffFactor(pData:Number):void{  
            _tariffFactor=pData;  
        }  
  
        public function get commercialFactor():Number{  
            return _commercialFactor;  
        }  
  
        public function set commercialFactor(pData:Number):void{  
            _commercialFactor=pData;  
        }  
  
        public function get purePremium():Number{  
            return _purePremium;  
        }  
  
        public function set purePremium(pData:Number):void{  
            _purePremium=pData;  
        }  
  
        public function get tariffPremium():Number{  
            return _tariffPremium;  
        }  
  
        public function set tariffPremium(pData:Number):void{  
            _tariffPremium=pData;  
        }  
  
        public function get commercialPremium():Number{  
            return _commercialPremium;  
        }  
  
        public function set commercialPremium(pData:Number):void{  
            _commercialPremium=pData;  
        }  
  
        public function get profitPremium():Number{  
            return _profitPremium;  
        }  
  
        public function set profitPremium(pData:Number):void{  
            _profitPremium=pData;  
        }  
  
        public function get technicalPremium():Number{  
            return _technicalPremium;  
        }  
  
        public function set technicalPremium(pData:Number):void{  
            _technicalPremium=pData;  
        }  

        public function get retainedPremium():Number{  
            return _retainedPremium;  
        }  
  
        public function set retainedPremium(pData:Number):void{  
            _retainedPremium=pData;  
        }
        
		public function get earnedPremium():Number{  
            return _earnedPremium;  
        }  
  
        public function set earnedPremium(pData:Number):void{  
            _earnedPremium=pData;  
        }  
  
        public function get unearnedPremium():Number{  
            return _unearnedPremium;  
        }  
  
        public function set unearnedPremium(pData:Number):void{  
            _unearnedPremium=pData;  
        }  
  
        public function get netPremium():Number{  
            return _netPremium;  
        }  
  
        public function set netPremium(pData:Number):void{  
            _netPremium=pData;  
        }  
  
		public function get fractioningAdditional():Number {
			return _fractioningAdditional;
		}
	
		public function set fractioningAdditional(pData:Number):void {
			_fractioningAdditional = pData;
		}
	
        public function get taxIncidence():Boolean{  
            return _taxIncidence;  
        }  
  
        public function set taxIncidence(pData:Boolean):void{  
            _taxIncidence=pData;  
        }  
  
        public function get taxValue():Number{  
            return _taxValue;  
        }  
  
        public function set taxValue(pData:Number):void{  
            _taxValue=pData;  
        }  
  
        public function get totalPremium():Number{  
            return _totalPremium;  
        }  
  
        public function set totalPremium(pData:Number):void{  
            _totalPremium=pData;  
        }  
        
        public function get deductibleId():String{  
            return _deductibleId;  
        }
        
        public function set deductibleId(pData:String):void{  
            _deductibleId=pData;  
        }
        
        public function get deductibleValue():Number{  
            return _deductibleValue;  
        }  
  
        public function set deductibleValue(pData:Number):void{  
            _deductibleValue=pData;  
        }  
  
        public function get deductibleValueMinimum():Number{  
            return _deductibleValueMinimum;  
        }  
  
        public function set deductibleValueMinimum(pData:Number):void{  
            _deductibleValueMinimum=pData;  
        }  
  
        public function get deductibleValueMaximum():Number{  
            return _deductibleValueMaximum;  
        }  
  
        public function set deductibleValueMaximum(pData:Number):void{  
            _deductibleValueMaximum=pData;  
        }  
  
        public function get deductibleDescription():String{  
            return _deductibleDescription;  
        }  
  
        public function set deductibleDescription(pData:String):void{  
            _deductibleDescription=pData;  
        }  
  
        public function get commission():Number{  
            return _commission;  
        }  
  
        public function set commission(pData:Number):void{  
            _commission=pData;  
        }  
  
        public function get commissionFactor():Number{  
            return _commissionFactor;  
        }  
  
        public function set commissionFactor(pData:Number):void{  
            _commissionFactor=pData;  
        }  
  
        public function get commissionFactorOriginal():Number{  
            return _commissionFactorOriginal;  
        }  
  
        public function set commissionFactorOriginal(pData:Number):void{  
            _commissionFactorOriginal=pData;  
        }  
  
		public function get labour():Number {
			return _labour;
		}
	
		public function set labour(pData:Number):void {
			_labour = pData;
		}
	
		public function get labourFactor():Number {
			return _labourFactor;
		}
	
		public function set labourFactor(pData:Number):void {
			_labourFactor = pData;
		}
	
		public function get agency():Number {
			return _agency;
		}
	
		public function set agency(pData:Number):void {
			_agency = pData;
		}
	
		public function get agencyFactor():Number {
			return _agencyFactor;
		}
	
		public function set agencyFactor(pData:Number):void {
			_agencyFactor = pData;
		}
	
		public function get bonus():Number {
			return _bonus;
		}
	
		public function set bonus(pData:Number):void {
			_bonus = pData;
		}
	
		public function get bonusFactor():Number {
			return _bonusFactor;
		}
	
		public function set bonusFactor(pData:Number):void {
			_bonusFactor = pData;
		}
	
		public function get displayOrder():int {
			return _displayOrder;
		}
	
		public function set displayOrder(pData:int):void {
			_displayOrder = pData;
		}
		
        public function get calcSteps():ArrayCollection{  
            return _calcSteps;  
        }  
  
        public function set calcSteps(pData:ArrayCollection):void{  
            _calcSteps=pData;  
        }

		public function updateKey(parent:Item):void{
			if ( parent.id == null ) {
				throw new IllegalOperationError();
			}
			this.item = parent;
			if ( this.id == null ) {
				//this.setId( new ItemCoverageId(0,parent.getId().getItemId(),parent.getId().getEndorsementId(),parent.getId().getContractId()) );
				this.id =  new ItemCoverageId();
				this.id.coverageId = 0;
				this.id.itemId = parent.id.itemId;
				this.id.endorsementId = parent.id.endorsementId;
				this.id.contractId = parent.id.contractId;
			}else{
				this.id.itemId = parent.id.itemId;
				this.id.endorsementId = parent.id.endorsementId;
				this.id.contractId = parent.id.contractId;
			}
			
	 		for each( var tmpCalcStep:CalcStep in this.calcSteps ) {
				tmpCalcStep.updateKey( this );
			}
	 		
		}

		public function addCalcStep():CalcStep{
			var newCalcStep:CalcStep = new CalcStep();
			
			var newCalcStepId:CalcStepId = new CalcStepId();
			if ( this.id != null ) {
				newCalcStepId.contractId = this.id.contractId;
				newCalcStepId.endorsementId = this.id.endorsementId;
				newCalcStepId.itemId = this.id.itemId;
				newCalcStepId.coverageId = this.id.coverageId;
			}
			newCalcStepId.sequenceId = findNextCalcStepSequence() ;
			
			newCalcStep.id = newCalcStepId ;
			newCalcStep.itemCoverage = this ;
			
			this.calcSteps.addItem( newCalcStep );
			
			return newCalcStep;
		}

		/**
		 * @return
		 */
		public function findNextCalcStepSequence():int {
			var maxSequenceId:int = 0;
			
			if (this.calcSteps == null)
				this.calcSteps = new ArrayCollection();
			
			for each ( var tmpCalcStep:CalcStep in this.calcSteps ) {
				if ( tmpCalcStep.id != null && tmpCalcStep.id.sequenceId > maxSequenceId ) {
					maxSequenceId=tmpCalcStep.id.sequenceId;
				}
			}
			
			maxSequenceId++;
			return maxSequenceId;
		}
		
		public function limpaPremios():void {
			this.purePremium = 0.0 ;
			this.tariffPremium = 0.0 ;
			this.commercialPremium = 0.0 ;
			this.profitPremium = 0.0 ;
			this.technicalPremium = 0.0 ;
			this.retainedPremium = 0.0 ;
			this.earnedPremium = 0.0 ;
			this.unearnedPremium = 0.0 ;
			this.netPremium = 0.0 ;
			this.totalPremium = 0.0 ;		
		}
	
    }  
}