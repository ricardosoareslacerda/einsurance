package  br.com.tratomais.einsurance.policy.model.vo   
{
	
	import br.com.tratomais.einsurance.products.model.vo.PlanPersonalRisk;
	import br.com.tratomais.einsurance.products.model.vo.ProductOption;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.EndorsementOption")]     
    [Managed]  
    public class EndorsementOption extends HibernateBean  
    {  
        private var _endorsement:Endorsement; 
        private var _endorsementOptionId:EndorsementOption;
        private var _action:int;  
        private var _partnerName:String;  
        private var _channelName:String;  
        private var _productOption:ProductOption;
        private var _planPersonalRisk:PlanPersonalRisk;
  
  		public static const ACTION_EFFECTIVE:int = 1;
		public static const ACTION_UNLOCK:int = 2;
  		public static const ACTION_ANULLATION:int = 3;
  		public static const ACTION_CHANGE_REGISTER:int = 4;
  		public static const ACTION_CHANGE_TECHNICAL:int = 5;

        public function EndorsementOption(){}
        
        
        public function get endorsementOptionId():EndorsementOption{  
            return _endorsementOptionId;  
        }  
  
        public function set endorsementOptionId(endorsementOptionId:EndorsementOption):void{  
            this._endorsementOptionId=endorsementOptionId;  
        }  
  
          
  
        public function get endorsement():Endorsement{  
            return _endorsement;  
        }  
  
        public function set endorsement(pData:Endorsement):void{  
            _endorsement=pData;  
        }  
  
        public function get action():int{  
            return _action;  
        }  
  
        public function set action(pData:int):void{  
            _action=pData;  
        }  

        public function get partnerName():String{  
            return _partnerName;  
        }  
  
        public function set partnerName(pData:String):void{  
            _partnerName=pData;  
        }  

        public function get channelName():String{  
            return _channelName;  
        }  
  
        public function set channelName(pData:String):void{  
            _channelName=pData;  
        }  
        
        public function get productOption():ProductOption{  
	    	return _productOption;  
        }  
  
        public function set productOption(pData:ProductOption):void{  
            _productOption=pData;  
        } 

        public function get planPersonalRisk():PlanPersonalRisk{  
	    	return _planPersonalRisk;  
        }  
  
        public function set planPersonalRisk(pData:PlanPersonalRisk):void{  
            _planPersonalRisk=pData;  
        }  
    }  
}