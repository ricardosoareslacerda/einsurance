package br.com.tratomais.einsurance.policy.model.vo 
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.ItemProfileId")]   
    [Managed]  
    public class ItemProfileId extends HibernateBean 
    {  
        private var _responseId:int; 
        private var _questionId:int;  
        private var _questionnaireId:int;  
        private var _itemId:int;  
        private var _endorsementId:int;  
        private var _contractId:int;  
  
        public function ItemProfileId()  
        {  
        }  
  
        public function get responseId():int{  
            return _responseId;  
        }  
  
        public function set responseId(pData:int):void{  
            _responseId=pData;  
        }  
  
        public function get questionId():int{  
            return _questionId;  
        }  
  
        public function set questionId(pData:int):void{  
            _questionId=pData;  
        }  
  
        public function get questionnaireId():int{  
            return _questionnaireId;  
        }  
  
        public function set questionnaireId(pData:int):void{  
            _questionnaireId=pData;  
        }  
  
        public function get itemId():int{  
            return _itemId;  
        }  
  
        public function set itemId(pData:int):void{  
            _itemId=pData;  
        }  
  
        public function get endorsementId():int{  
            return _endorsementId;  
        }  
  
        public function set endorsementId(pData:int):void{  
            _endorsementId=pData;  
        }  
  
        public function get contractId():int{  
            return _contractId;  
        }  
  
        public function set contractId(pData:int):void{  
            _contractId=pData;  
        }  
    }  
}