package br.com.tratomais.einsurance.policy.model.vo   
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.ItemProfile")]     
    [Managed]  
    public class ItemProfile extends HibernateBean 
    {  
        private var _id:ItemProfileId;  
        private var _item:Item;   
        private var _questionnaireType:int;  
        private var _questionName:String;  
        private var _responseName:String;  
        private var _responseValue:String;  
        private var _responsePoints:Number;  
        private var _displayOrder:int;  
  
        public function ItemProfile()  
        {  
        }  
  
        public function get id():ItemProfileId{  
            return _id;  
        }  
  
        public function set id(pData:ItemProfileId):void{  
            _id=pData;  
        }  
  
        public function get item():Item{  
            return _item;  
        }  
  
        public function set item(pData:Item):void{  
            _item=pData;  
        }  
        
        public function get questionnaireType():int{  
            return _questionnaireType;  
        }  
  
        public function set questionnaireType(pData:int):void{  
            _questionnaireType=pData;  
        }  
  
        public function get questionName():String{  
            return _questionName;  
        }  
  
        public function set questionName(pData:String):void{  
            _questionName=pData;  
        }  
  
        public function get responseName():String{  
            return _responseName;  
        }  
  
        public function set responseName(pData:String):void{  
            _responseName=pData;  
        }  
  
        public function get responseValue():String{  
            return _responseValue;  
        }  
  
        public function set responseValue(pData:String):void{  
            _responseValue=pData;  
        }  
  
        public function get responsePoints():Number{  
            return _responsePoints;  
        }  
  
        public function set responsePoints(pData:Number):void{  
            _responsePoints=pData;  
        }  
  
        public function get displayOrder():int{  
            return _displayOrder;  
        }  
  
        public function set displayOrder(pData:int):void{  
            _displayOrder=pData;  
        }  
    }  
}