package br.com.tratomais.einsurance.policy.model.vo
{
	import flash.errors.IllegalOperationError;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.Installment")]     
    [Managed]  
    public class Installment extends HibernateBean 
    {  
        private var _id:InstallmentId;  
        private var _endorsement:Endorsement;  
        private var _billingMethodId:int;  
        private var _installmentType:int;  
        private var _paymentType:int;  
        private var _effectiveDate:Date;  
        private var _expiryDate:Date;  
        private var _issueDate:Date;  
	private var _dueDay:int;
        private var _dueDate:Date;  
        private var _paymentDate:Date;  
        private var _cancelDate:Date;  
        private var _netPremium:Number;  
        private var _policyCost:Number;  
        private var _inspectionCost:Number;  
        private var _fractioningAdditional:Number;  
        private var _taxValue:Number;  
        private var _installmentValue:Number;  
        private var _paidValue:Number;  
		private var _currencyId:Number;
		private var _currencyDate:Date;
		private var _conversionRate:Number;
        private var _extendedInstallmentValue:Number;  
        private var _extendedExpirationDate:Date;  
        private var _commission:Number;  
		private var _commissionFactor:Number;
        private var _commisionPaymentDate:Date;  
		private var _labour:Number;
		private var _labourFactor:Number;
		private var _labourPaymentDate:Date;
		private var _agency:Number;
		private var _agencyFactor:Number;
		private var _agencyPaymentDate:Date;
        private var _bankNumber:int;  
        private var _bankAgencyNumber:String;  
        private var _bankAccountNumber:String;  
        private var _bankCheckNumber:String;  
        private var _cardType:int;  
        private var _cardBrandType:int;  
        private var _cardNumber:String;  
        private var _cardExpirationDate:int;  
        private var _cardHolderName:String;  
        private var _invoiceNumber:int;  
        private var _otherAccountNumber:String;  
        private var _debitAuthorizationCode:int;  
        private var _creditAuthorizationCode:int;  
        private var _transmitted:Boolean;
        private var _installmentStatus:int;  
		private var _shippingCollection:Boolean;
	private var _paymentNotified:Boolean;
	private var _paymentTermId:int;

		/**
		 * Installment type - Credit
		 */
		public static const INSTALLMENT_TYPE_CREDIT:int = 156;
		
		/**
		 * Installment type - Debit type
		 */
		public static const INSTALLMENT_TYPE_DEBIT:int = 157;
	

 
        public function Installment()  
        {  
        }  
  
        public function get id():InstallmentId{  
            return _id;  
        }  
  
        public function set id(pData:InstallmentId):void{  
            _id=pData;  
        }  
  
        public function get endorsement():Endorsement{  
            return _endorsement;  
        }  
  
        public function set endorsement(pData:Endorsement):void{  
            _endorsement=pData;  
        }  
  
        public function get billingMethodId():int{  
            return _billingMethodId;  
        }  
  
        public function set billingMethodId(pData:int):void{  
            _billingMethodId=pData;  
        }  
  
        public function get installmentType():int{  
            return _installmentType;  
        }  
  
        public function set installmentType(pData:int):void{  
            _installmentType=pData;  
        }  
  
        public function get paymentType():int{  
            return _paymentType;  
        }  
  
        public function set paymentType(pData:int):void{  
            _paymentType=pData;  
        }  
  
        public function get effectiveDate():Date{  
            return _effectiveDate;  
        }  
  
        public function set effectiveDate(pData:Date):void{  
            _effectiveDate=pData;  
        }  
  
        public function get expiryDate():Date{  
            return _expiryDate;  
        }  
  
        public function set expiryDate(pData:Date):void{  
            _expiryDate=pData;  
        }  
  
        public function get issueDate():Date{  
            return _issueDate;  
        }  
  
        public function set issueDate(pData:Date):void{  
            _issueDate=pData;  
        } 
	
		public function get dueDay():int{
			return _dueDay;
		}
	
		public function set dueDay(pData:int):void{
			_dueDay = pData;
		}
	
        public function get dueDate():Date{  
            return _dueDate;  
        }  
  
        public function set dueDate(pData:Date):void{  
            _dueDate=pData;  
        }  
  
        public function get paymentDate():Date{  
            return _paymentDate;  
        }  
  
        public function set paymentDate(pData:Date):void{  
            _paymentDate=pData;  
        }  
  
        public function get cancelDate():Date{  
            return _cancelDate;  
        }  
  
        public function set cancelDate(pData:Date):void{  
            _cancelDate=pData;  
        }  
  
        public function get netPremium():Number{  
            return _netPremium;  
        }  
  
        public function set netPremium(pData:Number):void{  
            _netPremium=pData;  
        }  
  
        public function get policyCost():Number{  
            return _policyCost;  
        }  
  
        public function set policyCost(pData:Number):void{  
            _policyCost=pData;  
        }  
  
        public function get inspectionCost():Number{  
            return _inspectionCost;  
        }  
  
        public function set inspectionCost(pData:Number):void{  
            _inspectionCost=pData;  
        }  
  
        public function get fractioningAdditional():Number{  
            return _fractioningAdditional;  
        }  
  
        public function set fractioningAdditional(pData:Number):void{  
            _fractioningAdditional=pData;  
        }  
  
        public function get taxValue():Number{  
            return _taxValue;  
        }  
  
        public function set taxValue(pData:Number):void{  
            _taxValue=pData;  
        }  
  
        public function get installmentValue():Number{  
            return _installmentValue;  
        }  
  
        public function set installmentValue(pData:Number):void{  
            _installmentValue=pData;  
        }  
  
        public function get paidValue():Number{  
            return _paidValue;  
        }  
  
        public function set paidValue(pData:Number):void{  
            _paidValue=pData;  
        }  
  
		public function get currencyId():Number {
			return _currencyId;
		}
	
		public function set currencyId(pData:Number):void {
			_currencyId = pData;
		}
	
		public function get currencyDate():Date {
			return _currencyDate;
		}
	
		public function set currencyDate(pData:Date):void {
			_currencyDate = pData;
		}
	
		public function get conversionRate():Number {
			return _conversionRate;
		}
	
		public function set conversionRate(pData:Number):void {
			_conversionRate = pData;
		}

        public function get extendedInstallmentValue():Number{  
            return _extendedInstallmentValue;  
        }  
  
        public function set extendedInstallmentValue(pData:Number):void{  
            _extendedInstallmentValue=pData;  
        }  
  
        public function get extendedExpirationDate():Date{  
            return _extendedExpirationDate;  
        }  
  
        public function set extendedExpirationDate(pData:Date):void{  
            _extendedExpirationDate=pData;  
        }  
  
        public function get commission():Number{  
            return _commission;  
        }  
  
        public function set commission(pData:Number):void{  
            _commission=pData;  
        }  
  

		public function get commissionFactor():Number {
			return _commissionFactor;
		}
	
		public function set commissionFactor(pData:Number) {
			_commissionFactor = pData;
		}
	
        public function get commisionPaymentDate():Date{  
            return _commisionPaymentDate;  
        }  
  
        public function set commisionPaymentDate(pData:Date):void{  
            _commisionPaymentDate=pData;  
        }  
  
		public function get labour() {
			return _labour;
		}
	
		public function set labour(pData:Number) {
			_labour = pData;
		}
	
		public function get labourFactor() {
			return _labourFactor;
		}
	
		public function set labourFactor(pData:Number) {
			_labourFactor = pData;
		}
	
		public function get labourPaymentDate() {
			return _labourPaymentDate;
		}
	
		public function set labourPaymentDate(pData:Date) {
			_labourPaymentDate = pData;
		}
	
		public function get agency() {
			return _agency;
		}
	
		public function set agency(pData:Number) {
			_agency = pData;
		}
	
		public function get agencyFactor() {
			return _agencyFactor;
		}
	
		public function set agencyFactor(pData:Number) {
			_agencyFactor = pData;
		}
	
		public function get agencyPaymentDate() {
			return _agencyPaymentDate;
		}
	
		public function set agencyPaymentDate(pData:Date) {
			_agencyPaymentDate = pData;
		}
	
        public function get bankNumber():int{  
            return _bankNumber;  
        }  
  
        public function set bankNumber(pData:int):void{  
            _bankNumber=pData;  
        }  
  
        public function get bankAgencyNumber():String{  
            return _bankAgencyNumber;  
        }  
  
        public function set bankAgencyNumber(pData:String):void{  
            _bankAgencyNumber=pData;  
        }  
  
        public function get bankAccountNumber():String{  
            return _bankAccountNumber;  
        }  
  
        public function set bankAccountNumber(pData:String):void{  
            _bankAccountNumber=pData;  
        }  
  
        public function get bankCheckNumber():String{  
            return _bankCheckNumber;  
        }  
  
        public function set bankCheckNumber(pData:String):void{  
            _bankCheckNumber=pData;  
        }  
  
        public function get cardType():int{  
            return _cardType;  
        }  
  
        public function set cardType(pData:int):void{  
            _cardType=pData;  
        }  
  
        public function get cardBrandType():int{  
            return _cardBrandType;  
        }  
  
        public function set cardBrandType(pData:int):void{  
            _cardBrandType=pData;  
        }  
  
        public function get cardNumber():String{  
            return _cardNumber;  
        }  
  
        public function set cardNumber(pData:String):void{  
            _cardNumber=pData;  
        }  
  
        public function get cardExpirationDate():int{  
            return _cardExpirationDate;  
        }  
  
        public function set cardExpirationDate(pData:int):void{  
            _cardExpirationDate=pData;  
        }  
  
        public function get cardHolderName():String{  
            return _cardHolderName;  
        }  
  
        public function set cardHolderName(pData:String):void{  
            _cardHolderName=pData;  
        }  
  
        public function get invoiceNumber():int{  
            return _invoiceNumber;  
        }  
  
        public function set invoiceNumber(pData:int):void{  
            _invoiceNumber=pData;  
        }  
  
        public function get otherAccountNumber():String{  
            return _otherAccountNumber;  
        }  
  
        public function set otherAccountNumber(pData:String):void{  
            _otherAccountNumber=pData;  
        }  
  
        public function get debitAuthorizationCode():int{  
            return _debitAuthorizationCode;  
        }  
  
        public function set debitAuthorizationCode(pData:int):void{  
            _debitAuthorizationCode=pData;  
        }  
  
        public function get creditAuthorizationCode():int{  
            return _creditAuthorizationCode;  
        }  
  
        public function set creditAuthorizationCode(pData:int):void{  
            _creditAuthorizationCode=pData;  
        }  
  
        public function get transmitted(): Boolean { 
        	return _transmitted;
        }
        
        public function set transmitted(pTransmitted: Boolean): void {
        	_transmitted = pTransmitted;
        }
        
        public function get installmentStatus():int{  
            return _installmentStatus;  
        }  
  
        public function set installmentStatus(pData:int):void{  
            _installmentStatus=pData;  
        }  

        public function get shippingCollection():Boolean{  
            return _shippingCollection;  
        }  
  
        public function set shippingCollection(pData:Boolean):void{  
            _shippingCollection=pData;  
        }  
        
        public function get paymentNotified():Boolean{  
            return _paymentNotified;  
        }  
  
        public function set paymentNotified(pData:Boolean):void{  
            _paymentNotified=pData;  
        }  
        
		public function updateKey(parent:Endorsement):void{
			if ( parent.id == null ) {
					throw new IllegalOperationError();
			}
	
			this.endorsement = parent;
	
			if ( this.id == null ) {
	//			this.setId( new InstallmentId(0, parent.getId().getEndorsementId(), parent.getId().getContractId()) );
				this.id =  new InstallmentId();
				this.id.installmentId = 0;
				this.id.endorsementId = parent.id.endorsementId;
				this.id.contractId = parent.id.contractId;
				
			}else{
				this.id.endorsementId = parent.id.endorsementId;
				this.id.contractId = parent.id.contractId;
			}
			this.endorsement = parent ;
		}
	
		public function get paymentTermId():int{
			return _paymentTermId;
		}
	
		public function set paymentTermId(pData:int):void{
			_paymentTermId = pData;
		}
   }
}