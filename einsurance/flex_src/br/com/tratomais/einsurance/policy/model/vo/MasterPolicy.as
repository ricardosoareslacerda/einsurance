package br.com.tratomais.einsurance.policy.model.vo 
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.MasterPolicy")]    
    [Managed]  
    public class MasterPolicy extends HibernateBean 
 	{  
  
        private var _masterPolicyId:int;  
        private var _insurerId:int;  
        private var _partnerId:int;  
        private var _brokerId:int;
        private var _productId:int;  
        private var _policyNumber:String; 
        private var _policyInformed:Boolean;
        private var _commissionFactor:Number;  
        private var _effectiveDate:Date;  
        private var _expiryDate:Date;  
        private var _labourFactor:Number;  
        private var _agencyFactor:Number;  
        private var _invoiceCutDay:int;  
        private var _invoiceIssueDay:int;  
        private var _invoiceDueDay:int;  
        private var _contracts:ArrayCollection;  
  		private var _automaticRenewal:Boolean;
  		private var _renewalBeforeDay:int;
  
  		private var _partnerName:String;
  		private var _productName:String;
  		private var _brokerName:String;
  		
        public function MasterPolicy()  
        {  
        }  
  
        public function get masterPolicyId():int{  
            return _masterPolicyId;  
        }  
  
        public function set masterPolicyId(pData:int):void{  
            _masterPolicyId=pData;  
        }  
  
        public function get insurerId():int{  
            return _insurerId;  
        }  
  
        public function set insurerId(pData:int):void{  
            _insurerId=pData;  
        }  
  
        public function get partnerId():int{  
            return _partnerId;  
        }  
  
        public function set partnerId(pData:int):void{  
            _partnerId=pData;  
        }
          
  		public function get brokerId():int{  
            return _brokerId;  
        }  
  
        public function set brokerId(pData:int):void{  
            _brokerId=pData;  
        }
         
        public function get productId():int{  
            return _productId;  
        }  
  
        public function set productId(pData:int):void{  
            _productId=pData;  
        }  
  
        public function get policyNumber():String{  
            return _policyNumber;  
        }  
  
        public function set policyNumber(pData:String):void{  
            _policyNumber=pData;  
        }  
  
        public function get commissionFactor():Number{  
            return _commissionFactor;  
        }  
  
        public function set commissionFactor(pData:Number):void{  
            _commissionFactor=pData;  
        }  
  
        public function get labourFactor():Number{  
            return _labourFactor;  
        }  
  
        public function set labourFactor(pData:Number):void{  
            _labourFactor=pData;  
        }  
  
        public function get agencyFactor():Number{  
            return _agencyFactor;  
        }  
  
        public function set agencyFactor(pData:Number):void{  
            _agencyFactor=pData;  
        }  
  
        public function get invoiceCutDay():int{  
            return _invoiceCutDay;  
        }  
  
        public function set invoiceCutDay(pData:int):void{  
            _invoiceCutDay=pData;  
        }  
  
        public function get invoiceIssueDay():int{  
            return _invoiceIssueDay;  
        }  
  
        public function set invoiceIssueDay(pData:int):void{  
            _invoiceIssueDay=pData;  
        }  
  
        public function get invoiceDueDay():int{  
            return _invoiceDueDay;  
        }  
  
        public function set invoiceDueDay(pData:int):void{  
            _invoiceDueDay=pData;  
        }  
  
        public function get contracts():ArrayCollection{  
            return _contracts;  
        }  
  
        public function set contracts(pData:ArrayCollection):void{  
            _contracts=pData;  
        }
        
        public function get policyInformed():Boolean{
        	return _policyInformed;
        }  
        
        public function set policyInformed(pData:Boolean):void{
        	_policyInformed=pData;
        }
        
        public function get effectiveDate():Date{  
            return _effectiveDate;  
        }  
  
        public function set effectiveDate(pData:Date):void{  
            _effectiveDate=pData;  
        }  
  
        public function get expiryDate():Date{  
            return _expiryDate;  
        }  
  
        public function set expiryDate(pData:Date):void{  
            _expiryDate=pData;  
        }  
  
        public function get partnerName():String{
        	return _partnerName;
        }
        
        public function set partnerName(pData:String):void{
        	_partnerName=pData;
        }
        
        public function get productName():String{
        	return _productName;
        }
        
        public function set productName(pData:String):void{
        	_productName=pData;
        }
        
        public function get brokerName():String{
        	return _brokerName;
        }
        
        public function set brokerName(pData:String):void{
        	_brokerName=pData;
        }
        
        public function get automaticRenewal(): Boolean {
        	return this._automaticRenewal;
        }
        
        public function set automaticRenewal(pData: Boolean):void{
        	this._automaticRenewal = pData;
        }
        
        public function get renewalBeforeDay():int{
        	return this._renewalBeforeDay;
        }
        
        public function set renewalBeforeDay(pData: int):void{
        	this._renewalBeforeDay = pData;
        }
    }  
}  