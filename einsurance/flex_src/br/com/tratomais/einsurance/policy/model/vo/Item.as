package br.com.tratomais.einsurance.policy.model.vo
{
	import br.com.tratomais.einsurance.customer.model.vo.Customer;
	
	import flash.errors.IllegalOperationError;
	
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.Item")]    
    [Managed]  
    public class Item  extends HibernateBean  
    {  
        private var _id:ItemId;  
        private var _customer:Customer;  
        private var _endorsement:Endorsement;  
        private var _objectId:int;  
        private var _coveragePlanId:int;  
        private var _renewalType:int;  
        private var _renewalInsurerId:int;  
        private var _renewalPolicyNumber:String;  
		private var _bonusType:int;
        private var _effectiveDate:Date;  
        private var _cancelDate:Date;  
        private var _tariffFactor:Number;  
        private var _commercialFactor:Number;  
        private var _purePremium:Number;  
        private var _tariffPremium:Number;  
        private var _commercialPremium:Number;  
        private var _profitPremium:Number;
        private var _technicalPremium:Number;
        private var _retainedPremium:Number;  
        private var _earnedPremium:Number;
        private var _unearnedPremium:Number;
        private var _netPremium:Number;  
		private var _commission:Number;
		private var _commissionFactor:Number;
		private var _labour:Number;
		private var _labourFactor:Number;
		private var _agency:Number;
		private var _agencyFactor:Number;
		private var _fractioningAdditional:Number;
        private var _taxValue:Number;  
        private var _totalPremium:Number;  
        private var _authorizationLevel:int;  
        private var _calculationValid:Boolean; 
        private var _itemStatus:int;  
		private var _claimNumber:String;
		private var _claimDate:Date;
		private var _claimNotification:Date;        
        private var _itemClauses:ArrayCollection = new ArrayCollection();  
        private var _itemCoverages:ArrayCollection = new ArrayCollection();   
        private var _itemProfiles:ArrayCollection = new ArrayCollection();   
        private var _itemRiskTypes:ArrayCollection = new ArrayCollection();
        private var _beneficiaries:ArrayCollection = new ArrayCollection();  
        private var _coverageOptions:ArrayCollection = new ArrayCollection();
		private var _itemClause:ItemClause;
		
		public static const OBJECT_LIFE:int = 1; 
		public static const OBJECT_PERSONAL_ACCIDENT:int = 2; 
		public static const OBJECT_LIFE_CYCLE:int = 3;
		public static const OBJECT_HABITAT:int = 4; 
		public static const OBJECT_CREDIT_INSURANCE:int = 5;
		public static const OBJECT_AUTOMOVIL:int = 6;
		public static const OBJECT_PURCHASE_PROTECTED:int = 7;
		public static const OBJECT_CAPITAL:int = 8;
		public static const OBJECT_CONDOMINIUM:int = 10;
		
		public static const ACTIVE:int = 110;
		public static const INACTIVE:int = 111;
		
	  	public static const RENEW_TYPE_NEW:int = 91;
		public static const RENEW_TYPE_RENEWAL:int = 93;
		
        public function Item()  
        {  
        }  
          		
        public function get id():ItemId{  
            return _id;  
        }  
  
        public function set id(pData:ItemId):void{  
            _id=pData;  
        }  
  
        public function get customer():Customer{  
            return _customer;  
        }  
  
        public function set customer(pData:Customer):void{  
            _customer=pData;  
        }  
  
        public function get endorsement():Endorsement{  
            return _endorsement;  
        }  
  
        public function set endorsement(pData:Endorsement):void{  
            _endorsement=pData;  
        }  
  
        public function get objectId():int{  
            return _objectId;  
        }  
  
        public function set objectId(pData:int):void{  
            _objectId=pData;  
        }  
  
        public function get coveragePlanId():int{  
            return _coveragePlanId;  
        }  
  
        public function set coveragePlanId(pData:int):void{  
            _coveragePlanId=pData;  
        }  
  
        public function get renewalType():int{  
            return _renewalType;  
        }  
  
        public function set renewalType(pData:int):void{  
            _renewalType=pData;  
        }  
  
        public function get renewalInsurerId():int{  
            return _renewalInsurerId;  
        }  
  
        public function set renewalInsurerId(pData:int):void{  
            _renewalInsurerId=pData;  
        }  
  
        public function get renewalPolicyNumber():String{  
            return _renewalPolicyNumber;  
        }  
  
        public function set renewalPolicyNumber(pData:String):void{  
            _renewalPolicyNumber=pData;  
        }  
  
		public function get bonusType():int {
			return _bonusType;
		}
	
		public function set bonusType(pData:int):void {
			_bonusType = pData;
		}
	
        public function get effectiveDate():Date{  
            return _effectiveDate;  
        }  
  
        public function set effectiveDate(pData:Date):void{  
            _effectiveDate=pData;  
        }  
  
        public function get cancelDate():Date{  
            return _cancelDate;  
        }  
  
        public function set cancelDate(pData:Date):void{  
            _cancelDate=pData;  
        }  
  
        public function get tariffFactor():Number{  
            return _tariffFactor;  
        }  
  
        public function set tariffFactor(pData:Number):void{  
            _tariffFactor=pData;  
        }  
  
        public function get commercialFactor():Number{  
            return _commercialFactor;  
        }  
  
        public function set commercialFactor(pData:Number):void{  
            _commercialFactor=pData;  
        }  
  
        public function get purePremium():Number{  
            return _purePremium;  
        }  
  
        public function set purePremium(pData:Number):void{  
            _purePremium=pData;  
        }  
  
        public function get tariffPremium():Number{  
            return _tariffPremium;  
        }  
  
        public function set tariffPremium(pData:Number):void{  
            _tariffPremium=pData;  
        }  
  
        public function get commercialPremium():Number{  
            return _commercialPremium;  
        }  
  
        public function set commercialPremium(pData:Number):void{  
            _commercialPremium=pData;  
        }  
  
        public function get profitPremium():Number{  
            return _profitPremium;  
        }  
  
        public function set profitPremium(pData:Number):void{  
            _profitPremium=pData;  
        }  
  
        public function get technicalPremium():Number{  
            return _technicalPremium;  
        }  
  
        public function set technicalPremium(pData:Number):void{  
            _technicalPremium=pData;  
        }  
  
        public function get retainedPremium():Number{  
            return _retainedPremium;  
        }  
  
        public function set retainedPremium(pData:Number):void{  
            _retainedPremium=pData;  
        }
        
		public function get earnedPremium():Number{  
            return _earnedPremium;  
        }  
  
        public function set earnedPremium(pData:Number):void{  
            _earnedPremium=pData;  
        }  
  
        public function get unearnedPremium():Number{  
            return _unearnedPremium;  
        }  
  
        public function set unearnedPremium(pData:Number):void{  
            _unearnedPremium=pData;  
        }  
  
        public function get netPremium():Number{  
            return _netPremium;  
        }  
  
        public function set netPremium(pData:Number):void{  
            _netPremium=pData;  
        }  
  
  		public function get commission():Number{  
            return _commission;  
        }  
  
        public function set commission(pData:Number):void{  
            _commission=pData;  
        }  

  		public function get commissionFactor():Number{  
            return _commissionFactor;  
        }  
  
        public function set commissionFactor(pData:Number):void{  
            _commissionFactor=pData;  
        }  

		public function get labour():Number{  
            return _labour;  
        }  
  
        public function set labour(pData:Number):void{  
            _labour=pData;  
        } 
        
        public function get labourFactor():Number{  
            return _labourFactor;  
        }  
  
        public function set labourFactor(pData:Number):void{  
            _labourFactor=pData;  
        } 
	
	  	public function get agency():Number{  
            return _agency;  
        }  
  
        public function set agency(pData:Number):void{  
            _agency=pData;  
        } 
        
        public function get agencyFactor():Number{  
            return _agencyFactor;  
        }  
  
        public function set agencyFactor(pData:Number):void{  
            _agencyFactor=pData;  
        } 

		public function get fractioningAdditional():Number {
			return _fractioningAdditional;
		}
	
		public function set fractioningAdditional(pData:Number):void {
			_fractioningAdditional = pData;
		}
	
	    public function get taxValue():Number{  
            return _taxValue;  
        }  
  
        public function set taxValue(pData:Number):void{  
            _taxValue=pData;  
        } 

	    public function get totalPremium():Number{  
            return _totalPremium;  
        }  
  
        public function set totalPremium(pData:Number):void{  
            _totalPremium=pData;  
        } 
        
        public function get authorizationLevel():Number{  
            return _authorizationLevel;  
        }  
  
        public function set authorizationLevel(pData:Number):void{  
            _authorizationLevel=pData;  
        } 

        public function get calculationValid(): Boolean { 
        	return _calculationValid;
        }
        
        public function set calculationValid(pCalculationValid: Boolean): void {
        	this._calculationValid = pCalculationValid;
        }

        public function get itemStatus():int{  
            return _itemStatus;  
        }  
  
        public function set itemStatus(pData:int):void{  
            _itemStatus=pData;  
        }  

		public function get claimNumber():String{
			return _claimNumber
		}
		
		public function set claimNumber(pData:String):void{
			_claimNumber = pData;
		}		

		public function get claimDate():Date{
			return _claimDate
		}
		
		public function set claimDate(pData:Date):void{
			_claimDate = pData;
		}		

		public function get claimNotification():Date{
			return _claimNotification
		}
		
		public function set claimNotification(pData:Date):void{
			_claimNotification = pData;
		}
  
        public function get itemClauses():ArrayCollection{  
            return _itemClauses;  
        }  
  
        public function set itemClauses(pData:ArrayCollection):void{  
            _itemClauses=pData;  
        }  
  
        public function get itemCoverages():ArrayCollection{  
            return _itemCoverages;  
        }  
  
        public function set itemCoverages(pData:ArrayCollection):void{  
            _itemCoverages=pData;  
        }  
  
        public function get itemProfiles():ArrayCollection{  
            return _itemProfiles;  
        }  
  
        public function set itemProfiles(pData:ArrayCollection):void{  
            _itemProfiles=pData;  
        }
        
        public function get itemRiskTypes():ArrayCollection{  
            return _itemRiskTypes;
        }  
  
        public function set itemRiskTypes(pData:ArrayCollection):void{  
            _itemRiskTypes=pData;
        }
        
        public function get beneficiaries():ArrayCollection{  
            return _beneficiaries;  
        }  
  
        public function set beneficiaries(pData:ArrayCollection):void{  
            _beneficiaries=pData;  
        }  
        
		public function updateKey(parent:Endorsement):void{
			if ( parent.id == null ) {
				throw new IllegalOperationError();
			}
			this.endorsement = parent ;
			if ( this.id == null ) {
				this.id =  new ItemId();
				this.id.itemId = parent.getNextItemId();
				this.id.endorsementId = parent.id.endorsementId;
				this.id.contractId = parent.id.contractId;
			}else{
				this.id.endorsementId = parent.id.endorsementId;
				this.id.contractId = parent.id.contractId;
			}
		}

		public function addCoverage(coverageId:int):ItemCoverage{
			if ( coverageId == 0 ) {
				throw new IllegalOperationError("invalid coverage code");
			}
			var newCoverage:ItemCoverage = new ItemCoverage();
			var newCoverageId:ItemCoverageId = new ItemCoverageId();
	
			if ( this.id != null  ) {
				newCoverageId.itemId = this.id.itemId;
				newCoverageId.endorsementId = this.id.endorsementId;
				newCoverageId.contractId = this.id.contractId;
			}
			newCoverageId.coverageId =  coverageId ;
	
			newCoverage.id = newCoverageId;
			
			newCoverage.item = this ;
			this.itemCoverages.addItem( newCoverage );
			
			return newCoverage;
		}
		
		public function attachCoverage(newItemCoverage:ItemCoverage):ItemCoverage{
			this.itemCoverages.addItem( newItemCoverage );
			return newItemCoverage;
		}	

  		public function addBeneficiary() : Beneficiary{
			var beneficiary: Beneficiary = new Beneficiary();
			var newBeneficiaryId : BeneficiaryId = new BeneficiaryId();
	
			if ( this.id != null  ) {
				newBeneficiaryId.itemId = 		this.id.itemId;
				newBeneficiaryId.endorsementId= this.id.endorsementId;
				newBeneficiaryId.contractId= 	this.id.contractId;
			}
 			newBeneficiaryId.beneficiaryId = this.getNextBeneficiaryId();
 	
			beneficiary.id = newBeneficiaryId ;
			
			beneficiary.item = this;
			this.beneficiaries.addItem( beneficiary );
				
			return beneficiary;
		}
		
		public function attachBeneficiary(newBeneficiary:Beneficiary):Beneficiary{
			this.beneficiaries.addItem( newBeneficiary );
			return newBeneficiary;
		}	
	
		/**
		 * @return
		 */
		public function getNextBeneficiaryId() : int {
			var maxSequenceId : int = 0;
			
			for each (var tmpBeneficiary : Beneficiary in this.beneficiaries ) {
				if ( tmpBeneficiary.id != null && tmpBeneficiary.id.beneficiaryId > maxSequenceId ) {
					maxSequenceId = tmpBeneficiary.id.beneficiaryId;
				}
			}
			
			maxSequenceId++;
			return maxSequenceId;
		}

        public function get coverageOptions():ArrayCollection{  
            return _coverageOptions;  
        }  
  
        public function set coverageOptions(pData:ArrayCollection):void{  
            _coverageOptions=pData;  
        }
        
 		public function addRiskType(riskType:int):ItemRiskType{
			if ( riskType == 0 ) {
				throw new IllegalOperationError("invalid riskType code");
			}
			var newRiskType:ItemRiskType = new ItemRiskType();
			var newRiskTypeId:ItemRiskTypeId = new ItemRiskTypeId();
			
			if ( this.id != null  ) {
				newRiskTypeId.itemId = this.id.itemId;
				newRiskTypeId.endorsementId = this.id.endorsementId;
				newRiskTypeId.contractId = this.id.contractId;
			}
			newRiskTypeId.riskType = riskType;
			
			newRiskType.id = newRiskTypeId;
			
			newRiskType.item = this ;
			this.itemRiskTypes.addItem( newRiskType );
			
			return newRiskType;
		}
		
		public function attachRiskType(newItemRiskType:ItemRiskType):ItemRiskType{
			this.itemRiskTypes.addItem( newItemRiskType );
			return newItemRiskType;
		}        
    }  
} 
