package br.com.tratomais.einsurance.policy.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
    [RemoteClass(alias="br.com.tratomais.core.model.auto.AutoVersion")]    
    [Managed]	
	public class AutoVersion extends HibernateBean
	{
		private var _id:AutoVersionID
		private var _expiryDate:Date
		private var _autoClassType:int
		private var _autoTariffType:int
		private var _autoCategoryType:int
		private var _autoGroupType:int
		private var _autoUseType:int
		private var _registred:Date
		private var _updated:Date

		public function set id( pData:AutoVersionID ):void
		{
			this._id = pData;
		}
		
		public function get id():AutoVersionID
		{
			return _id;
		}
		
		public function set expiryDate( pData:Date ):void
		{
			this._expiryDate = pData;
		}
		
		public function get expiryDate():Date
		{
			return _expiryDate;
		}
		
		public function set autoClassType( pData:int ):void
		{
			this._autoClassType = pData;
		}
		
		public function get autoClassType():int
		{
			return _autoClassType;
		}
		
		public function get autoTariffType():int
		{
			return this._autoTariffType;
		}
		
		
		public function set autoTariffType( pData:int ):void
		{
			this._autoTariffType = pData;
		}
		
		public function get autoCategoryType():int
		{
			return _autoCategoryType;
		}
		
		public function set autoCategoryType( pData:int ):void
		{
			this._autoCategoryType = pData;
		}
		
		public function set autoGroupType( pData:int ):void
		{
			this._autoGroupType = pData;
		}
		
		public function get autoGroupType():int
		{
			return _autoGroupType;
		}
		
		public function set autoUseType( pData:int ):void
		{
			this._autoUseType = pData;
		}
		
		public function get autoUseType():int
		{
			return _autoUseType;
		}
		
		public function set registred( pData:Date ):void
		{
			this._registred = pData;
		}
		
		public function get registred():Date
		{
			return _registred;
		}
		
		public function set updated( pData:Date ):void
		{
			this._updated = pData;
		}
		
		public function get updated():Date
		{
			return _updated;
		}
	}
}