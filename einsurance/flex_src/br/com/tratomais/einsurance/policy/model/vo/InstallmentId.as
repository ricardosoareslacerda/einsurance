package br.com.tratomais.einsurance.policy.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.InstallmentId")]   
    [Managed]  
    public class InstallmentId extends HibernateBean 
    {  
        private var _installmentId:int;  
        private var _endorsementId:int;  
        private var _contractId:int;  
  
        public function InstallmentId()  
        {  
        }  
  
        public function get installmentId():int{  
            return _installmentId;  
        }  
  
        public function set installmentId(pData:int):void{  
            _installmentId=pData;  
        }  
  
        public function get endorsementId():int{  
            return _endorsementId;  
        }  
  
        public function set endorsementId(pData:int):void{  
            _endorsementId=pData;  
        }  
  
        public function get contractId():int{  
            return _contractId;  
        }  
  
        public function set contractId(pData:int):void{  
            _contractId=pData;  
        }  
    }  
}