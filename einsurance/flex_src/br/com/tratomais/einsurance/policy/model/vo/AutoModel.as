package br.com.tratomais.einsurance.policy.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
	[RemoteClass(alias="br.com.tratomais.core.model.auto.AutoModel")]	
	[Managed]
	public class AutoModel extends HibernateBean
	{

		private var _autoModelId:int;
		private var _autoMark:AutoBrand;
		private var _autoModelCode:String;
		private var _modelName:String;
		private var _autoFuelType:int;
		private var _autoTransmissionType:int;
		private var _doorNumber:int;
		private var _passengerNumber:int;
		private var _registred:Date;
		private var _updated:Date;


		public function AutoModel()
		{
			
		}

		public function get autoModelId():int{
			return _autoModelId;
		}

		public function set autoModelId(pData:int):void{
			_autoModelId=pData;
		}

		public function get autoMark():AutoBrand{
			return _autoMark;
		}

		public function set autoMark(pData:AutoBrand):void{
			_autoMark=pData;
		}

		public function get autoModelCode():String{
			return _autoModelCode;
		}

		public function set autoModelCode(pData:String):void{
			_autoModelCode=pData;
		}

		public function get modelName():String{
			return _modelName;
		}

		public function set modelName(pData:String):void{
			_modelName=pData;
		}

		public function get autoFuelType():int{
			return _autoFuelType;
		}

		public function set autoFuelType(pData:int):void{
			_autoFuelType=pData;
		}

		public function get autoTransmissionType():int{
			return _autoTransmissionType;
		}

		public function set autoTransmissionType(pData:int):void{
			_autoTransmissionType=pData;
		}

		public function get doorNumber():int{
			return _doorNumber;
		}

		public function set doorNumber(pData:int):void{
			_doorNumber=pData;
		}

		public function get passengerNumber():int{
			return _passengerNumber;
		}

		public function set passengerNumber(pData:int):void{
			_passengerNumber=pData;
		}

		public function get registred():Date{
			return _registred;
		}

		public function set registred(pData:Date):void{
			_registred=pData;
		}

		public function get updated():Date{
			return _updated;
		}

		public function set updated(pData:Date):void{
			_updated=pData;
		}


		
	}
}