package br.com.tratomais.einsurance.policy.model.vo
{
	

	
	[RemoteClass(alias="br.com.tratomais.core.model.policy.MotoristId")]	
	[Bindable]
	public class MotoristId
	{

		private var _contractId:int;
		private var _endorsementId:int;
		private var _itemId:int;
		private var _motoristId:int;


		public function MotoristId()
		{
			
		}

		public function get contractId():int{
			return _contractId;
		}

		public function set contractId(pData:int):void{
			_contractId=pData;
		}

		public function get endorsementId():int{
			return _endorsementId;
		}

		public function set endorsementId(pData:int):void{
			_endorsementId=pData;
		}

		public function get itemId():int{
			return _itemId;
		}

		public function set itemId(pData:int):void{
			_itemId=pData;
		}

		public function get motoristId():int{
			return _motoristId;
		}

		public function set motoristId(pData:int):void{
			_motoristId=pData;
		}
	}
}