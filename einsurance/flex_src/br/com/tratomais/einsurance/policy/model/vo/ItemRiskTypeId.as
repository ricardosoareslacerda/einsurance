package br.com.tratomais.einsurance.policy.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;

    [RemoteClass(alias="br.com.tratomais.core.model.policy.ItemRiskTypeId")]      
    [Managed] 
	public class ItemRiskTypeId extends HibernateBean
	{
		private var _contractId:int;
		private var _endorsementId:int;
		private var _itemId:int;
		private var _riskType:int;
		
		public function ItemRiskTypeId(){}

		public function get contractId():int
		{
			return this._contractId;
		}
		
		public function set contractId( pData:int ):void
		{
			this._contractId = pData;
		}
		
		public function get endorsementId():int
		{
			return this._endorsementId;
		}
		
		public function set endorsementId( pData:int ):void
		{
			this._endorsementId = pData;
		}
		
		public function get itemId():int
		{
			return this._itemId;
		}
		
		public function set itemId( pData:int ):void
		{
			this._itemId = pData;
		}
		
		public function get riskType():int
		{
			return this._riskType;
		}
		
		public function set riskType( pData:int ):void
		{
			this._riskType = pData;
		}
	}
}