package br.com.tratomais.einsurance.policy.model.vo
{
	import flash.errors.IllegalOperationError;
	
	[RemoteClass(alias="br.com.tratomais.core.model.policy.Motorist")]	
    [Managed]
	public class Motorist
	{

		public static const MOTORIST_TYPE_MAIN = 484;
		public static const MOTORIST_TYPE_OTHERS = 485;
		
		private var _id:MotoristId;
		private var _itemAuto:ItemAuto;
		private var _motoristType:int;
		private var _name:String;
		private var _lastName:String;
		private var _firstName:String;
		private var _kinshipType:int;
		private var _gender:int;
		private var _birthDate:Date;
		private var _maritalStatus:int;
		private var _licenseDate:Date;
		private var _licenseCode:String;
		private var _documentType:int;
		private var _documentNumber:String;
		private var _documentIssuer:String;
		private var _documentValidity:Date;
		private var _displayOrder:int;

		public function Motorist () {
			
		}

		public function get id():MotoristId{
			return _id;
		}

		public function set id(pData:MotoristId):void{
			_id=pData;
		}

		public function get itemAuto():ItemAuto{
			return _itemAuto;
		}

		public function set itemAuto(pData:ItemAuto):void{
			_itemAuto=pData;
		}

		public function get motoristType():int{
			return _motoristType;
		}

		public function set motoristType(pData:int):void{
			_motoristType=pData;
		}

		public function get name():String{
			return _name;
		}

		public function set name(pData:String):void{
			_name=pData;
		}

		public function get lastName():String{
			return _lastName;
		}

		public function set lastName(pData:String):void{
			_lastName=pData;
		}

		public function get firstName():String{
			return _firstName;
		}

		public function set firstName(pData:String):void{
			_firstName=pData;
		}

		public function get kinshipType():int{
			return _kinshipType;
		}

		public function set kinshipType(pData:int):void{
			_kinshipType=pData;
		}

		public function get gender():int{
			return _gender;
		}

		public function set gender(pData:int):void{
			_gender=pData;
		}

		public function get birthDate():Date{
			return _birthDate;
		}

		public function set birthDate(pData:Date):void{
        	if( pData )
            	_birthDate= new Date(pData.valueOf() + pData.getTimezoneOffset()*60000);
            else
            	_birthDate = null;
		}

		public function get maritalStatus():int{
			return _maritalStatus;
		}

		public function set maritalStatus(pData:int):void{
			_maritalStatus=pData;
		}

		public function get licenseDate():Date{
			return _licenseDate;
		}

		public function set licenseDate(pData:Date):void{
			_licenseDate=pData;
		}

		public function get licenseCode():String{
			return _licenseCode;
		}

		public function set licenseCode(pData:String):void{
			_licenseCode=pData;
		}

		public function get documentType():int{
			return _documentType;
		}

		public function set documentType(pData:int):void{
			_documentType=pData;
		}

		public function get documentNumber():String{
			return _documentNumber;
		}

		public function set documentNumber(pData:String):void{
			_documentNumber=pData;
		}

		public function get documentIssuer():String{
			return _documentIssuer;
		}

		public function set documentIssuer(pData:String):void{
			_documentIssuer=pData;
		}

		public function get documentValidity():Date{
			return _documentValidity;
		}

		public function set documentValidity(pData:Date):void{
			_documentValidity=pData;
		}

		public function get displayOrder():int{
			return _displayOrder;
		}

		public function set displayOrder(pData:int):void{
			_displayOrder=pData;
		}

		public function updateKey(parent:ItemAuto):void{
			if ( parent.id == null ) {
				throw new IllegalOperationError();
			}
			this.itemAuto = parent ;
			if ( this.id == null ) {
				this.id =  new MotoristId();
				this.id.motoristId = parent.getNextMotoristId();
				this.id.itemId = parent.id.itemId;
				this.id.endorsementId = parent.id.endorsementId;
				this.id.contractId = parent.id.contractId;
			}else{
				this.id.itemId = parent.id.itemId;
				this.id.endorsementId = parent.id.endorsementId;
				this.id.contractId = parent.id.contractId;
			}
	    }
	}
}