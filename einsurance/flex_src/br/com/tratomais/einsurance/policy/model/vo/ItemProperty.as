package br.com.tratomais.einsurance.policy.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.ItemProperty")]    
    [Managed]  
    public class ItemProperty extends Item  
    {  
        /* private var _item:Item;  */ 
        private var _propertyType:int;  
        private var _activityType:int
        private var _addressStreet:String;  
        private var _districtName:String;  
        private var _cityId:String;  
        private var _cityName:String;  
        private var _regionName:String;  
        private var _stateId:String;  
        private var _stateName:String;  
        private var _zipCode:String;  
        private var _countryId:String;  
        private var _countryName:String;  
        private var _propertyRiskType:int;  
        private var _contentValue:Number;  
        private var _structureValue:Number;  
        private var _propertyValue:Number;  
        private var _acquisitionDate:Date;  
        private var _registrationDate:Date;  
        private var _registrationName:String;  
        private var _registrationTome:String;  
        private var _registrationNumber:String;  
        private var _registrationProtocol:String;  
        private var _boundaryNorth:String;  
        private var _boundarySouth:String;  
        private var _boundaryEast:String;  
        private var _boundaryWest:String;
        private var _singleAddress:Boolean;  
        private var _regionCode:int;
    		
        public function ItemProperty()  
        {  
        }  
  
        /* public function get item():Item{  
            return _item;  
        }  
  
        public function set item(pData:Item):void{  
            _item=pData;  
        } */  
  
        public function get regionCode():int{  
            return _regionCode;  
        }  
  
        public function set regionCode(pData:int):void{  
            _regionCode=pData;  
        }  
        
        public function get propertyType():int{  
            return _propertyType;  
        }  
  
        public function set propertyType(pData:int):void{  
            _propertyType=pData;  
        }  
  
        public function get activityType():int{  
            return _activityType;  
        }  
  
        public function set activityType(pData:int):void{  
            _activityType=pData;  
        }
        
        public function get addressStreet():String{  
            return _addressStreet;  
        }  
  
        public function set addressStreet(pData:String):void{  
            _addressStreet=pData;  
        }  
  
        public function get districtName():String{  
            return _districtName;  
        }  
  
        public function set districtName(pData:String):void{  
            _districtName=pData;  
        }  
  
        public function get cityId():String{  
            return _cityId;  
        }  
  
        public function set cityId(pData:String):void{  
            _cityId=pData;  
        }  
  
        public function get cityName():String{  
            return _cityName;  
        }  
  
        public function set cityName(pData:String):void{  
            _cityName=pData;  
        }  
  
        public function get regionName():String{  
            return _regionName;  
        }  
  
        public function set regionName(pData:String):void{  
            _regionName=pData;  
        }  
  
        public function get stateId():String{  
            return _stateId;  
        }  
  
        public function set stateId(pData:String):void{  
            _stateId=pData;  
        }  
  
        public function get stateName():String{  
            return _stateName;  
        }  	
  
        public function set stateName(pData:String):void{  
            _stateName=pData;  
        }  
  
        public function get zipCode():String{  
            return _zipCode;  
        }  
  
        public function set zipCode(pData:String):void{  
            _zipCode=pData;  
        }  
  
        public function get countryId():String{  
            return _countryId;  
        }  
  
        public function set countryId(pData:String):void{  
            _countryId=pData;  
        }  
  
        public function get countryName():String{  
            return _countryName;  
        }  
  
        public function set countryName(pData:String):void{  
            _countryName=pData;  
        }  
  
        public function get propertyRiskType():int{  
            return _propertyRiskType;  
        }  
  
        public function set propertyRiskType(pData:int):void{  
            _propertyRiskType=pData;  
        }  
  
        public function get contentValue():Number{  
            return _contentValue;  
        }  
  
        public function set contentValue(pData:Number):void{  
            _contentValue=pData;  
        }  
  
        public function get structureValue():Number{  
            return _structureValue;  
        }  
  
        public function set structureValue(pData:Number):void{  
            _structureValue=pData;  
        }  
  
        public function get propertyValue():Number{  
            return _propertyValue;  
        }  
  
        public function set propertyValue(pData:Number):void{  
            _propertyValue=pData;  
        }  
  
        public function get acquisitionDate():Date{  
            return _acquisitionDate;  
        }  
  
        public function set acquisitionDate(pData:Date):void{  
            _acquisitionDate=pData;  
        }  
  
        public function get registrationDate():Date{  
            return _registrationDate;  
        }  
  
        public function set registrationDate(pData:Date):void{  
            _registrationDate=pData;  
        }  
  
        public function get registrationName():String{  
            return _registrationName;  
        }  
  
        public function set registrationName(pData:String):void{  
            _registrationName=pData;  
        }  
  
        public function get registrationTome():String{  
            return _registrationTome;  
        }  
  
        public function set registrationTome(pData:String):void{  
            _registrationTome=pData;  
        }  
  
        public function get registrationNumber():String{  
            return _registrationNumber;  
        }  
  
        public function set registrationNumber(pData:String):void{  
            _registrationNumber=pData;  
        }  
  
        public function get registrationProtocol():String{  
            return _registrationProtocol;  
        }  
  
        public function set registrationProtocol(pData:String):void{  
            _registrationProtocol=pData;  
        }  
  
        public function get boundaryNorth():String{  
            return _boundaryNorth;  
        }  
  
        public function set boundaryNorth(pData:String):void{  
            _boundaryNorth=pData;  
        }  
  
        public function get boundarySouth():String{  
            return _boundarySouth;  
        }  
  
        public function set boundarySouth(pData:String):void{  
            _boundarySouth=pData;  
        }  
  
        public function get boundaryEast():String{  
            return _boundaryEast;  
        }  
  
        public function set boundaryEast(pData:String):void{  
            _boundaryEast=pData;  
        }  
  
        public function get boundaryWest():String{  
            return _boundaryWest;  
        }  
  
        public function set boundaryWest(pData:String):void{  
            _boundaryWest=pData;  
        }
        
		public function get singleAddress():Boolean {
			return _singleAddress;
		}
	
		public function set singleAddress(pData:Boolean):void {
			_singleAddress = pData;
		}          
   }  
} 