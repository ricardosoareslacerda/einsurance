package br.com.tratomais.einsurance.policy.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
    [RemoteClass(alias="br.com.tratomais.core.model.policy.ItemRiskType")]    
    [Managed]
	public class ItemRiskType extends HibernateBean
	{
		private var _id:ItemRiskTypeId;
		private var _riskTypeValue:Number;
		private var _riskTypeName:String;
		private var _item:Item;
		
		public function ItemRiskType(){}

		public function get id():ItemRiskTypeId
		{
			return this._id
		}
		
		public function set id( pData:ItemRiskTypeId ):void
		{
			this._id = pData;
		}
		
		public function get riskTypeValue():Number
		{
			return this._riskTypeValue;
		}
		
		public function set riskTypeValue( pData:Number ):void
		{
			this._riskTypeValue = pData
		}
		
		public function get riskTypeName():String
		{
			return this._riskTypeName;
		}
		
		public function set riskTypeName( pData:String ):void
		{
			this._riskTypeName = pData
		}
		
		public function get item():Item
		{
			return this._item
		}
		
		public function set item( pData:Item ):void
		{
			this._item = pData;
		}
	}
}