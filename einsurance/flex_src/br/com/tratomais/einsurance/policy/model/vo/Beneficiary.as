package br.com.tratomais.einsurance.policy.model.vo
{
	import flash.errors.IllegalOperationError;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.Beneficiary")]     
    [Managed]  
    public class Beneficiary extends HibernateBean 
    {  
        private var _id:BeneficiaryId;  
        private var _item:Item;  
        private var _beneficiaryType:int;  
        private var _name:String;  
        private var _lastName:String;  
        private var _firstName:String;  
        private var _kinshipType:int; 
        private var _personType:int;
        private var _gender:String;
        private var _birthDate:Date;  
        private var _maritalStatus:int;
		private var _undocumented:Boolean;  
        private var _documentType:String;  
        private var _documentNumber:String;  
        private var _documentIssuer:String;  
        private var _documentValidity:Date;  
        private var _participationPercentage:Number;  
        private var _displayOrder:int;  

        private var _kinshipTypeDescription:String;
        private var _personTypeDescription:String;
        private var _genderDescription:String;          
        private var _maritalStatusDescription:String;
        private var _transientDocumentDescription:String;

        public function Beneficiary()  
        {  
        }  
  
        public function get id():BeneficiaryId{  
            return _id;  
        }  
  
        public function set id(pData:BeneficiaryId):void{  
            _id=pData;  
        }  
  
        public function get item():Item{  
            return _item;  
        }  
  
        public function set item(pData:Item):void{  
            _item=pData;  
        }  
  
        public function get beneficiaryType():int{  
            return _beneficiaryType;  
        }  
  
        public function set beneficiaryType(pData:int):void{  
            _beneficiaryType=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get lastName():String{  
            return _lastName;  
        }  
  
        public function set lastName(pData:String):void{  
            _lastName=pData;  
        }  
  
        public function get firstName():String{  
            return _firstName;  
        }  
  
        public function set firstName(pData:String):void{  
            _firstName=pData;  
        }  
  
        public function get kinshipType():int{  
            return _kinshipType;  
        }  
  
        public function set kinshipType(pData:int):void{  
            _kinshipType=pData;  
        }  
        
        public function get personType():int{  
            return _personType;  
        }  
  
        public function set personType(pData:int):void{  
            _personType=pData;  
        }  
  
        public function get gender():String{  
            return _gender;  
        } 
  
        public function set gender(pData:String):void{  
            _gender=pData;  
        }  
  
        public function get birthDate():Date{  
            return _birthDate;  
        }  
  
        public function set birthDate(pData:Date):void{  
            this._birthDate = new Date(pData.valueOf() + pData.getTimezoneOffset()*60000);  
        }  

        public function get maritalStatus():int{  
            return _maritalStatus;  
        }  
  
        public function set maritalStatus(pData:int):void{  
            _maritalStatus=pData;  
        }
          
        public function get undocumented(): Boolean { 
        	return _undocumented;
        }
        
        public function set undocumented(pData: Boolean): void {
        	_undocumented = pData;
        }
                
        public function get documentType():String{  
            return _documentType;  
        }  
  
        public function set documentType(pData:String):void{  
            _documentType=pData;  
        }  

        public function get documentNumber():String{  
            return _documentNumber;  
        }  
  
        public function set documentNumber(pData:String):void{  
            _documentNumber=pData;  
        }  
  
        public function get documentIssuer():String{  
            return _documentIssuer;  
        }  
  
        public function set documentIssuer(pData:String):void{  
            _documentIssuer=pData;  
        }  
  
        public function get documentValidity():Date{  
            return _documentValidity;  
        }  
  
        public function set documentValidity(pData:Date):void{  
            _documentValidity=pData;  
        }  
  
        public function get participationPercentage():Number{  
            return _participationPercentage;  
        }  
  
        public function set participationPercentage(pData:Number):void{  
            _participationPercentage=pData;  
        }  
  
        public function get displayOrder():int{  
            return _displayOrder;  
        }  
  
        public function set displayOrder(pData:int):void{  
            _displayOrder=pData;  
        }  

		public function updateKey(parent:Item):void{
			if ( parent.id == null ) {
				throw new IllegalOperationError();
			}
			this.item = parent;
			if ( this.id == null ) {
				//this.setId( new BeneficiaryId(parent.getNextBeneficiaryId(),parent.getId().getItemId(),parent.getId().getEndorsementId(),parent.getId().getContractId()) );
				this.id =  new BeneficiaryId();
				this.id.beneficiaryId = parent.getNextBeneficiaryId();
				this.id.itemId = parent.id.itemId;
				this.id.endorsementId = parent.id.endorsementId;
				this.id.contractId = parent.id.contractId;
			}else{
				this.id.itemId = parent.id.itemId;
				this.id.endorsementId = parent.id.endorsementId;
				this.id.contractId = parent.id.contractId;
			}
			
		}
	
        public function get kinshipTypeDescription():String{  
            return _kinshipTypeDescription;  
        }  
  
        public function set kinshipTypeDescription(pData:String):void{  
            _kinshipTypeDescription=pData;  
        } 
          
        public function get personTypeDescription():String{  
            return _personTypeDescription;  
        }  
  
        public function set personTypeDescription(pData:String):void{  
            _personTypeDescription=pData;  
        }
        
        public function get genderDescription():String{  
            return _genderDescription;  
        } 
  
        public function set genderDescription(pData:String):void{  
            _genderDescription=pData;  
        }  
  
        public function get maritalStatusDescription():String{  
            return _maritalStatusDescription;  
        }  
  
        public function set maritalStatusDescription(pData:String):void{  
            _maritalStatusDescription=pData;  
        }        
  
        public function get transientDocumentDescription():String{  
            return _transientDocumentDescription;  
        }  
  
        public function set transientDocumentDescription(pData:String):void{  
            _transientDocumentDescription=pData;  
        } 
  

    }  
}