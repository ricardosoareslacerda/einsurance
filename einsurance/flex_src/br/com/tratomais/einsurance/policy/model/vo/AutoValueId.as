package br.com.tratomais.einsurance.policy.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
    [RemoteClass(alias="br.com.tratomais.core.model.auto.AutoValueId")]    
    [Managed] 	
	public class AutoValueId extends HibernateBean
	{
		private var _autoReferId:int;
		private var _yearRefer:int;
		private var _effectiveDate:Date;
		
		public function set autoReferId( pData:int ):void
		{
			this._autoReferId = pData;
		}
		
		public function get autoReferId():int
		{
			return _autoReferId;
		}
		
		public function set yearRefer( pData:int ):void
		{
			this._yearRefer = pData;
		}
		
		public function get yearRefer():int
		{
			return _yearRefer;
		}
		
		public function set effectiveDate( pData:Date ):void
		{
			this._effectiveDate = pData;
		}
		
		public function get effectiveDate():Date
		{
			return _effectiveDate;
		}
	}
}