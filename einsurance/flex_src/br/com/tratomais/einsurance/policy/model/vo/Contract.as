package br.com.tratomais.einsurance.policy.model.vo 
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.Contract")]    
    [Managed]  
    public class Contract extends HibernateBean  
    {  
        private var _contractId:int;
		private var _parentId:int;
        /* private var _parent:Contract; */
		private var _renewalId:int; 
        /* private var _renewal:Contract; */
        private var _insurerId:int;
        private var _subsidiaryId:int;
        private var _brokerId:int;
        private var _partnerId:int;
        private var _masterPolicyId:int;
        private var _policyType:String;
        private var _policyInformed:Boolean;
        private var _policyNumber:String;
        private var _certificateNumber:int;
        private var _externalKey:String;
        private var _productId:int;
        private var _currencyId:int;
        private var _termId:int;
		private var _renewed:Boolean;
        private var _referenceDate:Date;
        private var _effectiveDate:Date;
        private var _expiryDate:Date;
        private var _renewalLimitDate:Date;
        private var _cancelDate:Date;
        private var _contractStatus:int;
        private var _endorsements:ArrayCollection = new ArrayCollection();
        private var _itemSequences:ArrayCollection = new ArrayCollection();

		public static const CONTRACT_STATUS_ACTIVE:int = 101;
		public static const CONTRACT_STATUS_ABROGATED:int =102;

        public function Contract()  
        {  
        }  
  
        public function get contractId():int{  
            return _contractId;  
        }  
  
        public function set contractId(pData:int):void{  
            _contractId=pData;  
        }  
        
         public function get parentId():int{
            return _parentId;
        }  
  
        public function set parentId(pData:int):void{
            _parentId=pData;
        } 

/*         public function get parent():Contract{  
            return _parent;
        }  
  
        public function set parent(pData:Contract):void{  
            _parent=pData;  
        }   */

         public function get renewalId():int{
            return _renewalId;
        }  
  
        public function set renewalId(pData:int):void{
            _renewalId=pData;
        } 

/*         public function get renewal():Contract{  
            return _renewal;  
        }  
  
        public function set renewal(pData:Contract):void{  
            _renewal=pData;  
        } */

        public function get insurerId():int{  
            return _insurerId;  
        }  
  
        public function set insurerId(pData:int):void{  
            _insurerId=pData;  
        }  
  
        public function get subsidiaryId():int{  
            return _subsidiaryId;  
        }  
  
        public function set subsidiaryId(pData:int):void{  
            _subsidiaryId=pData;  
        }  
  
        public function get brokerId():int{  
            return _brokerId;  
        }  
  
        public function set brokerId(pData:int):void{  
            _brokerId=pData;  
        }  
  
        public function get partnerId():int{  
            return _partnerId;  
        }  
  
        public function set partnerId(pData:int):void{  
            _partnerId=pData;  
        }  
  
        public function get masterPolicyId():int{  
            return _masterPolicyId;  
        }  
  
        public function set masterPolicyId(pData:int):void{  
            _masterPolicyId=pData;  
        }  
  
        public function get policyType():String{  
            return _policyType;  
        }  
  
        public function set policyType(pData:String):void{  
            _policyType=pData;  
        }  
  
  		 public function get policyInformed():Boolean{  
            return _policyInformed;  
        }  
  
        public function set policyInformed(pData:Boolean):void{  
            _policyInformed=pData;  
        } 

       public function get policyNumber():String{  
            return _policyNumber;  
        }  
  
        public function set policyNumber(pData:String):void{  
            _policyNumber=pData;  
        }  
  
        public function get certificateNumber():int{  
            return _certificateNumber;  
        }  
  
        public function set certificateNumber(pData:int):void{  
            _certificateNumber=pData;  
        }  
  
         public function get externalKey():String{  
            return _externalKey;  
        }  
  
        public function set externalKey(pData:String):void{  
            _externalKey=pData;  
        }
  
        public function get productId():int{  
            return _productId;  
        }  
  
        public function set productId(pData:int):void{  
            _productId=pData;  
        }  
  
        public function get currencyId():int{  
            return _currencyId;  
        }  
  
        public function set currencyId(pData:int):void{  
            _currencyId=pData;  
        }  
  
        public function get termId():int{  
            return _termId;  
        }  
  
        public function set termId(pData:int):void{  
            _termId=pData;  
        }  
  
        public function get renewed(): Boolean { 
        	return _renewed;
        }
        
        public function set renewed(pDados: Boolean): void {
        	this._renewed = pDados;
        }  
  
        public function get referenceDate():Date{  
            return _referenceDate;  
        }  
  
        public function set referenceDate(pData:Date):void{  
            _referenceDate=pData;  
        }  
  
        public function get effectiveDate():Date{  
            return _effectiveDate;  
        }  
  
        public function set effectiveDate(pData:Date):void{  
            _effectiveDate=pData;  
        }  
  
        public function get expiryDate():Date{  
            return _expiryDate;  
        }  
  
        public function set expiryDate(pData:Date):void{  
            _expiryDate=pData;  
        }  
  
        public function get renewalLimitDate():Date{  
            return _renewalLimitDate;  
        }  
  
        public function set renewalLimitDate(pData:Date):void{  
            _renewalLimitDate=pData;  
        }
  
        public function get cancelDate():Date{  
            return _cancelDate;  
        }  
  
        public function set cancelDate(pData:Date):void{  
            _cancelDate=pData;  
        }  
  
        public function get contractStatus():int{  
            return _contractStatus;  
        }  
  
        public function set contractStatus(pData:int):void{  
            _contractStatus=pData;  
        }  
  
        public function get endorsements():ArrayCollection{  
            return _endorsements;  
        }  
  
        public function set endorsements(pData:ArrayCollection):void{  
            _endorsements=pData;  
        }  
  
        public function get itemSequences():ArrayCollection{  
            return _itemSequences;  
        }  
  
        public function set itemSequences(pData:ArrayCollection):void{  
            _itemSequences=pData;  
        }  
 
        public function addEndorsement () : Endorsement{
        	var newEndorsement : Endorsement = new Endorsement();
        	var newEndorsementId : EndorsementId = new EndorsementId();


			newEndorsementId.contractId = this.contractId;
			newEndorsementId.endorsementId = this.nextEndorsementId()


        	newEndorsement.contract = this;
        	this.endorsements.addItem(newEndorsement);
			return newEndorsement;        	
        }

		/**
		 * @return
		 */
		public function nextEndorsementId():int {
			var maxSequenceId:int = 0;
			
			for each( var tmpEndorsement:Endorsement in this.endorsements ) {
				if ( tmpEndorsement.id != null && tmpEndorsement.id.endorsementId > maxSequenceId ) {
					maxSequenceId=tmpEndorsement.id.endorsementId;
				}
			}
			
			maxSequenceId++;
			return maxSequenceId;
		}
    }
}