package br.com.tratomais.einsurance.policy.model.proxy
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.policy.business.MasterPolicyDelegateProxy;
	import br.com.tratomais.einsurance.policy.model.vo.MasterPolicy;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
		
	[Bindable]
	public class MasterPolicyProxy extends Proxy implements IProxy 
	{
		public static const NAME:String = "MasterPolicyProxy";
		
		[Embed(source="/assets/images/error.png")]
		public var errorMsg:Class;
				
		public function MasterPolicyProxy(data:Object = null)
		{
			super(NAME, data );
		}
		
		public function listAll():void
		{
			var delegate:MasterPolicyDelegateProxy = new MasterPolicyDelegateProxy(new Responder(onListAllResult, onFault));
			delegate.listAllWithDescriptions();
		}

		private function onListAllResult(pResultEvt:ResultEvent):void
		{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.MASTER_POLICY_LISTED, result);
		}
		
		public function saveObject(masterPolicy:MasterPolicy):void{
			var delegate:MasterPolicyDelegateProxy = new MasterPolicyDelegateProxy(new Responder(onSaveResult, onFault));
			delegate.saveObject( masterPolicy );		
		}		
		
		private function onSaveResult(pResultEvt:ResultEvent):void{			
            var masterPolicy:MasterPolicy =pResultEvt.result as MasterPolicy ;
            sendNotification(NotificationList.MASTER_POLICY_SAVE_SUCCESS);
		}		
		
		private function onFault(pFaultEvt:FaultEvent):void{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
		}		

	}
}