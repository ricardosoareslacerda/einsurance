package br.com.tratomais.einsurance.policy.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;

	[RemoteClass(alias="br.com.tratomais.core.model.policy.InvoiceHolderId")]
	[Managed]
	public class InvoiceHolderId extends HibernateBean
	{
		private var _contractId:int
		private var _endorsementId:int
		private var _policyHolderId:int

		public function InvoiceHolderId(contractId:int = 0, endorsementId:int = 0, policyHolderId:int = 0){
			super();
			
			_contractId = contractId;
			_endorsementId = endorsementId;
			_policyHolderId = policyHolderId;
		}

		public function get contractId():int{
			return _contractId;
		}

		public function set contractId(pData:int):void{
			_contractId = pData;
		}

		public function get endorsementId():int{
			return _endorsementId;
		}

		public function set endorsementId(pData:int):void{
			_endorsementId = pData;
		}

		public function get policyHolderId():int{
			return _policyHolderId;
		}

		public function set policyHolderId(pData:int):void{
			_policyHolderId = pData;
		}

		public function clone():InvoiceHolderId{
			return new InvoiceHolderId(this.contractId, this.endorsementId, this.policyHolderId);
		}

		public function toKey():String{
			return ('[' + 'contractId:' + this.contractId + ';' + 'endorsementId:' + this.endorsementId + ';' + 'policyHolderId:' + this.policyHolderId + ']');
		}
	}
}
