package br.com.tratomais.einsurance.policy.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
    [RemoteClass(alias="br.com.tratomais.core.model.auto.AutoVersionID")]    
    [Managed]	
	public class AutoVersionID extends HibernateBean
	{
		private var _autoModelID:int
		private var _effectiveDate:Date

		public function set autoModelID( pData:int ):void
		{
			this._autoModelID = pData;
		}
		
		public function get autoModelID():int
		{
			return _autoModelID;
		}
		
		public function set effectiveDate( pData:Date ):void
		{
			this._effectiveDate = pData;
		}
		
		public function get effectiveDate():Date
		{
			return _effectiveDate;
		}		

	}
}