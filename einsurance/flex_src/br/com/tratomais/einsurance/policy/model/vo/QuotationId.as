package br.com.tratomais.einsurance.policy.model.vo   
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.QuotationId")]     
    [Managed]  
    public class QuotationId extends HibernateBean 
    {  
        private var _endorsementId:int;  
        private var _contractId:int;  

        public function QuotationId()  
        {  
        }  
  
        public function get endorsementId():int{  
            return _endorsementId;  
        }  
  
        public function set endorsementId(pData:int):void{  
            _endorsementId=pData;  
        }  
  
        public function get contractId():int{  
            return _contractId;  
        }  
  
        public function set contractId(pData:int):void{  
            _contractId=pData;  
        }  
    }  
}