package br.com.tratomais.einsurance.policy.model.vo 
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.Quotation")]   
    [Managed]  
    public class Quotation extends HibernateBean 
    {  
        private var _id:QuotationId;  
        private var _endorsement:Endorsement;  
        private var _insuredName:String;  
        private var _phoneType:int;  
        private var _phoneNumber:String;  
        private var _emailAddress:String;  
/*         private var _serialization:Clob;  
 */  
        public function Quotation()  
        {  
        }  
  
        public function get id():QuotationId{  
            return _id;  
        }  
  
        public function set id(pData:QuotationId):void{  
            _id=pData;  
        }  
  
        public function get endorsement():Endorsement{  
            return _endorsement;  
        }  
  
        public function set endorsement(pData:Endorsement):void{  
            _endorsement=pData;  
        }  
  
        public function get insuredName():String{  
            return _insuredName;  
        }  
  
        public function set insuredName(pData:String):void{  
            _insuredName=pData;  
        }  
  
        public function get phoneType():int{  
            return _phoneType;  
        }  
  
        public function set phoneType(pData:int):void{  
            _phoneType=pData;  
        }  
  
        public function get phoneNumber():String{  
            return _phoneNumber;  
        }  
  
        public function set phoneNumber(pData:String):void{  
            _phoneNumber=pData;  
        }  
  
        public function get emailAddress():String{  
            return _emailAddress;  
        }  
  
        public function set emailAddress(pData:String):void{  
            _emailAddress=pData;  
        }  
  
/*         public function get serialization():Clob{  
            return _serialization;  
        }  
  
        public function set serialization(pData:Clob):void{  
            _serialization=pData;  
        }  
*/ 
	}  
}