package br.com.tratomais.einsurance.policy.model.vo   
{
	import flash.errors.IllegalOperationError;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.ItemPersonalRiskGroup")]   
    [Managed]  
    public class ItemPersonalRiskGroup extends HibernateBean 
    {  
        private var _id:ItemPersonalRiskGroupId;  
        private var _itemPersonalRisk:ItemPersonalRisk;  
        private var _personalRiskType:int;  
        private var _insuredName:String;  
        private var _lastName:String;  
        private var _firstName:String;  
        private var _kinshipType:int;  
        private var _gender:int;  
        private var _birthDate:Date;  
        private var _hireDate:Date;  
        private var _maritalStatus:int;  
        private var _childrenNumber:int;
        private var _undocumented:Boolean;
        private var _documentType:int;  
        private var _documentNumber:String;  
        private var _documentIssuer:String;  
        private var _documentValidity:Date;  
        private var _effectiveDate:Date;  
        private var _expiryDate:Date;  
		private var _claimNumber:String;
		private var _claimDate:Date;
		private var _claimNotification:Date;	        

        private var _transientDocumentDescription:String;  
        private var _transientKinshipDescription:String;  
 		private var _transientPersonRiskTypeDescription:String;  
 
        public function ItemPersonalRiskGroup()  
        {  
        }  
  
        public function get id():ItemPersonalRiskGroupId{  
            return _id;  
        }  
  
        public function set id(pData:ItemPersonalRiskGroupId):void{  
            _id=pData;  
        }  
  
        public function get itemPersonalRisk():ItemPersonalRisk{  
            return _itemPersonalRisk;  
        }  
  
        public function set itemPersonalRisk(pData:ItemPersonalRisk):void{  
            _itemPersonalRisk=pData;  
        }  
  
        public function get personalRiskType():int{  
            return _personalRiskType;  
        }  
  
        public function set personalRiskType(pData:int):void{  
            _personalRiskType=pData;  
        }  
  
        public function get insuredName():String{  
            return _insuredName;  
        }  
  
        public function set insuredName(pData:String):void{  
            _insuredName=pData;  
        }  
  
        public function get lastName():String{  
            return _lastName;  
        }  
  
        public function set lastName(pData:String):void{  
            _lastName=pData;  
        }  
  
        public function get firstName():String{  
            return _firstName;  
        }  
  
        public function set firstName(pData:String):void{  
            _firstName=pData;  
        }  
  
        public function get kinshipType():int{  
            return _kinshipType;  
        }
  
        public function set kinshipType(pData:int):void{  
            _kinshipType=pData;  
        }
  
        public function get gender():int{  
            return _gender;  
        }
  
        public function set gender(pData:int):void{  
            _gender=pData;  
        }
  
        public function get birthDate():Date{  
            return _birthDate;  
        }
  
        public function set birthDate(pData:Date):void{  
            this._birthDate = new Date(pData.valueOf() + pData.getTimezoneOffset()*60000);  
        }
        
        public function get hireDate():Date{  
            return _hireDate;  
        }  
  
        public function set hireDate(pData:Date):void{  
            _hireDate=pData;  
        }  
  
        public function get maritalStatus():int{  
            return _maritalStatus;  
        }  
  
        public function set maritalStatus(pData:int):void{  
            _maritalStatus=pData;  
        }  
  
        public function get childrenNumber():int{  
            return _childrenNumber;  
        }  
  
        public function set childrenNumber(pData:int):void{  
            _childrenNumber=pData;  
        }  
          
        public function get undocumented(): Boolean { 
        	return _undocumented;
        }
        
        public function set undocumented(pData: Boolean): void {
        	_undocumented = pData;
        }
        
        public function get documentType():int{  
            return _documentType;  
        }  
  
        public function set documentType(pData:int):void{  
            _documentType=pData;  
        }  
  
        public function get documentNumber():String{  
            return _documentNumber;  
        }  
  
        public function set documentNumber(pData:String):void{  
            _documentNumber=pData;  
        }  
  
        public function get documentIssuer():String{  
            return _documentIssuer;  
        }  
  
        public function set documentIssuer(pData:String):void{  
            _documentIssuer=pData;  
        }  
  
        public function get documentValidity():Date{  
            return _documentValidity;  
        }  
  
        public function set documentValidity(pData:Date):void{  
            _documentValidity=pData;  
        }  
  
        public function get effectiveDate():Date{  
            return _effectiveDate;  
        }  
  
        public function set effectiveDate(pData:Date):void{  
            _effectiveDate=pData;  
        }  
  
        public function get expiryDate():Date{  
            return _expiryDate;  
        }  
  
        public function set expiryDate(pData:Date):void{  
            _expiryDate=pData;  
        }  

		public function get claimNumber():String{
			return _claimNumber
		}
		
		public function set claimNumber(pData:String):void{
			_claimNumber = pData;
		}		

		public function get claimDate():Date{
			return _claimDate
		}
		
		public function set claimDate(pData:Date):void{
			_claimDate = pData;
		}		

		public function get claimNotification():Date{
			return _claimNotification
		}
		
		public function set claimNotification(pData:Date):void{
			_claimNotification = pData;
		}
		        
        public function get transientDocumentDescription():String{  
            return _transientDocumentDescription;  
        }  
  
        public function set transientDocumentDescription(pData:String):void{  
            _transientDocumentDescription=pData;  
        }  
        
         public function get transientKinshipDescription():String{  
            return _transientKinshipDescription;  
        }  
  
        public function set transientKinshipDescription(pData:String):void{  
            _transientKinshipDescription=pData;  
        } 

 		public function get transientPersonRiskTypeDescription():String{  
            return _transientPersonRiskTypeDescription;  
        }  
  
        public function set transientPersonRiskTypeDescription(pData:String):void{  
            _transientPersonRiskTypeDescription=pData;  
        }
        
		public function updateKey(parent:ItemPersonalRisk):void{
			if ( parent.id == null ) {
				throw new IllegalOperationError();
			}
			this.itemPersonalRisk = parent ;
			if ( this.id == null ) {
				this.id =  new ItemPersonalRiskGroupId();
				this.id.riskGroupId = parent.getNextItemPersonalRiskGroupId();
				this.id.itemId = parent.id.itemId;
				this.id.endorsementId = parent.id.endorsementId;
				this.id.contractId = parent.id.contractId;
			}else{
				this.id.itemId = parent.id.itemId;
				this.id.endorsementId = parent.id.endorsementId;
				this.id.contractId = parent.id.contractId;
			}
	    }
    }
}