package br.com.tratomais.einsurance.policy.model.vo 
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.ItemClauseId")]    
    [Managed]  
    public class ItemClauseId extends HibernateBean 
    {  
        private var _clauseId:int;  
        private var _itemId:int;  
        private var _endorsementId:int;  
        private var _contractId:int;  
  
        public function ItemClauseId()  
        {  
        }  
  
        public function get clauseId():int{  
            return _clauseId;  
        }  
  
        public function set clauseId(pData:int):void{  
            _clauseId=pData;  
        }  
  
        public function get itemId():int{  
            return _itemId;  
        }  
  
        public function set itemId(pData:int):void{  
            _itemId=pData;  
        }  
  
        public function get endorsementId():int{  
            return _endorsementId;  
        }  
  
        public function set endorsementId(pData:int):void{  
            _endorsementId=pData;  
        }  
  
        public function get contractId():int{  
            return _contractId;  
        }  
  
        public function set contractId(pData:int):void{  
            _contractId=pData;  
        }  
	}  
}