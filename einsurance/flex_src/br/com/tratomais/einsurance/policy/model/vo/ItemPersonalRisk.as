package br.com.tratomais.einsurance.policy.model.vo  
{
	import flash.errors.IllegalOperationError;
	
	import mx.collections.ArrayCollection;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.ItemPersonalRisk")]    
    [Managed]  
    public class ItemPersonalRisk extends Item 
    {  
//        private var _item:Item;  
        private var _insuredName:String;  
        private var _lastName:String;  
        private var _firstName:String;  
        private var _gender:int;
        private var _birthDate:Date;  
        private var _hireDate:Date;  
        private var _smoker:Boolean;  
        private var _ageActuarial:int;  
        private var _maritalStatus:int;  
        private var _childrenNumber:int;
        private var _undocumented:Boolean;
        private var _documentType:int;  
        private var _documentNumber:String;  
        private var _documentIssuer:String;  
        private var _documentValidity:Date;  
        private var _professionId:String;  
        private var _inflow:Number;  
        private var _occupationId:String;  
        private var _occupationRiskType:int;  
        private var _personalRiskValue:Number;
        private var _creditEffectiveDate:Date;
        private var _creditExpiryDate:Date;
	    private var _personalRiskType:int;
	    private var _kinshipType:int;
		private var _restrictionRiskType:int;
        private var _itemPersonalRiskGroups:ArrayCollection = new ArrayCollection();
  	
		private var _transientQuantityAdditional:int;
  		private var _transientQuantityNewborn:int;
        private var _transientDocumentDescription:String;  
        private var _transientKinshipDescription:String;  
        private var _transientPersonRiskTypeDescription:String;
        private var _transientIsLoaded:Boolean
        private var _qtdAdditional:int;
        private var _firstInvoice:Boolean;
        private var _qtdNewborn:int;
  		  		
		public static const PERSONAL_RISK_TYPE_TITULAR:int = 293;
		public static const PERSONAL_RISK_TYPE_FAMILY_GROUP:int = 194;
		public static const PERSONAL_RISK_TYPE_ADDITIONAL:int = 195;
		public static const PERSONAL_RISK_TYPE_NEWBORN:int = 333;
	

        public function ItemPersonalRisk()  
        {  
        }  
  
//        public function get item():Item{  
//            return _item;  
//        }  
//        
//        public function set item(pData:Item):void{  
//            _item=pData;  
//        }  
  
        public function get insuredName():String{  
            return _insuredName;  
        }  
  
        public function set insuredName(pData:String):void{  
            _insuredName=pData;  
        }  
  
        public function get lastName():String{  
            return _lastName;  
        }  
  
        public function set lastName(pData:String):void{  
            _lastName=pData;  
        }  
  
        public function get firstName():String{  
            return _firstName;  
        }  
  
        public function set firstName(pData:String):void{  
            _firstName=pData;  
        }  
  
        public function get gender():int{  
            return _gender;  
        }  
  
        public function set gender(pData:int):void{  
            _gender=pData;  
        }  
  
        public function get birthDate():Date{  
            return _birthDate;
        }
  
        public function set birthDate(pData:Date):void{
        	if( pData )
            	this._birthDate = new Date(pData.valueOf() + pData.getTimezoneOffset()*60000);
        }
  
        public function get hireDate():Date{  
            return _hireDate;  
        }  
  
        public function set hireDate(pData:Date):void{  
            _hireDate=pData;  
        }  
  
        public function get smoker():Boolean{  
            return _smoker;  
        }  
  
        public function set smoker(pData:Boolean):void{  
            _smoker=pData;  
        }  
  
        public function get ageActuarial():int{  
            return _ageActuarial;  
        }  
  
        public function set ageActuarial(pData:int):void{  
            _ageActuarial=pData;  
        }  
  
        public function get maritalStatus():int{  
            return _maritalStatus;  
        }  
  
        public function set maritalStatus(pData:int):void{  
            _maritalStatus=pData;  
        }  
  
        public function get childrenNumber():int{  
            return _childrenNumber;  
        }  
  
        public function set childrenNumber(pData:int):void{  
            _childrenNumber=pData;  
        }
        
        public function get undocumented(): Boolean { 
        	return _undocumented;
        }
        
        public function set undocumented(pData: Boolean): void {
        	_undocumented = pData;
        }
        
        public function get documentType():int{  
            return _documentType;  
        }  
  
        public function set documentType(pData:int):void{  
            _documentType=pData;  
        }  
  
        public function get documentNumber():String{  
            return _documentNumber;  
        }  
  
        public function set documentNumber(pData:String):void{  
            _documentNumber=pData;  
        }  
  
        public function get documentIssuer():String{  
            return _documentIssuer;  
        }  
  
        public function set documentIssuer(pData:String):void{  
            _documentIssuer=pData;  
        }  
  
        public function get documentValidity():Date{  
            return _documentValidity;  
        }  
  
        public function set documentValidity(pData:Date):void{  
            _documentValidity=pData;  
        }  
  
        public function get professionId():String{  
            return _professionId;  
        }  
  
        public function set professionId(pData:String):void{  
            _professionId=pData;  
        }  
  
        public function get inflow():Number{  
            return _inflow;  
        }  
  
        public function set inflow(pData:Number):void{  
            _inflow=pData;  
        }  
  
        public function get occupationId():String{  
            return _occupationId;  
        }  
  
        public function set occupationId(pData:String):void{  
            _occupationId=pData;  
        }  
  
        public function get occupationRiskType():int{  
            return _occupationRiskType;  
        }  
  
        public function set occupationRiskType(pData:int):void{  
            _occupationRiskType=pData;  
        }  
  
         public function get personalRiskValue():Number{  
            return _personalRiskValue;  
        }  
  
        public function set personalRiskValue(pData:Number):void{  
            _personalRiskValue=pData;  
        }  
        
        public function get creditEffectiveDate():Date{  
            return _creditEffectiveDate;  
        } 

        public function set creditEffectiveDate(pData:Date):void{  
            _creditEffectiveDate=pData;  
        }  
  
        public function get creditExpiryDate():Date{  
            return _creditExpiryDate;  
        } 
        
        public function set creditExpiryDate(pData:Date):void{  
            _creditExpiryDate=pData;  
        }  
  
	    public function get personalRiskType():int {
		return _personalRiskType;
		}

		public function set personalRiskType(pData:int):void {
			_personalRiskType = pData;
		}
	
		public function get kinshipType():int {
			return _kinshipType;
		}
	
		public function set kinshipType(pData:int):void {
			_kinshipType = pData;
		}
	
		public function get restrictionRiskType():int {
			return _restrictionRiskType;
		}
	
		public function set restrictionRiskType(pData:int):void {
			_restrictionRiskType = pData;
		}

        public function get itemPersonalRiskGroups():ArrayCollection{  
            return _itemPersonalRiskGroups;  
        }  
  
        public function set itemPersonalRiskGroups(pData:ArrayCollection):void{  
            _itemPersonalRiskGroups=pData;  
        }  
        
        public function get transientQuantityAdditional():int{  
            return _transientQuantityAdditional;  
        }  

         public function set transientQuantityAdditional(pData:int):void{  
            _transientQuantityAdditional=pData;  
        }  

        public function get transientQuantityNewborn():int{  
            return _transientQuantityNewborn;  
        }  
        
   		public function set transientQuantityNewborn(pData:int):void{  
            _transientQuantityNewborn=pData;  
        }  
  
        public function get transientDocumentDescription():String{  
            return _transientDocumentDescription;  
        }  
  
        public function set transientDocumentDescription(pData:String):void{  
            _transientDocumentDescription=pData;  
        }  
        
         public function get transientKinshipDescription():String{  
            return _transientKinshipDescription;  
        }  
  
        public function set transientKinshipDescription(pData:String):void{  
            _transientKinshipDescription=pData;  
        } 
        
        public function get transientPersonRiskTypeDescription():String{  
            return _transientPersonRiskTypeDescription;  
        }  
  
        public function set transientPersonRiskTypeDescription(pData:String):void{  
            _transientPersonRiskTypeDescription=pData;  
        }
        
        public function get transientIsLoaded():Boolean
        {
        	return _transientIsLoaded;
        }
        
        public function set transientIsLoaded( pData:Boolean ):void
        {
        	_transientIsLoaded = pData;
        }
        
        public function get qtdAdditional():int{  
            return _qtdAdditional;  
        }  
        
   		public function set qtdAdditional(pData:int):void{  
            _qtdAdditional=pData;  
        }
        
        public function get qtdNewborn():int{  
            return _qtdNewborn;  
        }  
        
   		public function set qtdNewborn(pData:int):void{  
            _qtdNewborn=pData;  
        }  
        
		public function get firstInvoice():Boolean{
			return _firstInvoice;
		}

		public function set firstInvoice(pData:Boolean):void{
			_firstInvoice = pData;
		}
		
		override public function updateKey(parent:Endorsement):void{
			if ( parent.id == null ) {
				throw new IllegalOperationError();
			}
			
			super.updateKey( parent );
			for each (var workItemPersonalRiskGroup : ItemPersonalRiskGroup in  this.itemPersonalRiskGroups){
				workItemPersonalRiskGroup.updateKey( this );
			}				
		}
	
		public function addItemPersonalRiskGroup():ItemPersonalRiskGroup{
			var newItemPersonalRiskGroup:ItemPersonalRiskGroup = new ItemPersonalRiskGroup();
			var newItemPersonalRiskGroupId:ItemPersonalRiskGroupId = new ItemPersonalRiskGroupId();
	
			if ( this.id != null  ) {
				newItemPersonalRiskGroupId.itemId = this.id.itemId;
				newItemPersonalRiskGroupId.endorsementId = this.id.endorsementId;
				newItemPersonalRiskGroupId.contractId = this.id.contractId;
			}
			newItemPersonalRiskGroupId.riskGroupId = getNextItemPersonalRiskGroupId();
			newItemPersonalRiskGroup.id = newItemPersonalRiskGroupId;
			
			newItemPersonalRiskGroup.itemPersonalRisk = this ;
			this.itemPersonalRiskGroups.addItem( newItemPersonalRiskGroup );
			
			return newItemPersonalRiskGroup;
		}
		
		public function attachItemPersonalRiskGroup(newItemPersonalRiskGroup:ItemPersonalRiskGroup):ItemPersonalRiskGroup{
			this.itemPersonalRiskGroups.addItem( newItemPersonalRiskGroup );
			newItemPersonalRiskGroup.updateKey( this );
			return newItemPersonalRiskGroup;
		}
	
		/**
		 * @return
		 */
		public function getNextItemPersonalRiskGroupId():int {
			var maxSequenceId:int = 0;
			
			for each ( var tmpItemPersonalRiskGroup:ItemPersonalRiskGroup in this.itemPersonalRiskGroups ) {
				if ( tmpItemPersonalRiskGroup.id != null && tmpItemPersonalRiskGroup.id.riskGroupId > maxSequenceId ) {
					maxSequenceId=tmpItemPersonalRiskGroup.id.riskGroupId;
				}
			}
			
			maxSequenceId++;
			return maxSequenceId;
		}
    }  
}