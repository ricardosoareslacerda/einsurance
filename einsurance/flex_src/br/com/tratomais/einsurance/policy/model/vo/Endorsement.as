package  br.com.tratomais.einsurance.policy.model.vo   
{
	import br.com.tratomais.einsurance.core.ErrorList;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.customer.model.vo.Customer;
	import br.com.tratomais.einsurance.sales.model.vo.Term;
	
	import flash.errors.IllegalOperationError;
	
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.Endorsement")]     
    [Managed]  
    public class Endorsement extends HibernateBean  
    {  
		/**
		 * Issuing Type
		 */
		public static const ISSUING_STATUS_QUOTATION:int = 104;		// Cota��o
		public static const ISSUING_STATUS_PROPOSAL:int = 105;		// Proposta
		public static const ISSUING_STATUS_EMISSION:int = 106;		// Emitida
		public static const ISSUING_STATUS_REFUSED:int = 107;		// Recusada
		public static const ISSUING_STATUS_SCHEDULING:int = 108;	// Agendamento
		public static const ISSUANCE_TYPE_INVOICE:int = 775;					// Faturamento
        private var _id:EndorsementId;  
        private var _contract:Contract;  
        private var _customerByInsuredId:Customer;  
        private var _customerByPolicyHolderId:Customer;  
		private var _endorsedId:int;
        private var _issuanceDate:Date;  
        private var _issuanceType:int;  
        private var _userId:int;  
        private var _channelId:int;  
		private var _insured:Customer;
        private var _riskPlanId:int;  
        private var _termId:int;  
        private var _hierarchyType:int;  
        private var _authorizationType:int;  
        private var _paymentTermId:int;  
        private var _referenceDate:Date;  
        private var _effectiveDate:Date;  
        private var _expiryDate:Date;  
        private var _refusalDate:Date;  
        private var _cancelDate:Date;  
        private var _quotationNumber:int;  
        private var _quotationDate:Date;  
        private var _proposalNumber:int;  
        private var _proposalDate:Date;  
        private var _endorsedNumber:int;  
        private var _endorsementType:int;  
        private var _endorsementNumber:int;  
        private var _endorsementDate:Date;  
        private var _purePremium:Number;  
        private var _tariffPremium:Number;  
        private var _commercialPremium:Number;  
        private var _profitPremium:Number;
        private var _technicalPremium:Number;
        private var _retainedPremium:Number;  
        private var _earnedPremium:Number;
        private var _unearnedPremium:Number;
        private var _netPremium:Number;  
        private var _commission:Number;  
        private var _commissionFactor:Number;  
        private var _labour:Number;
        private var _labourFactor:Number;
        private var _agency:Number;
        private var _agencyFactor:Number;
        private var _policyCost:Number;  
        private var _inspectionCost:Number;  
        private var _fractioningAdditional:Number;  
        private var _taxExemptionType:int;  
        private var _taxRate:Number;  
        private var _taxValue:Number;  
        private var _totalPremium:Number;
		private var _currencyDate:Date;
        private var _conversionRate:Number;
        private var _transmitted:Boolean;
        private var _issuingStatus:int;  
		private var _changeLocked:Boolean;
		private var _cancelReason:int;
		private var _claimNumber:String;
		private var _claimDate:Date;
		private var _claimNotification:Date;		
        private var _items:ArrayCollection = new ArrayCollection(); ;  
//        private var _quotation:Quotation;  
        private var _installments:ArrayCollection = new ArrayCollection();  
		private var _term:Term;
        private var _errorList:ErrorList;
        private var _renewalAlerts:ArrayCollection = new ArrayCollection();
        
	  	public static const ISSUING_STATUS_COTACAO=104;
		public static const ISSUING_STATUS_PROPOSTA=105;
		public static const ISSUING_STATUS_EMITIDA=106;
		public static const ISSUING_STATUS_ANULADA=107;
		public static const ISSUANCE_TYPE_EMISSAO=6;
		public static const ISSUANCE_TYPE_REATIVACAO=15;
		public static const ISSUANCE_TYPE_POLICY_CANCELLATION_INSURED=8;
		public static const ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR=7;		
		public static const ISSUANCE_TYPE_ALTERACAO_REGISTRAL=21;
		public static const ISSUANCE_TYPE_ALTERACAO_TECNICA=22;
		public static const ISSUANCE_TYPE_POLICY_CANCELLATION_PAYMENT=28;
		public static const ENDORSEMENT_TYPE_COLLECTION=30;
		public static const ENDORSEMENT_TYPE_RESTITUTION=33;
		
        public function Endorsement()  
        {  
        }  
  
        public function get id():EndorsementId{  
            return _id;  
        }  
  
        public function set id(pData:EndorsementId):void{  
            _id=pData;  
        }  
  
        public function get contract():Contract{  
            return _contract;  
        }  
  
        public function set contract(pData:Contract):void{  
            _contract=pData;  
        }  
  
        public function get customerByInsuredId():Customer{  
            return _customerByInsuredId;  
        }  
  
        public function set customerByInsuredId(pData:Customer):void{  
            _customerByInsuredId=pData;  
        }  
  
        public function get customerByPolicyHolderId():Customer{  
            return _customerByPolicyHolderId;  
        }  
  
        public function set customerByPolicyHolderId(pData:Customer):void{  
            _customerByPolicyHolderId=pData;  
        }  
	
		public function get endorsedId():int{
			return _endorsedId;
		}
		
		public function set endorsedId(pData:int):void{
			_endorsedId = pData;
		}
		  
        public function get issuanceDate():Date{  
            return _issuanceDate;  
        }  
  
        public function set issuanceDate(pData:Date):void{  
			_issuanceDate = pData;
        }  
  
        public function get issuanceType():int{  
            return _issuanceType;  
        }  
  
        public function set issuanceType(pData:int):void{  
            _issuanceType=pData;  
        }  
  
        public function get userId():int{  
            return _userId;  
        }  
  
        public function set userId(pData:int):void{  
            _userId=pData;  
        }  
  
        public function get channelId():int{  
            return _channelId;  
        }  
  
        public function set channelId(pData:int):void{  
            _channelId=pData;  
        }  
  

		public function get insured():Customer{
			return _insured;
		}

		public function set insured(pData:Customer):void{
			_insured = pData;
		}
        public function get riskPlanId():int{  
            return _riskPlanId;  
        }  
  
        public function set riskPlanId(pData:int):void{  
            _riskPlanId=pData;  
        }  
  
        public function get termId():int{  
            return _termId;  
        }  
  
        public function set termId(pData:int):void{  
            _termId=pData;  
        }  
  
        public function get hierarchyType():int{  
            return _hierarchyType;  
        }  
  
        public function set hierarchyType(pData:int):void{  
            _hierarchyType=pData;  
        }  
  
        public function get authorizationType():int{  
            return _authorizationType;  
        }  
  
        public function set authorizationType(pData:int):void{  
            _authorizationType=pData;  
        }  
  
        public function get paymentTermId():int{  
            return _paymentTermId;  
        }  
  
        public function set paymentTermId(pData:int):void{  
            _paymentTermId=pData;  
        }  
  
        public function get referenceDate():Date{  
            return _referenceDate;  
        }  
  
        public function set referenceDate(pData:Date):void{  
			_referenceDate = pData; 
        }  
  
        public function get effectiveDate():Date{  
            return _effectiveDate;  
        }  
  
        public function set effectiveDate(pData:Date):void{  
		_effectiveDate = pData;
        }  
  
        public function get expiryDate():Date{  
            return _expiryDate;  
        }  
  
        public function set expiryDate(pData:Date):void{  
			_expiryDate = pData;
        }  
  
        public function get refusalDate():Date{  
            return _refusalDate;  
        }  
  
        public function set refusalDate(pData:Date):void{  
			_refusalDate = pData;
        }  
  
        public function get cancelDate():Date{  
            return _cancelDate;  
        }  
  
        public function set cancelDate(pData:Date):void{  
			_cancelDate = pData;
        }  
  
        public function get quotationNumber():int{  
            return _quotationNumber;  
        }  
  
        public function set quotationNumber(pData:int):void{  
            _quotationNumber=pData;  
        }  
  
        public function get quotationDate():Date{  
            return _quotationDate;  
        }  
  
        public function set quotationDate(pData:Date):void{  
			_quotationDate = pData;
        }  
  
        public function get proposalNumber():int{  
            return _proposalNumber;  
        }  
  
        public function set proposalNumber(pData:int):void{  
            _proposalNumber=pData;  
        }  
  
        public function get proposalDate():Date{  
            return _proposalDate;  
        }  
  
        public function set proposalDate(pData:Date):void{  
			_proposalDate = pData;
        }  
  
        public function get endorsedNumber():int{  
            return _endorsedNumber;  
        }  
  
        public function set endorsedNumber(pData:int):void{  
            _endorsedNumber=pData;  
        }  
  
        public function get endorsementType():int{  
            return _endorsementType;  
        }  
  
        public function set endorsementType(pData:int):void{  
            _endorsementType=pData;  
        }  
  
        public function get endorsementNumber():int{  
            return _endorsementNumber;  
        }  
  
        public function set endorsementNumber(pData:int):void{  
            _endorsementNumber=pData;  
        }  
  
        public function get endorsementDate():Date{  
            return _endorsementDate;  
        }  
  
        public function set endorsementDate(pData:Date):void{  
			_endorsementDate = pData;
        }  
  
        public function get purePremium():Number{  
            return _purePremium;  
        }  
  
        public function set purePremium(pData:Number):void{  
            _purePremium=pData;  
        }  
  
        public function get tariffPremium():Number{  
            return _tariffPremium;  
        }  
  
        public function set tariffPremium(pData:Number):void{  
            _tariffPremium=pData;  
        }  
  
        public function get commercialPremium():Number{  
            return _commercialPremium;  
        }  
  
        public function set commercialPremium(pData:Number):void{  
            _commercialPremium=pData;  
        }  
  
        public function get profitPremium():Number{  
            return _profitPremium;  
        }  
  
        public function set profitPremium(pData:Number):void{  
            _profitPremium=pData;  
        }  
  
		public function get technicalPremium():Number{  
            return _technicalPremium;  
        }  
  
        public function set technicalPremium(pData:Number):void{  
            _technicalPremium=pData;  
        }  
  
        public function get retainedPremium():Number{  
            return _retainedPremium;  
        }  
  
        public function set retainedPremium(pData:Number):void{  
            _retainedPremium=pData;  
        }
        
		public function get earnedPremium():Number{  
            return _earnedPremium;  
        }  
  
        public function set earnedPremium(pData:Number):void{  
            _earnedPremium=pData;  
        }  
  
        public function get unearnedPremium():Number{  
            return _unearnedPremium;  
        }  
  
        public function set unearnedPremium(pData:Number):void{  
            _unearnedPremium=pData;  
        }  
  
        public function get netPremium():Number{  
            return _netPremium;  
        }  
  
        public function set netPremium(pData:Number):void{  
            _netPremium=pData;  
        }  
  
        public function get commission():Number{  
            return _commission;  
        }  
  
        public function set commission(pData:Number):void{  
            _commission=pData;  
        }  
  
        public function get commissionFactor():Number{  
            return _commissionFactor;  
        }  
  
        public function set commissionFactor(pData:Number):void{  
            _commissionFactor=pData;  
        }  
  
		public function get labour():Number {
			return _labour;
		}
	
		public function set labour(pData:Number) {
			_labour = pData;
		}
	
		public function get labourFactor():Number {
			return _labourFactor;
		}
	
		public function set labourFactor(pData:Number) {
			_labourFactor = pData;
		}
	
		public function get agency():Number {
			return _agency;
		}
	
		public function set agency(pData:Number) {
			_agency = pData;
		}
	
		public function get agencyFactor():Number {
			return _agencyFactor;
		}
	
		public function set agencyFactor(pData:Number) {
			_agencyFactor = pData;
		}
	
        public function get policyCost():Number{  
            return _policyCost;  
        }  
  
        public function set policyCost(pData:Number):void{  
            _policyCost=pData;  
        }  
  
        public function get inspectionCost():Number{  
            return _inspectionCost;  
        }  
  
        public function set inspectionCost(pData:Number):void{  
            _inspectionCost=pData;  
        }  
  
        public function get fractioningAdditional():Number{  
            return _fractioningAdditional;  
        }  
  
        public function set fractioningAdditional(pData:Number):void{  
            _fractioningAdditional=pData;  
        }  
  
        public function get taxExemptionType():int{  
            return _taxExemptionType;  
        }  
  
        public function set taxExemptionType(pData:int):void{  
            _taxExemptionType=pData;  
        }  
  
        public function get taxRate():Number{  
            return _taxRate;  
        }  
  
        public function set taxRate(pData:Number):void{  
            _taxRate=pData;  
        }  
  
        public function get taxValue():Number{  
            return _taxValue;  
        }  
  
        public function set taxValue(pData:Number):void{  
            _taxValue=pData;  
        }  
  
        public function get totalPremium():Number{  
            return _totalPremium;  
        }  
  
        public function set totalPremium(pData:Number):void{  
            _totalPremium=pData;  
        }  

		public function get currencyDate():Date{
			return _currencyDate
		}
		
		public function set currencyDate(pData:Date):void{
			_currencyDate = pData;
		}

        public function get conversionRate():Number{  
            return _conversionRate;  
        }  
  
        public function set conversionRate(pData:Number):void{  
            _conversionRate=pData;  
        }

        public function get transmitted(): Boolean { 
        	return _transmitted;
        }
        
        public function set transmitted(pTransmitted: Boolean): void {
        	this._transmitted = pTransmitted;
        }
        
        public function get issuingStatus():int{  
            return _issuingStatus;  
        }  
  
        public function set issuingStatus(pData:int):void{  
            _issuingStatus=pData;  
        }  
  
		public function get changeLocked():Boolean {
			return _changeLocked;
		}
	
		public function set changeLocked(pData:Boolean):void {
			_changeLocked = pData;
		}

		public function get cancelReason():int {
			return _cancelReason;
		}
	
		public function set cancelReason(pData:int):void {
			_cancelReason = pData;
		}

		public function get claimNumber():String{
			return _claimNumber
		}
		
		public function set claimNumber(pData:String):void{
			_claimNumber = pData;
		}		

		public function get claimDate():Date{
			return _claimDate
		}
		
		public function set claimDate(pData:Date):void{
			_claimDate = pData;
		}		

		public function get claimNotification():Date{
			return _claimNotification
		}
		
		public function set claimNotification(pData:Date):void{
			_claimNotification = pData;
		}		

		public function get term():Term{
			return _term;
		}

		public function set term(pData:Term):void{
			_term = pData;
		}
        public function get items():ArrayCollection{  
            return _items;  
        }  
  
        public function set items(pData:ArrayCollection):void{  
            _items=pData;  
        }  
  
//        public function get quotation():Quotation{  
//            return _quotation;  
//        }  
//  
//        public function set quotation(pData:Quotation):void{  
//            _quotation=pData;  
//        }  
  
        public function get installments():ArrayCollection{  
            return _installments;  
        }  
  
        public function set installments(pData:ArrayCollection):void{  
            _installments=pData;  
        }  
        
        public function get errorList():ErrorList{  
            return _errorList;  
        }  
  
        public function set errorList(pData:ErrorList):void{  
            _errorList=pData;  
        }
        
        public function get renewalAlerts():ArrayCollection{  
            return _renewalAlerts;  
        }  
  
        public function set renewalAlerts(pData:ArrayCollection):void{  
            _renewalAlerts=pData;  
        } 
        
   		public function addItem(objectId : int) : Item{
			var newItem:Item = this.getInstanceItemByObjectId(objectId);
			var newItemId:ItemId =  new ItemId();

			if ( this.id != null ) {
				newItemId.contractId = this.id.contractId ;
				newItemId.endorsementId = this.id.endorsementId ;
			}
			newItemId.itemId = getNextItemId();
			
			newItem.id =  newItemId;
			newItem.endorsement = this;
			newItem.objectId = objectId;
			this.items.addItem( newItem );
			
			return newItem;
   		}

		public function attachItem(newItem:Item):Item{
			this.items.addItem( newItem );
			newItem.updateKey( this );
			return newItem;
		}
	
		
		/**
		 * @return
		 */
		public function getNextItemId():int {
			var maxSequenceId:int = 0;
			
			for each ( var tmpItem:Item in this.items ) {
				if ( tmpItem.id != null && tmpItem.id.itemId > maxSequenceId ) {
					maxSequenceId=tmpItem.id.itemId;
				}
			}
			
			maxSequenceId++;
			return maxSequenceId;
		}

		/**
		 * Returns the main Item
		 * @return
		 */
		public function getMainItem () : Item{
			var itemLifeCycle:Item = null;
			var mainItem:Item = null;
			for each(var workItem : Item in this.items){
	 	 		if(workItem.objectId == Item.OBJECT_LIFE_CYCLE){
	 	 			itemLifeCycle = workItem;
 				}
				if(workItem.id.itemId == 1){
	 				mainItem = workItem;
 				}
 	 		}
 			return mainItem;
 		}
 		
 		public function listNotHolder() : ArrayCollection{
 			var arrayReturn : ArrayCollection = new ArrayCollection();
			for each(var courseItem : Item in items){
				var itemPersonalRisk : ItemPersonalRisk = (courseItem as ItemPersonalRisk);
					if(itemPersonalRisk != null && itemPersonalRisk.personalRiskType != ItemPersonalRisk.PERSONAL_RISK_TYPE_TITULAR){
 						arrayReturn.addItem(itemPersonalRisk);
				}
			}
 			return arrayReturn;
 		}

		public function addInstallment(installmentId:int):Installment{
			var newInstallment:Installment = new Installment();
			var newInstallmentId:InstallmentId = new InstallmentId();
	
			if ( this.id != null  ) {
				newInstallmentId.endorsementId = this.id.endorsementId;
				newInstallmentId.contractId = this.id.contractId;
			}
			
			newInstallmentId.installmentId = installmentId ;
	
			newInstallment.id = newInstallmentId;
			
			newInstallment.endorsement = this;
			this.installments.addItem( newInstallment );
			
			return newInstallment;
		}
		
		public function attachInstallment(newInstallment:Installment):Installment{
			if (this.installments == null){
				this.installments = new ArrayCollection();
			}
			this.installments.addItem( newInstallment );
			newInstallment.updateKey( this );
			return newInstallment;
		}	

		public function updateKey(parent:Contract):void{
			if ( parent.contractId == 0 ) {
				throw new IllegalOperationError();
			}
	
			this.contract = parent ;
			
			if ( this.id == null ) {
				this.id =  new EndorsementId();
				this.id.endorsementId = parent.nextEndorsementId();
				this.id.contractId = parent.contractId;
			}else{
				this.id.contractId = parent.contractId;
			}
			
			for each ( var workItem:Item in this.items ) {
				workItem.updateKey( this );
			}
			
 			for each ( var workInstallment:Installment in this.installments ) {
				workInstallment.updateKey( this );
			}
		}
		
 		private function getInstanceItemByObjectId (objectId : int) : Item{
 			var itemRisk : Item = null;
	 		switch (objectId){
				case Item.OBJECT_LIFE:
				 	itemRisk = new ItemPersonalRisk();
				 	break;	 			
				case Item.OBJECT_LIFE_CYCLE:
				 	itemRisk = new ItemPersonalRisk();
				 	break;
				case Item.OBJECT_PERSONAL_ACCIDENT: 
					itemRisk = new ItemPersonalRisk();
					break;
				case Item.OBJECT_HABITAT: 
					itemRisk = new ItemProperty();
					break;
				case Item.OBJECT_CREDIT_INSURANCE:
					itemRisk = new ItemPersonalRisk();
					break;
				case Item.OBJECT_AUTOMOVIL:
					itemRisk = new ItemAuto();
					break;
				case Item.OBJECT_CONDOMINIUM:
					itemRisk = new ItemProperty();
					break;
				case Item.OBJECT_CAPITAL:
					itemRisk = new ItemProperty();
					break;
				case Item.OBJECT_PURCHASE_PROTECTED:
					itemRisk = new ItemPurchaseProtected();
					break;
			}
			return itemRisk;
		}
 		
 		public function addCustomer() : Customer{
 			var customer : Customer = new Customer();
 			customerByInsuredId = customer;
 			return customer;
 		}

		public function addCustomerPolicyHolder() : Customer{
 			var customer : Customer = new Customer();
 			customerByPolicyHolderId = customer;
 			return customer;
 		}

		public function installmentById(installmentId:int):Installment{
			for each(var installment:Installment in installments){
				if(installment.id.installmentId == installmentId){
					return installment;
				}
			}
			return null;
		}

 		public function getFirstInstallment() : Installment{
 			if(installments != null){
 				for ( var i:int = 0; i < installments.length; i++ ){
		 			if( installments[i] as Installment )
		 				return installments[i];
		 		}
 			}
 			return null;
 		}

		public function isFlexible():Boolean {
			if (this.term
					&& this.term.invoiceType
						&& this.term.invoiceType == Term.INVOICE_TYPE_FLEXIBLE) {
				return true;
			}
			return false;
		}
    }  

}