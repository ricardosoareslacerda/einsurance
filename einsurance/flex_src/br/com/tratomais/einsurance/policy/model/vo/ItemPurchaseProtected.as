package br.com.tratomais.einsurance.policy.model.vo
{
	
	[RemoteClass(alias="br.com.tratomais.core.model.policy.ItemPurchaseProtected")]	
	[Managed]
	public class ItemPurchaseProtected extends ItemPersonalRisk
	{

		private var _cardType:int;
		private var _cardBrandType:int;
		private var _cardNumber:String;
		private var _cardExpirationDate:int;
		private var _cardHolderName:String;


		public function ItemPurchaseProtected()
		{
			
		}

		public function get cardType():int{
			return _cardType;
		}

		public function set cardType(pData:int):void{
			_cardType=pData;
		}

		public function get cardBrandType():int{
			return _cardBrandType;
		}

		public function set cardBrandType(pData:int):void{
			_cardBrandType=pData;
		}

		public function get cardNumber():String{
			return _cardNumber;
		}

		public function set cardNumber(pData:String):void{
			_cardNumber=pData;
		}

		public function get cardExpirationDate():int{
			return _cardExpirationDate;
		}

		public function set cardExpirationDate(pData:int):void{
			_cardExpirationDate=pData;
		}

		public function get cardHolderName():String{
			return _cardHolderName;
		}

		public function set cardHolderName(pData:String):void{
			_cardHolderName=pData;
		}


		
	}
}