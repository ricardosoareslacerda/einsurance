package br.com.tratomais.einsurance.policy.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.policy.ItemClause")]      
    [Managed]  
    public class ItemClause extends HibernateBean  
    {  
        private var _id:ItemClauseId;  
        private var _item:Item;  
        private var _clauseCode:String;  
        private var _clauseValue:Number;  
        private var _clauseDescription:String;
        private var _clauseAutomatically:Boolean; 
        private var _effectiveDate:Date;  
        private var _expiryDate:Date;  
  
        public function ItemClause()  
        {  
        }  
  
        public function get id():ItemClauseId{  
            return _id;  
        }  
  
        public function set id(pData:ItemClauseId):void{  
            _id=pData;  
        }  
  
        public function get item():Item{  
            return _item;  
        }  
  
        public function set item(pData:Item):void{  
            _item=pData;  
        }  
  
        public function get clauseCode():String{  
            return _clauseCode;  
        }  
  
        public function set clauseCode(pData:String):void{  
            _clauseCode=pData;  
        }  
  
        public function get clauseValue():Number{  
            return _clauseValue;  
        }  
  
        public function set clauseValue(pData:Number):void{  
            _clauseValue=pData;  
        }  
  
        public function get clauseDescription():String{  
            return _clauseDescription;  
        }  
  
        public function set clauseDescription(pData:String):void{  
            _clauseDescription=pData;  
        }  
        
        public function get clauseAutomatically(): Boolean { 
        	return _clauseAutomatically;
        }
        
        public function set clauseAutomatically(pClauseAutomatically: Boolean): void {
        	this._clauseAutomatically = pClauseAutomatically;
        }
  
        public function get effectiveDate():Date{  
            return _effectiveDate;  
        }  
  
        public function set effectiveDate(pData:Date):void{  
            _effectiveDate=pData;  
        }  
  
        public function get expiryDate():Date{  
            return _expiryDate;  
        }  
  
        public function set expiryDate(pData:Date):void{  
            _expiryDate=pData;  
        }  
  
  
          
    }  
}