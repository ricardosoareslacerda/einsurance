package br.com.tratomais.einsurance.policy.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
	
	[RemoteClass(alias="br.com.tratomais.core.model.auto.AutoBrand")]	
	[Managed]
	public class AutoBrand extends HibernateBean
	{

		private var _autoMarkId:int;
		private var _autoMarkCode:String;
		private var _markName:String;
		private var _registred:Date;
		private var _updated:Date;


		public function AutoBrand()
		{
			
		}

		public function get autoMarkId():int{
			return _autoMarkId;
		}

		public function set autoMarkId(pData:int):void{
			_autoMarkId=pData;
		}

		public function get autoMarkCode():String{
			return _autoMarkCode;
		}

		public function set autoMarkCode(pData:String):void{
			_autoMarkCode=pData;
		}

		public function get markName():String{
			return _markName;
		}

		public function set markName(pData:String):void{
			_markName=pData;
		}

		public function get registred():Date{
			return _registred;
		}

		public function set registred(pData:Date):void{
			_registred=pData;
		}

		public function get updated():Date{
			return _updated;
		}

		public function set updated(pData:Date):void{
			_updated=pData;
		}


		
	}
}