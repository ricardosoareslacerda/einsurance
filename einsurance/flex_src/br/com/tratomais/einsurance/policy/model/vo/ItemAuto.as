package br.com.tratomais.einsurance.policy.model.vo
{
	import flash.errors.IllegalOperationError;
	import mx.collections.ArrayCollection;
	
    [RemoteClass(alias="br.com.tratomais.core.model.policy.ItemAuto")]    
    [Managed]  
	public class ItemAuto extends Item
	{
		public static const PERSONAL_RISK_TYPE_MOTORIST = 496;
		public static const PERSONAL_KINSHIP_TYPE_OWN = 294;
		
		private var _autoTableType:int;
		private var _autoModelId:int;
		private var _autoBrandId:int;
		private var _brandName:String;
		private var _modelName:String;
		private var _autoFuelType:int;
		private var _autoTransmissionType:int;
		private var _autoClassType:int;
		private var _autoTariffType:int
		private var _autoCategoryType:int
		private var _autoGroupType:int
		private var	_autoUseType:int
		private var _countryID:String
		private var _stateID:String;
		private var _cityID:String;
		private var _localityCode:String;
		private var _doorNumber:int;
		private var _passengerNumber:int
		private var _yearFabrication:int
		private var _yearModel:int;
		private var _zeroKm:Boolean;
		private var _valueRefer:Number;
		private var _modalityType:String;
		private var _variationFactor:Number;
		private var _tonneQuantity:int;
		private var _kmQuantity:int;
		private var _colorType:String
		private var _licensePlate:String;
		private var _chassisSerial:String;
		private var _engineSerial:String;
		private var _licenseCode:String;
		private var _invoiceNumber:String
		private var _invoiceDate:Date;
		private var _inspectionNumber:String;
		private var _inspectionDate:Date;
		private var _attributeValue:int;
		private var _motorists:ArrayCollection = new ArrayCollection();
		
		public function get autoModelId():int{
			return _autoModelId;
		}

		public function set autoModelId(pData:int):void{
			_autoModelId=pData;
		}

		public function get autoBrandId():int{
			return _autoBrandId;
		}

		public function set autoBrandId(pData:int):void{
			_autoBrandId=pData;
		}
				
		public function get motorists():ArrayCollection{
			return _motorists;
		}

		public function set motorists(pData:ArrayCollection):void{
			_motorists=pData;
		}
		
		public function set autoTableType( pData:int ):void
		{
			this._autoTableType = pData;
		}
		
		public function get autoTableType():int
		{
			return _autoTableType;
		}
		
		public function set brandName( pData:String ):void
		{
			this._brandName = pData;
		}
		
		public function get brandName():String
		{
			return _brandName;
		}
		
		public function set modelName( pData:String ):void
		{
			this._modelName = pData;
		}
		
		public function get modelName():String
		{
			return _modelName;
		}

		public function set autoFuelType( pData:int ):void
		{
			this._autoFuelType = pData;
		}
		
		public function get autoFuelType():int
		{
			return _autoFuelType;
		}

		public function set autoTransmissionType( pData:int ):void
		{
			this._autoTransmissionType = pData;
		}
		
		public function get autoTransmissionType():int
		{
			return _autoTransmissionType;
		}
		
		public function set autoClassType( pData:int ):void
		{
			this._autoClassType = pData;
		}
		
		public function get autoClassType():int
		{
			return _autoClassType;
		}
		
		public function set autoTariffType( pData:int ):void
		{
			this._autoTariffType = pData;
		}
		
		public function get autoTariffType():int
		{
			return _autoTariffType;
		}
				
		public function set autoCategoryType( pData:int ):void
		{
			this._autoCategoryType = pData;
		}
		
		public function get autoCategoryType():int
		{
			return _autoCategoryType;
		}
		
		public function set autoGroupType( pData:int ):void
		{
			this._autoGroupType = pData;
		}
		
		public function get autoGroupType():int
		{
			return _autoGroupType;
		}
		
		public function set autoUseType( pData:int ):void
		{
			this._autoUseType = pData;
		}
		
		public function get autoUseType():int
		{
			return _autoUseType;
		}
		
		public function set countryID( pData:String ):void
		{
			this._countryID = pData;
		}
		
		public function get countryID():String
		{
			return _countryID;
		}
		
		public function set stateID( pData:String ):void
		{
			this._stateID = pData;
		}
		
		public function get stateID():String
		{
			return _stateID;
		}
		
		public function set cityID( pData:String ):void
		{
			this._cityID = pData;
		}
		
		public function get cityID():String
		{
			return _cityID;
		}
		
		public function set localityCode( pData:String ):void
		{
			this._localityCode = pData;
		}
		
		public function get localityCode():String
		{
			return _localityCode;
		}
		
		public function set doorNumber( pData:int ):void
		{
			this._doorNumber = pData;
		}
		
		public function get doorNumber():int
		{
			return _doorNumber;
		}
		
		public function set passengerNumber( pData:int ):void
		{
			this._passengerNumber = pData;
		}
		
		public function get passengerNumber():int
		{
			return _passengerNumber;
		}
		
		public function set yearFabrication( pData:int ):void
		{
			this._yearFabrication = pData;
		}
		
		public function get yearFabrication():int
		{
			return _yearFabrication;
		}
		
		public function set yearModel( pData:int ):void
		{
			this._yearModel = pData;
		}
		
		public function get yearModel():int
		{
			return _yearModel;
		}
		
		public function set zeroKm( pData:Boolean ):void
		{
			this._zeroKm = pData;
		}
		
		public function get zeroKm():Boolean
		{
			return _zeroKm;
		}
		
		public function set valueRefer( pData:Number ):void
		{
			this._valueRefer = pData;
		}
		
		public function get valueRefer():Number
		{
			return _valueRefer;
		}
		
		public function set modalityType( pData:String ):void
		{
			this._modalityType = pData;
		}
		
		public function get modalityType():String
		{
			return _modalityType;
		}
		
		public function set variationFactor( pData:Number ):void
		{
			this._variationFactor = pData;
		}
		
		public function get variationFactor():Number
		{
			return _variationFactor;
		}
		
		public function set tonneQuantity( pData:int ):void
		{
			this._tonneQuantity = pData;
		}
		
		public function get tonneQuantity():int
		{
			return _tonneQuantity;
		}
		
		public function set kmQuantity( pData:int ):void
		{
			this._kmQuantity = pData;
		}
		
		public function get kmQuantity():int
		{
			return _kmQuantity;
		}
		
		public function set colorType( pData:String ):void
		{
			this._colorType = pData;
		}
		
		public function get colorType():String
		{
			return _colorType;
		}
		
		public function set licensePlate( pData:String ):void
		{
			this._licensePlate = pData;
		}
		
		public function get licensePlate():String
		{
			return _licensePlate;
		}
		
		public function set chassisSerial( pData:String ):void
		{
			this._chassisSerial = pData;
		}
		
		public function get chassisSerial():String
		{
			return _chassisSerial;
		}
		
		public function set engineSerial( pData:String ):void
		{
			this._engineSerial = pData;
		}
		
		public function get engineSerial():String
		{
			return _engineSerial;
		}
		
		public function set licenseCode( pData:String ):void
		{
			this._licenseCode = pData;
		}
		
		public function get licenseCode():String
		{
			return _licenseCode;
		}
		
		public function set invoiceNumber( pData:String ):void
		{
			this._invoiceNumber = pData;
		}
		
		public function get invoiceNumber():String
		{
			return _invoiceNumber;
		}
		
		public function set invoiceDate( pData:Date ):void
		{
			this._invoiceDate = pData;
		}
		
		public function get invoiceDate():Date
		{
			return _invoiceDate;
		}
		
		public function set inspectionNumber( pData:String ):void
		{
			this._inspectionNumber = pData;
		}
		
		public function get inspectionNumber():String
		{
			return _inspectionNumber;
		}
		
		public function set inspectionDate( pData:Date ):void
		{
			this._inspectionDate = pData;
		}
		
		public function get inspectionDate():Date
		{
			return _inspectionDate;
		}
		
		public function set attributeValue( pData:int ):void
		{
			this._attributeValue = pData;
		}
		
		public function get attributeValue():int
		{
			return _attributeValue;
		}
		
		override public function updateKey(parent:Endorsement):void{
			if ( parent.id == null ) {
				throw new IllegalOperationError();
			}
			
			super.updateKey( parent );
			for each (var workMotorist : Motorist in  this.motorists){
				workMotorist.updateKey( this );
			}
		}
		
		public function addMotorist():Motorist{
			var newMotorist:Motorist = new Motorist();
			var newMotoristId:MotoristId = new MotoristId();
	
			if ( this.id != null  ) {
				newMotoristId.itemId = this.id.itemId;
				newMotoristId.endorsementId = this.id.endorsementId;
				newMotoristId.contractId = this.id.contractId;
			}
			newMotoristId.motoristId = getNextMotoristId();
			newMotorist.id = newMotoristId;
			
			newMotorist.itemAuto = this;
			this.motorists.addItem( newMotorist );
			
			return newMotorist;
		}
		
		public function attachMotorist(newMotorist:Motorist):Motorist{
			this.motorists.addItem( newMotorist );
			newMotorist.updateKey( this );
			return newMotorist;
		}
		
		/**
		 * @return
		 */
		public function getNextMotoristId():int {
			var maxSequenceId:int = 0;
			
			for each ( var tmpMotorist:Motorist in this.motorists ) {
				if ( tmpMotorist.id != null && tmpMotorist.id.motoristId > maxSequenceId ) {
					maxSequenceId=tmpMotorist.id.motoristId;
				}
			}
			
			maxSequenceId++;
			return maxSequenceId;
		}
		
		/**
		 * Returns the main Motorist
		 * @return
		 */
		public function getMainMotorist(): Motorist {
			var mainMotorist:Motorist = null;
			for each (var workMotorist:Motorist in this.motorists) {
	 	 		if (workMotorist.motoristType == Motorist.MOTORIST_TYPE_MAIN){
	 	 			mainMotorist = workMotorist;
 				}
 	 		}
 			return mainMotorist;
 		}
	}
}