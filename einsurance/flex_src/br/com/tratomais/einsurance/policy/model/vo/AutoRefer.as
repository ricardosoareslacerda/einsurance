package br.com.tratomais.einsurance.policy.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
    [RemoteClass(alias="br.com.tratomais.core.model.auto.AutoRefer")]    
    [Managed] 	
	public class AutoRefer extends HibernateBean
	{
		public static const AUTO_TABLE_TYPE_MAIN = 340;
		
		private var _id:int;
		private var _autoTableType:int;
		private var _markCode:String
		private var _markName:String
		private var _modelCode:String
		private var _modelName:String;
		private var _versionCode:String;
		private var _versionName:String;

		public function set id( pData:int ):void
		{
			this._id = pData;
		}
		
		public function get id():int
		{
			return _id;
		}
		
		public function set autoTableType( pData:int ):void
		{
			this._autoTableType = pData;
		}
		
		public function get autoTableType():int
		{
			return _autoTableType;
		}
		
		public function set markCode( pData:String ):void
		{
			this._markCode = pData;
		}
		
		public function get markCode():String
		{
			return _markCode;
		}

		public function set markName( pData:String ):void
		{
			this._markName = pData;
		}
		
		public function get markName():String
		{
			return _markName;
		}
		
		public function set modelCode( pData:String ):void
		{
			this._modelCode = pData;
		}
		
		public function get modelCode():String
		{
			return _modelCode;
		}
		
		public function set modelName( pData:String ):void
		{
			this._modelName = pData;
		}
		
		public function get modelName():String
		{
			return _modelName;
		}

		public function set versionCode( pData:String ):void
		{
			this._versionCode = pData;
		}
		
		public function get versionCode():String
		{
			return _versionCode;
		}
		
		public function set versionName( pData:String ):void
		{
			this._versionName = pData;
		}
		
		public function get versionName():String
		{
			return _versionName;
		}
	}
}