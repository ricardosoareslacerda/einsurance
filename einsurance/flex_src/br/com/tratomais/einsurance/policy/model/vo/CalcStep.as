package br.com.tratomais.einsurance.policy.model.vo
{
	import flash.errors.IllegalOperationError;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;

    [RemoteClass(alias="br.com.tratomais.core.model.policy.CalcStep")]    
    [Managed]  
    public class CalcStep extends HibernateBean  
    {  
        private var _id:CalcStepId;  
        private var _itemCoverage:ItemCoverage;  
        private var _factorId:int;  
        private var _roadmapType:int
        private var _factorType:int;  
        private var _description:String;  
        private var _initialValue:Number;  
        private var _rateFactor:Number;  
        private var _fixedValue:Number;  
        private var _minimumValue:Number;  
        private var _finalValue:Number;  
  
        public function CalcStep()  
        {  
        }  
  
        public function get id():CalcStepId{  
            return _id;  
        }  
  
        public function set id(pData:CalcStepId):void{  
            _id=pData;  
        }  
  
        public function get itemCoverage():ItemCoverage{  
            return _itemCoverage;  
        }  
  
        public function set itemCoverage(pData:ItemCoverage):void{  
            _itemCoverage=pData;  
        }  
  
        public function get factorId():int{  
            return _factorId;  
        }  
  
        public function set factorId(pData:int):void{  
            _factorId=pData;  
        }  
 
        public function get roadmapType():int{  
            return _roadmapType;  
        }  
  
        public function set roadmapType(pData:int):void{  
            _roadmapType=pData;  
        } 
          
        public function get factorType():int{  
            return _factorType;  
        }  
  
        public function set factorType(pData:int):void{  
            _factorType=pData;  
        }  
  
        public function get description():String{  
            return _description;  
        }  
  
        public function set description(pData:String):void{  
            _description=pData;  
        }  
  
        public function get initialValue():Number{  
            return _initialValue;  
        }  
  
        public function set initialValue(pData:Number):void{  
            _initialValue=pData;  
        }  
  
        public function get rateFactor():Number{  
            return _rateFactor;  
        }  
  
        public function set rateFactor(pData:Number):void{  
            _rateFactor=pData;  
        }  
  
        public function get fixedValue():Number{  
            return _fixedValue;  
        }  
  
        public function set fixedValue(pData:Number):void{  
            _fixedValue=pData;  
        }  
  
        public function get minimumValue():Number{  
            return _minimumValue;  
        }  
  
        public function set minimumValue(pData:Number):void{  
            _minimumValue=pData;  
        }  
  
        public function get finalValue():Number{  
            return _finalValue;  
        }  
  
        public function set finalValue(pData:Number):void{  
            _finalValue=pData;  
        }  

		public function updateKey(parent:ItemCoverage):void{
			if ( parent.id == null ) {
				throw new IllegalOperationError();
			}
			this.itemCoverage = parent;
			if ( this.id == null ) {
				//this.setId( new CalcStepId(parent.findNextCalcStepSequence(),parent.getId().getContractId(),parent.getId().getItemId(),parent.getId().getEndorsementId(),parent.getId().getContractId()) );
				this.id.sequenceId = parent.findNextCalcStepSequence();
				this.id.coverageId = parent.id.coverageId;
				this.id.itemId = parent.id.itemId;
				this.id.endorsementId = parent.id.endorsementId;
				this.id.contractId = parent.id.contractId;
			}else{
				this.id.coverageId = parent.id.coverageId;
				this.id.itemId = parent.id.itemId;
				this.id.endorsementId = parent.id.endorsementId;
				this.id.contractId = parent.id.contractId;
			}
		}

    }
} 