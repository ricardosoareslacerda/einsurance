package br.com.tratomais.einsurance.policy.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
    [RemoteClass(alias="br.com.tratomais.core.model.auto.AutoValue")]    
    [Managed]  
	public class AutoValue extends HibernateBean
	{
		private var _autoValueId:AutoValueId;
		private var _expiryDate:Date
		private var _valueRefer:Number
		private var _registred:Date
		private var _updated:Date

		public function set autoValueId( pData:AutoValueId ):void
		{
			this._autoValueId = pData;
		}
		
		public function get autoValueId():AutoValueId
		{
			return _autoValueId;
		}
		
		public function set expiryDate( pData:Date ):void
		{
			this._expiryDate = pData;
		}
		
		public function get expiryDate():Date
		{
			return _expiryDate;
		}
		
		public function set valueRefer( pData:Number ):void
		{
			this._valueRefer = pData;
		}
		
		public function get valueRefer():Number
		{
			return _valueRefer;
		}
		
		public function set registred( pData:Date ):void
		{
			this._registred = pData;
		}
		
		public function get registred():Date
		{
			return _registred;
		}
		
		public function set updated( pData:Date ):void
		{
			this._updated = pData;
		}
		
		public function get updated():Date
		{
			return _updated;
		}
	}
}