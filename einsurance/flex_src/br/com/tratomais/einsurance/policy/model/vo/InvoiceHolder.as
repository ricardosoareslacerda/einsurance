package br.com.tratomais.einsurance.policy.model.vo
{
	import br.com.tratomais.einsurance.customer.model.vo.Customer;

	import flash.errors.IllegalOperationError;

	import mx.collections.ArrayCollection;

	import net.digitalprimates.persistence.hibernate.HibernateBean;

	[RemoteClass(alias="br.com.tratomais.core.model.policy.InvoiceHolder")]
	[Managed]
	public class InvoiceHolder extends HibernateBean
	{
		private var _id:InvoiceHolderId;
		private var _endorsement:Endorsement;
		private var _policyHolder:Customer;
		private var _invoiceCutDay:int;
		private var _invoiceIssueDay:int;
		private var _invoiceDueDay:int;
		private var _invoiceEffectiveDay:int;
		private var _invoiceLocked:Boolean;
		private var _invoiceMovementType:int;
		private var _invoiceCutType:int;
		private var _invoiceDueType:int;
		private var _processed:Boolean;
		private var _billingMethodId:int;
		private var _bankNumber:int;
		private var _bankAgencyNumber:String;
		private var _bankAccountNumber:String;
		private var _paymentGroupedCode:String;
		private var _changeSchedule:Boolean = true;

		/**
		 * Transients
		 */
		private var _changed:Boolean;
		private var _descriptions:ArrayCollection;

		public function InvoiceHolder(id:InvoiceHolderId = null, policyHolder:Customer = null, parent:Endorsement = null){
			_id = id;
			_policyHolder = policyHolder;
			_endorsement = parent;
		}

		public function get id():InvoiceHolderId{
			return _id;
		}

		public function set id(pData:InvoiceHolderId):void{
			_id = pData;
		}

		public function get endorsement():Endorsement{
			return _endorsement;
		}

		public function set endorsement(pData:Endorsement):void{
			_endorsement = pData;
		}

		public function get policyHolder():Customer{
			return _policyHolder;
		}

		public function set policyHolder(pData:Customer):void{
			_policyHolder = pData;
		}

		public function get invoiceCutDay():int{
			return _invoiceCutDay;
		}

		public function set invoiceCutDay(pData:int):void{
			_invoiceCutDay = pData;
		}

		public function get invoiceIssueDay():int{
			return _invoiceIssueDay;
		}

		public function set invoiceIssueDay(pData:int):void{
			_invoiceIssueDay = pData;
		}

		public function get invoiceDueDay():int{
			return _invoiceDueDay;
		}

		public function set invoiceDueDay(pData:int):void{
			_invoiceDueDay = pData;
		}

		public function get invoiceEffectiveDay():int{
			return _invoiceEffectiveDay;
		}

		public function set invoiceEffectiveDay(pData:int):void{
			_invoiceEffectiveDay = pData;
		}

		public function get invoiceLocked():Boolean{
			return _invoiceLocked;
		}

		public function set invoiceLocked(pData:Boolean):void{
			_invoiceLocked = pData;
		}

		public function get invoiceMovementType():int{
			return _invoiceMovementType;
		}

		public function set invoiceMovementType(pData:int):void{
			_invoiceMovementType = pData;
		}

		public function get invoiceCutType():int{
			return _invoiceCutType;
		}

		public function set invoiceCutType(pData:int):void{
			_invoiceCutType = pData;
		}

		public function get invoiceDueType():int{
			return _invoiceDueType;
		}

		public function set invoiceDueType(pData:int):void{
			_invoiceDueType = pData;
		}

		public function get processed():Boolean{
			return _processed;
		}

		public function set processed(pData:Boolean):void{
			_processed = pData;
		}

		public function get billingMethodId():int{
			return _billingMethodId;
		}

		public function set billingMethodId(pData:int):void{
			_billingMethodId = pData;
		}

		public function get bankNumber():int{
			return _bankNumber;
		}

		public function set bankNumber(pData:int):void{
			_bankNumber = pData;
		}

		public function get bankAgencyNumber():String{
			return _bankAgencyNumber;
		}

		public function set bankAgencyNumber(pData:String):void{
			_bankAgencyNumber = pData;
		}

		public function get bankAccountNumber():String{
			return _bankAccountNumber;
		}

		public function set bankAccountNumber(pData:String):void{
			_bankAccountNumber = pData;
		}

		public function get paymentGroupedCode():String{
			return _paymentGroupedCode;
		}

		public function set paymentGroupedCode(pData:String):void{
			_paymentGroupedCode = pData;
		}

		public function get changeSchedule():Boolean{
			return _changeSchedule;
		}

		public function set changeSchedule(pData:Boolean):void{
			_changeSchedule = pData;
		}

		public function updateKey(parent:Endorsement):void{
			if(!parent || !parent.id){
				throw new IllegalOperationError();
			}
			
			this.endorsement = parent;
			
			if(id == null){
				id = new InvoiceHolderId(parent.id.contractId, parent.id.endorsementId);
			}
			else{
				id.contractId = parent.id.contractId;
				id.endorsementId = parent.id.endorsementId;
			}
		}

		public function updatePolicyHolder(pData:Customer):void{
			if(pData){
				if(policyHolder != null
						&& policyHolder == pData)
					return;
				
				this.policyHolder = pData;
				
				id.policyHolderId = pData.customerId;
			}
		}

		public function get changed():Boolean{
			return _changed;
		}

		public function set changed(pData:Boolean):void{
			_changed = pData;
		}

		public function get descriptions():ArrayCollection{
			if(!_descriptions)
				_descriptions = new ArrayCollection; // to not null instance
			
			return _descriptions;
		}

		public function set descriptions(pData:ArrayCollection):void{
			if(!pData || pData.length == 0)
				pData = new ArrayCollection; // to not duplicate instance
			
			_descriptions = pData;
		}
	}
}