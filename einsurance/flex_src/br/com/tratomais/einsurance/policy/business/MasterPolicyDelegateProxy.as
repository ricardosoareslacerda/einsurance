////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Valéria Sousa
// @version 1.0
// @lastModified 13/01/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.policy.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	import br.com.tratomais.einsurance.policy.model.vo.MasterPolicy;
	
	import mx.rpc.IResponder;
	
	public class MasterPolicyDelegateProxy
	{
		private var responder : IResponder;
		private var service : Object;
		
		public function MasterPolicyDelegateProxy(pResponder : IResponder )
		{
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_MASTER_POLICY);
			responder = pResponder;
		}
		
		public function listAllWithDescriptions():void
		{
			var call:Object = service.listAllWithDescriptions();
			call.addResponder(responder);
		}		

		public function saveObject( masterPolicy:MasterPolicy ):void{
			var call:Object = service.saveMasterPolicy( masterPolicy );
			call.addResponder(responder);		
		}
	}
}