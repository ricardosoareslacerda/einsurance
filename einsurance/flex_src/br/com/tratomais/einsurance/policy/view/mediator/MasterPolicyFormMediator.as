////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Valéria Sousa
// @version 1.0
// @lastModified 13/01/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.policy.view.mediator
{
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.customer.model.vo.Broker;
	import br.com.tratomais.einsurance.policy.model.vo.MasterPolicy;
	import br.com.tratomais.einsurance.policy.view.components.MasterPolicyForm;
	import br.com.tratomais.einsurance.products.model.vo.Product;
	import br.com.tratomais.einsurance.useradm.model.vo.Partner;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class MasterPolicyFormMediator extends Mediator implements IMediator
	{
		public static const NAME:String = 'MasterPolicyFormMediator';
		
		public function MasterPolicyFormMediator(viewComponent:Object=null)
		{
			
            super(NAME, viewComponent);  
            try{
            	
            }
            catch(error:Error){
            	throw error;
            }        
		}
		
		override public function onRegister():void 
		{
			super.onRegister();
		}
		
		override public function onRemove():void 
		{
			super.onRemove();
		}
		
		public function get masterPolicyForm():MasterPolicyForm{
			return viewComponent as MasterPolicyForm;
		}		
		
		override public function listNotificationInterests():Array
		{
			return [];
		}
		
		override public function handleNotification(notification:INotification):void
		{
            switch ( notification.getName() )  
            {}  
		}

		public function enabledForm(valor:Boolean):void{
			
			masterPolicyForm.cmbxPartner.enabled = valor;
			masterPolicyForm.cmbxBroker.enabled = valor;
			masterPolicyForm.cmbxProduct.enabled = valor;
			masterPolicyForm.txtPolicyNumber.enabled = valor;	
			masterPolicyForm.chkPolicyInformed.enabled = valor;		
			masterPolicyForm.txtCommissionFactor.enabled = valor;	
			masterPolicyForm.txtLabourFactor.enabled = valor;	
			masterPolicyForm.txtAgencyFactor.enabled = valor;	
			masterPolicyForm.txtInvoiceCutDay.enabled = valor;	
			masterPolicyForm.txtInvoiceIssueDay.enabled = valor;	
			masterPolicyForm.txtInvoiceDueDay.enabled = valor;
			masterPolicyForm.dfEffectiveDate.enabled = valor;
			masterPolicyForm.dfExpiryDate.enabled = valor;
			masterPolicyForm.chkAutomaticRenewal.enabled = valor;
			masterPolicyForm.txtRenewalBeforeDay.enabled = valor;
		}
	
		public function populateDetailsToForm( masterPolicy:MasterPolicy ):void
		{
			if(masterPolicyForm.cmbxProduct.selectedItem as Product){
				masterPolicyForm.txtPolicyNumberDigit.text = (masterPolicyForm.cmbxProduct.selectedItem as Product).productCode;
			}

			masterPolicyForm.cmbxPartner.selectedValue		= masterPolicy.partnerId.toString();
			masterPolicyForm.cmbxBroker.selectedValue		= masterPolicy.brokerId.toString();
			masterPolicyForm.cmbxProduct.selectedValue		= masterPolicy.productId.toString();			
			masterPolicyForm.chkPolicyInformed.selected 	= masterPolicy.policyInformed;	
			masterPolicyForm.txtCommissionFactor.text 		= masterPolicy.commissionFactor.toString();
			masterPolicyForm.txtLabourFactor.text 			= masterPolicy.labourFactor.toString();
			masterPolicyForm.txtAgencyFactor.text 			= masterPolicy.agencyFactor.toString();
			masterPolicyForm.txtInvoiceCutDay.value 		= masterPolicy.invoiceCutDay as int;
			masterPolicyForm.txtInvoiceIssueDay.value 		= masterPolicy.invoiceIssueDay as int;
			masterPolicyForm.txtInvoiceDueDay.value			= masterPolicy.invoiceDueDay as int;
			masterPolicyForm.dfEffectiveDate.selectedDate 	= masterPolicy.effectiveDate;
			masterPolicyForm.dfExpiryDate.selectedDate		= masterPolicy.expiryDate;
			masterPolicyForm.chkAutomaticRenewal.selected 	= masterPolicy.automaticRenewal;
			masterPolicyForm.txtRenewalBeforeDay.value 		= masterPolicy.renewalBeforeDay as int;
			
			if(masterPolicy.policyNumber != null){
				masterPolicyForm.txtPolicyNumber.text 		= masterPolicy.policyNumber.substr(3, masterPolicy.policyNumber.length -1);
				masterPolicyForm.txtPolicyNumberDigit.text	= masterPolicy.policyNumber.substr(0, 3);
				masterPolicyForm.txtPolicyNumber.enabled = false;
			}
			Utilities.formatNumber(masterPolicyForm.txtCommissionFactor, 4);
			Utilities.formatNumber(masterPolicyForm.txtLabourFactor, 4);
			Utilities.formatNumber(masterPolicyForm.txtAgencyFactor, 4);
        }

		public function populateDetailsToObject( masterPolicy:MasterPolicy ):void
		{
			var _effectiveDate:Date = masterPolicyForm.dfEffectiveDate.newDate; 	//new Date( formatDate( masterPolicyForm.dfEffectiveDate.text ) );
			var _expiryDate:Date = masterPolicyForm.dfExpiryDate.newDate; 			// new Date( formatDate( masterPolicyForm.dfExpiryDate.text ) );
			
			try {
				
				masterPolicy.partnerId 			=	(masterPolicyForm.cmbxPartner.selectedItem as Partner).partnerId as int;
				masterPolicy.brokerId			= 	(masterPolicyForm.cmbxBroker.selectedItem as Broker).brokerId as int;
				masterPolicy.productId			= 	(masterPolicyForm.cmbxProduct.selectedItem as Product).productId as int;
				masterPolicy.policyNumber 		=	masterPolicyForm.txtPolicyNumberDigit.text + masterPolicyForm.txtPolicyNumber.text;
				masterPolicy.policyInformed 	=	masterPolicyForm.chkPolicyInformed.selected;
				masterPolicy.commissionFactor 	=	Number( masterPolicyForm.txtCommissionFactor.text );
				masterPolicy.labourFactor 		=	Number( masterPolicyForm.txtLabourFactor.text );
				masterPolicy.agencyFactor 		=	Number( masterPolicyForm.txtAgencyFactor.text );
				masterPolicy.invoiceCutDay 		=	masterPolicyForm.txtInvoiceCutDay.value as int;
				masterPolicy.invoiceIssueDay 	=	masterPolicyForm.txtInvoiceIssueDay.value as int;
				masterPolicy.invoiceDueDay		=	masterPolicyForm.txtInvoiceDueDay.value as int;	
				masterPolicy.effectiveDate		=  _effectiveDate;
				masterPolicy.expiryDate			=  _expiryDate;
				masterPolicy.automaticRenewal 	=	masterPolicyForm.chkAutomaticRenewal.selected;
				masterPolicy.renewalBeforeDay	=	masterPolicyForm.txtRenewalBeforeDay.value as int;
        	}
			catch(error:Error){
				throw error;
			}				
		}
		
		public function validateEndDate() : Boolean{
			return 	(masterPolicyForm.dfExpiryDate.newDate > masterPolicyForm.dfEffectiveDate.newDate   
					&&  masterPolicyForm.dfExpiryDate.newDate > new Date());
		}
		
		public function validateInitialDate() : Boolean{
			if(masterPolicyForm.dfEffectiveDate.enabled == true){
				var initialDate : Date = masterPolicyForm.dfEffectiveDate.newDate;
				var today : Date = new Date();
				if(		today.day == initialDate.day 
					&& 	today.month == initialDate.month
					&& 	today.fullYear == initialDate.fullYear){
						
					return true;
				}
				return (initialDate > today);
			}else{
				return true;
			}
		}

		public function validateMaximumComissionValue(   ) : Boolean{
			var valueComission : Number = Number( masterPolicyForm.txtCommissionFactor.text ) 
				+ Number( masterPolicyForm.txtLabourFactor.text) 
				+ Number( masterPolicyForm.txtAgencyFactor.text);
			return (valueComission > 0 && valueComission < 100);
		}
	}
}