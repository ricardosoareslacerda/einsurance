////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Valéria Sousa
// @version 1.0
// @lastModified 13/01/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.policy.view.mediator
{
	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.core.components.DoubleClickManager;
	import br.com.tratomais.einsurance.core.controller.StateEnum;
	import br.com.tratomais.einsurance.customer.model.proxy.BrokerProxy;
	import br.com.tratomais.einsurance.customer.model.proxy.PartnerProxy;
	import br.com.tratomais.einsurance.policy.model.proxy.MasterPolicyProxy;
	import br.com.tratomais.einsurance.policy.model.vo.MasterPolicy;
	import br.com.tratomais.einsurance.policy.view.components.MasterPolicyMain;
	import br.com.tratomais.einsurance.products.model.proxy.ProductProxy;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class MasterPolicyMainMediator extends Mediator implements IMediator
	{
		public static const NAME:String = 'MasterPolicyMainMediator';
		private var masterPolicyFormMediator:MasterPolicyFormMediator;
		private var doubleClickManager:DoubleClickManager = null;
		
		private var masterPolicyProxy:MasterPolicyProxy;
						
        public function MasterPolicyMainMediator(viewComponent:Object=null)  
        {  
           super(NAME, viewComponent);  
            masterPolicyProxy = MasterPolicyProxy( facade.retrieveProxy( MasterPolicyProxy.NAME ) );
        } 

        public function get masterPolicyMain():MasterPolicyMain{  
            return viewComponent as MasterPolicyMain;  
        } 

		override public function onRegister():void 
		{
			super.onRegister();
			
			try{
				masterPolicyFormMediator = new MasterPolicyFormMediator(masterPolicyMain.masterPolicyForm);				 
				ApplicationFacade.getInstance().registerOnlyNewMediator(masterPolicyFormMediator);

				masterPolicyMain.addEventListener(StateEnum.SAVE.toString(), onSaveHandler);
				masterPolicyMain.addEventListener(StateEnum.ADD.toString(), onAddHandler);
				masterPolicyMain.addEventListener(StateEnum.CANCEL.toString(), onCancelHandler);				
	
				doubleClickManager = new DoubleClickManager(masterPolicyMain.dtgMasterPolicy);
				doubleClickManager.addEventListener(MouseEvent.CLICK, viewItem);
				doubleClickManager.addEventListener(MouseEvent.DOUBLE_CLICK, changeItem);

				PartnerProxy(facade.retrieveProxy(PartnerProxy.NAME)).listAll();
				ProductProxy(facade.retrieveProxy(ProductProxy.NAME)).listAllProduct();
				BrokerProxy(facade.retrieveProxy(BrokerProxy.NAME)).listAll();
				
			}
			catch(error:Error){
				throw error;	
			}
			
			// Initialize
			this.mainInitialize();		
    	}

		override public function onRemove():void 
		{
			super.onRemove();
			ApplicationFacade.getInstance().removeMediator(masterPolicyFormMediator.getMediatorName());
			
			masterPolicyMain.removeEventListener(StateEnum.SAVE.toString(), onSaveHandler);  
			masterPolicyMain.removeEventListener(StateEnum.ADD.toString(), onAddHandler);  
			masterPolicyMain.removeEventListener(StateEnum.CANCEL.toString(), onCancelHandler);			

			doubleClickManager.removeEventListener(MouseEvent.CLICK, viewItem);
			doubleClickManager.removeEventListener(MouseEvent.DOUBLE_CLICK, changeItem);
			
			onClean();
			masterPolicyMain.dtgMasterPolicy.dataProvider=null;
			
		}
		
        override public function listNotificationInterests():Array  
        {  
            return [NotificationList.MASTER_POLICY_LISTED,
            		NotificationList.PARTNER_LIST_REFRESH,
            		NotificationList.BROKER_LISTED,
            		NotificationList.PRODUCT_LISTED,
            		NotificationList.MASTER_POLICY_SAVE_SUCCESS
             ];  
        } 		

        override public function handleNotification(notification:INotification):void  
        {  
              
            switch ( notification.getName() )  
            {  
				case NotificationList.MASTER_POLICY_LISTED: 
					var listArrayResult: ArrayCollection = notification.getBody() as ArrayCollection;
					this.masterPolicyMain.dtgMasterPolicy.dataProvider = listArrayResult;
					break;
				case NotificationList.PARTNER_LIST_REFRESH:
					var listPartnerResult: ArrayCollection = notification.getBody() as ArrayCollection;
					masterPolicyMain.masterPolicyForm.cmbxPartner.dataProvider = listPartnerResult;
					break;
				case NotificationList.BROKER_LISTED:
					var listBrokerResult: ArrayCollection = notification.getBody() as ArrayCollection;
					masterPolicyMain.masterPolicyForm.cmbxBroker.dataProvider = listBrokerResult;
					break;
				case NotificationList.PRODUCT_LISTED:
					var listArrayResult: ArrayCollection = notification.getBody() as ArrayCollection;
					masterPolicyMain.masterPolicyForm.cmbxProduct.dataProvider = listArrayResult;
					break;	
				case NotificationList.MASTER_POLICY_SAVE_SUCCESS:
					this.onSaveSucess();
					
					var log:String;
					log = ApplicationFacade.getInstance().loggedUser.login;
					log += ":" + ApplicationFacade.getInstance().loggedUser.domain.name;
					log += ":MASTER POLICY";
					log += ":SAVE";
					ApplicationFacade.getInstance().auditLogger = log;
										
					break;									     	
            }
                  
        } 
		
		private function mainInitialize():void{
			this.initializeButtons();
			masterPolicyProxy.listAll();

      		masterPolicyMain.dtgMasterPolicy.enabled = true;
			masterPolicyMain.accordionContainer.selectedIndex = 0;			
			
			this.enabledForms(false);
		}		
		
		private function onSaveSucess():void{
			this.onClean();
			masterPolicyMain.dtgMasterPolicy.dataProvider=null;
			masterPolicyFormMediator.masterPolicyForm.resetForm();      		
			masterPolicyMain.dtgMasterPolicy.enabled = true; 
			this.mainInitialize();
			
			//change enabled of buttons
			this.initializeButtons();				
		}
		
		private function onSaveHandler(event:Event):void{
			var masterPolicy : MasterPolicy = this.getObjectSelect();
			
			if(masterPolicy == null){
				masterPolicy = new MasterPolicy();
			}
			
			if(Utilities.validateForm(masterPolicyMain.masterPolicyForm.validatorForm, ResourceManager.getInstance().getString('resources', 'masterPolicyDetails'))){			
				if(masterPolicyFormMediator.validateInitialDate()){
					if(masterPolicyFormMediator.validateEndDate()){
						if(masterPolicyFormMediator.validateMaximumComissionValue()){
			  				masterPolicyFormMediator.populateDetailsToObject( masterPolicy );
			  				masterPolicyProxy.saveObject(masterPolicy);
						}else{
							Utilities.warningMessage('maximumComissionValue');
						}
					}else{
						Utilities.warningMessage('hgherEndDate');
					}
				}else{
					Utilities.warningMessage('hgherInitialDate');
				}
	  		}
		}

		private function onAddHandler( event:Event ):void{
  			masterPolicyMain.dtgMasterPolicy.selectedItem = null;
      		masterPolicyMain.dtgMasterPolicy.enabled = false;
			masterPolicyMain.masterPolicyForm.chkPolicyInformed.selected = true;
			masterPolicyMain.masterPolicyForm.chkAutomaticRenewal.selected = false;

			this.enabledForms(true);	
			this.onClean();
			masterPolicyMain.imgNew.enabled=false;
			masterPolicyMain.imgSave.enabled=true;
			masterPolicyMain.imgUndo.enabled=true;
		}
		
		private function editItem( event:Event ):void{
			this.enabledForms(true);
      		var obj:Object = masterPolicyMain.dtgMasterPolicy.selectedItem;
			if(obj != null){
				masterPolicyFormMediator.populateDetailsToForm( obj as MasterPolicy);
			}		
		}
		
		private function onCancelHandler( event:Event ):void{
			this.onClean();		
			masterPolicyFormMediator.masterPolicyForm.resetForm();      		
			masterPolicyMain.dtgMasterPolicy.enabled = true; 
			//channelMain.accordionContainer.selectedIndex = 0
			this.enabledForms(false);
			this.initializeButtons();
		}

		private function initializeButtons(): void{
			masterPolicyMain.imgNew.enabled=true;
			masterPolicyMain.imgSave.enabled=false;
			masterPolicyMain.imgUndo.enabled=false;
		}
				
		private function viewItem(event:Event):void {

			var masterPolicy : MasterPolicy = this.getObjectSelect();
			if(editorIsEnabled()==false && masterPolicy != null){
				this.enabledForms(false);				
				masterPolicyFormMediator.populateDetailsToForm( masterPolicy );
			}
		
		}
		
		private function changeItem( event:Event ):void{
		
  			var masterPolicy: MasterPolicy = this.getObjectSelect();
			if(editorIsEnabled()==false && masterPolicy != null){
				this.enabledForms(true);
				masterPolicyMain.imgNew.enabled=false;
				masterPolicyMain.imgSave.enabled=true;
				masterPolicyMain.imgUndo.enabled=true;
				masterPolicyMain.dtgMasterPolicy.enabled = false;
				masterPolicyFormMediator.populateDetailsToForm( masterPolicy );
				masterPolicyFormMediator.masterPolicyForm.enabledEdit();
        	}		
		}
		
		public function enabledForms(valor:Boolean):void{
			masterPolicyFormMediator.enabledForm(valor);

		}
		
		private function editorIsEnabled() : Boolean{
			var isEnabled:Boolean = false;
			if(masterPolicyMain.dtgMasterPolicy.enabled == false){
				isEnabled = true;
			}
			return isEnabled;
		}		
		
      	private function getObjectSelect() : MasterPolicy {
      		var obj : Object = masterPolicyMain.dtgMasterPolicy.selectedItem;
      		var masterPolicy : MasterPolicy = null;
      		if(obj != null){
      			masterPolicy = obj as MasterPolicy;
      			return masterPolicy;
      		}else{
      			return null;
      		}
      	}
      	
        private function onClean():void{
			masterPolicyFormMediator.masterPolicyForm.resetForm(); 
        }      					
		
	}
}