////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2009 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 17/12/2009
//
// NOTICE: Adobe permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance
{  
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.core.controller.ApplicationShutdownCommand;
    import br.com.tratomais.einsurance.core.controller.ApplicationStartupCommand;
    import br.com.tratomais.einsurance.customer.model.proxy.BrokerProxy;
    import br.com.tratomais.einsurance.customer.model.proxy.ChannelProxy;
    import br.com.tratomais.einsurance.customer.model.proxy.CityProxy;
    import br.com.tratomais.einsurance.customer.model.proxy.CountryProxy;
    import br.com.tratomais.einsurance.customer.model.proxy.DataOptionProxy;
    import br.com.tratomais.einsurance.customer.model.proxy.PartnerProxy;
    import br.com.tratomais.einsurance.customer.model.proxy.StateProxy;
    import br.com.tratomais.einsurance.customer.model.proxy.SubsidiaryProxy;
    import br.com.tratomais.einsurance.policy.model.proxy.MasterPolicyProxy;
    import br.com.tratomais.einsurance.products.model.proxy.ProductProxy;
    import br.com.tratomais.einsurance.report.model.proxy.CollectionMainProxy;
    import br.com.tratomais.einsurance.report.model.proxy.InterfaceMainProxy;
    import br.com.tratomais.einsurance.report.model.proxy.ReportProxy;
    import br.com.tratomais.einsurance.sales.model.proxy.CalculateProxy;
    import br.com.tratomais.einsurance.sales.model.proxy.ProposalProxy;
    import br.com.tratomais.einsurance.search.model.proxy.SearchDocumentProxy;
    import br.com.tratomais.einsurance.support.model.proxy.SupportPolicyProxy;
    import br.com.tratomais.einsurance.useradm.model.proxy.FunctionalityProxy;
    import br.com.tratomais.einsurance.useradm.model.proxy.LoginProxy;
    import br.com.tratomais.einsurance.useradm.model.proxy.ModuleProxy;
    import br.com.tratomais.einsurance.useradm.model.proxy.RoleProxy;
    import br.com.tratomais.einsurance.useradm.model.proxy.UserProxy;
    import br.com.tratomais.einsurance.useradm.model.vo.Partner;
    import br.com.tratomais.einsurance.useradm.model.vo.User;
    import br.com.tratomais.einsurance.useradm.model.vo.UserPartner;
    
    import flash.events.Event;
    
    import mx.collections.ArrayCollection;
    
    import org.puremvc.as3.interfaces.IMediator;
        
      /**
     * A concrete <code>Facade</code> for the <code>eInsurance</code> application.
     * <P>
     * The main job of the <code>ApplicationFacade</code> is to act as a single 
     * place for mediators, proxies and commands to access and communicate
     * with each other without having to interact with the Model, View, and
     * Controller classes directly. All this capability it inherits from 
     * the PureMVC Facade class.</P>
     * 
     * <P>
     * This concrete Facade subclass is also a central place to define 
     * notification constants which will be shared among commands, proxies and
     * mediators, as well as initializing the controller with Command to 
     * Notification mappings.</P>
     */
    public class ApplicationFacade extends SwitchFacade  
    {  
    	private var appFacade:einsurance;
    	private var _loggedUser:User;

        /**
         * Singleton ApplicationFacade Factory Method
         * @return instancia do objeto ApplicationFacade
         */
 	    public static function getInstance() : ApplicationFacade {  
            if (instance == null){
            	instance = new ApplicationFacade();  
            } 
            return instance as ApplicationFacade;  
        }         
          
        /**
         * Register Commands with the Controller 
         */
        override protected function initializeController():void {  
            super.initializeController();  
            registerCommand( NotificationList.STARTUP, ApplicationStartupCommand );
            registerCommand( NotificationList.SHUTDOWN, ApplicationShutdownCommand );
        }  
  
  		/**
  		 * Inicializa a applicação enviando uma notificação do tipo
  		 * NotificationList.STARTUP para o ApplicationStartupCommand
  		 * 
  		 * @param einsurance - Applicação
  		 */
        public function startup( app:einsurance ):void {      
        	appFacade = app;
        	sendNotification(NotificationList.STARTUP, app);
        }  
        
        /**
        * Recupera a instancia da aplicação
        * @return einsurance
        */        
        public function get getEinsurance(): einsurance {
            return appFacade;
        }
        
        /**
        * Seta o valor para o log de auditoria
        * @param msg - mensagem que será logada.
        */
        public function set auditLogger(msg:String):void{
        	var loginProxy:LoginProxy = LoginProxy(this.retrieveProxy(LoginProxy.NAME));
        	loginProxy.auditLogger(msg);
    	}
    
        public function get loggedUser() : User{
        	return this._loggedUser;
        }
        
        public function set loggedUser(user : User) : void{
        	this._loggedUser = user;
        }
       
        public function changePartnerDefaultLoggedUser(partnerId : int) : void {
        	loggedUser.transientSelectedPartner = partnerId;
        	var userProxy:UserProxy = UserProxy(this.retrieveProxy(UserProxy.NAME));
        	userProxy.changePartnerDefaultLoggedUser(partnerId);
        }
        
        public function registerOnlyNewMediator(mediator : IMediator) : Boolean{
        	if(this.hasMediator(mediator.getMediatorName()) == false){
        		this.registerMediator(mediator);
        		return true;
        	}else{
        		return false;
        	}
        }
       
        private function removeAllProxy(){
         	this.removeProxy(DataOptionProxy.NAME);
			this.removeProxy(LoginProxy.NAME);
			this.removeProxy(UserProxy.NAME);
			this.removeProxy(ModuleProxy.NAME);
			this.removeProxy(FunctionalityProxy.NAME);
			this.removeProxy(BrokerProxy.NAME);
			this.removeProxy(RoleProxy.NAME);
			this.removeProxy(ChannelProxy.NAME);
			this.removeProxy(CountryProxy.NAME);
			this.removeProxy(CityProxy.NAME);
			this.removeProxy(StateProxy.NAME);
			this.removeProxy(ProductProxy.NAME);
			this.removeProxy(ProposalProxy.NAME);
			this.removeProxy(SubsidiaryProxy.NAME);
			this.removeProxy(PartnerProxy.NAME);
			this.removeProxy(CalculateProxy.NAME);
			this.removeProxy(MasterPolicyProxy.NAME);
			this.removeProxy(SearchDocumentProxy.NAME);
			this.removeProxy(ReportProxy.NAME);
			this.removeProxy(SupportPolicyProxy.NAME);
			this.removeProxy(CollectionMainProxy.NAME);
			this.removeProxy(InterfaceMainProxy.NAME);
         }

        private function removeAllMediator(){
/*          	this.removeMediator(BrokerMainMediator.NAME);
			this.removeMediator(ChannelMainMediator.NAME);
			this.removeMediator(PartnerMainMediator.NAME);
			this.removeMediator(SubsidiaryMainMediator.NAME);
			this.removeMediator(MasterPolicyMainMediator.NAME);
			this.removeMediator(ProductsMediator.NAME);
			this.removeMediator(ProposalMediator.NAME);
			this.removeMediator(QuoteMainMediator.NAME);
			this.removeMediator(RolesMainMediator.NAME);
			this.removeMediator(UserMainMediator.NAME);
			this.removeMediator(UserLoginMediator.NAME);
			this.removeMediator(UserPartnerFormMediator.NAME);
			this.removeMediator(UserRolesMediator.NAME);
			this.removeMediator(UserFormMediator.NAME);
			this.removeMediator(RolesTreeMediator.NAME);
			this.removeMediator(RolesFormMediator.NAME);
			this.removeMediator(CertificateReportMainMediator.NAME);
 */	        }

        public function logout (event:Event, logoutInServer : Boolean) : void{
			
  			if(logoutInServer){
  				(LoginProxy (retrieveProxy(LoginProxy.NAME))).logout();
  			} 
			//instance=null;
  			
  			getEinsurance.desktop.visible = false;
			getEinsurance.desktop.menuBarCollection = null;
			//getEinsurance.desktop.partnerList.dataProvider = null;
			//getEinsurance.desktop.menu.dataProvider = null;
			
			getEinsurance.desktop.user.text = "";
			getEinsurance.login.visible = true;
			getEinsurance.login.txtUser.text = "";
			getEinsurance.login.txtPassword.text = "";
			getEinsurance.login.txtUser.errorString = "";
			getEinsurance.login.txtPassword.errorString = "";
			//sendNotification(NotificationList.STARTUP, appFacade);

  			loggedUser = null;

         	this.removeAllProxy();
        	this.removeAllMediator();

			getEinsurance.iniApp(event);
        }
        
        /**
        * 
        * @return return the selected partner of user
        * @param not found
        * 
        * **/
   		public function getSelectedPartner() : Partner{
			var userPartners : ArrayCollection = loggedUser.userPartners;
			if(userPartners != null){
				for each(var userPartner : UserPartner in userPartners){
					if(userPartner.partner.partnerId == loggedUser.transientSelectedPartner){
						return userPartner.partner;
					}
				}
			}
			return null;
		}
    }  
}