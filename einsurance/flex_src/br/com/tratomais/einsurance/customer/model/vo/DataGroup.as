package br.com.tratomais.einsurance.customer.model.vo
{
/* 	import mx.collections.ArrayCollection;
 */	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.DataGroup")]      
    [Managed]  
    public class DataGroup extends HibernateBean 
    {  
        private var _dataGroupId:int;  
        private var _parent:DataGroup;  
        private var _fieldName:String;  
        private var _description:String;  
        private var _registred:Date;  
        private var _updated:Date;  
/*         private var _dataOptions:ArrayCollection;  
        private var _dataGroups:ArrayCollection;  
        private var _attributes:ArrayCollection;  
 */  
 		public static const ISSUANCE_TYPE = 3
 		public static const CANCEL_REASON = 87

        public function DataGroup()  
        {  
              
        }  
  
        public function get dataGroupId():int{  
            return _dataGroupId;  
        }  
  
        public function set dataGroupId(pData:int):void{  
            _dataGroupId=pData;  
        }  
  
        public function get parent():DataGroup{  
            return _parent;  
        }  
  
        public function set parent(pData:DataGroup):void{  
            _parent=pData;  
        }  
  
        public function get fieldName():String{  
            return _fieldName;  
        }  
  
        public function set fieldName(pData:String):void{  
            _fieldName=pData;  
        }  
  
        public function get description():String{  
            return _description;  
        }  
  
        public function set description(pData:String):void{  
            _description=pData;  
        }  
  
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
  
/*         public function get dataOptions():ArrayCollection{  
            return _dataOptions;  
        }  
  
        public function set dataOptions(pData:ArrayCollection):void{  
            _dataOptions=pData;  
        }  
  
        public function get dataGroups():ArrayCollection{  
            return _dataGroups;  
        }  
  
        public function set dataGroups(pData:ArrayCollection):void{  
            _dataGroups=pData;  
        }  
  
        public function get attributes():ArrayCollection{  
            return _attributes;  
        }  
  
        public function set attributes(pData:ArrayCollection):void{  
            _attributes=pData;  
        }  
 */    }  
}  