package br.com.tratomais.einsurance.customer.model.proxy
{
	
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.customer.business.SubsidiaryDelegateProxy;
	import br.com.tratomais.einsurance.customer.model.vo.Subsidiary;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;

	[Bindable]
	public class SubsidiaryProxy extends Proxy implements IProxy 
	{
	
		public static const NAME:String = "SubsidiaryProxy";
//		public var subsidiaryCollection:ArrayCollection = new ArrayCollection(); 


		public function SubsidiaryProxy(data:Object = null)
		{
			super(NAME, data );
		}

		public function saveObject( subsidiary:Subsidiary ) : void{
			var delegate:SubsidiaryDelegateProxy = new SubsidiaryDelegateProxy(new Responder(onResult, onFault));
			delegate.saveObject( subsidiary );
		}
		
		private function onResult(pResultEvt:ResultEvent):void{			
            var subsidiary:Subsidiary =pResultEvt.result as Subsidiary ;
            sendNotification(NotificationList.SUBSIDIARY_SAVE_SUCCESS, subsidiary);
		}

		
		public function listAllSubsidiary():void
		{
			var delegate:SubsidiaryDelegateProxy = new SubsidiaryDelegateProxy(new Responder(onListAllResult, onFault));
			delegate.listAllSubsidiary();
		}

		private function onListAllResult(pResultEvt:ResultEvent):void
		{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.SUBSIDIARY_LIST_REFRESH, result);
		}
		
		/**
		 * Checks if external code exists for Insurer/Subsidiary 
		 * @param insurerId Insures's record id
		 * @param subsidiaryId subsidiary's record id
		 * @param externalCode External code to be checked
		 */
		public function subsidiaryExternalCodeExists(insurerId: int, subsidiaryId: int, externalCode:String):void {
			var delegate:SubsidiaryDelegateProxy = new SubsidiaryDelegateProxy(new Responder(onSubsidiaryExternalCodeExists, onFault));
			delegate.subsidiaryExternalCodeExists(insurerId,subsidiaryId,externalCode);
		}
		
		private function onSubsidiaryExternalCodeExists(pResultEvt:ResultEvent):void {
			var result:Boolean = pResultEvt.result as Boolean ;
         	sendNotification(NotificationList.EXTERNAL_CODE_SUBSIDIARY_EXISTS_CHECK, result);
		}

		public function listAllActiveSubsidiary():void
		{
			var delegate:SubsidiaryDelegateProxy = new SubsidiaryDelegateProxy(new Responder(onLlistAllActiveSubsidiaryResult, onFault));
			delegate.listAllActiveSubsidiary();
		}

		private function onLlistAllActiveSubsidiaryResult(pResultEvt:ResultEvent):void
		{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.SUBSIDIARY_LIST_REFRESH, result);
		}

		private function onFault(pFaultEvt:FaultEvent):void{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
		}					
	}
}