package br.com.tratomais.einsurance.customer.model.proxy
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.customer.business.FindCepDelegateProxy;
	import br.com.tratomais.einsurance.customer.model.vo.ZipCodeAddress;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;

	[Bindable]	
	public class FindCepProxy extends Proxy implements IProxy 
	{
		public static const NAME:String = "FindCepProxy";
		
		[Embed(source="/assets/images/error.png")]
		public var errorMsg:Class;
		
		public function FindCepProxy(data:Object = null)
		{
			super(NAME, data );
		}
		
		public function identifiedListFindCep(identifiedList:IdentifiedList, zipCode:String):void
		{
			var delegate:FindCepDelegateProxy = new FindCepDelegateProxy(new Responder(onResult, onFault));
			delegate.identifiedListFindCep(identifiedList, zipCode);
		}

		private function onResult(pResultEvt:ResultEvent):void
		{
			var result:IdentifiedList=pResultEvt.result as IdentifiedList ;
         	sendNotification(NotificationList.IDENTIFIED_CEP_FOUND, result);
		}
		
		private function onFault(pFaultEvt:FaultEvent):void{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
		}
	}
}
