package br.com.tratomais.einsurance.customer.model.proxy
{  
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.customer.business.ChannelDelegateProxy;
    import br.com.tratomais.einsurance.customer.business.PartnerDelegateProxy;
    import br.com.tratomais.einsurance.customer.model.vo.Channel;
    import br.com.tratomais.einsurance.useradm.model.vo.Partner;
    import br.com.tratomais.einsurance.useradm.model.vo.User;
    
    import mx.collections.ArrayCollection;
    import mx.rpc.Responder;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    
    import org.puremvc.as3.interfaces.IProxy;
    import org.puremvc.as3.patterns.proxy.Proxy;         
      
    [Bindable]  
    public class ChannelProxy extends Proxy implements IProxy   
    {  
      
        public static const NAME:String = "ChannelProxy";  
  		
		[Embed(source="/assets/images/error.png")]
		public var errorMsg:Class;    
  
        public function ChannelProxy(data:Object=null)  
        {  
            super(NAME, data );  
        }  
  		
		public function listChannel(partnerId:int, onlyActive:Boolean):void
		{
			var delegate:ChannelDelegateProxy = new ChannelDelegateProxy(new Responder(onListChannelResult, onFault));
			delegate.findChannelByPartnerId(partnerId, onlyActive);
		}

		private function onListChannelResult(pResultEvt:ResultEvent):void
		{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.CHANNEL_LIST_LISTED, result);
		}
		
		public function listAll():void
		{
			var delegate:ChannelDelegateProxy = new ChannelDelegateProxy(new Responder(onListAllResult, onFault));
			delegate.listAll();
		}

		private function onListAllResult(pResultEvt:ResultEvent):void
		{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.CHANNEL_LIST_LISTED, result);
		}
		
		public function findByNameAndPartne(name:String, partner:Partner):void{
			var delegate:ChannelDelegateProxy = new ChannelDelegateProxy(new Responder(onResultFindByName, onFault));
			delegate.findByNameAndPartner( name, partner );
		
		}
		
		public function onResultFindByName(pResultEvt:ResultEvent):void{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.CHANNEL_LIST_LISTED, result);
		} 
		

		public function deleteObject(channel:Channel):void{
			var delegate:ChannelDelegateProxy = new ChannelDelegateProxy(new Responder(onResultDelete, onFault));
			delegate.deleteObject( channel );
		}
		
		public function onResultDelete( pResultEvt:ResultEvent ):void{			
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.CHANNEL_DELETE_SUCCESS, result);			
						
		}

		public function saveObject( channel:Channel ) : void{
			var delegate:ChannelDelegateProxy = new ChannelDelegateProxy(new Responder(onResult, onFault));
			delegate.saveObject( channel );
		}
		
		private function onResult(pResultEvt:ResultEvent):void{			
            var channel:Channel =pResultEvt.result as Channel ;
            sendNotification(NotificationList.CHANNEL_ADD_SUCCESS);
		}		  		
       
		public function listPartner(user:User):void{
			var delegate:PartnerDelegateProxy = new PartnerDelegateProxy(new Responder( onListPartnerResult, onFault));
			delegate.listPartnersAll( user );
		}
		
		public function onListPartnerResult(pResultEvt:ResultEvent):void{
			var list:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.CHANNEL_PARTNER_LIST_LISTED, list);			
		}       
        
		public function listAllActivePartner(user:User):void{
			var delegate:PartnerDelegateProxy = new PartnerDelegateProxy(new Responder( onListAllActivePartner, onFault));
			delegate.listAllActivePartner( user );
		}
		
		public function onListAllActivePartner(pResultEvt:ResultEvent):void{
			var list:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.CHANNEL_PARTNER_LIST_LISTED, list);			
		}  
		
		public function listUserByPartnerId(partnerId : int):void{
			var delegate:ChannelDelegateProxy = new ChannelDelegateProxy(new Responder( onListUserByPartnerId, onFault));
			delegate.listUserByPartnerId( partnerId );
		}
		
		public function onListUserByPartnerId(pResultEvt:ResultEvent):void{
			var list:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.CHANNEL_USER_LIST_REFRESH, list);			
		}    
		
		public function fillListUserByChannel(channel : Channel):void{
			var delegate:ChannelDelegateProxy = new ChannelDelegateProxy(new Responder( onFillListUserByChannel, onFault));
			delegate.fillListUserByChannel( channel );
		}
		
		public function onFillListUserByChannel(pResultEvt:ResultEvent):void{
			var list:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.CHANNEL_USER_LIST_REFRESH, list);			
		}

		public function findByLoggedUserAndPartner(partner:int):void{
			var delegate:ChannelDelegateProxy = new ChannelDelegateProxy(new Responder( onFindByLoggedUserAndPartner, onFault));
			delegate.findByLoggedUserAndPartner( partner );
		}
		
		public function onFindByLoggedUserAndPartner(pResultEvt:ResultEvent):void{
			var list:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.CHANNEL_LIST_USER_PARTNER, list);			
		}
		    
		private function onFault(pFaultEvt:FaultEvent):void{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
		}
    }  
}  