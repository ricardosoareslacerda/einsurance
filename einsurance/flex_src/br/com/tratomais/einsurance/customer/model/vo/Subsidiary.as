package br.com.tratomais.einsurance.customer.model.vo  
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	 
    [RemoteClass(alias="br.com.tratomais.core.model.customer.Subsidiary")]    
    [Managed]  
    public class Subsidiary extends HibernateBean  
    {  
  
        private var _subsidiaryId:int;  
        private var _insurerId:int;  
        private var _name:String;  
        private var _nickName:String;  
        private var _externalCode:String;  
        private var _addressStreet:String;  
        private var _districtName:String;  
        private var _cityId:String;  
        private var _cityName:String;  
        private var _regionName:String;  
        private var _stateId:String;  
        private var _stateName:String;  
        private var _zipCode:String;  
        private var _countryId:String;  
        private var _countryName:String;  
        private var _emailAddress:String;  
        private var _phoneNumber:String;  
        private var _faxNumber:String;  
        private var _channels:ArrayCollection;  
  		private var _active:Boolean;
  
        public function Subsidiary()  
        {  
              
        }  
  
        public function get subsidiaryId():int{  
            return _subsidiaryId;  
        }  
  
        public function set subsidiaryId(pData:int):void{  
            _subsidiaryId=pData;  
        }  
  
        public function get insurerId():int{  
            return _insurerId;  
        }  
  
        public function set insurerId(pData:int):void{  
            _insurerId=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get nickName():String{  
            return _nickName;  
        }  
  
        public function set nickName(pData:String):void{  
            _nickName=pData;  
        }  
  
        public function get externalCode():String{  
            return _externalCode;  
        }  
  
        public function set externalCode(pData:String):void{  
            _externalCode=pData;  
        }  
  
        public function get addressStreet():String{  
            return _addressStreet;  
        }  
  
        public function set addressStreet(pData:String):void{  
            _addressStreet=pData;  
        }  
  
        public function get districtName():String{  
            return _districtName;  
        }  
  
        public function set districtName(pData:String):void{  
            _districtName=pData;  
        }  
  
        public function get cityId():String{  
            return _cityId;  
        }  
  
        public function set cityId(pData:String):void{  
            _cityId=pData;  
        }  
  
        public function get cityName():String{  
            return _cityName;  
        }  
  
        public function set cityName(pData:String):void{  
            _cityName=pData;  
        }  
  
        public function get regionName():String{  
            return _regionName;  
        }  
  
        public function set regionName(pData:String):void{  
            _regionName=pData;  
        }  
  
        public function get stateId():String{  
            return _stateId;  
        }  
  
        public function set stateId(pData:String):void{  
            _stateId=pData;  
        }  
  
        public function get stateName():String{  
            return _stateName;  
        }  
  
        public function set stateName(pData:String):void{  
            _stateName=pData;  
        }  
  
        public function get zipCode():String{  
            return _zipCode;  
        }  
  
        public function set zipCode(pData:String):void{  
            _zipCode=pData;  
        }  
  
        public function get countryId():String{  
            return _countryId;  
        }  
  
        public function set countryId(pData:String):void{  
            _countryId=pData;  
        }  
  
        public function get countryName():String{  
            return _countryName;  
        }  
  
        public function set countryName(pData:String):void{  
            _countryName=pData;  
        }  
  
        public function get emailAddress():String{  
            return _emailAddress;  
        }  
  
        public function set emailAddress(pData:String):void{  
            _emailAddress=pData;  
        }  
  
        public function get phoneNumber():String{  
            return _phoneNumber;  
        }  
  
        public function set phoneNumber(pData:String):void{  
            _phoneNumber=pData;  
        }  
  
        public function get faxNumber():String{  
            return _faxNumber;  
        }  
  
        public function set faxNumber(pData:String):void{  
            _faxNumber=pData;  
        }  
  
        public function get channels():ArrayCollection{  
            return _channels;  
        }  
  
        public function set channels(pData:ArrayCollection):void{  
            _channels=pData;  
        }
        
        public function get active():Boolean{
        	return _active;
        }
        
        public function set active(value:Boolean):void{
        	_active=value;
        }
    }  
}