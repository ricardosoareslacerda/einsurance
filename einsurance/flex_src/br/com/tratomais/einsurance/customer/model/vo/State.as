package br.com.tratomais.einsurance.customer.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.State")]      
	[Managed]
    public class State extends HibernateBean  
    {  
        private var _stateId:int;  
        private var _countryId:int;  
        private var _name:String;  
        private var _externalCode:String;  
		private var _localityCode:Number;
   
        public function State()  
        {  
        }  
  
        public function get stateId():int{  
            return _stateId;  
        }  
  
        public function set stateId(pData:int):void{  
            _stateId=pData;  
        }  
  
        public function get countryId():int{  
            return _countryId;  
        }  
  
        public function set countryId(pData:int):void{  
            _countryId=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get externalCode():String{  
            return _externalCode;  
        }  
  
        public function set externalCode(pData:String):void{  
            _externalCode=pData;  
        }
                  
		public function get localityCode():Number {
			return _localityCode;
		}
	
		public function set localityCode(pData:Number):void {
			_localityCode = pData;
		}

    }  
}  