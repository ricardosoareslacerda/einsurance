package br.com.tratomais.einsurance.customer.model.proxy
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.customer.business.StateDelegateProxy;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;

	[Bindable]	
	public class StateProxy extends Proxy implements IProxy 
	{
		public static const NAME:String = "StateProxy";
		
		[Embed(source="/assets/images/error.png")]
		public var errorMsg:Class;
		
		public function StateProxy(data:Object = null)
		{
			super(NAME, data );
		}
		
		public function listAll():void
		{
			var delegate:StateDelegateProxy = new StateDelegateProxy(new Responder(onListAllResult, onFault));
			delegate.listAll();
		}

		private function onListAllResult(pResultEvt:ResultEvent):void
		{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.STATE_LISTED, result);
		}
        
		public function listStateByCountry(identifiedList:IdentifiedList, countryId:int):void
		{
			var delegate:StateDelegateProxy = new StateDelegateProxy(new Responder(onListStateByCountryResult, onFault));
			delegate.listStateByCountry(identifiedList, countryId);
		}

		private function onListStateByCountryResult(pResultEvt:ResultEvent):void
		{
			var identifiedList:IdentifiedList=pResultEvt.result as IdentifiedList;
         	sendNotification(NotificationList.STATE_LISTED, identifiedList);
		}
		
		private function onFault(pFaultEvt:FaultEvent):void{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
		}
	}
}