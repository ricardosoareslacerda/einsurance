package br.com.tratomais.einsurance.customer.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;

    [RemoteClass(alias="br.com.tratomais.core.model.customer.Contact")]    
    [Managed]  
    public class Contact extends HibernateBean 
    {  
        private var _contactId:int;  
        private var _customer:Customer;  
        private var _contactType:int;  
        private var _name:String;  
        private var _lastName:String;  
        private var _firstName:String;  
        private var _title:String;  
        private var _documentType:int;  
        private var _documentNumber:String;  
        private var _documentEmitter:String;  
        private var _documentValidity:Date;  
        private var _emailAddress:String;  
        private var _phoneNumber:String;  
        private var _faxNumber:String;  
        private var _active:Boolean;  
  
        public function Contact()  
        {  
        }  
  
        public function get contactId():int{  
            return _contactId;  
        }  
  
        public function set contactId(pData:int):void{  
            _contactId=pData;  
        }  
  
        public function get customer():Customer{  
            return _customer;  
        }  
  
        public function set customer(pData:Customer):void{  
            _customer=pData;  
        }  
  
        public function get contactType():int{  
            return _contactType;  
        }  
  
        public function set contactType(pData:int):void{  
            _contactType=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get lastName():String{  
            return _lastName;  
        }  
  
        public function set lastName(pData:String):void{  
            _lastName=pData;  
        }  
  
        public function get firstName():String{  
            return _firstName;  
        }  
  
        public function set firstName(pData:String):void{  
            _firstName=pData;  
        }  
  
        public function get title():String{  
            return _title;  
        }  
  
        public function set title(pData:String):void{  
            _title=pData;  
        }  
  
        public function get documentType():int{  
            return _documentType;  
        }  
  
        public function set documentType(pData:int):void{  
            _documentType=pData;  
        }  
  
        public function get documentNumber():String{  
            return _documentNumber;  
        }  
  
        public function set documentNumber(pData:String):void{  
            _documentNumber=pData;  
        }  
  
        public function get documentEmitter():String{  
            return _documentEmitter;  
        }  
  
        public function set documentEmitter(pData:String):void{  
            _documentEmitter=pData;  
        }  
  
        public function get documentValidity():Date{  
            return _documentValidity;  
        }  
  
        public function set documentValidity(pData:Date):void{  
            _documentValidity=pData;  
        }  
  
        public function get emailAddress():String{  
            return _emailAddress;  
        }  
  
        public function set emailAddress(pData:String):void{  
            _emailAddress=pData;  
        }  
  
        public function get phoneNumber():String{  
            return _phoneNumber;  
        }  
  
        public function set phoneNumber(pData:String):void{  
            _phoneNumber=pData;  
        }  
  
        public function get faxNumber():String{  
            return _faxNumber;  
        }  
  
        public function set faxNumber(pData:String):void{  
            _faxNumber=pData;  
        }  
  
        public function get active():Boolean{  
            return _active;  
        }  
  
        public function set active(pData:Boolean):void{  
            _active=pData;  
        }  
    }  
}