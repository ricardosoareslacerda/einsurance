package br.com.tratomais.einsurance.customer.model.vo
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.customer.Address")]   
    [Managed]  
    public class Address extends HibernateBean  
    {  
        private var _addressId:int;  
        private var _customer:Customer;  
        private var _addressType:int;  
        private var _addressStreet:String;  
        private var _districtName:String;  
        private var _cityId:int;  
        private var _cityName:String;  
        private var _regionName:String;  
        private var _stateId:int;  
        private var _stateName:String;  
        private var _zipCode:String;  
        private var _countryId:int;  
        private var _countryName:String;  
        private var _active:Boolean;
        private var _phones:ArrayCollection = new ArrayCollection();  
  
        public function Address()  
        {  
        }  
        
        public function get addressId():int{  
            return _addressId;  
        }  
  
        public function set addressId(pData:int):void{  
            _addressId=pData;  
        }  
  
        public function get customer():Customer{  
            return _customer;  
        }  
  
        public function set customer(pData:Customer):void{  
            _customer=pData;  
        }  
  
        public function get addressType():int{  
            return _addressType;  
        }  
  
        public function set addressType(pData:int):void{  
            _addressType=pData;  
        }  
  
        public function get addressStreet():String{  
            return _addressStreet;  
        }  
  
        public function set addressStreet(pData:String):void{  
            _addressStreet=pData;  
        }  
  
        public function get districtName():String{  
            return _districtName;  
        }  
  
        public function set districtName(pData:String):void{  
            _districtName=pData;  
        }  
  
        public function get cityId():int{  
            return _cityId;  
        }  
  
        public function set cityId(pData:int):void{  
            _cityId=pData;  
        }  
  
        public function get cityName():String{  
            return _cityName;  
        }  
  
        public function set cityName(pData:String):void{  
            _cityName=pData;  
        }  
  
        public function get regionName():String{  
            return _regionName;  
        }  
  
        public function set regionName(pData:String):void{  
            _regionName=pData;  
        }  
  
        public function get stateId():int{  
            return _stateId;  
        }  
  
        public function set stateId(pData:int):void{  
            _stateId=pData;  
        }  
  
        public function get stateName():String{  
            return _stateName;  
        }  
  
        public function set stateName(pData:String):void{  
            _stateName=pData;  
        }  
  
        public function get zipCode():String{  
            return _zipCode;  
        }  
  
        public function set zipCode(pData:String):void{  
            _zipCode=pData;  
        }  
  
        public function get countryId():int{  
            return _countryId;  
        }  
  
        public function set countryId(pData:int):void{  
            _countryId=pData;  
        }  
  
        public function get countryName():String{  
            return _countryName;  
        }  
  
        public function set countryName(pData:String):void{  
            _countryName=pData;  
        }  
  
        public function get active(): Boolean { 
        	return _active;
        }
        
        public function set active(pDados: Boolean): void {
        	this._active = pDados;
        }

        public function get phones():ArrayCollection{  
            return _phones;  
        }  
  
        public function set phones(pData:ArrayCollection):void{  
            _phones=pData;  
        }  
 
		public function addPhone() : Phone{
			var newPhone : Phone = new Phone();
			newPhone.address = this;
			if(phones == null){
		   		phones = new ArrayCollection();
			}
			phones.addItem(newPhone);
			return newPhone;
		}
  

    }  
}