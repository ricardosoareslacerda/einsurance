package br.com.tratomais.einsurance.customer.model.vo  
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.DataOption")]     
    [Managed]   
    public class DataOption extends HibernateBean 
    { 
		/**
		 * Kinship Type
		 */
		public static const KINSHIP_TYPE_OTHER:int = 145;	// Outros
		public static const KINSHIP_TYPE_SELF:int = 294;	// Próprio

		/**
		 * Person Type
		 */
		public static const PERSON_TYPE_NATURAL:int = 63;	// Física
		public static const PERSON_TYPE_LEGALLY:int = 64;	// Jurídica

		/**
		 * Document Type
		 */
		public static const DOCUMENT_TYPE_RG:int = 75;	// RG
		public static const DOCUMENT_TYPE_CPF:int = 76;	// CPF

        private var _dataOptionId:int;  
        private var _parent:DataOption; 
        private var _related:DataOption; 
        private var _dataGroup:DataGroup; 
        private var _effectiveDate:Date;  
        private var _expiryDate:Date;  
        private var _fieldValue:String;  
        private var _fieldDescription:String;  
        private var _fieldExclusive:Boolean;  
        private var _externalCode:String;  
        private var _description:String;  
        private var _displayOrder:int;  
        private var _registred:Date;  
        private var _updated:Date;  
        private var _active:Boolean;
        private var _dataOptions:ArrayCollection;  
 
        public function DataOption() {
        	
        }  
  
        public function get dataOptionId():int{  
            return _dataOptionId;  
        }  
  
        public function set dataOptionId(pData:int):void{  
            _dataOptionId=pData;  
        }  
  
        public function get parent():DataOption{  
            return _parent;  
        }  
  
        public function set parent(pData:DataOption):void{  
            _parent=pData;  
        }  

        public function get related():DataOption{  
            return _related;  
        }  
  
        public function set related(pData:DataOption):void{  
            _related=pData;  
        }
        
        public function get dataGroup():DataGroup{  
            return _dataGroup;  
        }  
  
        public function set dataGroup(pData:DataGroup):void{  
            _dataGroup=pData;  
        }
          
        public function get effectiveDate():Date{  
            return _effectiveDate;  
        }  
  
        public function set effectiveDate(pData:Date):void{  
            _effectiveDate=pData;  
        }  
  
        public function get expiryDate():Date{  
            return _expiryDate;  
        }  
  
        public function set expiryDate(pData:Date):void{  
            _expiryDate=pData;  
        }  
  
        public function get fieldValue():String{  
            return _fieldValue;  
        }  
  
        public function set fieldValue(pData:String):void{  
            _fieldValue=pData;  
        }  
  
        public function get fieldDescription():String{  
            return _fieldDescription;  
        }  
  
        public function set fieldDescription(pData:String):void{  
            _fieldDescription=pData;  
        }  
  
        public function get fieldExclusive():Boolean{  
            return _fieldExclusive;  
        }  
  
        public function set fieldExclusive(pData:Boolean):void{  
            _fieldExclusive=pData;  
        }  
  
        public function get externalCode():String{  
            return _externalCode;  
        }  
  
        public function set externalCode(pData:String):void{  
            _externalCode=pData;  
        }  
  
        public function get description():String{  
            return _description;  
        }  
  
        public function set description(pData:String):void{  
            _description=pData;  
        }  
  
        public function get displayOrder():int{  
            return _displayOrder;  
        }  
  
        public function set displayOrder(pData:int):void{  
            _displayOrder=pData;  
        }  
  
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
  
        public function get active(): Boolean { 
        	return _active;
        }
        
        public function set active(pActive: Boolean): void {
        	this._active = pActive;
        }
        
 		public function get dataOptions():ArrayCollection{  
            return _dataOptions;  
        }  
  
        public function set dataOptions(pData:ArrayCollection):void{  
            _dataOptions=pData;  
        }  
        
       
    
     }  
} 