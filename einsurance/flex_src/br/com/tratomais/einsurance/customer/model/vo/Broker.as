package br.com.tratomais.einsurance.customer.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;

    [RemoteClass(alias="br.com.tratomais.core.model.customer.Broker")]     
 	[Managed]
	public class Broker extends HibernateBean
   {
        private var _brokerId:int;  
        private var _insurerId:int;  
        private var _name:String;  
        private var _nickName:String;  
        private var _brokerType:int;  
        private var _brokerRating:String;  
        private var _regulatoryCode:String;  
        private var _externalCode:String;  
        private var _documentType:String;  
        private var _documentNumber:String;  
        private var _documentIssuer:String;  
        private var _documentValidity:Date;  
        private var _addressStreet:String;  
        private var _districtName:String;  
        private var _cityId:String;  
        private var _cityName:String;  
        private var _regionName:String;  
        private var _stateId:String;  
        private var _stateName:String;  
        private var _zipCode:String;  
        private var _countryId:String;  
        private var _countryName:String;  
        private var _emailAddress:String;  
        private var _phoneNumber:String;  
        private var _faxNumber:String; 
        private var _active:Boolean=true;  
  
        public function Broker()  
        {  
        }  
  
        public function get brokerId():int{  
            return _brokerId;  
        }  
  
        public function set brokerId(pData:int):void{  
            _brokerId=pData;  
        }  
  
        public function get insurerId():int{  
            return _insurerId;  
        }  
  
        public function set insurerId(pData:int):void{  
            _insurerId=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get nickName():String{  
            return _nickName;  
        }  
  
        public function set nickName(pData:String):void{  
            _nickName=pData;  
        }  
  
        public function get brokerType():int{  
            return _brokerType;  
        }  
  
        public function set brokerType(pData:int):void{  
            _brokerType=pData;  
        }  
  
        public function get brokerRating():String{  
            return _brokerRating;  
        }  
  
        public function set brokerRating(pData:String):void{  
            _brokerRating=pData;  
        }  
  
        public function get regulatoryCode():String{  
            return _regulatoryCode;  
        }  
  
        public function set regulatoryCode(pData:String):void{  
            _regulatoryCode=pData;  
        }  
  
        public function get externalCode():String{  
            return _externalCode;  
        }  
  
        public function set externalCode(pData:String):void{  
            _externalCode=pData;  
        }  
  
        public function get documentType():String{  
            return _documentType;  
        }  
  
        public function set documentType(pData:String):void{  
            _documentType=pData;  
        }  
  
        public function get documentNumber():String{  
            return _documentNumber;  
        }  
  
        public function set documentNumber(pData:String):void{  
            _documentNumber=pData;  
        }  
  
        public function get documentIssuer():String{  
            return _documentIssuer;  
        }  
  
        public function set documentIssuer(pData:String):void{  
            _documentIssuer=pData;  
        }  
  
        public function get documentValidity():Date{  
            return _documentValidity;  
        }  
  
        public function set documentValidity(pData:Date):void{  
            _documentValidity=pData;  
        }  
  
        public function get addressStreet():String{  
            return _addressStreet;  
        }  
  
        public function set addressStreet(pData:String):void{  
            _addressStreet=pData;  
        }  
  
        public function get districtName():String{  
            return _districtName;  
        }  
  
        public function set districtName(pData:String):void{  
            _districtName=pData;  
        }  
  
        public function get cityId():String{  
            return _cityId;  
        }  
  
        public function set cityId(pData:String):void{  
            _cityId=pData;  
        }  
  
        public function get cityName():String{  
            return _cityName;  
        }  
  
        public function set cityName(pData:String):void{  
            _cityName=pData;  
        }  
  
        public function get regionName():String{  
            return _regionName;  
        }  
  
        public function set regionName(pData:String):void{  
            _regionName=pData;  
        }  
  
        public function get stateId():String{  
            return _stateId;  
        }  
  
        public function set stateId(pData:String):void{  
            _stateId=pData;  
        }  
  
        public function get stateName():String{  
            return _stateName;  
        }  
  
        public function set stateName(pData:String):void{  
            _stateName=pData;  
        }  
  
        public function get zipCode():String{  
            return _zipCode;  
        }  
  
        public function set zipCode(pData:String):void{  
            _zipCode=pData;  
        }  
  
        public function get countryId():String{  
            return _countryId;  
        }  
  
        public function set countryId(pData:String):void{  
            _countryId=pData;  
        }  
  
        public function get countryName():String{  
            return _countryName;  
        }  
  
        public function set countryName(pData:String):void{  
            _countryName=pData;  
        }  
  
        public function get emailAddress():String{  
            return _emailAddress;  
        }  
  
        public function set emailAddress(pData:String):void{  
            _emailAddress=pData;  
        }  
  
        public function get phoneNumber():String{  
            return _phoneNumber;  
        }  
  
        public function set phoneNumber(pData:String):void{  
            _phoneNumber=pData;  
        }  
  
        public function get faxNumber():String{  
            return _faxNumber;  
        }  
  
        public function set faxNumber(pData:String):void{  
            _faxNumber=pData;  
        }  

        public function get active():Boolean{  
            return _active;  
        }  
  
        public function set active(pData:Boolean):void{  
            _active=pData;  
        }  

  
    }  
}  