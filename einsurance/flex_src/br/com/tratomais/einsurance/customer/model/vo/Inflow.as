package br.com.tratomais.einsurance.customer.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
    [RemoteClass(alias="br.com.tratomais.core.model.Inflow")]     
	[Managed]
    public class Inflow extends HibernateBean  
    {  
        private var _inflowId:Number;  
        private var _inflowType:String;  
        private var _description:String;  
        private var _inflowOf:Number;  
        private var _inflowUntil:Number;  
		private var _currencyId:Number;
        private var _externalCode:String;  
        private var _displayOrder:Number;  
        private var _active:Boolean;   
  
        public function Inflow()  
        {  
        }  
  
        public function get inflowId():Number{  
            return _inflowId;  
        }  
  
        public function set inflowId(pData:Number):void{  
            _inflowId=pData;  
        }  
  
        public function get inflowType():String{  
            return _inflowType;  
        }  
  
        public function set inflowType(pData:String):void{  
            _inflowType=pData;  
        }  
  
        public function get description():String{  
            return _description;  
        }  
  
        public function set description(pData:String):void{  
            _description=pData;  
        }  
  
        public function get inflowOf():Number{  
            return _inflowOf;  
        }  
  
        public function set inflowOf(pData:Number):void{  
            _inflowOf=pData;  
        }  
  
        public function get inflowUntil():Number{  
            return _inflowUntil;  
        }  
  
        public function set inflowUntil(pData:Number):void{  
            _inflowUntil=pData;  
        }  
  
		public function get currencyId():Number {
			return _currencyId;
		}
		
		public function set currencyId(pData:Number):void {
			_currencyId = pData;
		}
		
        public function get externalCode():String{  
            return _externalCode;  
        }  
  
        public function set externalCode(pData:String):void{  
            _externalCode=pData;  
        }  
  
        public function get displayOrder():Number{  
            return _displayOrder;  
        }  
  
        public function set displayOrder(pData:Number):void{  
            _displayOrder=pData;  
        }  
  
        public function get active():Boolean{  
            return _active;  
        }  
  
        public function set active(pData:Boolean):void{  
            _active=pData;  
        }  
  
    }  
} 