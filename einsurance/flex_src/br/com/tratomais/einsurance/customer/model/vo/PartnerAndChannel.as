package br.com.tratomais.einsurance.customer.model.vo
{
   [RemoteClass(alias="br.com.tratomais.core.model.customer.PartnerAndChannel")]    
   [Managed]  
   public class PartnerAndChannel
	{
		public function PartnerAndChannel()
		{
		}
	  private var _partnerName:String;  
      private var _channelName:String;  

		public function get partnerName():String{
			return _partnerName;
		}

		public function set partnerName(pData:String):void{
			_partnerName=pData;
		}
		public function get channelName():String{
			return _channelName;
		}

		public function set channelName(pData:String):void{
			_channelName=pData;
		}

	}
}