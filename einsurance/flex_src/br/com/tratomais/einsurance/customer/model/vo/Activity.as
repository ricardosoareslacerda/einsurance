package br.com.tratomais.einsurance.customer.model.vo
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
	[RemoteClass(alias="br.com.tratomais.core.model.customer.Activity")]	
	[Managed]
	public class Activity  extends HibernateBean
	{

		private var _activityId:int;
		private var _name:String;
		private var _externalCode:String;
		private var _customers:ArrayCollection;


		public function Activity()
		{
			
		}

		public function get activityId():int{
			return _activityId;
		}

		public function set activityId(pData:int):void{
			_activityId=pData;
		}

		public function get name():String{
			return _name;
		}

		public function set name(pData:String):void{
			_name=pData;
		}

		public function get externalCode():String{
			return _externalCode;
		}

		public function set externalCode(pData:String):void{
			_externalCode=pData;
		}

		public function get customers():ArrayCollection{
			return _customers;
		}

		public function set customers(pData:ArrayCollection):void{
			_customers=pData;
		}
	}
}