package br.com.tratomais.einsurance.customer.model.vo    
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.customer.Profession")]    
    [Managed]  
    public class Profession extends HibernateBean 
    {  
        private var _professionId:int;  
        private var _name:String;  
        private var _externalCode:String;  
        private var _professionRisk:Boolean;  
  
        public function Profession()  
        {  
        }  
  
        public function get professionId():int{  
            return _professionId;  
        }  
  
        public function set professionId(pData:int):void{  
            _professionId=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get externalCode():String{  
            return _externalCode;  
        }  
  
        public function set externalCode(pData:String):void{  
            _externalCode=pData;  
        }  
    
        public function get professionRisk():Boolean{  
            return _professionRisk;  
        }  
  
        public function set professionRisk(pData:Boolean):void{  
            _professionRisk=pData;  
        }  
    }  
}