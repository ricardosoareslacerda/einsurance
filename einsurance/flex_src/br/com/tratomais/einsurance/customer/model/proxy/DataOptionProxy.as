package br.com.tratomais.einsurance.customer.model.proxy
{
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.customer.business.DataOptionDelegateProxy;
    import br.com.tratomais.einsurance.customer.model.vo.DataOption;
    import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
    
    import mx.collections.ArrayCollection;
    import mx.rpc.Responder;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    
    import org.puremvc.as3.interfaces.IProxy;
    import org.puremvc.as3.patterns.proxy.Proxy;  
	
	public class DataOptionProxy extends Proxy implements IProxy 
	{
		public static const NAME:String = "DataGroupProxy";  
		
		[Bindable]		
		[Embed(source="/assets/images/error.png")]
		public var errorMsg:Class;
				
		public function DataOptionProxy(data:Object=null)  
        {  
            super(NAME, data );  
        }  
  		
		public function listDataOption(dataGroupId:int):void
		{
			var delegate:DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder(onListAllResult, onFault));
			delegate.findDataOptionByDataGroup(dataGroupId);
		}
		
		private function onListAllResult(pResultEvt:ResultEvent):void
		{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.DATA_OPTION_LISTED, result);
		}
		
		public function listDataOptionByActive(dataGroupId:int, active:Boolean):void
		{
			var delegate:DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder(onListDataOptionByActive, onFault));
			delegate.findDataOptionByDataGroupByActive(dataGroupId, active);
		}

		private function onListDataOptionByActive(pResultEvt:ResultEvent):void
		{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.DATA_OPTION_LISTED_REPORT, result);
		}

		
		
		public function listIdentifiedDataOption(dataGroupId:int, groupId:String, date:Date = null):void 
		{
			var delegate:DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder(onListAllIdentifiedResult, onFault));
			delegate.findIdentifiedListDataOptionByDataGroup(dataGroupId, groupId, date);
		}

		private function onListAllIdentifiedResult(pResultEvt:ResultEvent):void
		{
			var result:IdentifiedList=pResultEvt.result as IdentifiedList ;
         	sendNotification(NotificationList.DATA_OPTION_IDENT_LISTED, result);
		}
		
		public function getDataOptionById(id : int): void
		{
			var delegate: DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder(onGetDataOptionById, onFault));
			delegate.getDataOptionById(id);
		}
		
		private function onGetDataOptionById (pResultEvent : ResultEvent) :void
		{
			var result : DataOption = pResultEvent.result as DataOption;
			sendNotification(NotificationList.DATA_OPTION_LOADED, result);
		}
								
		public function findIdentifiedListDataOptionByDataGroupParentId(dataGroupId:int, parentId:int, identifierString:String, date:Date = null): void
		{
			var delegate: DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder(onListAllIdentifiedResult, onFault));
			delegate.findIdentifiedListDataOptionByDataGroupParentId(dataGroupId, parentId, identifierString, date);
		}
				
		private function onFault(pFaultEvt:FaultEvent):void{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
		}	

	}
}