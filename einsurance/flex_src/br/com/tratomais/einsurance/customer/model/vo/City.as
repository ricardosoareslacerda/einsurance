package br.com.tratomais.einsurance.customer.model.vo  
{
	import mx.collections.ArrayCollection;
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.City")]   
	[Managed]
    public class City extends HibernateBean  
    {  
        private var _cityId:int;  
        private var _stateId:int;  
        private var _name:String;  
        private var _externalCode:String;  
          
        public function City()  
        {  
        }  
  
        public function get cityId():int{  
            return _cityId;  
        }  
  
        public function set cityId(pData:int):void{  
            _cityId=pData;  
        }  
  
        public function get stateId():int{  
            return _stateId;  
        }  
  
        public function set stateId(pData:int):void{  
            _stateId=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get externalCode():String{  
            return _externalCode;  
        }  
  
        public function set externalCode(pData:String):void{  
            _externalCode=pData;  
        }  
    
    }  
}  