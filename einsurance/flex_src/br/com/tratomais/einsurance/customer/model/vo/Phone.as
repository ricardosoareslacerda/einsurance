package br.com.tratomais.einsurance.customer.model.vo  
{
	import mx.collections.ArrayCollection;
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.customer.Phone")]      
    [Managed]  
    public class Phone extends HibernateBean 
    {  
        private var _phoneId:int;  
        private var _address:Address;  
        private var _phoneType:int;  
        private var _phoneNumber:String;  
        private var _active:Boolean;  
        private var _customers:ArrayCollection;  
  
        public function Phone()  
        {  
        }  
  
        public function get phoneId():int{  
            return _phoneId;  
        }  
  
        public function set phoneId(pData:int):void{  
            _phoneId=pData;  
        }  
  
        public function get address():Address{  
            return _address;  
        }  
  
        public function set address(pData:Address):void{  
            _address=pData;  
        }  
  
        public function get phoneType():int{  
            return _phoneType;  
        }  
  
        public function set phoneType(pData:int):void{  
            _phoneType=pData;  
        }  
  
        public function get phoneNumber():String{  
            return _phoneNumber;  
        }  
  
        public function set phoneNumber(pData:String):void{  
            _phoneNumber=pData;  
        }  

        public function get active():Boolean{  
            return _active;  
        }  
  
        public function set active(pData:Boolean):void{  
            _active=pData;  
        } 
          
        public function get customers():ArrayCollection{  
            return _customers;  
        }  
  
        public function set customers(pData:ArrayCollection):void{  
            _customers=pData;  
        }  
    }  
}