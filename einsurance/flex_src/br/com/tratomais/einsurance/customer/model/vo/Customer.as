package br.com.tratomais.einsurance.customer.model.vo  
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.customer.Customer")]      
    [Managed]  
    public class Customer extends HibernateBean 
    {  
        private var _customerId:int;  
        private var _phone:Phone;  
        private var _address:Address;  
        private var _name:String;  
        private var _lastName:String;  
        private var _firstName:String;  
        private var _tradeName:String;  
        private var _personType:int;  
        private var _gender:int;  
        private var _birthDate:Date;  
        private var _registerDate:Date;  
        private var _maritalStatus:int;  
        private var _documentType:int;  
        private var _documentNumber:String;  
        private var _documentIssuer:String;  
        private var _documentValidity:Date;  
        private var _professionType:int;  
        private var _professionId:String;  
        private var _occupationId:String;  
        private var _activityId:String;  
        private var _inflowId:String;  
        private var _emailAddress:String;  
        private var _active:Boolean;
        private var _phoneNumber:String;  
        private var _contacts:ArrayCollection = new ArrayCollection();  
        private var _addresses:ArrayCollection = new ArrayCollection();  
  
  		public function addPhone() : Phone{
  			var addPhone : Phone = new Phone();
  			this.phone = addPhone;
  			return addPhone;
  		}
  		
  		public function addAddress() : Address{
  			var newAddress : Address = new Address();
  			newAddress.customer = this;
  			this.addresses = new ArrayCollection();
  			this.addresses.addItem(newAddress);
  			return newAddress;
  		}
  		
        public function Customer()  
        {  
        }  
  
        public function get customerId():int{  
            return _customerId;  
        }  
  
        public function set customerId(pData:int):void{  
            _customerId=pData;  
        }  
  
        public function get phone():Phone{  
            return _phone;  
        }  
  
        public function set phone(pData:Phone):void{  
            _phone=pData;  
        }  
  
        public function get address():Address{  
            return _address;  
        }  
  
        public function set address(pData:Address):void{  
            _address=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get lastName():String{  
            return _lastName;  
        }  
  
        public function set lastName(pData:String):void{  
            _lastName=pData;  
        }  
  
        public function get firstName():String{  
            return _firstName;  
        }  
  
        public function set firstName(pData:String):void{  
            _firstName=pData;  
        }  
  
        public function get tradeName():String{  
            return _tradeName;  
        }  
  
        public function set tradeName(pData:String):void{  
            _tradeName=pData;  
        }  
  
        public function get personType():int{  
            return _personType;  
        }  
  
        public function set personType(pData:int):void{  
            _personType=pData;  
        }  
  
        public function get gender():int{  
            return _gender;  
        }  
  
        public function set gender(pData:int):void{  
            _gender=pData;  
        }  
  
        public function get birthDate():Date{  
            return _birthDate;  
        }  
  
        public function set birthDate(pData:Date):void{  
        	if( pData )
            	_birthDate= new Date(pData.valueOf() + pData.getTimezoneOffset()*60000);
            else
            	_birthDate = null;
        }
  
        public function get registerDate():Date{  
            return _registerDate;  
        }  
  
        public function set registerDate(pData:Date):void{  
            _registerDate=pData;  
        }  
  
        public function get maritalStatus():int{  
            return _maritalStatus;  
        }  
  
        public function set maritalStatus(pData:int):void{  
            _maritalStatus=pData;  
        }  
  
        public function get documentType():int{  
            return _documentType;  
        }  
  
        public function set documentType(pData:int):void{  
            _documentType=pData;  
        }  
  
        public function get documentNumber():String{  
            return _documentNumber;  
        }  
  
        public function set documentNumber(pData:String):void{  
            _documentNumber=pData;  
        }  
  
        public function get documentIssuer():String{  
            return _documentIssuer;  
        }  
  
        public function set documentIssuer(pData:String):void{  
            _documentIssuer=pData;  
        }  
  
        public function get documentValidity():Date{  
            return _documentValidity;  
        }  
  
        public function set documentValidity(pData:Date):void{  
            _documentValidity=pData;  
        }  
  
        public function get professionType():int{  
            return _professionType;  
        }  
  
        public function set professionType(pData:int):void{  
            _professionType=pData;  
        }  
  
        public function get professionId():String{  
            return _professionId;  
        }  
  
        public function set professionId(pData:String):void{  
            _professionId=pData;  
        }  
  
        public function get occupationId():String{  
            return _occupationId;  
        }  
  
        public function set occupationId(pData:String):void{  
            _occupationId=pData;  
        }  
  
        public function get activityId():String{  
            return _activityId;  
        }  
  
        public function set activityId(pData:String):void{  
            _activityId=pData;  
        }  
  
        public function get inflowId():String{  
            return _inflowId;  
        }  
  
        public function set inflowId(pData:String):void{  
            _inflowId=pData;  
        }  
  
        public function get emailAddress():String{  
            return _emailAddress;  
        }  
  
        public function set emailAddress(pData:String):void{  
            _emailAddress=pData;  
        }  
  
        public function get active(): Boolean { 
        	return _active;
        }
        
        public function set active(pDados: Boolean): void {
        	this._active = pDados;
        }
         public function get phoneNumber():String{  
            return _phoneNumber;  
        }  
  
        public function set phoneNumber(pData:String):void{  
            _phoneNumber=pData;  
        }  
        public function get contacts():ArrayCollection{  
            return _contacts;  
        }  
  
        public function set contacts(pData:ArrayCollection):void{  
            _contacts=pData;  
        }  
  
        public function get addresses():ArrayCollection{  
            return _addresses;  
        }  
  
        public function set addresses(pData:ArrayCollection):void{  
            _addresses=pData;  
        }  
	}  
} 