package br.com.tratomais.einsurance.customer.model.vo  
{
	import mx.collections.ArrayCollection;
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.Country")]    
	[Managed]
    public class Country extends HibernateBean  
    {  
        private var _countryId:int;  
        private var _name:String;  
        private var _externalCode:String;  
  
        public function Country()  
        {  
        }  
  
        public function get countryId():int{  
            return _countryId;  
        }  
  
        public function set countryId(pData:int):void{  
            _countryId=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get externalCode():String{  
            return _externalCode;  
        }  
  
        public function set externalCode(pData:String):void{  
            _externalCode=pData;  
        }  
   
    }  
}  