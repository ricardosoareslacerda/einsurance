package br.com.tratomais.einsurance.customer.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.customer.Occupation")]    
    [Managed]  
    public class Occupation extends HibernateBean 
    {  
        private var _occupationId:int;  
        private var _name:String;  
        private var _externalCode:String;  
		private var _occupationRiskType:int;
		
        public function Occupation()  
        {  
        }  
  
        public function get occupationId():int{  
            return _occupationId;  
        }  
  
        public function set occupationId(pData:int):void{  
            _occupationId=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get externalCode():String{  
            return _externalCode;  
        }  
  
        public function set externalCode(pData:String):void{  
            _externalCode=pData;  
        }  
		
		public function get occupationRiskType():int{
			return _occupationRiskType;
		}  
		
		public function set occupationRiskType(pData:int):void{
			_occupationRiskType=pData;
		}
    }  
}