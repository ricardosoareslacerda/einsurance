package br.com.tratomais.einsurance.customer.model.vo
{
	import mx.collections.ArrayCollection;

	import net.digitalprimates.persistence.hibernate.HibernateBean;

    [RemoteClass(alias="br.com.tratomais.core.util.findcep.ZipCodeAddress")]
    [Managed]
    public class ZipCodeAddress extends HibernateBean
    {
        private var _addressStreet:String;
        private var _districtName:String;
        private var _cityId:int;
        private var _cityName:String;
        private var _stateId:int;
        private var _stateName:String;
        private var _zipCode:String;
        private var _countryId:int;
        private var _countryName:String;

        public function ZipCodeAddress()
        {
        	
        }

        public function get addressStreet():String{
            return _addressStreet;
        }

        public function set addressStreet(pData:String):void{
            _addressStreet=pData;
        }

        public function get districtName():String{
            return _districtName;
        }

        public function set districtName(pData:String):void{
            _districtName=pData;
        }

        public function get cityId():int{
            return _cityId;
        }

        public function set cityId(pData:int):void{
            _cityId=pData;
        }

        public function get cityName():String{
            return _cityName;
        }

        public function set cityName(pData:String):void{
            _cityName=pData;
        }

        public function get stateId():int{
            return _stateId;
        }

        public function set stateId(pData:int):void{
            _stateId=pData;
        }

        public function get stateName():String{
            return _stateName;
        }

        public function set stateName(pData:String):void{
            _stateName=pData;
        }

        public function get zipCode():String{
            return _zipCode;
        }

        public function set zipCode(pData:String):void{
            _zipCode=pData;
        }

        public function get countryId():int{
            return _countryId;
        }

        public function set countryId(pData:int):void{
            _countryId=pData;
        }

        public function get countryName():String{
            return _countryName;
        }

        public function set countryName(pData:String):void{
            _countryName=pData;
        }
    }
}
