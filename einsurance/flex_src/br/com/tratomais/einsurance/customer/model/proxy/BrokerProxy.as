package br.com.tratomais.einsurance.customer.model.proxy
{
	
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.customer.business.BrokerDelegateProxy;
	import br.com.tratomais.einsurance.customer.business.DataOptionDelegateProxy;
	import br.com.tratomais.einsurance.customer.model.vo.Broker;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;

		
	
	[Bindable]
	public class BrokerProxy extends Proxy implements IProxy 
	{
	
		public static const NAME:String = "BrokerProxy";

		public function BrokerProxy(data:Object = null)
		{
			super(NAME, data );
		}

		public function saveObject( brokerVO:Broker ) : void{
			var delegate:BrokerDelegateProxy = new BrokerDelegateProxy(new Responder(onResult, onSaveFault));
			delegate.saveObject( brokerVO );
		}
		
		private function onResult(pResultEvt:ResultEvent):void{			
            var broker:Broker =pResultEvt.result as Broker ;
            sendNotification(NotificationList.BROKER_SAVE_SUCCESS, broker);
		}
		
		private function onSaveFault(pFaultEvent:FaultEvent):void{			
            sendNotification(NotificationList.BROKER_SAVE_FAULT, pFaultEvent);
		}
		
		public function listAll():void
		{
			var delegate:BrokerDelegateProxy = new BrokerDelegateProxy(new Responder(onListAllResult, onFault));
			delegate.listAll();
		}

		private function onListAllResult(pResultEvt:ResultEvent):void
		{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.BROKER_LISTED, result);
		}
		
		public function findByName(name:String):void{
			var delegate:BrokerDelegateProxy = new BrokerDelegateProxy(new Responder(onResultSearch, onFault));
			delegate.findByName( name );
		
		}
		
		public function onResultSearch(pResultEvt:ResultEvent):void{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.BROKER_LISTED, result);
		}
		
		
		public function listBrokerType(brokerType:int):void{
			var delegate:DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder( onListBrokerTypeResult, onFault));
			delegate.findDataOptionByDataGroup( brokerType );
		}
		
		public function onListBrokerTypeResult(pResultEvt:ResultEvent):void{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.BROKER_TYPE_LISTED, result);			
		}
		
		public function listDocumentType(documentType:int):void{
			var delegate:DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder( onListDocumentTypeResult, onFault));
			delegate.findDataOptionByDataGroup( documentType );
		}
		
		public function onListDocumentTypeResult(pResultEvt:ResultEvent):void{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.BROKER_DOCUMENT_TYPE_LISTED, result);			
		}			
				
		private function onFault(pFaultEvt:FaultEvent):void{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
		}
	}
}