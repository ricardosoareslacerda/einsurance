package br.com.tratomais.einsurance.customer.model.proxy
{
	
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.customer.business.CityDelegateProxy;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;

	[Bindable]	
	public class CityProxy extends Proxy implements IProxy 
	{
		public static const NAME:String = "CityProxy";
		
		[Embed(source="/assets/images/error.png")]
		public var errorMsg:Class;

		public function CityProxy(data:Object = null)
		{
			super(NAME, data );
		}
		
		public function listAll():void
		{
			var delegate:CityDelegateProxy = new CityDelegateProxy(new Responder(onListAllResult, onFault));
			delegate.listAll();
		}

		private function onListAllResult(pResultEvt:ResultEvent):void
		{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.CITY_LISTED, result);
		}
		
		public function listIdentifiedListCity(identifiedList:IdentifiedList):void
		{
			var delegate:CityDelegateProxy = new CityDelegateProxy(new Responder(onIdentifiedListCity, onFault));
			delegate.listIdentifiedListCity(identifiedList);
		}

		private function onIdentifiedListCity(pResultEvt:ResultEvent):void
		{
			var identifiedList:IdentifiedList=pResultEvt.result as IdentifiedList;
         	sendNotification(NotificationList.CITY_LISTED, identifiedList);
		}		
		
		public function listCityByState(identifiedList:IdentifiedList, stateId:int):void
		{
			var delegate:CityDelegateProxy = new CityDelegateProxy(new Responder(onListCityByStateResult, onFault));
			delegate.listCityByState(identifiedList, stateId);
		}

		private function onListCityByStateResult(pResultEvt:ResultEvent):void
		{
			var identifiedList:IdentifiedList=pResultEvt.result as IdentifiedList;
         	sendNotification(NotificationList.CITY_LISTED, identifiedList);
		}
		
		private function onFault(pFaultEvt:FaultEvent):void{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
		}		
						
	}
}