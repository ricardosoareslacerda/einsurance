package br.com.tratomais.einsurance.customer.model.proxy
{
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.customer.business.DataOptionDelegateProxy;
    import br.com.tratomais.einsurance.customer.business.PartnerDelegateProxy;
    import br.com.tratomais.einsurance.useradm.model.vo.Partner;
    import br.com.tratomais.einsurance.useradm.model.vo.User;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.rpc.Responder;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    
    import org.puremvc.as3.interfaces.IProxy;
    import org.puremvc.as3.patterns.proxy.Proxy;  

    [Bindable]  
    public class PartnerProxy extends Proxy implements IProxy   
    {  
      
        public static const NAME:String = "PartnerProxy"; 

		[Embed(source="/assets/images/error.png")]
		public var errorMsg:Class;
    	
        public function PartnerProxy( data:Object = null)  
        {  
            super(NAME, data );  
        }  
 
 		public function listAll():void
		{
			var delegate:PartnerDelegateProxy = new PartnerDelegateProxy(new Responder(onListResult, onFault));  
			delegate.listAll();
		} 
				
		private function onListResult(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.PARTNER_LIST_REFRESH, collection);
        }
        public function listPartnersByUserAll(user : User):void
		{
			var delegate:PartnerDelegateProxy = new PartnerDelegateProxy(new Responder(onListPartnersAll, onFault));  
			delegate.listAllActivePartner(user);
		}   
		
		private function onListPartnersAll(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.USER_PARTNER_REFRESH, collection);
        }
   		
        public function findByName(name: String):void	{
			var delegate:PartnerDelegateProxy = new PartnerDelegateProxy(new Responder(onFindByNameResult, onFault));  
			delegate.findByName(name);
		} 
		
		private function onFindByNameResult(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.PARTNER_LIST_REFRESH, collection);
        }  

     	 public function saveObjectInsert(partner : Partner):void	{
			var delegate:PartnerDelegateProxy = new PartnerDelegateProxy(new Responder(onSaveObjectResult, onFault));  
			delegate.saveObjectInsert(partner);
		} 

        public function saveObject(partner : Partner):void	{
			var delegate:PartnerDelegateProxy = new PartnerDelegateProxy(new Responder(onSaveObjectResult, onFault));  
			delegate.saveObject(partner);
		} 
		
		private function onSaveObjectResult(pResultEvt:ResultEvent):void  {  
			var partner:Partner =pResultEvt.result as Partner ;
			sendNotification(NotificationList.PARTNER_SAVE_SUCCESS, partner);
        }  
		
		public function listDocumentType(documentType:int):void{
			var delegate:DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder( onListDocumentTypeResult, onFault));
			delegate.findDataOptionByDataGroup( documentType );
		}
		
		public function onListDocumentTypeResult(pResultEvt:ResultEvent):void{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.PARTNER_DOCUMENT_TYPE_LISTED, result);			
		}		
		
		private function onFault(pFaultEvt:FaultEvent):void{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
		}
    }  
}  