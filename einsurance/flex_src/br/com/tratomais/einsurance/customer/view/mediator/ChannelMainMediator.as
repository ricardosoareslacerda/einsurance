////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 26/03/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.customer.view.mediator
{  
    import br.com.tratomais.einsurance.ApplicationFacade;
    import br.com.tratomais.einsurance.core.Constant;
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.core.Utilities;
    import br.com.tratomais.einsurance.core.components.CityCombo.view.mediator.CityComboMediator;
    import br.com.tratomais.einsurance.core.components.CountryCombo.view.mediator.CountryComboMediator;
    import br.com.tratomais.einsurance.core.components.DoubleClickManager;
    import br.com.tratomais.einsurance.core.components.StateCombo.view.mediator.StateComboMediator;
    import br.com.tratomais.einsurance.core.controller.StateEnum;
    import br.com.tratomais.einsurance.core.proxy.UtilProxy;
    import br.com.tratomais.einsurance.customer.model.proxy.ChannelProxy;
    import br.com.tratomais.einsurance.customer.model.proxy.DataOptionProxy;
    import br.com.tratomais.einsurance.customer.model.proxy.SubsidiaryProxy;
    import br.com.tratomais.einsurance.customer.model.vo.Channel;
    import br.com.tratomais.einsurance.customer.view.components.ChannelMain;
    import br.com.tratomais.einsurance.useradm.model.vo.User;
    import br.com.tratomais.einsurance.useradm.model.vo.UserPartner;
    
    import flash.events.Event;
    import flash.events.MouseEvent;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.resources.ResourceManager;
    
    import org.puremvc.as3.interfaces.IMediator;
    import org.puremvc.as3.interfaces.INotification;
    import org.puremvc.as3.patterns.mediator.Mediator;  
  
    public class ChannelMainMediator extends Mediator implements IMediator  
    {  
        public static const NAME:String = 'ChannelMainMediator';  
        private var channelProxy:ChannelProxy; 
        private var dataOptionProxy:DataOptionProxy;
        private var subsidiaryProxy:SubsidiaryProxy;
        private var utilProxy: UtilProxy;
        private var doubleCLickManager:DoubleClickManager = null;
        
        private var countryComboMediator:CountryComboMediator;
        private var stateComboMediator:StateComboMediator;
        private var cityComboMediator:CityComboMediator;        
        
        private var _channel:Channel;
        /** logged user */
        private var _loggedUser:User;
        
        /** @private */
        protected function get loggedUser(): User {
        	if (_loggedUser == null)
        		_loggedUser = ApplicationFacade.getInstance().loggedUser;
        	return _loggedUser;
        }
        
        /** @private */
        protected function get userPartner():UserPartner {
			for each(var userPartner:UserPartner in loggedUser.userPartners){
				if (userPartner.partner.partnerId == this.loggedUser.transientSelectedPartner)
					return userPartner;
			}
        	return null;
        }
        
        /** @return if the user is or not administrator */
        private function isAdministrator():Boolean {
        	return (loggedUser.administrator) || (( (userPartner != null) && (userPartner.administrator) ));
        }
        
        public function ChannelMainMediator(viewComponent:Object=null){  
            super(NAME, viewComponent); 
        }  
          
        public function get channelMain():ChannelMain{  
            return viewComponent as ChannelMain;  
        }         

		override public function onRegister():void{
			super.onRegister();
			
			try{			
				channelProxy = ChannelProxy(facade.retrieveProxy(ChannelProxy.NAME));
				subsidiaryProxy = SubsidiaryProxy(facade.retrieveProxy(SubsidiaryProxy.NAME));
				dataOptionProxy = DataOptionProxy(facade.retrieveProxy(DataOptionProxy.NAME));
	
				countryComboMediator = new CountryComboMediator(channelMain.channelAddress.cmbxCountry);
				stateComboMediator = new StateComboMediator(channelMain.channelAddress.cmbxState);
				cityComboMediator = new CityComboMediator(channelMain.channelAddress.cmbxCity);
	
				ApplicationFacade.getInstance().registerMediator(countryComboMediator);
				ApplicationFacade.getInstance().registerMediator(stateComboMediator);
				ApplicationFacade.getInstance().registerMediator(cityComboMediator);
	
				channelMain.addEventListener(StateEnum.SAVE.toString(), onSaveHandle);
				channelMain.addEventListener(StateEnum.ADD.toString(), onAddHandle);
				channelMain.addEventListener(StateEnum.CANCEL.toString(), onCancelHandle);
	
				doubleCLickManager = new DoubleClickManager(channelMain.dtgChannel);
				doubleCLickManager.addEventListener(MouseEvent.CLICK, onViewHandle);
				doubleCLickManager.addEventListener(MouseEvent.DOUBLE_CLICK, onChangeHandle);
			}
			catch(error:Error){
				Alert.show(error.message, "Error", Alert.OK, null, null, Utilities.errorMsg);
			}
			this.mainInitialize();
    	}

		override public function onRemove():void 
		{
			super.onRemove();

			ApplicationFacade.getInstance().removeMediator(countryComboMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(stateComboMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(cityComboMediator.getMediatorName());

			channelMain.removeEventListener(StateEnum.SAVE.toString(), onSaveHandle);
			channelMain.removeEventListener(StateEnum.ADD.toString(), onAddHandle);
			channelMain.removeEventListener(StateEnum.CANCEL.toString(), onCancelHandle);

			doubleCLickManager.removeEventListener(MouseEvent.CLICK, onViewHandle);
			doubleCLickManager.removeEventListener(MouseEvent.DOUBLE_CLICK, onChangeHandle);
			
			setViewComponent(null);
		}
          
        override public function listNotificationInterests():Array  
        {  
            return [NotificationList.CHANNEL_LIST_LISTED,
            		NotificationList.CHANNEL_ADD_SUCCESS,
            		NotificationList.SUBSIDIARY_LIST_REFRESH,
            		NotificationList.CHANNEL_USER_LIST_REFRESH,
            		NotificationList.PARNTER_CHANGE,
            		NotificationList.CHANNEL_PARTNER_LIST_LISTED,
            		NotificationList.DATA_OPTION_LISTED
             ];  
        }  
          
        override public function handleNotification(notification:INotification):void  
        {  
              
            switch ( notification.getName() )  
            {  
                case NotificationList.CHANNEL_LIST_LISTED: 
	           		 channelMain.channelList = notification.getBody() as ArrayCollection;
               		 break;
                case NotificationList.CHANNEL_ADD_SUCCESS: 
             		this.findChannelByPartner(loggedUser.transientSelectedPartner);
					this.mainInitialize();             		
             	    break; 
				case NotificationList.SUBSIDIARY_LIST_REFRESH:
					channelMain.channelForm.cmbxSubsidiary.dataProvider = notification.getBody() as ArrayCollection;
          			break;
				case NotificationList.CHANNEL_USER_LIST_REFRESH:
					channelMain.channelUser.dgrChannelUser.dataProvider = notification.getBody() as ArrayCollection;
          			break;
				case NotificationList.PARNTER_CHANGE:
					this.findChannelByPartner(ApplicationFacade.getInstance().loggedUser.transientSelectedPartner);
          			break;
				case NotificationList.CHANNEL_PARTNER_LIST_LISTED:
	           		 channelMain.channelList = notification.getBody() as ArrayCollection;				
               		 break;
				case NotificationList.DATA_OPTION_LISTED:
		        	channelMain.channelForm.cmbxChannelType.dataProvider = notification.getBody() as ArrayCollection;
					break;               		 
            }       
        } 		
		
      	private function getObjectSelect() : Channel {
      		var obj : Object = channelMain.dtgChannel.selectedItem;
      		var channel : Channel = null;
      		if(obj != null){
      			channel = obj as Channel;
      			return channel;
      		}else{
      			return null;
      		}
      	}

		private function enabledForms(valor:Boolean):void{
			channelMain.channelForm.enabledForm(valor);
			channelMain.channelAddress.enabledForm(valor); 
			channelMain.channelUser.enabledList(valor);
		}

        private function listSubsidiary():void  {
        	try{
   			 subsidiaryProxy.listAllActiveSubsidiary();
        	}catch(error:Error){
        		Alert.show(error.message, "Error", Alert.OK, null, null, Utilities.errorMsg);
        	}
        } 	

        private function onMainClean():void{
        	channelMain.channelForm.channel = new Channel();
        	channelMain.channelAddress.channel = new Channel();
        	channelMain.channelUser.channel = new Channel();
        	
			channelMain.channelForm.resetForm();
			channelMain.channelAddress.resetForm(); 
			channelMain.channelUser.resetList(); 
        }

		public function findChannelByPartner(partnerId:int) : void {
        	channelProxy.listChannel(partnerId, false);
      		channelMain.dtgChannel.enabled = true;
			channelMain.accordionContainer.selectedIndex = 0;
        	this.mainInitialize();
        	this.onMainClean();		        	
		}

		/** Validate the logged user, disabling the buttons if necessary */
        private function validateLogedUser():void{					
        	if (! isAdministrator() ){
				channelMain.imgNew.enabled = false;
				channelMain.imgSave.enabled = false;
				channelMain.imgUndo.enabled = false;
        	}        	
        	channelProxy.listChannel(this.loggedUser.transientSelectedPartner, false);
        }
                 
		private function mainInitialize():void{  	
           	this.validateLogedUser();
           	this.listSubsidiary();
           	
           	dataOptionProxy.listDataOption(Constant.CHANNEL_TYPE);
           	
			channelMain.dtgChannel.enabled = true;
			channelMain.accordionContainer.selectedIndex = 0;
           	
			this.enabledForms(false);
			this.initializeButtons();
      	}       
        
        /** Initialize the buttons, depending if user is or not an administrator */
		private function initializeButtons(): void{
			channelMain.imgNew.enabled = isAdministrator();
			channelMain.imgSave.enabled = false;
			channelMain.imgUndo.enabled = false;
		}        
        
        private function onAddHandle(event:Event):void  {
  			_channel = new Channel();
  			channelMain.dtgChannel.selectedItem = null;
      		channelMain.dtgChannel.enabled = false;
			channelMain.accordionContainer.selectedIndex = 0;
			
			this.enabledForms(true);
			this.onMainClean();
        	
        	channelProxy.listUserByPartnerId(ApplicationFacade.getInstance().loggedUser.transientSelectedPartner);
			channelMain.channelForm.channel = _channel;
			channelMain.channelAddress.channel = _channel;
			channelMain.channelUser.channel = _channel;				
			
			channelMain.imgNew.enabled = false;
			channelMain.imgSave.enabled = true;
			channelMain.imgUndo.enabled = true;			
		}	
		
		private function onViewHandle(event:Event):void  {
			_channel = this.getObjectSelect();
			if(_channel != null){
				this.enabledForms(false);
				
				channelProxy.fillListUserByChannel(_channel);
				channelMain.channelForm.channel = _channel;
				channelMain.channelAddress.channel = _channel;
				channelMain.channelUser.channel = _channel;	
			}
      	}

		/** Called with a double click, to make changes on records */
		private function onChangeHandle(event:Event):void  {
			if (isAdministrator()){
	  			_channel = this.getObjectSelect();
				if(_channel != null){
					this.enabledForms(true);
					channelMain.dtgChannel.enabled = false;
									
					channelProxy.fillListUserByChannel(_channel);
					channelMain.channelForm.channel = _channel;
					channelMain.channelAddress.channel = _channel;
					channelMain.channelUser.channel = _channel;
					
					channelMain.imgNew.enabled = false;
					channelMain.imgSave.enabled = true;
					channelMain.imgUndo.enabled = true;
	        	}
   			}
   			else {
   				Utilities.warningMessage("thisUserNotAreAllowedAsAdministrator");
   			}
		}     			
      	
        private function onCancelHandle(event:Event):void{
			this.onMainClean();
			_channel = null;
			channelMain.dtgChannel.dataProvider = null;
			channelMain.dtgChannel.enabled = true; 
			channelMain.accordionContainer.selectedIndex = 0;
			this.findChannelByPartner(loggedUser.transientSelectedPartner);			
			this.enabledForms(false);
			this.initializeButtons();								
      	}      				

	     private function selectedUserSave(channel:Channel):void{
			var channelList:ArrayCollection = channelMain.channelUser.dgrChannelUser.dataProvider as ArrayCollection;
			var channelSelected:ArrayCollection = new ArrayCollection();
			
			for each (var user:User in channelList){
				if(user.transientSelected == true){
					channelSelected.addItem(user);
				}
			}
			channel.users = channelSelected;
	    } 
			      	
      	private function onSaveHandle(event:Event):void  {
			_channel = this.getObjectSelect();
			
			if(Utilities.validateForm(channelMain.channelForm.validatorForm, ResourceManager.getInstance().getString('resources', 'channelDetails')) &&
			   Utilities.validateForm(channelMain.channelAddress.validatorAddress, ResourceManager.getInstance().getString('resources', 'address'))){
				try{
					_channel = channelMain.channelForm.channel;
					_channel = channelMain.channelAddress.channel;
					_channel.partnerId = ApplicationFacade.getInstance().loggedUser.transientSelectedPartner;
					this.selectedUserSave(_channel);
	  				this.channelProxy.saveObject(_channel);
					this.enabledForms(false);
					this.initializeButtons();
					
					var log:String;
					log = ApplicationFacade.getInstance().loggedUser.login;
					log += ":" + ApplicationFacade.getInstance().loggedUser.domain.name;
					log += ":CHANNEL";
					log += ":SAVE";
					ApplicationFacade.getInstance().auditLogger = log;					
													
				}
	  			catch(error:Error){
	  				Alert.show(error.message, "Error", Alert.OK, null, null, Utilities.errorMsg);
	  			}
			}
      	} 
    } 
} 