////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 04/01/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.customer.view.mediator
{  
    import br.com.tratomais.einsurance.ApplicationFacade;
    import br.com.tratomais.einsurance.core.Constant;
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.core.Utilities;
    import br.com.tratomais.einsurance.core.components.CityCombo.view.mediator.CityComboMediator;
    import br.com.tratomais.einsurance.core.components.CountryCombo.view.mediator.CountryComboMediator;
    import br.com.tratomais.einsurance.core.components.DoubleClickManager;
    import br.com.tratomais.einsurance.core.components.StateCombo.view.mediator.StateComboMediator;
    import br.com.tratomais.einsurance.core.controller.StateEnum;
    import br.com.tratomais.einsurance.customer.model.proxy.DataOptionProxy;
    import br.com.tratomais.einsurance.customer.model.proxy.PartnerProxy;
    import br.com.tratomais.einsurance.customer.view.components.PartnerMain;
    import br.com.tratomais.einsurance.useradm.model.proxy.UserProxy;
    import br.com.tratomais.einsurance.useradm.model.vo.Partner;
    import br.com.tratomais.einsurance.useradm.model.vo.User;
    
    import flash.events.Event;
    import flash.events.MouseEvent;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.resources.ResourceManager;
    
    import org.puremvc.as3.interfaces.IMediator;
    import org.puremvc.as3.interfaces.INotification;
    import org.puremvc.as3.patterns.mediator.Mediator;  
  
    /**
     * A Mediator for interacting with the PartnerMain component.
     */  
    public class PartnerMainMediator extends Mediator implements IMediator  
    {  
    	
        public static const NAME:String = 'PartnerMainMediator';  
          
        private var partnerProxy:PartnerProxy; 
        private var userProxy:UserProxy; 
		private var partner:Partner;
		private var dataOptionProxy:DataOptionProxy  
		private var doubleCLickManager:DoubleClickManager = null;
		
        private var countryComboMediator:CountryComboMediator;
        private var stateComboMediator:StateComboMediator;
        private var cityComboMediator:CityComboMediator;
        
		private var _partner:Partner;
						 
        public function PartnerMainMediator(viewComponent:Object){  
            super(NAME, viewComponent);  
        }  
          
        public function get partnerMain():PartnerMain{  
            return viewComponent as PartnerMain;  
        }

		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */
		override public function onRegister():void{
			super.onRegister();
            
            //retrieve the proxies
            partnerProxy 	= PartnerProxy(facade.retrieveProxy(PartnerProxy.NAME)); 
            userProxy	 	= UserProxy(facade.retrieveProxy(UserProxy.NAME));
            dataOptionProxy = DataOptionProxy(facade.retrieveProxy(DataOptionProxy.NAME))
            			
			//register mediator
			countryComboMediator = new CountryComboMediator(partnerMain.partnerAddress.cmbxCountry);
			stateComboMediator = new StateComboMediator(partnerMain.partnerAddress.cmbxState);
			cityComboMediator = new CityComboMediator(partnerMain.partnerAddress.cmbxCity);
			
			ApplicationFacade.getInstance().registerMediator(countryComboMediator);
			ApplicationFacade.getInstance().registerMediator(stateComboMediator);
			ApplicationFacade.getInstance().registerMediator(cityComboMediator);
						
			//add event the component
			partnerMain.addEventListener(StateEnum.SAVE.toString(), onSaveHandler);  
			partnerMain.addEventListener(StateEnum.ADD.toString(), onAddHandler);  
			partnerMain.addEventListener(StateEnum.CANCEL.toString(), onCancelHandler);
			
			doubleCLickManager = new DoubleClickManager(partnerMain.dtgPartner);
			doubleCLickManager.addEventListener(MouseEvent.CLICK, onViewHandler);
			doubleCLickManager.addEventListener(MouseEvent.DOUBLE_CLICK, onChangeHandler);
			
			//initialize	
			this.mainInitialize();
    	}      
           
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
			
			//remove mediators;
			ApplicationFacade.getInstance().removeMediator(countryComboMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(stateComboMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(cityComboMediator.getMediatorName());			

			//remove events the component
			partnerMain.removeEventListener(StateEnum.SAVE.toString(), onSaveHandler);  
			partnerMain.removeEventListener(StateEnum.ADD.toString(), onAddHandler);  
			partnerMain.removeEventListener(StateEnum.CANCEL.toString(), onCancelHandler);
			
			doubleCLickManager.removeEventListener(MouseEvent.CLICK, onViewHandler);
			doubleCLickManager.removeEventListener(MouseEvent.DOUBLE_CLICK, onChangeHandler);			
		}
          
        override public function listNotificationInterests():Array{  
            return [NotificationList.PARTNER_LIST_REFRESH,
   		 	        NotificationList.PARTNER_SAVE_SUCCESS,
   		 	        NotificationList.DOMAIN_LIST_REFRESH,
   		 	        NotificationList.PARTNER_DOCUMENT_TYPE_LISTED,
   		 	        NotificationList.DATA_OPTION_LISTED,
   		 	        NotificationList.CHANNEL_LIST_LISTED];  
        }
          
        override public function handleNotification(notification:INotification):void{  
            switch ( notification.getName() )  
            {  
        		case NotificationList.PARTNER_LIST_REFRESH:
        			var partnersCollection:ArrayCollection = notification.getBody() as ArrayCollection;
        			partnerMain.partnerCollection = partnersCollection;
		        	break;
				case NotificationList.PARTNER_SAVE_SUCCESS:
					this.onClean(); 				
	             	partnerProxy.listAll();
		      		partnerMain.dtgPartner.enabled = true;
					partnerMain.accordionContainer.selectedIndex = 0;
					this.enabledForms(false);
					
					var log:String;
					log = ApplicationFacade.getInstance().loggedUser.login;
					log += ":" + ApplicationFacade.getInstance().loggedUser.domain.name;
					log += ":USER";
					log += ":SAVE";
					ApplicationFacade.getInstance().auditLogger = log;
										
					break;
				case NotificationList.DOMAIN_LIST_REFRESH:
               		var list:ArrayCollection = notification.getBody() as ArrayCollection;
					partnerMain.partnerForm.cmbxDomain.dataProvider = list;
                 	break;
                 case NotificationList.PARTNER_DOCUMENT_TYPE_LISTED:
                 	var list:ArrayCollection = notification.getBody() as ArrayCollection;
                 	this.partnerMain.partnerForm.cmbxDocumentType.dataProvider = list;
                 	break;
                 case NotificationList.DATA_OPTION_LISTED:	
                 	var list:ArrayCollection = notification.getBody() as ArrayCollection;
                 	this.partnerMain.partnerForm.cmbxPartnerType.dataProvider = list;
                 	break;
	        	case NotificationList.CHANNEL_LIST_LISTED:
            		var channel:ArrayCollection = notification.getBody() as ArrayCollection;
					this.partnerMain.partnerChannel.editorChannel.visible = false;
            		this.partnerMain.partnerChannel.dgrListChannel.dataProvider = channel;
            }  
        }         
        
        /**
        * Initialize the main Partner Screen
        **/
		private function mainInitialize():void  {
			
			partnerProxy.listAll();
			dataOptionProxy.listDataOption(Constant.PARTNER_TYPE);
			partnerProxy.listDocumentType(Constant.DOCUMENT_TYPE);
			userProxy.listAllDomainActive();
			
			this.enabledForms(false);
			this.initializeButtons();
      	}
      	
        /**
        * Initialize the buttons on Partner main screen
        **/      	
		private function initializeButtons(): void{
			partnerMain.imgNew.enabled=true;
			partnerMain.imgSave.enabled=false;
			partnerMain.imgUndo.enabled=false;
		}
		
		/**
		 * Comments
		 */        
        public function onCancelHandler(event:Event):void{
			partnerMain.dtgPartner.enabled = true; 
			partnerMain.accordionContainer.selectedIndex = 0;			
			this.onClean();
			this.enabledForms(false);
			partnerMain.accordionContainer.enabled = true;
			this.initializeButtons();		
      	}
      	      	
		/**
		 * Comments
		 */        
        private function onAddHandler(event:Event):void  {
      		partnerMain.dtgPartner.selectedItem = null;
      		partnerMain.dtgPartner.enabled = false;
			partnerMain.accordionContainer.selectedIndex = 0;
			
			this.onClean();
			this.enabledForms(true);
			
			_partner = new Partner();
			
			partnerMain.partnerForm.partner = _partner;
			partnerMain.partnerAddress.partner = _partner;
			partnerMain.partnerChannel.partner = _partner;			
			
			partnerMain.imgNew.enabled=false;
			partnerMain.imgSave.enabled=true;
			partnerMain.imgUndo.enabled=true;					
		}      	
      	
		private function onViewHandler(event:Event):void {
			_partner = this.getObjectSelect();
			if(editorIsEnabled()==false && _partner != null){
				this.enabledForms(false);
				partnerMain.partnerAddress.partner = _partner;
				partnerMain.partnerForm.partner = _partner;
				partnerMain.partnerChannel.partner = _partner;
			}
			partnerMain.dtgPartner.enabled = true;
      	}
      	
		public function editorIsEnabled() : Boolean{
			var isEnabled:Boolean = false;
			if(partnerMain.dtgPartner.enabled == false){
				isEnabled = true;
			}
			return isEnabled;
		}      	
      	
		private function onChangeHandler(event:Event):void  {
			var loggedUser : User = ApplicationFacade.getInstance().loggedUser;
			
			//if(loggedUser.administrator == false){
			//	Utilities.warningMessage("thisUserNotAreAllowedAsAdministrator");
			//}else{
				partnerMain.dtgPartner.enabled = false;
				_partner = this.getObjectSelect();
				if(_partner != null){
					this.enabledForms(true);				
					partnerMain.imgNew.enabled=false;
					partnerMain.imgSave.enabled=true;
					partnerMain.imgUndo.enabled=true;	
					partnerMain.dtgPartner.enabled=false;			
					partnerMain.partnerAddress.partner = _partner;
					partnerMain.partnerForm.partner = _partner;
					partnerMain.partnerChannel.partner = _partner;
	    		}
   			//}
      	}
		
		/**
		 * Comments
		 */
      	private function onSaveHandler(event:Event):void  {
			if(Utilities.validateForm(partnerMain.partnerForm.validatorForm, ResourceManager.getInstance().getString('resources', 'partnerDetails')) &&
			   Utilities.validateForm(partnerMain.partnerAddress.validatorAddress, ResourceManager.getInstance().getString('resources', 'address'))){	
				try{
					_partner = partnerMain.partnerAddress.partner;
					_partner = partnerMain.partnerForm.partner;
					_partner = partnerMain.partnerChannel.partner;
					
					if(this.getObjectSelect() == null){
						partnerProxy.saveObjectInsert(_partner);
					}else{
						partnerProxy.saveObject(_partner);
					}
					//this.initializeButtons();
				}catch(error:Error){
					Alert.show(error.message, "Error", Alert.OK, null, null, Utilities.errorMsg);
				}
			}
      	}

		/**
		 * Comments
		 */        
        private function onClean():void{
        	partnerMain.partnerAddress.partner = new Partner();
        	partnerMain.partnerChannel.partner = new Partner();
        	partnerMain.partnerForm.partner = new Partner(); 
        	
			partnerMain.partnerAddress.resetForm();
			partnerMain.partnerChannel.resetList();
			partnerMain.partnerForm.resetForm();
			
			partnerProxy.listAll();
        }			
		
		private function getObjectSelect() : Partner {
      		var obj : Object = partnerMain.dtgPartner.selectedItem;
      		var partner : Partner = null;
      		if(obj != null){
      			partner = obj as Partner;
      			return partner;
      		}else{
      			return null;
      		}
      	}
      	
      	public function enabledForms(valor:Boolean):void{
      		partnerMain.partnerAddress.enabledForm(valor);
			partnerMain.partnerForm.enabledForm(valor);
			partnerMain.partnerChannel.enabledList(valor);
      	}
    }
}