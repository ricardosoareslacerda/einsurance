////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 04/01/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.customer.view.mediator
{  
    import br.com.tratomais.einsurance.ApplicationFacade;
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.core.Utilities;
    import br.com.tratomais.einsurance.core.components.CityCombo.view.mediator.CityComboMediator;
    import br.com.tratomais.einsurance.core.components.CountryCombo.view.mediator.CountryComboMediator;
    import br.com.tratomais.einsurance.core.components.DoubleClickManager;
    import br.com.tratomais.einsurance.core.components.StateCombo.view.mediator.StateComboMediator;
    import br.com.tratomais.einsurance.core.controller.StateEnum;
    import br.com.tratomais.einsurance.customer.model.proxy.SubsidiaryProxy;
    import br.com.tratomais.einsurance.customer.model.vo.Subsidiary;
    import br.com.tratomais.einsurance.customer.view.components.SubsidiaryMain;
    
    import flash.events.Event;
    import flash.events.MouseEvent;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.resources.ResourceManager;
    
    import org.puremvc.as3.interfaces.IMediator;
    import org.puremvc.as3.interfaces.INotification;
    import org.puremvc.as3.patterns.mediator.Mediator;  

    /**
     * A Mediator for interacting with the SubsidiaryMain component.
     */
    public class SubsidiaryMainMediator extends Mediator implements IMediator  
    {  
    	
        public static const NAME:String = 'SubsidiaryMainMediator';  
		private var doubleCLickManager:DoubleClickManager = null;
          
        private var subsidiaryProxy:SubsidiaryProxy;  
		
        private var countryComboMediator:CountryComboMediator;
        private var stateComboMediator:StateComboMediator;
        private var cityComboMediator:CityComboMediator;
		
		private var _subsidiary:Subsidiary;
		
		/**
		 * Comments
		 */	    
	    public function SubsidiaryMainMediator(viewComponent:Object)  
        {  
            super(NAME, viewComponent);
           	subsidiaryProxy = SubsidiaryProxy(facade.retrieveProxy(SubsidiaryProxy.NAME));
             
        }  
          
		/**
		 * Comments
		 */        
        public function get subsidiaryMain():SubsidiaryMain{  
            return viewComponent as SubsidiaryMain;
        }
      	
		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */
		override public function onRegister():void 
		{
			super.onRegister();
			
			countryComboMediator = new CountryComboMediator(subsidiaryMain.subsidiaryAddress.cmbxCountry);
			stateComboMediator = new StateComboMediator(subsidiaryMain.subsidiaryAddress.cmbxState);
			cityComboMediator = new CityComboMediator(subsidiaryMain.subsidiaryAddress.cmbxCity);
			
			ApplicationFacade.getInstance().registerMediator(countryComboMediator);
			ApplicationFacade.getInstance().registerMediator(stateComboMediator);
			ApplicationFacade.getInstance().registerMediator(cityComboMediator);

			subsidiaryMain.addEventListener(StateEnum.SAVE.toString(), onSaveHandle);  
			subsidiaryMain.addEventListener(StateEnum.ADD.toString(), onAddHandle);  
			subsidiaryMain.addEventListener(StateEnum.CANCEL.toString(), onCancelHandle);

			doubleCLickManager = new DoubleClickManager(subsidiaryMain.dtgSubsidiary);
			doubleCLickManager.addEventListener(MouseEvent.CLICK, onViewHandle);
			doubleCLickManager.addEventListener(MouseEvent.DOUBLE_CLICK, onChangeHandle);
			
			// Initialize
			this.mainInitialize();
    	}      
        
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
			ApplicationFacade.getInstance().removeMediator(countryComboMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(stateComboMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(cityComboMediator.getMediatorName());

			subsidiaryMain.removeEventListener(StateEnum.SAVE.toString(), onSaveHandle);  
			subsidiaryMain.removeEventListener(StateEnum.ADD.toString(), onAddHandle);  
			subsidiaryMain.removeEventListener(StateEnum.CANCEL.toString(), onCancelHandle);

			doubleCLickManager.removeEventListener(MouseEvent.CLICK, onViewHandle);
			doubleCLickManager.removeEventListener(MouseEvent.DOUBLE_CLICK, onChangeHandle);			
		}
		
		/**
		 * Comments
		 */		
        override public function listNotificationInterests():Array  
        {  
            return [  
   		 	        NotificationList.SUBSIDIARY_LIST_REFRESH,
   		 	        NotificationList.SUBSIDIARY_GENERAL_MESSAGE,
   		 	        NotificationList.SUBSIDIARY_SAVE_SUCCESS,
   		            ];  
        }  
        
		/**
		 * Comments
		 */          
        override public function handleNotification(notification:INotification):void  
        {  
            switch ( notification.getName() )  
            {  
        		case NotificationList.SUBSIDIARY_LIST_REFRESH:
        			var subsidiarysCollection:ArrayCollection = notification.getBody() as ArrayCollection;
        			subsidiaryMain.subsidiaryCollection = subsidiarysCollection;
		        	break;  
 				case NotificationList.SUBSIDIARY_GENERAL_MESSAGE:
 					var message: String = notification.getBody() as String;
					break;
 				case NotificationList.SUBSIDIARY_SAVE_SUCCESS:
	             	this.onClean();
	             	this.listSubsidiarys();
		      		subsidiaryMain.dtgSubsidiary.enabled = true;
					subsidiaryMain.accordionContainer.selectedIndex = 0;
					this.enabledForms(false);
					this.initializeButtons();
					
					var log:String;
					log = ApplicationFacade.getInstance().loggedUser.login;
					log += ":" + ApplicationFacade.getInstance().loggedUser.domain.name;
					log += ":SUBSIDIARY";
					log += ":SAVE";
					ApplicationFacade.getInstance().auditLogger = log;					
					
					break;
 		    }
        }
        
		/**
		 * Comments
		 */        
        private function listSubsidiarys():void  {
			subsidiaryProxy.listAllSubsidiary();
      	}
      	
		/**
		 * Comments
		 */		      	
		private function onChangeHandle(event:Event):void  {
  			_subsidiary = this.getObjectSelect();
			if(editorIsEnabled()==false && _subsidiary != null){
				this.enabledForms(true);
				subsidiaryMain.imgNew.enabled=false;
				subsidiaryMain.imgSave.enabled=true;
				subsidiaryMain.imgUndo.enabled=true;
				subsidiaryMain.dtgSubsidiary.enabled=false;
				subsidiaryMain.subsidiaryAddress.subsidiary = _subsidiary;
				subsidiaryMain.subsidiaryForm.subsidiary = _subsidiary;
        	}
		}

		public function enabledForms(valor:Boolean):void{
			subsidiaryMain.subsidiaryAddress.enabledForm(valor);
			subsidiaryMain.subsidiaryForm.enabledForm(valor);
		}

		private function editorIsEnabled() : Boolean{
			var isEnabled:Boolean = false;
			if(subsidiaryMain.dtgSubsidiary.enabled == false){
				isEnabled = true;
			}
			return isEnabled;
		}

		/**
		 * Comments
		 */      	
      	private function getObjectSelect() : Subsidiary{
      		var obj : Object = subsidiaryMain.dtgSubsidiary.selectedItem;
      		var subsidiary : Subsidiary = null;
      		if(obj != null){
      			subsidiary = obj as Subsidiary;
      			return subsidiary;
      		}else{
      			return null;
      		}
      	}

		/**
		 * Comments
		 */        
        private function onClean():void{
        	subsidiaryMain.subsidiaryForm.subsidiary = new Subsidiary();
        	subsidiaryMain.subsidiaryAddress.subsidiary = new Subsidiary();
        	
        	subsidiaryMain.subsidiaryForm.resetForm();
        	subsidiaryMain.subsidiaryAddress.resetForm();
        	
        	listSubsidiarys();
        }
        
		/**
		 * Comments
		 */        
        private function onCancelHandle(event:Event):void{
			this.onClean();	      		
			subsidiaryMain.dtgSubsidiary.enabled = true; 
			subsidiaryMain.accordionContainer.selectedIndex = 0;
			this.enabledForms(false);
			this.initializeButtons();
      	}
         
		/**
		 * Comments
		 */		
		private function mainInitialize():void  {
			
			listSubsidiarys();
			
      		subsidiaryMain.dtgSubsidiary.enabled = true;
			subsidiaryMain.accordionContainer.selectedIndex = 0;
			this.enabledForms(false);	
			this.initializeButtons();					
      	}

		/**
		 * initialize buttons 
		 */        
		private function initializeButtons(): void{
			subsidiaryMain.imgNew.enabled=true;
			subsidiaryMain.imgSave.enabled=false;
			subsidiaryMain.imgUndo.enabled=false;
		}
        
		/**
		 * Comments
		 */        
        private function onAddHandle(event:Event):void  {
      		subsidiaryMain.dtgSubsidiary.selectedItem = null;
      		subsidiaryMain.dtgSubsidiary.enabled = false;
			subsidiaryMain.accordionContainer.selectedIndex = 0;
			this.onClean();
			this.enabledForms(true);
			
			_subsidiary = new Subsidiary();
			
			subsidiaryMain.subsidiaryAddress.subsidiary = _subsidiary; 
			subsidiaryMain.subsidiaryForm.subsidiary = _subsidiary;
			
			subsidiaryMain.subsidiaryForm.chkActive.selected = true;
			subsidiaryMain.imgNew.enabled=false;
			subsidiaryMain.imgSave.enabled=true;
			subsidiaryMain.imgUndo.enabled=true;			
		}
		
		/**
		 * Comments
		 */		
		private function onViewHandle(event:Event):void  {
			_subsidiary = this.getObjectSelect();
			if(editorIsEnabled()==false && _subsidiary != null){
				this.enabledForms(false);
				subsidiaryMain.subsidiaryAddress.subsidiary = _subsidiary;
				subsidiaryMain.subsidiaryForm.subsidiary = _subsidiary;
			}
      	}
   	
      	
		/**
		 * Comments
		 */      	
      	private function onSaveHandle(event:Event):void  {
			_subsidiary = this.getObjectSelect();
			
			if(Utilities.validateForm(subsidiaryMain.subsidiaryForm.validatorForm, ResourceManager.getInstance().getString('resources', 'subsidiaryDetails')) &&
			   Utilities.validateForm(subsidiaryMain.subsidiaryAddress.validatorAddress, ResourceManager.getInstance().getString('resources', 'address'))){
				
				try{
					_subsidiary = subsidiaryMain.subsidiaryAddress.subsidiary;
					_subsidiary = subsidiaryMain.subsidiaryForm.subsidiary;
					
	  				subsidiaryProxy.saveObject(_subsidiary);
				
					//criar função para tratar os botões
					subsidiaryMain.imgNew.enabled=true;
					subsidiaryMain.imgSave.enabled=false;
					subsidiaryMain.imgUndo.enabled=true;		  				
				}
	  			catch(error:Error){
	  				Alert.show(error.message, "Error", Alert.OK, null, null, Utilities.errorMsg);
	  			}
			}
      	}      	
    }  
}  