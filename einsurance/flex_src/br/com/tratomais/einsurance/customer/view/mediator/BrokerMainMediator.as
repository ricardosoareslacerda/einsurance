////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 04/01/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.customer.view.mediator
{  
  
    import br.com.tratomais.einsurance.ApplicationFacade;
    import br.com.tratomais.einsurance.core.Constant;
    import br.com.tratomais.einsurance.core.ErrorWindow;
    import br.com.tratomais.einsurance.core.Errors;
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.core.Utilities;
    import br.com.tratomais.einsurance.core.components.CityCombo.view.mediator.CityComboMediator;
    import br.com.tratomais.einsurance.core.components.CountryCombo.view.mediator.CountryComboMediator;
    import br.com.tratomais.einsurance.core.components.DoubleClickManager;
    import br.com.tratomais.einsurance.core.components.StateCombo.view.mediator.StateComboMediator;
    import br.com.tratomais.einsurance.core.controller.StateEnum;
    import br.com.tratomais.einsurance.customer.model.proxy.BrokerProxy;
    import br.com.tratomais.einsurance.customer.model.vo.*;
    import br.com.tratomais.einsurance.customer.view.components.BrokerMain;
    import br.com.tratomais.einsurance.useradm.model.vo.User;
    
    import flash.events.Event;
    import flash.events.MouseEvent;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.resources.ResourceManager;
    import mx.rpc.events.FaultEvent;
    
    import org.puremvc.as3.interfaces.IMediator;
    import org.puremvc.as3.interfaces.INotification;
    import org.puremvc.as3.patterns.mediator.Mediator;  

    /**
     * A Mediator for interacting with the BrokerMain component.
     */  
    public class BrokerMainMediator extends Mediator implements IMediator  
    {  
          
        public static const NAME:String = 'BrokerMainMediator';  
        private var brokerProxy:BrokerProxy;
        private var doubleCLickManager:DoubleClickManager = null;

		private var countryComboMediator:CountryComboMediator;
        private var stateComboMediator:StateComboMediator;
        private var cityComboMediator:CityComboMediator;
        
        private var _broker:Broker;
        
		/**
		 * Constructor and creates new instance of BrokerMain.
		 */
        public function BrokerMainMediator(viewComponent:Object=null)  
        {  
           super(NAME, viewComponent);  
        }  
          
        /**
        * Returns instance of viewComponent (BrokerMain) 
        */
        public function get brokerMain():BrokerMain{  
            return viewComponent as BrokerMain;  
        }         

		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */
		override public function onRegister():void 
		{
			super.onRegister();
			
			try{
				brokerProxy = BrokerProxy( facade.retrieveProxy( BrokerProxy.NAME ));
							
				countryComboMediator = new CountryComboMediator(brokerMain.brokerAddress.cmbxCountry);
				stateComboMediator = new StateComboMediator(brokerMain.brokerAddress.cmbxState);
				cityComboMediator = new CityComboMediator(brokerMain.brokerAddress.cmbxCity);
				
				ApplicationFacade.getInstance().registerMediator(countryComboMediator);
				ApplicationFacade.getInstance().registerMediator(stateComboMediator);
				ApplicationFacade.getInstance().registerMediator(cityComboMediator);
								 
				brokerMain.addEventListener(StateEnum.SAVE.toString(), onSaveHandler);
				brokerMain.addEventListener(StateEnum.ADD.toString(), onAddHandler);
				brokerMain.addEventListener(StateEnum.CANCEL.toString(), onCancelHandler);
	
				doubleCLickManager = new DoubleClickManager(brokerMain.dtgBroker);
				doubleCLickManager.addEventListener(MouseEvent.CLICK, onViewHandler);
				doubleCLickManager.addEventListener(MouseEvent.DOUBLE_CLICK, onChangeHandler);
			}
			catch(error:Error){
				Alert.show(error.message, "Error", Alert.OK, null, null, Utilities.errorMsg);
			}
			this.mainInitialize();
    	}

		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
			
			try{
				ApplicationFacade.getInstance().removeMediator(countryComboMediator.getMediatorName());
				ApplicationFacade.getInstance().removeMediator(stateComboMediator.getMediatorName());
				ApplicationFacade.getInstance().removeMediator(cityComboMediator.getMediatorName());
			
				brokerMain.removeEventListener(StateEnum.SAVE.toString(), onSaveHandler);  
				brokerMain.removeEventListener(StateEnum.ADD.toString(), onAddHandler);  
				brokerMain.removeEventListener(StateEnum.CANCEL.toString(), onCancelHandler);
				
				doubleCLickManager.removeEventListener(MouseEvent.CLICK, onViewHandler);
				doubleCLickManager.removeEventListener(MouseEvent.DOUBLE_CLICK, onChangeHandler);
				
				this.onMainClean();
				
			}catch(error:Error){
				Alert.show(error.message, "Error", Alert.OK, null, null, Utilities.errorMsg);
			}
		}
		
		
		/**
		 * Defines the list of Notification Interests
		 */          
        override public function listNotificationInterests():Array  
        {  
            return [NotificationList.BROKER_LISTED,
            		NotificationList.BROKER_SAVE_SUCCESS,
            		NotificationList.BROKER_SAVE_FAULT,
   		 	        NotificationList.BROKER_GENERAL_MESSAGE,
   		 	        NotificationList.BROKER_TYPE_LISTED,
					NotificationList.BROKER_DOCUMENT_TYPE_LISTED];  
        }  
          
        /**
        * handle Notification 
        */  
        override public function handleNotification(notification:INotification):void  
        {  
            switch ( notification.getName() )  
            {  
				case NotificationList.BROKER_LISTED: 
					var listArrayResult: ArrayCollection = notification.getBody() as ArrayCollection;
					this.brokerMain.dtgBroker.dataProvider = listArrayResult;
					break;
              	case NotificationList.BROKER_TYPE_LISTED: 
		        	var brokerTypeArrayResult: ArrayCollection = notification.getBody() as ArrayCollection;
		        	this.brokerMain.brokerForm.cmbxBrokerType.dataProvider = brokerTypeArrayResult;  
                	break;
              	case NotificationList.BROKER_DOCUMENT_TYPE_LISTED: 
		        	var documentTypeArrayResult: ArrayCollection = notification.getBody() as ArrayCollection;
		        	this.brokerMain.brokerForm.cmbxDocumentType.dataProvider = documentTypeArrayResult;  
                	break;					
				case NotificationList.BROKER_SAVE_SUCCESS:
					this.initializeButtons();
	             	this.onMainClean();
	             	this.listBroker();
		      		brokerMain.dtgBroker.enabled = true;
					brokerMain.accordionContainer.selectedIndex = 0;
					this.enabledForms(false);
					
					var log:String;
					log = ApplicationFacade.getInstance().loggedUser.login;
					log += ":" + ApplicationFacade.getInstance().loggedUser.domain.name;
					log += ":BROKER";
					log += ":SAVE";
					ApplicationFacade.getInstance().auditLogger = log;
											
					break;
				case NotificationList.BROKER_SAVE_FAULT:
					brokerMain.imgNew.enabled=false;
					brokerMain.imgSave.enabled=true;
					brokerMain.imgUndo.enabled=true;
					brokerMain.dtgBroker.enabled=false;
					this.enabledForms(true);
					if(! (notification.getBody() instanceof FaultEvent)){
						Utilities.errorMessage("internalError");
					}
					else {
						ErrorWindow.createAndShowError(Errors.INTERNAL_ERROR,(notification.getBody() as FaultEvent).fault.getStackTrace()); 
					}
					break;
				case NotificationList.BROKER_GENERAL_MESSAGE:
					var message: String = notification.getBody() as String;
					break;         	
            }
        }

		private function initializeButtons(): void{
			brokerMain.imgNew.enabled=true;
			brokerMain.imgSave.enabled=false;
			brokerMain.imgUndo.enabled=false;
		}

        private function listBroker():void  {
        	try{
   				brokerProxy.listAll();
        	}catch(error:Error){
        		Alert.show(error.message, "Error", Alert.OK, null, null, Utilities.errorMsg);
        	}
      	}

		private function validateLogedUser():void{
			var loggedUser : User = ApplicationFacade.getInstance().loggedUser;
			
			/*        	        	
        	if (loggedUser.administrator == false){        		
				brokerMain.imgNew.enabled=false;
				brokerMain.imgSave.enabled=false;
				brokerMain.imgUndo.enabled=false;
				
				//remove this listener to don´t let the user change something
				//just view
				doubleCLickManager.removeEventListener(MouseEvent.DOUBLE_CLICK, onChangeHandler);				
        	}
        	*/			
		}
		      	
		private function onChangeHandler(event:Event):void  {
  			_broker = this.getObjectSelect();
			if(editorIsEnabled()==false && _broker != null){
				this.enabledForms(true);
				brokerMain.imgNew.enabled=false;
				brokerMain.imgSave.enabled=true;
				brokerMain.imgUndo.enabled=true;
				brokerMain.dtgBroker.enabled=false;
				brokerMain.brokerForm.broker = _broker;
				brokerMain.brokerAddress.broker = _broker;
        	}
		}

      	private function getObjectSelect() : Broker {
      		var obj : Object = brokerMain.dtgBroker.selectedItem;
      		var broker : Broker = null;
      		if(obj != null){
      			broker = obj as Broker;
      			return broker;
      		}else{
      			return null;
      		}
      	}
		
		public function enabledForms(valor:Boolean):void{
			brokerMain.brokerForm.enabledForm(valor);
			brokerMain.brokerAddress.enabledForm(valor);
		}
		
		private function editorIsEnabled() : Boolean{
			var isEnabled:Boolean = false;
			if(brokerMain.dtgBroker.enabled == false){
				isEnabled = true;
			}
			return isEnabled;
		}
						
        private function onMainClean():void{
        	brokerMain.brokerForm.broker = new Broker();
			brokerMain.brokerAddress.broker = new Broker();
			
			brokerMain.brokerForm.resetForm();
			brokerMain.brokerAddress.resetForm();
			
			this.listBroker();
        }
        
        private function onCancelHandler(event:Event):void{
			this.onMainClean();
			brokerMain.dtgBroker.enabled = true; 
			brokerMain.accordionContainer.selectedIndex = 0;
			this.enabledForms(false);		
			this.initializeButtons();
      	}
         
		private function mainInitialize():void  {
			this.initializeButtons();
			this.validateLogedUser();
			
			this.listBroker();
			
			brokerProxy.listBrokerType(Constant.BROKER_TYPE);
			brokerProxy.listDocumentType(Constant.DOCUMENT_TYPE);
      		
      		brokerMain.dtgBroker.enabled = true;
			brokerMain.accordionContainer.selectedIndex = 0;
			
			this.enabledForms(false);
      	}
        
        private function onAddHandler(event:Event):void  {
  			brokerMain.dtgBroker.selectedItem = null;
      		brokerMain.dtgBroker.enabled = false;
			brokerMain.accordionContainer.selectedIndex = 0;
			
			this.enabledForms(true);			
			this.onMainClean();

			_broker = new Broker();
						
			brokerMain.brokerForm.broker = _broker;
			brokerMain.brokerAddress.broker = _broker;
			
			brokerMain.imgNew.enabled=false;
			brokerMain.imgSave.enabled=true;
			brokerMain.imgUndo.enabled=true;			
		}
		
		private function onViewHandler(event:Event):void  {
			_broker=this.getObjectSelect();
			if(editorIsEnabled()==false && _broker != null){
				this.enabledForms(false);
				
				brokerMain.brokerForm.broker = _broker;
				brokerMain.brokerAddress.broker = _broker;
			}
      	}
      	
      	private function onSaveHandler(event:Event):void  {
			if(Utilities.validateForm(brokerMain.brokerForm.validatorForm, ResourceManager.getInstance().getString('resources', 'brokerDetails')) &&
			   Utilities.validateForm(brokerMain.brokerAddress.validatorAddress, ResourceManager.getInstance().getString('resources', 'address')) ){
				try{
  					//by reference
  					_broker = brokerMain.brokerForm.broker;
  					_broker = brokerMain.brokerAddress.broker;
  					
  					brokerProxy.saveObject(_broker);
  					
					this.enabledForms(false);
				}
	  			catch(error:Error){
	  				Alert.show(error.message, "Error", Alert.OK, null, null, Utilities.errorMsg);
	  			}
			}
      	}      	
    }  
}  