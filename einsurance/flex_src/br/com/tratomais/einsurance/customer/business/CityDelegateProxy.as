package br.com.tratomais.einsurance.customer.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	
	import mx.rpc.IResponder;
	
	public class CityDelegateProxy
	{
		private var responder : IResponder;  
        private var service : Object;  
        
		public function CityDelegateProxy(pResponder : IResponder)
		{
			
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_CITY);
			responder = pResponder;			
		}

        public function findById(id:int):void  
        {  
            var call:Object = service.findById(id);  
            call.addResponder(responder);  
        }  
  
        public function findByName(name:String):void  
        {  
            var call:Object = service.findByName(name);  
            call.addResponder(responder);  
        }  
  
        public function listAll():void  
        {  
            var call:Object = service.listAllCity();  
            call.addResponder(responder);  
        }
        
		public function listIdentifiedListCity(identifiedList:IdentifiedList):void
		{
			var call:Object = service.listIdentifiedListCity(identifiedList);  
            call.addResponder(responder); 
		}
		
		public function listCityByState(identifiedList:IdentifiedList, stateId:int):void
		{
			var call:Object = service.listCityByState(identifiedList, stateId);  
            call.addResponder(responder); 
		}
  
	}
}