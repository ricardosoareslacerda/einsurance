package br.com.tratomais.einsurance.customer.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	import br.com.tratomais.einsurance.customer.model.vo.Broker;
	
	import mx.rpc.IResponder;
    
	public class BrokerDelegateProxy
	{
		private var responder : IResponder;
		private var service : Object;
		
		public function BrokerDelegateProxy(pResponder : IResponder )
		{
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_BROKER);
			responder = pResponder;
		}   

		public function findByName(name:String):void
		{
			var call:Object = service.findByName(name);
			call.addResponder(responder);
		}

		public function findByNickName(userName:String):void
		{
			var call:Object = service.findByNickName(userName);
			call.addResponder(responder);
		}

		public function deleteObject(broker:Broker):void
		{
			var call:Object = service.deleteBroker(broker);
			call.addResponder(responder);
		}

		public function findById(id:Number):void
		{
			var call:Object = service.findById(id);
			call.addResponder(responder);
		}

		public function listAll():void
		{
			var call:Object = service.listAllBroker();
			call.addResponder(responder);
		}

		public function saveObject(brokerVO:Broker):void
		{
			var call:Object = service.saveBroker(brokerVO);
			call.addResponder(responder);
		}
	}
}	