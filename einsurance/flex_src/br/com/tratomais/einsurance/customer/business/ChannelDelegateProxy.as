	package br.com.tratomais.einsurance.customer.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	import br.com.tratomais.einsurance.customer.model.vo.Channel;
	import br.com.tratomais.einsurance.useradm.model.vo.Partner;
	
	import mx.rpc.IResponder;
	
	public class ChannelDelegateProxy
	{
        private var responder : IResponder;  
        private var service : Object;  
          
        public function ChannelDelegateProxy(pResponder : IResponder )  
        {  
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_CHANNEL); 
            responder = pResponder;  
        }     
  
        public function findByName(name:String):void  
        {  
            var call:Object = service.findByName(name);  
            call.addResponder(responder);  
        }  
  
        public function findByNickName(userName:String):void  
        {  
            var call:Object = service.findByNickName(userName);  
            call.addResponder(responder);  
        }  
  
        public function findById(id:Number):void  
        {  
            var call:Object = service.findById(id);  
            call.addResponder(responder);  
        }  
  
  		public function findChannelByPartnerId(partnerId:int, onlyActive : Boolean):void{
            var call:Object = service.findChannelByPartnerId(partnerId, onlyActive);  
            call.addResponder(responder);    		
  		}
  
        public function listAll():void  
        {  
            var call:Object = service.listAllChannel();  
            call.addResponder(responder);  
        }  
  
        public function deleteObject(channelVO:Channel):void  
        {  
            var call:Object = service.deleteChannel(channelVO);  
            call.addResponder(responder);  
        }  
  
        public function saveObject(channelVO:Channel):void  
        {  
            var call:Object = service.saveChannel(channelVO);  
            call.addResponder(responder);  
        }
        
        public function findByNameAndPartner(name:String, partner:Partner):void  
        {  
            var call:Object = service.findByNameAndPartner( name, partner );  
            call.addResponder(responder);  
        }         
        
		public function listUserByPartnerId(partnerId : int):void{
            var call:Object = service.listUserByPartnerId( partnerId );  
            call.addResponder(responder);  
		}
		
		public function fillListUserByChannel(channel : Channel):void{
            var call:Object = service.fillListUserByChannel( channel );  
            call.addResponder(responder);  
		}
          
		public function findByLoggedUserAndPartner(partner:int):void{
            var call:Object = service.findByLoggedUserAndPartner(partner);  
            call.addResponder(responder);  
		}
	}
}