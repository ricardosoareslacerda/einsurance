package br.com.tratomais.einsurance.customer.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	import br.com.tratomais.einsurance.policy.model.vo.Endorsement;
	
	import mx.rpc.IResponder;
	
	public class DataOptionDelegateProxy
	{
		
		private var responder : IResponder;  
        private var service : Object; 
        		
		public function DataOptionDelegateProxy(pResponder : IResponder)
		{
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_DATA_OPTION);
			responder = pResponder;			
		}

        public function getDataOptionById(id:int):void  
        {  
            var call:Object = service.getDataOptionById(id);  
            call.addResponder(responder);  
        }  
  
        public function findByName(name:String):void  
        {  
            var call:Object = service.findByName(name);  
            call.addResponder(responder);  
        }  
  
        public function listAll():void  
        {  
            var call:Object = service.listAllDataOption();  
            call.addResponder(responder);  
        }  

		public function findDataOptionByDataGroup(dataGroupId:int):void{
            var call:Object = service.findDataOptionByDataGroup(dataGroupId);  
            call.addResponder(responder);
		}
		
		public function findDataOptionByDataGroupByActive(dataGroupId:int, active:Boolean):void{
            var call:Object = service.findDataOptionByDataGroup(dataGroupId, active);  
            call.addResponder(responder);
		}		
		
		public function findIdentifiedListDataOptionByDataGroup(dataGroupId:int, identifierString:String, data: Date):void {
			var call:Object =  service.findIdentifiedListDataOptionByDataGroup(dataGroupId, identifierString, data);		
			call.addResponder(responder); 	
		}
		
		public function findIdentifiedListDataOptionByDataGroupParentId(dataGroupId:int, parentId:int, identifierString:String, data: Date):void {
			var call:Object =  service.findIdentifiedListDataOptionByDataGroupParentId(dataGroupId, parentId, identifierString, data);		
			call.addResponder(responder); 	
		}
		
		
	}
}