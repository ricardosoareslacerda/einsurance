package br.com.tratomais.einsurance.customer.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	import br.com.tratomais.einsurance.customer.model.vo.Subsidiary;
	
	import mx.rpc.IResponder;
    
	public class SubsidiaryDelegateProxy
	{
		private var responder : IResponder;
		private var service : Object;
		
		public function SubsidiaryDelegateProxy(pResponder : IResponder )
		{
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_SUBSIDIARY);
			responder = pResponder;
		}   

		public function findByName(name:String):void
		{
			var call:Object = service.findByName(name);
			call.addResponder(responder);
		}

		public function findByNickName(userName:String):void
		{
			var call:Object = service.findByNickName(userName);
			call.addResponder(responder);
		}

		public function deleteObject(subsidiary:Subsidiary):void
		{
			var call:Object = service.deleteSubsidiary(subsidiary);
			call.addResponder(responder);
		}

		public function findById(id:Number):void
		{
			var call:Object = service.findById(id);
			call.addResponder(responder);
		}

		public function listAllSubsidiary():void
		{
			var call:Object = service.listAllSubsidiary();
			call.addResponder(responder);
		}


		public function listAllActiveSubsidiary():void
		{
			var call:Object = service.listAllActiveSubsidiary();
			call.addResponder(responder);
		}

		public function saveObject(subsidiary:Subsidiary):void
		{
			var call:Object = service.saveSubsidiary(subsidiary);
			call.addResponder(responder);
		}
		
		/**
		 * Checks if external code exists for Insurer/Subsidiary 
		 * @param insurerId Insures's record id
		 * @param subsidiaryId subsidiary's record id
		 * @param externalCode External code to be checked
		 */
		public function subsidiaryExternalCodeExists(insurerId: int, subsidiaryId: int, externalCode:String):void {
			var call:Object = service.subsidiaryExternalCodeExists(insurerId,subsidiaryId,externalCode);
			call.addResponder(responder);
		}
		
	}
}	