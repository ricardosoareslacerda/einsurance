package br.com.tratomais.einsurance.customer.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	
	import mx.rpc.IResponder;
	
	public class StateDelegateProxy
	{
		private var responder : IResponder;  
        private var service : Object;  
        
		public function StateDelegateProxy(pResponder : IResponder)
		{
			
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_STATE);
			responder = pResponder;			
		}

        public function findById(id:int):void  
        {  
            var call:Object = service.findById(id);  
            call.addResponder(responder);  
        }  
  
        public function findByName(name:String):void  
        {  
            var call:Object = service.findByName(name);  
            call.addResponder(responder);  
        }  
  
        public function listAll():void  
        {  
            var call:Object = service.listAllState();  
            call.addResponder(responder);  
        }  
        
		public function listStateByCountry(identifiedList:IdentifiedList, countryId:int):void
		{
            var call:Object = service.listStateByCountry(identifiedList, countryId);  
            call.addResponder(responder);
		}
  
	}
}