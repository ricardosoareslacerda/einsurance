package br.com.tratomais.einsurance.customer.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	
	import mx.rpc.IResponder;
	
	public class FindCepDelegateProxy
	{
		private var responder : IResponder;  
        private var service : Object;  
        
		public function FindCepDelegateProxy(pResponder : IResponder)
		{
			
			service = UtilitiesService.getInstance().getService(UtilitiesService.FIND_CEP);
			responder = pResponder;			
		}

        public function identifiedListFindCep(identifiedList:IdentifiedList, zipCode:String):void  
        {  
            var call:Object = service.identifiedListFindCep(identifiedList, zipCode);  
            call.addResponder(responder);  
        }
  
	}
}
