package br.com.tratomais.einsurance.customer.business
{  
    import br.com.tratomais.einsurance.core.UtilitiesService;
    import br.com.tratomais.einsurance.useradm.model.vo.Partner;
    import br.com.tratomais.einsurance.useradm.model.vo.User;
    
    import mx.rpc.IResponder;  
      
    public class PartnerDelegateProxy  
    {  
        private var responder : IResponder;  
        private var service : Object;  
          
        public function PartnerDelegateProxy(pResponder : IResponder )  
        {  
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_PARTNER);
			responder = pResponder;
        }     
      
        public function saveObject(partner:Partner):void  
        {  
            var call:Object = service.savePartner(partner);  
            call.addResponder(responder);  
        }  
        
         public function saveObjectInsert(partner:Partner):void  
        {  
            var call:Object = service.savePartnerInsert(partner);  
            call.addResponder(responder);  
        }  
  
        public function listAll():void  
        {  
            var call:Object = service.listAllPartner();  
            call.addResponder(responder);  
        }  
        
        public function listPartnersAll(user : User):void  
        {  
            var call:Object = service.listAllPartner(user);  
            call.addResponder(responder);  
        }  

        public function listAllActivePartner(user : User):void  
        {  
            var call:Object = service.listAllActivePartner(user);  
            call.addResponder(responder);  
        } 
  
  
        public function findById(id:Number):void  
        {  
            var call:Object = service.findById(id);  
            call.addResponder(responder);  
        }  
  
        public function findByName(name:String):void  
        {  
            var call:Object = service.findByName(name);  
            call.addResponder(responder);  
        }  
  
        public function deleteObject(partner : Partner):void  
        {  
            var call:Object = service.deletePartner(partner);  
            call.addResponder(responder);  
        }  
    }  
}