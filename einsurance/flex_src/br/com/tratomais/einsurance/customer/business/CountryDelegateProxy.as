package br.com.tratomais.einsurance.customer.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	
	import mx.rpc.IResponder;
		
	public class CountryDelegateProxy
	{
		private var responder : IResponder;  
        private var service : Object;  
        
		public function CountryDelegateProxy(pResponder : IResponder)
		{
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_COUNTRY);
			responder = pResponder;			
		}

        public function findById(id:int):void  
        {  
            var call:Object = service.findById(id);  
            call.addResponder(responder);  
        }  
  
        public function findByName(name:String):void  
        {  
            var call:Object = service.findByName(name);  
            call.addResponder(responder);  
        }  
  
        public function listAll():void  
        {  
            var call:Object = service.listAllCountry();  
            call.addResponder(responder);  
        }
        
        public function listCountry(identifiedList:IdentifiedList):void  
        {  
            var call:Object = service.listCountry(identifiedList);  
            call.addResponder(responder);  
        } 

	}
}