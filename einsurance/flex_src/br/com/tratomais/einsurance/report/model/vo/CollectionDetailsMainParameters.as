package br.com.tratomais.einsurance.report.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
	[RemoteClass(alias="br.com.tratomais.core.model.report.CollectionDetailsMainParameters")]     
   	[Managed]	
	public class CollectionDetailsMainParameters extends HibernateBean
	{
		private var _lotId:int;
		private var _page:int;
		private var _totalPage:int;
		private var _currentPage:int;
		
		public function CollectionDetailsMainParameters()
		{
		}
		
		public function set lotId( lotId ):void
		{
			this._lotId = lotId;
		}		
		
		public function get lotId():int
		{
			return _lotId;
		}
		
		public function set page( page:int ):void
		{
			this._page = page;
		}
		
		public function get page():int
		{
			return _page;
		}

		public function set totalPage( totalPage:int ):void
		{
			this._totalPage = totalPage;
		}
		
		public function get totalPage():int
		{
			return _totalPage;
		}	
		
		public function set currentPage( currentPage:int ):void
		{
			this._currentPage = currentPage;
		}
		
		public function get currentPage():int
		{
			return _currentPage;
		}		
	}
}