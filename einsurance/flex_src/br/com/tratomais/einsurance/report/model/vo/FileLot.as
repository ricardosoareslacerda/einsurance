package br.com.tratomais.einsurance.report.model.vo
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
	[RemoteClass(alias="br.com.tratomais.core.model.interfaces.FileLot")]     
    [Managed] 	
	public class FileLot extends HibernateBean
	{
		public static const FILE_LOT_STATUS_PENDING:int = 318;
		public static const FILE_LOT_STATUS_PROCESSED:int = 319;
		public static const FILE_LOT_STATUS_ERROR:int = 320;
		
		private var _statusDescription:String;
		private var _typeDescription:String;
	
		private var _fileLotID:int;
		private var _lot:Lot;
		private var _fileLotType:int;
		private var _fileName:String;
		private var _source:String;
		private var _destination:String;
		private var _sendDate:Date;
		private var _recieveDate:Date;
		private var _registryDate:Date;
		private var _processDate:Date;
		private var _recordsQuantity:int;
		private var _recordsAmount:Number;
		private var _errorID:int;
		private var _errorMessage:String;
		private var _fileLotStatus:int;
		private var _fileLotDetailses:ArrayCollection = new ArrayCollection();
		
		public function FileLot()
		{
		}
		
		public function get fileLotID():int
		{
			return _fileLotID;
		}
		
		public function set fileLotID( pData:int ):void
		{
			this._fileLotID = pData;
		}
		
		public function get lot():Lot
		{
			return _lot;
		}
		
		public function set lot( pData:Lot ):void
		{
			this._lot = pData;
		}
		
		public function get fileLotType():int
		{
			return _fileLotType;
		}
		
		public function set fileLotType( pData:int ):void
		{
			this._fileLotType = pData;
		}
		
		public function get fileName():String
		{
			return _fileName;
		}
		
		public function set fileName( pData:String ):void
		{
			this._fileName = pData;
		}

		public function get source():String
		{
			return _source;
		}
		
		public function set source( pData:String ):void
		{
			this._source = pData;
		}
		
		public function get destination():String
		{
			return _destination;
		}
		
		public function set destination( pData:String ):void
		{
			this._destination = pData;
		}
		
		public function get sendDate():Date
		{
			return _sendDate;
		}
		
		public function set sendDate( pData:Date ):void
		{
			this._sendDate = pData;
		}
		
		public function get recieveDate():Date
		{
			return _recieveDate;
		}
		
		public function set recieveDate( pData:Date ):void
		{
			this._recieveDate = pData;
		}
		
		public function get registryDate():Date
		{
			return _registryDate;
		}
		
		public function set registryDate( pData:Date ):void
		{
			this._registryDate = pData;
		}
		
		public function get processDate():Date
		{
			return _processDate;
		}
		
		public function set processDate( pData:Date ):void
		{
			this._processDate = pData;
		}
		
		public function get recordsQuantity():int
		{
			return _recordsQuantity;
		}
		
		public function set recordsQuantity( pData:int ):void
		{
			this._recordsQuantity = pData;
		}
		
		public function get recordsAmount():Number
		{
			return _recordsAmount;
		}
		
		public function set recordsAmount( pData:Number ):void
		{
			this._recordsAmount = pData;
		}
		
		public function get errorID():int
		{
			return _errorID;
		}
		
		public function set errorID( pData:int ):void
		{
			this._errorID = pData;
		}
		
		public function get errorMessage():String
		{
			return _errorMessage;
		}
		
		public function set errorMessage( pData:String ):void
		{
			this._errorMessage = pData;
		}
		
		public function get fileLotStatus():int
		{
			return _fileLotStatus;
		}
		
		public function set fileLotStatus( pData:int ):void
		{
			this._fileLotStatus = pData;
		}
		
		public function get fileLotDetailses():ArrayCollection
		{
			return _fileLotDetailses;
		}
		
		public function set fileLotDetailses( pData:ArrayCollection ):void
		{
			this._fileLotDetailses = pData;
		}

		public function get statusDescription():String
		{
			return _statusDescription;
		}
		
		public function set statusDescription( pData:String ):void
		{
			this._statusDescription = pData;
		}
		
		public function get typeDescription():String
		{
			return _typeDescription;
		}
		
		public function set typeDescription( pData:String ):void
		{
			this._typeDescription = pData;
		}
	}
}