package br.com.tratomais.einsurance.report.model.proxy
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.report.business.ReportDelegateProxy;
	import br.com.tratomais.einsurance.report.model.vo.ReportListData;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	[Bindable]
	public class ReportProxy extends Proxy implements IProxy {
		
		public static const NAME:String = "ReportProxy";
		
		[Embed(source="/assets/images/error.png")]
		public var errorMsg:Class;
		
		public function ReportProxy(data:Object = null){
			super(NAME, data );
		}
		
		public function listReportData(partnerId:int, issuingStatus:int, installmentStatus:int, annulmentType:int, annulmentCode:String, active:Boolean):void{
			var delegate:ReportDelegateProxy = new ReportDelegateProxy(new Responder( onListReportDataResult, onFault));
			delegate.listCertificateReport(partnerId, issuingStatus, installmentStatus, annulmentType, annulmentCode, active);
		}
		
		public function onListReportDataResult(pResultEvt:ResultEvent):void{
			var result:ReportListData=pResultEvt.result as ReportListData;
         	sendNotification(NotificationList.LOAD_REPORT_LIST, result);			
		}
		
		public function loadReportListAnnulment(partnerId:int, productId:int, brokerId:int):void{
			var delegate:ReportDelegateProxy = new ReportDelegateProxy(new Responder(onLoadReportListAnnulmentResult, onFault));
			delegate.loadReportListAnnulment(partnerId, productId, brokerId);
		}
		
		public function onLoadReportListAnnulmentResult(pResultEvt:ResultEvent):void{
			var result:ReportListData=pResultEvt.result as ReportListData ;
         	sendNotification(NotificationList.LOAD_ANNULMENT_LIST, result);			
		}
		
		public function listReportId(contractId:int, endorsementId:int):void{
			var delegate:ReportDelegateProxy = new ReportDelegateProxy(new Responder(onListReportId, onFault));
			delegate.listReportId(contractId, endorsementId);
		}
		
		public function onListReportId(pResultEvt:ResultEvent):void{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection;
         	sendNotification(NotificationList.REPORT_LISTED, result);
		}
		
		public function getReportId(contractId:int, endorsementId:int, reportType:int):void{
			var delegate:ReportDelegateProxy = new ReportDelegateProxy(new Responder(onGetReportId, onFault));
			delegate.getReportId(contractId, endorsementId, reportType);
		}
		
		public function onGetReportId(pResultEvt:ResultEvent):void{
			var result:int=pResultEvt.result as int;
         	sendNotification(NotificationList.REPORT_GET, result);			
		}
		
		private function onFault(pFaultEvt:FaultEvent):void{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
		}
	}
}