package br.com.tratomais.einsurance.report.model.vo
{
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.customer.model.vo.DataOption;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
    [RemoteClass(alias="br.com.tratomais.core.model.report.DataViewInterface")]     
    [Managed]
	public class DataViewInterface extends HibernateBean
	{
		private var _listExchangeType:ArrayCollection = new ArrayCollection;
		private var _listExchange:ArrayCollection = new ArrayCollection;
		private var _listLotStatus:ArrayCollection = new ArrayCollection;
		
		public function DataViewInterface()
		{
		}
		
		public function get listExchangeType():ArrayCollection
		{
			return _listExchangeType;
		}

		public function set listExchangeType( pData:ArrayCollection ):void
		{
			this._listExchangeType = pData;
		}
		
		public function get listExchange():ArrayCollection
		{
			return _listExchange;
		}

		public function set listExchange( pData:ArrayCollection ):void
		{
			this._listExchange = pData;
		}
		
		public function get listLotStatus():ArrayCollection
		{
			return _listLotStatus;
		}

		public function set listLotStatus( pData:ArrayCollection ):void
		{
			if( pData )
			{
				var allStatus:DataOption = new DataOption();
				allStatus.displayOrder = 0;
				allStatus.fieldDescription = "*" + ResourceManager.getInstance().getString('resources', 'allStatus') + "*";
				pData.addItem( allStatus );
				Utilities.sortCollection( pData, "displayOrder", true );
			}
			this._listLotStatus = pData;
		}
	}
}