package br.com.tratomais.einsurance.report.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
	[RemoteClass(alias="br.com.tratomais.core.model.report.CollectionMainParameters")]     
   	[Managed]
	public class CollectionMainParameters extends HibernateBean
	{
		private var _billingMethodId:int;
		private var _effectiveDate:Date;
		private var _expiryDate:Date;
		private var _lotCode:String;
		private var _clientReference:String;
		private var _billingReference:String;
		private var _recordType:int;
		private var _page:int;
		private var _totalPage:int;
		private var _currentPage:int;
		private var _lotType:int;
		private var _exchangeId:int;
		private var _lotId:int;
		private var _statusId;
				
		public function CollectionMainParameters()
		{
		}
		
		public function set billingMethodId( billingMethodId:int ):void
		{
			this._billingMethodId = billingMethodId;
		}
		
		public function get billingMethodId():int
		{
			return _billingMethodId;
		}
		
		public function set effectiveDate( effectiveDate:Date ):void
		{
			this._effectiveDate = effectiveDate;
		}
		
		public function get effectiveDate():Date
		{
			return _effectiveDate;
		}
		
		public function set expiryDate( expiryDate:Date ):void
		{
			this._expiryDate = expiryDate;
		}
		
		public function get expiryDate():Date
		{
			return _expiryDate;
		}	
		
		public function set lotCode( lotCode:String ):void
		{
			this._lotCode = lotCode;
		}
		
		public function get lotCode():String
		{
			return _lotCode;
		}
		
		public function set clientReference( clientReference:String ):void
		{
			this._clientReference = clientReference;
		}
		
		public function get clientReference():String
		{
			return _clientReference;
		}		
		
		public function set billingReference( billingReference:String ):void
		{
			this._billingReference = billingReference;
		}
		
		public function get billingReference():String
		{
			return _billingReference;
		}	
		
		public function set recordType( recordType:int ):void
		{
			this._recordType = recordType;
		}
		
		public function get recordType():int
		{
			return _recordType;
		}

		public function set page( page:int ):void
		{
			this._page = page;
		}
		
		public function get page():int
		{
			return _page;
		}
		
		public function set totalPage( totalPage:int ):void
		{
			this._totalPage = totalPage;
		}
		
		public function get totalPage():int
		{
			return _totalPage;
		}	
		
		public function set currentPage( currentPage:int ):void
		{
			this._currentPage = currentPage;
		}
		
		public function get currentPage():int
		{
			return _currentPage;
		}
		
		public function set lotType( lotType:int ):void
		{
			this._lotType = lotType;
		}
		
		public function get lotType():int
		{
			return _lotType;
		}	
		
		public function set exchangeId( exchangeId:int ):void
		{
			this._exchangeId = exchangeId;
		}
		
		public function get exchangeId():int
		{
			return _exchangeId;
		}
		
		public function set lotId( lotId:int ):void
		{
			this._lotId = lotId;
		}
		
		public function get lotId():int
		{
			return _lotId;
		}	
		public function set statusId( statusId:int ):void
		{
			this._statusId = statusId;
		}
		
		public function get statusId():int
		{
			return _statusId;
		}				
	}
}