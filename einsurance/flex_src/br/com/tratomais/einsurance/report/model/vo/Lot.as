package br.com.tratomais.einsurance.report.model.vo
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
	[RemoteClass(alias="br.com.tratomais.core.model.interfaces.Lot")]     
    [Managed] 
	public class Lot extends HibernateBean
	{
		public static var LOT_STATUS_PENDING:int = 303;
		public static var LOT_STATUS_PROCESSED:int = 304;
		public static var LOT_STATUS_ERROR:int = 305;
		
		private var _statusDescription:String;
		
		private var _lotID:int;
		private var _parentLotID:int;
		private var _exchange:Exchange;
		private var _insurerID:int;
		private var _partnerID:int;
		private var _lotCode:String;
		private var _lotType:int;
		private var _fileName:String;
		private var _source:String;
		private var _destination:String;
		private var _sendDate:Date;
		private var _recieveDate:Date;
		private var _processDate:Date;
		private var _recordsQuantity:int;
		private var _recordsAmount:Number;
		private var _errorID:int;
		private var _errorMessage:String;
		private var _lotStatus:int;
		private var _fileLots:ArrayCollection = new ArrayCollection();
		
		public function Lot()
		{
		}
		
		public function get lotID():int
		{
			return _lotID;
		}
		
		public function set lotID( pData:int ):void
		{
			this._lotID = pData;
		}
		
		public function get parentLotID():int
		{
			return _parentLotID;
		}
		
		public function set parentLotID( pData:int ):void
		{
			this._parentLotID = pData;
		}
		
		public function get exchange():Exchange
		{
			return _exchange;
		}
		
		public function set exchange( pData:Exchange ):void
		{
			this._exchange = pData;
		}
		
		public function get insurerID():int
		{
			return _insurerID;
		}
		
		public function set insurerID( pData:int ):void
		{
			this._insurerID = pData;
		}
		
		public function get partnerID():int
		{
			return _partnerID;
		}
		
		public function set partnerID( pData:int ):void
		{
			this._partnerID = pData;
		}
		
		public function get lotCode():String
		{
			return _lotCode;
		}
		
		public function set lotCode( pData:String ):void
		{
			this._lotCode = pData;
		}
		
		public function get lotType():int
		{
			return _lotType;
		}
		
		public function set lotType( pData:int ):void
		{
			this._lotType = pData;
		}
		
		public function get fileName():String
		{
			return _fileName;
		}
		
		public function set fileName( pData:String ):void
		{
			this._fileName = pData;
		}
		
		public function get source():String
		{
			return _source;
		}
		
		public function set source( pData:String ):void
		{
			this._source = pData;
		}
		
		public function get destination():String
		{
			return _destination;
		}
		
		public function set destination( pData:String ):void
		{
			this._destination = pData;
		}

		public function get sendDate():Date
		{
			return _sendDate;
		}
		
		public function set sendDate( pData:Date ):void
		{
			this._sendDate = pData;
		}
		
		public function get recieveDate():Date
		{
			return _recieveDate;
		}
		
		public function set recieveDate( pData:Date ):void
		{
			this._recieveDate = pData;
		}
		
		public function get processDate():Date
		{
			return _processDate;
		}
		
		public function set processDate( pData:Date ):void
		{
			this._processDate = pData;
		}
		
		public function get recordsQuantity():int
		{
			return _recordsQuantity;
		}
		
		public function set recordsQuantity( pData:int ):void
		{
			this._recordsQuantity = pData;
		}
		
		public function get recordsAmount():Number
		{
			return _recordsAmount;
		}
		
		public function set recordsAmount( pData:Number ):void
		{
			this._recordsAmount = pData;
		}
		
		public function get errorID():int
		{
			return _errorID;
		}
		
		public function set errorID( pData:int ):void
		{
			this._errorID = pData;
		}
		
		public function get errorMessage():String
		{
			return _errorMessage;
		}
		
		public function set errorMessage( pData:String ):void
		{
			this._errorMessage = pData;
		}
		
		public function get lotStatus():int
		{
			return _lotStatus;
		}
		
		public function set lotStatus( pData:int ):void
		{
			this._lotStatus = pData;
		}
		
		public function set fileLots( pData:ArrayCollection ):void
		{
			this._fileLots = pData;
		}
		
		public function get fileLots():ArrayCollection
		{
			return _fileLots;
		}
		
		public function get statusDescription():String
		{
			return _statusDescription;
		}
		
		public function set statusDescription( pData:String ):void
		{
			this._statusDescription = pData;
		}
	}
}