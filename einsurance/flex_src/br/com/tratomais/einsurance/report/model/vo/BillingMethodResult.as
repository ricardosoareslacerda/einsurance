package br.com.tratomais.einsurance.report.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
    [RemoteClass(alias="br.com.tratomais.core.model.report.BillingMethodResult")]     
    [Managed]		
	public class BillingMethodResult extends HibernateBean
	{
		private var _exchangeId:int;
		private var _exchangeType:int;
		private var _chartAccountId:int;
		private var _name:String;
		 
		public function BillingMethodResult()
		{
		}
		
		public function set exchangeId( exchangeId:int ):void
		{
			this._exchangeId = exchangeId;
		}
		
		public function get exchangeId():int
		{
			return _exchangeId;
		}
				
		public function get exchangeType():int
		{
			return _exchangeType;
		}
		
		public function set exchangeType( pData:int ):void
		{
			this._exchangeType = pData;
		}
		
		public function set chartAccountId( chartAccountId:int ):void
		{
			this._chartAccountId = chartAccountId;
		}
		
		public function get chartAccountId():int
		{
			return _chartAccountId;
		}		
		
		public function set name( name:String ):void
		{
			this._name = name;
		}
		
		public function get name():String
		{
			return _name;
		}
	}
}