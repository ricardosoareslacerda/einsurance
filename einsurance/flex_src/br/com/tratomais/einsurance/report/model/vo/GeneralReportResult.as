////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Bruno Souza
// @version 1.0
// @lastModified 28/01/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.report.model.vo
{  
      
  	import fr.kapit.logger.INamedQueue;
  	
  	import net.digitalprimates.persistence.hibernate.HibernateBean;
      
    [RemoteClass(alias="br.com.tratomais.core.model.report.GeneralReportResult")]     
    [Managed]    
    public class GeneralReportResult extends HibernateBean 
    {  
  		private var _endorsementId:int;
  		private var _contractId:int;
        private var _policyNumber:Number;
        private var _certificateNumber:Number;
        private var _productName:String;
        private var _insuredName:String;
        private var _paymentTerm:String;
        private var _issuingStatus:String;
  
        public function GeneralReportResult(){  
        }  

  		public function get endorsementId():int{
  			return _endorsementId;
  		}
  		
  		public function set endorsementId(pData:int):void{
  			_endorsementId = pData;
  		}
  		
  		public function get contractId():int{
  			return _contractId;	
  		}
  		  		
  		public function set contractId(pData:int){
  			_contractId = pData;	
  		}  		
  
        public function get policyNumber():Number{  
            return _policyNumber;  
        }  
  
        public function set policyNumber(pData:Number):void{  
            _policyNumber=pData;  
        }  
  
        public function get certificateNumber():Number{  
            return _certificateNumber;  
        }  
  
        public function set certificateNumber(pData:Number):void{  
            _certificateNumber=pData;  
        }  
  
        public function get insuredName():String{  
            return _insuredName;  
        }  
  
        public function set insuredName(pData:String):void{  
            _insuredName=pData;  
        }  
  
        public function get productName():String{  
            return _productName;  
        }  
  
        public function set productName(pData:String):void{  
            _productName=pData;  
        }  
  
        public function get paymentTerm():String{  
            return _paymentTerm;  
        }  
  
        public function set paymentTerm(pData:String):void{  
            _paymentTerm=pData;  
        }  
  
        public function get issuingStatus():String{  
            return _issuingStatus;  
        }  
  
        public function set issuingStatus(pData:String):void{  
            _issuingStatus=pData;  
        }  
    }  
}