package br.com.tratomais.einsurance.report.model.vo
{
	import mx.collections.ArrayCollection;
	
	[RemoteClass(alias="br.com.tratomais.core.model.report.ReportListData")]      
    [Managed]  
	public class ReportListData
	{
		public function ReportListData(){}
		
		public var listChannel:ArrayCollection;
		public var listIssuingStatus:ArrayCollection;
		public var listInstallmentStatus:ArrayCollection;
		public var listUserPerPartner:ArrayCollection;
		public var listProductsPerPartner:ArrayCollection;
		public var listAnnulmentType:ArrayCollection;
		public var listPartnerPerProductOrBroker:ArrayCollection;
		public var listBrokerPerProductOrPartner:ArrayCollection;
		public var listProductPerPartnerOrBroker:ArrayCollection;
	}
}