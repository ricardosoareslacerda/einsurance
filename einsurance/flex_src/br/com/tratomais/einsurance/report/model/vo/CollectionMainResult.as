package br.com.tratomais.einsurance.report.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
    [RemoteClass(alias="br.com.tratomais.core.model.report.CollectionMainResult")]     
    [Managed]		
	public class CollectionMainResult extends HibernateBean
	{
		private var _lotId:int;
		private var _lotStatus:int;
		private var _fieldDescription:String;
		private var _lotCode:String;
		private var _fileName:String;
		private var _sendDate:Date;
		private var _recieveDate:Date;
		private var _quantity:int;
		private var _amount:Number;
		private var _errorMessage:String;
				
		public function CollectionMainResult()
		{
		}
		
		public function set lotStatus( lotStatus:int ):void
		{
			this._lotStatus = lotStatus;
		}
		
		public function get lotStatus():int
		{
			return _lotStatus;
		}
		
		public function set fieldDescription( fieldDescription:String ):void
		{
			this._fieldDescription = fieldDescription;
		}
		
		public function get fieldDescription():String
		{
			return _fieldDescription;
		}		
		
		public function set lotCode( lotCode:String ):void
		{
			this._lotCode = lotCode;
		}
		
		public function get lotCode():String
		{
			return _lotCode;
		}	
		
		public function set fileName( fileName:String ):void
		{
			this._fileName = fileName;
		}
		
		public function get fileName():String
		{
			return _fileName;
		}	
		
		public function set sendDate( sendDate:Date ):void
		{
			this._sendDate = sendDate;
		}
		
		public function get sendDate():Date
		{
			return _sendDate;
		}
		
		public function set recieveDate( recieveDate:Date ):void
		{
			if( recieveDate )
				this._recieveDate = recieveDate;
		}
		
		public function get recieveDate():Date
		{
			return _recieveDate;
		}
		
		public function set quantity( quantity:int ):void
		{
			this._quantity = quantity;
		}
		
		public function get quantity():int
		{
			return _quantity;
		}		

		public function set amount( amount:Number ):void
		{
			this._amount = amount;
		}
		
		public function get amount():Number
		{
			return _amount;
		}
		
		public function set errorMessage( errorMessage:String ):void
		{
			this._errorMessage = errorMessage;
		}
		
		public function get errorMessage():String
		{
			return _errorMessage;
		}	
		
		public function set lotId( lotId:int ):void
		{
			this._lotId = lotId;
		}
		
		public function get lotId():int
		{
			return _lotId;
		}
	}
}