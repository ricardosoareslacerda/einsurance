package br.com.tratomais.einsurance.report.model.vo
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
	[RemoteClass(alias="br.com.tratomais.core.model.interfaces.Exchange")]     
    [Managed]
	public class Exchange extends HibernateBean
	{
		private var _exchangeID:int;
		private var _exchangeType:int;
		private var _partnerID:int;
		private var _name:String;
		private var _description:String;
		private var _source:String;
		private var _destination:String;
		private var _sendClassMethod:String;
		private var _recieveClassMethod:String;
		private var _processClassMethod:String;
		private var _sendSequenceNumber:int;
		private var _recieveSequenceNumber:int;
		private var _lastSendDate:Date;
		private var _lastRecieveDate:Date;
		private var _lastProcessDate:Date;
		private var _exchangeGroup:String;
		private var _registred:Date;
		private var _updated:Date;
		private var _active:Boolean;
		private var _lots:ArrayCollection = new ArrayCollection();
		
		public function Exchange()
		{
		}
		
		public function get exchangeID():int
		{
			return _exchangeID;
		}
		
		public function set exchangeID( pData:int ):void
		{
			this._exchangeID = pData;
		}
		
		public function get exchangeType():int
		{
			return _exchangeType;
		}
		
		public function set exchangeType( pData:int ):void
		{
			this._exchangeType = pData;
		}
		
		public function get partnerID():int
		{
			return _partnerID;
		}
		
		public function set partnerID( pData:int ):void
		{
			this._partnerID = pData;
		}
		public function get name():String
		{
			return _name;
		}
		
		public function set name( pData:String ):void
		{
			this._name = pData;
		}
		
		public function get description():String
		{
			return _description;
		}
		
		public function set description( pData:String ):void
		{
			this._description = pData;
		}
		
		public function get source():String
		{
			return _source;
		}
		
		public function set source( pData:String ):void
		{
			this._source = pData;
		}
		
		public function get destination():String
		{
			return _destination;
		}
		
		public function set destination( pData:String ):void
		{
			this._destination = pData;
		}
		
		public function get sendClassMethod():String
		{
			return _sendClassMethod;
		}
		
		public function set sendClassMethod( pData:String ):void
		{
			this._sendClassMethod = pData;
		}
		
		public function get recieveClassMethod():String
		{
			return _recieveClassMethod;
		}
		
		public function set recieveClassMethod( pData:String ):void
		{
			this._recieveClassMethod = pData;
		}
		
		public function get processClassMethod():String
		{
			return _processClassMethod;
		}
		
		public function set processClassMethod( pData:String ):void
		{
			this._processClassMethod = pData;
		}
		
		public function get sendSequenceNumber():int
		{
			return _sendSequenceNumber;
		}
		
		public function set sendSequenceNumber( pData:int ):void
		{
			this._sendSequenceNumber = pData;
		}
		
		public function get recieveSequenceNumber():int
		{
			return _recieveSequenceNumber;
		}
		
		public function set recieveSequenceNumber( pData:int ):void
		{
			this._recieveSequenceNumber = pData;
		}
		
		public function get lastSendDate():Date
		{
			return _lastSendDate;
		}
		
		public function set lastSendDate( pData:Date ):void
		{
			this._lastSendDate = pData;
		}
		
		public function get lastRecieveDate():Date
		{
			return _lastRecieveDate;
		}
		
		public function set lastRecieveDate( pData:Date ):void
		{
			this._lastRecieveDate = pData;
		}
		
		public function get lastProcessDate():Date
		{
			return _lastProcessDate;
		}
		
		public function set lastProcessDate( pData:Date ):void
		{
			this._lastProcessDate = pData;
		}
		
		public function get exchangeGroup():String
		{
			return _exchangeGroup;
		}
		
		public function set exchangeGroup( pData:String ):void
		{
			this._exchangeGroup = pData;
		}
		
		public function get registred():Date
		{
			return _registred;
		}
		
		public function set registred( pData:Date ):void
		{
			this._registred = pData;
		}
		
		public function get updated():Date
		{
			return _updated;
		}
		
		public function set updated( pData:Date ):void
		{
			this._updated = pData;
		}
		
		public function get active():Boolean
		{
			return _active;
		}
		
		public function set active( pData:Boolean ):void
		{
			this._active = pData;
		}
		
		public function get lots():ArrayCollection
		{
			return _lots;
		}
		
		public function set lots( pData:ArrayCollection ):void
		{
			this._lots = pData;
		}
	}
}