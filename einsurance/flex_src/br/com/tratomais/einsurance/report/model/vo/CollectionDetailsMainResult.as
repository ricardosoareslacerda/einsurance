package br.com.tratomais.einsurance.report.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
    [RemoteClass(alias="br.com.tratomais.core.model.report.CollectionDetailsMainResult")]     
    [Managed]	
	public class CollectionDetailsMainResult extends HibernateBean
	{
		private var _detailsStatus:String;
		private var _lineNumber:int;
		private var _clientReference:String;
		private var _billingReference:String;
		private var _installmentValue:Number;
		private var _recordType:String;
		private var _responseCode:String;
		private var _responseDescription:String;
		private var _textLine:String;
		private var _errorMessage:String;
		private var _fileLotDetailsStatus:int;
		
		public function set detailsStatus( detailsStatus:String):void
		{
			this._detailsStatus = detailsStatus;
		}
		
		public function get detailsStatus():String
		{
			return _detailsStatus;
		}

		public function set lineNumber( lineNumber:int ):void
		{
			this._lineNumber = lineNumber;
		}
		
		public function get lineNumber():int
		{
			return _lineNumber;
		}
		
		public function set clientReference( clientReference:String):void
		{
			this._clientReference = clientReference;
		}
		
		public function get clientReference():String
		{
			return _clientReference;
		}		
		
		public function set billingReference( billingReference:String):void
		{
			this._billingReference = billingReference;
		}
		
		public function get billingReference():String
		{
			return _billingReference;
		}	
		
		public function set installmentValue(installmentValue:Number):void
		{
			this._installmentValue = installmentValue;
		}
		
		public function get installmentValue():Number
		{
			return _installmentValue;
		}
		
		public function set recordType( recordType:String):void
		{
			this._recordType = recordType;
		}
		
		public function get recordType():String
		{
			return _recordType;
		}	
		
		public function set responseCode( responseCode:String):void
		{
			this._responseCode = responseCode;
		}
		
		public function get responseCode():String
		{
			return _responseCode;
		}	

		public function set responseDescription( responseDescription:String):void
		{
			this._responseDescription = responseDescription;
		}
		
		public function get responseDescription():String
		{
			return _responseDescription;
		}
		
		public function set textLine( textLine:String):void
		{
			this._textLine = textLine;
		}
		
		public function get textLine():String
		{
			return _textLine;
		}	
		
		public function set errorMessage( errorMessage:String):void
		{
			this._errorMessage = errorMessage;
		}
		
		public function get errorMessage():String
		{
			return _errorMessage;
		}
		
		public function set fileLotDetailsStatus( fileLotDetailsStatus:int ):void
		{
			this._fileLotDetailsStatus = fileLotDetailsStatus;
		}
		
		public function get fileLotDetailsStatus():int
		{
			return _fileLotDetailsStatus;
		}			
	}
}