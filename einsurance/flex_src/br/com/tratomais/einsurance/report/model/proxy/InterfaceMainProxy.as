package br.com.tratomais.einsurance.report.model.proxy
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	import br.com.tratomais.einsurance.report.business.InterfaceMainDelegateProxy;
	import br.com.tratomais.einsurance.report.model.vo.DataViewInterface;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	public class InterfaceMainProxy extends Proxy implements IProxy
	{
	
		public static const NAME:String = "InterfaceMainProxy";
		
		public function InterfaceMainProxy( data:Object = null )
		{
			super( NAME, data );
		}

		public function listLot( identifiedList:IdentifiedList, exchangeId:int, lotStatus:int, rangeBefore:Date, rangeAfter:Date ):void
		{
			var delegate:InterfaceMainDelegateProxy = new InterfaceMainDelegateProxy( new Responder(onListIdentified, onFault) );
			delegate.listLot( identifiedList, exchangeId, lotStatus, rangeBefore, rangeAfter );
		}
		
		public function listFileLot( identifiedList:IdentifiedList, lotId:int ):void
		{
			var delegate:InterfaceMainDelegateProxy = new InterfaceMainDelegateProxy( new Responder(onListIdentified, onFault) );
			delegate.listFileLot( identifiedList, lotId );
		}
		
		public function listFileLotDetails( identifiedList:IdentifiedList, fileLotId:int ):void
		{
			var delegate:InterfaceMainDelegateProxy = new InterfaceMainDelegateProxy( new Responder(onListIdentified, onFault) );
			delegate.listFileLotDetails( identifiedList, fileLotId );
		}
		
		public function onListIdentified( resultEvent:ResultEvent ):void
		{
			var result:IdentifiedList = resultEvent.result as IdentifiedList;
			sendNotification( NotificationList.INTERFACE_MAIN_IDENTIFIED, result );
		}
		
		public function getDataViewInterface():void
		{
			var delegate:InterfaceMainDelegateProxy = new InterfaceMainDelegateProxy( new Responder(onGetDataViewInterface, onFault) );
			delegate.getDataViewInterface();			
		}
		
		public function onGetDataViewInterface( resultEvent:ResultEvent ):void
		{
			var result:DataViewInterface = resultEvent.result as DataViewInterface;
			sendNotification( NotificationList.LOAD_INTERFACE_MAIN, result );
		}
		
		private function onFault( pFaultEvt:FaultEvent ):void
		{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
		}
	}
}