package br.com.tratomais.einsurance.report.model.vo
{
    [RemoteClass(alias="br.com.tratomais.core.model.report.Report")]
    [Managed]
	public class Report
	{
		private var _reportId:int;
		private var _name:String;
		
		public function set reportId(reportId:int):void{
			this._reportId = reportId;
		}
		
		public function get reportId():int{
			return _reportId;
		}
		
		public function set name(name:String):void{
			this._name = name;
		}
		
		public function get name():String{
			return _name;
		}
	}
}