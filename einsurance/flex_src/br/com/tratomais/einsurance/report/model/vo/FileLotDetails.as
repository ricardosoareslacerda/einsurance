package br.com.tratomais.einsurance.report.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
	[RemoteClass(alias="br.com.tratomais.core.model.interfaces.FileLotDetails")]     
    [Managed] 
	public class FileLotDetails extends HibernateBean
	{
		public static const FILE_LOT_DETAIL_STATUS_PENDING:int = 321;
		public static const FILE_LOT_DETAIL_STATUS_PROCESSED:int = 322;
		public static const FILE_LOT_DETAIL_STATUS_ERROR:int = 323;
		
		private var _policyNumber:String;
		private var _certificateNumber:String;
		private var _subsidiaryID:int;
		private var _statusDescription:String;		
		
		private var _fileLotDetailsID:int;
		private var _fileLot:FileLot;
		private var _recordType:int;
		private var _operation:String;
		private var _masterPolicyID:int;
		private var _contractID:int;
		private var _endorsementID:int;
		private var _installmentID:int;
		private var _itemID:int;
		private var _lineNumber:int;
		private var _textLine:String;
		private var _registryDate:Date;
		private var _processDate:Date;
		private var _errorID:int;
		private var _errorMessage:String;
		private var _responseCode:String;
		private var _responseDescription:String;
		private var _fileLotDetailsStatus:int;
		
		public function FileLotDetails()
		{
		}
		
		public function get fileLotDetailsID():int
		{
			return _fileLotDetailsID;
		}
		
		public function set fileLotDetailsID( pData:int ):void
		{
			this._fileLotDetailsID = pData;
		}

		public function get fileLot():FileLot
		{
			return _fileLot;
		}
		
		public function set fileLot( pData:FileLot ):void
		{
			this._fileLot = pData;
		}
		
		public function get recordType():int
		{
			return _recordType;
		}
		
		public function set recordType( pData:int ):void
		{
			this._recordType = pData;
		}
		
		public function get operation():String
		{
			return _operation;
		}
		
		public function set operation( pData:String ):void
		{
			this._operation = pData;
		}
		
		public function get masterPolicyID():int
		{
			return _masterPolicyID;
		}
		
		public function set masterPolicyID( pData:int ):void
		{
			this._masterPolicyID = pData;
		}
		
		public function get contractID():int
		{
			return _contractID;
		}
		
		public function set contractID( pData:int ):void
		{
			this._contractID = pData;
		}
		
		public function get endorsementID():int
		{
			return _endorsementID;
		}
		
		public function set endorsementID( pData:int ):void
		{
			this._endorsementID = pData;
		}
		
		public function get installmentID():int
		{
			return _installmentID;
		}
		
		public function set installmentID( pData:int ):void
		{
			this._installmentID = pData;
		}
		
		public function get itemID():int
		{
			return _itemID;
		}
		
		public function set itemID( pData:int ):void
		{
			this._itemID = pData;
		}
		
		public function get lineNumber():int
		{
			return _lineNumber;
		}
		
		public function set lineNumber( pData:int ):void
		{
			this._lineNumber = pData;
		}
		
		public function get textLine():String
		{
			return _textLine;
		}
		
		public function set textLine( pData:String ):void
		{
			this._textLine = pData;
		}
		
		public function get registryDate():Date
		{
			return _registryDate;
		}
		
		public function set registryDate( pData:Date ):void
		{
			this._registryDate = pData;
		}
		
		public function get processDate():Date
		{
			return _processDate;
		}
		
		public function set processDate( pData:Date ):void
		{
			this._processDate = pData;
		}
		
		public function get errorID():int
		{
			return _errorID;
		}
		
		public function set errorID( pData:int ):void
		{
			this._errorID = pData;
		}
		
		public function get errorMessage():String
		{
			return _errorMessage;
		}
		
		public function set errorMessage( pData:String ):void
		{
			this._errorMessage = pData;
		}
		
		public function get responseCode():String
		{
			return _responseCode;
		}
		
		public function set responseCode( pData:String ):void
		{
			this._responseCode = pData;
		}
		
		public function get responseDescription():String
		{
			return _responseDescription;
		}
		
		public function set responseDescription( pData:String ):void
		{
			this._responseDescription = pData;
		}
		
		public function get fileLotDetailsStatus():int
		{
			return _fileLotDetailsStatus;
		}
		
		public function set fileLotDetailsStatus( pData:int ):void
		{
			this._fileLotDetailsStatus = pData;
		}
		
		public function get policyNumber():String{  
            return _policyNumber;  
        }  
  
        public function set policyNumber(pData:String):void{  
            _policyNumber=pData;  
        } 
        
        public function get certificateNumber():String{  
            return _certificateNumber;  
        }  
  
        public function set certificateNumber(pData:String):void{  
            _certificateNumber=pData;  
        }
        
		public function get subsidiaryID():int
		{
			return _subsidiaryID;
		}
		
		public function set subsidiaryID( pData:int ):void
		{
			this._subsidiaryID = pData;
		}
		
		public function get statusDescription():String
		{
			return _statusDescription;
		}
		
		public function set statusDescription( pData:String ):void
		{
			this._statusDescription = pData;
		}
	}
}