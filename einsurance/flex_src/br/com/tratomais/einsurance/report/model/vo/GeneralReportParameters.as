////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Bruno Souza
// @version 1.0
// @lastModified 28/01/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.report.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;

    [RemoteClass(alias="br.com.tratomais.core.model.report.GeneralReportParameters")]     
   	[Managed]   
    public class GeneralReportParameters extends HibernateBean    
    {  
        private var _partnerId:int;  
        private var _channelId:int;
        private var _productId:int;  
        private var _issuingStatus:int;  
        private var _effectiveDate:Date;
        private var _breakDate:Date;
        private var _paymentDate:Date;  
        private var _installmentId:int;
        private var _userId:int;
  
        public function GeneralReportParameters(){  
        }  
  		
        public function get partnerId():int{  
            return _partnerId;  
        }  
  
        public function set partnerId(pData:int):void{  
            _partnerId=pData;  
        }
          
        public function get channelId():int{  
            return _channelId;  
        }
  
        public function set channelId(pData:int):void{  
            _channelId=pData;  
        }
  
        public function get productId():int{  
            return _productId;  
        }  
  
        public function set productId(pData:int):void{  
            _productId=pData;  
        }
  
        public function get issuingStatus():int{  
            return _issuingStatus;  
        }
  
        public function set issuingStatus(pData:int):void{  
            _issuingStatus=pData;  
        }
  
        public function get effectiveDate():Date{  
            return _effectiveDate;  
        }
  
        public function set effectiveDate(pData:Date):void{  
            _effectiveDate=pData;  
        }
  
        public function get breakDate():Date{  
            return _breakDate;  
        }
  
        public function set breakDate(pData:Date):void{  
            _breakDate=pData;  
        }
  
        public function get paymentDate():Date{  
            return _paymentDate;  
        }
  
        public function set paymentDate(pData:Date):void{  
            _paymentDate=pData;  
        }
          		
        public function get installmentId():int{  
            return _installmentId;  
        }  
  
        public function set installmentId(pData:int):void{  
            _installmentId=pData;  
        }
          
        public function get userId():int{  
            return _userId;  
        }
  
        public function set userId(pData:int):void{  
            _userId=pData;  
        }
    }  
}