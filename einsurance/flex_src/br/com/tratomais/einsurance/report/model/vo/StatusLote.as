package br.com.tratomais.einsurance.report.model.vo
{
    [RemoteClass(alias="br.com.tratomais.core.model.report.StatusLote")]     
    [Managed]	
	public class StatusLote
	{
		private var _statusId:int;
		private var _fieldDescription:String;		
		
		public function set statusId( statusId:int ):void
		{
			this._statusId = statusId;
		}
		
		public function get statusId():int
		{
			return _statusId;
		}		
		
		public function set fieldDescription( fieldDescription:String ):void
		{
			this._fieldDescription = fieldDescription;
		}
		
		public function get fieldDescription():String
		{
			return _fieldDescription;
		}

	}
}