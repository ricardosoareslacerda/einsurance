package br.com.tratomais.einsurance.report.model.proxy
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.components.paging.vo.Paginacao;
	import br.com.tratomais.einsurance.report.business.CollectionMainDelegateProxy;
	import br.com.tratomais.einsurance.report.model.vo.CollectionDetailsMainParameters;
	import br.com.tratomais.einsurance.report.model.vo.CollectionMainParameters;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	[Bindable]
	public class CollectionMainProxy extends Proxy implements IProxy
	{
		public static const NAME:String = "CollectionMainProxy";

		public function CollectionMainProxy( data:Object = null )
		{
			super( NAME, data );
		}

		public function listDetailsLevyPaging( collectionDetailsMainParameters:CollectionDetailsMainParameters ):void
		{
			var delegate:CollectionMainDelegateProxy = new CollectionMainDelegateProxy( new Responder(onListDetailsLevyPaging, onFault) );
			delegate.listDetailsLevyPaging( collectionDetailsMainParameters );			
		}

		public function onListDetailsLevyPaging( resultEvent:ResultEvent ):void
		{
			var result:Paginacao = resultEvent.result as Paginacao;
			sendNotification( NotificationList.SEARCH_DETAILS_LEVY_LISTED, result );
		}

		public function listBillingMethod():void
		{
			var delegate:CollectionMainDelegateProxy = new CollectionMainDelegateProxy( new Responder(onListBillingMethod, onFault) );
			delegate.listBillingMethod();
		}

		public function onListBillingMethod( resultEvent:ResultEvent ):void
		{
			var result:ArrayCollection = resultEvent.result as ArrayCollection;
			sendNotification( NotificationList.BILLING_METHOD_LISTED, result );
		}

		public function listStatusLote():void
		{
			var delegate:CollectionMainDelegateProxy = new CollectionMainDelegateProxy( new Responder(onListStatusLote, onFault) );
			delegate.listStatusLote();			
		}

		public function onListStatusLote( resultEvent:ResultEvent ):void
		{
			var result:ArrayCollection = resultEvent.result as ArrayCollection;
			sendNotification( NotificationList.STATUS_LOT_LISTED, result );			
		}

		public function listLevyPaging( collectionMainParameters:CollectionMainParameters )
		{
			var delegate:CollectionMainDelegateProxy = new CollectionMainDelegateProxy( new Responder(onListLevyPaging, onFault) );
			delegate.listLevyPaging( collectionMainParameters );
		}

		public function onListLevyPaging( resultEvent:ResultEvent ):void
		{
			var result:Paginacao = resultEvent.result as Paginacao;
			sendNotification( NotificationList.SEARCH_LEVY_LISTED, result );
		}

		public function listCollectionPaging( collectionMainParameters:CollectionMainParameters ):void
		{
			var delegate:CollectionMainDelegateProxy = new CollectionMainDelegateProxy( new Responder(onListCollectionPaging, onFault) );
			delegate.listCollectionPaging( collectionMainParameters );			
		}

		public function onListCollectionPaging( resultEvent:ResultEvent ):void
		{
			var result:Paginacao = resultEvent.result as Paginacao;
			sendNotification( NotificationList.SEARCH_COLLECTION_LISTED, result );
		}

		public function listCollectionDetailsPaging( collectionDetailsMainParameters:CollectionDetailsMainParameters ):void
		{
			var delegate:CollectionMainDelegateProxy = new CollectionMainDelegateProxy( new Responder(onListCollectionDetailsPaging, onFault) );
			delegate.listCollectionDetailsPaging( collectionDetailsMainParameters );			
		}

		public function onListCollectionDetailsPaging( resultEvent:ResultEvent ):void
		{
			var result:Paginacao = resultEvent.result as Paginacao;
			sendNotification( NotificationList.SEARCH_COLLECTION_DETAILS_LISTED, result );
		}

		public function listExchangeCollection():void
		{
			var delegate:CollectionMainDelegateProxy = new CollectionMainDelegateProxy( new Responder(onListExchangeCollection, onFault) );
			delegate.listExchangeCollection();	
		}

		public function onListExchangeCollection( resultEvent:ResultEvent ):void
		{
			var result:ArrayCollection = resultEvent.result as ArrayCollection;
			sendNotification( NotificationList.EXCHANGE_COLLECTION_LISTED, result );
		}

		private function onFault( pFaultEvt:FaultEvent ):void
		{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
		}
	}
}