package br.com.tratomais.einsurance.report.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	
	import mx.rpc.IResponder;
	
	public class InterfaceMainDelegateProxy
	{
		private var responder : IResponder;
		private var service : Object;
				
		public function InterfaceMainDelegateProxy( pResponder : IResponder )
		{
			service = UtilitiesService.getInstance().getService( UtilitiesService.SERVICE_INTERFACE );
			responder = pResponder;
		}
		
		public function listLot( identifiedList:IdentifiedList, exchangeId:int, lotStatus:int, rangeBefore:Date, rangeAfter:Date ):void
		{
			var call:Object = service.listLot( identifiedList, exchangeId, lotStatus, rangeBefore, rangeAfter );
			call.addResponder( responder );			
		}
		
		public function listFileLot( identifiedList:IdentifiedList, lotId:int ):void
		{
			var call:Object = service.listFileLot( identifiedList, lotId );
			call.addResponder( responder );			
		}
		
		public function listFileLotDetails( identifiedList:IdentifiedList, fileLotId:int ):void
		{
			var call:Object = service.listFileLotDetails( identifiedList, fileLotId );
			call.addResponder( responder );			
		}
		
		public function getDataViewInterface():void
		{
			var call:Object = service.getDataViewInterface();
			call.addResponder( responder );			
		}
	}
}