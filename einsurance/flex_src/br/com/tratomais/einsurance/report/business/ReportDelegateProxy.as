////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Bruno Souza
// @version 1.0
// @lastModified 25/10/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.report.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	
	import mx.rpc.IResponder;
	
	public class ReportDelegateProxy{
		private var responder:IResponder;
		private var service:Object;

		public function ReportDelegateProxy(pResponder:IResponder){
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_REPORT);
			responder = pResponder;
		}
		
		public function listCertificateReport(partnerId:int, issuingStatus:int, installmentStatus:int, annulmentType:int, annulmentCode:String, active:Boolean):void{
			var call:Object = service.loadReportList(partnerId, issuingStatus, installmentStatus, annulmentType, annulmentCode, active);
			call.addResponder(responder);
		}
		
		public function loadReportListAnnulment(partnerId:int, productId:int, brokerId:int):void{
			var call:Object = service.loadReportListAnnulment(partnerId, productId, brokerId);
			call.addResponder(responder);
		}
		
		public function listReportId(contractId:int, endorsementId:int):void{
			var call:Object = service.listReportIdByType(contractId, endorsementId);
			call.addResponder(responder);
		}
		
		public function getReportId(contractId:int, endorsementId:int, reportType:int):void{
			var call:Object = service.getReportIdByType(contractId, endorsementId, reportType);
			call.addResponder(responder);
		}
	}
}