package br.com.tratomais.einsurance.report.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	import br.com.tratomais.einsurance.report.model.vo.CollectionDetailsMainParameters;
	import br.com.tratomais.einsurance.report.model.vo.CollectionMainParameters;
	
	import mx.rpc.IResponder;
	
	public class CollectionMainDelegateProxy
	{
		private var responder : IResponder;
		private var service : Object;

		public function CollectionMainDelegateProxy( pResponder : IResponder )
		{
			service = UtilitiesService.getInstance().getService( UtilitiesService.SERVICE_BATCH_ADMINISTRATION_MAIN );
			responder = pResponder;
		}

		public function listDetailsLevyPaging( parameters:CollectionDetailsMainParameters ):void
		{
			var call:Object = service.listCollectionDetailsMainPaging( parameters );			
			call.addResponder( responder );	
		}

		public function listBillingMethod():void
		{
			var call:Object = service.listBillingMethod();
			call.addResponder( responder );			
		}

		public function listStatusLote():void
		{
			var call:Object = service.listStatusLote();
			call.addResponder( responder );				
		}

		public function listLevyPaging( parameters:CollectionMainParameters ):void
		{
			var call:Object = service.listCollectionMainPaging( parameters );
			call.addResponder( responder );
		}

		public function listCollectionPaging( parameters:CollectionMainParameters ):void
		{
			var call:Object = service.listCollectionMainPaging( parameters );
			call.addResponder( responder );
		}

		public function listCollectionDetailsPaging( parameters:CollectionDetailsMainParameters ):void
		{
			var call:Object = service.listCollectionDetailsMainPaging( parameters );
			call.addResponder( responder );	
		}

		public function listExchangeCollection():void
		{
			var call:Object = service.listExchangeCollection();
			call.addResponder( responder );	
		}
	}
}