////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Jones Silva
// @version 1.0
// @lastModified 02/03/2011
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.report.view.mediator
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.core.components.paging.vo.Paginacao;
	import br.com.tratomais.einsurance.core.components.paging.vo.PagingParameters;
	import br.com.tratomais.einsurance.report.model.proxy.CollectionMainProxy;
	import br.com.tratomais.einsurance.report.model.vo.BillingMethodResult;
	import br.com.tratomais.einsurance.report.model.vo.CollectionDetailsMainParameters;
	import br.com.tratomais.einsurance.report.model.vo.CollectionMainParameters;
	import br.com.tratomais.einsurance.report.model.vo.StatusLote;
	import br.com.tratomais.einsurance.report.view.components.collection.CollectionMain;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.resources.ResourceManager;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class CollectionMainMediator extends Mediator implements IMediator
	{
		private var collectionMainProxy:CollectionMainProxy;
		private var paginationCurrent:String;

		public static const NAME:String = 'CollectionMainMediator';
		
		public function CollectionMainMediator(viewComponent:Object)
		{
			super( NAME, viewComponent );

			collectionMainProxy = CollectionMainProxy( facade.retrieveProxy( CollectionMainProxy.NAME ) );
		}

        public function get collectionMain():CollectionMain
        {  
            return viewComponent as CollectionMain; 
        }

        override public function onRegister():void
        {
        	collectionMainProxy.listBillingMethod();
        	collectionMainProxy.listStatusLote();
        	clearFields();
        	try
        	{
        		collectionMain.addEventListener( CollectionMain.INITIALIZE_PAGE, initialize, false, 0, true );
        		collectionMain.addEventListener( CollectionMain.SEARCH_LEVY, listLevyPaging, false, 0, true );
        		collectionMain.addEventListener( CollectionMain.SEARCH_DETAILS_LEVY, listDetailsLevyPaging, false, 0, true );
        		collectionMain.addEventListener( CollectionMain.SEARCH_COLLECTION, listCollectionPaging, false, 0, true );
        		collectionMain.addEventListener( CollectionMain.SEARCH_COLLECTION_DETAILS, listCollectionDetailsPaging, false, 0, true );
        		
        		collectionMain.addEventListener( CollectionMain.LOAD_LEVY_TO_GRID, loadLevyToGrid, false, 0, true );
        		collectionMain.addEventListener( CollectionMain.LOAD_COLLECTION_TO_GRID, loadCollectionToGrid, false, 0, true );
        		
        		collectionMain.dtgCollectionDetailsPaging.addEventListener( MouseEvent.CLICK, pagination, true, 0, true );
        		collectionMain.dtgCollectionPaging.addEventListener( MouseEvent.CLICK, pagination, true, 0, true );
        		collectionMain.dtgLevyDetailsPaging.addEventListener( MouseEvent.CLICK, pagination, true, 0, true );
        		collectionMain.dtgLevyPaging.addEventListener( MouseEvent.CLICK, pagination, true, 0, true );
        	}
        	catch( error:Error )
        	{
        		Alert.show(error.message, "Error", Alert.OK, null, null, Utilities.errorMsg);
        	}
        }

        override public function onRemove():void
        {
        	super.onRemove();
        	try
        	{
        		collectionMain.removeEventListener( CollectionMain.INITIALIZE_PAGE, initialize );
        		collectionMain.removeEventListener( CollectionMain.SEARCH_LEVY, listLevyPaging );
        		collectionMain.removeEventListener( CollectionMain.SEARCH_DETAILS_LEVY, listDetailsLevyPaging );
        		collectionMain.removeEventListener( CollectionMain.SEARCH_COLLECTION, listCollectionPaging );
        		collectionMain.removeEventListener( CollectionMain.SEARCH_COLLECTION_DETAILS, listCollectionDetailsPaging );
        		
        		collectionMain.removeEventListener( CollectionMain.LOAD_LEVY_TO_GRID, loadLevyToGrid );
        		collectionMain.removeEventListener( CollectionMain.LOAD_COLLECTION_TO_GRID, loadCollectionToGrid );        		
        		
        		collectionMain.dtgCollectionDetailsPaging.removeEventListener( MouseEvent.CLICK, pagination );
        		collectionMain.dtgCollectionPaging.removeEventListener( MouseEvent.CLICK, pagination );
        		collectionMain.dtgLevyDetailsPaging.removeEventListener( MouseEvent.CLICK, pagination );
        		collectionMain.dtgLevyPaging.removeEventListener( MouseEvent.CLICK, pagination );
        		
        		collectionMain.dtgLevyPaging.stateInitial();
        		collectionMain.dtgLevyDetailsPaging.stateInitial();
        		collectionMain.dtgCollectionPaging.stateInitial();
        		collectionMain.dtgCollectionDetailsPaging.stateInitial();  
        		
        		this.clearFields();      		
        	} 
        	catch(error:Error)
        	{
	
        	}
        }

		private function listStatus( listData:ArrayCollection ):void{
			var status:StatusLote = new StatusLote();
			var newList:ArrayCollection = new ArrayCollection();
			
			status.fieldDescription = "*" + ResourceManager.getInstance().getString('resources', 'allStatus') + "*";
			status.statusId = 0;
			newList.addItem( status );
			
			for(var i:int = 0; i < listData.length; i++)
				newList.addItem( listData.getItemAt( i ) );

			collectionMain.cmbStatus.dataProvider = newList;
			collectionMain.cmbStatus.dropdown.dataProvider = newList;			
		}

        override public function listNotificationInterests():Array
        {
        	return [NotificationList.SEARCH_LEVY_LISTED,
        			NotificationList.BILLING_METHOD_LISTED,
        			NotificationList.GRIDVIEW_UPDATE_LIST,
        			NotificationList.SEARCH_DETAILS_LEVY_LISTED,
        			NotificationList.SEARCH_COLLECTION_LISTED,
        			NotificationList.SEARCH_COLLECTION_DETAILS_LISTED,
        			NotificationList.STATUS_LOT_LISTED];
        }

        override public function handleNotification(notification:INotification):void
        {
        	switch( notification.getName())
        	{
        		case NotificationList.BILLING_METHOD_LISTED:
        			collectionMain.cmbBillingMethod.dataProvider = notification.getBody() as ArrayCollection;
        			break;
        		case NotificationList.STATUS_LOT_LISTED:
        			listStatus( notification.getBody() as ArrayCollection );
        			break;
        		case NotificationList.SEARCH_LEVY_LISTED:
        			var result:Paginacao = notification.getBody() as Paginacao;
        			
        			collectionMain.dtgLevyPaging.paginacao = result;
        			collectionMain.viewLevy( true );
					if( collectionMain.dtgLevyPaging.paginacao.totalDados > 0 )
					{
						collectionMain.levyProvider = collectionMain.dtgLevyPaging.paginacao.listaDados;
					}
					else
					{
						Utilities.warningMessage("dataNotFound");
						collectionMain.dtgLevy.dataProvider = null;
					}
        			break;
        		case NotificationList.GRIDVIEW_UPDATE_LIST:
        			var pagingParameters:PagingParameters = notification.getBody() as PagingParameters;
        			
        			if( paginationCurrent == "dtgLevyPaging" )
        			{        			
	        			if( collectionMain.levyParameters != null ) 
	        			{
		        			collectionMain.levyParameters.page = pagingParameters.page;
		        			collectionMain.levyParameters.currentPage = pagingParameters.currentPage;
		        			collectionMain.levyParameters.totalPage = pagingParameters.totalPage;
		        			
		        			collectionMainProxy.listLevyPaging( collectionMain.levyParameters );
		        		}        				
        			}
        			else if( paginationCurrent == "dtgLevyDetailsPaging" )
        			{
        				if( collectionMain.levyDetailsParameters != null )
        				{
		        			collectionMain.levyDetailsParameters.page = pagingParameters.page;
		        			collectionMain.levyDetailsParameters.currentPage = pagingParameters.currentPage;
		        			collectionMain.levyDetailsParameters.totalPage = pagingParameters.totalPage;  
		        			
		        			collectionMainProxy.listDetailsLevyPaging( collectionMain.levyDetailsParameters ); 
		        		}     				
        			}
        			else if( paginationCurrent == "dtgCollectionPaging" )
        			{
	        			if( collectionMain.collectionParameters != null )
	        			{
		        			collectionMain.collectionParameters.page = pagingParameters.page;
		        			collectionMain.collectionParameters.currentPage = pagingParameters.currentPage;
		        			collectionMain.collectionParameters.totalPage = pagingParameters.totalPage; 
		        			
		        			collectionMainProxy.listCollectionPaging( collectionMain.collectionParameters ); 
		        		}      				
        			}
        			else
        			{
        				if( collectionMain.collectionDetailsParameters != null )
        				{
		        			collectionMain.collectionDetailsParameters.page = pagingParameters.page;
		        			collectionMain.collectionDetailsParameters.currentPage = pagingParameters.currentPage;
		        			collectionMain.collectionDetailsParameters.totalPage = pagingParameters.totalPage;
		        			
		        			collectionMainProxy.listCollectionDetailsPaging( collectionMain.collectionDetailsParameters ); 
		        		}         				
        			}

        			break;
        		case NotificationList.SEARCH_DETAILS_LEVY_LISTED:
        			var result:Paginacao = notification.getBody() as Paginacao;
        			
        			collectionMain.dtgLevyDetailsPaging.paginacao = result;
        			if( collectionMain.dtgLevyDetailsPaging.paginacao.totalDados > 0 )
        			{
        				collectionMain.levyDetailsProvider = result.listaDados;
        				collectionMain.dtgLevy.dataProvider = collectionMain.levyCollectionResult;        				
        				collectionMain.viewDetailsLevy( true );
        			}
        			else
        			{
        				Utilities.warningMessage("dataNotFound");
        				collectionMain.dtgLevyDetails.dataProvider = null;
        			}
        			break;
        		case NotificationList.SEARCH_COLLECTION_LISTED:
        			var result:Paginacao = notification.getBody() as Paginacao;
        			
        			collectionMain.dtgCollectionPaging.paginacao = result;
        			if( collectionMain.dtgCollectionPaging.paginacao.totalDados > 0 )
        			{
        				collectionMain.collectionProvider = result.listaDados;
        				collectionMain.dtgLevy.dataProvider = collectionMain.levyCollectionResult;
        				collectionMain.viewCollection( true );
        			}
        			else
        			{
        				Utilities.warningMessage("dataNotFound");
        				collectionMain.dtgCollection.dataProvider = null;
        			}
        			break;
        		case NotificationList.SEARCH_COLLECTION_DETAILS_LISTED:
        			var result:Paginacao = notification.getBody() as Paginacao;
        			
        			collectionMain.dtgCollectionDetailsPaging.paginacao = result;
        			if( collectionMain.dtgCollectionDetailsPaging.paginacao.totalDados > 0 )
        			{
        				collectionMain.collectionDetailsProvider = result.listaDados;
        				collectionMain.dtgCollection.dataProvider = collectionMain.collectionResult;        				
        				collectionMain.viewCollectionDetails( true );
        			}	
        			else
        			{
        				Utilities.warningMessage("dataNotFound");
        				collectionMain.dtgCollectionDetails.dataProvider = null;
        			}        	
        			break;		
        	}
        }

        public function pagination( event:Event ):void
        {
        	paginationCurrent = event.currentTarget.id;
        }

        public function loadLevyToGrid( event:Event ):void
        {
        	collectionMainProxy.listLevyPaging( collectionMain.levyParameters );
        }

        public function loadCollectionToGrid( event:Event ):void
        {
        	collectionMainProxy.listCollectionPaging( collectionMain.collectionParameters );
        }

        public function initialize( event:Event ):void
        {
			this.clearFields();
        }

        public function clearFields():void
        {
        	collectionMain.batchNumber.text = "";
        	collectionMain.txtBillingReference.text = "";
        	collectionMain.txtClientReference.text = "";
        	collectionMain.txtProcessingEndDate.text = "";
        	collectionMain.txtProcessingStartDate.text = "";
        	collectionMain.cmbBillingMethod.selectedItem = null;
        	collectionMain.cmbStatus.selectedItem = null;
        	
        	collectionMain.levyProvider.removeAll();
        	collectionMain.levyProvider.refresh();
        	collectionMain.dtgLevy.data = null;
        	collectionMain.dtgLevy.dataProvider = null;
        	
        	collectionMain.levyDetailsProvider.removeAll();
        	collectionMain.levyDetailsProvider.refresh();
        	collectionMain.dtgLevyDetails.data = null
        	collectionMain.dtgLevyDetails.dataProvider = null;
        	
        	collectionMain.collectionProvider.removeAll();
        	collectionMain.collectionProvider.refresh();
        	collectionMain.dtgCollection.dataProvider = null;
        	collectionMain.dtgCollectionDetails.dataProvider = null;
        	
			collectionMain.collectionDetailsProvider.removeAll();
			collectionMain.collectionDetailsProvider.refresh();
			collectionMain.dtgCollectionDetails.data = null;
			collectionMain.dtgCollectionDetails.dataProvider = null;
        	
        	collectionMain.levyParameters = null;
        	collectionMain.levyDetailsParameters = null;
        	collectionMain.collectionParameters = null;
        	collectionMain.collectionDetailsParameters = null;
        	
        	collectionMain.viewLevy( true );
        }

        public function listLevyPaging( event:Event ):void
        {
       		if( collectionMain.levyParameters == null )
       			collectionMain.levyParameters = new CollectionMainParameters();
        	
        	collectionMain.levyParameters.billingReference = collectionMain.txtBillingReference.text;
        	collectionMain.levyParameters.clientReference = collectionMain.txtClientReference.text;
        	collectionMain.levyParameters.effectiveDate = collectionMain.txtProcessingStartDate.newDate;
        	collectionMain.levyParameters.expiryDate = collectionMain.txtProcessingEndDate.newDate;
        	collectionMain.levyParameters.lotCode = collectionMain.batchNumber.text;
        	collectionMain.levyParameters.statusId = collectionMain.cmbStatus.selectedItem != null?( collectionMain.cmbStatus.selectedItem as StatusLote ).statusId : 0;
        	collectionMain.levyParameters.recordType = 325;
        	collectionMain.levyParameters.exchangeId = (collectionMain.cmbBillingMethod.selectedItem as BillingMethodResult).exchangeId;
        	
        	collectionMain.dtgLevyPaging.pagingParameters.page = 0;
        	collectionMain.dtgLevyPaging.pagingParameters.currentPage = 1;
        	
        	collectionMain.levyParameters.page = collectionMain.dtgLevyPaging.pagingParameters.page;
        	collectionMain.levyParameters.currentPage = collectionMain.dtgLevyPaging.pagingParameters.currentPage;
        	collectionMain.levyParameters.totalPage = collectionMain.dtgLevyPaging.pagingParameters.totalPage;
        	collectionMain.levyParameters.lotType = 301;        		
        	
        	collectionMainProxy.listLevyPaging( collectionMain.levyParameters );
        }

        public function listDetailsLevyPaging( event:Event ):void
        {
        	if( collectionMain.levyDetailsParameters == null )
        		collectionMain.levyDetailsParameters = new CollectionDetailsMainParameters;
        	
        	collectionMain.dtgLevyDetailsPaging.pagingParameters.page = 0;
        	collectionMain.dtgLevyDetailsPaging.pagingParameters.currentPage = 1;
        	
        	collectionMain.levyDetailsParameters.page = collectionMain.dtgLevyDetailsPaging.pagingParameters.page;
        	collectionMain.levyDetailsParameters.currentPage = collectionMain.dtgLevyDetailsPaging.pagingParameters.currentPage;
        	collectionMain.levyDetailsParameters.totalPage = collectionMain.dtgLevyDetailsPaging.pagingParameters.totalPage;
        	collectionMain.levyDetailsParameters.lotId = event.target.levyCollectionResult.lotId as int;
        	
        	collectionMainProxy.listDetailsLevyPaging( collectionMain.levyDetailsParameters );
        }

        public function listCollectionPaging( event:Event ):void
        {
        	if( collectionMain.collectionParameters == null )
        		collectionMain.collectionParameters = new CollectionMainParameters();
        	
        	collectionMain.dtgCollectionPaging.pagingParameters.page = 0;
        	collectionMain.dtgCollectionPaging.pagingParameters.currentPage = 1;
        	
        	collectionMain.collectionParameters.page = collectionMain.dtgCollectionPaging.pagingParameters.page;
        	collectionMain.collectionParameters.currentPage = collectionMain.dtgCollectionPaging.pagingParameters.currentPage;
        	collectionMain.collectionParameters.totalPage = collectionMain.dtgCollectionPaging.pagingParameters.totalPage;
        	collectionMain.collectionParameters.lotId = event.target.levyCollectionResult.lotId as int;
        		
        	collectionMainProxy.listCollectionPaging( collectionMain.collectionParameters );
        }

		public function listCollectionDetailsPaging( event:Event ):void
		{
			if( collectionMain.collectionDetailsParameters == null )
				collectionMain.collectionDetailsParameters = new CollectionDetailsMainParameters();
			
			collectionMain.dtgCollectionDetailsPaging.pagingParameters.page = 0;
			collectionMain.dtgCollectionDetailsPaging.pagingParameters.currentPage = 1;
			
        	collectionMain.collectionDetailsParameters.page = collectionMain.dtgCollectionDetailsPaging.pagingParameters.page;
        	collectionMain.collectionDetailsParameters.currentPage = collectionMain.dtgCollectionDetailsPaging.pagingParameters.currentPage;
        	collectionMain.collectionDetailsParameters.totalPage = collectionMain.dtgCollectionDetailsPaging.pagingParameters.totalPage;
        	collectionMain.collectionDetailsParameters.lotId = event.target.collectionResult.lotId as int;
        	
        	collectionMainProxy.listCollectionDetailsPaging( collectionMain.collectionDetailsParameters );
		}
	}
}