////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Bruno Souza
// @version 1.0
// @lastModified 03/11/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.report.view.mediator {
	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.customer.model.proxy.PartnerProxy;
	import br.com.tratomais.einsurance.report.model.proxy.CollectionMainProxy;
	import br.com.tratomais.einsurance.report.view.components.collection.CollectionReportMain;
	import br.com.tratomais.einsurance.useradm.model.vo.Partner;
	import br.com.tratomais.einsurance.useradm.model.vo.User;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.formatters.DateFormatter;
	import mx.resources.ResourceManager;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
		
	public class CollectionReportMainMediator extends Mediator implements IMediator {
		
		public static const NAME:String = 'CollectionReportMainMediator';

		private var loggedUser:User
		private var stringUrl:String;
		private var reportId:int;
		private var reportOutput:String;
		private var partnerId:String = null;
		private var errorNumber:String = null;
		private var batchNumber:String = null; 
    	private var startDate:Date;
		private var endDate:Date;
		private var dateFormatter = new DateFormatter();

		[Bindable][Embed(source="/assets/images/warning.png")]
		public var warningMsg:Class;

		public function CollectionReportMainMediator(viewComponent:Object = null){  
           super(NAME, viewComponent);           
        } 
        
        public function get collectionReportMain():CollectionReportMain{  
            return viewComponent as CollectionReportMain;  
        } 

		override public function onRegister():void{
			super.onRegister();
			
			try{
				collectionReportMain.addEventListener(CollectionReportMain.SEARCH_REPORT, searchReport);
				collectionReportMain.addEventListener(CollectionReportMain.INITIALIZE_PAGE, initialize);
			}catch(error:Error){
				Alert.show(error.message, "Atención", Alert.OK, null, null, warningMsg) ;	
			}
			
			this.initialize();
    	}

		override public function onRemove():void{
			super.onRemove();
			
			try{
				collectionReportMain.removeEventListener(CollectionReportMain.SEARCH_REPORT, searchReport);
				collectionReportMain.removeEventListener(CollectionReportMain.INITIALIZE_PAGE, initialize);
			}catch(error:Error){
				Alert.show(error.message, "Atención", Alert.OK, null, null, warningMsg) ;	
			}
			
			this.clearFields();
			setViewComponent(null);	
		}
		
        override public function listNotificationInterests():Array{  
            return [NotificationList.USER_PARTNER_REFRESH,
            		NotificationList.EXCHANGE_COLLECTION_LISTED];  
        } 		

        override public function handleNotification(notification:INotification):void{
            switch(notification.getName()){
				case NotificationList.USER_PARTNER_REFRESH:
					var listPartnerResult: ArrayCollection = notification.getBody() as ArrayCollection;
					var partner:Partner = new Partner();
					
					partner.nickName = "*" + ResourceManager.getInstance().getString('messages', 'allPartner') + "*";
					partner.partnerId = 99999;
					listPartnerResult.addItem(partner);
					
					collectionReportMain.cmbxPartner.dataProvider = listPartnerResult
					collectionReportMain.cmbxPartner.dropdown.dataProvider = listPartnerResult;
					break;
				case NotificationList.EXCHANGE_COLLECTION_LISTED:
					var listExchangeCollection: ArrayCollection = notification.getBody() as ArrayCollection;
					
					collectionReportMain.cmbxCollectionType.dataProvider = listExchangeCollection;
					collectionReportMain.cmbxCollectionType.dropdown.dataProvider = listExchangeCollection;
					break;
            }
        }
        
        private function initialize():void{
        	clearFields();
        	loggedUser = ApplicationFacade.getInstance().loggedUser;
        	PartnerProxy(facade.retrieveProxy(PartnerProxy.NAME)).listPartnersByUserAll(loggedUser);
        	CollectionMainProxy(facade.retrieveProxy(CollectionMainProxy.NAME)).listExchangeCollection();
        }
        
        private function clearFields():void{      	
			collectionReportMain.txtProcessingStartDate.text = "";    
			collectionReportMain.txtProcessingEndDate.text = "";
			collectionReportMain.txtBatchNumber.text = "";
			collectionReportMain.txtErrorNumber.text = "";

			collectionReportMain.cmbxPartner.data = null;
			collectionReportMain.cmbxPartner.dataProvider = new ArrayCollection()
			collectionReportMain.cmbxPartner.dropdown.dataProvider = new ArrayCollection();
			
			collectionReportMain.cmbxCollectionType.selectedIndex = -1;
			
			collectionReportMain.frmStartDate.required = true;
			collectionReportMain.frmEndDate.required = true;
			collectionReportMain.frmBatchNumber.required = false;
			collectionReportMain.frmErrorNumber.visible = false;
        }
                
        public function searchReport(event:Event):void{
        	dateFormatter.formatString = "DD/MM/YYYY";
        	stringUrl = Utilities.URL_BASE + Utilities.URL_BASE_REPORT + Utilities.URL_REPORT;
        	reportOutput = collectionReportMain.REPORT_OUTPUT;

			batchNumber = null;
			errorNumber = null;
        	        	
        	if(collectionReportMain.txtBatchNumber.text == ""){
        		startDate = collectionReportMain.txtProcessingStartDate.newDate;
        		endDate = collectionReportMain.txtProcessingEndDate.newDate;
        	}else{
        		startDate = new Date(2000,01,01);
        		endDate = new Date(2078,12,01);
        		batchNumber = collectionReportMain.txtBatchNumber.text;
        	}
        	
        	var exchange:Object = collectionReportMain.cmbxCollectionType.selectedItem as Object;
        	
        	if( exchange.exchangeType == 596 && exchange.exchangeId != 3 ){
        		reportId = 14; //send collection (recaudacion)
        	} else if( exchange.exchangeType == 597 && exchange.exchangeId != 4 ){
        		reportId = 15; //return collection (conciliacion)
        	} else if( exchange.exchangeId == 3 ){
        		reportId = 23; //send affiliation (remesa)
        	} else if( exchange.exchangeId == 4 ){
        		reportId = 24; //return affiliation (retorno)
        	}
        	
			if(collectionReportMain.cmbxPartner.selectedItem != null){
				if((collectionReportMain.cmbxPartner.selectedItem as Partner).partnerId != 99999){
			   		partnerId = (collectionReportMain.cmbxPartner.selectedItem as Partner).partnerId.toString();
				}
			} 
        	
        	stringUrl += "id=" + reportId;
        	stringUrl += "&reportOutput=" + reportOutput;
        	stringUrl += "&ExchangeID_" + reportId + "=" + exchange.exchangeId;
        	stringUrl += "&CollectionDateStart_" + reportId + "=" + dateFormatter.format(startDate).toString();
        	stringUrl += "&CollectionDateEnd_" + reportId + "=" + dateFormatter.format(endDate).toString();
        	stringUrl += "&LotCode_" + reportId + "=" + batchNumber;
        	stringUrl += "&PartnerID_" + reportId + "=" + partnerId;
        	
        	if(reportId == 15 || reportId == 24){
	        	if(collectionReportMain.txtErrorNumber.text != ""){
	        		errorNumber = collectionReportMain.txtErrorNumber.text; 
	        	}           	
        	
        		stringUrl += "&ResponseCode_" + reportId + "=" + errorNumber;
        	}
        	
			if(reportOutput == Utilities.REPORT_PDF){
				collectionReportMain.reportView(stringUrl);	
			}else{
				collectionReportMain.URL = stringUrl;
				collectionReportMain.URL = "";	
			}        	
        }      
	}
}