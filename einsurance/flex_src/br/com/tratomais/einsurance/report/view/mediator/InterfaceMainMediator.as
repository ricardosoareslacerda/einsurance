package br.com.tratomais.einsurance.report.view.mediator
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.components.paging.vo.Paginacao;
	import br.com.tratomais.einsurance.core.components.paging.vo.PagingParameters;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	import br.com.tratomais.einsurance.report.model.proxy.InterfaceMainProxy;
	import br.com.tratomais.einsurance.report.model.vo.DataViewInterface;
	import br.com.tratomais.einsurance.report.model.vo.FileLot;
	import br.com.tratomais.einsurance.report.model.vo.Lot;
	import br.com.tratomais.einsurance.report.view.components.*;
	import br.com.tratomais.einsurance.report.view.components.interfaces.InterfaceMain;
	
	import flash.events.Event;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class InterfaceMainMediator extends Mediator implements IMediator
	{
		private var interfaceMainProxy:InterfaceMainProxy;
		
		public static const NAME:String = "InterfaceMainMediator";
		
		private static const LOT_IDENTIFIED:String = "INTERFACE_MAIN_MEDIATOR.LOT";
		private static const FILE_LOT_IDENTIFIED:String = "INTERFACE_MAIN_MEDIATOR.FILE.LOT";
		private static const FILE_LOT_DETAILS_IDENTIFIED:String = "INTERFACE_MAIN_MEDIATOR.FILE.LOT.DETAILS";
		
		public function InterfaceMainMediator(viewComponent:Object)
		{
			super( NAME, viewComponent );
			
			interfaceMainProxy = InterfaceMainProxy( facade.retrieveProxy( InterfaceMainProxy.NAME ) );
		}
		
		public function get interfaceMain():InterfaceMain
		{
			return viewComponent as InterfaceMain;
		}
		
		override public function onRegister():void
		{
			super.onRegister();
			
			interfaceMainProxy.getDataViewInterface();
			interfaceMain.onClean();
			
			interfaceMain.addEventListener( InterfaceMain.SEARCH_LOT, loadLotToGrid, false, 0, true );
			interfaceMain.addEventListener( InterfaceMain.SEARCH_FILE_LOT, loadFileLotToGrid, false, 0);
			interfaceMain.addEventListener( InterfaceMain.SEARCH_FILE_LOT_DETAILS, loadFileLotDetailsToGrid, false, 0);
		}
		
		override public function onRemove():void
		{
			super.onRemove();
		}
		
		override public function listNotificationInterests():Array
		{
			return [NotificationList.LOAD_INTERFACE_MAIN,
					NotificationList.INTERFACE_MAIN_IDENTIFIED,
					NotificationList.GRIDVIEW_UPDATE_LIST];
		}
		
		override public function handleNotification(notification:INotification):void
		{
        	switch( notification.getName())
        	{
        		case NotificationList.LOAD_INTERFACE_MAIN:
        			var dataViewInterface:DataViewInterface = notification.getBody() as DataViewInterface;
        			interfaceMain.loadDataViewInterface( dataViewInterface );
        			break;
        		case NotificationList.INTERFACE_MAIN_IDENTIFIED:
        			var identifiedList:IdentifiedList = notification.getBody() as IdentifiedList;
					var paginacao:Paginacao = identifiedList.value as Paginacao;
					
					switch( identifiedList.groupId )
					{
						case LOT_IDENTIFIED:
        					interfaceMain.loadGridLot( paginacao.listaDados );
        					interfaceMain.dtgLotPaging.paginacao = paginacao;
							break;
						case FILE_LOT_IDENTIFIED:
        					interfaceMain.loadGridFileLot( paginacao.listaDados );
        					interfaceMain.dtgFileLotPaging.paginacao = paginacao;
							break;
						case FILE_LOT_DETAILS_IDENTIFIED:
        					interfaceMain.loadGridFileLotDetails( paginacao.listaDados );
        					interfaceMain.dtgFileLotDetailsPaging.paginacao = paginacao;
							break;
					}
        			break;
        		case NotificationList.GRIDVIEW_UPDATE_LIST:
        			var pagingParameters:PagingParameters = notification.getBody() as PagingParameters;
        			
        			if( pagingParameters.componentId == interfaceMain.dtgLotPaging.id )
        			{
        				loadLotToGrid();
        			}
        			else if ( pagingParameters.componentId == interfaceMain.dtgFileLotPaging.id )
        			{
        				loadFileLotToGrid();
        			} 
        			else 
        			{
        				loadFileLotDetailsToGrid();
        			}
        			break;
        	}
		}
		
		public function loadLotToGrid( event:Event = null ):void 
		{
			var exchangeID:int = interfaceMain.cmbExchange.selectedValue;
			var lotStatus:int = interfaceMain.cmbStatus.selectedValue;
			var startDate:Date = interfaceMain.txtProcessingStartDate.newDate;
			var endDate:Date = interfaceMain.txtProcessingEndDate.newDate;
			var identifiedList:IdentifiedList = createIdentifiedList( LOT_IDENTIFIED, interfaceMain.dtgLotPaging.pagingParameters, event );
			
			interfaceMainProxy.listLot( identifiedList, exchangeID, lotStatus, startDate, endDate ); 
		}
		
		public function loadFileLotToGrid( event:Event = null ):void
		{
			var lot:Lot = interfaceMain.dtgLot.selectedItem as Lot;
			var identifiedList:IdentifiedList = createIdentifiedList( FILE_LOT_IDENTIFIED, interfaceMain.dtgFileLotPaging.pagingParameters, event );
			
			interfaceMainProxy.listFileLot( identifiedList, lot.lotID ); 
		}
		
		public function loadFileLotDetailsToGrid( event:Event = null ):void
		{
			var fileLot:FileLot = interfaceMain.dtgFileLot.selectedItem as FileLot;
			var identifiedList:IdentifiedList = createIdentifiedList( FILE_LOT_DETAILS_IDENTIFIED, interfaceMain.dtgFileLotDetailsPaging.pagingParameters, event );
			
			interfaceMainProxy.listFileLotDetails( identifiedList, fileLot.fileLotID ); 
		}
		
		private function createIdentifiedList( groupId:String, value:PagingParameters, event:Event ):IdentifiedList
		{
			if( event && !event.cancelable )
			{
				value.page = 0;
				value.currentPage = 1;
			}
			
			var identifiedList:IdentifiedList = new IdentifiedList();
			identifiedList.groupId = groupId;
			identifiedList.value = value;
			
			return identifiedList;
		}
	}
}