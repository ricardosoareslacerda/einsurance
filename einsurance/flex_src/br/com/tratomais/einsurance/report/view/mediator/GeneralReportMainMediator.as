////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Bruno Souza
// @version 1.0
// @lastModified 19/10/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.report.view.mediator
{
	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.NotificationViewsList;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.customer.model.vo.Broker;
	import br.com.tratomais.einsurance.customer.model.vo.Channel;
	import br.com.tratomais.einsurance.customer.model.vo.DataOption;
	import br.com.tratomais.einsurance.products.model.vo.Product;
	import br.com.tratomais.einsurance.report.model.proxy.ReportProxy;
	import br.com.tratomais.einsurance.report.model.vo.GeneralReportParameters;
	import br.com.tratomais.einsurance.report.model.vo.ReportListData;
	import br.com.tratomais.einsurance.report.view.components.general.GeneralReportMain;
	import br.com.tratomais.einsurance.useradm.model.proxy.UserProxy;
	import br.com.tratomais.einsurance.useradm.model.vo.Partner;
	import br.com.tratomais.einsurance.useradm.model.vo.User;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.formatters.DateFormatter;
	import mx.resources.ResourceManager;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class GeneralReportMainMediator extends Mediator implements IMediator
	{
		public static const NAME:String = 'GeneralReportMainMediator';
		
		public static const ISSUING_STATUS:int = 22;
		public static const INSTALLMENT_STATUS:int = 30;
		public static const ANNULMENT_TYPE:int = 3;
		public static const ANNULMENT_CODE:String = 'B';
		
		private var userProxy:UserProxy;
		private var reportProxy:ReportProxy;
		
		private var loggedUser:User;

		private var selectedItemBroker:int;
		private var selectedItemPartner:int;
		private var selectedItemProduct:int;
				
		private var generalReportParameters:GeneralReportParameters = new GeneralReportParameters();
				
		[Bindable]
		[Embed(source="/assets/images/warning.png")]
		public var warningMsg:Class; 		
		
		public function GeneralReportMainMediator(viewComponent:Object=null){
			super(NAME, viewComponent);
			userProxy = UserProxy(facade.retrieveProxy(UserProxy.NAME));
			reportProxy = ReportProxy(facade.retrieveProxy(ReportProxy.NAME));
		}
		
        public function get generalReportMain():GeneralReportMain{  
            return viewComponent as GeneralReportMain;  
        }
        
		override public function onRegister():void{
			super.onRegister();
			
			try{ 
				generalReportMain.addEventListener(GeneralReportMain.SEARCH_ITEM, searchItem);
				generalReportMain.addEventListener(GeneralReportMain.SEARCH_USER, selectUserPerChannel);
				generalReportMain.addEventListener(GeneralReportMain.SEARCH_ANNULMENT, selectPartnerAndProductAndBroker);
				generalReportMain.addEventListener(GeneralReportMain.INITIALIZE_PAGE, initializePage);
			}catch(error:Error){
				Alert.show(error.message, "Atención", Alert.OK, null, null, warningMsg) ;	
			}

			var event:Event;
			this.initializePage(event);
    	}
    	
		override public function onRemove():void{
			super.onRemove();
			
			try{
				generalReportMain.removeEventListener(GeneralReportMain.SEARCH_ITEM, searchItem);
				generalReportMain.removeEventListener(GeneralReportMain.SEARCH_USER, selectUserPerChannel);
				generalReportMain.removeEventListener(GeneralReportMain.SEARCH_ANNULMENT, selectPartnerAndProductAndBroker);
				generalReportMain.removeEventListener(GeneralReportMain.INITIALIZE_PAGE, initializePage);
				generalReportMain.iFrameGeneral.dispose();
			}catch(error:Error){
				Alert.show(error.message, "Atención", Alert.OK, null, null, warningMsg) ;	
			}
			
			clearFields();
		}
    	
    	private function enableReport(channel:Boolean, 
    								user:Boolean, 
    								product:Boolean, 
    								typeInstallmentDate:Boolean, 
    								issuingStatus:Boolean, 
    								installmentStatus:Boolean, 
    								partner:Boolean,
    								broker:Boolean,
    								productFromAnnulment:Boolean,
    								annulmentType:Boolean):void{

			generalReportMain.frmChannel.visible = channel;
			generalReportMain.frmChannel.includeInLayout = channel;
			
			generalReportMain.frmUser.visible = user;
			generalReportMain.frmUser.includeInLayout = user;
			
			generalReportMain.frmProduct.visible = product;
			generalReportMain.frmProduct.includeInLayout = product;
			
			generalReportMain.frmTypeInstallmentDate.visible = typeInstallmentDate;
			generalReportMain.frmTypeInstallmentDate.includeInLayout = typeInstallmentDate;
			
			generalReportMain.frmIssuingStatus.visible = issuingStatus;
			generalReportMain.frmIssuingStatus.includeInLayout = issuingStatus;
			
			generalReportMain.frmInstallmentStatus.visible = installmentStatus;
			generalReportMain.frmInstallmentStatus.includeInLayout = installmentStatus;
			
			generalReportMain.frmPartner.visible = partner;
			generalReportMain.frmPartner.includeInLayout = partner;
			
			generalReportMain.frmBroker.visible = broker;
			generalReportMain.frmBroker.includeInLayout = broker;
			
			generalReportMain.frmProductFromAnnulment.visible = productFromAnnulment;
			generalReportMain.frmProductFromAnnulment.includeInLayout = productFromAnnulment;
			
			generalReportMain.frmAnnulmentType.visible = annulmentType;
			generalReportMain.frmAnnulmentType.includeInLayout = annulmentType;
    	}

		private function clearFields():void{
			generalReportMain.cmbxChannel.data = null;
			generalReportMain.cmbxChannel.dataProvider = null;
			generalReportMain.cmbxChannel.dropdown.dataProvider = new ArrayCollection();
		
			generalReportMain.cmbxUser.data = null;
			generalReportMain.cmbxUser.dataProvider = null;
			generalReportMain.cmbxUser.dropdown.dataProvider = new ArrayCollection();
			
			generalReportMain.cmbxProduct.data = null;
			generalReportMain.cmbxProduct.dataProvider = null;
			generalReportMain.cmbxProduct.dropdown.dataProvider = new ArrayCollection();
			
			generalReportMain.cmbxIssuingStatus.data = null;
			generalReportMain.cmbxIssuingStatus.dataProvider = new ArrayCollection();
			generalReportMain.cmbxIssuingStatus.dropdown.dataProvider = new ArrayCollection();

			generalReportMain.cmbxPartner.data = null;
			generalReportMain.cmbxPartner.dataProvider = new ArrayCollection();
			generalReportMain.cmbxPartner.dropdown.dataProvider = new ArrayCollection();
			
			generalReportMain.cmbxBroker.data = null;
			generalReportMain.cmbxBroker.dataProvider = new ArrayCollection();
			generalReportMain.cmbxBroker.dropdown.dataProvider = new ArrayCollection();
			
			generalReportMain.cmbxProductFromAnnulment.data = null;
			generalReportMain.cmbxProductFromAnnulment.dataProvider = new ArrayCollection();
			generalReportMain.cmbxProductFromAnnulment.dropdown.dataProvider = new ArrayCollection();						

			generalReportMain.cmbxAnnulmentType.data = null;
			generalReportMain.cmbxAnnulmentType.dataProvider = new ArrayCollection();
			generalReportMain.cmbxAnnulmentType.dropdown.dataProvider = new ArrayCollection();
			
			generalReportMain.txtEffectiveDate.text = "";
			generalReportMain.txtExpireDateDate.text = "";
			
			generalReportMain.rbOptionTypeInstallmentDate.selectedValue = 0;
			
			this.selectedItemBroker = 0;
			this.selectedItemPartner = 0;
			this.selectedItemProduct = 0;
		}    	    	
    	    	
    	private function initializePage(event:Event):void{
			loggedUser = ApplicationFacade.getInstance().loggedUser;
			generalReportParameters.partnerId = loggedUser.transientSelectedPartner;							    		    		
    		generalReportMain.resourceLabel = ResourceManager.getInstance().getString('resources', 'duration');
			clearFields();
			
			reportProxy.listReportData(generalReportParameters.partnerId, GeneralReportMainMediator.ISSUING_STATUS, GeneralReportMainMediator.INSTALLMENT_STATUS, GeneralReportMainMediator.ANNULMENT_TYPE, GeneralReportMainMediator.ANNULMENT_CODE, false);
    	}
    	        
		private function selectUserPerChannel(event:Event):void{
			generalReportMain.cmbxUser.data = null;
			generalReportMain.cmbxUser.dataProvider = null;
			generalReportMain.cmbxUser.dropdown.dataProvider = new ArrayCollection();
			
			if(generalReportMain.notificationName == NotificationViewsList.VIEW_SALES_REPORT){
				if((generalReportMain.cmbxChannel.selectedItem as Channel).channelId != 99999){		
					var channelUser:Channel = new Channel();
					var users:ArrayCollection = new ArrayCollection();
					var user:User = new User();
					
					channelUser = generalReportMain.cmbxChannel.selectedItem as Channel;
					
					users.addAll(channelUser.users);
					user.name = "*" + ResourceManager.getInstance().getString('messages', 'allSales') + "*";				    			
					user.userId = 99999;
					users.addItem(user);
										
	    			generalReportMain.cmbxUser.dataProvider = users;
	    			generalReportMain.cmbxUser.dropdown.dataProvider = users;									
				}else{
					userProxy.listUserPerPartner(generalReportParameters.partnerId); 	
				}     		
			}
		}    	        
    	        
        override public function listNotificationInterests():Array{  
            return [NotificationList.USER_LIST_PARTNER,
            		NotificationList.PARNTER_CHANGE,
            		NotificationList.LOAD_REPORT_LIST,
            		NotificationList.LOAD_ANNULMENT_LIST];
        }
        
        override public function handleNotification(notification:INotification):void{
            switch(notification.getName()){
        		case NotificationList.USER_LIST_PARTNER:
        			var userChannellist:ArrayCollection = notification.getBody() as ArrayCollection;
        			listUserPerPartner(userChannellist);
        			break; 	        			   	
        		case NotificationList.PARNTER_CHANGE:
        			generalReportMain.initializePage();		   		   
        			break;
        		case NotificationList.LOAD_REPORT_LIST:
        			reportListData(notification.getBody() as ReportListData);
        			break;
        		case NotificationList.LOAD_ANNULMENT_LIST:
        			reportAnnulmentListData(notification.getBody() as ReportListData);
        			break;
            }
		}

		private function listProductPerPartner(listData:ArrayCollection):void{
			var product:Product = new Product();
			
			product.nickName = "*" +  ResourceManager.getInstance().getString('messages', 'allProduct') + "*";
			product.productId = 99999;
			listData.addItem(product);
			
			generalReportMain.cmbxProduct.dataProvider = listData;
			generalReportMain.cmbxProduct.dropdown.dataProvider = listData;
		}

		private function listIssuingStatus(listData:ArrayCollection):void{
			var addAll:Boolean = false;
			var dataOption:DataOption = new DataOption();
			
			for each(var workDataOption:DataOption in listData){
				if(workDataOption.dataOptionId == 99999){
					addAll = true;
				} 
			}
			        					
			if(addAll == false){
				dataOption.fieldDescription = "*" + ResourceManager.getInstance().getString('messages', 'allSituation') + "*";
				dataOption.dataOptionId = 99999;
				listData.addItem(dataOption);
			}
			
			generalReportMain.cmbxIssuingStatus.dataProvider = listData;
			generalReportMain.cmbxIssuingStatus.dropdown.dataProvider = listData;
		}

		private function listInstallmentStatus(listData:ArrayCollection):void{
			var addAll:Boolean = false;
			var dataOption:DataOption = new DataOption();
			
			for each(var workDataOption:DataOption in listData){
				if(workDataOption.dataOptionId == 99999){
					addAll = true;
				} 
			}
			        					
			if(addAll == false){
				dataOption.fieldDescription = "*" + ResourceManager.getInstance().getString('messages', 'allSituation') + "*";
				dataOption.dataOptionId = 99999;
				listData.addItem(dataOption);
			}
			
			generalReportMain.cmbxInstallmentStatus.dataProvider = listData;
			generalReportMain.cmbxInstallmentStatus.dropdown.dataProvider = listData;
		}

		private function listAnnulmentType(listData:ArrayCollection):void{
			var dataOption:DataOption = new DataOption();

			dataOption.fieldDescription = "*" + ResourceManager.getInstance().getString('messages', 'allSituation') + "*";
			dataOption.dataOptionId = 99999;
			listData.addItem(dataOption);
			
			generalReportMain.cmbxAnnulmentType.dataProvider = listData;
			generalReportMain.cmbxAnnulmentType.dropdown.dataProvider = listData;
		}

		private function listChannel(listData:ArrayCollection):void{
			var channel:Channel = new Channel();
			
			channel.nickName = "*" + ResourceManager.getInstance().getString('messages', 'allChannel') + "*";
			channel.channelId = 99999;
			listData.addItem(channel);

			generalReportMain.cmbxChannel.dataProvider = listData;
			generalReportMain.cmbxChannel.dropdown.dataProvider = listData;			
		}

		private function listUserPerPartner(listData:ArrayCollection):void{
			var user:User = new User();
		
			user.name = "*" + ResourceManager.getInstance().getString('messages', 'allSales') + "*";				    			
			user.userId = 99999;
			listData.addItem(user);

			generalReportMain.cmbxUser.dataProvider = listData;
			generalReportMain.cmbxUser.dropdown.dataProvider = listData;   			
		}
		
		private function listPartnerToAnnulment(listData:ArrayCollection):void{
			var partner:Partner = new Partner();
		
			partner.name = "*" + ResourceManager.getInstance().getString('messages', 'allPartner') + "*";				    			
			partner.partnerId = 99999;
			listData.addItem(partner);

			generalReportMain.cmbxPartner.dataProvider = listData;
			generalReportMain.cmbxPartner.dropdown.dataProvider = listData;
			
			if(this.selectedItemPartner > 0){
				generalReportMain.cmbxPartner.selectedValue = this.selectedItemPartner;
			}
		}
		
		private function listBrokerToAnnulment(listData:ArrayCollection):void{
			var broker:Broker = new Broker();
		
			broker.name = "*" + ResourceManager.getInstance().getString('messages', 'allBroker') + "*";				    			
			broker.brokerId = 99999;
			listData.addItem(broker);

			generalReportMain.cmbxBroker.dataProvider = listData;
			generalReportMain.cmbxBroker.dropdown.dataProvider = listData;
			
			if(this.selectedItemBroker > 0){
				generalReportMain.cmbxBroker.selectedValue = this.selectedItemBroker;
			}
		}
		
		private function listProductToAnnulment(listData:ArrayCollection):void{
			var product:Product = new Product();
			
			product.nickName = "*" +  ResourceManager.getInstance().getString('messages', 'allProduct') + "*";
			product.productId = 99999;
			listData.addItem(product);
			
			generalReportMain.cmbxProductFromAnnulment.dataProvider = listData;
			generalReportMain.cmbxProductFromAnnulment.dropdown.dataProvider = listData;
			
			if(this.selectedItemProduct > 0){
				generalReportMain.cmbxProductFromAnnulment.selectedValue = this.selectedItemProduct;
			}
		}
		
		private function reportAnnulmentListData(listData:ReportListData):void{
			listBrokerToAnnulment(listData.listBrokerPerProductOrPartner);
			listPartnerToAnnulment(listData.listPartnerPerProductOrBroker);
			listProductToAnnulment(listData.listProductPerPartnerOrBroker);
		}
		
		private function reportListData(listData:ReportListData):void{
			switch(generalReportMain.notificationName){
				case NotificationViewsList.VIEW_CHANNEL_REPORT:
					generalReportMain.resource = ResourceManager.getInstance().getString('resources', 'channelReport');
					enableReport(true, false, false, false, true, false, false, false, false, false);

					listChannel(listData.listChannel);
					listIssuingStatus(listData.listIssuingStatus);
					break;
				case NotificationViewsList.VIEW_COMMISSION_REPORT:
					generalReportMain.resource = ResourceManager.getInstance().getString('resources', 'commissionReport');
					generalReportMain.resourceLabel = ResourceManager.getInstance().getString('resources', 'dateRange');
    				enableReport(true, false, false, true, false, true, false, false, false, false);
    				
					listChannel(listData.listChannel);
					listInstallmentStatus(listData.listInstallmentStatus);
					break;
				case NotificationViewsList.VIEW_LABOUR_REPORT:
					generalReportMain.resource = ResourceManager.getInstance().getString('resources', 'labourReport');
					generalReportMain.resourceLabel = ResourceManager.getInstance().getString('resources', 'dateRange');
    				enableReport(true, false, false, true, false, true, false, false, false, false);
    				
    				listChannel(listData.listChannel);
					listInstallmentStatus(listData.listInstallmentStatus);
					break;
				case NotificationViewsList.VIEW_INSTALLMENT_REPORT:
					generalReportMain.resource = ResourceManager.getInstance().getString('resources', 'installmentReport');
					generalReportMain.resourceLabel = ResourceManager.getInstance().getString('resources', 'dateRange');
					enableReport(false, false, false, true, false, true, false, false, false, false);
					
					listProductPerPartner(listData.listProductsPerPartner);
					listInstallmentStatus(listData.listInstallmentStatus);
					break;
				case NotificationViewsList.VIEW_PRODUCT_REPORT:
					generalReportMain.resource = ResourceManager.getInstance().getString('resources', 'productReport');
					enableReport(false, false, true, false, true, false, false, false, false, false);
					
					listProductPerPartner(listData.listProductsPerPartner);
					listIssuingStatus(listData.listIssuingStatus);
					break;
				case NotificationViewsList.VIEW_SALES_REPORT:
					generalReportMain.resource = ResourceManager.getInstance().getString('resources', 'salesReport');			
    				enableReport(true, true, false, false, true, false, false, false, false, false);
    				
    				listChannel(listData.listChannel);
    				listUserPerPartner(listData.listUserPerPartner);
    				listIssuingStatus(listData.listIssuingStatus);			
					break;
				case NotificationViewsList.VIEW_ANNULMENT_REPORT:
					generalReportMain.resource = ResourceManager.getInstance().getString('resources', 'annulmentReport');
					enableReport(false, false, false, false, false, false, true, true, true, true);
					
					listPartnerToAnnulment(listData.listPartnerPerProductOrBroker);
					listProductToAnnulment(listData.listProductPerPartnerOrBroker);
					listBrokerToAnnulment(listData.listBrokerPerProductOrPartner);
					listAnnulmentType(listData.listAnnulmentType);
					break;
			}
		}

		/**
		 * metodo utilizado nos combos do relatorio de anulacao 
		 * 
		 */
		private function selectPartnerAndProductAndBroker(event:Event):void{			
			if((generalReportMain.cmbxPartner.selectedItem as Partner) != null){
				this.selectedItemPartner = (generalReportMain.cmbxPartner.selectedItem as Partner).partnerId;
			}
			
			if((generalReportMain.cmbxProductFromAnnulment.selectedItem as Product) != null){
				this.selectedItemProduct = (generalReportMain.cmbxProductFromAnnulment.selectedItem as Product).productId;
			}
			
			if((generalReportMain.cmbxBroker.selectedItem as Broker) != null){
				this.selectedItemBroker = (generalReportMain.cmbxBroker.selectedItem as Broker).brokerId;
			}
			
			reportProxy.loadReportListAnnulment(this.selectedItemPartner, this.selectedItemProduct, this.selectedItemBroker);
		}

        private function searchItem(event:Event):void{
			var dateFormatter:DateFormatter;
			dateFormatter = new DateFormatter();
        	dateFormatter.formatString = "DD/MM/YYYY";
        	
        	var reportId:int;
			var reportOutput:String;
			var stringUrl:String;
			
			var effectiveDate:Date;
			var expiryDate:Date;
			
			var channelId:String = null;
			var productId:String = null;
			var userId:String = null;
			var partnerId:String = null;
			var brokerId:String = null;
			var annulmentType:String = null;
			
			var issuingStatus:String = null;
			var installmentStatus:String = null;
			
			var typeInstallmentDate:String;
			
        	stringUrl = Utilities.URL_BASE + Utilities.URL_BASE_REPORT + Utilities.URL_REPORT;
        	reportOutput = generalReportMain.REPORT_OUTPUT;
        	
        	effectiveDate = generalReportMain.txtEffectiveDate.newDate;
        	expiryDate = generalReportMain.txtExpireDateDate.newDate;

			if(generalReportMain.notificationName == NotificationViewsList.VIEW_COMMISSION_REPORT
				|| generalReportMain.notificationName == NotificationViewsList.VIEW_LABOUR_REPORT 
				|| generalReportMain.notificationName == NotificationViewsList.VIEW_CHANNEL_REPORT
				|| generalReportMain.notificationName == NotificationViewsList.VIEW_SALES_REPORT){
					
				if((generalReportMain.cmbxChannel.selectedItem as Channel).channelId != 99999){
				   channelId = (generalReportMain.cmbxChannel.selectedItem as Channel).channelId.toString();
				}
			}
			
			if(generalReportMain.notificationName == NotificationViewsList.VIEW_CHANNEL_REPORT
				|| generalReportMain.notificationName == NotificationViewsList.VIEW_PRODUCT_REPORT
				|| generalReportMain.notificationName == NotificationViewsList.VIEW_SALES_REPORT){

				if((generalReportMain.cmbxIssuingStatus.selectedItem as DataOption).dataOptionId != 99999){
				   	issuingStatus = (generalReportMain.cmbxIssuingStatus.selectedItem as DataOption).dataOptionId.toString();
				}
			}
		
			if(generalReportMain.notificationName == NotificationViewsList.VIEW_COMMISSION_REPORT
				|| generalReportMain.notificationName == NotificationViewsList.VIEW_LABOUR_REPORT 
				|| generalReportMain.notificationName == NotificationViewsList.VIEW_INSTALLMENT_REPORT){

				if((generalReportMain.cmbxInstallmentStatus.selectedItem as DataOption).dataOptionId != 99999){
				   	installmentStatus = (generalReportMain.cmbxInstallmentStatus.selectedItem as DataOption).dataOptionId.toString();
				}
			}
			
			switch(generalReportMain.notificationName){
				case NotificationViewsList.VIEW_CHANNEL_REPORT:
					reportId = 1;
					stringUrl += "id=" + reportId;
					stringUrl += "&reportOutput=" + reportOutput;
					stringUrl += "&PartnerID_" + reportId + "=" + generalReportParameters.partnerId.toString();
					stringUrl += "&ChannelID_" + reportId + "=" + channelId;  
					stringUrl += "&IssuingStatus_" + reportId + "=" + issuingStatus;
					stringUrl += "&EffectiveDateIni_" + reportId + "=" + dateFormatter.format(effectiveDate).toString();
					stringUrl += "&EffectiveDateEnd_" + reportId + "=" + dateFormatter.format(expiryDate).toString();
					break;
				case NotificationViewsList.VIEW_COMMISSION_REPORT:
					if(generalReportMain.rbOptionTypeInstallmentDate.selectedValue as int == 0){
						reportId = 2; 
						typeInstallmentDate = ResourceManager.getInstance().getString('resources','paymentDate');
					}else{
						reportId = 12;
						typeInstallmentDate = ResourceManager.getInstance().getString('resources','dueDate');
					}				
					
					stringUrl += "id=" + reportId;
					stringUrl += "&reportOutput=" + reportOutput;
					stringUrl += "&PartnerID_" + reportId + "=" + generalReportParameters.partnerId.toString();
					stringUrl += "&ChannelID_" + reportId + "=" + channelId; 
					stringUrl += "&InstallmentStatus_" + reportId + "=" + installmentStatus;
					stringUrl += "&InstallmentDateIni_" + reportId + "=" + dateFormatter.format(effectiveDate).toString();
					stringUrl += "&InstallmentDateEnd_" + reportId + "=" + dateFormatter.format(expiryDate).toString();
					stringUrl += "&typeInstallmentDate_" + reportId + "=" + typeInstallmentDate;
					break;
				case NotificationViewsList.VIEW_LABOUR_REPORT:
					if(generalReportMain.rbOptionTypeInstallmentDate.selectedValue as int == 0){
						reportId = 18; 
						typeInstallmentDate = ResourceManager.getInstance().getString('resources','paymentDate');
					}else{
						reportId = 19;
						typeInstallmentDate = ResourceManager.getInstance().getString('resources','dueDate');
					}				
					
					stringUrl += "id=" + reportId;
					stringUrl += "&reportOutput=" + reportOutput;
					stringUrl += "&PartnerID_" + reportId + "=" + generalReportParameters.partnerId.toString();
					stringUrl += "&ChannelID_" + reportId + "=" + channelId; 
					stringUrl += "&InstallmentStatus_" + reportId + "=" + installmentStatus;
					stringUrl += "&InstallmentDateIni_" + reportId + "=" + dateFormatter.format(effectiveDate).toString();
					stringUrl += "&InstallmentDateEnd_" + reportId + "=" + dateFormatter.format(expiryDate).toString();
					stringUrl += "&typeInstallmentDate_" + reportId + "=" + typeInstallmentDate;
					break;
				case NotificationViewsList.VIEW_INSTALLMENT_REPORT:				
					if(generalReportMain.rbOptionTypeInstallmentDate.selectedValue as int == 0){
						reportId = 9; 
						typeInstallmentDate = ResourceManager.getInstance().getString('resources','paymentDate');
					}else{
						reportId = 3;
						typeInstallmentDate = ResourceManager.getInstance().getString('resources','dueDate');
					}
					
					stringUrl += "id=" + reportId;
					stringUrl += "&reportOutput=" + reportOutput;
					stringUrl += "&PartnerID_" + reportId + "=" + generalReportParameters.partnerId.toString();
					stringUrl += "&InstallmentStatus_" + reportId + "=" + installmentStatus;
					stringUrl += "&InstallmentDateIni_" + reportId + "=" + dateFormatter.format(effectiveDate).toString();
					stringUrl += "&InstallmentDateEnd_" + reportId + "=" + dateFormatter.format(expiryDate).toString();
					stringUrl += "&typeInstallmentDate_" + reportId + "=" + typeInstallmentDate;
					break;
				case NotificationViewsList.VIEW_PRODUCT_REPORT:					
					if((generalReportMain.cmbxProduct.selectedItem as Product).productId != 99999){
					   productId = (generalReportMain.cmbxProduct.selectedItem as Product).productId.toString();
					}
					
					reportId = 4;
					stringUrl += "id=" + reportId;
					stringUrl += "&reportOutput=" + reportOutput;
					stringUrl += "&PartnerID_" + reportId + "=" + generalReportParameters.partnerId.toString();					
					stringUrl += "&IssuingStatus_" + reportId + "=" + issuingStatus;
					stringUrl += "&ProductID_" + reportId + "=" + productId;
					stringUrl += "&EffectiveDateIni_" + reportId + "=" + dateFormatter.format(effectiveDate).toString();
					stringUrl += "&EffectiveDateEnd_" + reportId + "=" + dateFormatter.format(expiryDate).toString();
					break;
				case NotificationViewsList.VIEW_SALES_REPORT:
					if((generalReportMain.cmbxUser.selectedItem as User).userId != 99999){
					   	userId = (generalReportMain.cmbxUser.selectedItem as User).userId.toString();
					}
					
					reportId = 5;
					stringUrl += "id=" + reportId;
					stringUrl += "&reportOutput=" + reportOutput;
					stringUrl += "&PartnerID_" + reportId + "=" + generalReportParameters.partnerId.toString();
					stringUrl += "&ChannelID_" + reportId + "=" + channelId; 
					stringUrl += "&IssuingStatus_" + reportId + "=" + issuingStatus;
					stringUrl += "&UserID_" + reportId + "=" + userId;
					stringUrl += "&EffectiveDateIni_" + reportId + "=" + dateFormatter.format(effectiveDate).toString();
					stringUrl += "&EffectiveDateEnd_" + reportId + "=" + dateFormatter.format(expiryDate).toString();										
					break;
				case NotificationViewsList.VIEW_ANNULMENT_REPORT:
					if((generalReportMain.cmbxPartner.selectedItem as Partner).partnerId != 99999){
					   	partnerId = (generalReportMain.cmbxPartner.selectedItem as Partner).partnerId.toString();
					}
					
					if((generalReportMain.cmbxProductFromAnnulment.selectedItem as Product).productId != 99999){
					   	productId = (generalReportMain.cmbxProductFromAnnulment.selectedItem as Product).productId.toString();
					}
					
					if((generalReportMain.cmbxBroker.selectedItem as Broker).brokerId != 99999){
					   	brokerId = (generalReportMain.cmbxBroker.selectedItem as Broker).brokerId.toString();
					}
					
					if((generalReportMain.cmbxAnnulmentType.selectedItem as DataOption).dataOptionId != 99999){
					   	annulmentType = (generalReportMain.cmbxAnnulmentType.selectedItem as DataOption).dataOptionId.toString();
					}
					
					if(reportOutput == Utilities.REPORT_PDF){
						reportId = 20;
					}else{
						reportId = 21;
					}
					
					stringUrl += "id=" + reportId;
					stringUrl += "&reportOutput=" + reportOutput;
					stringUrl += "&PartnerId_" + reportId + "=" + partnerId;
					stringUrl += "&BrokerId_" + reportId + "=" + brokerId;
					stringUrl += "&ProductId_" + reportId + "=" + productId;
					stringUrl += "&IssuanceType_" + reportId + "=" + annulmentType;
					stringUrl += "&startDateValidity_" + reportId + "=" + dateFormatter.format(effectiveDate).toString();
					stringUrl += "&endDateValidity_" + reportId + "=" + dateFormatter.format(expiryDate).toString();
					break;
			}
 			try{
				if(reportOutput == Utilities.REPORT_PDF){
					generalReportMain.reportView(stringUrl);	
				}else{
					generalReportMain.URL = stringUrl;
					generalReportMain.URL = "";	
				}
			}
  			catch(error:Error){
  				throw error;
  			}
        }  
	}
}