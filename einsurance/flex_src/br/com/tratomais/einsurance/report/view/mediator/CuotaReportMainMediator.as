////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Bruno Souza
// @version 1.0
// @lastModified 03/11/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.report.view.mediator
{
	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.Constant;
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.customer.model.proxy.DataOptionProxy;
	import br.com.tratomais.einsurance.customer.model.vo.DataOption;
	import br.com.tratomais.einsurance.report.view.components.collection.CuotaReportMain;
	import br.com.tratomais.einsurance.useradm.model.vo.User;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.formatters.DateFormatter;
	import mx.resources.ResourceManager;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class CuotaReportMainMediator extends Mediator implements IMediator {
		public static const NAME:String = 'CuotaReportMainMediator';
		
		private var dataOptionProxy:DataOptionProxy;
		private var loggedUser:User
		
		private var reportOutput:String;
		private var dateFormatter:DateFormatter;
		private var effectiveDate:Date;
		private var expiryDate:Date;
		private var reportId:int;
		private var statusId:String;
		private var typeInstallmentDate:String;

		private var stringUrl:String;
						
		[Bindable][Embed(source="/assets/images/warning.png")]
		public var warningMsg:Class; 		
		
		public function CuotaReportMainMediator(viewComponent:Object=null){
			super(NAME, viewComponent);
			dataOptionProxy = DataOptionProxy(facade.retrieveProxy(DataOptionProxy.NAME));
		}
		
        public function get cuotaReportMain():CuotaReportMain{  
            return viewComponent as CuotaReportMain;  
        }
        
		override public function onRegister():void{
			super.onRegister();
			
			try{ 
				cuotaReportMain.addEventListener(CuotaReportMain.SEARCH_REPORT, searchReport);
				cuotaReportMain.addEventListener(CuotaReportMain.INITIALIZE_PAGE, initializePage);
			}catch(error:Error){
				Alert.show(error.message, "Atención", Alert.OK, null, null, warningMsg) ;	
			}
			
			var event:Event;
			this.initializePage(event);
    	}
    	
		override public function onRemove():void{
			super.onRemove();
			
			try{
				cuotaReportMain.removeEventListener(CuotaReportMain.SEARCH_REPORT, searchReport);
				cuotaReportMain.removeEventListener(CuotaReportMain.INITIALIZE_PAGE, initializePage);
			}catch(error:Error){
				Alert.show(error.message, "Atención", Alert.OK, null, null, warningMsg) ;	
			}
			
			clearFields();
		}
    	
		private function clearFields():void{		
			cuotaReportMain.rbOptionTypeInstallmentDate.selectedValue = 0;
			cuotaReportMain.PaymentDate.selected = true;
			
			cuotaReportMain.cmbxIssuingStatus.data = null;
			cuotaReportMain.cmbxIssuingStatus.dataProvider = new ArrayCollection();
			cuotaReportMain.cmbxIssuingStatus.dropdown.dataProvider = new ArrayCollection();
			
			cuotaReportMain.txtEffectiveDate.text = "";
			cuotaReportMain.txtExpireDateDate.text = "";
		}    	    	
    	    	
    	private function initializePage(event:Event):void{
    		this.clearFields();
			loggedUser = ApplicationFacade.getInstance().loggedUser;
			dataOptionProxy.listDataOptionByActive(Constant.INSTALLMENT_STATUS, true);			 		
    	}
    	        
        override public function listNotificationInterests():Array{  
            return [NotificationList.DATA_OPTION_LISTED_REPORT];
        }
        
        override public function handleNotification(notification:INotification):void{
            switch(notification.getName()){
				case NotificationList.DATA_OPTION_LISTED_REPORT:
					var issuingStatusList:ArrayCollection = notification.getBody() as ArrayCollection;
					var addAll:Boolean = false;
					
					for each(var workDataOption:DataOption in issuingStatusList){
						if(workDataOption.dataOptionId == 99999){
							addAll = true;
						} 
					}
					
					if(addAll == false){
						var dataOption:DataOption = new DataOption();
						dataOption.fieldDescription = "*" + ResourceManager.getInstance().getString('messages', 'allSituation') + "*";
						dataOption.dataOptionId = 99999;
						issuingStatusList.addItem(dataOption);
					}
					cuotaReportMain.cmbxIssuingStatus.dataProvider = issuingStatusList;
					cuotaReportMain.cmbxIssuingStatus.dropdown.dataProvider = issuingStatusList;
					break;
            }
		}   

        public function searchReport(event:Event):void{
        	stringUrl = Utilities.URL_BASE + Utilities.URL_BASE_REPORT + Utilities.URL_REPORT;
        	reportOutput = cuotaReportMain.REPORT_OUTPUT;
        	effectiveDate = cuotaReportMain.txtEffectiveDate.newDate;
        	expiryDate = cuotaReportMain.txtExpireDateDate.newDate;
        	dateFormatter = new DateFormatter();
        	dateFormatter.formatString = "DD/MM/YYYY";
        	
			if((cuotaReportMain.cmbxIssuingStatus.selectedItem as DataOption).dataOptionId != 99999){
			   	statusId = (cuotaReportMain.cmbxIssuingStatus.selectedItem as DataOption).dataOptionId.toString();
			}else{
				statusId = null;
			}
			
			if(cuotaReportMain.rbOptionTypeInstallmentDate.selectedValue as int == 0){
				reportId = 16; 
				typeInstallmentDate = ResourceManager.getInstance().getString('resources','paymentDate');
			}else{
				reportId = 17;
				typeInstallmentDate = ResourceManager.getInstance().getString('resources','dueDate');
			}
			
			stringUrl += "id=" + reportId;
			stringUrl += "&reportOutput=" + reportOutput;
			stringUrl += "&InstallmentStatus_" + reportId + "=" + statusId;
			stringUrl += "&InstallmentDateStart_" + reportId + "=" + dateFormatter.format(effectiveDate).toString();
			stringUrl += "&InstallmentDateEnd_" + reportId + "=" + dateFormatter.format(expiryDate).toString();
			stringUrl += "&TypeInstallmentDate_" + reportId + "=" + typeInstallmentDate;

 			try{
				if(reportOutput == Utilities.REPORT_PDF){
					cuotaReportMain.reportView(stringUrl);	
				}else{
					cuotaReportMain.URL = stringUrl;
					cuotaReportMain.URL = "";	
				}
			}catch(error:Error){
  				throw error;
  			}
        }  
	}
}