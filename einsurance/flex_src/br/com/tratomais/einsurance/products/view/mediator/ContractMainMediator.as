////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 29/01/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.products.view.mediator
{
	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.products.model.proxy.ProductProxy;
	import br.com.tratomais.einsurance.products.model.vo.ProductVersion;
	import br.com.tratomais.einsurance.products.view.components.ContractMain;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;

	public class ContractMainMediator extends Mediator implements IMediator
	{
		public static const NAME:String = 'ContractMainMediator';
		
		private var productProxy:ProductProxy;
		private var contractBaseProductMediator:ContractBaseProductMediator;
		private var contractCopyProductMediator:ContractCopyProductMediator;
		
		public function ContractMainMediator(viewComponent:Object=null){
			super(NAME, viewComponent);
			productProxy = ProductProxy(facade.retrieveProxy(ProductProxy.NAME));
		}
		
        protected function get contractMain():ContractMain{  
          return viewComponent as ContractMain;  
        }
		
		override public function onRegister():void{
			super.onRegister();
			contractMain.addEventListener(ContractMain.INITIALIZE_PAGE, initializePage);
			contractMain.addEventListener(ContractMain.COPY_PRODUCT, onCopyProduct);
			contractMain.addEventListener(ContractMain.CLEAN_PRODUCT_COPY, onNewProduct);
			contractMain.addEventListener(ContractMain.CANCEL_EDIT, onCancelEdit);
			contractMain.addEventListener(ContractMain.SAVE_PRODUCT_COPY, onSaveProductCopy);
			
			contractBaseProductMediator = new ContractBaseProductMediator(contractMain.baseProduct);
			contractCopyProductMediator = new ContractCopyProductMediator(contractMain.copyProduct);
			
			ApplicationFacade.getInstance().registerOnlyNewMediator(contractBaseProductMediator);
			ApplicationFacade.getInstance().registerOnlyNewMediator(contractCopyProductMediator);
			
			this.initializePage();
		}
		
		override public function onRemove():void{
			
			super.onRemove();
			
			contractMain.removeEventListener(ContractMain.INITIALIZE_PAGE, initializePage);
			contractMain.removeEventListener(ContractMain.COPY_PRODUCT, onCopyProduct);
			contractMain.removeEventListener(ContractMain.CLEAN_PRODUCT_COPY, onNewProduct);
			contractMain.removeEventListener(ContractMain.CANCEL_EDIT, onCancelEdit);
			contractMain.removeEventListener(ContractMain.SAVE_PRODUCT_COPY, onSaveProductCopy);
			
			ApplicationFacade.getInstance().removeMediator(ContractBaseProductMediator.NAME);
			ApplicationFacade.getInstance().removeMediator(ContractCopyProductMediator.NAME);
		
			contractCopyProductMediator.resetForm();
			contractCopyProductMediator.enabledFormCopy(false);
			contractBaseProductMediator.resetForm();
		}
		
		private function initializePage():void{
			contractBaseProductMediator.resetForm();
			contractCopyProductMediator.resetForm();
			contractCopyProductMediator.enabledFormCopy(false);
			productProxy.listAllProduct();
			contractMain.imgCopy.enabled=false;
			contractMain.imgNew.enabled=true;
			contractMain.imgSave.enabled=false;
			contractMain.imgUndo.enabled=false;
		}
		
		private function onCopyProduct(event:Event):void{
			if(contractMain.baseProduct.cmbProductVersion.selectedItem!=null){
				var selectedProductVersion:ProductVersion = new ProductVersion();
				selectedProductVersion = ObjectUtil.copy(contractMain.baseProduct.cmbProductVersion.selectedItem) as ProductVersion;
				contractCopyProductMediator.loadInfoSelectedProduct(selectedProductVersion);
				contractMain.imgCopy.enabled=false;
				
				contractBaseProductMediator.enabledForm(false);
			}
		}
		
		private function onNewProduct(event:Event):void{
			if(contractMain.baseProduct.cmbProducts.selectedItem!=null &&
				contractMain.baseProduct.cmbProductVersion.selectedItem!=null){
				
				contractCopyProductMediator.resetForm();
				contractCopyProductMediator.enabledFormCopy(true);
				contractMain.imgCopy.enabled=true;
				contractMain.imgNew.enabled=false;
				contractMain.imgSave.enabled=true;
				contractMain.imgUndo.enabled=true;
			}else{
				Utilities.warningMessage("pleaseSelectProductToCopy");
			}
		}
		
		private function onCancelEdit(event:Event):void{
			contractCopyProductMediator.resetForm();
			contractCopyProductMediator.enabledFormCopy(false);
			contractBaseProductMediator.enabledForm(true);
			contractMain.imgCopy.enabled=false;
			contractMain.imgNew.enabled=true;
			contractMain.imgSave.enabled=false;
			contractMain.imgUndo.enabled=false;			
		}
		
		private function onSaveProductCopy(event:Event):void{
			if(contractMain.copyProduct.copiedProductVersion != null){
				if(Utilities.validateForm(contractMain.copyProduct.validatorForm, ResourceManager.getInstance().getString('resources', 'contract'))){
					contractMain.copyProduct.copiedProductVersion.product.nickName =  contractMain.copyProduct.productName.text;
					contractMain.copyProduct.copiedProductVersion.product.productCode = contractMain.copyProduct.txtProductCode.text;
					contractMain.copyProduct.copiedProductVersion.id.effectiveDate = contractMain.copyProduct.mdfEffectiveDate.newDate;
					contractMain.copyProduct.copiedProductVersion.versionName = contractMain.copyProduct.txtDescription.text;
					
					productProxy.saveProductVersionCopy(contractMain.copyProduct.copiedProductVersion);
				}
			}else{
				Utilities.warningMessage("pleasePressButtonCopyProduct");
			}	
		}
		
        override public function listNotificationInterests():Array{  
            return [NotificationList.PRODUCT_LISTED,
            		NotificationList.PRODUCT_COPY_SAVED];  
        }
		          
        override public function handleNotification(notification:INotification):void{  
            switch (notification.getName())  
            {
            	case NotificationList.PRODUCT_LISTED:
            		var allProductsResult:ArrayCollection = (notification.getBody() as ArrayCollection);
            		contractMain.baseProduct.listProducts = allProductsResult; 
            		break;
            	case NotificationList.PRODUCT_COPY_SAVED:
            		var resultProductCopySaved:ProductVersion = (notification.getBody() as ProductVersion);
            		if(resultProductCopySaved!=null){
            			Alert.show("El Producto " + resultProductCopySaved.product.nickName + ", fue guardado con exito!", "Exito", Alert.OK, null, null, Utilities.successMsg);
            			contractMain.copyProduct.copiedProductVersion=new ProductVersion();
            			contractBaseProductMediator.resetForm();
            			contractCopyProductMediator.resetForm();
            			this.initializePage();
						contractBaseProductMediator.enabledForm(true);
						
						var log:String;
						log = ApplicationFacade.getInstance().loggedUser.login;
						log += ":" + ApplicationFacade.getInstance().loggedUser.domain.name;
						log += ":USER";
						log += ":SAVE";
						ApplicationFacade.getInstance().auditLogger = log;						
            		}
					break;            		
            }
    	}
	}
}