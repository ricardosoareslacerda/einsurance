package br.com.tratomais.einsurance.products.view.mediator
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.components.DoubleClickManager;
	import br.com.tratomais.einsurance.products.model.proxy.ProductProxy;
	import br.com.tratomais.einsurance.products.model.vo.Product;
	import br.com.tratomais.einsurance.products.model.vo.ProductPlan;
	import br.com.tratomais.einsurance.products.view.components.ContractBaseProduct;
	import br.com.tratomais.einsurance.products.view.components.ContractMain;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;

	public class ContractBaseProductMediator extends Mediator implements IMediator
	{
		public static const NAME:String = 'ContractBaseProductMediator';
		private var productProxy:ProductProxy;
		private var doubleCLickManager:DoubleClickManager = null;
		    	
		public function ContractBaseProductMediator(viewComponent:Object=null){
			super(NAME, viewComponent);
			productProxy = ProductProxy(facade.retrieveProxy(ProductProxy.NAME));
		}
        
        protected function get contractBaseProduct():ContractBaseProduct{  
          return viewComponent as ContractBaseProduct;
        }
		
		override public function onRegister():void{
			super.onRegister();
			contractBaseProduct.addEventListener(ContractBaseProduct.CHANGE_PRODUCT, onChangeProduct);

			doubleCLickManager = new DoubleClickManager(contractBaseProduct.dgProductPlan);
			doubleCLickManager.addEventListener(MouseEvent.CLICK, onItemPlanClick);
		}
		
		override public function onRemove():void{
			super.onRemove();
			contractBaseProduct.removeEventListener(ContractBaseProduct.CHANGE_PRODUCT, onChangeProduct);
			doubleCLickManager.removeEventListener(MouseEvent.CLICK, onItemPlanClick);
		}
		 
		private function onChangeProduct(event:Event):void{
			if(contractBaseProduct.cmbProducts.selectedItem!=null){
				productProxy.getProductVersionByProduct((contractBaseProduct.cmbProducts.selectedItem as Product).productId);
				contractBaseProduct.dgProductPlan.dataProvider=null;
				contractBaseProduct.dgPlanCoverage.dataProvider=null;
			}
		}
		
		private function onItemPlanClick(event:Event):void{
			if(contractBaseProduct.dgProductPlan.selectedItem!=null){
				var productPlan:ProductPlan = (contractBaseProduct.dgProductPlan.selectedItem as ProductPlan);
				contractBaseProduct.dgPlanCoverage.dataProvider = productPlan.coveragePlans;
			}
		}
		
		private function loadComboVersion(pProductVersionList:ArrayCollection):void{
			contractBaseProduct.cmbProductVersion.dataProvider = pProductVersionList;
		}
		
		public function resetForm():void{
			contractBaseProduct.listProducts=null;
			contractBaseProduct.cmbProductVersion.dataProvider=null;
			contractBaseProduct.dgProductPlan.dataProvider=null;
			contractBaseProduct.dgPlanCoverage.dataProvider=null;
		}
		
		public function enabledForm(pValue:Boolean):void{
			contractBaseProduct.cmbProducts.enabled=pValue;
			contractBaseProduct.cmbProductVersion.enabled=pValue;
			contractBaseProduct.dgProductPlan.enabled=pValue;
			contractBaseProduct.dgPlanCoverage.enabled=pValue;			
		}
      	
		override public function listNotificationInterests():Array{
			return [NotificationList.PRODUCT_VERSION_LISTED];
		}
		
		override public function handleNotification(notification:INotification):void{
            switch (notification.getName())  
            {			
				case NotificationList.PRODUCT_VERSION_LISTED:
					var prodcutListFound:ArrayCollection = notification.getBody() as ArrayCollection;
					this.loadComboVersion(prodcutListFound);
					break;
            }
		}
	}
}