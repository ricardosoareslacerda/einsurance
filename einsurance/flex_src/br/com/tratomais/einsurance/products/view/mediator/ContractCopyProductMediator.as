package br.com.tratomais.einsurance.products.view.mediator
{
	import br.com.tratomais.einsurance.core.components.DoubleClickManager;
	import br.com.tratomais.einsurance.products.model.proxy.ProductProxy;
	import br.com.tratomais.einsurance.products.model.vo.ProductPlan;
	import br.com.tratomais.einsurance.products.model.vo.ProductVersion;
	import br.com.tratomais.einsurance.products.view.components.ContractCopyProduct;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;

	public class ContractCopyProductMediator extends Mediator implements IMediator
	{
		public static const NAME:String = 'ContractCopyProductMediator';
		private var productProxy:ProductProxy;
		private var doubleCLickManager:DoubleClickManager = null;
				
		public function ContractCopyProductMediator(viewComponent:Object=null){
			super(NAME, viewComponent);
			productProxy = ProductProxy(facade.retrieveProxy(ProductProxy.NAME));
		}
        
        protected function get contractCopyProduct():ContractCopyProduct{  
          return viewComponent as ContractCopyProduct;
        }
		
		override public function onRegister():void{
			super.onRegister();
			doubleCLickManager = new DoubleClickManager(contractCopyProduct.dgProductPlanCopy);
			doubleCLickManager.addEventListener(MouseEvent.CLICK, onItemPlanClick);
		}
		
		override public function onRemove():void{
			super.onRemove();
			doubleCLickManager.removeEventListener(MouseEvent.CLICK, onItemPlanClick);
		}
		 
		private function onItemPlanClick(event:Event):void{
			if(contractCopyProduct.dgProductPlanCopy.selectedItem!=null){
				var productPlan:ProductPlan = (contractCopyProduct.dgProductPlanCopy.selectedItem as ProductPlan);
				contractCopyProduct.listCoveragePlan = productPlan.coveragePlans;
			}
		}
			
		public function loadInfoSelectedProduct(selectedProductVersion:ProductVersion):void{
			contractCopyProduct.copiedProductVersion = new ProductVersion();
			contractCopyProduct.copiedProductVersion = selectedProductVersion;
			contractCopyProduct.loadForm();
		}
					
		public function resetForm():void{
			contractCopyProduct.copiedProductVersion=null;
			contractCopyProduct.productName.text="";
			contractCopyProduct.productName.errorString="";
			contractCopyProduct.txtProductCode.text="";
			contractCopyProduct.txtProductCode.errorString="";			
			contractCopyProduct.mdfEffectiveDate.data=null;
			contractCopyProduct.mdfEffectiveDate.text="";
			contractCopyProduct.mdfEffectiveDate.errorString="";
			contractCopyProduct.txtDescription.text="";
			contractCopyProduct.txtDescription.errorString="";
			contractCopyProduct.listProductPlan=null
			contractCopyProduct.listCoveragePlan=null;
		}
		
		public function enabledFormCopy(pValue:Boolean):void{
			contractCopyProduct.productName.enabled=pValue;
			contractCopyProduct.txtProductCode.enabled=pValue;
			contractCopyProduct.mdfEffectiveDate.enabled=pValue;
			contractCopyProduct.txtDescription.enabled=pValue;
			contractCopyProduct.dgProductPlanCopy.enabled=pValue;
			contractCopyProduct.dgPlanCoverageCopy.enabled=pValue;			
		}
      	
		override public function listNotificationInterests():Array{
			return [];
		}
		
		override public function handleNotification(notification:INotification):void{
            switch (notification.getName())  
            {}
		}
	}
}