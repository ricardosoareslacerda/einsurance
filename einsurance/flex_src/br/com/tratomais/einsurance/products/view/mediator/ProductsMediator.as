package br.com.tratomais.einsurance.products.view.mediator
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.components.DoubleClickManager;
	import br.com.tratomais.einsurance.products.model.proxy.ProductProxy;
	import br.com.tratomais.einsurance.products.view.components.ProductsForm;
	import br.com.tratomais.einsurance.useradm.model.vo.Partner;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator; 

	public class ProductsMediator extends Mediator implements IMediator  
	{
	    public static const NAME:String = 'ProductMediator';  
        private var doubleCLickManager:DoubleClickManager = null;
		private var productProxy : ProductProxy;
		               
		public function ProductsMediator(viewComponent:Object)  
        {  
            super(NAME, viewComponent);
        } 
        
        protected function get productsForm():ProductsForm{  
          return viewComponent as ProductsForm;  
        }
		
		override public function onRegister():void 
		{
			super.onRegister();
			productProxy = ProductProxy ( facade.retrieveProxy(ProductProxy.NAME));
			productsForm.addEventListener(ProductsForm.INITIALIZE_PRODUCTS, initializeProducts);
		}
		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
			productsForm.removeEventListener(ProductsForm.INITIALIZE_PRODUCTS, initializeProducts);
		}
		
		private function initializeProducts(event: Event):void{
			productProxy.findProductsbyPartner(productsForm.partnerId, 1);
		}
		
		private function viewItem(event : Event):void{
		
		}
		
		public function populateListProducts (list : ArrayCollection) : void {
			productsForm.containerProducts.dataProvider = list;
 		}
		

          
        override public function listNotificationInterests():Array  
        {  
            return [
              		NotificationList.PRODUCT_LIST_REFRESH,
              		NotificationList.PARNTER_CHANGE
             ];  
        }
		          
        override public function handleNotification(notification:INotification):void  
        {  
            switch ( notification.getName() )  
            {  
           	case NotificationList.PRODUCT_LIST_REFRESH:
				this.populateListProducts(notification.getBody() as ArrayCollection);
	   	  		break;
   	  		case NotificationList.PARNTER_CHANGE:
				productsForm.partnerId = ((notification.getBody() as Partner).partnerId);
	  			break;
    	  	}
    	}    
	}  
}