package br.com.tratomais.einsurance.products.business
{  
    import br.com.tratomais.einsurance.core.UtilitiesService;
    import br.com.tratomais.einsurance.policy.model.vo.Item;
    import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
    import br.com.tratomais.einsurance.products.model.vo.ProductVersion;
    
    import mx.rpc.IResponder;  
      
    public class ProductDelegateProxy  
    {  
        private var responder : IResponder;  
        private var service : Object;  
          
        public function ProductDelegateProxy(pResponder : IResponder )  
        {  
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_PRODUCT);
			responder = pResponder;
        }
        
		/*
		 * #####  APPLICATION  ####
		 */
		public function findApplicationProperty(applicationId:int, propertyName:String):void{
			var call:Object = service.getApplicationPropertyValue(applicationId, propertyName);
			call.addResponder(responder);
        }
      
 		public function findProductsbyPartner(partnerId : int, insurerId : int, refDate : Date):void
        {  
            var call:Object = service.findProductsbyPartner(partnerId, insurerId, refDate);  
            call.addResponder(responder);  
        }
        
        public function listCoveragePlanByRefDate(productId : int, coveragePlanId : int, refDate : Date) : void{
        	var call:Object = service.listCoveragePlanByRefDate(productId, coveragePlanId, refDate);
        	call.addResponder(responder);
        }
        
        public function listCoverageOptionByRefDate(productId : int, coveragePlanId : int, refDate : Date) : void{
        	var call:Object = service.listCoverageOptionByRefDate(productId, coveragePlanId, refDate);
        	call.addResponder(responder);
        }
        
        public function findPlanByProductId(productId : int, refDate : Date) : void{
        	var call:Object = service.findPlanByProductId(productId, refDate);
        	call.addResponder(responder);
        }
        
        public function listCoveragePlanByProductId(productId : int, riskPlanId:int, refDate : Date) : void{
        	var call:Object = service.listCoveragePlanByProductId(productId, riskPlanId, refDate);
        	call.addResponder(responder);
        }
        
        public function findRiskPlanByProductId(productId : int, refDate : Date) : void{
        	var call:Object = service.findRiskPlanByProductId(productId, refDate);
        	call.addResponder(responder);
        }
        
        public function listPersonalRiskPlanByProductId(productId : int, refDate : Date) : void{
        	var call:Object = service.listPersonalRiskPlanByProductId(productId, refDate);
        	call.addResponder(responder);
        }
        
        public function loadProposalList():void{
        	var call:Object = service.loadProposalList();
        	call.addResponder(responder);        	
        }
        
		public function listAllProduct():void
		{
			var call:Object = service.listAllProduct();
			call.addResponder(responder);
		}
        
		public function listCountry(identifiedList:IdentifiedList):void
		{
			var call:Object = service.listCountry(identifiedList);
			call.addResponder(responder);
		}
				
		public function listStateByCountry(identifiedList:IdentifiedList, countryId:int):void
		{
			var call:Object = service.listStateByCountry(identifiedList, countryId);
			call.addResponder(responder);
		}

		public function listIdentifiedListCity(identifiedList:IdentifiedList):void{
			var call:Object = service.listIdentifiedListCity(identifiedList);
			call.addResponder(responder);			
		}

		public function listCityByState(identifiedList:IdentifiedList, stateId:int):void
		{
			var call:Object = service.listCityByState(identifiedList, stateId);
			call.addResponder(responder);
		}
        
        public function findProductToBeCopy(productId:int):void{
        	var call:Object = service.findProductToBeCopy(productId);
        	call.addResponder(responder);
        }
        
        public function getProductVersionByProduct(productId:int):void{
        	var call:Object = service.getProductVersionByProduct(productId);
        	call.addResponder(responder);
        }
        
        public function findProductsPerPartner(partnerId:int):void{
        	var call:Object = service.findProductsPerPartner(partnerId);
        	call.addResponder(responder);
        }

        public function getRiskPlanById(productId:int, id : int, referDate:Date):void{
        	var call:Object = service.getRiskPlanById(productId, id, referDate);
        	call.addResponder(responder);
        }

        public function saveProductVersionCopy(inputProductVersion:ProductVersion):void{
        	var call:Object = service.saveProductVersionCopy(inputProductVersion);
        	call.addResponder(responder);
        }

        public function listPlanKinshipByRiskPlan(productId:int, riskPlanId:int, renewalType:int, refDate:Date) : void{
        	var call:Object = service.listPlanKinshipByRiskPlan(productId, riskPlanId, renewalType, refDate);
        	call.addResponder(responder);
        }
        
        public function listCoverageRangeValueByRefDate(productId:int, coveragePlanId:int, coverageId:int, refDate:Date):void{
        	var call:Object = service.listCoverageRangeValueByRefDate(productId, coveragePlanId, coverageId, refDate);
        	call.addResponder(responder);
        }
        
        public function listPlanRiskTypeDescriptionByRefDate(productId:int, planId:int, refDate:Date):void{
        	var call:Object = service.listPlanRiskTypeDescriptionByRefDate(productId, planId, refDate)
        	call.addResponder(responder);
        }

        public function listPlanKinshipByPersonalRiskType(productId:int, planId:int, personalRiskType:int, renewalType:int, refDate:Date):void{
        	var call:Object = service.listPlanKinshipByPersonalRiskType(productId, planId, personalRiskType, renewalType, refDate);
        	call.addResponder(responder);
        }
        
        public function listAutoBrands():void{
        	var call:Object = service.listAutoBrands();
        	call.addResponder(responder);
        }
        
        public function listAutoModelForBrand( autoBrandId:int ):void{
        	var call:Object = service.listAutoModelForBrand( autoBrandId );
        	call.addResponder(responder);
        } 
        
		public function listAutoYear( autoModelId:int, referenceDate:Date ):void{
        	var call:Object = service.listAutoYear( autoModelId, referenceDate );
        	call.addResponder(responder);
        }                
		public function getAutoCost( autoModelId:int, year:int, referenceDate:Date ):void{
        	var call:Object = service.getAutoCost( autoModelId, year, referenceDate );
        	call.addResponder(responder);
        } 
        	
		public function findAutoVersion( autoModelId: int, effectiveDate:Date ):void{
        	var call:Object = service.findAutoVersion( autoModelId, effectiveDate );
        	call.addResponder(responder);
        }  
        	
		public function findProfileList( productId:int, referenceDate:Date):void{
        	var call:Object = service.findProfileList( productId, referenceDate );
        	call.addResponder(responder);
        }    
        	 
		/**
		 * Retrieves a product version based on productId and Date
		 * @param productId ProductId
		 * @param data Date of reference
		 */
        public function getProductVersionByRefDate(productId: int, data: Date): void {
        	var call:Object = service.getProductVersionByRefDate(productId, data);
        	call.addResponder(responder);        	
        }

        public function getProduct( productId:int, referenceDate:Date ):void
        {
        	var call:Object = service.getProduct( productId, referenceDate );
        	call.addResponder(responder);        	
        }        

		public function getProductVersionByBetweenRefDate( productId:int, referenceDate:Date ):void
        {
         	var call:Object = service.getProductVersionByBetweenRefDate(productId, referenceDate);
        	call.addResponder(responder); 
        }
        	
		public function getCoverageInsuredValue(item : Item, ruleType : int, productId : int, coverageId : int, referenceDate : Date):void{
        	var call:Object = service.getCoverageInsuredValue( item, ruleType, productId, coverageId, referenceDate );
        	call.addResponder(responder);
        }     
        
		public function getResponseRelationship(productId : int, questionnaireId : int, questionId : int, responseId : int, referenceDate : Date):void{
        	var call:Object = service.getResponseRelationship( productId, questionnaireId, questionId, responseId, referenceDate );
        	call.addResponder(responder);
        }     
        
		public function listAllDeductible():void{
        	var call:Object = service.listAllDeductible();
        	call.addResponder(responder);
        }

        public function getApplicationPropertyValue( applicationID:int, name:String ):void
        {
        	var call:Object = service.getApplicationPropertyValue( applicationID, name );
        	call.addResponder(responder);
        }
	
	public function listProductPaymentTermByRefDate(productId:int, refDate:Date):void{
		var call:Object = service.listProductPaymentTermByRefDate(productId, refDate);
		call.addResponder(responder);
	}
	
        public function getIdentifiedListApplicationPropertyValue( identifiedList:IdentifiedList, applicationID:int, name:String ):void
        {
        	var call:Object = service.getApplicationPropertyValue( identifiedList, applicationID, name );
        	call.addResponder(responder);
        }
    }
}