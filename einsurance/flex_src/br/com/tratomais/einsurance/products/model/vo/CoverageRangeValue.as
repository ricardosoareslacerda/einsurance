package br.com.tratomais.einsurance.products.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
	[RemoteClass(alias="br.com.tratomais.core.model.product.CoverageRangeValue")]	
	[Managed]
	public class CoverageRangeValue extends HibernateBean
	{

		private var _id:CoverageRangeValueId;
		private var _expiryDate:Date;
		private var _rangeValue:Number;
		private var _currencyId:String;
		private var _startValue:Number;
		private var _endValue:Number;
		private var _description:String;
		private var _displayOrder:int;
		private var _registred:Date;
		private var _updated:Date;


		public function CoverageRangeValue()
		{
			
		}

		public function get id():CoverageRangeValueId{
			return _id;
		}

		public function set id(pData:CoverageRangeValueId):void{
			_id=pData;
		}

		public function get expiryDate():Date{
			return _expiryDate;
		}

		public function set expiryDate(pData:Date):void{
			_expiryDate=pData;
		}

		public function get rangeValue():Number{
			return _rangeValue;
		}

		public function set rangeValue(pData:Number):void{
			_rangeValue=pData;
		}

		public function get currencyId():String{
			return _currencyId;
		}

		public function set currencyId(pData:String):void{
			_currencyId=pData;
		}

		public function get startValue():Number{
			return _startValue;
		}

		public function set startValue(pData:Number):void{
			_startValue=pData;
		}

		public function get endValue():Number{
			return _endValue;
		}

		public function set endValue(pData:Number):void{
			_endValue=pData;
		}

		public function get description():String{
			return _description;
		}

		public function set description(pData:String):void{
			_description=pData;
		}

		public function get displayOrder():int{
			return _displayOrder;
		}

		public function set displayOrder(pData:int):void{
			_displayOrder=pData;
		}

		public function get registred():Date{
			return _registred;
		}

		public function set registred(pData:Date):void{
			_registred=pData;
		}

		public function get updated():Date{
			return _updated;
		}

		public function set updated(pData:Date):void{
			_updated=pData;
		}


		
	}
}