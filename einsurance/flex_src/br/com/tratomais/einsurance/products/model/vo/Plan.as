package br.com.tratomais.einsurance.products.model.vo 
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.Plan")]   
    [Managed]  
    public class Plan extends HibernateBean 
    {  
        private var _planId:int;  
        private var _plan:Plan;  
        private var _planCode:String;  
        private var _planType:int;  
        private var _name:String;  
        private var _nickName:String;  
        private var _registred:Date;  
        private var _updated:Date;  
		private var _planRelationshipsForPlanId:ArrayCollection;
		private var _planRelationshipsForParentId:ArrayCollection;
  
        public function Plan()  
        {  
        }  
  
        public function get planId():int{  
            return _planId;  
        }  
  
        public function set planId(pData:int):void{  
            _planId=pData;  
        }  
  
        public function get plan():Plan{  
            return _plan;  
        }  
  
        public function set plan(pData:Plan):void{  
            _plan=pData;  
        }  
  
        public function get planCode():String{  
            return _planCode;  
        }  
  
        public function set planCode(pData:String):void{  
            _planCode=pData;  
        }  
  
        public function get planType():int{  
            return _planType;  
        }  
  
        public function set planType(pData:int):void{  
            _planType=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get nickName():String{  
            return _nickName;  
        }  
  
        public function set nickName(pData:String):void{  
            _nickName=pData;  
        }  
  
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
  
		public function get planRelationshipsForPlanId():ArrayCollection
		{
			return _planRelationshipsForPlanId;
		}
		
		public function set planRelationshipsForPlanId(pData:ArrayCollection):void
		{
			_planRelationshipsForPlanId = pData;
		}
		
		public function get planRelationshipsForParentId():ArrayCollection
		{
			return _planRelationshipsForParentId;
		}
		
		public function set planRelationshipsForParentId(pData:ArrayCollection):void
		{
			_planRelationshipsForParentId = pData;
		}		
    }  
}