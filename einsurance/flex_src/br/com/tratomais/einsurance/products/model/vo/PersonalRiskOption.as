package br.com.tratomais.einsurance.products.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.PersonalRiskOption")]    
    [Bindable]  
	public class PersonalRiskOption
	{
		private var _personType:int;
		private var _personTypeValue:String;
		private var _personTypeDescription:String;		
		private var _quantityPersonalRisk:int;
		private var _quantityAdditional:int;
		private var _additionalNewborn:Boolean;
	
		public function PersonalRiskOption()
		{
		}
		
		public static const NATURAL_PERSON:int = 63; 
		public static const LEGALLY_PERSON:int = 64; 
		public static const NATURAL_PERSON_NAME:String = "F"; 
		public static const LEGALLY_PERSON_NAME:String = "J"; 
		
		
        public function get personType():int{  
            return _personType;  
        }  
  
        public function set personType(pData:int):void{  
            _personType=pData;  
        }

        public function get personTypeValue():String{  
            return _personTypeValue;  
        }  
  
        public function set personTypeValue(pData:String):void{  
            _personTypeValue=pData;  
        } 
 
        public function get personTypeDescription():String{  
            return _personTypeDescription;  
        }  
  
        public function set personTypeDescription(pData:String):void{  
            _personTypeDescription=pData;  
        }
        
        public function get quantityPersonalRisk():int{  
            return _quantityPersonalRisk;  
        }  
  
        public function set quantityPersonalRisk(pData:int):void{  
            _quantityPersonalRisk=pData;  
        }  
        
        public function get quantityAdditional():int{  
            return _quantityAdditional;  
        }  
  
        public function set quantityAdditional(pData:int):void{  
            _quantityAdditional=pData;  
        }  
        
        public function get additionalNewborn():Boolean{  
            return _additionalNewborn;  
        }  
  
        public function set additionalNewborn(pData:Boolean):void{  
            _additionalNewborn=pData;  
        }  

	}
}