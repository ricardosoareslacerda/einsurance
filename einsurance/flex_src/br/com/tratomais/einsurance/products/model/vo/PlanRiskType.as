package br.com.tratomais.einsurance.products.model.vo  
{ 
	import net.digitalprimates.persistence.hibernate.HibernateBean;
		
    [RemoteClass(alias="br.com.tratomais.core.model.product.PlanRiskType")]   
    [Managed]
    public class PlanRiskType extends HibernateBean  
    {  
  
        private var _id:PlanRiskTypeId;  
        private var _expiryDate:Date;  
        private var _quantityRisk:int;  
        private var _registred:Date;  
        private var _updated:Date; 
        private var _descriptionRisk:String;
        private var _descriptionRiskId:int;
        private var _defaultRisk:Boolean;
        private var _compulsory:Boolean;
        private var _riskValueRestatement:Boolean;
    
        public function PlanRiskType()  
        {  
              
        }  
  
        public function get id():PlanRiskTypeId{  
            return _id;  
        }  
  
        public function set id(pData:PlanRiskTypeId):void{  
            _id=pData;  
        }  
  
        public function get expiryDate():Date{  
            return _expiryDate;  
        }  
  
        public function set expiryDate(pData:Date):void{  
            _expiryDate=pData;  
        }  
  
        public function get quantityRisk():int{  
            return _quantityRisk;  
        }  
  
        public function set quantityRisk(pData:int):void{  
            _quantityRisk=pData;  
        }  
  
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
  
        public function get descriptionRisk():String{  
            return _descriptionRisk;  
        }  
  
        public function set descriptionRisk(pData:String):void{  
            _descriptionRisk=pData;  
        }   

        public function get descriptionRiskId():int{  
            return _descriptionRiskId;  
        }  
  
        public function set descriptionRiskId(pData:int):void{  
            _descriptionRiskId=pData;  
        } 

        public function get defaultRisk():Boolean{  
            return _defaultRisk;  
        }  
  
        public function set defaultRisk(pData:Boolean):void{  
            _defaultRisk=pData;  
        }
        
        public function get compulsory():Boolean{  
            return _compulsory;  
        }  
  
        public function set compulsory(pData:Boolean):void{  
            _compulsory=pData;  
        }
        
        public function get riskValueRestatement():Boolean{  
            return _riskValueRestatement;  
        }  
  
        public function set riskValueRestatement(pData:Boolean):void{  
            _riskValueRestatement=pData;  
        }   
    }  
}  