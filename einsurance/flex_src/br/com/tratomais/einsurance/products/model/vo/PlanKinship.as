package br.com.tratomais.einsurance.products.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;

    [RemoteClass(alias="br.com.tratomais.core.model.product.PlanKinship")]    
    [Managed]  
    public class PlanKinship extends HibernateBean  
    {  
        private var _id:PlanKinshipId;
        private var _kinshipGroup:KinshipGroup;
        private var _expiryDate:Date;
        private var _minimumAge:Number;
        private var _maximumAge:Number;
		private var _kinshipExclusive:Boolean;
        private var _registred:Date;
        private var _updated:Date;
        private var _transientKinshipDescription:String;
  
        public function PlanKinship(){  
        }
  
        public function get id():PlanKinshipId{  
            return _id;  
        }  
  
        public function set id(pData:PlanKinshipId):void{  
            _id=pData;
        }
        
        public function get kinshipGroup():KinshipGroup{  
            return _kinshipGroup;
        }  
  
        public function set kinshipGroup(pData:KinshipGroup):void{  
            _kinshipGroup=pData;
        }
  
        public function get expiryDate():Date{  
            return _expiryDate;  
        }  
  
        public function set expiryDate(pData:Date):void{  
            _expiryDate=pData;  
        }  
  
        public function get minimumAge():Number{  
            return _minimumAge;  
        }  
  
        public function set minimumAge(pData:Number):void{  
            _minimumAge=pData;  
        }  
  
        public function get maximumAge():Number{  
            return _maximumAge;  
        }  
  
        public function set maximumAge(pData:Number):void{  
            _maximumAge=pData;  
        }  
  
		public function get kinshipExclusive():Boolean{
			return _kinshipExclusive;
		}
	
		public function set kinshipExclusive(pData:Boolean):void{
			_kinshipExclusive = pData;
		}

        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
  
        public function get transientKinshipDescription():String{  
            return _transientKinshipDescription;  
        }  
  
        public function set transientKinshipDescription(pData:String):void{  
            _transientKinshipDescription=pData;  
        }  
  
  
          
    }  
}  