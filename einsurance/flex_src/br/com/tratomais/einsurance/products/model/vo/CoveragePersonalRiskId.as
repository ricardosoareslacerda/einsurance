package br.com.tratomais.einsurance.products.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.CoveragePersonalRiskId")]   
    [Managed]  
    public class CoveragePersonalRiskId  extends HibernateBean
    {  
  
        private var _effectiveDate:Date;  
        private var _personalRiskType:int;  
        private var _coverageId:int;  
        private var _planId:int;  
        private var _productId:int;  
  
  
        public function CoveragePersonalRiskId()  { 
        }  
  
        public function get effectiveDate():Date{  
            return _effectiveDate;  
        }  
  
        public function set effectiveDate(pData:Date):void{  
            _effectiveDate=pData;  
        }  
  
        public function get personalRiskType():int{  
            return _personalRiskType;  
        }  
  
        public function set personalRiskType(pData:int):void{  
            _personalRiskType=pData;  
        }  
  
        public function get coverageId():int{  
            return _coverageId;  
        }  
  
        public function set coverageId(pData:int):void{  
            _coverageId=pData;  
        }  
  
        public function get planId():int{  
            return _planId;  
        }  
  
        public function set planId(pData:int):void{  
            _planId=pData;  
        }  
  
        public function get productId():int{  
            return _productId;  
        }  
  
        public function set productId(pData:int):void{  
            _productId=pData;  
        }  
  
  
          
    }  
}  