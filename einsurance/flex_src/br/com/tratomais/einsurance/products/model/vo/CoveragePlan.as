package br.com.tratomais.einsurance.products.model.vo
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.CoveragePlan")]   
    [Managed]  
    public class CoveragePlan extends HibernateBean 
    {  
        private var _id:CoveragePlanId;  
        private var _plan:Plan;
        private var _coverage:Coverage;  
        private var _expiryDate:Date;  
        private var _nickName:String;  
        private var _roadmapCode:int;  
        private var _currencyId:int;  
        private var _subBranchId:int;  
		private var _basic:Boolean;
        private var _compulsory:Boolean;
        private var _insuredValueType:int;  
        private var _insuredValueBy:int;
        private var _factorRiskValue:Number;  
        private var _fixedInsuredValue:Number;  
        private var _changeInsuredValue:Boolean;
        private var _insuredValueLimitType:int;  
        private var _minimumInsuredValue:Number;  
        private var _maximumInsuredValue:Number;  
        private var _minimumPremium:Number;  
        private var _minimumRefund:Number;  
        private var _commissionIncidenceType:int;  
        private var _defaultCommissionFactor:Number;  
        private var _minimumCommissionFactor:Number;  
        private var _maximumCommissionFactor:Number;  
        private var _displayOrder:int;
        private var _printGroup:String;  
        private var _registred:Date;  
        private var _updated:Date;  
 		private var _selectedCoverage: Boolean;

        private var _coverageRangeValues:ArrayCollection;
        private var _coveragePersonalRisks:ArrayCollection;
              
        private var xxxx:CoveragePersonalRisk;
        
		public static const INSURED_VALUE_TYPE_CSA:int = 165; // Con Suma Asegurada
		public static const INSURED_VALUE_TYPE_SSA:int = 166; // Sin Suma Asegurada
		public static const INSURED_VALUE_TYPE_PVR:int = 167; // Porcentaje del Valor de Riesgo
		public static const INSURED_VALUE_TYPE_SSC:int = 169; // Sumatoria de las SubCoberturas
		public static const INSURED_VALUE_TYPE_RSA:int = 207; // Rango de Suma Asegurada
		public static const INSURED_VALUE_TYPE_CSP:int = 489; // Suma Asegurada por Pasajero
		public static const INSURED_VALUE_TYPE_CSR:int = 490; // Regla de Suma Asegurada
		public static const INSURED_VALUE_TYPE_SAF:int = 536; // Suma Asegurada Fija
		public static const INSURED_VALUE_TYPE_RSI:int = 734; // Rango de Suma Asegurada Informado
		public static const INSURED_VALUE_TYPE_PMR:int = 735; // Porcentaje de Multiplos Riesgos

		public static const INSURED_VALUE_BY_RSK:int = 740; // por Riesgo
		public static const INSURED_VALUE_BY_PSG:int = 741; // por Pasajero

        public function CoveragePlan(){  
        }  
  
        public function get id():CoveragePlanId{  
            return _id;  
        }  
  
        public function set id(pData:CoveragePlanId):void{  
            _id=pData;  
        }  
        
         public function get plan():Plan{  
            return _plan;  
        }  
  
        public function set plan(pData:Plan):void{  
            _plan=pData;  
        } 
  
        public function get coverage():Coverage{  
            return _coverage;  
        }  
  
        public function set coverage(pData:Coverage):void{  
            _coverage=pData;  
        }  
  
        public function get expiryDate():Date{  
            return _expiryDate;  
        }  
  
        public function set expiryDate(pData:Date):void{  
            _expiryDate=pData;  
        }  
  
        public function get nickName():String{  
            return _nickName;  
        }  
  
        public function set nickName(pData:String):void{  
            _nickName=pData;  
        }  
  
        public function get roadmapCode():int{  
            return _roadmapCode;  
        }  
  
        public function set roadmapCode(pData:int):void{  
            _roadmapCode=pData;  
        }  
  
        public function get currencyId():int{  
            return _currencyId;  
        }  
  
        public function set currencyId(pData:int):void{  
            _currencyId=pData;  
        }  
  
        public function get subBranchId():int{  
            return _subBranchId;  
        }  
  
        public function set subBranchId(pData:int):void{  
            _subBranchId=pData;  
        }
  
		public function get basic():Boolean {
			return _basic;
		}
	
		public function set basic(pData: Boolean):void {
			_basic = pData;
		}
	
        public function get compulsory(): Boolean { 
        	return _compulsory;
        }
        
        public function set compulsory(pData: Boolean): void {
        	this._compulsory = pData;
        }

        public function get insuredValueType():int{  
            return _insuredValueType;  
        }  
  
        public function set insuredValueType(pData:int):void{  
            _insuredValueType=pData;  
        }  

        public function get insuredValueBy():int{  
            return _insuredValueBy;  
        }  
  
        public function set insuredValueBy(pData:int):void{  
            _insuredValueBy=pData;  
        }  

        public function get factorRiskValue():Number{  
            return _factorRiskValue;  
        }  
  
        public function set factorRiskValue(pData:Number):void{  
            _factorRiskValue=pData;  
        }  
		
        public function get fixedInsuredValue():Number{  
            return _fixedInsuredValue;  
        }  
  
        public function set fixedInsuredValue(pData:Number):void{  
            _fixedInsuredValue=pData;  
        }  
  
        public function get changeInsuredValue(): Boolean { 
        	return _changeInsuredValue;
        }
        
        public function set changeInsuredValue(pData: Boolean): void {
        	this._changeInsuredValue = pData;
        }
		  
        public function get insuredValueLimitType():int{  
            return _insuredValueLimitType;  
        }  
  
        public function set insuredValueLimitType(pData:int):void{  
            _insuredValueLimitType=pData;  
        }  
  
        public function get minimumInsuredValue():int{  
            return _minimumInsuredValue;  
        }  
  
        public function set minimumInsuredValue(pData:int):void{  
            _minimumInsuredValue=pData;  
        }  
  
        public function get maximumInsuredValue():int{  
            return _maximumInsuredValue;  
        }  
  
        public function set maximumInsuredValue(pData:int):void{  
            _maximumInsuredValue=pData;  
        }  
  
        public function get minimumPremium():Number{  
            return _minimumPremium;  
        }  
  
        public function set minimumPremium(pData:Number):void{  
            _minimumPremium=pData;  
        }  
  
        public function get minimumRefund():Number{  
            return _minimumRefund;  
        }  
  
        public function set minimumRefund(pData:Number):void{  
            _minimumRefund=pData;  
        }  
  
        public function get commissionIncidenceType():int{  
            return _commissionIncidenceType;  
        }  
  
        public function set commissionIncidenceType(pData:int):void{  
            _commissionIncidenceType=pData;  
        }  
  
        public function get defaultCommissionFactor():Number{  
            return _defaultCommissionFactor;  
        }  
  
        public function set defaultCommissionFactor(pData:Number):void{  
            _defaultCommissionFactor=pData;  
        }  
  
        public function get minimumCommissionFactor():Number{  
            return _minimumCommissionFactor;  
        }  
  
        public function set minimumCommissionFactor(pData:Number):void{  
            _minimumCommissionFactor=pData;  
        }  
  
        public function get maximumCommissionFactor():Number{  
            return _maximumCommissionFactor;  
        }  
  
        public function set maximumCommissionFactor(pData:Number):void{  
            _maximumCommissionFactor=pData;  
        }  
  
        public function get displayOrder():int{  
            return _displayOrder;  
        }  
  
        public function set displayOrder(pData:int):void{  
            _displayOrder=pData;  
        }  
  
	  	public function get printGroup():String {
			return _printGroup;
		}
	
		public function set printGroup(pData:String):void {
			_printGroup = pData;
		}
  
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        } 
        
        public function get selectedCoverage():Boolean{  
            return _selectedCoverage;  
        }  
  
        public function set selectedCoverage(pData:Boolean):void{  
            _selectedCoverage=pData;  
        } 
                
        public function get coverageRangeValues():ArrayCollection{  
            return _coverageRangeValues;  
        }  
  
        public function set coverageRangeValues(pData:ArrayCollection):void{  
            _coverageRangeValues=pData;  
        }  
  
        public function get coveragePersonalRisks():ArrayCollection{  
            return _coveragePersonalRisks;  
        }  
  
        public function set coveragePersonalRisks(pData:ArrayCollection):void{  
            _coveragePersonalRisks=pData;  
        } 
     
    }  
} 