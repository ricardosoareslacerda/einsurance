package br.com.tratomais.einsurance.products.model.vo  
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.ProductPlan")]    
    [Managed]  
    public class ProductPlan  extends HibernateBean
    {  
        private var _id:ProductPlanId;  
        private var _product : Product;
        private var _plan:Plan;  
        private var _expiryDate:Date;  
        private var _planCode:String;  
        private var _nickName:String;  
        private var _standard:Boolean;
        private var _fixedInsuredValue:Number;
        private var _displayOrder:int;
        private var _registred:Date;  
        private var _updated:Date;
          
        private var _coveragePlans:ArrayCollection;  
        private var _planPersonalRisks:ArrayCollection;  
        private var _planKinships:ArrayCollection;  
  		private var _quantityPersonalRisk:int;
		private var _quantityAdditional:int;
		private var _additionalNewborn:Boolean;
		private var _planRiskTypes:ArrayCollection;
		private var _riskCoverageRangeValues:ArrayCollection;
		private var _planRiskTypeDefault:PlanRiskType;
	
		private var _xxx:PlanRiskType;
	
        public function ProductPlan(){  
        }  
  
        public function get id():ProductPlanId{  
            return _id;  
        }  
  
        public function set id(pData:ProductPlanId):void{  
            _id=pData;  
        }  
  
  		 public function get product():Product{  
            return _product;  
        }  
  
        public function set product(pData:Product):void{  
            _product=pData;  
        }  
        
        public function get plan():Plan{  
            return _plan;  
        }  
  
        public function set plan(pData:Plan):void{  
            _plan=pData;  
        }  
  
        public function get expiryDate():Date{  
            return _expiryDate;  
        }  
  
        public function set expiryDate(pData:Date):void{  
            _expiryDate=pData;  
        }  
  
        public function get planCode():String{  
            return _planCode;  
        }  
  
        public function set planCode(pData:String):void{  
            _planCode=pData;  
        }  
  
        public function get nickName():String{  
            return _nickName;  
        }  
  
        public function set nickName(pData:String):void{  
            _nickName=pData;  
        }
        
        public function get standard():Boolean { 
        	return _standard;
        }
        
        public function set standard(pData:Boolean): void {
        	_standard=pData;
        }
        
        public function get fixedInsuredValue():Number{  
            return _fixedInsuredValue;  
        }  
  
        public function set fixedInsuredValue(pData:Number):void{  
            _fixedInsuredValue=pData;  
        } 
    
        public function get displayOrder():int{  
            return _displayOrder;  
        }  
  
        public function set displayOrder(pData:int):void{  
            _displayOrder=pData;  
        }  
        
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
  
        public function get coveragePlans():ArrayCollection{  
            return _coveragePlans;  
        }  
  
        public function set coveragePlans(pData:ArrayCollection):void{  
            _coveragePlans=pData;  
        }  
  
        public function get planPersonalRisks():ArrayCollection{  
            return _planPersonalRisks;  
        }  
  
        public function set planPersonalRisks(pData:ArrayCollection):void{  
            _planPersonalRisks=pData;  
        }  
  
        public function get planKinships():ArrayCollection{  
            return _planKinships;  
        }  
  
        public function set planKinships(pData:ArrayCollection):void{  
            _planKinships=pData;  
        }  

        public function get planRiskTypes():ArrayCollection{  
            return _planRiskTypes;  
        }  
  
        public function set planRiskTypes(pData:ArrayCollection):void{  
            _planRiskTypes=pData;  
        } 
        	
		public function get quantityPersonalRisk():int{  
            return _quantityPersonalRisk;  
        }  
  
        public function set quantityPersonalRisk(pData:int):void{  
            _quantityPersonalRisk=pData;  
        }  
        
        public function get quantityAdditional():int{  
            return _quantityAdditional;  
        }  
  
        public function set quantityAdditional(pData:int):void{  
            _quantityAdditional=pData;  
        }  
        
        public function get additionalNewborn():Boolean{  
            return _additionalNewborn;  
        }  
  
        public function set additionalNewborn(pData:Boolean):void{  
            _additionalNewborn=pData;  
        } 
        
        public function get riskCoverageRangeValues():ArrayCollection{  
            return _riskCoverageRangeValues;  
        }  
  
        public function set riskCoverageRangeValues(pData:ArrayCollection):void{  
            _riskCoverageRangeValues=pData;  
        }
        
        public function getPlanRiskTypeDefault():PlanRiskType
        {
        	for each( var planRiskType:PlanRiskType in planRiskTypes )
        	{
        		if( planRiskType.defaultRisk )
        			return planRiskType;
        	}
        	
        	return null;
        }
    }  
}  