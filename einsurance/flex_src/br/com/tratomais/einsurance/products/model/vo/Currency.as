package br.com.tratomais.einsurance.products.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.Currency")]   
    [Managed]  
    public class Currency extends HibernateBean 
    {  
        private var _currencyId:int;  
        private var _isocode:String;  
        private var _symbol:String;  
        private var _name:String;  
        private var _standard:Boolean;
        private var _registred:Date;  
        private var _updated:Date;  
        private var _active:Boolean;

        public function Currency()  
        {  
        }  
  
        public function get currencyId():int{  
            return _currencyId;  
        }  
  
        public function set currencyId(pData:int):void{  
            _currencyId=pData;  
        }  
  
        public function get isocode():String{  
            return _isocode;  
        }  
  
        public function set isocode(pData:String):void{  
            _isocode=pData;  
        }  
  
        public function get symbol():String{  
            return _symbol;  
        }  
  
        public function set symbol(pData:String):void{  
            _symbol=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get standard():Boolean { 
        	return _standard;
        }
        
        public function set standard(pData:Boolean):void {
        	_standard=pData;
        }   
        
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
  
        public function get active(): Boolean { 
        	return _active;
        }
        
        public function set active(pActive: Boolean): void {
        	this._active = pActive;
        }        
    }  
}