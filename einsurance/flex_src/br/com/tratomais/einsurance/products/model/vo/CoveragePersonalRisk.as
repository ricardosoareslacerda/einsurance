package br.com.tratomais.einsurance.products.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
	
    [RemoteClass(alias="br.com.tratomais.core.model.product.CoveragePersonalRisk")]     
	[Managed]  
    public class CoveragePersonalRisk extends HibernateBean
    {  
  
        private var _id:CoveragePersonalRiskId;  
        private var _expiryDate:Date;  
        private var _registred:Date;  
        private var _updated:Date;  
  
  
        public function CoveragePersonalRisk()  
        {  
              
        }  
  
        public function get id():CoveragePersonalRiskId{  
            return _id;  
        }  
  
        public function set id(pData:CoveragePersonalRiskId):void{  
            _id=pData;  
        }  
  
        public function get expiryDate():Date{  
            return _expiryDate;  
        }  
  
        public function set expiryDate(pData:Date):void{  
            _expiryDate=pData;  
        }  
  
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
  
  
          
    }  
}  