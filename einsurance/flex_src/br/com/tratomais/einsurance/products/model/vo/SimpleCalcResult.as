package br.com.tratomais.einsurance.products.model.vo  
{
	import br.com.tratomais.einsurance.policy.model.vo.Endorsement;
	
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	

	[RemoteClass(alias="br.com.tratomais.core.model.product.SimpleCalcResult")]	
	[Bindable]
	public class SimpleCalcResult extends HibernateBean
	{

		private var _endorsement:Endorsement;
		private var _paymentOptions:ArrayCollection;
		private var _erros:ArrayCollection;


		public function SimpleCalcResult()
		{
			
		}

		public function get endorsement():Endorsement{
			return _endorsement;
		}

		public function set endorsement(pData:Endorsement):void{
			_endorsement=pData;
		}

		public function get paymentOptions():ArrayCollection{
			return _paymentOptions;
		}

		public function set paymentOptions(pData:ArrayCollection):void{
			_paymentOptions=pData;
		}

		public function get erros():ArrayCollection{
			return _erros;
		}
 
		public function set erros(pData:ArrayCollection):void{
			_erros=pData;
		}


		
	}
}