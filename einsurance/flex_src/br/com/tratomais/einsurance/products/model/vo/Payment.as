package br.com.tratomais.einsurance.products.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
      
    [RemoteClass(alias="br.com.tratomais.core.model.product.Payment")]     
    [Managed]  
    public class Payment extends HibernateBean  
    {  
 
        public function Payment()  
        {  
        }  
        
        private var _paymentMethod:String;  
        private var _valueQuota:Number;  
        private var _valueTotal:Number;  

     	public function get paymentMethod():String{  
            return _paymentMethod;  
        }  
  
        public function set paymentMethod(pData:String):void{  
            _paymentMethod=pData;  
        }  

     	public function get valueQuota():Number{  
            return _valueQuota;  
        }  
  
        public function set valueQuota(pData:Number):void{  
            _valueQuota=pData;  
        }  

     	public function get valueTotal():Number{  
            return _valueTotal;  
        }  
  
        public function set valueTotal(pData:Number):void{  
            _valueTotal=pData;  
        }  
    }
} 