package br.com.tratomais.einsurance.products.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.PlanPersonalRisk")]     
    [Managed]  	
	public class PlanPersonalRisk extends HibernateBean
	{

		private var _id:PlanPersonalRiskId;
		private var _plan:Plan;
		private var _expiryDate:Date;
		private var _quantityPersonalRisk:int;
		private var _quantityAdditional:int;
		private var _additionalNewborn:Boolean;
		private var _registred:Date;
		private var _updated:Date;
			
		public function PlanPersonalRisk(){
		}

        public function get id():PlanPersonalRiskId{  
            return _id;
        }  
  
        public function set id(pData:PlanPersonalRiskId):void{  
            _id=pData;
        }
        
        public function get plan():Plan{  
            return _plan;
        }  
  
        public function set plan(pData:Plan):void{  
            _plan=pData;
        }        

        public function get expiryDate():Date{  
            return _expiryDate;
        }  
  
        public function set expiryDate(pData:Date):void{  
            _expiryDate=pData;
        }

        public function get quantityPersonalRisk():int{  
            return _quantityPersonalRisk;
        }  
  
        public function set quantityPersonalRisk(pData:int):void{  
            _quantityPersonalRisk=pData;
        }
        
        public function get quantityAdditional():int{  
            return _quantityAdditional;
        }  
  
        public function set quantityAdditional(pData:int):void{  
            _quantityAdditional=pData;
        }
        
        public function get additionalNewborn():Boolean{  
            return _additionalNewborn;
        }  
  
        public function set additionalNewborn(pData:Boolean):void{  
            _additionalNewborn=pData;
        }
        
        public function get registred():Date{
            return _registred;
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;
        }
        
       	public function get updated():Date{
            return _updated;
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;
        }
        
	}
}