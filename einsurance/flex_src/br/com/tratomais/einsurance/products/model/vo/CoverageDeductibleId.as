package br.com.tratomais.einsurance.products.model.vo
{
	

	
	[RemoteClass(alias="br.com.tratomais.core.model.product.CoverageDeductibleId")]	
	[Managed]
	public class CoverageDeductibleId
	{

		private var _coverageId:int;
		private var _deductibleId:int;
		private var _effectiveDate:Date;


		public function CoverageDeductibleId()
		{
			
		}

		public function get coverageId():int{
			return _coverageId;
		}

		public function set coverageId(pData:int):void{
			_coverageId=pData;
		}

		public function get deductibleId():int{
			return _deductibleId;
		}

		public function set deductibleId(pData:int):void{
			_deductibleId=pData;
		}

		public function get effectiveDate():Date{
			return _effectiveDate;
		}

		public function set effectiveDate(pData:Date):void{
			_effectiveDate=pData;
		}


		
	}
}