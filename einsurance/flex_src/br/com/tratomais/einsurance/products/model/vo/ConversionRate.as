package br.com.tratomais.einsurance.products.model.vo   
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.ConversionRate")]     
    [Managed]  
    public class ConversionRate extends HibernateBean 
    {  
        private var _conversionRateId:int;  
        private var _currencyId:int;  
        private var _currencyDate:Date;  
        private var _multipleRateBy:Number;  
        private var _registred:Date;  
        private var _updated:Date;  
  
        public function ConversionRate()  
        {  
        }  
  
        public function get conversionRateId():int{  
            return _conversionRateId;  
        }  
  
        public function set conversionRateId(pData:int):void{  
            _conversionRateId=pData;  
        }  
  
        public function get currencyId():int{  
            return _currencyId;  
        }  
  
        public function set currencyId(pData:int):void{  
            _currencyId=pData;  
        }  
  
        public function get currencyDate():Date{  
            return _currencyDate;  
        }  
  
        public function set currencyDate(pData:Date):void{  
            _currencyDate=pData;  
        }  
  
        public function get multipleRateBy():Number{  
            return _multipleRateBy;  
        }  
  
        public function set multipleRateBy(pData:Number):void{  
            _multipleRateBy=pData;  
        }  
  
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
    }  
}