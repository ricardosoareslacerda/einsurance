package br.com.tratomais.einsurance.products.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.CoverageRelationshipId")]   
    [Managed]  
    public class CoverageRelationshipId extends HibernateBean 
    {  
        private var _coverageId:int;  
        private var _parentId:int; 
        private var _coverageRelationshipType:int; 
        private var _effectiveDate:Date;
        
        public function CoverageRelationshipId()  
        {  
        }  
  
        public function get coverageId():int{  
            return _coverageId;  
        }  
  
        public function set coverageId(pData:int):void{  
            _coverageId=pData;  
        }  
  
        public function get parentId():int{  
            return _parentId;  
        }  
  
        public function set parentId(pData:int):void{  
            _parentId=pData;  
        }     
  
        public function get coverageRelationshipType():int{  
            return _coverageRelationshipType;  
        }  
  
        public function set coverageRelationshipType(pData:int):void{  
            _coverageRelationshipType=pData;  
        }     
  
        public function get effectiveDate():Date{  
            return _effectiveDate;  
        }  
  
        public function set effectiveDate(pData:Date):void{  
            _effectiveDate=pData;  
        }     
    }  
}