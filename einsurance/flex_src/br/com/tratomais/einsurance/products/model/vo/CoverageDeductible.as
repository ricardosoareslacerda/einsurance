package br.com.tratomais.einsurance.products.model.vo
{
	

	
	[RemoteClass(alias="br.com.tratomais.core.model.product.CoverageDeductible")]	
	[Managed]
	public class CoverageDeductible
	{

		private var _id:CoverageDeductibleId;
		private var _deductible:Deductible;
		private var _coverage:Coverage;
		private var _expiryDate:Date;
		private var _incidenceValue:Number;
		private var _minimumValue:Number;
		private var _maximumValue:Number;
		private var _premiumFactor:Number;
		private var _displayOrder:Short;
		private var _registred:Date;
		private var _updated:Date;


		public function CoverageDeductible()
		{
			
		}

		public function get id():CoverageDeductibleId{
			return _id;
		}

		public function set id(pData:CoverageDeductibleId):void{
			_id=pData;
		}

		public function get deductible():Deductible{
			return _deductible;
		}

		public function set deductible(pData:Deductible):void{
			_deductible=pData;
		}

		public function get coverage():Coverage{
			return _coverage;
		}

		public function set coverage(pData:Coverage):void{
			_coverage=pData;
		}

		public function get expiryDate():Date{
			return _expiryDate;
		}

		public function set expiryDate(pData:Date):void{
			_expiryDate=pData;
		}

		public function get incidenceValue():Number{
			return _incidenceValue;
		}

		public function set incidenceValue(pData:Number):void{
			_incidenceValue=pData;
		}

		public function get minimumValue():Number{
			return _minimumValue;
		}

		public function set minimumValue(pData:Number):void{
			_minimumValue=pData;
		}

		public function get maximumValue():Number{
			return _maximumValue;
		}

		public function set maximumValue(pData:Number):void{
			_maximumValue=pData;
		}

		public function get premiumFactor():Number{
			return _premiumFactor;
		}

		public function set premiumFactor(pData:Number):void{
			_premiumFactor=pData;
		}

		public function get displayOrder():Short{
			return _displayOrder;
		}

		public function set displayOrder(pData:Short):void{
			_displayOrder=pData;
		}

		public function get registred():Date{
			return _registred;
		}

		public function set registred(pData:Date):void{
			_registred=pData;
		}

		public function get updated():Date{
			return _updated;
		}

		public function set updated(pData:Date):void{
			_updated=pData;
		}


		
	}
}