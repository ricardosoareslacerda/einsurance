package br.com.tratomais.einsurance.products.model.vo 
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
	[RemoteClass(alias="br.com.tratomais.core.model.product.ResponseRelationshipId")]	
	[Managed]
	public class ResponseRelationshipId extends HibernateBean
	{
		private var _productId:int;
		private var _questionnaireId:int;
		private var _questionId:int;
		private var _responseId:int;
		private var _responseRelationshipType:int;
		private var _questionIdrelationship:int;
		private var _effectiveDate:Date;

		public function ResponseRelationshipId()
		{
			
		}

		public function get productId():int{
			return _productId;
		}

		public function set productId(pData:int):void{
			_productId=pData;
		}

		public function get questionnaireId():int{
			return _questionnaireId;
		}

		public function set questionnaireId(pData:int):void{
			_questionnaireId=pData;
		}

		public function get questionId():int{
			return _questionId;
		}

		public function set questionId(pData:int):void{
			_questionId=pData;
		}

		public function get responseId():int{
			return _responseId;
		}

		public function set responseId(pData:int):void{
			_responseId=pData;
		}

		public function get responseRelationshipType():int{
			return _responseRelationshipType;
		}

		public function set responseRelationshipType(pData:int):void{
			_responseRelationshipType=pData;
		}

		public function get questionIdrelationship():int{
			return _questionIdrelationship;
		}

		public function set questionIdrelationship(pData:int):void{
			_questionIdrelationship=pData;
		}

		public function get effectiveDate():Date{
			return _effectiveDate;
		}

		public function set effectiveDate(pData:Date):void{
			_effectiveDate=pData;
		}
	}
}