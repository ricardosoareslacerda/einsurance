package br.com.tratomais.einsurance.products.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	

	[RemoteClass(alias="br.com.tratomais.core.model.product.PaymentOption")]	
	[Bindable]
	public class PaymentOption extends HibernateBean
	{

		private var _paymentTermName:String;
		private var _firstInstallmentValue:Number;
		private var _nextInstallmentValue:Number;
		private var _paymentId:int;
		private var _totalPremium:Number;
		private var _netPremium:Number;
		private var _fractioningAdditional:Number;
		private var _taxRate:Number;
		private var _taxValue:Number;
		private var _billingMethodId:int;
		private var _paymentType:int;
		private var _bankNumber:int;
		private var _bankAgencyNumber:String;
		private var _bankAccountNumber:String;
		private var _bankCheckNumber:String;
		private var _cardType:int;
		private var _cardBrandType:int;
		private var _cardNumber:String;
		private var _cardExpirationDate:int;
		private var _cardHolderName:String;
		private var _invoiceNumber:int;
		private var _otherAccountNumber:String;
		private var _debitAuthorizationCode:int;
		private var _creditAuthorizationCode:int;
		private var _billingMethod:BillingMethod;
		private var _isDefault:Boolean;
		private var _quantityInstallment:int;
		private var _nextInstallment:int;
		private var _nextPaymentDate:Date;
		private var _dueDay:int;
		private var _selected:Boolean;	
				
		public function PaymentOption()
		{
			
		}

		public function get paymentTermName():String{
			return _paymentTermName;
		}

		public function set paymentTermName(pData:String):void{
			_paymentTermName=pData;
		}

		public function get firstInstallmentValue():Number{
			return _firstInstallmentValue;
		}

		public function set firstInstallmentValue(pData:Number):void{
			_firstInstallmentValue=pData;
		}

		public function get nextInstallmentValue():Number{
			return _nextInstallmentValue;
		}

		public function set nextInstallmentValue(pData:Number):void{
			_nextInstallmentValue=pData;
		}
		
		public function get nextPaymentDate():Date{
			return _nextPaymentDate;
		}

		public function set nextPaymentDate(pData:Date):void{
			_nextPaymentDate=pData;
		}
		
		public function get paymentId():int{
			return _paymentId;
		}

		public function set paymentId(pData:int):void{
			_paymentId=pData;
		}

		public function get totalPremium():Number{
			return _totalPremium;
		}

		public function set totalPremium(pData:Number):void{
			_totalPremium=pData;
		}

		public function get netPremium():Number{
			return _netPremium;
		}

		public function set netPremium(pData:Number):void{
			_netPremium=pData;
		}

		public function get fractioningAdditional():Number{
			return _fractioningAdditional;
		}

		public function set fractioningAdditional(pData:Number):void{
			_fractioningAdditional=pData;
		}

		public function get taxRate():Number{
			return _taxRate;
		}

		public function set taxRate(pData:Number):void{
			_taxRate = pData;
		}

		public function get taxValue():Number{
			return _taxValue;
		}

		public function set taxValue(pData:Number):void{
			_taxValue = pData;
		}

		public function get billingMethodId():int{
			return _billingMethodId;
		}

		public function set billingMethodId(pData:int):void{
			_billingMethodId=pData;
		}

		public function get paymentType():int{
			return _paymentType;
		}

		public function set paymentType(pData:int):void{
			_paymentType=pData;
		}

		public function get bankNumber():int{
			return _bankNumber;
		}

		public function set bankNumber(pData:int):void{
			_bankNumber=pData;
		}

		public function get bankAgencyNumber():String{
			return _bankAgencyNumber;
		}

		public function set bankAgencyNumber(pData:String):void{
			_bankAgencyNumber=pData;
		}

		public function get bankAccountNumber():String{
			return _bankAccountNumber;
		}

		public function set bankAccountNumber(pData:String):void{
			_bankAccountNumber=pData;
		}

		public function get bankCheckNumber():String{
			return _bankCheckNumber;
		}

		public function set bankCheckNumber(pData:String):void{
			_bankCheckNumber=pData;
		}

		public function get cardType():int{
			return _cardType;
		}

		public function set cardType(pData:int):void{
			_cardType=pData;
		}

		public function get cardBrandType():int{
			return _cardBrandType;
		}

		public function set cardBrandType(pData:int):void{
			_cardBrandType=pData;
		}

		public function get cardNumber():String{
			return _cardNumber;
		}

		public function set cardNumber(pData:String):void{
			_cardNumber=pData;
		}

		public function get cardExpirationDate():int{
			return _cardExpirationDate;
		}

		public function set cardExpirationDate(pData:int):void{
			_cardExpirationDate=pData;
		}

		public function get cardHolderName():String{
			return _cardHolderName;
		}

		public function set cardHolderName(pData:String):void{
			_cardHolderName=pData;
		}

		public function get invoiceNumber():int{
			return _invoiceNumber;
		}

		public function set invoiceNumber(pData:int):void{
			_invoiceNumber=pData;
		}

		public function get otherAccountNumber():String{
			return _otherAccountNumber;
		}

		public function set otherAccountNumber(pData:String):void{
			_otherAccountNumber=pData;
		}

		public function get debitAuthorizationCode():int{
			return _debitAuthorizationCode;
		}

		public function set debitAuthorizationCode(pData:int):void{
			_debitAuthorizationCode=pData;
		}

		public function get creditAuthorizationCode():int{
			return _creditAuthorizationCode;
		}

		public function set creditAuthorizationCode(pData:int):void{
			_creditAuthorizationCode=pData;
		}

		public function get billingMethod():BillingMethod{
			return _billingMethod;
		}

		public function set billingMethod(pData:BillingMethod):void{
			_billingMethod=pData;
		}

		
		public function get selected():Boolean{
			return _selected;
		}

		public function set selected(pData:Boolean):void{
			_selected=pData;
		}

		public function get isDefault():Boolean{
			return _isDefault;
		}

		public function set isDefault(pData:Boolean):void{
			_isDefault=pData;
		}

		public function get quantityInstallment():int{
			return _quantityInstallment;
		}

		public function set quantityInstallment(pData:int):void{
			_quantityInstallment=pData;
		}
		
		public function get nextInstallment():int{
			return _nextInstallment;
		}

		public function set nextInstallment(pData:int):void{
			_nextInstallment=pData;
		}		
		public function get dueDay():int{
			return _dueDay;
		}

		public function set dueDay(pData:int):void{
			_dueDay = pData;
		}
	}
}