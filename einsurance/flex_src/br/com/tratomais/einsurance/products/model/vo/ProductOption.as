package br.com.tratomais.einsurance.products.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
	[RemoteClass(alias="br.com.tratomais.core.model.product.ProductOption")]	
	[Bindable]
	public class ProductOption extends HibernateBean
	{
		private var _productId:int;
		private var _policyNumber:String;
		private var _masterPolicyId:int;
		private var _currencyId:int;
		private var _brokerId:int;
		private var _objectId:int;
		private var _objectType:int;
		private var _name:String;
		private var _nickName:String;
		private var _imageProperty:String;
		private var _referenceDate:Date;
		private var _effectiveDate:Date;
		private var _expiryDate:Date;
		private var _commissionFactory : Number;
		private var _beneficiaryRequired : Boolean;
		private var _profileRequired : Boolean;
		private var _riskValueType:int;
		
		/** Maximum occurrences of policies */
		private var _maxOccurrences: int;
		
        private var _transientSelected:Boolean;

		public function ProductOption()
		{
			
		}

		public function get productId():int{
			return _productId;
		}

		public function set productId(pData:int):void{
			_productId=pData;
		}

		public function get policyNumber():String{
			return _policyNumber;
		}

		public function set policyNumber(pData:String):void{
			_policyNumber=pData;
		}

		public function get masterPolicyId():int{
			return _masterPolicyId;
		}

		public function set masterPolicyId(pData:int):void{
			_masterPolicyId=pData;
		}
		
		public function get currencyId():int{
			return _currencyId;
		}

		public function set currencyId(pData:int):void{
			_currencyId=pData;
		}
		
		public function get brokerId():int{
			return _brokerId;
		}

		public function set brokerId(pData:int):void{
			_brokerId=pData;
		}

		public function get objectId():int{
			return _objectId;
		}

		public function set objectId(pData:int):void{
			_objectId=pData;
		}

		public function get objectType():int{
			return _objectType;
		}

		public function set objectType(pData:int):void{
			_objectType=pData;
		}

		public function get nickName():String{
			return _nickName;
		}

		public function set nickName(pData:String):void{
			_nickName=pData;
		}

		public function get name():String{
			return _name;
		}

		public function set name(pData:String):void{
			_name=pData;
		}

		public function get imageProperty():String{
			return _imageProperty;
		}

		public function set imageProperty(pData:String):void{
			_imageProperty=pData;
		}

		public function get referenceDate():Date{
			return _referenceDate;
		}

		public function set referenceDate(pData:Date):void{
			_referenceDate=pData;
		}

		public function get effectiveDate():Date{
			return _effectiveDate;
		}

		public function set effectiveDate(pData:Date):void{
			_effectiveDate=pData;
		}
		
		public function get expiryDate():Date{
			return _expiryDate;
		}

		public function set expiryDate(pData:Date):void{
			_expiryDate=pData;
		}
		
		
		public function get commissionFactory():Number{
			return _commissionFactory;
		}

		public function set commissionFactory(pData:Number):void{
			_commissionFactory=pData;
		}
		
		public function get transientSelected():Boolean{  
            return _transientSelected;  
        }  
  
        public function set transientSelected(pData:Boolean):void{  
            _transientSelected=pData;  
        }  

		public function get beneficiaryRequired():Boolean{
			return _beneficiaryRequired;
		}

		public function set beneficiaryRequired(pData:Boolean):void{
			_beneficiaryRequired=pData;
		}

		public function get profileRequired():Boolean{
			return _profileRequired;
		}

		public function set profileRequired(pData:Boolean):void{
			_profileRequired=pData;
		}
		
		public function set riskValueType(pData:int):void {
			this._riskValueType = pData;
		} 
		
		public function get riskValueType():int {
			return this._riskValueType;
		}
		
		/** @private */		
		public function get maxOccurrences():int {
			return _maxOccurrences;
		}

		/** @private */		
		public function set maxOccurrences(max:int):void {
			this._maxOccurrences = max;
		}
	}
}