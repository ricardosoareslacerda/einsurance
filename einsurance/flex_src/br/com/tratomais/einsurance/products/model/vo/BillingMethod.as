package br.com.tratomais.einsurance.products.model.vo
{
	import br.com.tratomais.einsurance.core.Utilities;

	import net.digitalprimates.persistence.hibernate.HibernateBean;

	[RemoteClass(alias="br.com.tratomais.core.model.product.BillingMethod")]
	[Managed]
	public class BillingMethod extends HibernateBean
	{
		/**
		 * Payment Object
		 */
		public static const PAYMENT_OBJECT_BRAZIL:int = 780;
		public static const PAYMENT_OBJECT_VENEZUELA:int = 781;

		/**
		 * Bank Number
		 */
		public static const BANK_NUMBER_BRASIL:int = 1;
		public static const BANK_NUMBER_SANTANDER:int = 33;
		public static const BANK_NUMBER_BANESE:int = 47;
		public static const BANK_NUMBER_BRADESCO:int = 237;
		public static const BANK_NUMBER_ITAU_UNIBANCO:int = 341;
		public static const BANK_NUMBER_HSBC:int = 399;
		public static const BANK_NUMBER_SAFRA:int = 422;
		public static const BANK_NUMBER_BANCOOB:int = 756;

		/**
		 * Variables
		 */
		private var _billingMethodId:int;
		private var _billingAgencyId:int;
		private var _name:String;
		private var _description:String;
		private var _paymentType:int;
		private var _paymentObject:int;
		private var _effectiveDate:Date;
		private var _expiryDate:Date;
		private var _swiftCode:String;
		private var _bankNumber:int;
		private var _bankAgencyNumberLength:int;
		private var _bankAgencyNumberDigit:Boolean;
		private var _bankAccountNumberLength:int;
		private var _bankAccountNumberDigit:Boolean;
		private var _cardType:int;
		private var _cardBrandType:int;
		private var _exchangeGroup:String;
		private var _validationClassMethod:String;
		private var _registred:Date;
		private var _updated:Date;

		public function BillingMethod(billingMethodId:int = 0, billingAgencyId:int = 0){
			_billingMethodId = billingMethodId;
			_billingAgencyId = billingAgencyId;
		}

		public function get billingMethodId():int{
			return _billingMethodId;
		}

		public function set billingMethodId(pData:int):void{
			_billingMethodId = pData;
		}

		public function get billingAgencyId():int{
			return _billingAgencyId;
		}

		public function set billingAgencyId(pData:int):void{
			_billingAgencyId = pData;
		}

		public function get name():String{
			return _name;
		}

		public function set name(pData:String):void{
			_name = pData;
		}

		public function get description():String{
			return _description;
		}

		public function set description(pData:String):void{
			_description = pData;
		}

		public function get paymentType():int{
			return _paymentType;
		}

		public function set paymentType(pData:int):void{
			_paymentType = pData;
		}

		public function get paymentObject():int{
			return _paymentObject;
		}

		public function set paymentObject(pData:int):void{
			_paymentObject = pData;
		}

		public function get effectiveDate():Date{
			return _effectiveDate;
		}

		public function set effectiveDate(pData:Date):void{
			_effectiveDate = Utilities.convertTimeZone(pData);
		}

		public function get expiryDate():Date{
			return _expiryDate;
		}

		public function set expiryDate(pData:Date):void{
			_expiryDate = Utilities.convertTimeZone(pData);
		}

		public function get swiftCode():String{
			return _swiftCode;
		}

		public function set swiftCode(pData:String):void{
			_swiftCode = pData;
		}

		public function get bankNumber():int{
			return _bankNumber;
		}

		public function set bankNumber(pData:int):void{
			_bankNumber = pData;
		}

		public function get bankAgencyNumberLength():int{
			return _bankAgencyNumberLength;
		}

		public function set bankAgencyNumberLength(pData:int):void{
			_bankAgencyNumberLength = pData;
		}

		public function get bankAgencyNumberDigit():Boolean{
			return _bankAgencyNumberDigit;
		}

		public function set bankAgencyNumberDigit(pData:Boolean):void{
			_bankAgencyNumberDigit = pData;
		}

		public function get bankAccountNumberLength():int{
			return _bankAccountNumberLength;
		}

		public function set bankAccountNumberLength(pData:int):void{
			_bankAccountNumberLength = pData;
		}

		public function get bankAccountNumberDigit():Boolean{
			return _bankAccountNumberDigit;
		}

		public function set bankAccountNumberDigit(pData:Boolean):void{
			_bankAccountNumberDigit = pData;
		}

		public function get cardType():int{
			return _cardType;
		}

		public function set cardType(pData:int):void{
			_cardType = pData;
		}

		public function get cardBrandType():int{
			return _cardBrandType;
		}

		public function set cardBrandType(pData:int):void{
			_cardBrandType = pData;
		}

		public function get exchangeGroup():String{
			return _exchangeGroup;
		}

		public function set exchangeGroup(pData:String):void{
			_exchangeGroup = pData;
		}

		public function get validationClassMethod():String{
			return _validationClassMethod;
		}

		public function set validationClassMethod(pData:String):void{
			_validationClassMethod = pData;
		}

		public function get registred():Date{
			return _registred;
		}

		public function set registred(pData:Date):void{
			_registred = Utilities.convertTimeZone(pData);
		}

		public function get updated():Date{
			return _updated;
		}

		public function set updated(pData:Date):void{
			_updated = Utilities.convertTimeZone(pData);
		}
	}
}