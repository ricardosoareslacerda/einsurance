package br.com.tratomais.einsurance.products.model.vo
{
	import br.com.tratomais.einsurance.policy.model.vo.Item;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.CoverageInsuredValue")]   
    [Managed]  
    public class CoverageInsuredValue
    {  
        private var _coveragePlanValue:Number;  
        private var _referenceDate:Date;
        private var _item:Item;  
        private var _ruleType:int;
        private var _productId:int;
        private var _coverageId:int;

		public static const RULE_TYPE_SA:int = 491;        
		
        public function CoverageInsuredValue()  
        {  
        }  
  
        public function get ruleType():int{  
            return _ruleType;  
        }  
  
        public function set ruleType(pData:int):void{  
            _ruleType=pData;  
        } 
     
        public function get productId():int{  
            return _productId;  
        }  
  
        public function set productId(pData:int):void{  
            _productId=pData;  
        } 
     
        public function get coverageId():int{  
            return _coverageId;  
        }  
  
        public function set coverageId(pData:int):void{  
            _coverageId=pData;  
        } 
             
        public function get coveragePlanValue():Number{  
            return _coveragePlanValue;  
        }  
  
        public function set coveragePlanValue(pData:Number):void{  
            _coveragePlanValue=pData;  
        } 
        
        public function get item():Item{  
            return _item;  
        }  
  
        public function set item(pData:Item):void{  
            _item=pData;  
        } 
        
        public function get referenceDate():Date{  
            return _referenceDate;  
        }  
  
        public function set referenceDate(pData:Date):void{  
            _referenceDate=pData;  
        } 
    }  
} 