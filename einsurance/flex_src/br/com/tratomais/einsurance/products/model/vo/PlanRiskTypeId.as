package br.com.tratomais.einsurance.products.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	      
    [RemoteClass(alias="br.com.tratomais.core.model.product.PlanRiskTypeId")]     
    [Managed]
    public class PlanRiskTypeId extends HibernateBean  
    {  
  
        private var _effectiveDate:Date;  
        private var _riskType:int;  
        private var _planId:int;  
        private var _productId:int;  
  
  
        public function PlanRiskTypeId()  
        {  
              
        }  
  
        public function get effectiveDate():Date{  
            return _effectiveDate;  
        }  
  
        public function set effectiveDate(pData:Date):void{  
            _effectiveDate=pData;  
        }  
  
        public function get riskType():int{  
            return _riskType;  
        }  
  
        public function set riskType(pData:int):void{  
            _riskType=pData;  
        }  
  
        public function get planId():int{  
            return _planId;  
        }  
  
        public function set planId(pData:int):void{  
            _planId=pData;  
        }  
  
        public function get productId():int{  
            return _productId;  
        }  
  
        public function set productId(pData:int):void{  
            _productId=pData;  
        }  
    }  
}  