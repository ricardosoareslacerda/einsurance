package br.com.tratomais.einsurance.products.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.Branch")]     
    [Managed]  
    public class Branch extends HibernateBean 
    {  
        private var _branchId:int;  
        private var _branch:Branch;  
        private var _branchCode:String;  
        private var _name:String;  
        private var _nickName:String;  
        private var _displayOrder:int;  
        private var _registred:Date;  
        private var _updated:Date;  
/*         private var _coveragePlans:ArrayCollection;  
        private var _branchs:ArrayCollection;  
        private var _products:ArrayCollection;  
        private var _coverages:ArrayCollection;  
 */  
        public function Branch()  
        {  
        }  
  
        public function get branchId():int{  
            return _branchId;  
        }  
  
        public function set branchId(pData:int):void{  
            _branchId=pData;  
        }  
  
        public function get branch():Branch{  
            return _branch;  
        }  
  
        public function set branch(pData:Branch):void{  
            _branch=pData;  
        }  
  
        public function get branchCode():String{  
            return _branchCode;  
        }  
  
        public function set branchCode(pData:String):void{  
            _branchCode=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get nickName():String{  
            return _nickName;  
        }  
  
        public function set nickName(pData:String):void{  
            _nickName=pData;  
        }  
  
        public function get displayOrder():int{  
            return _displayOrder;  
        }  
  
        public function set displayOrder(pData:int):void{  
            _displayOrder=pData;  
        }  
  
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
  
/*         public function get coveragePlans():ArrayCollection{  
            return _coveragePlans;  
        }  
  
        public function set coveragePlans(pData:ArrayCollection):void{  
            _coveragePlans=pData;  
        }  
  
        public function get branchs():ArrayCollection{  
            return _branchs;  
        }  
  
        public function set branchs(pData:ArrayCollection):void{  
            _branchs=pData;  
        }  
  
        public function get products():ArrayCollection{  
            return _products;  
        }  
  
        public function set products(pData:ArrayCollection):void{  
            _products=pData;  
        }  
  
        public function get coverages():ArrayCollection{  
            return _coverages;  
        }  
  
        public function set coverages(pData:ArrayCollection):void{  
            _coverages=pData;  
        }  
 */    }  
}