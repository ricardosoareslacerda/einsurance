package br.com.tratomais.einsurance.products.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.Questionnaire")]      
    [Managed]  
    public class Questionnaire extends HibernateBean 
    {  
        private var _questionnaireId:int;  
        private var _questionnaireCode:String;  
        private var _questionnaireType:int;  
        private var _name:String;  
        private var _nickName:String;  
        private var _registred:Date;  
        private var _updated:Date;  
/*         private var _questions:ArrayCollection;  
 */  
  
        public function Questionnaire()  
        {  
        }  
  
        public function get questionnaireId():int{  
            return _questionnaireId;  
        }  
  
        public function set questionnaireId(pData:int):void{  
            _questionnaireId=pData;  
        }  
  
        public function get questionnaireCode():String{  
            return _questionnaireCode;  
        }  
  
        public function set questionnaireCode(pData:String):void{  
            _questionnaireCode=pData;  
        }  
  
        public function get questionnaireType():int{  
            return _questionnaireType;  
        }  
  
        public function set questionnaireType(pData:int):void{  
            _questionnaireType=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get nickName():String{  
            return _nickName;  
        }  
  
        public function set nickName(pData:String):void{  
            _nickName=pData;  
        }  
  
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
  
/*         public function get questions():ArrayCollection{  
            return _questions;  
        }  
  
        public function set questions(pData:ArrayCollection):void{  
            _questions=pData;  
        }  
 */    }  
} 