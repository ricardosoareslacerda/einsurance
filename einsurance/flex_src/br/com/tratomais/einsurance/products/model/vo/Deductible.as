package br.com.tratomais.einsurance.products.model.vo
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
	

	
	[RemoteClass(alias="br.com.tratomais.core.model.product.Deductible")]	
	[Managed]
	public class Deductible extends HibernateBean
	{

		private var _deductibleId:int;
		private var _deductibleCode:String;
		private var _deductibleType:int;
		private var _description:String;
		private var _deductibleIncidenceType:int;
		private var _incidenceValue:Number;
		private var _minimumValue:Number;
		private var _maximumValue:Number;
		private var _premiumFactor:Number;
		private var _registred:Date;
		private var _updated:Date;
		private var _itemCoverages:ArrayCollection;
		private var _coverageDeductibles:ArrayCollection;


		public function Deductible()
		{
			
		}

		public function get deductibleId():int{
			return _deductibleId;
		}

		public function set deductibleId(pData:int):void{
			_deductibleId=pData;
		}

		public function get deductibleCode():String{
			return _deductibleCode;
		}

		public function set deductibleCode(pData:String):void{
			_deductibleCode=pData;
		}

		public function get deductibleType():int{
			return _deductibleType;
		}

		public function set deductibleType(pData:int):void{
			_deductibleType=pData;
		}

		public function get description():String{
			return _description;
		}

		public function set description(pData:String):void{
			_description=pData;
		}

		public function get deductibleIncidenceType():int{
			return _deductibleIncidenceType;
		}

		public function set deductibleIncidenceType(pData:int):void{
			_deductibleIncidenceType=pData;
		}

		public function get incidenceValue():Number{
			return _incidenceValue;
		}

		public function set incidenceValue(pData:Number):void{
			_incidenceValue=pData;
		}

		public function get minimumValue():Number{
			return _minimumValue;
		}

		public function set minimumValue(pData:Number):void{
			_minimumValue=pData;
		}

		public function get maximumValue():Number{
			return _maximumValue;
		}

		public function set maximumValue(pData:Number):void{
			_maximumValue=pData;
		}

		public function get premiumFactor():Number{
			return _premiumFactor;
		}

		public function set premiumFactor(pData:Number):void{
			_premiumFactor=pData;
		}

		public function get registred():Date{
			return _registred;
		}

		public function set registred(pData:Date):void{
			_registred=pData;
		}

		public function get updated():Date{
			return _updated;
		}

		public function set updated(pData:Date):void{
			_updated=pData;
		}		
	}
}