package br.com.tratomais.einsurance.products.model.vo
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
    [RemoteClass(alias="br.com.tratomais.core.model.product.CoverageOption")]   
    [Managed]  	
	public class CoverageOption extends HibernateBean
	{
		
		private var _contractCoverage:Boolean;
		private var _contractPlan:Boolean;
		private var _coveragePlan:CoveragePlan;
		private var _enableSelectionCoverage:Boolean;
		private var _enableSelectionPlan:Boolean;
		private var _middle:Boolean;
		private var _percentage:Number;
		private var _rangeValueId:int;	
		private	var _rangeValueList:ArrayCollection = new ArrayCollection();
		private	var _deductibleList:ArrayCollection = new ArrayCollection();
		private	var _calcStepList:ArrayCollection = new ArrayCollection();
		private var _value:Number;
		private var _valueTotal:Number;
		private var _totalPremium:Number;
		private	var _deductible:Deductible = new Deductible();
		private var _modified:Boolean;
		
		public function CoverageOption()
		{
		}
		
		
		public function get calcStepList():ArrayCollection
		{
			return this._calcStepList;
		}
		
		public function set calcStepList( pData:ArrayCollection ):void
		{
			this._calcStepList = pData;
		}
		
		public function get totalPremium():Number
		{
			return this._totalPremium;
		}
		
		public function set totalPremium( pData:Number ):void
		{
			this._totalPremium = pData;
		}	
		
		public function get deductible():Deductible
		{
			return this._deductible;
		}
		
		public function set deductible( pData:Deductible ):void
		{
			this._deductible = pData;
		}
		
		
		public function get deductibleList():ArrayCollection
		{
			return this._deductibleList;
		}
		
		public function set deductibleList( pData:ArrayCollection ):void
		{
			this._deductibleList = pData;
		}
		
		public function get contractCoverage():Boolean
		{
			return this._contractCoverage;
		}
		
		public function set contractCoverage( pData:Boolean ):void
		{
			this._contractCoverage = pData;
		}
		
		public function get contractPlan():Boolean
		{
			return this._contractPlan;
		}
		
		public function set contractPlan( pData:Boolean ):void
		{
			this._contractPlan = pData;
		}		

		public function get coveragePlan():CoveragePlan
		{
			return this._coveragePlan;
		}
		
		public function set coveragePlan( pData:CoveragePlan ):void
		{
			this._coveragePlan = pData;
		}
		
		public function get displayOrder():int
		{
			return coveragePlan.displayOrder;
		}
		
		public function get enableSelectionCoverage():Boolean
		{
			return this._enableSelectionCoverage;
		}
		
		public function set enableSelectionCoverage( pData:Boolean ):void
		{
			this._enableSelectionCoverage = pData;
		}
		
		public function get enableSelectionPlan():Boolean
		{
			return this._enableSelectionPlan;
		}
		
		public function set enableSelectionPlan( pData:Boolean ):void
		{
			this._enableSelectionPlan = pData;
		}
						
		public function get middle():Boolean
		{
			return this._middle;
		}
		
		public function set middle( pData:Boolean ):void
		{
			this._middle = pData;
		}
		
		public function get nickName():String
		{
			return ( _coveragePlan.coverage.coverageCode + ' - ' + _coveragePlan.nickName );
		}
		
		public function get nickNameFull():String
		{
			return ( _coveragePlan.coverage.coverageCode + '.' + _coveragePlan.coverage.goodsCode + ' - ' + _coveragePlan.nickName );
		}
		
		public function get percentage():Number
		{
			return this._percentage;
		}
		
		public function set percentage( pData:Number ):void
		{
			this._percentage = pData;
		}
		
		public function get rangeValueId():int
		{
			return this._rangeValueId;
		}
		
		public function set rangeValueId( pData:int ):void
		{
			this._rangeValueId = pData;
		}
		
		public function get rangeValueList():ArrayCollection
		{
			return this._rangeValueList;
		}
		
		public function set rangeValueList( pData:ArrayCollection ):void
		{
			this._rangeValueList = pData;
		}
				
		public function get value():Number
		{
			return this._value;
		}
		
		public function set value( pData:Number ):void
		{
			this._value = pData;
		}
		
		public function get valueTotal():Number
		{
			return this._valueTotal;
		}
		
		public function set valueTotal( pData:Number ):void
		{
			this._valueTotal = pData;
		}
		
		public function get modified():Boolean
		{
			return this._modified;
		}
		
		public function set modified( pData:Boolean ):void
		{
			this._modified = pData;
		}				
	}
}