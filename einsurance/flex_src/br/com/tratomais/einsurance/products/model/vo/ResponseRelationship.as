package br.com.tratomais.einsurance.products.model.vo 
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
	

	
	[RemoteClass(alias="br.com.tratomais.core.model.product.ResponseRelationship")]	
	[Managed]
	public class ResponseRelationship extends HibernateBean
	{

		private var _id:ResponseRelationshipId;
		private var _questionnaire:Questionnaire;
		private var _product:Product;
		private var _questionByQuestionIdrelationship:Question;
		private var _questionByQuestionId:Question;
		private var _response:Response;
		private var _expiryDate:Date;
		private var _displayOrder:int;
		private var _registred:Date;
		private var _updated:Date;

		public static const RELATIONSHIP_TYPE_DISABLE: int = 206;
		public static const RELATIONSHIP_TYPE_ENABLE: int = 205;

		public function ResponseRelationship()
		{
			
		}

		public function get id():ResponseRelationshipId{
			return _id;
		}

		public function set id(pData:ResponseRelationshipId):void{
			_id=pData;
		}

		public function get questionnaire():Questionnaire{
			return _questionnaire;
		}

		public function set questionnaire(pData:Questionnaire):void{
			_questionnaire=pData;
		}

		public function get product():Product{
			return _product;
		}

		public function set product(pData:Product):void{
			_product=pData;
		}

		public function get questionByQuestionIdrelationship():Question{
			return _questionByQuestionIdrelationship;
		}

		public function set questionByQuestionIdrelationship(pData:Question):void{
			_questionByQuestionIdrelationship=pData;
		}

		public function get questionByQuestionId():Question{
			return _questionByQuestionId;
		}

		public function set questionByQuestionId(pData:Question):void{
			_questionByQuestionId=pData;
		}

		public function get response():Response{
			return _response;
		}

		public function set response(pData:Response):void{
			_response=pData;
		}

		public function get expiryDate():Date{
			return _expiryDate;
		}

		public function set expiryDate(pData:Date):void{
			_expiryDate=pData;
		}

		public function get displayOrder():int{
			return _displayOrder;
		}

		public function set displayOrder(pData:int):void{
			_displayOrder=pData;
		}

		public function get registred():Date{
			return _registred;
		}

		public function set registred(pData:Date):void{
			_registred=pData;
		}

		public function get updated():Date{
			return _updated;
		}

		public function set updated(pData:Date):void{
			_updated=pData;
		}


		
	}
}