package br.com.tratomais.einsurance.products.model.vo 
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
    [RemoteClass(alias="br.com.tratomais.core.model.product.Question")]   
    [Managed]  
    public class Question extends HibernateBean 
    {  
        private var _questionId:int;  
        private var _questionnaire:Questionnaire;  
        private var _questionCode:String;  
        private var _questionType:int;  
        private var _name:String;  
        private var _displayOrder:int;  
        private var _registred:Date;  
        private var _updated:Date;  
        
		public static const QUESTION_TYPE_SINGLE:int = 198;
		public static const QUESTION_TYPE_MULTIPLE:int = 724;
        /* private var _responses:ArrayCollection; */  
  
        public function Question()  
        {  
        }  
  
        public function get questionId():int{  
            return _questionId;  
        }  
  
        public function set questionId(pData:int):void{  
            _questionId=pData;  
        }  
  
        public function get questionnaire():Questionnaire{  
            return _questionnaire;  
        }  
  
        public function set questionnaire(pData:Questionnaire):void{  
            _questionnaire=pData;  
        }  
  
        public function get questionCode():String{  
            return _questionCode;  
        }  
  
        public function set questionCode(pData:String):void{  
            _questionCode=pData;  
        }  
  
        public function get questionType():int{  
            return _questionType;  
        }  
  
        public function set questionType(pData:int):void{  
            _questionType=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get displayOrder():int{  
            return _displayOrder;  
        }  
  
        public function set displayOrder(pData:int):void{  
            _displayOrder=pData;  
        }  
  
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
  
/*         public function get responses():ArrayCollection{  
            return _responses;  
        }  
  
        public function set responses(pData:ArrayCollection):void{  
            _responses=pData;  
        }  
 */   }  
}