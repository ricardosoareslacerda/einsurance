package br.com.tratomais.einsurance.products.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.Response")]   
    [Managed]  
    public class Response extends HibernateBean 
    {  
        private var _responseId:int;  
        private var _question:Question;  
        private var _responseCode:String;  
        private var _responseType:int;  
        private var _name:String;  
        private var _contentType:int;  
        private var _contentLength:int;  
        private var _contentPrecision:String;  
        private var _contentMask:String;  
        private var _fixedValue:String;  
        private var _startValue:Number;  
        private var _endValue:Number;  
        private var _displayOrder:int;  
        private var _registred:Date;  
        private var _updated:Date;    
        
        public function Response()  
        {  
        }  
        
        public function get responseId():int{  
            return _responseId;  
        }  
  
        public function set responseId(pData:int):void{  
            _responseId=pData;  
        }  
  
        public function get question():Question{  
            return _question;  
        }  
  
        public function set question(pData:Question):void{  
            _question=pData;  
        }  
  
        public function get responseCode():String{  
            return _responseCode;  
        }  
  
        public function set responseCode(pData:String):void{  
            _responseCode=pData;  
        }  
  
        public function get responseType():int{  
            return _responseType;  
        }  
  
        public function set responseType(pData:int):void{  
            _responseType=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get contentType():int{  
            return _contentType;  
        }  
  
        public function set contentType(pData:int):void{  
            _contentType=pData;  
        }  
  
        public function get contentLength():int{  
            return _contentLength;  
        }  
  
        public function set contentLength(pData:int):void{  
            _contentLength=pData;  
        }  
  
        public function get contentPrecision():String{  
            return _contentPrecision;  
        }  
  
        public function set contentPrecision(pData:String):void{  
            _contentPrecision=pData;  
        }  
  
        public function get contentMask():String{  
            return _contentMask;  
        }  
  
        public function set contentMask(pData:String):void{  
            _contentMask=pData;  
        }  
  
        public function get fixedValue():String{  
            return _fixedValue;  
        }  
  
        public function set fixedValue(pData:String):void{  
            _fixedValue=pData;  
        }  
  
        public function get startValue():Number{  
            return _startValue;  
        }  
  
        public function set startValue(pData:Number):void{  
            _startValue=pData;  
        }  
  
        public function get endValue():Number{  
            return _endValue;  
        }  
  
        public function set endValue(pData:Number):void{  
            _endValue=pData;  
        }  
  
        public function get displayOrder():int{  
            return _displayOrder;  
        }  
  
        public function set displayOrder(pData:int):void{  
            _displayOrder=pData;  
        }  
  
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
    }  
} 