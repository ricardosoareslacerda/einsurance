package br.com.tratomais.einsurance.products.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;

    [RemoteClass(alias="br.com.tratomais.core.model.product.KinshipGroup")]    
    [Managed]  
	public class KinshipGroup extends HibernateBean
	{
		private var _kinshipGroupId:int;
		private var _name:String;
		private var _description:String;
		private var _quantityKinship:int;
		private var _kinshipExclusive:Boolean;
		private var _registred:Date;
		private var _updated:Date;

		public function KinshipGroup()
		{
		}
		
        public function get kinshipGroupId():int{  
            return _kinshipGroupId;  
        }
  
        public function set kinshipGroupId(pData:int):void{  
            _kinshipGroupId=pData;
        }
		
        public function get name():String{  
            return _name;  
        }
  
        public function set name(pData:String):void{  
            _name=pData;
        }
        
        public function get description():String{  
            return _description;  
        }
  
        public function set description(pData:String):void{  
            _description=pData;
        }

        public function get quantityKinship():int{  
            return _quantityKinship;  
        }
  
        public function set quantityKinship(pData:int):void{  
            _quantityKinship=pData;
        }

        public function get kinshipExclusive():Boolean{  
            return _kinshipExclusive;  
        }
  
        public function set kinshipExclusive(pData:Boolean):void{  
            _kinshipExclusive=pData;
        }

        public function get registred():Date{
            return _registred;  
        }
  
        public function set registred(pData:Date):void{  
            _registred=pData;
        }

        public function get updated():Date{
            return _updated;  
        }
  
        public function set updated(pData:Date):void{  
            _updated=pData;
        }
	}
}