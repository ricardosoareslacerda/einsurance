package br.com.tratomais.einsurance.products.model.vo 
{
	import br.com.tratomais.einsurance.customer.model.vo.City;
	import br.com.tratomais.einsurance.customer.model.vo.Country;
	import br.com.tratomais.einsurance.customer.model.vo.State;
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.CollectionAgency")]   
    [Managed]  
    public class CollectionAgency extends HibernateBean 
    {  
        private var _collectionAgencyId:int;  
        private var _state:State;  
        private var _country:Country;  
        private var _city:City;  
        private var _name:String;  
        private var _nickName:String;  
        private var _collectionAgencyType:int;  
        private var _externalCode:String;  
        private var _documentType:int;  
        private var _documentNumber:String;  
        private var _documentEmitter:String;  
        private var _documentValidity:Date;  
        private var _addressStreet:String;  
        private var _districtName:String;  
        private var _cityName:String;  
        private var _regionName:String;  
        private var _stateName:String;  
        private var _zipCode:String;  
        private var _countryName:String;  
        private var _emailAddress:String;  
        private var _phoneNumber:String;  
        private var _faxNumber:String;
        private var _active:Boolean;  
/*         private var _installments:ArrayCollection;  
 */  
  
        public function CollectionAgency()  
        {  
        }  
  
        public function get collectionAgencyId():int{  
            return _collectionAgencyId;  
        }  
  
        public function set collectionAgencyId(pData:int):void{  
            _collectionAgencyId=pData;  
        }  
  
        public function get state():State{  
            return _state;  
        }  
  
        public function set state(pData:State):void{  
            _state=pData;  
        }  
  
        public function get country():Country{  
            return _country;  
        }  
  
        public function set country(pData:Country):void{  
            _country=pData;  
        }  
  
        public function get city():City{  
            return _city;  
        }  
  
        public function set city(pData:City):void{  
            _city=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get nickName():String{  
            return _nickName;  
        }  
  
        public function set nickName(pData:String):void{  
            _nickName=pData;  
        }  
  
        public function get collectionAgencyType():int{  
            return _collectionAgencyType;  
        }  
  
        public function set collectionAgencyType(pData:int):void{  
            _collectionAgencyType=pData;  
        }  
  
        public function get externalCode():String{  
            return _externalCode;  
        }  
  
        public function set externalCode(pData:String):void{  
            _externalCode=pData;  
        }  
  
        public function get documentType():int{  
            return _documentType;  
        }  
  
        public function set documentType(pData:int):void{  
            _documentType=pData;  
        }  
  
        public function get documentNumber():String{  
            return _documentNumber;  
        }  
  
        public function set documentNumber(pData:String):void{  
            _documentNumber=pData;  
        }  
  
        public function get documentEmitter():String{  
            return _documentEmitter;  
        }  
  
        public function set documentEmitter(pData:String):void{  
            _documentEmitter=pData;  
        }  
  
        public function get documentValidity():Date{  
            return _documentValidity;  
        }  
  
        public function set documentValidity(pData:Date):void{  
            _documentValidity=pData;  
        }  
  
        public function get addressStreet():String{  
            return _addressStreet;  
        }  
  
        public function set addressStreet(pData:String):void{  
            _addressStreet=pData;  
        }  
  
        public function get districtName():String{  
            return _districtName;  
        }  
  
        public function set districtName(pData:String):void{  
            _districtName=pData;  
        }  
  
        public function get cityName():String{  
            return _cityName;  
        }  
  
        public function set cityName(pData:String):void{  
            _cityName=pData;  
        }  
  
        public function get regionName():String{  
            return _regionName;  
        }  
  
        public function set regionName(pData:String):void{  
            _regionName=pData;  
        }  
  
        public function get stateName():String{  
            return _stateName;  
        }  
  
        public function set stateName(pData:String):void{  
            _stateName=pData;  
        }  
  
        public function get zipCode():String{  
            return _zipCode;  
        }  
  
        public function set zipCode(pData:String):void{  
            _zipCode=pData;  
        }  
  
        public function get countryName():String{  
            return _countryName;  
        }  
  
        public function set countryName(pData:String):void{  
            _countryName=pData;  
        }  
  
        public function get emailAddress():String{  
            return _emailAddress;  
        }  
  
        public function set emailAddress(pData:String):void{  
            _emailAddress=pData;  
        }  
  
        public function get phoneNumber():String{  
            return _phoneNumber;  
        }  
  
        public function set phoneNumber(pData:String):void{  
            _phoneNumber=pData;  
        }  
  
        public function get faxNumber():String{  
            return _faxNumber;  
        }  
  
        public function set faxNumber(pData:String):void{  
            _faxNumber=pData;  
        }  
  
        public function get active(): Boolean { 
        	return _active;
        }
        
        public function set active(pActive: Boolean): void {
        	this._active = pActive;
        }

/*         public function get installments():ArrayCollection{  
            return _installments;  
        }  
  
        public function set installments(pData:ArrayCollection):void{  
            _installments=pData;  
        }  
 */    }  
}