package br.com.tratomais.einsurance.products.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.ClauseId")]     
    [Managed]  
    public class Clause extends HibernateBean 
    {  
        private var _clauseId:int;  
  
        public function ClauseId()  
        {  
        }  
  
        public function get clauseId():int{  
            return _clauseId;  
        }  
  
        public function set clauseId(pData:int):void{  
            _clauseId=pData;  
        }  
    
} 