package br.com.tratomais.einsurance.products.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.ProductVersion")]     
    [Managed]  
    public class ProductVersion extends HibernateBean 
    {  
        private var _id:ProductVersionId;
        private var _product:Product;  
        private var _expiryDate:Date;  
        private var _versionName:String;  
        private var _referenceDate:Date;  
        private var _validityQuotation:int;  
        private var _validityProposal:int;  
        private var _minimumInsuredValue:int;  
        private var _maximumInsuredValue:int;  
        private var _minimumPremium:Number;  
        private var _minimumRefund:Number;  
        private var _minimumInstallment:Number;  
        private var _defaultCommissionFactor:Number;  
        private var _minimumCommissionFactor:Number;  
        private var _maximumCommissionFactor:Number;  
		private var _maximumPolicy:int;
        private var _registred:Date;  
        private var _updated:Date;  
  
        public function ProductVersion()  
        {  
        }  
  
        public function get id():ProductVersionId{  
            return _id;  
        }  
  
        public function set id(pData:ProductVersionId):void{  
            _id=pData;  
        }  
  
        public function get product():Product{  
            return _product;  
        }  
  
        public function set product(pData:Product):void{  
            _product=pData;  
        }  
  
        public function get expiryDate():Date{  
            return _expiryDate;  
        }  
  
        public function set expiryDate(pData:Date):void{  
            _expiryDate=pData;  
        }  
  
        public function get versionName():String{  
            return _versionName;  
        }  
  
        public function set versionName(pData:String):void{  
            _versionName=pData;  
        }
  
        public function get referenceDate():Date{  
            return _referenceDate;  
        }  
  
        public function set referenceDate(pData:Date):void{  
            _referenceDate=pData;  
        }  
  
        public function get validityQuotation():int{  
            return _validityQuotation;  
        }  
  
        public function set validityQuotation(pData:int):void{  
            _validityQuotation=pData;  
        }  
  
        public function get validityProposal():int{  
            return _validityProposal;  
        }  
  
        public function set validityProposal(pData:int):void{  
            _validityProposal=pData;  
        }  
  
        public function get minimumInsuredValue():int{  
            return _minimumInsuredValue;  
        }  
  
        public function set minimumInsuredValue(pData:int):void{  
            _minimumInsuredValue=pData;  
        }  
  
        public function get maximumInsuredValue():int{  
            return _maximumInsuredValue;  
        }  
  
        public function set maximumInsuredValue(pData:int):void{  
            _maximumInsuredValue=pData;  
        }  
  
        public function get minimumPremium():Number{  
            return _minimumPremium;  
        }  
  
        public function set minimumPremium(pData:Number):void{  
            _minimumPremium=pData;  
        }  
  
        public function get minimumRefund():Number{  
            return _minimumRefund;  
        }  
  
        public function set minimumRefund(pData:Number):void{  
            _minimumRefund=pData;  
        }  
  
        public function get minimumInstallment():Number{  
            return _minimumInstallment;  
        }  
  
        public function set minimumInstallment(pData:Number):void{  
            _minimumInstallment=pData;  
        }  
  
        public function get defaultCommissionFactor():Number{  
            return _defaultCommissionFactor;  
        }  
  
        public function set defaultCommissionFactor(pData:Number):void{  
            _defaultCommissionFactor=pData;  
        }  
  
        public function get minimumCommissionFactor():Number{  
            return _minimumCommissionFactor;  
        }  
  
        public function set minimumCommissionFactor(pData:Number):void{  
            _minimumCommissionFactor=pData;  
        }  
  
        public function get maximumCommissionFactor():Number{  
            return _maximumCommissionFactor;  
        }  
  
        public function set maximumCommissionFactor(pData:Number):void{  
            _maximumCommissionFactor=pData;  
        }  
  
		public function get maximumPolicy():int {
			return _maximumPolicy;
		}
	
		public function set maximumPolicy(pData:int) {
			_maximumPolicy = pData;
		}

        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
    }  
} 