package br.com.tratomais.einsurance.products.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.Clause")]     
    [Managed]  
    public class Clause extends HibernateBean 
    {  
        private var _id:ClauseId;
        private var _clauseType:int;
        private var _clauseCode:string;
        private var _objectId:int;
        private var _name:string;
        private var _description:string;
        private var _registred:date;
        private var _updated:date;  
  
        public function Clause()  
        {  
        }  
  
  		public function set id(pData:ClauseId):void{  
            _id=pData;  
        }
        public function get id():ClauseId{  
            return _id;  
        }
        
        public function set clauseType(pData:int):void{  
            _clauseType=pData;
        }
        public function get clauseType():int{  
            return _clauseType;
        }
        
        public function set clauseCode(pData:string):void{  
            _clauseCode=pData;
        }
        public function get clauseCode():string{  
            return _clauseCode;
        }
        
        public function set objectId(pData:int):void{  
            _objectId=pData;
        }
        public function get objectId():int{  
            return _objectId;
        }
        
        public function set name(pData:string):void{  
            _name=pData;
        }
        public function get name():string{  
            return _name;
        }
        
        public function set description(pData:string):void{  
            _description=pData;
        }
        public function get description():string{  
            return _description;
        }
        
        public function set registred(pData:date):void{  
            _registred=pData;
        }
        public function get registred():date{  
            return _registred;
        }
        
        public function set updated(pData:date):void{  
            _updated=pData;
        }
        public function get updated():date{  
            return _updated;
        }
  
} 