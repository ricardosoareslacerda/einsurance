package br.com.tratomais.einsurance.products.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
   	
   	[RemoteClass(alias="br.com.tratomais.core.model.product.PlanPersonalRiskId")]     
    [Managed]
	public class PlanPersonalRiskId extends HibernateBean
	{
		
		private var _effectiveDate:Date;
		private var _personType:int;
		private var _planId:int;
		private var _productId:int;
			
		public function PlanPersonalRiskId(){
		}

        public function get effectiveDate():Date{  
            return _effectiveDate;
        }  
  
        public function set effectiveDate(pData:Date):void{  
            _effectiveDate=pData;
        }

        public function get personType():int{  
            return _personType;
        }  
  
        public function set personType(pData:int):void{  
            _personType=pData;
        }

        public function get planId():int{  
            return _planId;
        }  
  
        public function set planId(pData:int):void{  
            _planId=pData;
        }
        
        public function get productId():int{  
            return _productId;
        }  
  
        public function set productId(pData:int):void{  
            _productId=pData;
        }
	}
}