package br.com.tratomais.einsurance.products.model.vo
{
	import br.com.tratomais.einsurance.products.model.vo.CoveragePlan;
	
	import mx.collections.ArrayCollection;
	
	

	
	[RemoteClass(alias="br.com.tratomais.core.model.product.CoverageDataGridOption")]	
	[Bindable]
	public class CoverageDataGridOption
	{

		private var _coverageCode:String;
		private var _nickName:String;
		private var _displayOrder:int;
		private var _compulsory: Boolean;
		private var _selectedCoverage: Boolean;

		private var _riskType1:int;
		private var _riskType2:int;
		private var _riskType3:int;

		private var _insuredValueType1:int;
		private var _insuredValueType2:int;
		private var _insuredValueType3:int;

		private var _insuredValue1:Number;
		private var _insuredValue2:Number;
		private var _insuredValue3:Number;

		private var _rangeValueList1:ArrayCollection;
		private var _rangeValueList2:ArrayCollection;
		private var _rangeValueList3:ArrayCollection;

		private var _rangeValueId1:int;
		private var _rangeValueId2:int;
		private var _rangeValueId3:int;

		private var _coveragePlan1:CoveragePlan;
		private var _coveragePlan2:CoveragePlan;
		private var _coveragePlan3:CoveragePlan;

		private var _basicCoverageIsValue1:Number;
		private var _basicCoverageIsValue2:Number;
		private var _basicCoverageIsValue3:Number;

		public function CoverageDataGridOption()
		{
			
		}

		public function get coverageCode():String{
			return _coverageCode;
		}

		public function set coverageCode(pData:String):void{
			_coverageCode=pData;
		}

		public function get nickName():String{
			return _nickName;
		}

		public function set nickName(pData:String):void{
			_nickName=pData;
		}

		public function get displayOrder():int{
			return _displayOrder;
		}

		public function set displayOrder(pData:int):void{
			_displayOrder=pData;
		}

		public function get riskType1():int{
			return _riskType1;
		}

		public function set riskType1(pData:int):void{
			_riskType1=pData;
		}

		public function get riskType2():int{
			return _riskType2;
		}

		public function set riskType2(pData:int):void{
			_riskType2=pData;
		}

		public function get riskType3():int{
			return _riskType3;
		}

		public function set riskType3(pData:int):void{
			_riskType3=pData;
		}

		public function get insuredValueType1():int{
			return _insuredValueType1;
		}

		public function set insuredValueType1(pData:int):void{
			_insuredValueType1=pData;
		}

		public function get insuredValueType2():int{
			return _insuredValueType2;
		}

		public function set insuredValueType2(pData:int):void{
			_insuredValueType2=pData;
		}

		public function get insuredValueType3():int{
			return _insuredValueType3;
		}

		public function set insuredValueType3(pData:int):void{
			_insuredValueType3=pData;
		}

		public function get insuredValue1():Number{
			return _insuredValue1;
		}

		public function set insuredValue1(pData:Number):void{
			_insuredValue1=pData;
		}

		public function get insuredValue2():Number{
			return _insuredValue2;
		}

		public function set insuredValue2(pData:Number):void{
			_insuredValue2=pData;
		}

		public function get insuredValue3():Number{
			return _insuredValue3;
		}

		public function set insuredValue3(pData:Number):void{
			_insuredValue3=pData;
		}

		public function get rangeValueList1():ArrayCollection{
			return _rangeValueList1;
		}

		public function set rangeValueList1(pData:ArrayCollection):void{
			_rangeValueList1=pData;
		}

		public function get rangeValueList2():ArrayCollection{
			return _rangeValueList2;
		}

		public function set rangeValueList2(pData:ArrayCollection):void{
			_rangeValueList2=pData;
		}

		public function get rangeValueList3():ArrayCollection{
			return _rangeValueList3;
		}

		public function set rangeValueList3(pData:ArrayCollection):void{
			_rangeValueList3=pData;
		}

		public function get rangeValueId1():int{
			return _rangeValueId1;
		}

		public function set rangeValueId1(pData:int):void{
			_rangeValueId1=pData;
		}

		public function get rangeValueId2():int{
			return _rangeValueId2;
		}

		public function set rangeValueId2(pData:int):void{
			_rangeValueId2=pData;
		}

		public function get rangeValueId3():int{
			return _rangeValueId3;
		}

		public function set rangeValueId3(pData:int):void{
			_rangeValueId3=pData;
		}

		public function get coveragePlan1():CoveragePlan{
			return _coveragePlan1;
		}

		public function set coveragePlan1(pData:CoveragePlan):void{
			_coveragePlan1=pData;
		}

		public function get coveragePlan2():CoveragePlan{
			return _coveragePlan2;
		}

		public function set coveragePlan2(pData:CoveragePlan):void{
			_coveragePlan2=pData;
		}

		public function get coveragePlan3():CoveragePlan{
			return _coveragePlan3;
		}

		public function set coveragePlan3(pData:CoveragePlan):void{
			_coveragePlan3=pData;
		}

		public function get compulsory():Boolean{  
            return _compulsory;  
        }  
  
        public function set compulsory(pData:Boolean):void{  
            _compulsory=pData;  
        }  
        
        public function get selectedCoverage():Boolean{  
            return _selectedCoverage;  
        }  
  
        public function set selectedCoverage(pData:Boolean):void{  
            _selectedCoverage=pData;  
        } 

		public function get basicCoverageIsValue1():Number{
			return _basicCoverageIsValue1;
		}

		public function set basicCoverageIsValue1(pData:Number):void{
			_basicCoverageIsValue1=pData;
		}

		public function get basicCoverageIsValue2():Number{
			return _basicCoverageIsValue2;
		}

		public function set basicCoverageIsValue2(pData:Number):void{
			_basicCoverageIsValue2=pData;
		}

		public function get basicCoverageIsValue3():Number{
			return _basicCoverageIsValue3;
		}

		public function set basicCoverageIsValue3(pData:Number):void{
			_basicCoverageIsValue3=pData;
		}


		
	}
}