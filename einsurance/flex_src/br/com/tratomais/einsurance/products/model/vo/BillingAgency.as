package br.com.tratomais.einsurance.products.model.vo    
{
	import mx.collections.ArrayCollection;
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.BillingAgency")]      
    [Managed]  
    public class BillingAgency extends HibernateBean 
    {  
        private var _billingAgencyId:int;  
        private var _name:String;  
        private var _nickName:String;  
        private var _billingAgencyType:int;  
        private var _externalCode:String;  
        private var _documentType:int;  
        private var _documentNumber:String;  
        private var _documentIssuer:String;  
        private var _documentValidity:Date;  
        private var _addressStreet:String;  
        private var _districtName:String;  
        private var _cityId:int;  
        private var _cityName:String;  
        private var _regionName:String;  
        private var _stateId:int;  
        private var _stateName:String;  
        private var _zipCode:String;  
        private var _countryId:int;  
        private var _countryName:String;  
        private var _emailAddress:String;  
        private var _phoneNumber:String;  
        private var _faxNumber:String;  
        private var _active:Boolean;
        private var _billingMethods:ArrayCollection;  
  
        public function BillingAgency()  
        {  
        }  
  
        public function get billingAgencyId():int{  
            return _billingAgencyId;  
        }  
  
        public function set billingAgencyId(pData:int):void{  
            _billingAgencyId=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get nickName():String{  
            return _nickName;  
        }  
  
        public function set nickName(pData:String):void{  
            _nickName=pData;  
        }  
  
        public function get billingAgencyType():int{  
            return _billingAgencyType;  
        }  
  
        public function set billingAgencyType(pData:int):void{  
            _billingAgencyType=pData;  
        }  
  
        public function get externalCode():String{  
            return _externalCode;  
        }  
  
        public function set externalCode(pData:String):void{  
            _externalCode=pData;  
        }  
  
        public function get documentType():int{  
            return _documentType;  
        }  
  
        public function set documentType(pData:int):void{  
            _documentType=pData;  
        }  
  
        public function get documentNumber():String{  
            return _documentNumber;  
        }  
  
        public function set documentNumber(pData:String):void{  
            _documentNumber=pData;  
        }  
  
        public function get documentIssuer():String{  
            return _documentIssuer;  
        }  
  
        public function set documentIssuer(pData:String):void{  
            _documentIssuer=pData;  
        }  
  
        public function get documentValidity():Date{  
            return _documentValidity;  
        }  
  
        public function set documentValidity(pData:Date):void{  
            _documentValidity=pData;  
        }  
  
        public function get addressStreet():String{  
            return _addressStreet;  
        }  
  
        public function set addressStreet(pData:String):void{  
            _addressStreet=pData;  
        }  
  
        public function get districtName():String{  
            return _districtName;  
        }  
  
        public function set districtName(pData:String):void{  
            _districtName=pData;  
        }  
  
        public function get cityId():int{  
            return _cityId;  
        }  
  
        public function set cityId(pData:int):void{  
            _cityId=pData;  
        }  
  
        public function get cityName():String{  
            return _cityName;  
        }  
  
        public function set cityName(pData:String):void{  
            _cityName=pData;  
        }  
  
        public function get regionName():String{  
            return _regionName;  
        }  
  
        public function set regionName(pData:String):void{  
            _regionName=pData;  
        }  
  
        public function get stateId():int{  
            return _stateId;  
        }  
  
        public function set stateId(pData:int):void{  
            _stateId=pData;  
        }  
  
        public function get stateName():String{  
            return _stateName;  
        }  
  
        public function set stateName(pData:String):void{  
            _stateName=pData;  
        }  
  
        public function get zipCode():String{  
            return _zipCode;  
        }  
  
        public function set zipCode(pData:String):void{  
            _zipCode=pData;  
        }  
  
        public function get countryId():int{  
            return _countryId;  
        }  
  
        public function set countryId(pData:int):void{  
            _countryId=pData;  
        }  
  
        public function get countryName():String{  
            return _countryName;  
        }  
  
        public function set countryName(pData:String):void{  
            _countryName=pData;  
        }  
  
        public function get emailAddress():String{  
            return _emailAddress;  
        }  
  
        public function set emailAddress(pData:String):void{  
            _emailAddress=pData;  
        }  
  
        public function get phoneNumber():String{  
            return _phoneNumber;  
        }  
  
        public function set phoneNumber(pData:String):void{  
            _phoneNumber=pData;  
        }  
  
        public function get faxNumber():String{  
            return _faxNumber;  
        }  
  
        public function set faxNumber(pData:String):void{  
            _faxNumber=pData;  
        }  
  
        public function get billingMethods():ArrayCollection{  
            return _billingMethods;  
        }  
  
        public function set billingMethods(pData:ArrayCollection):void{  
            _billingMethods=pData;  
        }  
    }  
}