package br.com.tratomais.einsurance.products.model.vo
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.IdentifiedList")]   
    [Managed]  
	public class IdentifiedList extends HibernateBean
	{
		private var _groupId:String;
		private var _resultArray:ArrayCollection;
		private var _nameComponent:String;
		private var _value:Object;
		private var _response:String;

		public function IdentifiedList(nameComponent:String = null, value:Object = null, groupId:String = null, response:String = null, resultArray:ArrayCollection = null){
			_nameComponent = nameComponent;
			_value = value;
			_groupId = groupId;
			_response = response;
			_resultArray = resultArray;
		}
		
		public function set groupId(pData:String):void{
			_groupId = pData;
		}
		
		public function get groupId():String{
			return _groupId;
		}
		
		public function set resultArray(pData:ArrayCollection):void{
			_resultArray = pData;
		}
		
		public function get resultArray():ArrayCollection{
			return _resultArray;
		}
		
		public function set nameComponent(pData:String):void{
			_nameComponent = pData;
		}
		
		public function get nameComponent():String{
			return _nameComponent;
		}
		
		public function set value(pData:Object):void{
			_value = pData;
		}
		
		public function get value():Object{
			return _value;
		}

		public function set response(pData:String):void{
			_response = pData;
		}

		public function get response():String{
			return _response;
		}
	}
}