package br.com.tratomais.einsurance.products.model.proxy
{
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.core.model.vo.ApplicationProperty;
    import br.com.tratomais.einsurance.policy.model.vo.AutoVersion;
    import br.com.tratomais.einsurance.policy.model.vo.Item;
    import br.com.tratomais.einsurance.products.business.ProductDelegateProxy;
    import br.com.tratomais.einsurance.products.model.vo.CoverageInsuredValue;
    import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
    import br.com.tratomais.einsurance.products.model.vo.PlanKinship;
    import br.com.tratomais.einsurance.products.model.vo.Product;
    import br.com.tratomais.einsurance.products.model.vo.ProductOption;
    import br.com.tratomais.einsurance.products.model.vo.ProductPlan;
    import br.com.tratomais.einsurance.products.model.vo.ProductVersion;
    import br.com.tratomais.einsurance.sales.model.vo.ProposalListData;
    
    import mx.collections.ArrayCollection;
    import mx.rpc.Responder;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    
    import org.puremvc.as3.interfaces.IProxy;
    import org.puremvc.as3.patterns.proxy.Proxy;  

    [Bindable]  
    public class ProductProxy extends Proxy implements IProxy   
    {

        public static const NAME:String = "ProductProxy"; 

        public function ProductProxy( data:Object = null)  
        {  
            super(NAME, data );  
        }

		/*
		 * #####  APPLICATION  ####
		 */
		public function findApplicationProperty(identified:IdentifiedList, applicationId:int, propertyName:String):void{
			if (identified.groupId == null) identified.groupId = propertyName;
			var onResult:Function = function(resultEvent:ResultEvent):void{ onFindApplicationPropertyResult(identified, resultEvent) };
			var delegate:ProductDelegateProxy = new ProductDelegateProxy(new Responder(onResult, onFault));
			delegate.findApplicationProperty(applicationId, propertyName);
		}

		private function onFindApplicationPropertyResult(identified:IdentifiedList, resultEvent:ResultEvent):void{
			identified.value = resultEvent.result;
			sendNotification(NotificationList.APPLICATION_PROPERTY_FINDED, identified);
		}
 		public function findProductsbyPartner(partnerId : int, insurerId : int, refDate : Date = null):void
		{
			var delegate:ProductDelegateProxy = new ProductDelegateProxy(new Responder(onFindProductsByPartnerResult, onFault));  
			delegate.findProductsbyPartner(partnerId, insurerId, refDate);
		}

		private function onFindProductsByPartnerResult(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.PRODUCT_LIST_REFRESH, collection);
        }

		public function findPlanByProductId(productId : int, refDate : Date = null) : void {
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onFindPlanByProductId, onFault));
       		delegate.findPlanByProductId(productId, refDate);
        }

		public function listCoveragePlanByProductId(productId:int, riskPlanId:int, refDate:Date = null) : void {
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onFindPlanByProductId, onFault));
       		delegate.listCoveragePlanByProductId(productId, riskPlanId, refDate);
        }

        private function onFindPlanByProductId(pResultEvt:ResultEvent) : void{
        	var result : ArrayCollection = pResultEvt.result as ArrayCollection;
        	sendNotification(NotificationList.PRODUCT_LIST_PLAN_REFRESH, result);
        }

		public function findRiskPlanByProductId(productId : int, refDate : Date = null) : void {
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onFindRiskPlanByProductId, onFault));
       		delegate.findRiskPlanByProductId(productId, refDate);
        }

        private function onFindRiskPlanByProductId(pResultEvt:ResultEvent) : void{
        	var result : ArrayCollection = pResultEvt.result as ArrayCollection;
        	sendNotification(NotificationList.PRODUCT_LIST_RISK_PLAN_REFRESH, result);
        }

		public function listPersonalRiskPlanByProductId(productId : int, refDate : Date = null) : void {
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onListPersonalRiskPlanByProductId, onFault));
       		delegate.listPersonalRiskPlanByProductId(productId, refDate);
        }

        private function onListPersonalRiskPlanByProductId(pResultEvt:ResultEvent) : void{
        	var result : ArrayCollection = pResultEvt.result as ArrayCollection;
        	sendNotification(NotificationList.PRODUCT_LIST_RISK_PLAN_REFRESH, result);
        }

        public function listCoveragePlanByRefDate(productId : int, coveragePlanId : int, refDate : Date) : void{
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onListCoverageOptionByRefDate, onFault));
       		delegate.listCoveragePlanByRefDate(productId, coveragePlanId, refDate);
        }

        public function listCoverageOptionByRefDate(productId : int, coveragePlanId : int, refDate : Date) : void{
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onListCoverageOptionByRefDate, onFault));
       		delegate.listCoverageOptionByRefDate(productId, coveragePlanId, refDate);
        }

        public function getCoverageInsuredValue(item : Item, ruleType : int, productId : int, coverageId : int, referenceDate : Date) : void{
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onApplyCoverageRule, onFault));
       		delegate.getCoverageInsuredValue(item, ruleType, productId, coverageId, referenceDate);
        }

        private function onApplyCoverageRule(pResultEvt:ResultEvent) : void{
        	var result : CoverageInsuredValue = pResultEvt.result as CoverageInsuredValue;
        	sendNotification(NotificationList.PRODUCT_APPLY_COVERAGE_INSURED_VALUE, result);
        }

        private function onListCoverageOptionByRefDate(pResultEvt:ResultEvent) : void{
        	var result : ArrayCollection = pResultEvt.result as ArrayCollection;
        	sendNotification(NotificationList.PRODUCT_LIST_COVERAGE_REFRESH, result);
        }

        public function loadProposalList():void{
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onLoadProposalList, onFault));
       		delegate.loadProposalList();
        }

        private function onLoadProposalList(pResultEvt:ResultEvent):void{
        	var result : ProposalListData = pResultEvt.result as ProposalListData;
        	sendNotification(NotificationList.LOAD_PROPOSAL_LIST, result);        	
        }

		public function listAllProduct():void
		{
			var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onListAllResult, onFault));
			delegate.listAllProduct();
		}

		private function onListAllResult(pResultEvt:ResultEvent):void
		{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.PRODUCT_LISTED, result);
		}

		public function findProductToBeCopy(productId:int):void
		{
			var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onFindProductToBeCopy, onFault));
			delegate.findProductToBeCopy(productId);
		}

		private function onFindProductToBeCopy(pResultEvt:ResultEvent):void{
			var result:Product=pResultEvt.result as Product;
         	sendNotification(NotificationList.PRODUCT_TO_BE_COPY_FOUND, result);
		}

		public function getProductVersionByProduct(productId : int):void {
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onGetProductVersionByProduct, onFault));
       		delegate.getProductVersionByProduct(productId);
        }

        private function onGetProductVersionByProduct(pResultEvt:ResultEvent) : void{
        	var result : ArrayCollection = pResultEvt.result as ArrayCollection;
        	sendNotification(NotificationList.PRODUCT_VERSION_LISTED, result);
        }

		public function findProductsPerPartner(partnerId:int):void {
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onGetFindProductsPerPartner, onFault));
       		delegate.findProductsPerPartner(partnerId);
        }

        private function onGetFindProductsPerPartner(pResultEvt:ResultEvent) : void{
        	var result : ArrayCollection = pResultEvt.result as ArrayCollection;
        	sendNotification(NotificationList.PRODUCT_LIST_PER_PARTNER, result);
        }

        public function getRiskPlanById(productId:int, id : int, referDate:Date): void
		{
			var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onGetRiskPlanById, onFault));
       		delegate.getRiskPlanById(productId, id, referDate);
		}

		private function onGetRiskPlanById (pResultEvent : ResultEvent) :void
		{
			var result : ProductPlan = pResultEvent.result as ProductPlan;
			sendNotification(NotificationList.PLAN_LOADED, result);
		}

        public function saveProductVersionCopy(inputProductVersion:ProductVersion):void{
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onSaveProductVersionCopy, onFault));
       		delegate.saveProductVersionCopy(inputProductVersion);
        }

        private function onSaveProductVersionCopy(pResultEvt:ResultEvent) : void{
        	var result : ProductVersion = pResultEvt.result as ProductVersion;
        	sendNotification(NotificationList.PRODUCT_COPY_SAVED, result);        	
        }

 		public function listPlanKinshipByRiskPlan(productId:int, riskPlanId:int, renewalType:int, refDate:Date, func:Function) : void {
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(func, onFault));
       		delegate.listPlanKinshipByRiskPlan(productId, riskPlanId, renewalType, refDate);
        }

        public function onListPlanKinshipByRiskPlanProposal(pResultEvt:ResultEvent) : void{
        	var result : ArrayCollection = pResultEvt.result as ArrayCollection;
        	sendNotification(NotificationList.PRODUCT_LIST_PLAN_KINSHIP_REFRESH_PROPOSAL, result);
        }

        public function onListPlanKinshipByRiskPlanQuote(pResultEvt:ResultEvent) : void{
        	var result : ArrayCollection = pResultEvt.result as ArrayCollection;
        	sendNotification(NotificationList.PRODUCT_LIST_PLAN_KINSHIP_REFRESH_QUOTE, result);
        }

        /**
         * Retrieves a Product Version
         * @param productId Product Identification
         * @data data Date of reference 
         */ 
        public function getProductVersionByRefDate(productId: int, data: Date = null): void {
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onGetProductVersion, onFault));
       		delegate.getProductVersionByRefDate(productId, data);
        }

        private function onGetProductVersion(pResultEvt:ResultEvent) : void{
			var result : ProductVersion = pResultEvt.result as ProductVersion;
        	sendNotification(NotificationList.PRODUCT_VERSION_PER_DATE_LISTED, result);        	
        }

		public function listCoverageRangeValueByRefDate(productId:int, coveragePlanId:int, coverageId:int, refDate:Date):void{
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onListCoverageRangeValueByRefDate, onFault));
       		delegate.listCoverageRangeValueByRefDate(productId, coveragePlanId, coverageId, refDate);			
		}

        private function onListCoverageRangeValueByRefDate(pResultEvt:ResultEvent) : void{
			var result : ArrayCollection = pResultEvt.result as ArrayCollection;
        	sendNotification(NotificationList.COVERAGE_RANGE_VALUE_LISTED, result);        	
        }

		public function listPlanRiskTypeDescriptionByRefDate(productId:int, planId:int, refDate:Date):void{
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onListPlanRiskTypeDescriptionByRefDate, onFault));
       		delegate.listPlanRiskTypeDescriptionByRefDate(productId, planId, refDate);			
		}

        private function onListPlanRiskTypeDescriptionByRefDate(pResultEvt:ResultEvent) : void{
			var result : ArrayCollection = pResultEvt.result as ArrayCollection;
        	sendNotification(NotificationList.RESTRICTION_RISK_LISTED, result);        	
        }

		public function listPlanKinshipByPersonalRiskType(productId:int, planId:int, personalRiskType:int, renewalType:int, refDate:Date):void{
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onListPlanKinshipByPersonalRiskType, onFault));
       		delegate.listPlanKinshipByPersonalRiskType(productId, planId, personalRiskType, renewalType, refDate);			
		}

        private function onListPlanKinshipByPersonalRiskType(pResultEvt:ResultEvent) : void{
			var result : PlanKinship = pResultEvt.result as PlanKinship;
        	sendNotification(NotificationList.PRODUCT_LIST_PLAN_KINSHIP_RISK_TYPE, result);        	
        }

        public function listAutoBrands():void{
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onlistAutoBrands, onFault));
       		delegate.listAutoBrands();			
		}

        private function onlistAutoBrands(pResultEvt:ResultEvent) : void{
			var result : ArrayCollection = pResultEvt.result as ArrayCollection;
        	sendNotification(NotificationList.PRODUCT_LIST_AUTO_BRAND, result);        	
        }

        public function listAutoModelForBrand( autoBrandId:int ):void{
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onListAutoModelForBrand, onFault));
       		delegate.listAutoModelForBrand( autoBrandId );			
		}

        private function onListAutoModelForBrand(pResultEvt:ResultEvent) : void{
			var result : ArrayCollection = pResultEvt.result as ArrayCollection;
        	sendNotification(NotificationList.PRODUCT_LIST_AUTO_MODEL, result);        	
        }

        public function listAutoYear(autoModelId:int, referenceDate:Date ):void{
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onListAutoYear, onFault));
       		delegate.listAutoYear( autoModelId, referenceDate );			
		}

        public function findAutoVersion( autoModelId: int, effectiveDate:Date ):void{
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onFindAutoVersion, onFault));
       		delegate.findAutoVersion( autoModelId, effectiveDate );			
		}

        private function onFindAutoVersion(pResultEvt:ResultEvent) : void{
			var result : AutoVersion = pResultEvt.result as AutoVersion;
        	sendNotification(NotificationList.PRODUCT_FIND_AUTO_VERSION, result);        	
        }

        private function onListAutoYear(pResultEvt:ResultEvent) : void{
			var result : ArrayCollection = pResultEvt.result as ArrayCollection;
        	sendNotification(NotificationList.PRODUCT_LIST_AUTO_YEAR, result);        	
        }

        public function getAutoCost( autoModelId:int, year:int, referenceDate:Date ):void{
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onGetAutoCost, onFault));
       		delegate.getAutoCost( autoModelId, year, referenceDate );
		}

        private function onGetAutoCost(pResultEvt:ResultEvent) : void{
			var result:Number = new Number( pResultEvt.result );
        	sendNotification(NotificationList.PRODUCT_LIST_AUTO_COST, result);        	
        }

        public function loadItemAdditional():void{
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onLoadItemAdditional, onFault));
       		delegate.loadProposalList();
        }

        private function onLoadItemAdditional(pResultEvt:ResultEvent):void{
        	var result : ProposalListData = pResultEvt.result as ProposalListData;
        	sendNotification(NotificationList.LOAD_ITEM_ADDITIONAL, result);        	
        }

        public function getProductVersionByBetweenRefDate( productId:int, referenceDate:Date ):void
        {
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onGetProductVersionByBetweenRefDate, onFault));
       		delegate.getProductVersionByBetweenRefDate( productId, referenceDate );
       	}

        private function onGetProductVersionByBetweenRefDate(pResultEvt:ResultEvent):void{
			var result : ProductVersion = pResultEvt.result as ProductVersion;
        	sendNotification(NotificationList.PRODUCT_VERSION_BETWEEN_DATE_LISTED, result);       	
        }

        public function getProduct( productId:int, referenceDate:Date ):void
        {
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onGetProduct, onFault));
       		delegate.getProduct( productId, referenceDate );     	
        }

        private function onGetProduct(pResultEvt:ResultEvent):void{
			var result : ProductOption = pResultEvt.result as ProductOption;
        	sendNotification(NotificationList.PRODUCT_OPTION_LISTED, result);       	
        }

		public function findProfileList( productId:int, referenceDate:Date):void
        {
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onFindProfileList, onFault));
       		delegate.findProfileList( productId, referenceDate );     	
        }

        private function onFindProfileList(pResultEvt:ResultEvent):void{
			var result : ArrayCollection = pResultEvt.result as ArrayCollection;
        	sendNotification(NotificationList.PRODUCT_LIST_PROFILE_REFRESH, result);          	
        }

		public function getResponseRelationship(productId : int, questionnaireId : int, questionId : int, responseId : int, referenceDate : Date):void
        {
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onGetResponseRelationship, onFault));
       		delegate.getResponseRelationship( productId, questionnaireId, questionId, responseId, referenceDate );     	
        }

        private function onGetResponseRelationship(pResultEvt:ResultEvent):void{
			var result : ArrayCollection = pResultEvt.result as ArrayCollection;
        	sendNotification(NotificationList.PRODUCT_RESPONSE_RELATIONSHIP_REFRESH, result);          	
        }

		private function onFault(pFaultEvt:FaultEvent):void{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
        }

		public function listAllDeductible():void
        {
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onListAllDeductible, onFault));
       		delegate.listAllDeductible();     	
        }

        private function onListAllDeductible(pResultEvt:ResultEvent):void{
			var result : ArrayCollection = pResultEvt.result as ArrayCollection;
        	sendNotification(NotificationList.PRODUCT_LIST_DEDUCTIBLE_REFRESH, result);          	
        }

        public function getApplicationPropertyValue( applicationID:int, name:String ):void
        {
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onGetApplicationPropertyValue, onFault));
       		delegate.getApplicationPropertyValue( applicationID, name );        	
        }

		public function listProductPaymentTermByRefDate(productId:int, refDate:Date):void{
			var delegate:ProductDelegateProxy = new ProductDelegateProxy(new Responder(onListProductPaymentTermByRefDate, onFault));
			delegate.listProductPaymentTermByRefDate(productId, refDate);
		}
	
		private function onListProductPaymentTermByRefDate(resultEvent:ResultEvent):void{
			var result:ArrayCollection=resultEvent.result as ArrayCollection ;
			sendNotification(NotificationList.PRODUCT_PAYMENT_TERM_LISTED, result);
		}

        private function onGetApplicationPropertyValue( pResultEvt:ResultEvent ):void
        {
			var result : String = pResultEvt.result as String;
        	sendNotification(NotificationList.APPLICATION_PROPERTY_LISTED, result);
        }

        public function getIdentifiedListApplicationPropertyValue( identifiedList:IdentifiedList, applicationID:int, name:String ):void
        {
        	var delegate : ProductDelegateProxy = new ProductDelegateProxy(new Responder(onGetIdentifiedListApplicationPropertyValue, onFault));
       		delegate.getIdentifiedListApplicationPropertyValue(identifiedList, applicationID, name );        	
        }

        private function onGetIdentifiedListApplicationPropertyValue( pResultEvt:ResultEvent ):void
        {
			var result : IdentifiedList = pResultEvt.result as IdentifiedList;
        	sendNotification(NotificationList.APPLICATION_PROPERTY_IDENT_LISTED, result);
        }
    }
}