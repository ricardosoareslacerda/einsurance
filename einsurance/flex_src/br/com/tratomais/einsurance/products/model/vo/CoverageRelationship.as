package br.com.tratomais.einsurance.products.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.CoverageRelationship")]   
    [Managed]  
    public class CoverageRelationship extends HibernateBean 
    {  
        private var _id:CoverageRelationshipId;          
        private var _coverageByCoverageId:Coverage;
        private var _coverageByParentId:Coverage;  
        private var _expiryDate:Date;
		private var _relationshipFactor:Number;
		private var _minimumInsuredValue:Number;
		private var _maximumInsuredValue:Number;
        private var _registred:Date;  
        private var _updated:Date;  
          
        public function CoverageRelationship(){  
        }  
  
        public function get id():CoverageRelationshipId{  
            return _id;  
        }  
  
        public function set id(pData:CoverageRelationshipId):void{  
            _id=pData;  
        }  
  
        public function get coverageByCoverageId():Coverage{  
            return _coverageByCoverageId;  
        }  
  
        public function set coverageByCoverageId(pData:Coverage):void{  
            _coverageByCoverageId=pData;  
        }  
    
        public function get coverageByParentId():Coverage{  
            return _coverageByParentId;  
        }  
  
        public function set coverageByParentId(pData:Coverage):void{  
            _coverageByParentId=pData;  
        }          
            
        public function get relationshipFactor():Number{  
            return _relationshipFactor;  
        }  
  
        public function set relationshipFactor(pData:Number):void{  
            _relationshipFactor=pData;  
        }          
                    
        public function get minimumInsuredValue():Number{  
            return _minimumInsuredValue;  
        }  
  
        public function set minimumInsuredValue(pData:Number):void{  
            _minimumInsuredValue=pData;  
        }          
                    
        public function get maximumInsuredValue():Number{  
            return _maximumInsuredValue;  
        }  
  
        public function set maximumInsuredValue(pData:Number):void{  
            _maximumInsuredValue=pData;  
        }                
        
        public function get expiryDate():Date{  
            return _expiryDate;  
        }  
  
        public function set expiryDate(pData:Date):void{  
            _expiryDate=pData;  
        }  
  
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }    
    }  
}