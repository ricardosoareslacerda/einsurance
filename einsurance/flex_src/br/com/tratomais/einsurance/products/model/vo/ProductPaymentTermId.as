package br.com.tratomais.einsurance.products.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.ProductPaymentTermId")]   
    [Managed]  
    public class ProductPaymentTermId extends HibernateBean  
    {  
        private var _effectiveDate:Date;  
        private var _billingMethodId:int;  
        private var _paymentTermId:int;  
        private var _productId:int;  
  
        public function ProductPaymentTermId()  
        {  
        }  
  
        public function get effectiveDate():Date{  
            return _effectiveDate;  
        }  
  
        public function set effectiveDate(pData:Date):void{  
            _effectiveDate=pData;  
        }  
  
        public function get billingMethodId():int{  
            return _billingMethodId;  
        }  
  
        public function set billingMethodId(pData:int):void{  
            _billingMethodId=pData;  
        }  
  
        public function get paymentTermId():int{  
            return _paymentTermId;  
        }  
  
        public function set paymentTermId(pData:int):void{  
            _paymentTermId=pData;  
        }  
  
        public function get productId():int{  
            return _productId;  
        }  
  
        public function set productId(pData:int):void{  
            _productId=pData;  
        }  
    }  
}