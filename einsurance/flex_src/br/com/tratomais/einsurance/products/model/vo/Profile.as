package br.com.tratomais.einsurance.products.model.vo  
{
 
 import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.Profile")]    
    [Managed]  
    public class Profile extends HibernateBean 
    {  
       	private var _id:ProfileId;
		private var _questionnaire:Questionnaire;
		private var _product:Product;
		private var _question:Question;
		private var _response:Response;
		private var _expiryDate:Date;
		private var _responsePoints:Number;
		private var _registred:Date;
		private var _updated:Date;


		public function ProductProfile()
		{
			
		}

		public function get id():ProfileId{
			return _id;
		}

		public function set id(pData:ProfileId):void{
			_id=pData;
		}

		public function get questionnaire():Questionnaire{
			return _questionnaire;
		}

		public function set questionnaire(pData:Questionnaire):void{
			_questionnaire=pData;
		}

		public function get product():Product{
			return _product;
		}

		public function set product(pData:Product):void{
			_product=pData;
		}

		public function get question():Question{
			return _question;
		}

		public function set question(pData:Question):void{
			_question=pData;
		}

		public function get response():Response{
			return _response;
		}

		public function set response(pData:Response):void{
			_response=pData;
		}

		public function get expiryDate():Date{
			return _expiryDate;
		}

		public function set expiryDate(pData:Date):void{
			_expiryDate=pData;
		}

		public function get responsePoints():Number{
			return _responsePoints;
		}

		public function set responsePoints(pData:Number):void{
			_responsePoints=pData;
		}

		public function get registred():Date{
			return _registred;
		}

		public function set registred(pData:Date):void{
			_registred=pData;
		}

		public function get updated():Date{
			return _updated;
		}

		public function set updated(pData:Date):void{
			_updated=pData;
		}
    }  
} 