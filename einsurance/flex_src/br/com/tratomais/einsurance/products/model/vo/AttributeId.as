package  br.com.tratomais.einsurance.products.model.vo 
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.AttributeId")]    
    [Managed]  
    public class AttributeId extends HibernateBean 
    {  
        private var _effectiveDate:Date;  
        private var _attributeId:int;  

        public function AttributeId()  
        {  
        }  
  
        public function get effectiveDate():Date{  
            return _effectiveDate;  
        }  
  
        public function set effectiveDate(pData:Date):void{  
            _effectiveDate=pData;  
        }  
  
        public function get attributeId():int{  
            return _attributeId;  
        }  
  
        public function set attributeId(pData:int):void{  
            _attributeId=pData;  
        }  
    }  
}