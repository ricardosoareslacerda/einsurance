package br.com.tratomais.einsurance.products.model.vo 
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.ProductPaymentTerm")]     
    [Managed]  
    public class ProductPaymentTerm extends HibernateBean 
    {  
		/**
		 * Tipos de prazo de pagto
		 */
		public static const PAYMENT_TERM_TYPE_ALL:int = 176;			// Todos
		public static const PAYMENT_TERM_TYPE_POLICY:int = 174;			// Ap�lice
		public static const PAYMENT_TERM_TYPE_ENDORSEMENT:int = 175;	// Endosso
		public static const PAYMENT_TERM_TYPE_INVOICE:int = 781;		// Fatura
        private var _id:ProductPaymentTermId;  
        private var _paymentTerm:PaymentTerm;  
        private var _expiryDate:Date;  
        private var _paymentTermType:int;  
		private var _paymentTermDefault: Boolean;
		private var _applyFractioningAdditional: Boolean;
	    private var _registred:Date;  
        private var _updated:Date;  
  
        public function ProductPaymentTerm()  
        {  
        }  
  
        public function get id():ProductPaymentTermId{  
            return _id;  
        }  
  
        public function set id(pData:ProductPaymentTermId):void{  
            _id=pData;  
        }  
  
        public function get paymentTerm():PaymentTerm{  
            return _paymentTerm;  
        }  
  
        public function set paymentTerm(pData:PaymentTerm):void{  
            _paymentTerm=pData;  
        }  
  
        public function get expiryDate():Date{  
            return _expiryDate;  
        }  
  
        public function set expiryDate(pData:Date):void{  
            _expiryDate=pData;  
        }  
  
        public function get paymentTermType():int{  
            return _paymentTermType;  
        }  
  
        public function set paymentTermType(pData:int):void{  
            _paymentTermType=pData;  
        }  

        public function get paymentTermDefault():Boolean{  
            return _paymentTermDefault;  
        }  
  
        public function set applyFractioningAdditional(pData:Boolean):void{  
            _applyFractioningAdditional=pData;  
        }  
  
        public function get applyFractioningAdditional():Boolean{  
            return _applyFractioningAdditional;  
        }  
  
        public function set paymentTermDefault(pData:Boolean):void{  
            _paymentTermDefault=pData;  
        }  
        
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
  
  
          
    }  
}