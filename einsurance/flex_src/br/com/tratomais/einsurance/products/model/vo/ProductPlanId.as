package br.com.tratomais.einsurance.products.model.vo  
{  

	import net.digitalprimates.persistence.hibernate.HibernateBean;

    [RemoteClass(alias="br.com.tratomais.core.model.product.ProductPlanId")]      
    [Managed]  
    public class ProductPlanId  extends HibernateBean
    {  
        private var _effectiveDate:Date;  
        private var _planId:int;  
        private var _productId:int;  
  
        public function ProductPlanId() {                
        }  
  
        public function get effectiveDate():Date{  
            return _effectiveDate;  
        }  
  
        public function set effectiveDate(pData:Date):void{  
            _effectiveDate=pData;  
        }  
  
        public function get planId():int{  
            return _planId;  
        }  
  
        public function set planId(pData:int):void{  
            _planId=pData;  
        }  
  
        public function get productId():int{  
            return _productId;  
        }  
  
        public function set productId(pData:int):void{  
            _productId=pData;  
        }  
  
  
          
    }  
}  