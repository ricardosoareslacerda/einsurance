package br.com.tratomais.einsurance.products.model.vo  
{
/* 	import mx.collections.ArrayCollection;
 */	import mx.collections.ArrayCollection;
 
 import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.Product")]    
    [Managed]  
    public class Product extends HibernateBean 
    {  
        private var _productId:int;  
        private var _currencyId:int;  
        private var _objectId:int;  
        private var _branchId:int;  
        private var _productCode:String;  
        private var _name:String;  
        private var _nickName:String;  
		private var _masterPolicyRequired: Boolean;
		private var _beneficiaryRequired: Boolean;
		private var _inspectionRequired: Boolean;
		private var _invoiceRequired: Boolean;
		private var _profileRequired: Boolean;
		private var _riskValueRequired: Boolean;
        private var _riskValueType:int;  
        private var _reframingType:int;  
        private var _imageProperty:String;  
        private var _registred:Date;   
        private var _updated:Date;  
		private var _productVersions:ArrayCollection;
		private var _productPlans:ArrayCollection;
		private var _productPaymentTerm:ArrayCollection; 
		private var _productTerm:ArrayCollection; 
		private var _roadmaps:ArrayCollection;  
		private var _coveragePlans:ArrayCollection;
		private var _planPersonalRisks:ArrayCollection; 
		private var _planKinships:ArrayCollection;
		private var _productTerms:ArrayCollection;
		private var _productPaymentTerms:ArrayCollection;
	
        public function Product()  
        {  
        }  
  
        public function get productId():int{  
            return _productId;  
        }  
  
        public function set productId(pData:int):void{  
            _productId=pData;  
        }  
  
         public function get currencyId():int{  
            return _currencyId;  
        }  
  
        public function set currencyId(pData:int):void{  
            _currencyId=pData;   
        }  
  
        public function get objectId():int{  
            return _objectId;  
        }  
  
        public function set objectId(pData:int):void{  
            _objectId=pData;  
        }  
  
        public function get branchId():int{  
            return _branchId;  
        }  
  
        public function set branchId(pData:int):void{  
            _branchId=pData;  
        }  
  
        public function get productCode():String{  
            return _productCode;  
        }  
  
        public function set productCode(pData:String):void{  
            _productCode=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get nickName():String{  
            return _nickName;  
        }  
  
        public function set nickName(pData:String):void{  
            _nickName=pData;  
        }  

        public function get masterPolicyRequired(): Boolean { 
        	return _masterPolicyRequired;
        }
        
        public function set masterPolicyRequired(pActive: Boolean): void {
        	this._masterPolicyRequired = pActive;
        }

        public function get beneficiaryRequired(): Boolean { 
        	return _beneficiaryRequired;
        }
        
        public function set beneficiaryRequired(pActive: Boolean): void {
        	this._beneficiaryRequired = pActive;
        }

        public function get inspectionRequired(): Boolean { 
        	return _inspectionRequired;
        }
        
        public function set inspectionRequired(pActive: Boolean): void {
        	this._inspectionRequired = pActive;
        }
        public function get invoiceRequired(): Boolean { 
        	return _invoiceRequired;
        }
        
        public function set invoiceRequired(pActive: Boolean): void {
        	this._invoiceRequired = pActive;
        }
        public function get profileRequired(): Boolean { 
        	return _profileRequired;
        }
        
        public function set profileRequired(pActive: Boolean): void {
        	this._profileRequired = pActive;
        }

        public function get riskValueRequired(): Boolean { 
        	return _riskValueRequired;
        }
        
        public function set riskValueRequired(pActive: Boolean): void {
        	this._riskValueRequired = pActive;
        }
        		  
        public function get riskValueType():int{  
            return _riskValueType;  
        }  
  
        public function set riskValueType(pData:int):void{  
            _riskValueType=pData;  
        }  
  
        public function get reframingType():int{  
            return _reframingType;  
        }  
  
        public function set reframingType(pData:int):void{  
            _reframingType=pData;  
        }  
  
        public function get imageProperty():String{  
            return _imageProperty;  
        }  
  
        public function set imageProperty(pData:String):void{  
            _imageProperty=pData;  
        }  
  
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
  
		public function get productVersions():ArrayCollection{
			return _productVersions;
		}

        public function set productVersions(pData:ArrayCollection):void{  
            _productVersions=pData;  
        }		
		
		public function get productPlans():ArrayCollection{
			return _productPlans;
		}

        public function set productPlans(pData:ArrayCollection):void{  
            _productPlans=pData;  
        }		
		
		public function get productPaymentTerm():ArrayCollection{
			return _productPaymentTerm;
		}
		
        public function set productPaymentTerm(pData:ArrayCollection):void{  
            _productPaymentTerm=pData;  
        }		 
		
		public function get productTerm():ArrayCollection{
			return _productTerm;
		} 
		
        public function set productTerm(pData:ArrayCollection):void{  
            _productTerm=pData;  
        }
        		
		public function get roadmaps():ArrayCollection{
			return _roadmaps;
		}  
		
        public function set roadmaps(pData:ArrayCollection):void{  
            _roadmaps=pData;  
        }		
		
		public function get coveragePlans():ArrayCollection{
			return _coveragePlans;
		}
		
        public function set coveragePlans(pData:ArrayCollection):void{  
            _coveragePlans=pData;  
        }		
		
		public function get planPersonalRisks():ArrayCollection{
			return _planPersonalRisks;
		} 
		
        public function set planPersonalRisks(pData:ArrayCollection):void{  
            _planPersonalRisks=pData;  
        }
        		
		public function get planKinships():ArrayCollection{
			return _planKinships;
		}
		
        public function set planKinships(pData:ArrayCollection):void{  
            _planKinships=pData;  
        }		
		
		public function get productTerms():ArrayCollection{
			return _productTerms;
		}
		
        public function set productTerms(pData:ArrayCollection):void{  
            _productTerms=pData;  
        }
        
		public function get productPaymentTerms():ArrayCollection{
			return _productPaymentTerms;
		}
		
        public function set productPaymentTerms(pData:ArrayCollection):void{  
            _productPaymentTerms=pData;  
        }        
    }  
} 