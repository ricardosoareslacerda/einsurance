package br.com.tratomais.einsurance.products.model.vo 
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.PaymentTerm")]    
    [Managed]  
    public class PaymentTerm extends HibernateBean 
    {  
        private var _paymentTermId:int;  
        private var _paymentTermCode:String;  
        private var _name:String;  
        private var _paymentTermMultipleType:int;  
        private var _quantityInstallment:int;  
        private var _firstPaymentType:int;  
        private var _firstMultipleType:int;  
        private var _firstInstallment:int;  
        private var _cancelFirstInstallment:int;  
        private var _nextPaymentType:int;  
        private var _nextMultipleType:int;  
        private var _nextInstallment:int;  
        private var _cancelNextInstallment:int;  
        private var _fractioningAdditionalFactor:Number;  
        private var _interestRate:Number;  
        private var _displayOrder:int;  
        private var _registred:Date;  
        private var _updated:Date;  
  
        public function PaymentTerm()  
        {  
              
        }  
  
        public function get paymentTermId():int{  
            return _paymentTermId;  
        }  
  
        public function set paymentTermId(pData:int):void{  
            _paymentTermId=pData;  
        }  
  
        public function get paymentTermCode():String{  
            return _paymentTermCode;  
        }  
  
        public function set paymentTermCode(pData:String):void{  
            _paymentTermCode=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get paymentTermMultipleType():int{  
            return _paymentTermMultipleType;  
        }  
  
        public function set paymentTermMultipleType(pData:int):void{  
            _paymentTermMultipleType=pData;  
        }  
  
        public function get quantityInstallment():int{  
            return _quantityInstallment;  
        }  
  
        public function set quantityInstallment(pData:int):void{  
            _quantityInstallment=pData;  
        }  
  
        public function get firstPaymentType():int{  
            return _firstPaymentType;  
        }  
  
        public function set firstPaymentType(pData:int):void{  
            _firstPaymentType=pData;  
        }  
  
        public function get firstMultipleType():int{  
            return _firstMultipleType;  
        }  
  
        public function set firstMultipleType(pData:int):void{  
            _firstMultipleType=pData;  
        }  
  
        public function get firstInstallment():int{  
            return _firstInstallment;  
        }  
  
        public function set firstInstallment(pData:int):void{  
            _firstInstallment=pData;  
        }  
  
        public function get cancelFirstInstallment():int{  
            return _cancelFirstInstallment;  
        }  
  
        public function set cancelFirstInstallment(pData:int):void{  
            _cancelFirstInstallment=pData;  
        }  
  
        public function get nextPaymentType():int{  
            return _nextPaymentType;  
        }  
  
        public function set nextPaymentType(pData:int):void{  
            _nextPaymentType=pData;  
        }  
  
        public function get nextMultipleType():int{  
            return _nextMultipleType;  
        }  
  
        public function set nextMultipleType(pData:int):void{  
            _nextMultipleType=pData;  
        }  
  
        public function get nextInstallment():int{  
            return _nextInstallment;  
        }  
  
        public function set nextInstallment(pData:int):void{  
            _nextInstallment=pData;  
        }  
  
        public function get cancelNextInstallment():int{  
            return _cancelNextInstallment;  
        }  
  
        public function set cancelNextInstallment(pData:int):void{  
            _cancelNextInstallment=pData;  
        }  
  
        public function get fractioningAdditionalFactor():Number{  
            return _fractioningAdditionalFactor;  
        }  
  
        public function set fractioningAdditionalFactor(pData:Number):void{  
            _fractioningAdditionalFactor=pData;  
        }  
  
        public function get interestRate():Number{  
            return _interestRate;  
        }  
  
        public function set interestRate(pData:Number):void{  
            _interestRate=pData;  
        }  
  
        public function get displayOrder():int{  
            return _displayOrder;  
        }  
  
        public function set displayOrder(pData:int):void{  
            _displayOrder=pData;  
        }  
  
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
    }  
}