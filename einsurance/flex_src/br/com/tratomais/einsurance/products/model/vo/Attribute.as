package br.com.tratomais.einsurance.products.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.Attribute")]      
    [Managed]  
    public class Attribute extends HibernateBean 
    {  
        private var _id:AttributeId;  
        private var _expiryDate:Date;  
        private var _parentId:int;  
        private var _objectId:int;  
        private var _name:String;  
        private var _description:String;  
        private var _tableName:String;  
        private var _fieldName:String;
		private var _feature:Boolean;
		private var _calculation:Boolean;
		private var _quotation:Boolean;
		private var _proposal:Boolean;
		private var _informed:Boolean;           
        private var _dataGroupId:int;  
        private var _contentType:int;  
        private var _contentLength:int;  
        private var _contentPrecision:String;  
        private var _className:String;  
		private var _displayed:Boolean;
        private var _displayOrder:int;  
        private var _registred:Date;  
        private var _updated:Date;  
  
        public function Attribute()  
        {  
              
        }  
  
        public function get id():AttributeId{  
            return _id;  
        }  
  
        public function set id(pData:AttributeId):void{  
            _id=pData;  
        }  
  
        public function get expiryDate():Date{  
            return _expiryDate;  
        }  
  
        public function set expiryDate(pData:Date):void{  
            _expiryDate=pData;  
        }  
  
        public function get parentId():int{  
            return _parentId;  
        }  
  
        public function set parentId(pData:int):void{  
            _parentId=pData;  
        }  
  
        public function get objectId():int{  
            return _objectId;  
        }  
  
        public function set objectId(pData:int):void{  
            _objectId=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get description():String{  
            return _description;  
        }  
  
        public function set description(pData:String):void{  
            _description=pData;  
        }  
  
        public function get tableName():String{  
            return _tableName;  
        }  
  
        public function set tableName(pData:String):void{  
            _tableName=pData;  
        }  
  
        public function get fieldName():String{  
            return _fieldName;  
        }  
  
        public function set fieldName(pData:String):void{  
            _fieldName=pData;  
        }  

        public function get feature(): Boolean { 
        	return _feature;
        }
        
        public function set feature(pData: Boolean): void {
        	this._feature = pData;
        }
        
        public function get calculation(): Boolean { 
        	return _calculation;
        }
        
        public function set calculation(pData: Boolean): void {
        	this._calculation = pData;
        }
        
        public function get quotation(): Boolean { 
        	return _quotation;
        }
        
        public function set quotation(pData: Boolean): void {
        	this._quotation = pData;
        }
        
        public function get proposal(): Boolean { 
        	return _proposal;
        }
        
        public function set proposal(pData: Boolean): void {
        	this._proposal = pData;
        }
        
        public function get informed(): Boolean { 
        	return _informed;
        }
        
        public function set informed(pData: Boolean): void {
        	this._informed = pData;
        }
  
        public function get dataGroupId():int{  
            return _dataGroupId;  
        }  
  
        public function set dataGroupId(pData:int):void{  
            _dataGroupId=pData;  
        }  
  
        public function get contentType():int{  
            return _contentType;  
        }  
  
        public function set contentType(pData:int):void{  
            _contentType=pData;  
        }  
  
        public function get contentLength():int{  
            return _contentLength;  
        }  
  
        public function set contentLength(pData:int):void{  
            _contentLength=pData;  
        }  
  
        public function get contentPrecision():String{  
            return _contentPrecision;  
        }  
  
        public function set contentPrecision(pData:String):void{  
            _contentPrecision=pData;  
        }  
  
        public function get className():String{  
            return _className;  
        }  
  
        public function set className(pData:String):void{  
            _className=pData;  
        }  
  
        public function get displayOrder():int{  
            return _displayOrder;  
        }  
  
        public function get displayed(): Boolean { 
        	return _displayed;
        }
        
        public function set displayed(pDados: Boolean): void {
        	this._displayed = pDados;
        }

        public function set displayOrder(pData:int):void{  
            _displayOrder=pData;  
        }  
  
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
    }  
}