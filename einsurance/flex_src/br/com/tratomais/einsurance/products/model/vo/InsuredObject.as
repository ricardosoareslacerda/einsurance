package br.com.tratomais.einsurance.products.model.vo  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
    [RemoteClass(alias="br.com.tratomais.core.model.product.InsuredObject")]     
    [Managed]  
    public class InsuredObject extends HibernateBean  
    {  
        private var _objectId:int;  
        private var _objectCode:String;  
        private var _objectType:int;  
        private var _name:String;  
        private var _nickname:String;  
        private var _displayOrder:int;  
        private var _registred:Date;  
        private var _updated:Date;  
/*         private var _attributes:ArrayCollection;  
        private var _products:ArrayCollection;  
        private var _coverages:ArrayCollection;   */
  
        public function InsuredObject()  
        {  
        }  
  
        public function get objectId():int{  
            return _objectId;  
        }  
  
        public function set objectId(pData:int):void{  
            _objectId=pData;  
        }  
  
        public function get objectCode():String{  
            return _objectCode;  
        }  
  
        public function set objectCode(pData:String):void{  
            _objectCode=pData;  
        }  
  
        public function get objectType():int{  
            return _objectType;  
        }  
  
        public function set objectType(pData:int):void{  
            _objectType=pData;  
        }  
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get nickname():String{  
            return _nickname;  
        }  
  
        public function set nickname(pData:String):void{  
            _nickname=pData;  
        }  
  
        public function get displayOrder():int{  
            return _displayOrder;  
        }  
  
        public function set displayOrder(pData:int):void{  
            _displayOrder=pData;  
        }  
  
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
  
/*         public function get attributes():ArrayCollection{  
            return _attributes;  
        }  
  
        public function set attributes(pData:ArrayCollection):void{  
            _attributes=pData;  
        }  
  
        public function get products():ArrayCollection{  
            return _products;  
        }  
  
        public function set products(pData:ArrayCollection):void{  
            _products=pData;  
        }  
  
        public function get coverages():ArrayCollection{  
            return _coverages;  
        }  
  
        public function set coverages(pData:ArrayCollection):void{  
            _coverages=pData;  
        }   */
    }  
}