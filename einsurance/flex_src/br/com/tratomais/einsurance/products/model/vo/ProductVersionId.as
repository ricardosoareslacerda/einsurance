package br.com.tratomais.einsurance.products.model.vo 
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.ProductVersionId")]   
    [Managed]  
    public class ProductVersionId extends HibernateBean 
    {  
        private var _effectiveDate:Date;  
        private var _productId:int;  
  
        public function ProductVersionId()  
        {  
        }  
  
        public function get effectiveDate():Date{  
            return _effectiveDate;  
        }  
  
        public function set effectiveDate(pData:Date):void{  
            _effectiveDate=pData;  
        }  
  
        public function get productId():int{  
            return _productId;  
        }  
  
        public function set productId(pData:int):void{  
            _productId=pData;  
        }  
    }  
}  