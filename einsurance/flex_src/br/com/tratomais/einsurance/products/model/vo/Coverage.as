package br.com.tratomais.einsurance.products.model.vo  
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.Coverage")]   
    [Managed]  
    public class Coverage extends HibernateBean 
    {  
        private var _coverageId:int;  
        private var _coverage:Coverage;  
        private var _coverageCode:String;
        private var _goodsCode:String;
        private var _name:String;  
        private var _nickName:String;  
        private var _objectId:int;  
        private var _branchId:int;
        private var _basic:Boolean;
        private var _compulsory:Boolean;
        private var _service:Boolean;
        private var _riskType:int;  
        private var _displayed:Boolean;
        private var _displayOrder:int;  
        private var _registred:Date;  
        private var _updated:Date;  
        private var _coverages:ArrayCollection = new ArrayCollection();   
        
        private var _enable:Boolean;
        private var _checked:Boolean;
        
		public static const RISKTYPE_CONTENT:int = 298;  
		public static const RISKTYPE_STRUCTURE:int = 297; 
		public static const RISKTYPE_BOTH:int = 299;        
  
        public function Coverage()  
        {  
        }  
  
        public function get coverageId():int{  
            return _coverageId;  
        }  
  
        public function set coverageId(pData:int):void{  
            _coverageId=pData;  
        }  
  
        public function get coverage():Coverage{  
            return _coverage;  
        }  
  
        public function set coverage(pData:Coverage):void{  
            _coverage=pData;  
        }  
  
        public function get coverageCode():String{  
            return _coverageCode;  
        }  
  
        public function set coverageCode(pData:String):void{  
            _coverageCode=pData;  
        }
        
        public function get goodsCode():String{  
            return _goodsCode;  
        }  
  
        public function set goodsCode(pData:String):void{  
            _goodsCode=pData;  
        }
  
        public function get name():String{  
            return _name;  
        }  
  
        public function set name(pData:String):void{  
            _name=pData;  
        }  
  
        public function get nickName():String{  
            return _nickName;  
        }  
  
        public function set nickName(pData:String):void{  
            _nickName=pData;  
        }  
  
        public function get objectId():int{  
            return _objectId;  
        }  
  
        public function set objectId(pData:int):void{  
            _objectId=pData;  
        }  
  
        public function get branchId():int{  
            return _branchId;  
        }  
  
        public function set branchId(pData:int):void{  
            _branchId=pData;  
        }  
  
        public function get basic(): Boolean { 
        	return _basic;
        }
        
        public function set basic(pDados: Boolean): void {
        	this._basic = pDados;
        }

        public function get compulsory(): Boolean { 
        	return _compulsory;
        }
        
        public function set compulsory(pDados: Boolean): void {
        	this._compulsory = pDados;
        }

        public function get service(): Boolean { 
        	return _service;
        }
        
        public function set service(pDados: Boolean): void {
        	this._service = pDados;
        }

        public function get riskType():int{  
            return _riskType;  
        }  
  
        public function set riskType(pData:int):void{  
            _riskType=pData;  
        }  
  
         public function get displayed(): Boolean { 
        	return _displayed;
        }
        
        public function set displayed(pDados: Boolean): void {
        	this._displayed = pDados;
        }

        public function get displayOrder():int{  
            return _displayOrder;  
        }  
  
        public function set displayOrder(pData:int):void{  
            _displayOrder=pData;  
        }  
  
        public function get registred():Date{  
            return _registred;  
        }  
  
        public function set registred(pData:Date):void{  
            _registred=pData;  
        }  
  
        public function get updated():Date{  
            return _updated;  
        }  
  
        public function set updated(pData:Date):void{  
            _updated=pData;  
        }  
  
        public function get coverages():ArrayCollection{  
            return _coverages;  
        }  
  
        public function set coverages(pData:ArrayCollection):void{  
            _coverages=pData;  
        }  
        
        public function get enable():Boolean{  
            return _enable;  
        }  
  
        public function set enable(pData:Boolean):void{  
            _enable=pData;  
        }  
        
		public function get checked():Boolean{  
            return _checked;  
        }  
  
        public function set checked(pData:Boolean):void{  
            _checked=pData;  
        }
    }  
}