package br.com.tratomais.einsurance.support.view.mediator 
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.ReturnValue;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.support.model.proxy.SupportPolicyProxy;
	import br.com.tratomais.einsurance.support.view.components.SupportPolicyReactivation;
	
	import flash.events.Event;
	
	import mx.controls.Alert;
	import mx.resources.ResourceManager;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class SupportPolicyReactivationMediator extends Mediator implements IMediator 
	{
		public static const NAME:String = 'SupportPolicyReactivationMediator';
		
		private var supportPolicyProxy:SupportPolicyProxy;
		
		public function SupportPolicyReactivationMediator(viewComponent:Object=null){
			super(NAME, viewComponent);
			supportPolicyProxy = SupportPolicyProxy(facade.retrieveProxy(SupportPolicyProxy.NAME));
		}

        public function get supportPolicyReactivation():SupportPolicyReactivation{  
            return viewComponent as SupportPolicyReactivation; 
        } 

		override public function onRegister():void{
			super.onRegister();
			
			try{
				supportPolicyReactivation.addEventListener(SupportPolicyReactivation.SEND_CONTRACT, sendContract);
				supportPolicyReactivation.addEventListener(SupportPolicyReactivation.SEND_CONTRACT_ENDORSEMENT, sendContractEndorsement);
			}catch(error:Error){
				Alert.show(error.message, "Error", Alert.OK, null, null, Utilities.errorMsg);
			}
    	}

		override public function onRemove():void{
			super.onRemove();
			
			try{
				supportPolicyReactivation.removeEventListener(SupportPolicyReactivation.SEND_CONTRACT, sendContract);
				supportPolicyReactivation.removeEventListener(SupportPolicyReactivation.SEND_CONTRACT_ENDORSEMENT, sendContractEndorsement);
				this.clearFields();
				setViewComponent(null);
			}catch(error:Error){
				Alert.show(error.message, "Error", Alert.OK, null, null, Utilities.errorMsg);
			}
		}
		
        override public function listNotificationInterests():Array{  
            return [NotificationList.POLICY_REACTIVATION_RESULT, NotificationList.POLICY_CREATE_AND_FREE_PENDENCY_RESULT];  
        } 		

        override public function handleNotification(notification:INotification):void{
            switch(notification.getName()){
            	case NotificationList.POLICY_REACTIVATION_RESULT:
            		var result:ReturnValue = notification.getBody() as ReturnValue;
            		
            		if(result.success){
            			supportPolicyReactivation.lbResult.text = ResourceManager.getInstance().getString('messages', 'successfullyActivatedPolicy');
            		}else{
            			supportPolicyReactivation.lbResult.text = result.messages;
            		}
            		
            		break;
            	case NotificationList.POLICY_CREATE_AND_FREE_PENDENCY_RESULT:
            		var result:ReturnValue = notification.getBody() as ReturnValue;
           			supportPolicyReactivation.lbResult2.text = result.messages;
            		break;
            }
        }
        
        private function sendContract(event:Event):void{
        	try{
        		var contractId:int = int(supportPolicyReactivation.txtContratId.text);
        		supportPolicyProxy.policyReactivation(contractId);
        		supportPolicyReactivation.lbResult.text = "";
			}catch(error:Error){
				Alert.show(error.message, "Error", Alert.OK, null, null, Utilities.errorMsg);
			}
        }
        
        private function sendContractEndorsement(event:Event):void{
        	try{
        		var contractId:int = int(supportPolicyReactivation.txtContratId2.text);
        		var endorsementId:int = int(supportPolicyReactivation.txtEndorsementId2.text);
        		supportPolicyProxy.createAndFreePendency(contractId, endorsementId);
        		supportPolicyReactivation.lbResult.text = "";
			}catch(error:Error){
				Alert.show(error.message, "Error", Alert.OK, null, null, Utilities.errorMsg);
			}
        }
        
        private function clearFields():void{
			supportPolicyReactivation.txtContratId.text = "";
			supportPolicyReactivation.lbResult.text = "";
			
			supportPolicyReactivation.txtContratId2.text = ""; 
			supportPolicyReactivation.txtEndorsementId2.text = "";
			supportPolicyReactivation.lbResult2.text = "";
        }
	}
}