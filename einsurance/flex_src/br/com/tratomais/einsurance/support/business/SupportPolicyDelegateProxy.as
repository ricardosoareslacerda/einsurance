////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Bruno Souza
// @version 1.0
// @lastModified 09/11/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.support.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	
	import mx.rpc.IResponder;
	
	public class SupportPolicyDelegateProxy
	{
		private var responder:IResponder;
		private var service:Object;
		
		public function SupportPolicyDelegateProxy(pResponder:IResponder){
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_SUPPORT_POLICY);
			responder = pResponder;
		}
		
		public function policyReactivation(contractId:int):void{
			var call:Object = service.policyReactivation(contractId);
			call.addResponder(responder);
		}	
		
		public function createAndFreePendency(contractId:int, endorsementId:int):void{
			var call:Object = service.createAndFreePendency(contractId, endorsementId, new Date(), 2, 575);
			call.addResponder(responder);
		}	

	}
}