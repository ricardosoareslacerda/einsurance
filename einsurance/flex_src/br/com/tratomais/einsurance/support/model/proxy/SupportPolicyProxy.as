////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Bruno Souza
// @version 1.0
// @lastModified 09/11/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.support.model.proxy
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.ReturnValue;
	import br.com.tratomais.einsurance.support.business.SupportPolicyDelegateProxy;
	
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	[Bindable]
	public class SupportPolicyProxy extends Proxy implements IProxy
	{
		public static const NAME:String = "SupportPolicyProxy";
		
		[Embed(source="/assets/images/error.png")]
		public var errorMsg:Class;
		
		public function SupportPolicyProxy(data:Object = null){
			super(NAME, data);
		}

		public function policyReactivation(contractId:int):void{
			var delegate:SupportPolicyDelegateProxy = new SupportPolicyDelegateProxy(new Responder(onPolicyReactivationResult, onFault));
			delegate.policyReactivation(contractId);
		}		
		
		public function onPolicyReactivationResult(pResultEvt:ResultEvent):void{
			var result:ReturnValue=pResultEvt.result as ReturnValue;
         	sendNotification(NotificationList.POLICY_REACTIVATION_RESULT, result);			
		}

		public function createAndFreePendency(contractId:int, endorsementId:int):void{
			var delegate:SupportPolicyDelegateProxy = new SupportPolicyDelegateProxy(new Responder(onCreateAndFreePendency, onFault));
			delegate.createAndFreePendency(contractId, endorsementId);
		}

		public function onCreateAndFreePendency(pResultEvt:ResultEvent):void{
			var result:ReturnValue=new ReturnValue();
			result.success = true;
			result.messages = "Favor verificar a emissão!"
         	sendNotification(NotificationList.POLICY_CREATE_AND_FREE_PENDENCY_RESULT, result);			
		}
		
		private function onFault(pFaultEvt:FaultEvent):void{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
		}	
	}
}