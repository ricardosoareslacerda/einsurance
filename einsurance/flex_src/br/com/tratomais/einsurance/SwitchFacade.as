////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2009 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 17/12/2009
//
// NOTICE: Adobe permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance
{
    import fr.kapit.puremvc.as3.patterns.facade.DebugFacade;
    import org.puremvc.as3.patterns.facade.Facade;

    /** 
    * Release version of the facade, extends Facade class from PureMVC
    **/
    CONFIG::release
    public class SwitchFacade extends Facade
    {
      public function SwitchFacade()
      {
        super();
      }
    }

    // Debug version of the facade, extends DebugFacade class from PureMVCKapit
    CONFIG::debug
    public class SwitchFacade extends DebugFacade
    {
        public function SwitchFacade()
        {
          super();
        }
    }

}