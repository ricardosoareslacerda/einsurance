package br.com.tratomais.einsurance.search.model.vo
{
  	import net.digitalprimates.persistence.hibernate.HibernateBean;

    [RemoteClass(alias="br.com.tratomais.core.model.search.SearchDocumentResult")]     
    [Managed]    
    public class SearchDocumentResult extends HibernateBean 
    {  
  		private var _endorsementId:int;
  		private var _contractId:int;
        private var _policyNumber:Number;
        private var _channelExternalCode:String;
        private var _certificateNumber:Number;
        private var _productName:String;
        private var _productId:int;
        private var _insuredName:String;
        private var _paymentTerm:String;
        private var _issuingStatus:String;
        private var _objectId:int;
        private var _changeLocked:Boolean;
        private var _issuingStatusId:int;
		private var _issuanceType:String;
		private var _issuanceTypeId:int;
		private var _endorsementType:int;
		private var _contractStatusId:int;
		private var _contractStatus:String;
		private var _effectiveDate:Date;
		private var _lastEndorsementId:int;
		private var _renewed:Boolean;
		private var _existsRenewalAlert:Boolean;
	    private var _login:String;
	    
        public function SearchDocumentResult(){  
        }  

  		public function get endorsementId():int{
  			return _endorsementId;
  		}
  		
  		public function set endorsementId(pData:int):void{
  			_endorsementId = pData;
  		}
  		
  		public function get contractId():int{
  			return _contractId;	
  		}
  		  		
  		public function set contractId(pData:int){
  			_contractId = pData;	
  		}  		
  
        public function get policyNumber():Number{  
            return _policyNumber;  
        }  
  
        public function set policyNumber(pData:Number):void{  
            _policyNumber=pData;  
        }  
  
        public function get channelExternalCode():String{  
            return _channelExternalCode; 
        }  
  
        public function set channelExternalCode(pData:String):void{  
            _channelExternalCode=pData;  
        }   
  
        public function get certificateNumber():Number{  
            return _certificateNumber;  
        }  
  
        public function set certificateNumber(pData:Number):void{  
            _certificateNumber=pData;  
        }  
  
        public function get insuredName():String{  
            return _insuredName;  
        }  
  
        public function set insuredName(pData:String):void{  
            _insuredName=pData;  
        }  
  
        public function get productName():String{  
            return _productName;  
        }  
  
        public function set productName(pData:String):void{  
            _productName=pData;  
        }  
  
    	public function get productId():int{
  			return _productId;	
  		}
  		  		
  		public function set productId(pData:int){
  			_productId = pData;	
  		} 
  
        public function get paymentTerm():String{  
            return _paymentTerm;  
        }  
  
        public function set paymentTerm(pData:String):void{  
            _paymentTerm=pData;  
        }  
  
        public function get issuingStatus():String{  
            return _issuingStatus;  
        }  
  
        public function set issuingStatus(pData:String):void{  
            _issuingStatus=pData;  
        }
        public function get objectId():int{  
            return _objectId;  
        }  
  
        public function set objectId(pData:int):void{  
            _objectId=pData;  
        }        

        public function get changeLocked():Boolean{  
            return _changeLocked;  
        }  
  
        public function set changeLocked(pData:Boolean):void{  
            _changeLocked=pData;  
        }        

        public function get issuingStatusId():int{  
            return _issuingStatusId;  
        }  
  
        public function set issuingStatusId(pData:int):void{  
            _issuingStatusId=pData;  
        }

        public function get issuanceType():String{  
            return _issuanceType;
        }  
  
        public function set issuanceType(pData:String):void{  
            _issuanceType=pData;  
        }

        public function get issuanceTypeId():int{  
            return _issuanceTypeId;
        }  
  
        public function set issuanceTypeId(pData:int):void{  
            _issuanceTypeId=pData;  
        }
        
        public function get endorsementType():int{  
            return _endorsementType;  
        }  
  
        public function set endorsementType(pData:int):void{  
            _endorsementType=pData;  
        }
        
        public function get contractStatusId():int{  
            return _contractStatusId;  
        }  
  
        public function set contractStatusId(pData:int):void{  
            _contractStatusId=pData;  
        }
        
        public function get contractStatus():String{  
            return _contractStatus;
        }  
  
        public function set contractStatus(pData:String):void{  
            _contractStatus=pData;  
        } 
        
        public function get effectiveDate():Date{  
            return _effectiveDate;  
        }  
  
        public function set effectiveDate(pData:Date):void{
            _effectiveDate=pData;
        } 
        
        public function get lastEndorsementId():int{
        	return _lastEndorsementId;
        } 
        
        public function set lastEndorsementId(pData:int):void{
            _lastEndorsementId=pData;
        } 
        
        public function get renewed():Boolean{  
            return _renewed;
        }  
  
        public function set renewed(pData:Boolean):void{  
            _renewed=pData;
        }  
		
		public function get existsRenewalAlert():Boolean{  
            return _existsRenewalAlert;
        }  
  
        public function set existsRenewalAlert(pData:Boolean):void{  
            _existsRenewalAlert=pData;
        }

        public function get login():String{  
            return _login;
        }

        public function set login(pData:String):void{
            _login=pData;
        }
    }  
}