package br.com.tratomais.einsurance.search.model.proxy
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.ReturnValue;
	import br.com.tratomais.einsurance.core.ErrorList;
	import br.com.tratomais.einsurance.core.components.paging.vo.Paginacao;
	import br.com.tratomais.einsurance.search.business.SearchDocumentDelegateProxy;
	import br.com.tratomais.einsurance.search.model.vo.SearchDocumentParameters;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	[Bindable]
	public class SearchDocumentProxy extends Proxy implements IProxy {
		
		public static const NAME:String = "SearchDocumentProxy";
		
		[Embed(source="/assets/images/error.png")]
		public var errorMsg:Class;
		
		public function SearchDocumentProxy(data:Object = null)
		{
			super(NAME, data );
		}
		
		public function listSearchDocument(parameters:SearchDocumentParameters):void{
			var delegate:SearchDocumentDelegateProxy = new SearchDocumentDelegateProxy(new Responder( onListSearchDocumentResult, onFault));
			delegate.listSearchDocument( parameters );
		}		
		
		public function onListSearchDocumentResult(pResultEvt:ResultEvent):void{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.SEARCH_DOCUMENT_LISTED, result);			
		}

		public function listSearchDocumentPaging(parameters:SearchDocumentParameters):void{
			var delegate:SearchDocumentDelegateProxy = new SearchDocumentDelegateProxy(new Responder( onListSearchDocumentPagingResult, onFault));
			delegate.listSearchDocumentPaging( parameters );
		}		
		
		public function onListSearchDocumentPagingResult(pResultEvt:ResultEvent):void{
			var result:Paginacao=pResultEvt.result as Paginacao;
         	sendNotification(NotificationList.SEARCH_DOCUMENT_LISTED, result);			
		}

		private function onFault(pFaultEvt:FaultEvent):void
		{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
		}
		
		public function policyCancellation( contractId:int, issuanceType:int, cancelDate:Date, cancelReason:int ) : void {
        	var delegate : SearchDocumentDelegateProxy = new SearchDocumentDelegateProxy(new Responder(onPolicyCancellation, onFault));
        	delegate.policyCancellation( contractId, issuanceType, cancelDate, cancelReason );
        }				
        
        private function onPolicyCancellation(pResultEvent:ResultEvent):void  {  
        	var result : ReturnValue = pResultEvent.result as ReturnValue;
         	sendNotification(NotificationList.CERTIFICATE_EMISION_CANCELED, result);
        }
        
		public function listRenewalAlert(contractId:int, endorsementId:int, resolved:Boolean):void{
			var delegate:SearchDocumentDelegateProxy = new SearchDocumentDelegateProxy(new Responder(onlistRenewalAlertResult, onFault));
			delegate.listRenewalAlert(contractId, endorsementId, resolved);
		}
		
		public function onlistRenewalAlertResult(pResultEvt:ResultEvent):void{
			var result:ErrorList = pResultEvt.result as ErrorList;
         	sendNotification(NotificationList.RENEWAL_ALERT_RESULT, result);
		}
	}
}