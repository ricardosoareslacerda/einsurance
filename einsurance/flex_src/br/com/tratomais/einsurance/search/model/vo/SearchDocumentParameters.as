package br.com.tratomais.einsurance.search.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;

    [RemoteClass(alias="br.com.tratomais.core.model.search.SearchDocumentParameters")]     
   	[Managed]   
    public class SearchDocumentParameters extends HibernateBean    
    {  
        private var _partnerId:int;
        private var _contractId:int;  
        private var _certificateNumber:String;
        private var _policyNumber:String;  
        private var _name:String;  
        private var _documentType:int;  
        private var _documentNumber:String;
        private var _renewedOnly:Boolean;
        private var _page:int;
        private var _totalPage:int;
        private var _currentPage:int;
  
        public function SearchDocumentParameters(){  
        }  
  		
        public function get partnerId():int{  
            return _partnerId;  
        }  
  
        public function set partnerId(pData:int):void{  
            _partnerId=pData;  
        }
          
        public function get contractId():int{  
            return _contractId;  
        }
  
        public function set contractId(pData:int):void{  
            _contractId=pData;  
        }
  
        public function get certificateNumber():String{  
            return _certificateNumber;  
        }
  
        public function set certificateNumber(pData:String):void{  
            _certificateNumber=pData;  
        }
  
        public function get policyNumber():String{  
            return _policyNumber;  
        }
  
        public function set policyNumber(pData:String):void{  
            _policyNumber=pData;  
        }
  
        public function get name():String{  
            return _name;  
        }
  
        public function set name(pData:String):void{  
            _name=pData;  
        }
  
        public function get documentType():int{  
            return _documentType;  
        }
  
        public function set documentType(pData:int):void{  
            _documentType=pData;  
        }
  
        public function get documentNumber():String{  
            return _documentNumber;  
        }
  
        public function set documentNumber(pData:String):void{  
            _documentNumber=pData;  
        }
        
        public function get renewedOnly():Boolean{  
            return _renewedOnly;
        }  
  
        public function set renewedOnly(pData:Boolean):void{  
            _renewedOnly=pData;
        }
        
		public function get page():int{
			return _page;
		}
	
		public function set page(pData:int):void{
			_page = pData;
		}
	
		public function get totalPage():int{
			return _totalPage;
		}
	
		public function set totalPage(pData:int):void{
			_totalPage = pData;
		}
		
		public function get currentPage():int{
			return _currentPage;
		}
	
		public function set currentPage(pData:int):void{
			_currentPage = pData;
		}
    }  
}