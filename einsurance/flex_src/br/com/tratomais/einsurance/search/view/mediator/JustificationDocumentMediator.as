package br.com.tratomais.einsurance.search.view.mediator
{
	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.customer.model.proxy.DataOptionProxy;
	import br.com.tratomais.einsurance.customer.model.vo.DataGroup;
	import br.com.tratomais.einsurance.customer.model.vo.DataOption;
	import br.com.tratomais.einsurance.policy.model.vo.Endorsement;
	import br.com.tratomais.einsurance.search.model.proxy.SearchDocumentProxy;
	import br.com.tratomais.einsurance.search.view.components.JustificationFormEndorsement;
	import br.com.tratomais.einsurance.useradm.model.vo.Module;
	import br.com.tratomais.einsurance.useradm.model.vo.Role;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.resources.ResourceManager;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	[ResourceBundle("messages")]
	[ResourceBundle("resources")]
	public class JustificationDocumentMediator extends Mediator implements IMediator
	{
		public static const NAME:String = 'JustificationDocumentMediator';
		
		public function JustificationDocumentMediator(viewComponent:JustificationFormEndorsement, endorsement:int, contract : int, 
			effectiveDate:Date, certificateNumber:Number, policyNumber:Number)
		{
            super(NAME, viewComponent);
            searchDocumentProxy = SearchDocumentProxy (facade.retrieveProxy(SearchDocumentProxy.NAME));
            dataOptionMotive = DataOptionProxy (facade.retrieveProxy(DataOptionProxy.NAME));
            dataOptionType = DataOptionProxy (facade.retrieveProxy(DataOptionProxy.NAME));
            _endorsementId = endorsement;
            _contractId = contract;
            _effectiveDate = effectiveDate;
            _certificateNumber = certificateNumber;
            _policyNumber = policyNumber;
		}
		
		/**
		 * Proxy
		 * */
		private var searchDocumentProxy : SearchDocumentProxy;
		private var dataOptionMotive : DataOptionProxy;
		private var dataOptionType : DataOptionProxy;
		public var warningMsg:Class;
					
		/**
		 * 
		 * */
		private var _contractId : int;
		
		/**
		 * 
		 * **/
		private var _endorsementId : int;
		
		/**
		 * 
		 * **/
		 
		private var _certificateNumber:Number;
		/**
		 * 
		 *  **/
		private var _policyNumber:Number;

		/**
		 * 
		 *  **/
		 		 
		private var _effectiveDate : Date;

		public function set effectiveDate( pDate:Date ):void
		{				
			_effectiveDate = pDate;
		}
		
		public function get effectiveDate():Date
		{
			return _effectiveDate;
		}
		
		public function set endorsementId( pData : int){
			this._endorsementId = pData;
		}

		public function get endorsementId() : int{
			return this._endorsementId;
		}

		public function set contractId( pData : int){
			this._contractId = pData;
		}

		public function get contractId() : int{
			return this._contractId;
		}
		
		public function set certificateNumber( pData : Number){
			this._certificateNumber = pData;
		}

		public function get certificateNumber() : Number{
			return this._certificateNumber;
		}	
		
		public function set policyNumber( pData : Number){
			this._policyNumber = pData;
		}

		public function get policyNumber() : Number{
			return this._policyNumber;
		}	
		
        private function accessModule(_moduleId:int):Boolean{
			var allowedUserModule:ArrayCollection = ApplicationFacade.getInstance().loggedUser.roles;
			var access:Boolean = false;
			
			for each(var role:Role in allowedUserModule){
				for each(var module:Module in role.modules){
					if(module.moduleId == _moduleId){
						access = true;
					}
				}
			}
			
			return access;
        }		

		/**
		 * Return the password form
		 */		
        protected function get justificationFormEndorsement():JustificationFormEndorsement{  
          return viewComponent as JustificationFormEndorsement;  
        }

   		override public function listNotificationInterests():Array  
        {  
            return [ NotificationList.DATA_OPTION_LISTED ];
        }
            
        override public function handleNotification(notification:INotification):void  
        {  
            switch ( notification.getName() )  
            {  
        		case NotificationList.DATA_OPTION_LISTED:
        			var collection : ArrayCollection = notification.getBody() as ArrayCollection;
        			
        			if( ( collection.getItemAt(0) as DataOption ).dataGroup.dataGroupId == DataGroup.ISSUANCE_TYPE ) {
	        			var collectionIssuance : ArrayCollection = new ArrayCollection();
	        			var allowedModules:ArrayCollection = ApplicationFacade.getInstance().getEinsurance.desktop.searchDocument.allowedModules;
	        			
	        			for each(var module:Module in allowedModules){
	        				if( accessModule( module.moduleId ) ){
		        				for( var i:int = 0; i < collection.length; i++ )
		        				{
		        					if( ( collection.getItemAt(i) as DataOption ).dataOptionId == Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR && module.moduleId == 30201 || 
		        						( collection.getItemAt(i) as DataOption ).dataOptionId == Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_INSURED && module.moduleId == 30202 )
		        					{
		        						collectionIssuance.addItem( collection.getItemAt(i) );
		        					}
		        				}
			        		}
			        	}
        				justificationFormEndorsement.cmbxAnnulmentType.dataProvider = collectionIssuance;
        				justificationFormEndorsement.cmbxAnnulmentType.dropdown.dataProvider = collectionIssuance;
        				justificationFormEndorsement.cmbxAnnulmentType.selectedIndex = -1;			        	
			        }
        			if( ( collection.getItemAt(0) as DataOption ).dataGroup.dataGroupId == DataGroup.CANCEL_REASON ) 
        			{
        				justificationFormEndorsement.cmbxJustifications.dataProvider = collection;
        				justificationFormEndorsement.cmbxJustifications.dropdown.dataProvider = collection;
        				justificationFormEndorsement.cmbxJustifications.selectedIndex = -1;
        			}

    				justificationFormEndorsement.dfDateAnnulation.newDisabledRanges( new Date(), effectiveDate );	
        		break;
            }
        }
        		
		/**
		 * During the Register Phrase, register the Events that the form depends on. Initialize variables needed.
		 */        
		override public function onRegister():void 
		{
           	super.onRegister();
            justificationFormEndorsement.addEventListener(JustificationFormEndorsement.SAVE_CHANGES, saveChanges, false, 0, true);
            justificationFormEndorsement.addEventListener(JustificationFormEndorsement.CANCEL_CHANGES, cancelChanges, false, 0, true);
            this.initialize();
		}
		 
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
		   	justificationFormEndorsement.removeEventListener(JustificationFormEndorsement.CANCEL_CHANGES, cancelChanges);
           	justificationFormEndorsement.removeEventListener(JustificationFormEndorsement.SAVE_CHANGES, saveChanges);
		}
		
		private function initialize() : void{
			dataOptionMotive.listDataOption(87);//Motivo de la anulación
			dataOptionType.listDataOption(3);//Tipo de anulação	
			rangeMaskedDateField(); //carregar ranges da data de anulação
			justificationFormEndorsement.messageParameters = new Array(Utilities.padValue(certificateNumber, "0", 9, 'l'), policyNumber);
		}
		
		// ----------------------------------------------------------------------------------------------------------
		
		private function rangeMaskedDateField():void
		{
		
		}
		/**
		 * Saves changes, validating the field values
		 */
		private function saveChanges(event: Event):void{
			if (Utilities.validateForm(justificationFormEndorsement.validatorForm, ResourceManager.getInstance().getString('resources', 'reasonCancel'))) {
				if( justificationFormEndorsement.dfDateAnnulation.newDate < effectiveDate || justificationFormEndorsement.dfDateAnnulation.newDate > new Date() ){ 
					Alert.show(ResourceManager.getInstance().getString("messages", "hgherAnnulmentDate"), ResourceManager.getInstance().getString("resources","attention"), Alert.OK, null, null, warningMsg);
				}
				else {
					searchDocumentProxy.policyCancellation( contractId, justificationFormEndorsement.justificationTypeSelectedId,
															justificationFormEndorsement.justificationDateAnnulation,															
															justificationFormEndorsement.justificationReasonSelectedId );
				}
			}  
		}
		
		/**
		 * Close form without changes
		 * @param event E
		 */
		private function cancelChanges(event: Event):void{
	        justificationFormEndorsement.close();
		}
	}
}