////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Bruno Souza
// @version 1.0
// @lastModified 22/09/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.search.view.mediator {

	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.Constant;
	import br.com.tratomais.einsurance.core.Desktop;
	import br.com.tratomais.einsurance.core.DesktopMediator;
	import br.com.tratomais.einsurance.core.DesktopViewsList;
	import br.com.tratomais.einsurance.core.ErrorList;
	import br.com.tratomais.einsurance.core.ErrorWindow;
	import br.com.tratomais.einsurance.core.Errors;
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.NotificationViewsList;
	import br.com.tratomais.einsurance.core.ReturnValue;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.core.components.paging.vo.Paginacao;
	import br.com.tratomais.einsurance.core.components.paging.vo.PagingParameters;
	import br.com.tratomais.einsurance.customer.model.proxy.BrokerProxy;
	import br.com.tratomais.einsurance.customer.model.vo.Channel;
	import br.com.tratomais.einsurance.customer.model.vo.DataOption;
	import br.com.tratomais.einsurance.policy.model.vo.EndorsementOption;
	import br.com.tratomais.einsurance.report.model.proxy.ReportProxy;
	import br.com.tratomais.einsurance.report.model.vo.Report;
	import br.com.tratomais.einsurance.sales.model.proxy.ProposalProxy;
	import br.com.tratomais.einsurance.sales.view.mediator.ProposalMediator;
	import br.com.tratomais.einsurance.search.model.proxy.SearchDocumentProxy;
	import br.com.tratomais.einsurance.search.model.vo.SearchDocumentParameters;
	import br.com.tratomais.einsurance.search.model.vo.SearchDocumentResult;
	import br.com.tratomais.einsurance.search.view.components.SearchDocument;
	import br.com.tratomais.einsurance.useradm.model.proxy.ModuleProxy;
	import br.com.tratomais.einsurance.useradm.model.vo.Module;
	import br.com.tratomais.einsurance.useradm.model.vo.User;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.ItemClickEvent;
	import mx.events.MenuEvent;
	import mx.rpc.events.FaultEvent;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class SearchDocumentMediator extends Mediator implements IMediator{
	
	
	 protected function get desktop(): Desktop {
           return viewComponent as Desktop;
     }
     
     private function getChannelList(partnerId :int ) : ArrayCollection{
			var user: User = ApplicationFacade.getInstance().loggedUser;
			var channels : ArrayCollection = user.channels;
			var newChannels : ArrayCollection = new ArrayCollection();
			if(channels == null || channels.length == 0){
				return null;
			}else{
				for each(var channel : Channel in channels){
					if((channel.partnerId == partnerId) && (channel.active = true)){
						newChannels.addItem(channel);
					}
				}
				if(newChannels != null && newChannels.length > 0){
					return newChannels;
				}else{
					return null;
				}
			}
		}
		
		public static const NAME:String = 'SearchDocumentMediator';
		
		private var brokerProxy:BrokerProxy;
		private var searchDocumentProxy:SearchDocumentProxy;
		private var proposalProxy:ProposalProxy;
		private var moduleProxy:ModuleProxy;
		private var desktopMediator:DesktopMediator;
		private var reportProxy:ReportProxy;
		
		private var desktopInstance:Desktop = ApplicationFacade.getInstance().getEinsurance.desktop;
    		
		public function SearchDocumentMediator(viewComponent:Object=null){  
			super(NAME, viewComponent);
           
			searchDocumentProxy = SearchDocumentProxy( facade.retrieveProxy( SearchDocumentProxy.NAME ) );
			brokerProxy = BrokerProxy( facade.retrieveProxy( BrokerProxy.NAME ) );
			proposalProxy = ProposalProxy( facade.retrieveProxy(ProposalProxy.NAME));
			moduleProxy = ModuleProxy( facade.retrieveProxy(ModuleProxy.NAME));
			reportProxy = ReportProxy( facade.retrieveProxy(ReportProxy.NAME));
        } 
        
        public function get searchDocument():SearchDocument{  
            return viewComponent as SearchDocument; 
        } 

		override public function onRegister():void{
			super.onRegister();
			
			try{
				searchDocument.addEventListener(SearchDocument.SEARCH_ITEM, searchItem);
				searchDocument.addEventListener(SearchDocument.INITIALIZE_PAGE, initialize);
				searchDocument.addEventListener(SearchDocument.REPORT_GET, listReportId);
				searchDocument.addEventListener(MenuEvent.ITEM_CLICK, print);
			}catch(error:Error){
				Alert.show(error.message, "Error", Alert.OK, null, null, Utilities.errorMsg);
			}
			
			this.initialize(null);
    	}

		override public function onRemove():void{
			super.onRemove();
			
			try{
				searchDocument.removeEventListener(SearchDocument.SEARCH_ITEM, searchItem);
				searchDocument.removeEventListener(SearchDocument.INITIALIZE_PAGE, initialize);
				searchDocument.removeEventListener(SearchDocument.REPORT_GET, listReportId);
				searchDocument.removeEventListener(MenuEvent.ITEM_CLICK, print);
				this.clearFields();
				searchDocument.dtgPaging.stateInitial();
				setViewComponent(null);
			}catch(error:Error){
				Alert.show(error.message, "Error", Alert.OK, null, null, Utilities.errorMsg);
			}
		}
		
        override public function listNotificationInterests():Array{  
            return [NotificationList.SEARCH_DOCUMENT_LISTED,
            		NotificationList.BROKER_DOCUMENT_TYPE_LISTED,
            		NotificationList.CERTIFICATE_ENDORSEMENT_UNLOCK,
            		NotificationList.CERTIFICATE_POLICY_CHANGE,
            		NotificationList.CERTIFICATE_ENDORSEMENT_ANULLATION,
            		NotificationList.ENDORSEMENT_UNLOCKABLE_LISTED,
            		NotificationList.CERTIFICATE_EMISION_CANCELED,
            		NotificationList.GRIDVIEW_UPDATE_LIST,
            		NotificationList.RENEWAL_ALERT_LIST,
            		NotificationList.RENEWAL_ALERT_RESULT,
            		NotificationList.MODULE_FOUND,
            		NotificationList.REPORT_LISTED,
            		NotificationList.REPORT_GET];  
        } 		

        override public function handleNotification(notification:INotification):void{
            switch(notification.getName()){
            	case NotificationList.SEARCH_DOCUMENT_LISTED:
        			var result:Paginacao = notification.getBody() as Paginacao;
        			
        			searchDocument.dtgPaging.paginacao = result;
        			
        			if(searchDocument.dtgPaging.paginacao.totalDados > 0){
        				searchDocument.listSearchDocument = searchDocument.dtgPaging.paginacao.listaDados;
        			}else{
        				Utilities.warningMessage("dataNotFound");
						searchDocument.listSearchDocument = null;
        			}
		        	break;
              	case NotificationList.BROKER_DOCUMENT_TYPE_LISTED: 
		        	var documentTypeArrayResult: ArrayCollection = notification.getBody() as ArrayCollection;
		        	searchDocument.listDocumentType = documentTypeArrayResult;
                	break;
                case NotificationList.CERTIFICATE_POLICY_CHANGE:
                case NotificationList.CERTIFICATE_ENDORSEMENT_UNLOCK:
				case NotificationList.CERTIFICATE_ENDORSEMENT_ANULLATION:
                	var searchDocumentResult:SearchDocumentResult = notification.getBody() as SearchDocumentResult;
                	
        	        searchDocument.action = notification.getType();
        			searchDocument.notificationName = notification.getType();  

                	this.loadEndorsement(searchDocumentResult);
                	break;
                case NotificationList.ENDORSEMENT_UNLOCKABLE_LISTED:
                	var endorsementOptionResult:EndorsementOption = notification.getBody() as EndorsementOption;

       	        	switch(searchDocument.notificationName){
		        		case SearchDocument.UNLOCK_PROPOSAL:		        			
		        		case SearchDocument.ANULLMENT_PROPOSAL:
		        		case SearchDocument.CHANGE_POLICY_REGISTER:
		       				
							desktopInstance.makePartnerMenu = false;
							desktopInstance.proposal.returnModule = searchDocument.selectedModule;
							desktopInstance.currentView = DesktopViewsList.VIEW_PROPOSAL_MAIN;
							desktopInstance.proposal.endorsementOption = endorsementOptionResult;
							desktopInstance.proposal.searchDocumentParameters = searchDocument.searchDocumentParameters;
							
							endorsementOptionResult.action = endorsementAction( searchDocument.notificationName );
							
		        			if( searchDocument.notificationName == SearchDocument.CHANGE_POLICY_REGISTER )
		        				endorsementOptionResult.endorsement.installments.removeAll();					        				
							
							var wasRegistred:Boolean = ApplicationFacade.getInstance().registerOnlyNewMediator(new ProposalMediator(desktopInstance.proposal));
							
							if(!wasRegistred)desktopInstance.proposal.onInicializarPage();
		        			break;
		        		case SearchDocument.CHANGE_POLICY_TECHNICAL:
		        		case SearchDocument.RELOAD_QUOTE:
							desktopInstance.quoteMain.returnModule = searchDocument.selectedModule;
							desktopInstance.quoteMain.searchDocumentParameters = searchDocument.searchDocumentParameters;
							
							endorsementOptionResult.action = endorsementAction( searchDocument.notificationName );
							
							ApplicationFacade.getInstance().sendNotification(NotificationViewsList.VIEW_RELOAD_QUOTE, endorsementOptionResult);
		        			break;
       	        	}
								
					break;
	
				case NotificationList.GRIDVIEW_UPDATE_LIST:
					var pagingParameters:PagingParameters = notification.getBody() as PagingParameters;
					
					if(searchDocument.searchDocumentParameters == null){
						searchDocument.searchDocumentParameters = new SearchDocumentParameters();
					}
					
					searchDocument.searchDocumentParameters.page = pagingParameters.page;
					searchDocument.searchDocumentParameters.currentPage = pagingParameters.currentPage; 
					searchDocument.searchDocumentParameters.totalPage = pagingParameters.totalPage;
			
					searchDocumentProxy.listSearchDocumentPaging(searchDocument.searchDocumentParameters);
					break;
				case NotificationList.MODULE_FOUND:
					var moduleFound:Module = notification.getBody() as Module;
					searchDocument.search.enabled = true;
					searchDocument.allowedModules = moduleFound.modules; 
					break;
				case NotificationList.CERTIFICATE_EMISION_CANCELED:
					var resultEvent:ReturnValue = notification.getBody() as ReturnValue;
					if( resultEvent.success ) {
						searchDocument.justificationFormEmision.close();
						searchDocument.annulationMessage();
						searchDocumentProxy.listSearchDocumentPaging(searchDocument.searchDocumentParameters);
					} else {
						//searchDocument.justificationFormEmision.close();
						Utilities.directWarningMessage(resultEvent.messages);
				 	}
					break;
				case NotificationList.RENEWAL_ALERT_LIST:
                	var searchDocumentResult:SearchDocumentResult = notification.getBody() as SearchDocumentResult;
					searchDocumentProxy.listRenewalAlert(searchDocumentResult.contractId, searchDocumentResult.endorsementId, false);
					break;
				case NotificationList.RENEWAL_ALERT_RESULT:
					if (notification.getBody() instanceof ErrorList) {
						Utilities.validateEndorsementReturn(notification.getBody() as ErrorList, "warningRenewal");
					}
					else {
						showErrorMessage(notification.getBody());
					}
					break;
				case NotificationList.REPORT_LISTED:
					policyPrint(notification.getBody() as ArrayCollection);
					break;
				case NotificationList.REPORT_GET:
					searchDocument.print(notification.getBody().toString());
					break;
            }
        }
        
        private function endorsementAction( notificationName:String ):int
        {
        	var action:int = 0;
           	switch( notificationName ){
        		case SearchDocument.UNLOCK_PROPOSAL:
        			action = EndorsementOption.ACTION_UNLOCK;
        			break;
        		case SearchDocument.ANULLMENT_PROPOSAL:
        			action = EndorsementOption.ACTION_ANULLATION;
        			break;
        		case SearchDocument.CHANGE_POLICY_REGISTER:
        			action = EndorsementOption.ACTION_CHANGE_REGISTER;
        			break;
        		case SearchDocument.CHANGE_POLICY_TECHNICAL:
        			action = EndorsementOption.ACTION_CHANGE_TECHNICAL;
        			break;
        		case SearchDocument.RELOAD_QUOTE:
        			action = EndorsementOption.ACTION_EFFECTIVE;
        			break;
           	}
           	return action;
        }
        /*
        * inicialização do componente
        * 
        *  brokerProxy.listDocumentType : lista do tipo de documento (NotificationList.BROKER_DOCUMENT_TYPE_LISTED)
        * 
        */
        private function initialize(event:Event):void{
        	if(searchDocument.searchDocumentParameters == null){
	        	searchDocument.search.enabled = false;
	        	brokerProxy.listDocumentType(Constant.DOCUMENT_TYPE);
	        	moduleProxy.findById(searchDocument.selectedModule.moduleId);
        		this.clearFields();
        	}else{
	      		var itemClick:ItemClickEvent = new ItemClickEvent("itemClick");
        		
	        	searchDocument.search.enabled = false;
	        	brokerProxy.listDocumentType(Constant.DOCUMENT_TYPE);
	        	moduleProxy.findById(searchDocument.selectedModule.moduleId);
        		
        		if(searchDocument.searchDocumentParameters.contractId > 0){
        			searchDocument.txtContratId.text = searchDocument.searchDocumentParameters.contractId.toString();
        			itemClick.index = 0;
        			searchDocument.optionType.selectedValue = 1;
        		}
        		
        		if(searchDocument.searchDocumentParameters.certificateNumber != null && searchDocument.searchDocumentParameters.certificateNumber != ""
        			&& searchDocument.searchDocumentParameters.policyNumber != null && searchDocument.searchDocumentParameters.policyNumber != ""){
        			
        			searchDocument.txtCertificateNumber.text = searchDocument.searchDocumentParameters.certificateNumber;
        			searchDocument.txtPolicyNumber.text = searchDocument.searchDocumentParameters.policyNumber;
        			itemClick.index = 1;
        			searchDocument.optionType.selectedValue = 2;
        		}
        		
				if(searchDocument.searchDocumentParameters.name != null && searchDocument.searchDocumentParameters.name != ""){
					searchDocument.txtName.text = searchDocument.searchDocumentParameters.name;
					itemClick.index = 2;
					searchDocument.optionType.selectedValue = 4;
				}
        		
		    	if(searchDocument.searchDocumentParameters.documentType > 0 && searchDocument.searchDocumentParameters.documentNumber != null
		    		&& searchDocument.searchDocumentParameters.documentNumber != ""){
		    			
		    		searchDocument.cmbDocumentType.selectedValue = searchDocument.searchDocumentParameters.documentType;
		    		searchDocument.txtDocumentNumber.text = searchDocument.searchDocumentParameters.documentNumber;
		    		itemClick.index = 3;
		    		searchDocument.optionType.selectedValue = 3;
		    	}
		    	
		    	if (searchDocument.searchDocumentParameters.renewedOnly != null && searchDocument.searchDocumentParameters.renewedOnly)
		    	 	searchDocument.chkRenewedOnly.selected = true;
        		
        		searchDocument.enabledField(itemClick);
        		searchDocumentProxy.listSearchDocumentPaging(searchDocument.searchDocumentParameters);
        	}
        }
        
        /*
        * limpa e desabilita os campos da tela searchDocument.mxml
        *
        * 
        */
        private function clearFields():void{
        	searchDocument.txtCertificateNumber.text = "";
        	searchDocument.txtCertificateNumber.enabled = false;
        	searchDocument.txtPolicyNumber.text = "";
        	searchDocument.txtPolicyNumber.enabled = false;
        	searchDocument.txtContratId.text = "";
        	searchDocument.txtContratId.enabled = false;
        	searchDocument.txtDocumentNumber.text = "";
        	searchDocument.txtDocumentNumber.enabled = false;
        	searchDocument.txtName.text = "";
        	searchDocument.txtName.enabled = false;
        	searchDocument.cmbDocumentType.selectedIndex = -1;
        	searchDocument.cmbDocumentType.enabled = false;
        	searchDocument.chkRenewedOnly.selected = false;

        	if(searchDocument.optionType.selection != null){
        		searchDocument.optionType.selection.selected = false;
        	}
        	
        	if(searchDocument.listSearchDocument != null){
        		searchDocument.listSearchDocument.removeAll();
        		searchDocument.listSearchDocument.refresh();
        		searchDocument.dtgCertificateReport.data = null;
        		searchDocument.dtgCertificateReport.dataProvider = null;
        	}
        	
        	searchDocument.searchDocumentParameters = null;
        }
            
		/*
		 * busca os documentos conforme parametros selecionados
		 *
		 * searchDocumentProxy.listSearchDocument : lista de registros conforme parametros (NotificationList.SEARCH_DOCUMENT_LISTED)
		 * 
		 */  
        public function searchItem(event:Event):void{
        	var loggedUser : User = ApplicationFacade.getInstance().loggedUser;       	
        	searchDocument.searchDocumentParameters = new SearchDocumentParameters();
        	
    		searchDocument.searchDocumentParameters.partnerId = loggedUser.transientSelectedPartner;
        	searchDocument.searchDocumentParameters.contractId = int(searchDocument.txtContratId.text);
        	searchDocument.searchDocumentParameters.certificateNumber = searchDocument.txtCertificateNumber.text;
        	searchDocument.searchDocumentParameters.policyNumber = searchDocument.txtPolicyNumber.text;
        	searchDocument.searchDocumentParameters.name = searchDocument.txtName.text;
        	searchDocument.searchDocumentParameters.documentNumber = searchDocument.txtDocumentNumber.text;
        	searchDocument.searchDocumentParameters.renewedOnly = searchDocument.chkRenewedOnly.selected;
        	
        	if(searchDocument.cmbDocumentType.selectedItem != null){
        		searchDocument.searchDocumentParameters.documentType = (searchDocument.cmbDocumentType.selectedItem as DataOption).dataOptionId;
        	}
			
			searchDocument.dtgPaging.pagingParameters.page = 0;
			searchDocument.dtgPaging.pagingParameters.currentPage = 1;
			searchDocument.searchDocumentParameters.currentPage = searchDocument.dtgPaging.pagingParameters.currentPage; 
			searchDocument.searchDocumentParameters.page = searchDocument.dtgPaging.pagingParameters.page;
			searchDocument.searchDocumentParameters.totalPage = searchDocument.dtgPaging.pagingParameters.totalPage;
			
			searchDocumentProxy.listSearchDocumentPaging(searchDocument.searchDocumentParameters);
        }
        
        /*
        * retorna o objeto endorsementOption
        *
        * proposalProxy.loadUnlockabeEndorsement : retorna o objeto endorsement do registro selecionado (NotificationList.ENDORSEMENT_UNLOCKABLE_LISTED)
        *
        */
      	private function loadEndorsement(searchDocumentResult:SearchDocumentResult):void{
      		if(searchDocumentResult != null){
      			proposalProxy.loadUnlockabeEndorsement(searchDocumentResult.endorsementId, searchDocumentResult.contractId);
      		}
      	}
      	
      	private function loadAnullationForm(searchDocumentResult:SearchDocumentResult):void{
			Alert.show("Carregar formulario...");
      	}         
      	
      	/**
		 * Show Error Message
		 * @param object body received
		 */
		private function showErrorMessage(object: Object): void {
			if (object instanceof FaultEvent) {
				var faultEvent: FaultEvent = (object as FaultEvent);
				ErrorWindow.createAndShowError(Errors.INTERNAL_ERROR, faultEvent.fault.getStackTrace());
			}
			else {
				Utilities.errorMessage(Errors.INTERNAL_ERROR);
			}
		}

		private function policyPrint(result:ArrayCollection):void {
			if(result && result.length > 1) {
				searchDocument.addPrintMenu(result);
			} else {
				getReportId(searchDocument.searchDocumentResult.contractId, searchDocument.searchDocumentResult.endorsementId);
			}
		}

		private function listReportId(evt:Event):void{
			reportProxy.listReportId(searchDocument.searchDocumentResult.contractId, searchDocument.searchDocumentResult.endorsementId);
		}

		private function print(evt:MenuEvent):void {
  	  		var reporte:Report = evt.item as Report;
  	  		if(reporte != null){
				searchDocument.print(reporte.reportId.toString());
  	  		}
		}

		private function getReportId(contractId:int, endorsementId:int):void{
			reportProxy.getReportId(contractId, endorsementId, Constant.REPORT_TYPE_CERTIFICATE);
		}
	}
}