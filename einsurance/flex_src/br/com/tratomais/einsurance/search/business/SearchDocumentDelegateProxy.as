////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Bruno Souza
// @version 1.0
// @lastModified 21/09/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.search.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	import br.com.tratomais.einsurance.search.model.vo.SearchDocumentParameters;
	
	import mx.rpc.IResponder;
	
	public class SearchDocumentDelegateProxy
	{
		private var responder : IResponder;
		private var service : Object;
				
		public function SearchDocumentDelegateProxy(pResponder : IResponder )
		{
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_SEARCH_DOCUMENT);
			responder = pResponder;
		}
		
		public function listSearchDocument( parameters:SearchDocumentParameters ):void
		{
			var call:Object = service.listSearchDocument( parameters );
			call.addResponder(responder);
		}	
		
		public function listSearchDocumentPaging( parameters:SearchDocumentParameters ):void
		{
			var call:Object = service.listSearchDocumentPaging( parameters );
			call.addResponder(responder);
		}
		
   		public function policyCancellation( contractId:int, issuanceType:int, cancelDate:Date, cancelReason:int ) : void {
			var call : Object = service.policyCancellation( contractId, issuanceType, cancelDate, cancelReason );
			call.addResponder(responder);
   		}

		public function listRenewalAlert(contractId:int, endorsementId:int, resolved:Boolean) : void {
			var call : Object = service.listRenewalAlert(contractId, endorsementId, resolved);
			call.addResponder(responder);
   		}
	}
}