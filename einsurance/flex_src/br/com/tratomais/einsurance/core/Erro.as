package br.com.tratomais.einsurance.core  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.service.erros.Erro")]   
    [Managed]  
    public class Erro extends HibernateBean 
    {  
        private var _codigo:int;  
        private var _descricao:String;  
        private var _local:Class;  
        private var _tipoErro:int;  
        private var _origem:String;  
        private var _nrSeq:int;  
        private var _numItem:int;  
  
        public function Erro()  
        {  
        }  
  
        public function get codigo():int{  
            return _codigo;  
        }  
  
        public function set codigo(pData:int):void{  
            _codigo=pData;  
        }  
  
        public function get descricao():String{  
            return _descricao;  
        }  
  
        public function set descricao(pData:String):void{  
            _descricao=pData;  
        }  
  
        public function get local():Class{  
            return _local;  
        }  
  
        public function set local(pData:Class):void{  
            _local=pData;  
        }  
  
        public function get tipoErro():int{  
            return _tipoErro;  
        }  
  
        public function set tipoErro(pData:int):void{  
            _tipoErro=pData;  
        }  
  
        public function get origem():String{  
            return _origem;  
        }  
  
        public function set origem(pData:String):void{  
            _origem=pData;  
        }  
  
        public function get nrSeq():int{  
            return _nrSeq;  
        }  
  
        public function set nrSeq(pData:int):void{  
            _nrSeq=pData;  
        }  
  
        public function get numItem():int{  
            return _numItem;  
        }  
  
        public function set numItem(pData:int):void{  
            _numItem=pData;  
        }  
    }  
} 