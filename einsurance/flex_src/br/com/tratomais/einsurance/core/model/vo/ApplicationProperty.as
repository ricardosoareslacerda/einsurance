package br.com.tratomais.einsurance.core.model.vo
{
	import br.com.tratomais.einsurance.core.Utilities;

	import net.digitalprimates.persistence.hibernate.HibernateBean;

	[RemoteClass(alias="br.com.tratomais.core.model.ApplicationProperty")]
	[Managed]
	public class ApplicationProperty extends HibernateBean
	{
		/**
		 * Constantes
		 */
		public static const AUTHORIZATION_TYPE_COINSURANCE:String = 'authorizationTypeCoinsurance';
		public static const AUTHORIZATION_TYPE_GLOBAL_LIVES:String = 'authorizationTypeGlobalLives';
		public static const AUTHORIZATION_TYPE_INSURED_PERCENT:String = 'authorizationTypeInsuredPercent';
		public static const AUTHORIZATION_TYPE_INSURED_PERCENT_ALL:String = "authorizationTypeInsuredPercentAll";
		public static const AUTHORIZATION_TYPE_LABOUR:String = 'authorizationTypeLabour';
		public static const AUTHORIZATION_TYPE_LIMIT_VALUES:String = 'authorizationTypeLimitValues';
		public static const AUTHORIZATION_TYPE_PREMIUM_INFORMED:String = 'authorizationTypePremiumInformed';
		public static const AUTHORIZATION_TYPE_TECHNICAL_SURPLUS:String = 'authorizationTypeTechnicalSurplus';

		public static const CUSTOMER_CODE_CLAUSE:String = 'customerCodeClause';

		public static const EXCLUDE_SALE_PRODUCT:String = 'excludeSaleProduct';
		public static const EXCLUDE_VIEW_PRODUCT:String = 'excludeViewProduct';

		public static const FREE_PENDENCY_PROTOCOL:String = 'freePendencyProtocol';
		public static const FREE_PENDENCY_SHIPPING:String = 'freePendencyShipping';

		public static const INSURED_ACTIVITY_DEFAULT_RISK:String = 'insuredActivityDefaultRisk';

		public static const LOGIN_ATTEMPT:String = 'loginAttempt';
		public static const LOGIN_UNLOCK_MINUTES:String = 'loginUnlockMinutes';

		public static const MAXIMUM_QUANTITY_OF_AGENCY:String = 'maximumQuantityOfAgency';
		public static const MAXIMUM_QUANTITY_OF_BROKERAGE:String = 'maximumQuantityOfBrokerage';

		public static const PASSWORD_EXPIRE_DAYS:String = 'passwordExpireDays';
		public static const PASSWORD_RULES:String = 'passwordRules';
		public static const PASSWORD_RULES_ERROR_MESSAGE:String = 'passwordRulesErrorMessage';
		public static const PATH_UPLOAD_FILE:String = 'PathUploadFile';
		public static const PAYMENT_GROUPED_CLAUSE:String = 'paymentGroupedClause';
		public static const PERMIT_CHANGE_COVERAGE_PLAN:String = 'permitChangeCoveragePlan';
		public static const PRODUCTS_NOT_INFORM_LIMIT_VALUES:String = 'productsNotInformLimitValues';

		public static const ROADMAP_ACTIVE:String = 'roadmapActive';

		public static const SEQUENCE_FILE_NUMBER:String = 'SequenceFileNumber';
		public static const SINGLE_RISK_PRODUCT:String = 'singleRiskProduct';
		public static const SYSTEM_VERSION:String = 'SystemVersion';

		public static const TOLERANCE_TO_INSURED_CARD_EXPIRATION:String = 'ToleranceToInsuredCardExpiration';
		public static const TOLERANCE_TO_PAYMENT_CARD_EXPIRATION:String = 'ToleranceToPaymentCardExpiration';

		public static const WARNING_TO_EXPIRE_THE_PASSWORD:String = 'WarningToExpireThePassword';

		/**
		 * Variables
		 */
		private var _id:ApplicationPropertyID;
		private var _value:String;
		private var _registred:Date;
		private var _updated:Date;

		public function ApplicationProperty(id:ApplicationPropertyID = null){
			_id = id;
		}

		public function get id():ApplicationPropertyID{
			return _id;
		}

		public function set id(pData:ApplicationPropertyID):void{
			_id = pData;
		}

		public function get value():String{
			return _value;
		}

		public function set value(pData:String):void{
			_value = pData;
		}

		public function get registred():Date{
			return _registred;
		}

		public function set registred(pData:Date):void{
			_registred = Utilities.convertTimeZone(pData);
		}

		public function get updated():Date{
			return _updated;
		}

		public function set updated(pData:Date):void{
			_updated = Utilities.convertTimeZone(pData);
		}
	}
}