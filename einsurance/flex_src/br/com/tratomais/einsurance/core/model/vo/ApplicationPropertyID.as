package br.com.tratomais.einsurance.core.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;

	[RemoteClass(alias="br.com.tratomais.core.model.ApplicationPropertyID")]
	[Managed]
	public class ApplicationPropertyID extends HibernateBean
	{
		private var _applicationId:int;
		private var _name:String;

		public function ApplicationPropertyID(applicationId:int = 0, name:String = null){
			_applicationId = applicationId;
			_name = name;
		}

		public function get applicationId():int{
			return _applicationId;
		}

		public function set applicationId(pData:int):void{
			_applicationId = pData;
		}

		public function get name():String{
			return _name;
		}

		public function set name(pData:String):void{
			_name = pData;
		}
	}
}