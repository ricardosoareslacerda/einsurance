package br.com.tratomais.einsurance.core
{
	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.components.CityCombo.view.component.CityCombo;
	import br.com.tratomais.einsurance.core.components.CountryCombo.view.component.CountryCombo;
	import br.com.tratomais.einsurance.core.components.CustomComboBox;
	import br.com.tratomais.einsurance.core.components.MatchComboBox;
	import br.com.tratomais.einsurance.core.components.StateCombo.view.component.StateCombo;
	import br.com.tratomais.einsurance.core.components.advancedAutoComplete.components.AdvancedAutoComplete;
	import br.com.tratomais.einsurance.core.components.maskedDateField.classes.MaskedDateField;
	import br.com.tratomais.einsurance.useradm.model.vo.Functionality;
	
	import flexlib.containers.WindowShade;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.containers.Box;
	import mx.containers.Form;
	import mx.containers.FormItem;
	import mx.containers.HBox;
	import mx.containers.HDividedBox;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.NumericStepper;
	import mx.controls.TextInput;
	import mx.core.UIComponent;
	import mx.events.ValidationResultEvent;
	import mx.formatters.DateFormatter;
	import mx.formatters.NumberFormatter;
	import mx.resources.ResourceManager;
	import mx.rpc.events.FaultEvent;
	import mx.validators.Validator;

	public class Utilities
	{		
		public function Utilities()
		{
		}
	
		[Bindable]
		[Embed(source="/assets/images/error.png")]
		public static var errorMsg:Class;		
			
		[Bindable]
		[Embed(source="/assets/images/warning.png")]
		public static var warningMsg:Class;

		[Bindable]
		[Embed(source="/assets/images/success.png")]
		public static var successMsg:Class;

		[Bindable]
		[Embed(source="/assets/images/question.png")]
		public static var questionMsg:Class;
		
		public static const MILLISECOND:Number = 1;
		public static const SECOND:Number = MILLISECOND * 1000;
		public static const MINUTE:Number = SECOND * 60;
		public static const HOUR:Number = MINUTE * 60;
		public static const DAY:Number = HOUR * 24;	
		public static const YEAR:Number = DAY * 365;
		
		public static const ONLY_NUMBER:String = "0-9";
		public static const ONLY_TEXT_AND_NUMBER:String = "[a-zA-Z0-9]";
		public static const ONLY_TEXT:String = "^0-9";
		public static const ONLY_TEXT_NO_SPECIAL:String = "^0-9`~!@#$%&*()-_+=[]{}\\|;:/?>.<,\^";
		public static const ONLY_TEXT_LOGIN:String = "[a-z0-9]\\._";

		// formatacao cpf / cnpj
		private static var MASK_CNPJ:RegExp = /^\d{2}.\d{3}.\d{3}\/\d{4}-\d{2}$/;
		private static var MASK_CPF:RegExp = /^\d{3}.\d{3}.\d{3}-\d{2}$/;
		private static var MASK_CEP:RegExp = /^([\d]{2})([\d]{3})([\d]{3})|^[\d]{2}[\d]{3}-[\d]{3}/;
		private static var LENGTH_CPF:int = 11;
		private static var LENGTH_CNPJ:int = 14;

		public static const MASKED_TEXT_INPUT_MASK_CPF:String = "###/.###/.###-##";
		public static const MASKED_TEXT_INPUT_MASK_CNPJ:String = "##/.###/.###//####-##";
		public static const MASKED_TEXT_INPUT_DEFAULT_CHARACTER:String = "_";

		public static function getDateByString( value:String ):Date{
			return new Date((value.substr(3,2) + "/" + value.substr(0,2) + "/"  +  value.substr(6,4)).toString());
		}
		
		public static function getStringByDate(value:Date):String{
			var dateFormatter:DateFormatter = new DateFormatter();
				dateFormatter.formatString = ResourceManager.getInstance().getString('resources', 'dateMask');
			return (dateFormatter.format(value).toString());
		}
		
		public static function getDateExpireDate(today : Date, term : int) : Date{
			if (term == 1) {
				var dateExpireDate : Date = new Date();
				dateExpireDate.fullYear = dateExpireDate.getFullYear() + 1;
				return dateExpireDate;
			}
			return today;
		}
		
		public static function formatValue(value:Number, rounding:String = "up", precision:int = 2):String{
			var number : NumberFormatter = new NumberFormatter();
				number.precision = precision;
				number.rounding = rounding;
			number.decimalSeparatorFrom = ResourceManager.getInstance().getString('resources', 'decimalSeparator');
			number.decimalSeparatorTo = ResourceManager.getInstance().getString('resources', 'decimalSeparator');
			number.thousandsSeparatorFrom = ResourceManager.getInstance().getString('resources', 'thousandsSeparator');
			number.thousandsSeparatorTo = ResourceManager.getInstance().getString('resources', 'thousandsSeparator');
			return number.format(value);
		}
		
		public static function formatValuePrecision(value : Number, precision:int) : String{
			var number : NumberFormatter = new NumberFormatter();
			number.precision = precision;
			number.useThousandsSeparator = true;
			number.decimalSeparatorFrom = ResourceManager.getInstance().getString('resources', 'decimalSeparator');
			number.decimalSeparatorTo = ResourceManager.getInstance().getString('resources', 'decimalSeparator');
			number.thousandsSeparatorFrom = ResourceManager.getInstance().getString('resources', 'thousandsSeparator');
			number.thousandsSeparatorTo = ResourceManager.getInstance().getString('resources', 'thousandsSeparator');
			return number.format(value);
		}
		
		public static function formatNumber(textInput:TextInput, precision:int ):void{
			var strFormatNumber:String;
			var numberFormat : NumberFormatter = new NumberFormatter();
			numberFormat.precision = precision;
			numberFormat.useThousandsSeparator = false;
			numberFormat.decimalSeparatorTo = ".";
			numberFormat.useNegativeSign = true;
			if(textInput.text != ""){
				textInput.text = numberFormat.format(textInput.text);
			}
		} 
		
		public static function sortCollection (arrayCollection : ArrayCollection, nameColumnObject : String, isNumeric : Boolean) : void{
			var dataSortField : SortField = new SortField();
	   		dataSortField.name = nameColumnObject;
			dataSortField.numeric = isNumeric;
			dataSortField.caseInsensitive = true;
			dataSortField.descending = false;

			var stringDataSort : Sort = new Sort();
			stringDataSort.fields = [dataSortField];

			arrayCollection.sort = stringDataSort;
			arrayCollection.refresh();
		}
		
        public static function validateFormCollection(validatorArr:ArrayCollection):Boolean {
        	var array : Array = new Array();
        	for each (var validator : Validator in validatorArr){
        		array.push(validator);
        	}
        	return validateForm(array, null);
        }
		
        public static function validateForm(validatorArr:Array, formName:String):Boolean {
            var validatorErrorArray:Array = Validator.validateAll(validatorArr);
        	var isValid:Boolean = validatorErrorArray.length == 0;
            if (isValid == false){
                var validationResult:ValidationResultEvent;
                var errorMessageArray:Array = [];
                
                for each (validationResult in validatorErrorArray) {
                    var fieldLabel:String = null;
                    var field = validationResult.currentTarget.source.parent;

                	(field as UIComponent).errorString = validationResult.message.toString();
                	
                	if(field as FormItem){
                		fieldLabel = FormItem(field).label;
                	
                	}else if(field as HDividedBox ){
                		fieldLabel = HDividedBox(field).label;
                	
                	}else if(field as HBox){
                		fieldLabel = Box(field as Box).label;
                	}
                	
                	redBorderField(validationResult.currentTarget.source, true);
                    errorMessageArray.push(fieldLabel + " " + validationResult.message);
                }
                Alert.show(errorMessageArray.join("\n\n"), ResourceManager.getInstance().getString("messages", "warningTitle", [formName]), Alert.OK, null, null, errorMsg);
            }
            return isValid;
        }
        
        public static function validateEndorsementReturn(errorList:ErrorList, title:String="warning"):Boolean {
        	var isValid : Boolean = true;
        	if(errorList.listaErros != null && errorList.listaErros.length > 0){
 				var errorMessageArray:Array = [];
        		for each (var err: Erro in errorList.listaErros) {
                    errorMessageArray.push(err.codigo + " : " + err.descricao);
                }
                Alert.show(errorMessageArray.join("\n\n"), ResourceManager.getInstance().getString("messages", title), Alert.OK, null, null, warningMsg);
        		isValid = false;
        	}
        	return isValid;
        }
        
        public static function sumDateYear(date:Date, years:int):Date{
        	if( date && years > 1000 && years < 9999){
        		date = new Date(date.getFullYear() + years);
        		return date;
        	}
        	
        	return null;
        }
        
        public static function sumDateMonth(date:Date, months:int):Date{
        	if( date && months > -1 ){
        		date = new Date(date.getFullYear(), date.getMonth()+ months);
        		return date;
        	}
        	
        	return null;
        }
         
        public static function sumDateDay(date:Date, days:int):Date{
        	if( date && days > 0 ){
        		date = new Date(date.getFullYear(), date.getMonth(), date.getDate() + days);
        		return date;
        	}
        	
        	return null;
        }
        
          public static function validateRangeDate(date : Date):Boolean {
          		if(date == null){
          			return false;
          		}
          		if(date.fullYear < 1901){
          			return false;
          		}
			  	var maxDate:Date = new Date();
			  	if (maxDate != null) {
			  		maxDate = new Date(maxDate.fullYear,maxDate.month, maxDate.date);
			  		if (date > maxDate) {
			  			return false
			  		};
			  	}
		  	return true;
		}
		
		public static function differenceOfDaysBetweenDates(date1:Date, date2:Date):Number{
			var milis : Number = Math.abs(date1.getTime() - date2.getTime());
			var days : Number = milis / DAY;
			return days;	
		}
		
		public static function enabledFields( pData:ArrayCollection, enabled:Boolean = true ):void
	    {
	    	if ( pData )
	    	{
		    	for (var i:int = 0; i < pData.length; i++ )
		    		pData.getItemAt(i).enabled = enabled;
		    }
	    }
		
		public static function validRangeDateByYear(minimumAge:Number, maximumAge:Number, date:Date, dateRef:Date ):Boolean{
			var _minimumAge:Number = minimumAge;
			var _maximumAge:Number = maximumAge;
			var minimumDatePart:String = DATE_PART_YEAR;
			var maximumDatePart:String = DATE_PART_YEAR;
			var differenceDays:Number = int(differenceOfDaysBetweenDates(dateRef, date));
			
			if(minimumAge != 0){
			
				if(minimumAge < 1){
					var _minimumAge:Number = (minimumAge * 12);
					var minimumDatePart:String = DATE_PART_MONTH;
				}		
				
				var _previousDateMinimumAge:Date = dateAdd( minimumDatePart, -_minimumAge, dateRef );
				var minimumAgeDays:Number = int(differenceOfDaysBetweenDates(dateRef, _previousDateMinimumAge));				
			}
			
			if(maximumAge != 0){
				
				if(maximumAge < 1){
					var _maximumAge:Number = (maximumAge * 12);
					var maximumDatePart:String = DATE_PART_MONTH;
				}	
				
				var _previousDateMaximumAge:Date = dateAdd( maximumDatePart, -_maximumAge, dateRef );
				var maximumAgeDays:Number = int(differenceOfDaysBetweenDates(dateRef, _previousDateMaximumAge));			
			}
						
			if((differenceDays < minimumAgeDays && minimumAge != 0) || (differenceDays > maximumAgeDays && maximumAge != 0)){
				return false;
			}
			return true;
		}

		/**
		 * Adds the specified number of "date parts" to a date, e.g. 6 days
		 * 
		 * @param datePart	The part of the date that will be added
		 * @param number	The total number of "dateParts" to add to the date
		 * @param date		The date on which to add
		 * 
		 * @return			The new date
		 * 
		 * http://flexoop.com/2008/12/flex-dateutils-dateadd/
		 */	 
		public static const DATE_PART_YEAR:String			= "fullYear";
		public static const DATE_PART_MONTH:String			= "month";
		public static const DATE_PART_WEEK:String			= "week";
		public static const DATE_PART_DAY_OF_MONTH:String	= "date";
		public static const DATE_PART_HOURS:String			= "hours";
		public static const DATE_PART_MINUTES:String		= "minutes";
		public static const DATE_PART_SECONDS:String		= "seconds";
		public static const DATE_PART_MILLISECONDS:String	= "milliseconds";
		public static const DATE_PART_DAY_OF_WEEK:String	= "day"; 
		 
		public static function dateAdd( datePart:String, number:Number, date:Date ):Date {
			var _returnDate:Date = new Date( date );
		 
			switch ( datePart ) {
				case DATE_PART_YEAR:
				case DATE_PART_MONTH:
				case DATE_PART_DAY_OF_MONTH:
				case DATE_PART_HOURS:
				case DATE_PART_MINUTES:
				case DATE_PART_SECONDS:
				case DATE_PART_MILLISECONDS:
					_returnDate[ datePart ] += number;
					break;
				case DATE_PART_DAY_OF_WEEK:
					_returnDate[ DATE_PART_DAY_OF_MONTH ] += number * 7;
					break;
				default:
					/* Unknown date part, do nothing. */
					break;
			}
			return _returnDate;
		}			
		
		/**
		* This function will pad the left or right side of any variable passed in
		* elem [AS object]
		* padChar: String
		* finalLength: Number
		* dir: String
		*
		* return String
		*/
		public static function padValue(elem:Object, padChar:String, finalLength:Number, dir:String):String
		{
		  //make sure the direction is in lowercase
		  dir = dir.toLowerCase();
		
		  //store the elem length
		  var elemLen = elem.toString().length;
		
		  //check the length for escape clause
		  if(elemLen == finalLength)
		  {
		    return elem.toString();
		  }
		
		  //pad the value
		  switch(dir)
		  {
		    default:
		    case 'l':
		      return padValue(padChar + elem, padChar, finalLength, dir);
		      break;
		    case 'r':
		      return padValue(elem + padChar, padChar, finalLength, dir);
		      break;
		  }
		}

		public static const URL_BASE = "/einsurance";
		public static const URL_BASE_REPORT = "/einsuranceReport";
		public static const URL_REPORT = "/viewReport?";
		public static const REPORT_EXCEL = "EXCEL";
		public static const REPORT_PDF = "PDF";		

		public static function resetForm(form:Form):void{
		    var formItens:Array = form.getChildren();
		    for (var i:int = 0; i < formItens.length; i++)
		    {
		        var formItem:Object = formItens[i].getChildren();
		        if(formItem.length > 0){
		        	for(var a:int=0; a < formItem.length; a++){
				      	switch(formItem[a].className){
				      		case "TextInput":
				      		case "TextArea":
				      		case "MasterTextInput":
				      		case "Text":
				      		 	formItem[a].text = '';
				      		 	formItem[a].htmlText = '';
				      		 	break;
							case "ComboBox":
							case "MatchComboBox":
							case "CustomComboBox":
							case "CountryCombo":
								formItem[a].selectedIndex = -1;
								break;
							case "StateCombo":
							case "CityCombo":
								formItem[a].selectedItemId = 0;
								break;
							case "NumericStepper":
								formItem[a].value = 0;
								break;
							case "CheckBox":
								formItem[a].selected = true;
								break;
							case "MaskedDateField":
								formItem[a].selectedDate = null;
								if(formItem[a].newDate)
									formItem[a].text = '';
								break;
							case "AdvancedAutoComplete":
								formItem[a].selectedItem = null;
								break;
				      	}
		        		redBorderField(formItem[a], false);
					}
		        }
		    }		
		}

		public static function resetField(fields:Array):void{
			if(!fields)
				return;
			
			for(var i:int = 0; i < fields.length; i++){
				switch(fields[i].className){
					case "TextInputFilter":
						fields[i].listMenu = new ArrayCollection;
					case "MaskedTextInput":
					case "Label":
					case "TextInput":
					case "TextInputRenderer":
					case "TextArea":
					case "Text":
						fields[i].htmlText = '';
						fields[i].text = '';
						break;
					case "CustomComboBox":
						fields[i].selectedValue = null;
					case "ComboBox":
					case "MatchComboBox":
					case "CountryCombo":
						fields[i].selectedIndex = -1;
						break;
					case "StateCombo":
					case "CityCombo":
						fields[i].selectedItemId = 0;
						break;
					case "NumericStepper":
						fields[i].value = 0;
						break;
					case "CheckBox":
						fields[i].selected = true;
						break;
					case "MaskedDateField":
						fields[i].selectedDate = null;
						if(fields[i].newDate)
							fields[i].text = '';
						break;
					case "AdvancedAutoComplete":
						fields[i].selectedItem = null;
						break;
					case "MasterTextInput":
						fields[i].clear();
						break;
				}
				redBorderField(fields[i], false);
			}
		}
		
		public static function enabledForm(form:Form, pValue:Boolean):void{
		    var formItens:Array = form.getChildren();
		    for (var i:int = 0; i < formItens.length; i++)
		    {
		        var formItem:Object = formItens[i].getChildren();
		        if(formItem.length > 0){
		        	for(var a:int=0; a < formItem.length; a++){
				      	switch(formItem[a].className){
				      		case "TextInput": 
				      		case "TextArea":
				      		case "MasterTextInput":
				      		 	formItem[a].enabled = pValue;
				      		 	break;
							case "ComboBox":
							case "MatchComboBox":
							case "CustomComboBox":
							case "CountryCombo":
							case "StateCombo":
							case "CityCombo":
								formItem[a].enabled = pValue;
								break;
							case "NumericStepper":
								formItem[a].enabled = pValue;
								break;
							case "CheckBox":
								formItem[a].enabled = pValue;
								break;
							case "MaskedDateField":
								formItem[a].enabled = pValue;
								break;
							case "AdvancedAutoComplete":
								formItem[a].enabled = pValue;
								break;													
				      	}
		        	}
				}
		    }
		}
		
		

		public static function warningMessage(property : String, parameters:Array = null) : void{
			Alert.show(ResourceManager.getInstance().getString("messages", property, parameters), 
					   ResourceManager.getInstance().getString("messages","warning"), Alert.OK, null, null, warningMsg);
		}

		public static function errorMessage(property : String, parameters:Array = null) : void{
			Alert.show(ResourceManager.getInstance().getString("messages", property, parameters),"Error", Alert.OK, null, null, errorMsg);
		}
	
		public static function ageMessage(property:String, personalRiskType:String, minimumAge:Number, maximumAge:Number):void{
			var parameters:Array = new Array();		
			var message:String;
			
			//type message
			if(minimumAge == 0){
				message = "menor que ";
			}else if(maximumAge == 0){
				message = "maior que ";
			}else{
				message = "entre ";
			}
			
			//age type
			if(minimumAge > 0 && minimumAge < 1){
				var _minimumAge:Number = (minimumAge * 12);
				message += _minimumAge.toFixed(0) + " meses";	
			}else if(minimumAge > 0){
				message += minimumAge + " anos";
			}
			
			if(minimumAge > 0 && maximumAge > 0){
				message += " e ";
			}
			
			if(maximumAge > 0 && maximumAge < 1){
				var _maximumAge:Number = (maximumAge * 12);
				message += _maximumAge.toFixed(0) + " meses";
			}else if(maximumAge > 0){
				message += maximumAge + " anos";
			}
			
			parameters.push(personalRiskType);
			parameters.push(message);
			
			Alert.show(ResourceManager.getInstance().getString("messages", property, parameters), 
					   ResourceManager.getInstance().getString("messages","warning"), Alert.OK, null, null, warningMsg);			
		}
		
		/**
		 * Shows a direct Error Message, without looking in resources archieve
		 * @param message Message to be shown
		 */
		public static function directErrorMessage(message : String) : void{
			Alert.show(message,"Error", Alert.OK, null, null, errorMsg);
		}

		/**
		 * Shows a direct Warning Message, without looking in resources archieve
		 * @param message Message to be shown
		 */
		public static function directWarningMessage(message : String) : void{
			Alert.show(message,ResourceManager.getInstance().getString("messages","warning"), Alert.OK, null, null, warningMsg);
		}
				
		public static function infoMessage(property : String, parameters:Array = null) : void{
			Alert.show(ResourceManager.getInstance().getString("messages", property, parameters), 
			   		   ResourceManager.getInstance().getString("messages","info"), Alert.OK, null, null, warningMsg);				
		}
		
		public static function successMessage(property : String, parameters:Array = null) : void{
		    Alert.show(ResourceManager.getInstance().getString("messages", property, parameters),"", Alert.OK, null, null, successMsg);
   		}
		
		public static function initializeList(list : ArrayCollection) : ArrayCollection{
			if(list == null || list.length == 0){
				list = new ArrayCollection();
			}
			return list;
		}
		
		public static function decoratorDocumentNumber ( externalCode : String, documentNumber : String) : String{
			var document:String = "";
			
			if(externalCode != ""){
				document = externalCode + "-" + documentNumber;
			}
			return document;
		}
				
		/**
		 * Method that process general message.
		 * 
		 * @param faultEvent FaultEvent trigger
		 * 
		 * Return true for message processed
		 * Return false for unprocessed messages
		 * **/
		public static function processFaultEvent(faultEvent : FaultEvent) : Boolean{
			
			if(faultEvent.fault.faultString.indexOf( "sessionExpired", 0) != -1 ){
				Alert.show(ResourceManager.getInstance().getString("messages","expiredSession"), "Atenção", Alert.OK, null, null, warningMsg);
				ApplicationFacade.getInstance().sendNotification(NotificationList.LOGOUT);
				return true;
			}
			return false;
		}
		
		/**
		 *  Method that searches for the name
		 * 
		 * @param list list of dataOption
		 * @param searchValue identifier
		 * @param fieldDescription default is dataOptionId
		 * @param fieldValue default is fieldDescription
		 * 
		 * @return the description or null for searchValue not found
		 * **/
		public static function getOptionDescription(list : ArrayCollection, searchValue : Number, fieldValue : String = "dataOptionId", fieldDescription: String  = "fieldDescription") : String{
			if(list != null){
				for each( var object : Object in list){
					if(object.hasOwnProperty(fieldValue) && Number(object[fieldValue]) == searchValue){
						return object[fieldDescription];
					}
				}
			}
			return null;
		}

		/**
		 *  Method that searches for the name
		 * 
		 * @param list list of dataOption
		 * @param searchValue identifier
		 * @param fieldDescription default is dataOptionId
		 * @param fieldValue default is name
		 * 
		 * @return the description or null for searchValue not found
		 * **/
		public static function getCompositeIdOptionDescription(list : ArrayCollection, searchValue : Number, fieldValue : String, fieldDescription: String  = "name") : String{
			if(list != null){
				for each( var object : Object in list){
					if(object.hasOwnProperty(fieldValue) && Number((object["id"])[fieldValue]) == searchValue){
						return object[fieldDescription];
					}
				}
			}
			return null;
		}
		
		public static function StringToNumber(valor:String, separadorMilhar: String,  separadorDecimal: String):Number {
			var valorTratado: String, caracter: String;
			var aux: int;
			for (aux=0;aux<valorTratado.length; aux++){
				caracter = valor.charAt(aux);
				switch(caracter) {
				case separadorDecimal:
					valorTratado += '.';
					break;
				case separadorMilhar:
					valorTratado += ',';
					break;	
				default:
					valorTratado += caracter;
				} 
			}
			var retorno:Number = new Number(valorTratado);
			if (isNaN(retorno))
				retorno = null;
			return retorno;
		}
 	
 		/**
 		 * 
 		 * @author leo.costa
         *
 		 * Método que valida a conta corrente de acordo com a regra do mod 11
 		 * @param accountNumber numero completo da conta com 20 caracteres
 		 * 
 		 * @return 	true 	para valido
 		 * 			false 	para invalido
 		 * 
 		 * **/
		public static function validateBankAccount(accountNumber : String, bankNumber: int) : Boolean{
			if(accountNumber == null || accountNumber.length != 20){
				return false
			}
			var codBco: String = accountNumber.substring(0,4).toString();
			var codAgencia: String = accountNumber.substring(4,8).toString();
			var codDigito: String = accountNumber.substring(8,10).toString();
			var codConta: String = accountNumber.substring(10,accountNumber.length).toString();
			
			if(bankNumber != int(codBco)){
				return false;
			}
			
			var primerDigit : String = calculoModulo11( "000000" + codBco + codAgencia);
			var secondDigit : String = calculoModulo11( codAgencia + codConta);
			
			if(primerDigit != null && secondDigit != null){
				if((primerDigit.toString() + secondDigit.toString()) == codDigito){
					return true;
				}
			}

			return false
		}
	
	 	/**
 		 * 
 		 * @author leo.costa
         *
 		 * Método que calcula o digito verificador de acordo com o calculoModulo11
 		 * @param accountNumber conta com 14 caracteres
 		 * @return 	true 	para valido
 		 * 			false 	para invalido
 		 * 
 		 * **/
		private static function calculoModulo11(accountNumber: String) : String{
			if(accountNumber == null || accountNumber.length != 14){
				return null;
			}
			var peso14: String = "23456723456723";
		    var progressivo : int = 0;
		    var divisor : int = 0;
		    var digito : String;
		    
		    
		    for (var regressivo:int = peso14.length - 1; regressivo >= 0; regressivo = regressivo - 1) {
				var valueProgressivo:String = ""+accountNumber.charAt(regressivo);
				var valueRegressivo:String = ""+peso14.charAt(progressivo);
				divisor = divisor +  (int( valueRegressivo ) * int(valueProgressivo ) );
				progressivo = progressivo + 1;
			}
		    
		    var resto: int = divisor % 11;
		    var c = 11 - resto;
		    if(c > 9){
		    	digito = (c - 10).toString();
		    }else{
		    	digito = c;
		    }
			return digito;		 
		}
		
		public static function sendNotificationMenu(menuFunction : String):void{
			var functionality : Functionality = new Functionality();
			functionality.eventAction = menuFunction;
			ApplicationFacade.getInstance().sendNotification(NotificationList.MENU_FUNCTION, functionality);			
		}
		
		public static function redBorderField(field:Object, enable:Boolean):void{
	      	switch(field.className){
	      		case "TextInput":
	      			field as TextInput;
	      			break;
				case "ComboBox":
					field as ComboBox;
					break;
				case "MatchComboBox":
					field as MatchComboBox;
					break;
				case "CustomComboBox":
					field as CustomComboBox
					break;
				case "CountryCombo":
					field as CountryCombo;
					break;
				case "StateCombo":
					field as StateCombo;
					break;
				case "CityCombo":
					field as CityCombo;
					break;
				case "NumericStepper":
					field as NumericStepper;
					break;
				case "MaskedDateField":
					field as MaskedDateField;
					break;
				case "AdvancedAutoComplete":
					field as AdvancedAutoComplete;
					break;														
	      	}
			
			if(enable == true){
				field.errorString = ResourceManager.getInstance().getString('messages', 'thisFieldRequired');
			}else{
				field.errorString = "";	
			}
		}
		
		public static function redBorderForm(form:Form, isRedBorder:Boolean=true):void{		
		    var formItens:Array = form.getChildren();
		    var error:String;
		    for (var i:int = 0; i < formItens.length; i++)
		    {
		    	if(formItens[i].required == true && isRedBorder){
					error = ResourceManager.getInstance().getString('messages', 'thisFieldRequired');
		        }else{
		        	error = "";
		        }
		        
	        	var formItem:Object = formItens[i].getChildren();
	        	if(formItem.length > 0){
	        		for(var a:int=0; a < formItem.length; a++){
				      	switch(formItem[a].className){
				      		case "TextInput": 
				      		case "TextArea":
				      		case "MasterTextInput":	
				      		case "ComboBox":
							case "MatchComboBox":
							case "CustomComboBox":
							case "CountryCombo":
							case "StateCombo":
							case "CityCombo":
							case "NumericStepper":
							case "CheckBox":
							case "MaskedDateField":
							case "AdvancedAutoComplete":
								formItem[a].errorString = error;
							break;
				      	}
	        		}
				}		        
		    }		
		}
		
		public static function resizeFormToNaturalPerson(pData:Boolean, pForm:WindowShade, pHeight:Number):void{
			if(pData){
				pForm.height = pHeight;
			}else{
				pForm.height = pHeight;
			}
		}					
				
		public static function removeValidationError(target : UIComponent) : void {
			if (target.errorString != null && target.errorString != "") {
				target.validateNow();
				target.errorString = "";
			}
		}
		
		public static function clearCombo( combo:Object, enabled:Boolean = true ):void {
			combo.dataProvider = new ArrayCollection;
			if( combo is ComboBox )
				combo.dropdown.dataProvider = null;
			combo.selectedItem = null;
			combo.text = '';
			combo.enabled = enabled;
			redBorderField( combo, false );
		}

		public static function getErrorDescription(error:FaultEvent):String{
			if(!error)
				return null;
			return error.fault.faultString;
		}

		public static function convertTimeZone(date:Date):Date{
			if(date){
				date.hours = 12;
				date.minutes = 0;
				date.seconds = 0;
				date.milliseconds = 0;
			}
			return date;
		}

		/**
		 * Format Mask CEP
		 */	
		public static function formatarCEP(numCep:String):String{
			if(MASK_CEP.test(numCep)){
				return numCep.replace(MASK_CEP, "$1$2-$3");
			}
			return numCep;
		}

		/**
		 * Format Mask CPF
		 */
		public static function formatMaskCpf(numCpf:String):String{
			var str:String = "";
			
			if(!numCpf){
				return str;
			}
			
			if(MASK_CPF.test(numCpf)){
				str = CPFMaskNumber(numCpf);
			}
			else{
				str = numCpf;
			}
			
			var lengthDocument:int = (int(str.toString().length));
			if(lengthDocument < LENGTH_CPF){
				var zeroLeft:String = zeroLeftPad(numCpf, LENGTH_CPF);
				str = zeroLeft;
			}
			
			var tresDigitosPonto0:String = str.substr(0, 3); // copia 3 digitos
			var tresDigitosPonto3:String = str.substr(3, 3); // copia 3 digitos
			var tresDigitosTraco6:String = str.substr(6, 3); // copia 3 digitos
			var doisDigitosTraco9:String = str.substr(9, 2); // copia 2 digitos
			
			// novo cnpj
			var newCPF:String = 
				tresDigitosPonto0 + "." +
				tresDigitosPonto3 + "." +
				tresDigitosTraco6 + "-" +
				doisDigitosTraco9;
			
			if(MASK_CPF.test(newCPF)){
				return newCPF;
			}
			
			return numCpf;
		}

		/**
		 * Format Mask CNPJ
		 */
		public static function formatMaskCnpj(numCnpj:String):String{
			var str:String = "";
			
			if(!numCnpj){
				return str;
			}
			
			if(MASK_CNPJ.test(numCnpj)){
				str = CNPJMaskNumber(numCnpj);
			}
			else{
				str += str = numCnpj;
			}
			
			var lengthDocument:int = (int(str.toString().length));
			if(lengthDocument < LENGTH_CNPJ){
				var zeroLeft:String = zeroLeftPad(numCnpj, LENGTH_CNPJ);
				str = zeroLeft;
			}
			
			var doisDigitosPonto:String = str.substr(0, 2);  // copia 2 digitos
			var tresDigitosPonto:String = str.substr(2, 3);  // copia 3 digitos
			var tresDigitosBarra:String = str.substr(5, 3);  // copia 3 digitos
			var quatDigitosTraco:String = str.substr(8, 4);  // copia 4 digitos
			var doisDigitosFim:String   = str.substr(12, 2); // copia 2 digitos
			
			// novo cnpj
			var newCNPJ:String = 
				doisDigitosPonto + "." + 
				tresDigitosPonto + "." + 
				tresDigitosBarra + "/" + 
				quatDigitosTraco + "-" + 
				doisDigitosFim;
			
			if(MASK_CNPJ.test(newCNPJ)){
				return newCNPJ;
			}
			
			return numCnpj;
		}

		public static function CPFMaskNumber(CPFMasked:String):String{
			return CPFMasked.replace(/[.\-]/g, '');
		}

		public static function CNPJMaskNumber(CNPJMasked:String):String{
			return CNPJMasked.replace(/[.\-\/]/g, '');
		}

		/**
		 * adiciona zero a esquerda.
		 * @param number numero que deseja adicionar zero a esqueda
		 * @param width numero de caracteres desejado
		 * @param return retorna o numero com a quantidade de zeros a esqueda necessario.
		 */
		public static function zeroLeftPad(number:String, width:int):String{
		   var ret:String = "" + number;
		   while(ret.length < width)
			   ret="0" + ret;
		   return ret;
		}

		public static function validateCPF(value:Object):Boolean{
			if(!value)
				return false;
			
			var digits:String = CPFMaskNumber(value.toString());
			var regex:RegExp = /\d{11}/;
			
			// considera-se erro CPF's formados por uma sequencia de numeros iguais
			if( digits == '00000000000' || digits == '11111111111' ||
				digits == '22222222222' || digits == '33333333333' ||
				digits == '44444444444' || digits == '55555555555' ||
				digits == '66666666666' || digits == '77777777777' ||
				digits == '88888888888' || digits == '99999999999'){
				return false;
			}
			
			if(regex.test(digits)){
				digits = digits.substr(0,9);
				var sum:int = 0;
				
				for(var i:int = 10; i > 1; i--){
					sum += int(digits.charAt(10-i)) * i;
				}
				
				sum = sum % Number(11);
				
				digits += sum < 2? '0' : (11 - sum).toString();
				
				sum = 0;
				
				for(var j:int = 11; j > 1; j--){
					sum += int(digits.charAt(11-j)) * j;
				}
				
				sum = sum % Number(11);
				
				digits += sum < 2? '0' : (11 - sum).toString();
				
				if(digits == value.toString().replace(/\D/g, '')){
					return true;
				}
			}
			
			return false;
		}

		public static function validateCPF9Digits(value:Object):Boolean{
			if(!value)
				return false;
			
			if(value.toString().length < 9 || value.toString().length > 11)
				return false;
			
			var digits:String = CPFMaskNumber(value.toString());
			var i:int;
			var c:String = digits.substr(0, digits.length-2);
			var dv:String = digits.substr(digits.length-2, 2);
			var d1:int = 0;
			
			for(i = 0; i < c.length; i++){
				var op1:int = parseInt(c.substr(i, i+1));
				var op2:int = (10-(11-digits.length)-i);
				d1 += (op1 * op2);
			}
			
			if(d1 == 0)
				return false;
			
			d1 = 11 - (d1 % 11);
			
			if (d1 > 9)
				d1 = 0;
			
			if(parseInt(dv.charAt(0)) != d1)
				return false;
			
			d1 *= 2;
			
			for(i = 0; i < c.length; i++){
				op1 = parseInt(c.substr(i, i+1));
				op2 = (11-(11-digits.length)-i);
				d1 += (op1 * op2);
			}
			
			d1 = 11 - (d1 % 11);
			
			if(d1 > 9)
				d1 = 0;
			
			if(parseInt(dv.charAt(1)) != d1)
				return false;
			
			return true;
		}

		public static function validateCNPJ(value:Object):Boolean{
			if(!value)
				return false;
			
			var CNPJ:String = CNPJMaskNumber(value.toString());
			
			if(CNPJ.length != 14){
				return false;
			}
			else{
				var a:Array = new Array;
				var b:Number = new Number();
				var c:Array = new Array(6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2);
				
				for(var i:int=0; i<12; i++){
					a[i] = CNPJ.charAt(i);
					b += a[i]*c[i+1];
				}
				
				var x:int = b%11;
				
				if(x < 2){
					a[12] = 0;
				}
				else{
					a[12] = 11-x;
				}
				
				b = 0;
				
				for(var y:int = 0; y < 13; y++){
					b += (a[y]*c[y]);
				}
				
				x = b%11;
				
				if(x < 2){
					a[13] = 0;
				}
				else{
					a[13] = 11-x;
				}
				
				if((CNPJ.charAt(12) != a[12]) || (CNPJ.charAt(13) != a[13])){
					return false;
				}
				else{
					return true;
				}
			}
		}
	}
}