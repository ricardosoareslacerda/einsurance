package br.com.tratomais.einsurance.core  
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
    [RemoteClass(alias="br.com.tratomais.core.service.ReturnValue")]   
    [Managed]  
    public class ReturnValue extends HibernateBean 
    {
		private var _success:Boolean;
		private var _returnValue:Object;
		private var _messages:String;
		private var _listaErros:ErrorList;

        public function get success():Boolean{  
            return _success;  
        }  
  
        public function set success(pData:Boolean):void{  
            _success=pData;  
        }  

        public function get returnValue():Object{  
            return _returnValue;  
        }  
  
        public function set returnValue(pData:Object):void{  
            _returnValue=pData;  
        }  

        public function get messages():String{  
            return _messages;  
        }  
  
        public function set messages(pData:String):void{  
            _messages=pData;  
        }  

        public function get codigo():ErrorList{  
            return _listaErros;  
        }  
  
        public function set codigo(pData:ErrorList):void{  
            _listaErros=pData;  
        }
    }
}
