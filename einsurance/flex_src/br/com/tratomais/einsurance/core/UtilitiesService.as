package br.com.tratomais.einsurance.core
{
	import net.digitalprimates.persistence.hibernate.rpc.HibernateRemoteObject;
	
	public class UtilitiesService
	{
	
       	public static const SERVICE_LOGIN:String				= SERVICE_EINSURANCE;
      	public static const SERVICE_USER:String					= SERVICE_EINSURANCE;
		public static const SERVICE_MODULE:String				= SERVICE_EINSURANCE;
		public static const SERVICE_BROKER:String				= SERVICE_EINSURANCE;
		public static const SERVICE_ROLE:String			    	= SERVICE_EINSURANCE;
  		public static const SERVICE_BRANCH:String				= SERVICE_EINSURANCE;
  		public static const SERVICE_CHANNEL:String				= SERVICE_EINSURANCE;
  		public static const SERVICE_USER_PARTNER:String			= SERVICE_EINSURANCE
  		public static const SERVICE_INSURER:String				= SERVICE_EINSURANCE;
  		public static const SERVICE_CITY:String					= SERVICE_EINSURANCE;
  		public static const SERVICE_COUNTRY:String				= SERVICE_EINSURANCE;
  		public static const SERVICE_STATE:String				= SERVICE_EINSURANCE;
  		public static const SERVICE_PARTNER:String				= SERVICE_EINSURANCE;
  		public static const SERVICE_UTIL:String					= SERVICE_EINSURANCE;
  		public static const SERVICE_SUBSIDIARY:String			= SERVICE_EINSURANCE;
  		public static const SERVICE_DATA_OPTION:String			= SERVICE_EINSURANCE;
  		public static const SERVICE_PRODUCT:String				= SERVICE_EINSURANCE;
  		public static const SERVICE_PROPOSAL:String				= SERVICE_EINSURANCE;
  		public static const SERVICE_QUOTE:String				= SERVICE_EINSURANCE;
  		public static const SERVICE_CALC:String					= SERVICE_EINSURANCE;
  		public static const SERVICE_MASTER_POLICY:String		= SERVICE_EINSURANCE;
  		public static const SERVICE_SEARCH_DOCUMENT:String		= SERVICE_EINSURANCE;
  		public static const SERVICE_REPORT:String				= SERVICE_EINSURANCE;
  		public static const SERVICE_SUPPORT_POLICY:String		= SERVICE_EINSURANCE;
  		public static const SERVICE_BATCH_ADMINISTRATION_MAIN	= SERVICE_EINSURANCE;
  		public static const SERVICE_INTERFACE					= SERVICE_EINSURANCE;
  		public static const FIND_CEP:String						= SERVICE_EINSURANCE;
  		
  		private static const SERVICE_EINSURANCE:String		= "serviceEinsurance";
   		
  		private static var _instance:UtilitiesService;
  		private static var _service:HibernateRemoteObject;
  		
  		private static var _amfEndPoint:String = "http://localhost:8080/einsurance/messagebroker/amf";

		public static function getInstance():UtilitiesService 
		{
	 		if ( _instance == null )
				_instance = new UtilitiesService;
				 
			return _instance;
	    }

  		
  		/**
  		 * serviceName: Nome do servico. Ex. UtilitiesService.SERVICE_LOGIN
  		 * **/
		public function getService(serviceName : String): Object {
			if(_service == null ){
				var serviceNew :  HibernateRemoteObject = new HibernateRemoteObject();
				serviceNew.endpoint = _amfEndPoint;
				serviceNew.destination = SERVICE_EINSURANCE;
				serviceNew.source= SERVICE_EINSURANCE;
				serviceNew.makeObjectsBindable=true;
				serviceNew.showBusyCursor=true;
				_service = serviceNew;
			}
			return _service;
		}
		
		public function set amfEndPoint(endPoint:String):void{
			_amfEndPoint = endPoint;
		}
	}
}