package br.com.tratomais.einsurance.core 
{
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	  
    [RemoteClass(alias="br.com.tratomais.core.service.erros.ErrorList")]      
    [Managed]  
    public class ErrorList extends HibernateBean 
    {  
        private var _errorNumbers:int;  
        private var _listaErros:ArrayCollection;  
  
        public function ErrorList()  
        {  
        }  
  
        public function get errorCount():int{  
            return _listaErros.length;  
        }  
  
        public function set errorCount(pData:int):void{  
            //_errorNumbers=pData;  
        }  
  
        public function get listaErros():ArrayCollection{  
            return _listaErros;  
        }  
  
        public function set listaErros(pData:ArrayCollection):void{  
            _listaErros=pData;  
        }
          
		public function StringError(number: int): Erro{
			return Erro(this._listaErros.getItemAt(number));
		}
    }  
}