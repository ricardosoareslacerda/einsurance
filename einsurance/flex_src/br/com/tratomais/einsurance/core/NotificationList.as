package br.com.tratomais.einsurance.core
{
	public class NotificationList
	{		
		
		public static const STARTUP:String						= "startup"; 						/**** Purpose: Initializes the application eInsurance  ***/
		public static const SHUTDOWN:String 					= "shutdown";						/**** Purpose: Shutdown the application eInsurance  ***/
	    public static const LOGOUT: String 						= "LOGOUT";
    	public static const MENU_FUNCTION:String 				= "MENU_FUNCTION";  				/**** Purpose: Call Notification Menu.   Object:  Functionality  */
    	public static const FAULT_BLAZE:String					= "FAULT_BLAZE";		
		public static const UTIL_VERIFY_HASH:String 			= "UTIL_VERIFY_HASH"; 				/**** Purpose: Retorna o hash   * Objeto: String  ***/		
		public static const UTIL_LOGGED_USER:String				= "UTIL_LOGGED_USER"; 				/**** Purpose: Retorna o usuario logado * Objeto: User ***/
		public static const CITY_LISTED:String 					= "CITY_LISTED"; 					/**** Purpose: Notificação que retorno lista City. * Objeto: ArrayCollection CityVO ***/ 	    	
		public static const COUNTRY_LISTED:String 				= "COUNTRY_LISTED"; 				/**** Purpose: Notificação que retorno lista Country.  * Objeto: ArrayCollection CountryVO ***/ 
    	public static const STATE_LISTED:String 				= "STATE_LISTED"; 					/**** Purpose: Notificação que retorno lista State. * Objeto: ArrayCollection StateVO ***/
		public static const DATA_OPTION_LISTED:String 			= "DATA_OPTION_LISTED";				/****  **  ***/
		public static const DATA_OPTION_LISTED_REPORT:String 	= "DATA_OPTION_LISTED_REPORT";				/****  **  ***/
		
		/**** Modulo Broker ***/
		public static const APPLICATION_PROPERTY_FINDED:String = 'APPLICATION_PROPERTY_FINDED';
    	public static const BROKER_SAVE_SUCCESS:String 			= "BROKER_ADD_SUCCESS";				/****  **  ***/
    	public static const BROKER_SAVE_FAULT:String 			= "BROKER_SAVE_FAULT";				/****  **  ***/
    	public static const BROKER_LISTED:String				= "BROKER_LISTED";					/****  **  ***/
		public static const BROKER_TYPE_LISTED:String			= "BROKER_TYPE_LISTED";				/****  **  ***/
		public static const BROKER_DOCUMENT_TYPE_LISTED:String	= "BROKER_DOCUMENT_TYPE_LISTED";	/****  **  ***/
		public static const BROKER_GENERAL_MESSAGE:String		= "BROKER_GENERAL_MESSAGE";			/****  **  ***/
		public static const BROKER_FIND_NAME:String 			= "BROKER_FIND_NAME";
		public static const BROKER_MAIN_GENERAL_MESSAGE:String 	= "BROKER_MAIN_GENERAL_MESSAGE";
		
		public static const CHANNEL_ADD_SUCCESS:String			= "CHANNEL_ADD_SUCCESS";			/****  **  ***/
		public static const CHANNEL_DELETE_SUCCESS:String		= "CHANNEL_DELETE_SUCCESS";			/****  **  ***/
		public static const CHANNEL_LIST_LISTED:String			= "CHANNEL_LIST_LISTED";			/****  **  ***/
		public static const CHANNEL_PARTNER_LIST_LISTED:String	= "CHANNEL_PARTNER_LIST_LISTED";	/****  **  ***/
		public static const CHANNEL_USER_LIST_REFRESH:String	= "CHANNEL_USER_LIST_REFRESH";		/****  **  ***/
		public static const CHANNEL_LIST_USER_PARTNER:String	= "CHANNEL_LIST_USER_PARTNER";			/****  **  ***/

		public static const SUBSIDIARY_GENERAL_MESSAGE:String		= "SUBSIDIARY_GENERAL_MESSAGE";			/****  **  ***/
		public static const SUBSIDIARY_SAVE_SUCCESS:String			= "SUBSIDIARY_ADD_SUCCESS";				/****  **  ***/
		public static const SUBSIDIARY_LIST_REFRESH:String			= "SUBSIDIARY_LIST_REFRESH";			/****  **  ***/
		public static const SUBSIDIARY_COUNTRY_COMB_LISTED:String	= "SUBSIDIARY_COUNTRY_COMB_LISTED";		/****  **  ***/
		public static const SUBSIDIARY_STATE_COMB_LISTED:String		= "SUBSIDIARY_STATE_COMB_LISTED"; 		/****  **  ***/
		public static const SUBSIDIARY_CITY_COMB_LISTED:String		= "SUBSIDIARY_CITY_COMB_LISTED";		/****  **  ***/

   		public static const PARTNER_LIST_REFRESH:String			= "PARTNER_LIST_REFRESH";			/****  **  ***/
   		public static const PARTNER_SAVE_SUCCESS:String			= "PARTNER_ADD_SUCCESS";			/****  **  ***/
    	
    	public static const USER_ROLES_REFRESH:String			= "USER_ROLES_REFRESH"; 			/*** Objetivo:Notificação que atualiza o list de Partner do Usuario	 * Objeto: Arraylist RoleVO */				
		public static const USER_PARTNER_REFRESH:String			= "USER_PARTNER_REFRESH";			/****  **  ***/
		public static const PAYMENT_FORM_INITIALIZE_PARAMETERS:String = 'PAYMENT_FORM_INITIALIZE_PARAMETERS';
		public static const USER_LOGIN_EXISTENCE:String			= "USER_LOGIN_EXISTENCE";			/****  **  ***/
		public static const PAYMENT_FORM_RESIZE:String = 'PAYMENT_FORM_RESIZE';
		public static const USER_LOGIN_VIEW:String				= "USER_LOGIN_VIEW";				/****  **  ***/
		public static const PAYMENT_FORM_VALIDATE:String = 'PAYMENT_FORM_VALIDATE';
		public static const PAYMENT_FORM_VALIDATE_COMPLETED:String = 'PAYMENT_FORM_VALIDATE_COMPLETED';
    	public static const USER_MAIN_GENERAL_MESSAGE:String	= "USER_MAIN_GENERAL_MESSAGE";		/****  **  ***/
    	public static const USER_LIST_REFRESH:String			= "USER_LIST_REFRESH"; 				/** * Objetivo: Notifica que os usuarios estao listados * Objeto: ArrayCollection de UsuarioVO  */
		public static const USER_SAVE_SUCCESS:String			= "USER_SAVE_SUCCESS";				/****  **  ***/
    	public static const USER_LIST_PARTNER:String			= "USER_LIST_PARTNER";				/****  **  ***/
		/**
		 * Purpose: Verify existence of external Code in subsidiary
		 * 
		 * returnObject: Boolean value - true if exists
		 */
    	public static const EXTERNAL_CODE_SUBSIDIARY_EXISTS_CHECK: String	= "EXTERNAL_CODE_SUBSIDIARY_EXISTS_CHECK";
    	
    	public static const MODULE_LIST:String 					= "MODULE_LIST";					/*** Objetivo: Listar os modulos. * Objeto: ModuleVO * **/    	
    	public static const MODULE_LISTED:String 				= "MODULE_LISTED"; 					/*** Objetivo: Notifica que os modulos foram listados. * Objeto: ModuleVO ***/
		public static const MODULE_LISTED_EDIT:String			= "MODULE_LISTED_EDIT";			/****  **  ***/
		public static const MODULE_LISTED_BY_USER:String		= "MODULE_LISTED_BY_USER";			/****  **  ***/

		public static const FUNCTIONALITY_LISTED:String			= "FUNCTIONALITY_LISTED";			/****  **  ***/

		public static const LOGIN_VALIDATED:String 				= "LOGIN_VALIDATED";				/****  **  ***/   		
		public static const LOGIN_NOT_VALIDATED:String 			= "LOGIN_NOT_VALIDATED"; 			/*** Objetivo: Notificar que o login não foi validado. * Objeto: LoginVO * **/
		
		public static const ROLE_LIST_SAVE:String				= "ROLE_LIST_SAVE";					/****  **  ***/				
    	public static const ROLE_LIST_REFRESH:String			= "ROLE_LIST_REFRESH";				/*** Objetivo: Notificação que identifica evento do menu de usuario * Objeto: UsuarioVO */
    	public static const ROLE_SAVE_SUCCESS:String			= "ROLE_SAVE_SUCCESS";				/****  **  ***/
    			
		public static const ROLE_MAIN_LIST_REFRESH:String		= "ROLE_MAIN_LIST_REFRESH";			/****  **  ***/

    	public static const REPORT_GET:String					= "REPORT_GET";						/****  Lista os tipos de report/impresão de acordo com o status do seguro ***/
		public static const REPORT_LISTED:String				= "REPORT_LISTED";					/****  Obtem o o report/impressão de acordo com o status do seguro ***/
		
		public static const IDENTIFIED_CEP_FOUND:String = 'IDENTIFIED_CEP_FOUND'; 					//Retorno da pesquisa do CEP

    	/**** Module Quote ***/
		public static const PRODUCT_LIST_REFRESH:String					= "PRODUCT_LIST_REFRESH";			/****  **  ***/
		public static const PRODUCT_LIST_COVERAGE_REFRESH:String		= "PRODUCT_LIST_COVERAGE_REFRESH";	/****  **  ***/
		public static const PRODUCT_LIST_PLAN_REFRESH:String			= "PRODUCT_LIST_PLAN_REFRESH";		/****  **  ***/
		public static const PRODUCT_LIST_RISK_PLAN_REFRESH:String		= "PRODUCT_LIST_RISK_PLAN_REFRESH";	/****  **  ***/
		public static const PRODUCT_LIST_AUTO_BRAND:String				= "PRODUCT_LIST_AUTO_BRAND";	/****  **  ***/
		public static const PRODUCT_LIST_AUTO_MODEL:String				= "PRODUCT_LIST_AUTO_MODEL";	/****  **  ***/
		public static const PRODUCT_LIST_AUTO_YEAR:String				= "PRODUCT_LIST_AUTO_YEAR";	/****  **  ***/
		public static const PRODUCT_LIST_AUTO_COST:String				= "PRODUCT_LIST_AUTO_COST";	/****  **  ***/
		public static const PRODUCT_FIND_AUTO_VERSION:String			= "PRODUCT_FIND_AUTO_VERSION";	/****  **  ***/
		public static const PRODUCT_APPLY_COVERAGE_INSURED_VALUE:String	= "PRODUCT_APPLY_COVERAGE_INSURED_VALUE";	/****  **  ***/
		public static const PRODUCT_AUTO_DATA_CHANGED:String			= "PRODUCT_AUTO_DATA_CHANGED";	/****  **  ***/
		public static const PRODUCT_LIST_PROFILE_REFRESH:String			= "PRODUCT_LIST_PROFILE_REFRESH";	/****  **  ***/
		public static const PRODUCT_RESPONSE_RELATIONSHIP_REFRESH:String= "PRODUCT_RESPONSE_RELATIONSHIP_REFRESH";	/****  **  ***/
		public static const PRODUCT_LIST_DEDUCTIBLE_REFRESH:String			= "PRODUCT_LIST_DEDUCTIBLE_REFRESH";	/****  **  ***/
    	
    	
    	public static const INFLOW_LISTED:String				= "INFLOW_LISTED";			/****  **  ***/
    	
    	public static const INSURED_INFLOW_LISTED:String		= "INSURED_INFLOW_LISTED";			/****  **  ***/
    	
    	public static const POLICY_HOLDER_INFLOW_LISTED:String	= "POLICY_HOLDER_INFLOW_LISTED";			/****  **  ***/

		public static const PERSON_TYPE_LISTED:String			= "PERSON_TYPE_LISTED";				/****  **  ***/
		
		public static const MARITAL_STATUS_LISTED:String		= "MARITAL_STATUS_LISTED";			/****  **  ***/
		
		public static const DOCUMENT_TYPE_LISTED:String			= "DOCUMENT_TYPE_LISTED";			/****  **  ***/

		public static const ADDRESS_TYPE_LISTED:String			= "ADDRESS_TYPE_LISTED";			/****  **  ***/
    	
		public static const KINSHIP_TYPE_LISTED:String			= "KINSHIP_TYPE_LISTED";			/****  **  ***/
		
		public static const BENEFICIARY_TYPE_LISTED:String		= "BENEFICIARY_TYPE_LISTED";		/****  **  ***/
		
		public static const GENDER_TYPE_LISTED:String			= "GENDER_TYPE_LISTED";				/****  **  ***/
		
		public static const PROFESSION_TYPE_LISTED:String		= "PROFESSION_TYPE_LISTED";			/****  **  ***/
		
		public static const PHONE_TYPE_LISTED:String			= "PHONE_TYPE_LISTED";				/****  **  ***/
		
		public static const CONTACT_TYPE_LISTED:String			= "CONTACT_TYPE_LISTED";			/****  **  ***/
		
		public static const POLICY_TYPE_LISTED:String			= "POLICY_TYPE_LISTED";				/****  **  ***/
		
		public static const PAYMENT_TYPE_LISTED:String			= "PAYMENT_TYPE_LISTED";			/****  **  ***/

		public static const PAYMENT_TYPE_LIST:String			= "PAYMENT_TYPE_LIST";			/**** body contains paymentTypeId selected **  ***/
			
		public static const ACTIVITY_LISTED:String				= "ACTIVITY_LISTED";				/****  **  ***/
		
		public static const OCCUPATION_LISTED:String			= "OCCUPATION_LISTED";				/****  **  ***/
		
		public static const RESTRICTION_RISK_LISTED:String		= "RESTRICTION_RISK_LISTED";	    /****  **  ***/
		
		public static const RETURN_QUOTE:String						= "RETURN_QUOTE";					/****  **  ***/
		
		public static const PROFESSION_LISTED:String			= "PROFESSION_LISTED"; 				/****  **  ***/
		
		public static const PARTNER_DOCUMENT_TYPE_LISTED:String	= "PARTNER_DOCUMENT_TYPE_LISTED";	/****  **  ***/
		
		public static const DOMAIN_LIST_REFRESH:String			= "DOMAIN_LIST_REFRESH";			/****  **  ***/
		
		public static const ENDORSEMENT_LISTED:String			= "ENDORSEMENT_LISTED";				/****  **  ***/

		public static const ENDORSEMENT_UNLOCKABLE_LISTED:String	= "ENDORSEMENT_UNLOCKABLE_LISTED";				/****  **  ***/
		
		public static const ENDORSEMENT_UNLOCKED:String	= "ENDORSEMENT_UNLOCKED";				/****  **  ***/

		public static const ENDORSEMENT_SAVE_SUCCESS:String		= "ENDORSEMENT_SAVE_SUCCESS";		/****  **  ***/
		
		public static const POLICY_CHANGE_SUCCESS:String		= "POLICY_CHANGE_SUCCESS";		/****  **  ***/		
	
		public static const CALCULATED_QUOTATION:String			= "CALCULATED_QUOTATION";		/****  **  ***/
		
		public static const RESIZE_PROPOSAL_INSURED_FORM:String				= 	"RESIZE_PROPOSAL_INSURED_FORM";		/****  **  ***/
		
		public static const RESIZE_PROPOSAL_POLICY_HOLDER_FORM:String		= 	"RESIZE_PROPOSAL_POLICY_HOLDER_FORM";		/****  **  ***/
		
		public static const RESIZE_PROPOSAL_MOTORIST_FORM:String		= 	"RESIZE_PROPOSAL_MOTORIST_FORM";		/****  **  ***/

		public static const SEARCH_LEVY_LISTED:String 						= 	"SEARCH_LEVY_LISTED" 			/**** ** ***/

		public static const BILLING_METHOD_LISTED:String					= 	"BILLING_METHOD_LISTED"					/**** ** ***/

		public static const SEARCH_DETAILS_LEVY_LISTED:String				= 	"SEARCH_DETAILS_LEVY_LISTED"				/**** ** ***/

		public static const SEARCH_COLLECTION_LISTED:String					= 	"SEARCH_COLLECTION_LISTED"				/**** ** ***/

		public static const SEARCH_COLLECTION_DETAILS_LISTED:String			= 	"SEARCH_COLLECTION_DETAILS_LISTED"		/**** ** ***/
		
		public static const EXCHANGE_COLLECTION_LISTED:String				=	"EXCHANGE_COLLECTION_LISTED"					/**** ** ***/

		public static const STATUS_LOT_LISTED:String						=	"STATUS_LOT_LISTED"					/**** ** ***/
		/**
		 * Purpose: Return quantity of contracts
		 * 
		 * Returns number of contracts (int) 
		 */
		public static const MAX_DOCUMENTS_PER_PRODUCT:String	= "MAX_DOCUMENTS_PER_PRODUCT";		/****  **  ***/
		
		/**
		 * Purpose: MAKE VALIDATIONS ON CONTRACTS
		 * 
		 * Returns validations messages (Array of Strings)
		 */
		public static const CONTRACT_VALIDATIONS:String = "CONTRACTS_VALIDATIONS";

		public static const GET_QUANTITY_OF_CONTRACTS:String			= "GET_QUANTITY_OF_CONTRACTS";		/****  **  ***/

		public static const ERROR_CALCULATED_QUOTATION:String	= "ERROR_CALCULATED_QUOTATION";		/****  **  ***/

		public static const CUSTOMER_SAVE_SUCCESS:String		= "CUSTOMER_SAVE_SUCCESS";			/****  **  ***/
		
		public static const LOAD_PROPOSAL_LIST:String			= "LOAD_PROPOSAL_LIST";
		
		public static const LOAD_QUOTE_LIST:String				= "LOAD_QUOTE_LIST";
		
		public static const LOAD_REPORT_LIST:String 			= "LOAD_REPORT_LIST";
		
		public static const LOAD_ANNULMENT_LIST:String 			= "LOAD_ANNULMENT_LIST";
		
		public static const OPEN_POLICYHOLDER_DATA:String		= "OPEN_POLICYHOLDER_DATA";
		
		public static const CLOSE_POLICYHOLDER_DATA:String		= "CLOSE_POLICYHOLDER_DATA";
		
		public static const OPEN_MOTORIST_DATA:String			= "OPEN_MOTORIST_DATA";
		
		public static const CLOSE_MOTORIST_DATA:String			= "CLOSE_MOTORIST_DATA";
		
		public static const OPEN_CAPITAL_DATA:String			= "OPEN_CAPITAL_DATA";
		
		public static const CLOSE_CAPITAL_DATA:String			= "CLOSE_CAPITAL_DATA";
				
		public static const MASTER_POLICY_LISTED:String			= "MASTER_POLICY_LISTED";
		
		public static const MASTER_POLICY_SAVE_SUCCESS:String	= "MASTER_POLICY_SAVE_SUCCESS";
		
		public static const PRODUCT_LISTED:String				= "PRODUCT_LISTED";
		
		public static const PERSONAL_RISK_OPTION_LISTED:String  = "PERSONAL_RISK_OPTION_LISTED"; 
		
		public static const PARTNER_CHANNEL_LIST_REFRESH:String = "PARTNER_CHANNEL_LIST_REFRESH"; 

		public static const SEARCH_DOCUMENT_LISTED:String 	= "SEARCH_DOCUMENT_LISTED";			/****  **  ***/
		
		public static const CLEAR_COUNTRY_COMBO:String			= "CLEAR_COUNTRY_COMBO";
		
		public static const CLEAR_STATE_COMBO:String 			= "CLEAR_STATE_COMBO";

		public static const CLEAR_STATE_CITY:String 			= "CLEAR_STATE_CITY";
		
		public static const CERTIFICATE_DOCUMENT_TYPE_LISTED:String = "CERTIFICATE_DOCUMENT_TYPE_LISTED";

		public static const PAYMENT_OPTION_LISTED:String = "PAYMENT_OPTION_LISTED";

		public static const PAYMENT_OPTION_LIST:String = "PAYMENT_OPTION_LIST";
		
		public static const GENERAL_REPORT_LISTED:String 	= "GENERAL_REPORT_LISTED";			/****  **  ***/
		
		public static const PRODUCT_TO_BE_COPY_FOUND:String 	= "PRODUCT_TO_BE_COPY_FOUND";
		
		public static const SELECTED_PAYMENT_OPTION: String = "SELECTED_PAYMENT_OPTION";

		public static const PROPOSAL_EFFECTIVED_ENDORSEMENT: String = "PROPOSAL_EFFECTIVED_ENDORSEMENT";

		public static const PROPOSAL_NOT_EFFECTIVED_ENDORSEMENT: String = "PROPOSAL_NOT_EFFECTIVED_ENDORSEMENT";

	    public static const PRODUCT_VERSION_LISTED: String = "PRODUCT_VERSION_LISTED";
	    
	    public static const LOAD_ITEM_ADDITIONAL: String = "LOAD_ITEM_ADDITIONAL";
	    
		/**
		 * Purpose: Search a product version based on date and productId
		 * 
		 * returnObject: ProductVersion
		 */		
		public static const PRODUCT_VERSION_PER_DATE_LISTED: String = "PRODUCT_VERSION_PER_DATE_LISTED";
	    
	    public static const PRODUCT_LIST_PER_PARTNER: String = "PRODUCT_LIST_PER_PARTNER";

	    public static const PARNTER_CHANGE: String = "PARNTER_CHANGE";
	    
	    public static const DATA_OPTION_LOADED: String = "DATA_OPTION_LOADED";
	    public static const PLAN_LOADED: String = "PLAN_LOADED";

		public static const PRODUCT_COPY_SAVED:String = "PRODUCT_COPY_SAVED";
		
		/**** 
		 * Purpose: List a colection of PlanKinship filtered by Product, RiskPlan and RenewalType
		 * 
		 * returnObject: ArrayCollection of PlanKinship 
		 ***/
		public static const PRODUCT_LIST_PLAN_KINSHIP_REFRESH_PROPOSAL:String = "PRODUCT_LIST_PLAN_KINSHIP_REFRESH_PROPOSAL";
		
		/**** 
		 * Purpose: List a colection of PlanKinship filtered by Product, RiskPlan , personalRiskType and RenewalType
		 * 
		 * returnObject: ArrayCollection of PlanKinship 
		 ***/
		public static const PRODUCT_LIST_PLAN_KINSHIP_RISK_TYPE:String = "PRODUCT_LIST_PLAN_KINSHIP_RISK_TYPE";
		
		/**** 
		 * Purpose: View proposal locked for unlocked
		 ***/
		public static const CERTIFICATE_ENDORSEMENT_UNLOCK:String 	= "CERTIFICATE_ENDORSEMENT_UNLOCK";			/****  **  ***/

		/**** 
		 * Purpose: View proposal locked for anullation
		 * 
		 ***/
		public static const CERTIFICATE_ENDORSEMENT_ANULLATION:String 	= "CERTIFICATE_ENDORSEMENT_ANULLATION";			/****  **  ***/

		/****
		 * Purpose: View proposal locked for change
		 * 
		 ***/
		public static const CERTIFICATE_POLICY_CHANGE:String 	= "CERTIFICATE_POLICY_CHANGE";			/****  **  ***/

		/**** 
		 * Purpose: endorsement canceled
		 * 
		 ***/
		public static const CERTIFICATE_ENDORSEMENT_CANCELED:String 	= "CERTIFICATE_ENDORSEMENT_CANCELED";			/****  **  ***/
		
		/**
		 * Purpose: emision canceled
		 ***/
		public static const CERTIFICATE_EMISION_CANCELED:String 	= "CERTIFICATE_EMISION_CANCELED";			/****  **  ***/		
		
		/**
		 * Purpose: List an identified sort of data from data option
		 * 
		 * returnObject: ArrayCollection of IdentifiedList
		 */
		public static const DATA_OPTION_IDENT_LISTED: String = "DATA_OPTION_IDENT_LISTED";
		
		/**
		 * Purpose: Return from a password change request
		 * 
		 * returnObject: 0 - Changes ok / 1 - Error in authentication / 2 - Other error
		 */
		public static const PASSWORD_CHANGE: String = "PASSWORD_CHANGE";
		
    	public static const MENU_ABANDONMENT:String 				= "MENU_ABANDONMENT";  				/**** Purpose: Call Notification Menu.   Object:  Functionality  */

		public static const VIEW_PROPOSAL_MAIN_ABANDONMENT = "VIEW_PROPOSAL_MAIN_ABANDONMENT"; /**** Call view selected if the user decides to abandon the proposal*/
		
		public static const COVERAGE_RANGE_VALUE_LISTED:String = "COVERAGE_RANGE_VALUE_LISTED";

		public static const SET_POLICY_HOLDER_NAME:String = "SET_POLICY_HOLDER_NAME"; 

		public static const CLEAN_POLICY_HOLDER_NAME:String = "CLEAN_POLICY_HOLDER_NAME";

		public static const SET_CARD_HOLDER_NAME:String = "SET_CARD_HOLDER_NAME";

		public static const SELECTED_PERSONAL_RISK_PLAN:String = "SELECTED_PERSONAL_RISK_PLAN";
		
		public static const MODULE_FOUND:String = "MODULE_FOUND";

		/**
		 * Purpose: GridView Paging
		 *
		 */ 
		public static const GRIDVIEW_UPDATE_LIST:String = "GRIDVIEW_UPDATE_LIST";

		public static const POLICY_REACTIVATION_RESULT:String = "POLICY_REACTIVATION_RESULT";
				
		public static const POLICY_CREATE_AND_FREE_PENDENCY_RESULT = "POLICY_CREATE_AND_FREE_PENDENCY_RESULT";
				
		public static const ITEM_PERSONAL_RISK_LISTED:String = "ITEM_PERSONAL_RISK_LISTED";

		/**
		 * Renewal 
		 */
		public static const RENEWAL_ALERT_LIST:String = "RENEWAL_ALERT_LIST";
		public static const RENEWAL_ALERT_RESULT:String = "RENEWAL_ALERT_RESULT";
		
		public static const PRODUCT_VERSION_BETWEEN_DATE_LISTED:String = "PRODUCT_VERSION_BETWEEN_DATE_LISTED";
		
		public static const PRODUCT_OPTION_LISTED:String = "PRODUCT_OPTION_LISTED";
		
		public static const PRODUCT_LIST_PLAN_KINSHIP_REFRESH_QUOTE:String = "PRODUCT_LIST_PLAN_KINSHIP_REFRESH_QUOTE";
		
		public static const INSURED_NAME_SELECTED:String = 'INSURED_NAME_SELECTED';
		public static const APPLICATION_PROPERTY_LISTED:String = "APPLICATION_PROPERTY_LISTED";
		
		public static const APPLICATION_PROPERTY_IDENT_LISTED:String = "APPLICATION_PROPERTY_IDENT_LISTED";
		
		public static const LOAD_INTERFACE_MAIN:String = "LOAD_INTERFACE_MAIN";
		
		public static const PRODUCT_PAYMENT_TERM_LISTED:String = 'PRODUCT_PAYMENT_TERM_LISTED';
		public static const INTERFACE_MAIN_IDENTIFIED:String = "INTERFACE_MAIN_IDENTIFIED";
		
		public static const VALIDATE_BANK_INFO_RESULT:String = 'VALIDATE_BANK_INFO_RESULT'; /** Disparado pelo core trazendo informacoes de validacao de conta do banco */
		public static const FAULT_VALIDATE_BANK_INFO:String = 'FAULT_VALIDATE_BANK_INFO'; /** Disparado quando a erro de validacao de conta do banco no core */
		public static const GET_INSTALLMENT_LISTED:String = 'GET_INSTALLMENT_LISTED'; /** Disparado pelo core trazendo a Installment */
		public static const PREVIEW_DATE_INVOICE:String = 'PREVIEW_DATE_INVOICE'; /** Disparado pela selecao de forma de pagamento para recalcular a data de vencimento da fatura*/
		public function NotificationList(){
		}
	}
}