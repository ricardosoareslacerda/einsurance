package br.com.tratomais.einsurance.core.components.CountryCombo.view.mediator
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.components.CountryCombo.view.component.CountryCombo;
	import br.com.tratomais.einsurance.customer.model.proxy.CountryProxy;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	 
	public class CountryComboMediator extends Mediator implements IMediator
	{ 
		[Bindable]public var identifiedList:IdentifiedList;
		
		private var countryProxy:CountryProxy;
		private var resultArray:ArrayCollection; 
		
		public function CountryComboMediator(viewComponent:Object)
		{
			super(viewComponent.id, viewComponent);
		}
		
	    protected function get countryCombo(): CountryCombo {
            return viewComponent as CountryCombo;
        }
		
		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */
		override public function onRegister():void 
		{
			super.onRegister();
			
			countryProxy = CountryProxy(facade.retrieveProxy(CountryProxy.NAME));
			
			identifiedList = new IdentifiedList();
			identifiedList.groupId = countryCombo.groupId;
			identifiedList.resultArray = resultArray;
			
			countryCombo.addEventListener(CountryCombo.LOAD_COUNTRY_COMBO, loadCountryCombo);
		}
		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
		}
		
        override public function listNotificationInterests():Array  
        {  
            return [NotificationList.COUNTRY_LISTED];
        }
        
        override public function handleNotification(notification:INotification):void  
        {  
            switch ( notification.getName() )  
            {
            	case NotificationList.COUNTRY_LISTED:
		        	var result: IdentifiedList = notification.getBody() as IdentifiedList;
		        	if(result.groupId == countryCombo.groupId)
		        	{
		        		countryCombo.dataProvider = result.resultArray;
		        	}
	                break;
            }
        }
        
        private function loadCountryCombo(event:Event):void
        {
        	countryProxy.listCountry(identifiedList);
        }
	}
}