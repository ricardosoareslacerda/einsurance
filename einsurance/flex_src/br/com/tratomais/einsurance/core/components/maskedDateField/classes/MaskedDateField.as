package br.com.tratomais.einsurance.core.components.maskedDateField.classes
{
	import br.com.tratomais.einsurance.core.components.maskedDateField.controls.MaskedTextInput;

	import flash.events.Event;
	import flash.events.TextEvent;

	import mx.controls.DateChooser;
	import mx.controls.DateField;
	import mx.core.FlexVersion;
	import mx.core.mx_internal;
	import mx.events.CalendarLayoutChangeEvent;
	import mx.events.FlexEvent;
	import mx.resources.ResourceManager;
	import mx.styles.StyleProxy;

	use namespace mx_internal;

	public class MaskedDateField extends DateField
	{
		public static const defaultInputMask:String = "DD/MM/YYYY";
		public static const CHANGE:String = 'maskedtDateField.change';

		private static var _inputMask:String = defaultInputMask;
		private var _dropdown:DateChooser
		private var _newDate:Date;
		private var isDropdownSelected:Boolean;

		//Semana
		private var _sunday:String = ResourceManager.getInstance().getString('resources', 'sunday');
		private var _monday:String = ResourceManager.getInstance().getString('resources', 'monday');
		private var _tuesday:String = ResourceManager.getInstance().getString('resources', 'tuesday');
		private var _wednesday:String = ResourceManager.getInstance().getString('resources', 'wednesday');
		private var _thursday:String = ResourceManager.getInstance().getString('resources', 'thursday');
		private var _friday:String = ResourceManager.getInstance().getString('resources', 'friday');
		private var _saturday:String = ResourceManager.getInstance().getString('resources', 'saturday');

		//Meses
		private var _january:String = ResourceManager.getInstance().getString('resources', 'january');
		private var _february:String = ResourceManager.getInstance().getString('resources', 'february');
		private var _march:String = ResourceManager.getInstance().getString('resources', 'march');
		private var _april:String = ResourceManager.getInstance().getString('resources', 'april');
		private var _may:String = ResourceManager.getInstance().getString('resources', 'may');
		private var _june:String = ResourceManager.getInstance().getString('resources', 'june');
		private var _july:String = ResourceManager.getInstance().getString('resources', 'july');
		private var _august:String = ResourceManager.getInstance().getString('resources', 'august');
		private var _september:String = ResourceManager.getInstance().getString('resources', 'september');
		private var _october:String = ResourceManager.getInstance().getString('resources', 'october');
		private var _november:String = ResourceManager.getInstance().getString('resources', 'november');
		private var _december:String = ResourceManager.getInstance().getString('resources', 'december');

		/**
		 * constructor
		 */
		public function MaskedDateField(){
			super();
			
			super.dayNames = [_sunday, _monday, _tuesday, _wednesday, _thursday, _friday, _saturday];
			super.monthNames= [_january, _february, _march, _april, _may, _june, _july, _august, _september, _october, _november, _december];
			this.addEventListener(FlexEvent.CREATION_COMPLETE, handleCreationComplete);
		}

		/**
		 * @param value String, proper values 'DD/MM/YYYY','MM/DD/YYYY', 'YYYY/DD/MM' and 'YYYY/MM/DD'. Other would be ignored
		 * and used default value "MM/DD/YYYY" 
		 */
		public function set inputMask(value:String):void{
			_inputMask = value;
			
			if(textInput)
				(textInput as MaskedTextInput).inputMask = value;
			
			formatString = value;
		}

		public function get inputMask():String{
			if(textInput)
				return (textInput as MaskedTextInput).inputMask;
			else
				return defaultInputMask;
		}

		private function handleCreationComplete(event:FlexEvent):void{
			this.removeEventListener(FlexEvent.CREATION_COMPLETE, handleCreationComplete);
			inputMask = defaultInputMask;
		}

		public function get newDate():Date{
			return _newDate;
		}

		public function newDisabledRanges(dayStart:Date, dayEnd:Date):void{
			var initialDate:Date = new Date(1900, 0, 1, 12);
			var finalDate:Date = new Date(2100, 11, 31, 12);
			
			dayStart = new Date(dayStart.getFullYear(), dayStart.getMonth(), dayStart.getDate(), 12);
			dayEnd = new Date(dayEnd.getFullYear(), dayEnd.getMonth(), dayEnd.getDate()-1, 12);
			
			super.disabledRanges = [{rangeStart: initialDate, rangeEnd: dayStart},{rangeStart: dayEnd, rangeEnd: finalDate}];
		}

		override public function get text():String{
			return this.textInput.text;
		}

		/**
		 * we need to re-declarate adn re-create textInput instance (TextInput > MaskedTextInput)
		 */
		override protected function createChildren():void{
			super.createChildren();
			
			var level:Number = -1;
			
			if(textInput){
				if(owns(textInput)){
					level = getChildIndex(textInput);
					removeChild(textInput);
				}
				textInput = null;
			}
			
			if(!textInput){
				var textInputStyleName:Object = getStyle("textInputStyleName");
				if(!textInputStyleName)
					textInputStyleName = new StyleProxy(this, textInputStyleFilters);
				
				textInput = new MaskedTextInput();
				
				(textInput as MaskedTextInput).inputMask = _inputMask;
				textInput.editable = editable;
				textInput.restrict = "^\u001b";
				textInput.focusEnabled = false;
				textInput.imeMode = super.imeMode;
				textInput.styleName = textInputStyleName;
			   
				textInput.addEventListener('inputMaskEnd', uniHandler);
				textInput.addEventListener('change', uniHandler);
				textInput.addEventListener(FlexEvent.VALUE_COMMIT, uniHandler);
				textInput.addEventListener(TextEvent.TEXT_INPUT, uniHandler);
				
				if(level != -1) addChildAt(textInput, level);
				
				this.swapChildren(textInput,downArrowButton);
				textInput.move(0, 0);
			}
			
			_dropdown = dropdown;
			_dropdown.addEventListener(CalendarLayoutChangeEvent.CHANGE, handleChangeDropdown);
		}

		private function handleChangeDropdown(event:Event):void{
			isDropdownSelected = true;
		}

		override public function set selectedDate(value:Date):void{
			super.selectedDate = value;
			
			_newDate = value;
			
			if(!value)
				textInput.text = '';
		}

		/**
		 * Handler for 'inputMaskEnd', 'change' and 'valueCommit' MaskedInput instance to update DateChooser 
		 * selection in accordance to  fullText.
		 * @param event Event
		 */
		private function uniHandler(event:Event):void{
			_dropdown.selectedDate = stringToDate((textInput as MaskedTextInput).fullText, formatString);
			
			if(event.type == 'inputMaskEnd'){
				selectedDate = _dropdown.selectedDate;
				dispatchEvent(event);
			}
			else if(event.type == 'change' && fullText().length < 10){
				dispatchEvent(new Event('dateIsNotValid'));
			}
			
			if(_dropdown.selectedDate){
				_dropdown.displayedMonth = _dropdown.selectedDate.getMonth();
				_dropdown.displayedYear = _dropdown.selectedDate.getFullYear();
				_newDate = _dropdown.selectedDate;
			}
			else{
				_newDate = null;
			}
			
			if(event.type == 'change' || event.type == TextEvent.TEXT_INPUT || isDropdownSelected){
				isDropdownSelected = false;
				dispatchEvent(new Event(CHANGE));
			}
		}

		public function fullText():String{
			var pattern:RegExp = /[_]/ 
			var fullText:String = (textInput as MaskedTextInput).fullText.split(pattern).join('');
			return fullText;
		}

		/**
		* we need to redeclated  DateField.stringToDate function to process "/" symbol correctly 
		* @param valueString string (from MaskedTextInput instance) 
		* @param inputFormat defined with inputMask or default masked format 
		* @return Date (used in DateChooser open() function)
		*/
		public static function stringToDate(valueString:String, inputFormat:String):Date{
			var mask:String
			var temp:String;
			var dateString:String = "";
			var monthString:String = "";
			var yearString:String = "";
			var j:int = 0;
			
			var n:int = inputFormat.length;
			for (var i:int = 0; i < n; i++,j++){
				temp = "" + valueString.charAt(j);
				mask = "" + inputFormat.charAt(i);
				
				//cose we processing change event to
				if(temp == "_"){
					temp="0";
				}
				
				if(mask == "M"){
					if(isNaN(Number(temp)) || temp == " ")
						j--;
					else
						monthString += temp;
				}
				else if(mask == "D"){
					if(isNaN(Number(temp)) || temp == " ")
						j--;
					else
						dateString += temp;
				}
				else if(mask == "Y"){
					yearString += temp;
				}
				else if(mask == "/"){
					//just avoid null return
				}
				else if(!isNaN(Number(temp)) && temp != " "){
					return null;
				}
			}
			
			if(monthString == "") monthString = "01";
			if(dateString == "") dateString = "01";
			if(yearString == "") yearString = "1900";
			
			temp = "" + valueString.charAt(inputFormat.length - i + j);
			
			if(!(temp == "") && (temp != " "))
				return null;
			
			var monthNum:Number = Number(monthString);
			var dayNum:Number = Number(dateString);
			var yearNum:Number = Number(yearString);
			
			if(isNaN(yearNum) || isNaN(monthNum) || isNaN(dayNum))
				return null;
			
			if(yearString.length == 2 && yearNum < 70)
				yearNum += 2000;
			
			var newDate:Date = new Date(yearNum, monthNum - 1, dayNum, 12);
			
			if(dayNum != newDate.getDate() || (monthNum - 1) != newDate.getMonth())
				return null;
			
			return newDate;
		}

		/**
		* we need to override this measure() function to increase measuredMinWidth
		* (fast solution, some smart measurement needed here :))
		*/
		override protected function measure():void{
			var buttonWidth:Number = downArrowButton.getExplicitOrMeasuredWidth();
			var buttonHeight:Number = downArrowButton.getExplicitOrMeasuredHeight();
			
			var bigDate:Date = new Date(2004, 12, 31, 12);
			var txt:String = (labelFunction != null) ? labelFunction(bigDate) : dateToString(bigDate, formatString);
			
			measuredMinWidth = measuredWidth = measureText(txt).width + 25 + buttonWidth;
			
			if(FlexVersion.compatibilityVersion >= FlexVersion.VERSION_3_0)
				measuredMinWidth = measuredWidth += getStyle("paddingLeft") + getStyle("paddingRight");
			
			measuredMinHeight = measuredHeight = textInput.getExplicitOrMeasuredHeight();
		}
	}
}