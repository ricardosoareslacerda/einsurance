package br.com.tratomais.einsurance.core.components.events
{
	import flash.events.Event;

	public class RiskTypeEvent extends Event
	{
		public static const VALUE_CHANGED:String = "valueChanged";
		public var obj:Object=new Object();
		
		public function RiskTypeEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}