package br.com.tratomais.einsurance.core.components.CityCombo.view.mediator
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.components.CityCombo.view.component.CityCombo;
	import br.com.tratomais.einsurance.customer.model.proxy.CityProxy;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	 
	public class CityComboMediator extends Mediator implements IMediator
	{ 
		[Bindable]public var identifiedList:IdentifiedList;
		
		private var cityProxy:CityProxy;
		private var resultArray:ArrayCollection; 
		
		public function CityComboMediator(viewComponent:Object)
		{
			super(viewComponent.id, viewComponent);
		}
		
	    protected function get cityCombo(): CityCombo{
            return viewComponent as CityCombo;
        }
		
		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */
		override public function onRegister():void 
		{
			super.onRegister();
			
			cityProxy = CityProxy(facade.retrieveProxy(CityProxy.NAME));
			
			identifiedList = new IdentifiedList();
			identifiedList.groupId = cityCombo.groupId;
			
			
			cityCombo.addEventListener(CityCombo.LOAD_CITY_COMBO, loadCity);
		}
		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();			
		}
		
        override public function listNotificationInterests():Array  
        {  
            return [NotificationList.CITY_LISTED];
        }
        
        override public function handleNotification(notification:INotification):void  
        {  
            switch ( notification.getName() )  
            {
            	case NotificationList.CITY_LISTED:
		        	var result: IdentifiedList = notification.getBody() as IdentifiedList;
		        	if(result.groupId == cityCombo.groupId){
		        		cityCombo.dataProvider = result.resultArray;		        		
		        	} 
	                break;
            }
        }
        
        private function loadCity(event:Event):void
        {
        	cityProxy.listCityByState(identifiedList, cityCombo.stateId);
        }
	}
}