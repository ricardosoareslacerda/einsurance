package br.com.tratomais.einsurance.core.components
{
	import flexlib.containers.WindowShade;

	import mx.effects.Resize;
	import mx.events.EffectEvent;

	public class WindowShadeCustom extends WindowShade{

		private var _resize:Resize;
		private var _newHeight:int;

		override protected function runResizeEffect():void{
			if(_resize && _resize.isPlaying){
				_resize.end();
			}
			
			_resize = new Resize(this);
			
			var tempHeight:int;
			if(opened)
				tempHeight = _newHeight > 0 ? _newHeight : measuredHeight;
			else
				tempHeight = measuredHeight;
			
			_resize.heightTo = Math.min(maxHeight, tempHeight);
			_resize.duration = 300;
			
			_resize.play();
		}

		public function resize():void{
			runResizeEffect();
		}

		public function get newHeight():int{
			return _newHeight;
		}

		public function set newHeight(pData:int):void{
			_newHeight = pData;
		}
	}
}