package br.com.tratomais.einsurance.core.components.renderer
{
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.products.model.vo.CoverageOption;
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	
	import mx.controls.TextInput;

	public class TextInputRenderer extends TextInput
	{
		private var _coverageOption:CoverageOption;
		private var _object:Object;
		
		public function TextInputRenderer()
		{
			super();
		}
		
		public function get coverageOption():CoverageOption
		{
			return _coverageOption;
		}
		
		public function set coverageOption( pData:CoverageOption ):void
		{
			this._coverageOption = pData;
		}
		
		public function get object():Object
		{
			return _object;
		}
		
		public function set object( pData:Object ):void
		{
			this._object = pData;
		}
		
		public function createNumberValidator( nameText:String ):void
		{
			id = 'txt' + nameText;
			name = nameText;
			restrict = Utilities.ONLY_NUMBER;
			setStyle( 'textAlign', 'right' );
			addEventListener( KeyboardEvent.KEY_UP, formatNumber, false, 0, true );
		}
		
		override public function set text(value:String):void
		{
			super.text = value;
			Utilities.redBorderField( this, false );
		}
		
		private function formatNumber( evt:Event ):void
		{
			Utilities.formatNumber( this, 2 );
		}
	}
}