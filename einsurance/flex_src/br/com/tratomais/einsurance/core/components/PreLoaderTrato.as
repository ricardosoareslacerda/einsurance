package br.com.tratomais.einsurance.core.components
{
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.events.FlexEvent;
	import mx.preloaders.DownloadProgressBar;
	import mx.preloaders.IPreloaderDisplay;
	
	public class PreLoaderTrato extends DownloadProgressBar implements IPreloaderDisplay
	{
      	[Embed(source="assets/images/fundo_wide.jpg", mimeType="application/octet-stream")]
        [Bindable] 
        public var fundo:Class;
        
		public var m_Progress: ProgressBar;
		private var m_Timer: Timer;
        private var	bitmapData:BitmapData;
        		
		public function PreLoaderTrato(): void{
			super();

			m_Progress = new ProgressBar;	
			this.addChild(m_Progress);
			m_Timer = new Timer(1);
			m_Timer.addEventListener(TimerEvent.TIMER, timerEventHandler);
			m_Timer.start();
		}
		
		override public function set preloader(value:Sprite):void{
			value.addEventListener(ProgressEvent.PROGRESS, progressEventHandler);
			value.addEventListener(Event.COMPLETE, completeEventHandler);
			value.addEventListener(FlexEvent.INIT_PROGRESS, initProgressEventHandler);
			value.addEventListener(FlexEvent.INIT_COMPLETE, initCompleteEventHandler);
		}
		
		private function progressEventHandler(event: ProgressEvent): void{
			var progress: Number = event.bytesLoaded / event.bytesTotal * 100;
			if (m_Progress){
				m_Progress.progress = progress;
			}
		}
		
		private function timerEventHandler(event: TimerEvent): void{
			this.stage.addChild(this);
			var width: Number = this.stage.stageWidth * 40 / 100; // Get 40% for the Stage width
			// Set the Position of the Progress bar to the middle of the screen
			m_Progress.x = (this.stage.stageWidth - m_Progress.getWidth()) / 2;
			m_Progress.y = (this.stage.stageHeight - m_Progress.getHeight()) / 2;
			m_Progress.refreshProgressBar();
		}
		
		private function completeEventHandler(event: Event): void{
			var i: int = 0;
		}
		
		private function initProgressEventHandler(event: FlexEvent): void{
			var i: int = 0;
		}
		
		private function initCompleteEventHandler(event: FlexEvent): void{
			m_Progress.ready = true;
			m_Timer.stop();
			this.dispatchEvent(new Event(Event.COMPLETE));
		}

        override public function get backgroundImage():Object{
            return fundo;
        }
   
        override public function get backgroundSize():String{
            return "100%";
        }
                
       	override protected function showDisplayForInit(elapsedTime:int,
                                      count:int):Boolean {
        	return true;
        }
     
        override protected function showDisplayForDownloading(elapsedTime:int,
                                     event:ProgressEvent):Boolean {
        	return true;
        }        
	}
}