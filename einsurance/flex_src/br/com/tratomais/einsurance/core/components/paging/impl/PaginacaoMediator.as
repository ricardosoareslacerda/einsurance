package br.com.tratomais.einsurance.core.components.paging.impl {
	
	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.components.paging.PaginacaoUI;
	import br.com.tratomais.einsurance.core.components.paging.vo.Paginacao;
	import br.com.tratomais.einsurance.core.components.paging.vo.PagingParameters;
	
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.resources.ResourceManager;

	/**
	 * 
	 * @author Fabiel
	 * 
	 */
	public class PaginacaoMediator extends PaginacaoUI {
			
		private var _pagingParameters:PagingParameters = new PagingParameters();
		private var _paginacao:Paginacao;
		private var _totalPaginas:int;

		public function set pagingParameters(value:PagingParameters):void{
			_pagingParameters = value;
		}

		public function get pagingParameters():PagingParameters{
			return _pagingParameters;
		}
		
		public function set paginacao(value:Paginacao):void{
			_paginacao = value;
			if(value.totalDados > 0){
				this.atualizarBarraBotoes();
			}else{
				this.stateInitial();
			}
		}

		public function get paginacao():Paginacao{
			return _paginacao;
		}
		
		public function aumentarPaginacao( aumentarPaginacao:Boolean ):void
		{
			if( aumentarPaginacao && cbIntevalo != null )
			{
				var newDataprovider:ArrayCollection = new ArrayCollection();
				
				var obj1:Object = new Object();
				obj1.value = 10;
				obj1.label = "10";
				
				var obj2:Object = new Object();
				obj2.value = 15;
				obj2.label = "15";				

				var obj3:Object = new Object();
				obj3.value = 20;
				obj3.label = "20";
				
				var obj4:Object = new Object();
				obj4.value = 25;
				obj4.label = "25";
				
				var obj5:Object = new Object();
				obj5.value = 50;
				obj5.label = "50";
				
				var obj6:Object = new Object();
				obj6.value = 100;
				obj6.label = "100";
				
				newDataprovider.addItem( obj1 );
				newDataprovider.addItem( obj2 );
				newDataprovider.addItem( obj3 );
				newDataprovider.addItem( obj4 );
				newDataprovider.addItem( obj5 );
				newDataprovider.addItem( obj6 );

				cbIntevalo.dataProvider = newDataprovider;
				cbIntevalo.data = newDataprovider;			
			}
		}
						
		public function PaginacaoMediator() {
			super();
			this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreate);
		}
		
		override protected function childrenCreated():void{
			super.childrenCreated();
			
			/* Adicionando os Listeners */
			this.bpPrimeiraPagina.addEventListener(MouseEvent.CLICK, trateExibirPrimeiraPagina);
			this.bpPaginaAnterior.addEventListener(MouseEvent.CLICK, trateExibirPaginaAnterior);
			this.bpProximaPagina.addEventListener(MouseEvent.CLICK, trateExibirProximaPagina);
			this.bpUltimaPagina.addEventListener(MouseEvent.CLICK, trateExibirUltimaPagina);
			
			this.cbIntevalo.addEventListener(ListEvent.CHANGE, trateTrocaIntervalo);
		}
		
		override public function invalidateProperties():void{
			super.invalidateProperties();
		}
		
		/**
		 * inicializacao do componente
		 */
		private function onCreate(evt:FlexEvent):void{
			pagingParameters.page = 0;
			pagingParameters.totalPage = 10;
			pagingParameters.currentPage = 0;
			this.stateInitial();
		}
		
		/**
		 * Atualiza os label com numeração da pagina atual e total e configura os botoes de paginacao
		 */
		private function atualizarBarraBotoes():void{
			var resultTotalPages:Number = Number(paginacao.totalDados / pagingParameters.totalPage);
			var difResult:Number = Number((paginacao.totalDados % pagingParameters.totalPage)!=0?1:0);
			
			_totalPaginas = int(resultTotalPages + difResult);
			
			if(pagingParameters.currentPage > _totalPaginas){
				pagingParameters.currentPage = 1;
			}
			
			var _messageParameters:Array = new Array(pagingParameters.currentPage, _totalPaginas);
			
			if(pagingParameters.currentPage > 1){
				configBpPaginaAnterior();
			} else {
				configBpPaginaAnterior(false);
			}
			
			if(pagingParameters.currentPage == _totalPaginas){
				configBpProximaPagina(false);
			} else {
				configBpProximaPagina();
			}
			
			lbPaginaDePara.text = ResourceManager.getInstance().getString('resources', 'pagingFromTo', _messageParameters);
			this.indexTotalPage(pagingParameters.totalPage);
		}
		
		/**
		 * configura os botoes e combo para o status inicial
		 */
		public function stateInitial():void{
			configBpPaginaAnterior(false);
			configBpProximaPagina(false);
			lbPaginaDePara.text = "";
			cbIntevalo.selectedIndex = 0;
		}
		
		private function indexTotalPage(totalPage:int):void{
			switch(totalPage){
				case 10:
					cbIntevalo.selectedIndex = 0;
					break;
				case 15:
					cbIntevalo.selectedIndex = 1;
					break;
				case 20:
					cbIntevalo.selectedIndex = 2;
					break;
				case 25:
					cbIntevalo.selectedIndex = 3;
					break;
				case 50:
					cbIntevalo.selectedIndex = 4;
					break;
				case 100:
					cbIntevalo.selectedIndex = 5;
					break;										
				default:
					cbIntevalo.selectedIndex = 0;
					break;
			}
		}
		
		/**
		 * @private 
		 * Configura e renderiza os dados Base para ser amostrado na tela levendo-se em consideração o intervalo passado.
		 * @param intervloIncial
		 * 
		 */
		private function configurarListaNaPagina(page:int):void{
			pagingParameters.page = ((pagingParameters.currentPage - 1) * pagingParameters.totalPage);
			
			ApplicationFacade.getInstance().sendNotification(NotificationList.GRIDVIEW_UPDATE_LIST, pagingParameters);
		}

		/**
		 * @private
		 * Fica escutando quando o usuario trocou o total de interva de dados a ser amostrado na lista 
		 * @param evt
		 */
		private function trateTrocaIntervalo(evt:ListEvent):void{
			pagingParameters.totalPage = cbIntevalo.selectedItem.value;
			
			if(pagingParameters.currentPage != 0){
				pagingParameters.page = 1;
				pagingParameters.currentPage = 1;
				
				configurarListaNaPagina(pagingParameters.page);
			}
		}

		/**
		 * @private 
		 * Responsavel por exibir os dados da primeira pagina.
		 */
		private function trateExibirPrimeiraPagina(evt:MouseEvent):void{
			pagingParameters.page = 1;
			pagingParameters.currentPage = 1;
			pagingParameters.componentId = id;
			
			configurarListaNaPagina(pagingParameters.page);
		}

		/**
		 * @private 
		 * Responsavel por exibir os dados da pagina anterior.
		 */
		private function trateExibirPaginaAnterior(evt:MouseEvent):void{
			pagingParameters.currentPage = (pagingParameters.currentPage >= 1) ? pagingParameters.currentPage - 1 : 1;
			pagingParameters.componentId = id;
			
			configurarListaNaPagina(pagingParameters.currentPage);
		}

		/**
		 * @private 
		 * Responsavel por amostrar os dados da proxima pagina
		 */
		private function trateExibirProximaPagina(evt:MouseEvent):void{
			/* Verifica qual a pagina em que esta para pegar a proxima */
			pagingParameters.currentPage = (pagingParameters.currentPage < _totalPaginas) ? pagingParameters.currentPage + 1 : _totalPaginas;
			pagingParameters.componentId = id;
			
			configurarListaNaPagina(pagingParameters.currentPage);
		}

		/**
		 * @private 
		 * Responsavel por exibir os dados da ultima pagina.
		 */
		private function trateExibirUltimaPagina(evt:MouseEvent):void{
			pagingParameters.currentPage = _totalPaginas;
			pagingParameters.componentId = id;
			
			configurarListaNaPagina(pagingParameters.currentPage);
		}
				
		private function configBpPaginaAnterior(value:Boolean = true):void {
			this.bpPaginaAnterior.enabled = value;
			this.bpPrimeiraPagina.enabled = value;
		}

		private function configBpProximaPagina(value:Boolean = true):void {
			this.bpProximaPagina.enabled = value;
			this.bpUltimaPagina.enabled = value;
		}
	}
}