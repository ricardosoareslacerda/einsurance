package br.com.tratomais.einsurance.core.components.validators
{
	import mx.validators.NumberValidator;

	public class TextInputValidator extends NumberValidator
	{
		private var _id:String;
		
		public function TextInputValidator()
		{
			super();
			decimalSeparator = ".";
			allowNegative = false;
			requiredFieldError = resourceManager.getString('messages', 'thisFieldRequired');
			exceedsMaxError = resourceManager.getString('messages', 'exceedsMaxError');
			maxValue = 1000000000;
			precision = 2;
			required = true;
			property = 'text';			
		}
		
		public function set id( pData:String ):void
		{
			this._id = 'vld' + pData;
		}
		
		public function get id():String
		{
			return this._id;
		}
	}
}