package br.com.tratomais.einsurance.core.components.CountryCombo.view.component
{
	import br.com.tratomais.einsurance.core.components.CustomComboBox;
	import br.com.tratomais.einsurance.core.components.StateCombo.view.component.StateCombo;
	import br.com.tratomais.einsurance.customer.model.vo.Address;
	
	import flash.events.Event;
	
	import mx.events.ListEvent;

	[Exclude(name="dataProperty", kind="property")]
	[Exclude(name="labelField", kind="property")]

	public class CountryCombo extends CustomComboBox
	{
		public static const LOAD_COUNTRY_COMBO:String = "LOAD_COUNTRY_COMBO";
		 
		private var _groupId:String;

		[Bindable] private var isPermitModification:Boolean;
		[Bindable] private var _countryId:int;
		[Bindable] private var _stateCombo:StateCombo;
		[Bindable] private var _address:Object;
		
		public function CountryCombo()
		{
			super();

			this.labelField="name";
			this.dataProperty="countryId";
			this.toolTip= resourceManager.getString('resources', 'pleaseSelectCountry');
			
			this.addEventListener(ListEvent.CHANGE, changedList);
		}
		
		public function set groupId(pData:String):void{
			_groupId = pData;
		} 

		public function get groupId():String{
			return _groupId;
		} 
		
		public function set countryId(countryId:int):void
		{
			this._countryId = countryId;
		}
		
		public function get countryId():int
		{
			return this._countryId;
		}

		public function set stateCombo(pData:StateCombo):void
		{
			this._stateCombo = pData;
		}
		
		public function get stateCombo():StateCombo
		{
			return this._stateCombo;
		}
		
		public function set address(address:Object):void
		{
			this._address = address;
			
			if(_address)
				dispatchEvent(new Event(LOAD_COUNTRY_COMBO));
		}
		
		public function get address():Object{
			return _address;
		}

		override public function set enabled(value:Boolean):void
		{
			super.enabled = value;
			isPermitModification = value;
		}
		
		override public function set dataProvider(value:Object):void
		{
			super.dataProvider = value;
			
			selectedItem = null;
			
			if(value && _address)
			{
				selectedValue = _address.countryId;
				super.enabled = isPermitModification;
				commitProperties();
				
				dispatchEvent(new ListEvent(ListEvent.CHANGE));
			}
		}
		
		private function changedList(event:ListEvent):void
		{
			if(stateCombo)
			{
				stateCombo.countryId = selectedItem == null? null : selectedItem.countryId;
			}
			
			if(_address)
			{
				_address.countryId = selectedItem == null? null : selectedItem.countryId;
				_address.countryName = selectedItem == null? '' : selectedItem.name;
			}
		}
	}
}