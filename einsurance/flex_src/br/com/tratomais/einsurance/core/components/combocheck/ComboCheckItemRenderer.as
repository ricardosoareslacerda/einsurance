package br.com.tratomais.einsurance.core.components.combocheck {
    import flash.events.Event;
    import flash.events.MouseEvent;
    
    import mx.controls.Alert;
    import mx.controls.CheckBox;
    import mx.events.FlexEvent;
    
    public class ComboCheckItemRenderer extends CheckBox {
        
        public function ComboCheckItemRenderer() {
            super();
            addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
            addEventListener(MouseEvent.CLICK,onClick);
        }
        
        private function onCreationComplete(event:Event=null):void {
            if (data!= null && data.transientSelected==true) {
                selected=true;
            }
        }
        
        private function onClick(event:MouseEvent):void {
            data.transientSelected=selected;
            var myComboCheckEvent:ComboCheckEvent=new ComboCheckEvent(ComboCheckEvent.COMBO_CHECKED);
            myComboCheckEvent.obj=data;
            owner.dispatchEvent(myComboCheckEvent);
        }
        
        override protected function commitProperties():void
        {
        	super.commitProperties();
        	if( data )
        	{
        		var implemented : Boolean = data.hasOwnProperty("transientCompulsory");
        		if( implemented )
        		{
        			enabled = !data.transientCompulsory;
        			selected = data.transientSelected;
        		}
        	}
        }
    }
}

