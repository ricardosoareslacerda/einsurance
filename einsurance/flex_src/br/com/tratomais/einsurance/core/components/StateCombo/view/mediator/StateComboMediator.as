package br.com.tratomais.einsurance.core.components.StateCombo.view.mediator
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.components.StateCombo.view.component.StateCombo;
	import br.com.tratomais.einsurance.customer.model.proxy.StateProxy;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	 
	public class StateComboMediator extends Mediator implements IMediator
	{ 
		[Bindable]public var identifiedList:IdentifiedList;
		
		private var stateProxy:StateProxy;
		private var resultArray:ArrayCollection; 
		
		public function StateComboMediator(viewComponent:Object)
		{
			super(viewComponent.id, viewComponent);
		}
		
	    protected function get stateCombo(): StateCombo {
            return viewComponent as StateCombo;
        }
		
		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */
		override public function onRegister():void 
		{
			super.onRegister();
			stateProxy = StateProxy(facade.retrieveProxy(StateProxy.NAME));
			
			stateCombo.addEventListener(StateCombo.LOAD_STATE_COMBO, loadStateCombo);
			
			identifiedList = new IdentifiedList();
			identifiedList.groupId = stateCombo.groupId;
		}
		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
			stateCombo.removeEventListener(StateCombo.LOAD_STATE_COMBO, loadStateCombo);
		}
		
        override public function listNotificationInterests():Array  
        {
            return [NotificationList.STATE_LISTED];
        }
        
        override public function handleNotification(notification:INotification):void  
        {  
            switch ( notification.getName() )  
            {
            	case NotificationList.STATE_LISTED:
		        	var result: IdentifiedList = notification.getBody() as IdentifiedList;
		        	if(result.groupId == stateCombo.groupId){
		        		stateCombo.dataProvider = result.resultArray;
		        	}
					break;
            }
        }
        
        private function loadStateCombo(event:Event):void
        {
        	stateProxy.listStateByCountry(identifiedList, stateCombo.countryId);
        }
	}
}