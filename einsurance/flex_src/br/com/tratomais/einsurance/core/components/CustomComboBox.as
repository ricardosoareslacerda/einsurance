package br.com.tratomais.einsurance.core.components{
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.customer.model.vo.DataOption;
	
	import flash.events.Event;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	import mx.controls.ComboBox;
	
	

[Exclude(name="editable", kind="property")]



public class CustomComboBox extends ComboBox {
		
    /**
     * The VBComboBox (Value Bound ComboBox) control extends the functionality
     * of the standard ComboBox to allow the user to set a selectedValue
     * property that will automatically select the value in the dataProvider.
     *
     * <p>The <code>&lt;mx:VBComboBox&gt;</code> tag inherits all the tag
     * attributes of its superclass, and adds the following tag attributes:</p>
     *
     * <pre>
     * &lt;mx:VBComboBox
     *   <b>Properties</b>
     *   dataProperty="data"
     *   selectedValue="null"
     *  /&gt;
     *  </pre>
     */

    //---------------------------------------------------------------------
    //
    //  Variables
    //
    //---------------------------------------------------------------------

    //Indicator that the dataProvider has changed.
    private var dataProviderChanged:Boolean=false;

    //Indicator that the selectedValue has changed.
    private var selectedValueChanged:Boolean=false;

    //---------------------------------------------------------------------
    //
    //  Overridden properties
    //
    //---------------------------------------------------------------------

    //----------------------------------
    //  dataProvider
    //----------------------------------
    /**
     * @private
     * This override executes the superclass, and then sets the indicator
     * that the dataProvider has changed.
     **/
    override public function set dataProvider(value:Object):void
    {
        dataProviderChanged=true;
        super.dataProvider=value;
        Utilities.redBorderField(this, false);
    }

    //---------------------------------------------------------------------
    //
    //  Properties
    //
    //---------------------------------------------------------------------

    //----------------------------------
    //  dataProperty
    //----------------------------------
    /**
     * dataProperty identifies the property that contains the selectedValue
     * in the dataProvider objects.
     */
    public var dataProperty:String="data";

    //----------------------------------
    //  selectedValue
    //----------------------------------
    private var _selectedValue:*;

    /**
     * selectedValue is the value that will be searched within the
     * dataProperty property of the objects in the dataProvider.
     */
    public function get selectedValue():*
    {
		var item:Object = selectedItem;

		if (item == null || typeof(item) != "object"){
			return item;
		}else if(item.hasOwnProperty(dataProperty)) {
			return item[dataProperty];
		}
        return _selectedValue;
    }

    /**
     * @private
     **/
    public function set selectedValue(val:*):void
    {
        _selectedValue=val;
        selectedValueChanged=true;
        //invalidateProperties to force commitProperties to be executed.
        invalidateProperties();
    }

	[Bindable]private var _blankField:ArrayCollection;
	/**
	 * Sorted fields of the dataProvider and add one empty field
	 * */
	public function set blankField( blankField:ArrayCollection ):void{
		if( blankField )
		{
			_blankField = new ArrayCollection();
			var dataOption:DataOption = new DataOption();
			dataOption.fieldDescription = "";
			dataOption.description = "";
			_blankField.addItem(dataOption);
			for(var i:int = 0; i < blankField.length; i++ )
				_blankField.addItem(blankField.getItemAt(i));
            dataProvider = _blankField;
            
		}
	}
	private function clone(source:Object):*
	{
	    var buffer:ByteArray = new ByteArray();
	    buffer.writeObject(source);
	    buffer.position = 0;
	    return buffer.readObject();
	}

    //---------------------------------------------------------------------
    //
    //  Overridden methods
    //
    //---------------------------------------------------------------------
    /**
     * @private
     * This override executes the superclass, and sets the selectedIndex
     * of the selectedValue if it can be found in the dataProvider.
     **/
    override protected function commitProperties():void
    {
        super.commitProperties();

        //If the selectedValue and dataProvider are set, find the selectedIndex of the value.
        if ((selectedValueChanged == true && dataProvider != null) ||
            (dataProviderChanged == true && _selectedValue != undefined))
        {
            dataProviderChanged=false;
            selectedValueChanged=false;

            var idx:int=-1;

            //Loop through data provider until a record with the value is found.
            for (var i:int=0; i < dataProvider.length; i++)
            {
                if (this.dataProvider[i] != null &&
                    this.dataProvider[i].hasOwnProperty(dataProperty) &&
                    this.dataProvider[i][dataProperty] == _selectedValue)
                {
                    idx=i;
                    break;
                }
            }

            //Set the selectedIndex with the index found or -1 if not found.
            this.selectedIndex=idx;
        }
    }

    /**
     * @private
     * This override executes the superclass, and calls invalidateProperties to
     * force the commitProperties since there were changes in the dataProvider.
     */
    override protected function collectionChangeHandler(event:Event):void
    {
        super.collectionChangeHandler(event);

        dataProviderChanged=true;
        invalidateProperties();
    }
    
    override public function set selectedIndex(value:int):void
    {
    	super.selectedIndex=value;
    	Utilities.redBorderField(this, false);
    }
			
}
}