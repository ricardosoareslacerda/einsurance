package br.com.tratomais.einsurance.core.components.CityCombo.view.component
{
	import br.com.tratomais.einsurance.core.components.advancedAutoComplete.components.AdvancedAutoComplete;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ListCollectionView;
	import mx.events.ListEvent;
	import mx.events.MenuEvent;
	import mx.resources.ResourceManager;

	[Exclude(name="dataProperty", kind="property")]
	[Exclude(name="labelField", kind="property")]

	public class CityCombo extends AdvancedAutoComplete
	{
		public static const CLEAR_CITY_COMBO:String = "CLEAR_CITY_COMBO";
		public static const LOAD_CITY_COMBO:String = "LOAD_CITY_COMBO";
				 
		[Bindable] private var _stateId:int;
		[Bindable] private var _cityId:int;
		[Bindable] private var _address:Object;
		[Bindable] private var isPermitModification:Boolean;
		private var _groupId:String;
		
		public function CityCombo()
		{	
			labelField="name";
			keyField="cityId";
			useListBuilder=false; 
			matchType="anyPart";
			showBrowseButton = false;
			showOrderButtons=false;
			allowMultipleSelection=false; 
		    selectionLayout="horizontal";
		    actionsMenuDataProvider=ResourceManager.getInstance().getString("resources","browser");
		    showRemoveButton=false;		    
		    browserFields=['name'];
		    setStyle("selectedItemStyleName","none");
			toolTip=resourceManager.getString('resources', 'pleaseSelectCity');		    
		    showBrowser();
		    enabled=false;
		    
		    this.addEventListener(ListEvent.CHANGE, changedList);
		    this.addEventListener(MenuEvent.ITEM_CLICK, handleItemClick);
		}
		
	    
		public function set groupId(pData:String):void{
			_groupId = pData;
		} 

		public function get groupId():String{
			return _groupId;
		} 
		
		public function CleanCombo():void{
			selectedItemId = 0;
			textInput.text = '';
			
			commitProperties();
		}
		
		public function set selectedValue(data:String):void{
			selectedItemId = Number(data);			
		}
		
		public function get selectedValue():String{
			return selectedItemIdAsString;
		}
		
		public function set stateId(stateId:int):void
		{
			this._stateId = stateId;

			CleanCombo();
			
			if(stateId > 0)
			{			
				this.dispatchEvent(new Event(LOAD_CITY_COMBO));
				super.enabled = isPermitModification;
			}
			else
			{
				super.enabled = false;
			}
		}
		
		public function get cityId():int{
			return this._cityId;
		}

		public function set cityId(cityId:int):void{
			this._cityId = cityId;
		}
		
		public function set address(address:Object):void
		{
			this._address = address;
			super.enabled = false;
			
			if(_address)
			{
				cityId = _address.cityId;
				selectedItemId = _address.cityId;
			}
		}
		
		public function get address():Object{
			return _address;
		}
		
		public function get stateId():int
		{
			return this._stateId;
		}
		
		override public function set enabled(value:Boolean):void
		{
			super.enabled = value;
			isPermitModification = value;
		}

		override public function set dataProvider(value:ListCollectionView):void
		{
			super.dataProvider = value;
			
			if(value && _address)
			{
				selectedItemId = _address.cityId;

				commitProperties();

				dispatchEvent(new ListEvent(ListEvent.CHANGE));
			}
		}
		
		private function changedList(event:ListEvent):void
		{
			if(_address)
			{
				_address.cityId = selectedItem == null? null : selectedItem.cityId;
				_address.cityName = selectedItem == null? '' : selectedItem.name;
			}
		}
		
		private function handleItemClick(event:MenuEvent):void
		{
			this.showBrowser();
		}
	}
}