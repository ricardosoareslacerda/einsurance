package br.com.tratomais.einsurance.core.components.StateCombo.view.component
{
	import br.com.tratomais.einsurance.core.components.CityCombo.view.component.CityCombo;
	import br.com.tratomais.einsurance.core.components.advancedAutoComplete.components.AdvancedAutoComplete;
	
	import flash.events.Event;
	
	import mx.collections.ListCollectionView;
	import mx.events.ListEvent;
	import mx.events.MenuEvent;
	import mx.resources.ResourceManager;

	[Exclude(name="dataProperty", kind="property")]
	[Exclude(name="labelField", kind="property")]

	public class StateCombo extends AdvancedAutoComplete
	{
		public static const CLEAR_STATE_COMBO:String = "CLEAR_STATE_COMBO";
		public static const LOAD_STATE_COMBO:String = "LOAD_STATE_COMBO";
		
		[Bindable] private var _countryId:int;
		[Bindable] private var _stateId:int;
		[Bindable] private var _cityCombo:CityCombo;
		[Bindable] private var _address:Object;
		[Bindable] private var isPermitModification:Boolean;
		 
		private var _groupId:String;
		
		public function StateCombo()
		{
			labelField="name";
			keyField="stateId";
			useListBuilder=false; 
			matchType="anyPart";
			showBrowseButton = false;
			showOrderButtons=false;
			allowMultipleSelection=false; 
		    selectionLayout="horizontal";
		    actionsMenuDataProvider=ResourceManager.getInstance().getString("resources","browser");
		    showRemoveButton=false;		    
		    browserFields=['name'];
		    setStyle("selectedItemStyleName","none");
		    showBrowser()
		    toolTip=resourceManager.getString('resources', 'pleaseSelectState');
		    
			this.addEventListener(ListEvent.CHANGE, changedList);
			this.addEventListener(MenuEvent.ITEM_CLICK, handleItemClick);
		}
		
	    public function set groupId(pData:String):void{
			_groupId = pData;
		} 

		public function get groupId():String{
			return _groupId;
		} 
		
		public function CleanCombo():void{
			
			selectedItem = null;
			selectedItemId = 0;
			textInput.text = '';
			
			commitProperties();
		}
		
		public function set selectedValue(data:String):void{
			selectedItemId = Number(data);			
		}
		
		public function get selectedValue():String{
			return selectedItemIdAsString;
		}
		
		public function set countryId(countryId:int):void
		{
			this._countryId = countryId;

			CleanCombo();
			
			if(countryId > 0)
			{			
				this.dispatchEvent(new Event(LOAD_STATE_COMBO));
				super.enabled = isPermitModification;
			}
			else
			{
				super.enabled = false;
			}
		}
		
		public function get countryId():int
		{
			return this._countryId;
		}
		
		public function get stateId():int{
			return this._stateId;
		}

		public function set stateId(stateId:int):void{
			this._stateId = stateId;
		}
		
		public function set cityCombo(cityCombo:CityCombo):void
		{
			this._cityCombo = cityCombo;
		}
		
		public function get cityCombo():CityCombo
		{
			return this._cityCombo;
		}

		public function set address(address:Object):void
		{
			this._address = address;
			super.enabled = false;
			
			if(_address)
			{
				stateId = _address.stateId;
				selectedItemId = _address.stateId;
			}
		}
		
		public function get address():Object{
			return _address;
		}
		
		override public function set enabled(value:Boolean):void
		{
			super.enabled = value;
			isPermitModification = value;
		}
		
		override public function set dataProvider(value:ListCollectionView):void
		{
			super.dataProvider = value;
			
			if(value && _address)
			{
				selectedItemId = _address.stateId;

				commitProperties();

				dispatchEvent(new ListEvent(ListEvent.CHANGE));
			}
		}
		
		private function changedList(event:ListEvent):void
		{
			if(cityCombo)
			{
				cityCombo.stateId = selectedItem == null? null : selectedItem.stateId;
			}
			
			if(_address)
			{
				_address.stateId = selectedItem == null? null : selectedItem.stateId;
				_address.stateName = selectedItem == null? '' : selectedItem.name;
			}
		}
		
		private function handleItemClick(event:MenuEvent):void
		{
			this.showBrowser();
		}
	}
}