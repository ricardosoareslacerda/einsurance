package br.com.tratomais.einsurance.core.components.paging.vo {
	import mx.collections.ArrayCollection;
	
	import net.digitalprimates.persistence.hibernate.HibernateBean;	
	
	[RemoteClass(alias="br.com.tratomais.core.model.paging.Paginacao")]
	[Managed]
	public class Paginacao extends HibernateBean {
		
		private var _totalDados:int;
		private var _listaDados:ArrayCollection = new ArrayCollection();  
	
		public function Paginacao() {
		}

		public function set totalDados(value:int):void{
			_totalDados = value;
		}

		public function get totalDados():int{
			return _totalDados;
		}

		public function set listaDados(value:ArrayCollection):void{
			_listaDados = value;
		}

		public function get listaDados():ArrayCollection{
			return _listaDados;
		}
	}
}