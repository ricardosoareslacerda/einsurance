package br.com.tratomais.einsurance.core.components.renderer
{
	import br.com.tratomais.einsurance.products.model.vo.CoverageOption;
	
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.AdvancedDataGrid;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridListData;
	
	public class AdvancedDataGridRenderer extends AdvancedDataGrid
	{
		public function AdvancedDataGridRenderer()
		{
			super();
		}
		
		override protected function initListData(item:Object, adgListData:AdvancedDataGridListData):void
		{
			if (item == null)
            return;
            
            if( item as CoverageOption )
            {
            	super.initListData(item, adgListData);
            }
            else if( item.children is ArrayCollection )
            {
				 var list:ArrayCollection = item.children as ArrayCollection;
				if(list.length > 1) 
		        	super.initListData(item, adgListData);
            }
            	
		}	
	}
}