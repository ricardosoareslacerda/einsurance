package br.com.tratomais.einsurance.core.components.paging.vo 
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;
	
    
    [RemoteClass(alias="br.com.tratomais.core.util.PagingParameters")]     
    [Managed]
    public class PagingParameters extends HibernateBean{  
        private var _currentPage:int;
        private var _page:int;
        private var _totalPage:int;
        private var _componentId:String;
  
        public function PagingParameters(){}  

		 public function get currentPage():int{
			return _currentPage;
		}
	
		 public function set currentPage(pData:int):void{
			_currentPage = pData;
		}
		
		 public function get page():int{
			return _page;
		}
	
		 public function set page(pData:int):void{
			_page = pData;
		}
	
		 public function get totalPage():int{
			return _totalPage;
		}
	
		 public function set totalPage(pData:int):void{
			_totalPage = pData;
		}
		
		public function get componentId():String{
			return _componentId;
		}
	
		 public function set componentId(pData:String):void{
			_componentId = pData;
		}
    }  
}