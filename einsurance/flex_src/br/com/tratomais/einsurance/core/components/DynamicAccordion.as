package br.com.tratomais.einsurance.core.components
{
	import flash.display.DisplayObject;
	import flash.utils.Dictionary;
	
	import mx.containers.Accordion;
	
	public class DynamicAccordion extends Accordion
    {
        private var _hiddenHeader:Dictionary=new Dictionary();

        public function DynamicAccordion()
        {
                super();
        }

        public function hideHeader(header:DisplayObject):void
        {
                if (contains(header))
                {
                        _hiddenHeader[header]=getChildIndex(header);
                        removeChild(header);
                }


        }

        public function showHeader(header:DisplayObject):void
        {
                if (!contains(header))
                {
                        addChildAt(header, _hiddenHeader[header]);
                        delete _hiddenHeader[header]
                }
        }

        public function isHeaderHidden(header:DisplayObject):Boolean
        {                                               
                for (var key:Object in _hiddenHeader)
                {
                        if (key==header)
                                return true;                            
                }

                return false;
        }
        
        public function onClean():void {
       		_hiddenHeader=new Dictionary();
        }
        
    }

}