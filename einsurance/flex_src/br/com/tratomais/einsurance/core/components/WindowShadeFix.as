package br.com.tratomais.einsurance.core.components
{
	import flexlib.containers.WindowShade;
	
	import mx.events.EffectEvent;
	
	public class WindowShadeFix extends WindowShade
	{
		private var _resizeAuto:Boolean;
		private var _newPercentHeight:Number;
		
		public function set resizeAuto( resizeAuto:Boolean ):void
		{
			this._resizeAuto = resizeAuto;
		}
		
		public function get resizeAuto():Boolean
		{
			return this._resizeAuto;
		}
		
		public function set newPercentHeight( newPercentHeight:Number ):void
		{
			this._newPercentHeight = newPercentHeight;
		}
		
		public function get newPercentHeight():Number
		{
			return this._newPercentHeight;
		}
		
		public function WindowShadeFix()
		{
			super();
			percentHeight = 100;
        	height = NaN;
        	invalidateProperties();
        	commitProperties();			
			addEventListener(EffectEvent.EFFECT_END, handleEffectEnd, false, 0, true);			
		}

		private function handleEffectEnd(event:EffectEvent):void {
			if( resizeAuto )
				percentHeight = 100;
			else
			{
				if( opened )
					percentHeight = newPercentHeight;
			}
				
        	height = NaN;
        	invalidateProperties();
        	commitProperties();
        }
        
        public function resize():void
        {
			if( resizeAuto )
				percentHeight = 100;
			else
			{
				if( opened )
					percentHeight = newPercentHeight;
			}
				
        	height = NaN;
        	invalidateProperties();
        	commitProperties();        	
        }
	}
}