package br.com.tratomais.einsurance.core.components
{
	import br.com.tratomais.einsurance.core.components.renderer.TextInputRenderer;
	import br.com.tratomais.einsurance.core.components.validators.TextInputValidator;
	
	import mx.containers.FormItem;

	public class FormItemCapital extends FormItem
	{
		private var textInput:TextInputRenderer = new TextInputRenderer;
		private var validator:TextInputValidator = new TextInputValidator;
		
		public function FormItemCapital(value:String)
		{
			super();
			id = 'frmt' + value;
			label = resourceManager.getString( 'resources', 'sumInsured' ) + ' ' + value + ':';
			percentWidth = 100;
			name = value;
			required = true;
			textInput.createNumberValidator( value );
			addChild( textInput );
			validator.id = value;
			validator.source = textInput;			
		}		
		
		public function getTextInput():TextInputRenderer
		{
			return textInput;
		}
		
		public function getValidator():TextInputValidator
		{
			return validator;
		}
		
		public function invalidate():void
		{
			
		}
	}
}