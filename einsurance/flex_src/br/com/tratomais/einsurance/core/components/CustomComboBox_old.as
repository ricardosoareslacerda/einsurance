package br.com.tratomais.einsurance.core.components{
	
import mx.collections.ArrayCollection;
import mx.collections.IViewCursor;
import mx.controls.ComboBox;
import mx.core.ClassFactory;
import mx.logging.ILogger;
import mx.logging.Log;

[Exclude(name="editable", kind="property")]

/** 
 *	<b>FEATURES</b>
 *	MatchComboBox a replacement for a standard non-editable combobox. But instead of
 * 	searching via the first character it uses the text input field  for an incremental 
 * 	search to match the text input against the list an show the actual match in the
 * 	dropdown list. 
 * 	It doesn't make sense in smaller lists, but in huge list it can be helpful.
 * 	See demo at http://blog.sbistram.de
 * 
 *  <pre>
 *  &lt;MatchComboBox
 *    <b>Properties</b>
 *    matchFromFirstPosition="false"
 *    matchCaseSensitive="false"
 *  /&gt;
 *  </pre>
 * 
 * 
 * <b>TODO's</b>
 * - Its a non-editable combobox, so the skin should changed dynamically. It should look like
 *   a non-editable when the component has no focus and like an editable when it has focus.
 * - The dropdown renderer has some HTML limitations and the match color should be a
 *   style property.
 * - UI unit tests are missing (Flex Monkeys).
 * - never ever used or tested in any serious application (so be careful!!!)
 * - Make an editable version (should be easy to do).
 * - what else..?
 * 
 * <b>OPEN BUGS/ISSUES</b> 
 * 1. Using the prompt property is not working correctly
 * 2. Flex 3.2 bug:
 *    Start editing in the input field will open the dropdown list, but when
 *    you resize the window so the dropdown list will close the dropdown list 
 *    will stay invisible (dropdown.visible returns true!)
 * 
 * @version 0.2
 */
public class CustomComboBox_old extends ComboBox {
		
	[Bindable] public var matchCaseSensitive:Boolean = false;
	[Bindable] public var matchFirstPosition:Boolean = false;
	
	private var _selectedValue:String;
	
	private var _valueField:String;
    
    private var bSelectedValueSet:Boolean = false;

	// match key or other key strokes like enter, arrow up/down, page up/down
	private var _matchKey:Boolean = true;

	// initial index must be kept for restoring
	private var _lastValidIndex:int = -1;
	
	// initial rowCount needed for restoring
	private var _initialRowCount:int;

	/**
	 * Constructor
	 */
	public function CustomComboBox_old() {
		super();
		editable = false;
		rowCount = 5;
	}
	
	override public function open():void {
		super.open();
	}
	
	override public function set text(value:String):void {
		super.text = value;
	}

	override public function set selectedItem(value:Object):void
	 {
		super.selectedItem = value;
		if (value != null && selectedIndex == -1)
		{
			// do a painful search;
			if (collection && collection.length)
			{
				var cursor:IViewCursor = collection.createCursor();
				while (!cursor.afterLast)
				{
					var obj:Object = cursor.current;
					var nope:Boolean = false;
					for (var p:String in value)
					{
						if (obj[p] !== value[p])
						{
							nope = true;
							break;
						}
					}
					if (!nope)
					{
						super.selectedItem = obj;
						return;
					}
					cursor.moveNext();
				}
			}
		}
	 }

     /**
     *  @private
     */
    // Override committ, this may be called repeatedly
    override protected function commitProperties():void
    {
        // invoke ComboBox version
        super.commitProperties();

        // If value set and have dataProvider
        if (bSelectedValueSet && super.dataProvider)
        {
            // Set flag to false so code won't be called until selectedValue is set again
            bSelectedValueSet=false;
            
            // Loop through dataProvider
            for (var i:int=0;i<this.dataProvider.length;i++)
            {
                // Get this item's data
                if(_valueField!=""){
                	var item:String = this.dataProvider[i][_valueField];
                }
                
                // Check if is selectedValue
                if(item == _selectedValue)
                {
                    // Yes, set selectedIndex
                    this.selectedIndex = i;
                    break;
                }
            }
        }
    }
    
    // set for selectedValue
    public function set selectedValue(s:String):void{
        // Set flag
        bSelectedValueSet = true;

        // Save value
        _selectedValue = s;

        // Invalidate to force commit
        invalidateProperties();
    }
 
 	public function set valueField(s:String):void{
 		
 		//set de value
 		_valueField=s;
 		
        // Invalidate to force commit
        invalidateProperties();     			
 	}
	
	/**
	 * Get the index of the matched item out of the collection
	 * 
	 * @param	value the 
	 * @param	forceExactMatch
	 * @return  the index of the match or -1
	 */
	private function getMatchIndex(value:String, forceExactMatch:Boolean):int {
		if (value.length == 0) {
			return -1;
		}
		var ac:Array = (dataProvider as ArrayCollection).source;
		for (var index:int = 0; index < ac.length; index++) {
			if (isMatch(itemToLabel(ac[index]), forceExactMatch)) {
				return index;
			}
		}
		return -1;
	}
	
	/**
     * Using <code>matchCaseSensitive</code> and <code>matchFirstPosition</code>
	 * to find a match
	 *
	 * @param	value of the collection
	 * @param	forceExactMatch = false
	 * @return  true if it is a match
	 */
	private function isMatch(value:String, forceExactMatch:Boolean = false):Boolean {
		var valueText:String = matchCaseSensitive ? value : value.toLowerCase();
		var matchText:String = matchCaseSensitive ? text : text.toLowerCase();
		if (forceExactMatch == true) {
			return (valueText == matchText);
		} else {
			if (matchFirstPosition == true) {
				return valueText.indexOf(matchText) == 0;
			} else {
				return valueText.indexOf(matchText) != -1;
			}
		}
	}
	
	/**
	 * Make sure the selection is valid, even if the input text doesn't match any item. 
	 */
	private function validateSelection():void {		
		// it is a difference whether we have a selection via a match or via an arrow selection
		// Example: Arkansas, Kansas -> input match text:kansas
		// 1. The match was given in via text field
		// 		Select the first entry in the list: Arkansas
		// 2. The match was given by arrow selection from dropdown list
		//		Select the exact entry: Kansas
		// In both cases the last keystroke could be ENTER, so we have to check if there was a 
		// selection done (selectedIndex).
		var index:int = getMatchIndex(text, selectedIndex != -1);		
		
		if (index == -1 && _lastValidIndex != -1) {	// no match, use last valid selection
			index = _lastValidIndex;
		} 
		
		if (index == -1 && prompt == null) {	// nothing was selected, use the default or prompt
			index = 0;
		} 
		
		_matchKey = false;
		(dataProvider as ArrayCollection).refresh();
		selectedIndex = index;		// if -1 -> show prompt
		_lastValidIndex = index;	// remember last valid selection
	}
			
}
}