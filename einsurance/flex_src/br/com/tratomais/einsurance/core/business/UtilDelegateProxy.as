package br.com.tratomais.einsurance.core.business
{
	import br.com.tratomais.einsurance.core.UtilitiesService;
	
	import mx.rpc.IResponder;
	public class UtilDelegateProxy
	{
		private var responder : IResponder;
		private var service : Object;
	
		public function UtilDelegateProxy(pResponder : IResponder )
		{
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_UTIL);
			responder = pResponder;
		}   

		public function getStringHash(value : String):void
		{
			var call:Object = service.getStringHash(value);
			call.addResponder(responder);
		}	
		
		public function getLoggedUser():void
		{
			var call:Object = service.getLoggedUser();
			call.addResponder(responder);
		}	
		
	}
}	