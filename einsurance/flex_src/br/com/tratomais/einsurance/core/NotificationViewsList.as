package br.com.tratomais.einsurance.core
{
	public class NotificationViewsList
	{
		
		public static const VIEW_WELCOME:String 							= "VIEW_WELCOME";
		public static const VIEW_USER_MAIN:String 							= "VIEW_USER_MAIN";
		public static const VIEW_ROLES_LIST:String 							= "VIEW_ROLES_LIST";
		public static const VIEW_PARTNER_MAIN:String 						= "VIEW_PARTNER_MAIN";
		public static const VIEW_BROKER_MAIN:String 						= "VIEW_BROKER_MAIN";
		public static const VIEW_CHANNEL_MAIN:String 						= "VIEW_CHANNEL_MAIN";
		public static const VIEW_SUBSIDIARY_MAIN:String 					= "VIEW_SUBSIDIARY_MAIN";
		public static const VIEW_PRODUCTS:String 							= "VIEW_PRODUCTS";
		public static const VIEW_QUOTE_MAIN:String 							= "VIEW_QUOTE_MAIN";
		public static const VIEW_RELOAD_QUOTE:String 						= "VIEW_RELOAD_QUOTE";
		public static const VIEW_PROPOSAL_MAIN:String 						= "VIEW_PROPOSAL_MAIN";
		public static const VIEW_MASTER_POLICY_MAIN:String					= "VIEW_MASTER_POLICY_MAIN";
		public static const VIEW_SEARCH_DOCUMENT:String						= "VIEW_SEARCH_DOCUMENT";
		public static const VIEW_CHANNEL_REPORT:String						= "VIEW_CHANNEL_REPORT";
		public static const VIEW_PRODUCT_REPORT:String						= "VIEW_PRODUCT_REPORT";
		public static const VIEW_SALES_REPORT:String						= "VIEW_SALES_REPORT";
		public static const VIEW_INSTALLMENT_REPORT:String					= "VIEW_INSTALLMENT_REPORT";
		public static const VIEW_COMMISSION_REPORT:String					= "VIEW_COMMISSION_REPORT";
		public static const VIEW_LABOUR_REPORT:String						= "VIEW_LABOUR_REPORT";
		public static const VIEW_ANNULMENT_REPORT:String					= "VIEW_ANNULMENT_REPORT";
		public static const VIEW_GENERAL_REPORT:String						= "VIEW_GENERAL_REPORT";
		public static const VIEW_PRODUCT_COPY:String						= "VIEW_PRODUCT_COPY";
		public static const VIEW_OPERATIONAL_MANUAL:String					= "VIEW_OPERATIONAL_MANUAL";
		public static const VIEW_TECHNICAL_MANUAL:String					= "VIEW_TECHNICAL_MANUAL";
		public static const VIEW_PASSWORD_CHANGE:String 					= "VIEW_PASSWORD_CHANGE";
		public static const VIEW_UNLOCK_PROPOSAL:String 					= "VIEW_UNLOCK_PROPOSAL";
		public static const VIEW_CANCELLATION_PROPOSAL:String 				= "VIEW_CANCELLATION_PROPOSAL";
		public static const VIEW_EXTERNAL_INSTALLMENT_REPORT:String 		= "VIEW_EXTERNAL_INSTALLMENT_REPORT";
		public static const VIEW_EXTERNAL_CUOTA_REPORT:String				= "VIEW_EXTERNAL_CUOTA_REPORT";
		public static const VIEW_RETRIEVE_QUOTE:String						= "VIEW_RETRIEVE_QUOTE";
		public static const VIEW_POLICY_REACTIVATION_SUPPORT:String 		= "VIEW_POLICY_REACTIVATION_SUPPORT";
		public static const VIEW_EXTERNAL_ADMINISTRATION_COLLECTION:String 	= "VIEW_EXTERNAL_ADMINISTRATION_COLLECTION";
		public static const VIEW_ADMINISTRATION_INTERFACE:String			= "VIEW_ADMINISTRATION_INTERFACE";
		
		public function NotificationViewsList()
		{
			//TODO: implement function
		}

	}
}