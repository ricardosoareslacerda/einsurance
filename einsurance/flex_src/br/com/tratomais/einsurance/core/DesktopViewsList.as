package br.com.tratomais.einsurance.core
{
	import flash.text.StaticText;
	
	public class DesktopViewsList
	{
		
		public static const VIEW_WELCOME:int 							= 0;
		public static const VIEW_USER_MAIN:int 							= 1;
		public static const VIEW_ROLES_LIST:int 						= 2;
		public static const VIEW_PARTNER_MAIN:int 						= 3;
		public static const VIEW_BROKER_MAIN:int 						= 4;
		public static const VIEW_CHANNEL_MAIN:int 						= 5;
		public static const VIEW_SUBSIDIARY_MAIN:int 					= 6;
		public static const VIEW_PRODUCTS:int 							= 7;
		public static const VIEW_QUOTE_MAIN:int 						= 8;
		public static const VIEW_PROPOSAL_MAIN:int 						= 9;
		public static const VIEW_MASTER_POLICY_MAIN:int					= 10;
		public static const VIEW_SEARCH_DOCUMENT:int					= 11;
		public static const VIEW_GENERAL_REPORT:int						= 12;		
		public static const VIEW_CHANNEL_REPORT:int						= 12;
		public static const VIEW_PRODUCT_REPORT:int						= 12;
		public static const VIEW_SALES_REPORT:int						= 12;
		public static const VIEW_INSTALLMENT_REPORT:int					= 12;
		public static const VIEW_COMMISSION_REPORT:int					= 12;
		public static const VIEW_LABOUR_REPORT:int						= 12;
		public static const VIEW_ANNULMENT_REPORT:int					= 12;
		public static const VIEW_PRODUCT_COPY:int						= 13;
		public static const VIEW_EXTERNAL_INSTALLMENT_REPORT:int 		= 14;
		public static const VIEW_EXTERNAL_CUOTA_REPORT:int 				= 15;
		public static const VIEW_POLICY_REACTIVATION_SUPPORT:int		= 16;
		public static const VIEW_EXTERNAL_ADMINISTRATION_COLLECTION:int = 17;
		public static const VIEW_RELOAD_QUOTE:int 						= 18;
		public static const VIEW_ADMINISTRATION_INTERFACE:int			= 18;
		
		public function DesktopViewsList()
		{
			//TODO: implement function
		}

	}
}