////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2009 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 17/12/2009
//
// NOTICE: Trato+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.core.controller
{
	import br.com.tratomais.einsurance.core.ErrorWindow;
	import br.com.tratomais.einsurance.core.Errors;
	import br.com.tratomais.einsurance.core.Utilities;
	
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	import mx.rpc.events.FaultEvent;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	/**
	 * Centralizes fault treatment
	 */
	[ResourceBundle("error_messages")]
	public class FaultBlazeCommand extends SimpleCommand
	{
		/**
		 * Constructor
		 */
		public function FaultBlazeCommand()
		{
			super();
			resourceManager = ResourceManager.getInstance();
		}
		
		/**
		 * Resource Manager
		 */
		private var resourceManager : IResourceManager;
		
		/*
		 * (non-ASDoc)
		 * @see org.puremvc.as3.patterns.command.SimpleCommand
		 */
		override public function execute(note:INotification) :void {
		 	var faultEvent:FaultEvent = note.getBody() as FaultEvent;
			if(! Utilities.processFaultEvent(faultEvent)){
				// Alert.show(ResourceManager.getInstance().getString("messages",Errors.INTERNAL_ERROR), "Error", Alert.OK, null, null, Utilities.errorMsg);
				ErrorWindow.createAndShowError(
						Errors.INTERNAL_ERROR,
						faultEvent.fault.getStackTrace()
					);
			}
	 	}
	}
}