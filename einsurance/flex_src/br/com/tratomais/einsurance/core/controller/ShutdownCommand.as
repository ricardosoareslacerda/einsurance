////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2009 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 17/12/2009
//
// NOTICE: Adobe permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.core.controller
{
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	import br.com.tratomais.einsurance.ApplicationMediator;
    
    /**
     * Create and register <code>Proxy</code>s with the <code>Model</code>.
     */
	public class ShutdownCommand extends SimpleCommand
	{
		public function ShutdownCommand(){
		}
		
		 override public function execute( note:INotification ) :void {
		 	//facade.removeMediator(ApplicationMediator.NAME);
		}
    }
}