package br.com.tratomais.einsurance.core.controller
{
	[Bindable]
	public class StateModel
	{
		public var errorState:String;
		public var state:StateEnum;
		private static var instance:StateModel; 
		
		//constructor
		public function StateModel()
		{
			if(instance!=null){
				throw new Error("Only on ModelLocator instance should be instancied. use getInstance().");
			}
			instance=this;
		}
		
		public static function getInstance():StateModel{
			if(instance==null){
				instance = new StateModel();
			}
			return instance;
		}
	}
}