////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2009 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 17/12/2009
//
// NOTICE: Adobe permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.core.controller
{
	import br.com.tratomais.einsurance.core.NotificationList;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;

	public class ControllerPrepCommand extends SimpleCommand
	{
		public function ControllerPrepCommand()
		{
			super();
		}
		 override public function execute( note:INotification ) :void {
		 	facade.registerCommand(NotificationList.MENU_FUNCTION, MenuComand );
		 	facade.registerCommand(NotificationList.FAULT_BLAZE, FaultBlazeCommand );
		}

	}
}