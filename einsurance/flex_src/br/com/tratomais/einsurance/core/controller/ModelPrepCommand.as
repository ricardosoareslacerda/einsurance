////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2009 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 17/12/2009
//
// NOTICE: Adobe permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.core.controller
{
	import br.com.tratomais.einsurance.customer.model.proxy.BrokerProxy;
	import br.com.tratomais.einsurance.customer.model.proxy.ChannelProxy;
	import br.com.tratomais.einsurance.customer.model.proxy.CityProxy;
	import br.com.tratomais.einsurance.customer.model.proxy.CountryProxy;
	import br.com.tratomais.einsurance.customer.model.proxy.DataOptionProxy;
	import br.com.tratomais.einsurance.customer.model.proxy.FindCepProxy;
	import br.com.tratomais.einsurance.customer.model.proxy.PartnerProxy;
	import br.com.tratomais.einsurance.customer.model.proxy.StateProxy;
	import br.com.tratomais.einsurance.customer.model.proxy.SubsidiaryProxy;
	import br.com.tratomais.einsurance.policy.model.proxy.MasterPolicyProxy;
	import br.com.tratomais.einsurance.products.model.proxy.ProductProxy;
	import br.com.tratomais.einsurance.report.model.proxy.CollectionMainProxy;
	import br.com.tratomais.einsurance.report.model.proxy.InterfaceMainProxy;
	import br.com.tratomais.einsurance.report.model.proxy.ReportProxy;
	import br.com.tratomais.einsurance.sales.model.proxy.CalculateProxy;
	import br.com.tratomais.einsurance.sales.model.proxy.ProposalProxy;
	import br.com.tratomais.einsurance.search.model.proxy.SearchDocumentProxy;
	import br.com.tratomais.einsurance.support.model.proxy.SupportPolicyProxy;
	import br.com.tratomais.einsurance.useradm.model.proxy.FunctionalityProxy;
	import br.com.tratomais.einsurance.useradm.model.proxy.LoginProxy;
	import br.com.tratomais.einsurance.useradm.model.proxy.ModuleProxy;
	import br.com.tratomais.einsurance.useradm.model.proxy.RoleProxy;
	import br.com.tratomais.einsurance.useradm.model.proxy.UserProxy;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
    
    /**
     * Create and register <code>Proxy</code>s with the <code>Model</code>.
     */
	public class ModelPrepCommand extends SimpleCommand
	{
		public function ModelPrepCommand(){
		}
		
		 override public function execute( note:INotification ) :void {
		 	facade.registerProxy(new DataOptionProxy());
			facade.registerProxy(new LoginProxy());
			facade.registerProxy(new UserProxy());
			facade.registerProxy(new ModuleProxy());
			facade.registerProxy(new FunctionalityProxy());
			facade.registerProxy(new BrokerProxy());
			facade.registerProxy(new RoleProxy());
			facade.registerProxy(new ChannelProxy());
			facade.registerProxy(new CountryProxy());
			facade.registerProxy(new CityProxy());
			facade.registerProxy(new StateProxy());
			facade.registerProxy(new ProductProxy());
			facade.registerProxy(new ProposalProxy());
			facade.registerProxy(new SubsidiaryProxy());
			facade.registerProxy(new PartnerProxy());
			facade.registerProxy(new CalculateProxy());
			facade.registerProxy(new MasterPolicyProxy());
			facade.registerProxy(new SearchDocumentProxy());
			facade.registerProxy(new ReportProxy());
			facade.registerProxy(new SupportPolicyProxy());
			facade.registerProxy(new CollectionMainProxy());
			facade.registerProxy(new InterfaceMainProxy());
			facade.registerProxy(new FindCepProxy());
		}
    }
}