package br.com.tratomais.einsurance.core.controller
{
	public class StateEnum
	{
		public static const SEARCH:StateEnum	= new StateEnum("SEARCH");
		public static const DELETE:StateEnum	= new StateEnum("DELETE");
		public static const ADD:StateEnum		= new StateEnum("ADD");
		public static const EDIT:StateEnum		= new StateEnum("EDIT");
		public static const SAVE:StateEnum		= new StateEnum("SAVE");
		public static const CANCEL:StateEnum	= new StateEnum("CANCEL");
		
		private var _state:String;
		private static var locked:Boolean=false;
		
		//this block will be execute after all
		{
			locked=true;
		}
		
		public function StateEnum(state:String){
			if(locked){
				throw new Error("you can´t instantiate a Type Safe Enum.");
			}
			_state = state;
		}
		
		public function toString():String{
			return this._state;
		}
	}
}