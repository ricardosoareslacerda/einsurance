package br.com.tratomais.einsurance.core
{
	import br.com.tratomais.einsurance.customer.model.vo.City;
	import br.com.tratomais.einsurance.customer.model.vo.Country;
	import br.com.tratomais.einsurance.customer.model.vo.DataOption;
	import br.com.tratomais.einsurance.customer.model.vo.Occupation;
	import br.com.tratomais.einsurance.customer.model.vo.State;
	import br.com.tratomais.einsurance.customer.model.vo.Subsidiary;
	import br.com.tratomais.einsurance.products.model.vo.PersonalRiskOption;
	import br.com.tratomais.einsurance.products.model.vo.PlanKinship;
	import br.com.tratomais.einsurance.useradm.model.vo.User;
	import br.com.tratomais.einsurance.useradm.model.vo.UserPartner;
	
	import mx.collections.ArrayCollection;
	import mx.controls.ComboBox;
	import mx.events.ListEvent;
	
	public class ComboBoxUtililities
	{
		
		public function ComboBoxUtililities()
		{
		}
		
		public static function fillStateComboBox(comboBox : ComboBox, selectedObject : State) : void{
			var selected : Boolean = false;
			if(comboBox.dataProvider != null && selectedObject != null){
				for each (var object : State in comboBox.dataProvider as ArrayCollection){
					if(object.stateId == selectedObject.stateId){
		 				comboBox.selectedItem = object;
		 				comboBox.dispatchEvent(new ListEvent(ListEvent.CHANGE));
						selected = true;
					}				
				}  
			}
			if(selected == false){
				comboBox.selectedItem = null;
			}
		}
		
		public static function fillCityComboBox(comboBox : ComboBox, selectedObject : City) : void{
			var selected : Boolean = false;
			if(comboBox.dataProvider != null && selectedObject != null){
				for each (var object : City in comboBox.dataProvider as ArrayCollection){
					if(object.cityId == selectedObject.cityId){
		 				comboBox.selectedItem = object;
		 				selected = true;
					}				
				}  
			}
			if(selected == false){
				comboBox.selectedItem = null;
			}
		}
		
		public static function fillCountryComboBox(comboBox : ComboBox, selectedObject : Country) : void{
			var selected : Boolean = false;
			if(comboBox.dataProvider != null && selectedObject != null){
				for each (var object : Country in comboBox.dataProvider as ArrayCollection){
					if(object.countryId == selectedObject.countryId){
		 				comboBox.selectedItem = object;
		 				comboBox.dispatchEvent(new ListEvent(ListEvent.CHANGE));
		 				selected = true;
					}				
				}  
			}
			if(selected == false){
				comboBox.selectedItem = null;
			}
		}

		public static function fillPersonalRiskOptionComboBox(comboBox : ComboBox, personalRiskOptionId : int ) : void {
			var selected : Boolean = false;
			if(comboBox.dataProvider != null && personalRiskOptionId >= 0){
				for each (var object : PersonalRiskOption in comboBox.dataProvider as ArrayCollection){
					if(object.personType == personalRiskOptionId){
		 				comboBox.selectedItem = object;
		 				selected = true;
					}				
				}  
			}
			if(selected == false){
				comboBox.selectedItem = null;
			}
		}
		
		public static function fillDataOptionComboBox(comboBox : ComboBox, dataOptionId : int ) : void {
			var selected : Boolean = false;
			if(comboBox.dataProvider != null && dataOptionId >= 0){
				for each (var object : DataOption in comboBox.dataProvider as ArrayCollection){
					if(object.dataOptionId == dataOptionId){
		 				comboBox.selectedItem = object;
		 				selected = true;
					}				
				}  
			}
			if(selected == false){
				comboBox.selectedItem = null;
			}
		}
		
		public static function fillSubsidiaryComboBox(comboBox : ComboBox, dataOptionId : int ) : void {
			var selected : Boolean = false;
			if(comboBox.dataProvider != null && dataOptionId >= 0){
				for each (var object : Subsidiary in comboBox.dataProvider as ArrayCollection){
					if(object.subsidiaryId == dataOptionId){
		 				comboBox.selectedItem = object;
		 				selected = true;
					}				
				}  
			}
			if(selected == false){
				comboBox.selectedItem = null;
			}
		}		
		
		public static function fillPartnerComboBox(comboBox : ComboBox, loggedUser : User ) : void {
			var selected : Boolean = false;
			if(comboBox.dataProvider != null && loggedUser.transientSelectedPartner >= 0){
				for each (var object : UserPartner in loggedUser.userPartners as ArrayCollection){
					if(object.id.partnerId == loggedUser.transientSelectedPartner){
		 				comboBox.selectedItem = object.partner;
		 				selected = true;
					}				
				}  
			}
			if(selected == false){
				comboBox.selectedItem = null;
			}
		}	
		
		public static function fillOccupationComboBox(comboBox : ComboBox, occupationId : int ) : void {
			var selected : Boolean = false;
			if(comboBox.dataProvider != null && occupationId >= 0){
				for each (var object : Occupation in comboBox.dataProvider as ArrayCollection){
					if(object.occupationId == occupationId){
		 				comboBox.selectedItem = object;
		 				selected = true;
					}				
				}  
			}
			if(selected == false){
				comboBox.selectedItem = null;
			}
		}
		
		public static function fillPlanKinshipComboBox(comboBox : ComboBox, kinshipType : int ) : void {
			var selected : Boolean = false;
			if(comboBox.dataProvider != null && kinshipType >= 0){
				for each (var object : PlanKinship in comboBox.dataProvider as ArrayCollection){
					if(object.id.kinshipType == kinshipType){
		 				comboBox.selectedItem = object;
		 				selected = true;
					}				
				}  
			}
			if(selected == false){
				comboBox.selectedItem = null;
			}
		}
	}
}