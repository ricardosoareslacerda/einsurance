package br.com.tratomais.einsurance.core.events
{
	import br.com.tratomais.einsurance.products.model.vo.Question;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;

	public class ResponseAddedEvent extends Event {
		public static const EVENT_RESPONSE_ADDED:String = "EVENT_RESPONSE_ADDED";
		private var _responseList : ArrayCollection = new ArrayCollection();
		private var _question : Question = null;
		
		public function set responseList(value:ArrayCollection):void {
            _responseList=value;
        }
        
        public function get responseList():ArrayCollection {
            return _responseList;
        }        

		public function set question(value:Question):void {
            _question=value;
        }
        
        public function get question():Question {
            return _question;
        }     
        
		public function ResponseAddedEvent(type:String, newResponseList:ArrayCollection, curQuestion:Question, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_responseList = newResponseList;
			_question = curQuestion;
		}
	}
}