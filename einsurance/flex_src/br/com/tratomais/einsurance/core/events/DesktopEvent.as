package br.com.tratomais.einsurance.core.events
{
	import br.com.tratomais.einsurance.core.Constant;
	
	import flash.events.Event;
	
	import mx.events.IndexChangedEvent;

	public class DesktopEvent extends Event
	{

		public var originalEvent:Event
		public var newIndex:int

		public static const STACKVIEW_CHANGED:String = "STACKVIEW_CHANGED";
		public static const STACKVIEW_SHOW:String = "STACKVIEW_SHOW";

		public function DesktopEvent(type:String, objParam:Object, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
			
			switch (type)
			{
				case DesktopEvent.STACKVIEW_CHANGED:
					this.originalEvent = objParam as Event;
					var indexEvent:IndexChangedEvent = this.originalEvent as IndexChangedEvent;
					newIndex = indexEvent.newIndex
					break;
				case DesktopEvent.STACKVIEW_SHOW:
					newIndex = objParam as int;
					break;
			} 
		}
	}
}