package br.com.tratomais.einsurance.core
{
	public class Constant
	{		
		
/* 		public static const ID_INSURER: int = 1; // Zurich
		public static const ID_TERM_ANUAL: int = 1;// Anual
		public static const ID_TERM_PRORATA: int = 2;// Anual
		public static const ID_HIERARCHY_TYPE: int = 164; // corretor
		public static const ID_PLAN_RISK_DEFAULT= 1; 
		public static const ID_DATA_GROUP_HOUSING = 29; 
 */		
		//	payment
		public static const PAYMENT_TYPE:int = 31;
		public static const PAYMENT_TYPE_TEF:int = 9;
		public static const PAYMENT_TYPE_BILLET:int = 85;
		public static const PAYMENT_TYPE_CARD:int = 155;
		public static const PAYMENT_TYPE_CC:int = 153;
		public static const PAYMENT_TYPE_CP:int = 154;
		public static const PAYMENT_TYPE_FI:int = 761;
		public static const PAYMENT_TYPE_OT:int = 339;
		public static const PAYMENT_TYPE_CHECK:int = 338;
		public static const PAYMENT_TYPE_CARNE:int = 728;
		public static const PAYMENT_TYPE_FINANCING:int = 761;
		public static const PAYMENT_TYPE_BILLET_LOOSE:int = 733;

		// report
		public static const DOCUMENT_TYPE:int = 14; 	//  DocumentType
		public static const ISSUING_STATUS:int = 22; //datagroupId
		public static const INSTALLMENT_STATUS = 30; //datagroupId

		// broker
        public static const BROKER_TYPE:int = 6;  //  BrokerType
        public static const CHANNEL_TYPE:int = 9; // ChannelType
        
        //partner
        public static const PARTNER_TYPE:int = 8;  //  BrokerType
        
        //reportType
        public static const REPORT_TYPE_PROPOSAL:int = 764;  // proposta
        public static const REPORT_TYPE_CERTIFICATE:int = 765;  // certificado
		public static const CHANGE_REGISTER:String = 'changeRegister';
		public static const CHANGE_TECHNICAL:String = 'changeTechnical';
	}
}