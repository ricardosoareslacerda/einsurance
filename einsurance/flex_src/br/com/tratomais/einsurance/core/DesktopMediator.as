////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 31/03/2010
//
// NOTICE: Adobe permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
package br.com.tratomais.einsurance.core
{
	 import br.com.tratomais.einsurance.ApplicationFacade;
	 import br.com.tratomais.einsurance.core.events.DesktopEvent;
	 import br.com.tratomais.einsurance.customer.model.vo.Channel;
	 import br.com.tratomais.einsurance.customer.view.mediator.BrokerMainMediator;
	 import br.com.tratomais.einsurance.customer.view.mediator.ChannelMainMediator;
	 import br.com.tratomais.einsurance.customer.view.mediator.PartnerMainMediator;
	 import br.com.tratomais.einsurance.customer.view.mediator.SubsidiaryMainMediator;
	 import br.com.tratomais.einsurance.policy.model.vo.Contract;
	 import br.com.tratomais.einsurance.policy.model.vo.Endorsement;
	 import br.com.tratomais.einsurance.policy.model.vo.EndorsementOption;
	 import br.com.tratomais.einsurance.policy.view.mediator.MasterPolicyMainMediator;
	 import br.com.tratomais.einsurance.products.model.proxy.ProductProxy;
	 import br.com.tratomais.einsurance.products.model.vo.ProductOption;
	 import br.com.tratomais.einsurance.products.view.mediator.ContractMainMediator;
	 import br.com.tratomais.einsurance.products.view.mediator.ProductsMediator;
	 import br.com.tratomais.einsurance.report.view.mediator.CollectionMainMediator;
	 import br.com.tratomais.einsurance.report.view.mediator.CollectionReportMainMediator;
	 import br.com.tratomais.einsurance.report.view.mediator.CuotaReportMainMediator;
	 import br.com.tratomais.einsurance.report.view.mediator.GeneralReportMainMediator;
	 import br.com.tratomais.einsurance.report.view.mediator.InterfaceMainMediator;
	 import br.com.tratomais.einsurance.sales.view.mediator.ProposalMediator;
	 import br.com.tratomais.einsurance.sales.view.mediator.QuoteMainMediator;
	 import br.com.tratomais.einsurance.search.view.mediator.SearchDocumentMediator;
	 import br.com.tratomais.einsurance.support.view.mediator.SupportPolicyReactivationMediator;
	 import br.com.tratomais.einsurance.useradm.model.proxy.ModuleProxy;
	 import br.com.tratomais.einsurance.useradm.model.vo.Module;
	 import br.com.tratomais.einsurance.useradm.model.vo.Partner;
	 import br.com.tratomais.einsurance.useradm.model.vo.User;
	 import br.com.tratomais.einsurance.useradm.model.vo.UserPartner;
	 import br.com.tratomais.einsurance.useradm.view.mediator.RolesMainMediator;
	 import br.com.tratomais.einsurance.useradm.view.mediator.UserMainMediator;
	 
	 import flash.display.DisplayObject;
	 import flash.events.Event;
	 import flash.net.URLRequest;
	 import flash.net.navigateToURL;
	 
	 import mx.collections.ArrayCollection;
	 import mx.containers.ViewStack;
	 import mx.controls.Alert;
	 import mx.events.IndexChangedEvent;
	 import mx.events.MenuEvent;
	 import mx.resources.ResourceManager;
	 import mx.utils.ObjectUtil;
	 
	 import org.puremvc.as3.interfaces.IMediator;
	 import org.puremvc.as3.interfaces.INotification;
	 import org.puremvc.as3.patterns.mediator.Mediator;  
  	  
    public class DesktopMediator extends Mediator implements IMediator  
	{
       	public static const NAME:String = 'DesktopMediator';  
 		private var moduleProxy:ModuleProxy;
		//private var productOptionSelected : ProductOption;
    	
        public function DesktopMediator(viewComponent:Object)  
        {  
            super(NAME, viewComponent);  
            moduleProxy = ModuleProxy(facade.retrieveProxy(ModuleProxy.NAME));
        }  
        

	   protected function get desktop(): Desktop {
           return viewComponent as Desktop;
       }
  		
		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */
		override public function onRegister():void 
		{
			super.onRegister();
			desktop.addEventListener(DesktopEvent.STACKVIEW_CHANGED, onStackViewChanged);
			desktop.addEventListener(DesktopEvent.STACKVIEW_SHOW, onStackViewShow);
			desktop.addEventListener(desktop.LOGOUT, logout);
			desktop.addEventListener(MenuEvent.ITEM_CLICK, changePartner);
		}
		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
			desktop.removeEventListener(DesktopEvent.STACKVIEW_CHANGED, onStackViewChanged);
			desktop.removeEventListener(DesktopEvent.STACKVIEW_SHOW, onStackViewShow);
			desktop.removeEventListener(desktop.LOGOUT, logout);
			desktop.removeEventListener(MenuEvent.ITEM_CLICK, changePartner);			
			onClean();
			setViewComponent(null);
		}

        override public function listNotificationInterests():Array  
        {  
            return [  
   	            NotificationList.LOGIN_VALIDATED,
              	NotificationList.MODULE_LISTED,
              	NotificationList.MODULE_LIST,
              	NotificationList.MENU_FUNCTION,
              	NotificationList.LOGOUT,
              	NotificationViewsList.VIEW_QUOTE_MAIN,
              	NotificationViewsList.VIEW_PROPOSAL_MAIN,
            	NotificationList.PASSWORD_CHANGE,
            	NotificationList.MENU_ABANDONMENT,
            	NotificationViewsList.VIEW_RELOAD_QUOTE
             ];  
        }  
        
        /**
		 * @private 
		 */          
        override public function handleNotification(notification:INotification):void{  
            switch(notification.getName()){  
	           	case NotificationList.MODULE_LIST:
						this.initialize(notification.getBody() as ArrayCollection);
	   	    			break;
		      	case NotificationList.MODULE_LISTED:
					    this.initialize(notification.getBody() as ArrayCollection);
	        			break;
		      	case NotificationList.MENU_FUNCTION:
						var myModule:Module = notification.getBody() as Module;
						if(this.checkAbandonment(desktop.myViewstack, myModule)){
							this.selectView(myModule);
						}
	        			break;
	        	case NotificationViewsList.VIEW_QUOTE_MAIN:
	        			this.viewQuoteBySelectedProduct(notification.getBody() as ProductOption);
	        			break;
	        	case NotificationViewsList.VIEW_RELOAD_QUOTE:
	        			this.viewQuoteBySelectedEndorsementOption(notification.getBody() as EndorsementOption);
	        			break;
    			case NotificationViewsList.VIEW_PROPOSAL_MAIN:
						this.viewProposal(notification.getBody() as EndorsementOption);
						break;
	           	case NotificationList.LOGIN_VALIDATED:
		          		moduleProxy.listAllowedModulesByUserLogged();
	            		break;
				case NotificationList.LOGOUT:
						this.logout(null);
						break;
				case NotificationList.PASSWORD_CHANGE:
						var alterou:Boolean = notification.getBody() as Boolean;
						if(alterou)
							Utilities.successMessage("password.changed");
						else
							Utilities.warningMessage("password.not.changed");
						break;						
	 			case NotificationList.MENU_ABANDONMENT:
						var myModule:Module = notification.getBody() as Module;
						this.selectView(myModule);
	        			break;
        	}
        }  
        
   	   public function logout(event:Event):void{
			ApplicationFacade.getInstance().logout(event, event != null);
			desktop.currentView = DesktopViewsList.VIEW_WELCOME;
			sendNotification(NotificationList.SHUTDOWN);
  		}
  		
  	  public function changePartner(event:MenuEvent):void{ 
  	  		var partner : Partner = event.item as Partner;
  	  		if(partner != null){
  	  			var loggedUser : User = ApplicationFacade.getInstance().loggedUser;
	  	  		loggedUser.transientSelectedPartner = partner.partnerId;
	  	  		ApplicationFacade.getInstance().changePartnerDefaultLoggedUser(partner.partnerId);
				(ProductProxy(facade.retrieveProxy(ProductProxy.NAME))).findProductsbyPartner(loggedUser.transientSelectedPartner, 1);
				sendNotification(NotificationList.PARNTER_CHANGE, partner);
				if(desktop.currentView == DesktopViewsList.VIEW_PRODUCTS){
					this.viewProducts();
				}
				if(partner.imageProperty != null){
					desktop.imgPartnerMenu.source = Utilities.URL_BASE + "/assets/images/partners/" + partner.imageProperty;
				}else{
					desktop.imgPartnerMenu.source = Utilities.URL_BASE + "/assets/images/partners/default.png";
				}
				desktop.imgPartnerMenu.toolTip = partner.nickName;
  	  		}
  	  	}
  		
		private function validateDisplayShow(module:Module):void{
			var workList:ArrayCollection = new ArrayCollection();
			workList.addAll(module.modules);
			for each( var workModule:Module in workList ) {
				if (workModule.displayShow == false ) {
					var index:int = module.modules.getItemIndex(workModule);
					 module.modules.removeItemAt(index);
				}else{
					validateDisplayShow(workModule);
				}				
			}
		}
  		
	   public function initialize(collection:ArrayCollection):void{ 
        	var loggedUser : User = ApplicationFacade.getInstance().loggedUser;
        	var partners : ArrayCollection = new ArrayCollection();
        	
        	this.setSystemVersion('01.030.01');

			desktop.changePasswordBtn.visible = loggedUser.domain.autenticationType == 1;
			desktop.changePasswordBtn.includeInLayout = desktop.changePasswordBtn.visible;

			//VALIDA A APRESENTAÇÃO DE DETERMINADOS ITENS
			for each(var module:Module in collection) {
				if (module.displayShow == false ) {
					var index:int = collection.getItemIndex(module);
					 collection.removeItemAt(index);
				} else {
					validateDisplayShow(module);
				}
			}

	   		if(collection == null || collection.length == 0){
				Alert.show(ResourceManager.getInstance().getString("messages", "userNotFoundRoles"), "Atención", Alert.OK, null, null, Utilities.warningMsg);
	   		}else{
				desktop.menuBarCollection = collection; 
	   		}
	   		
    		for each(var userPartners : UserPartner in loggedUser.userPartners){
	       		partners.addItem(userPartners.partner);
       		}

			if(partners.length > 0){
				desktop.partnerMenuBarCollection = partners;
				desktop.user.text = ResourceManager.getInstance().getString("resources", "userWelcome") + ", " + loggedUser.name;
				if(ApplicationFacade.getInstance().getSelectedPartner().imageProperty != null){
					desktop.imgPartnerMenu.source = Utilities.URL_BASE + "/assets/images/partners/" + ApplicationFacade.getInstance().getSelectedPartner().imageProperty;
				}else{
					desktop.imgPartnerMenu.source = Utilities.URL_BASE + "/assets/images/partners/default.png";
				}
				
				desktop.imgPartnerMenu.toolTip = ApplicationFacade.getInstance().getSelectedPartner().nickName;
				 
				enabledMenu();
	   		}else{
				Alert.show(ResourceManager.getInstance().getString("messages", "userNotFoundPartner"), "Atención", Alert.OK, null, null, Utilities.warningMsg);
	   		}
  		}

		private function setSystemVersion(systemVersion:String):void{
			if(desktop.initialized){
				desktop.lblCopyright.text = ResourceManager.getInstance().getString('resources', 'copyright', [ systemVersion,  new Date().getFullYear() ]);
			}
		}

  		private function enabledMenu():void{
  			if(ApplicationFacade.getInstance().loggedUser.userPartners.length > 1){
  				desktop.makePartnerMenu = true;
  			}else{
  				desktop.makePartnerMenu = false;
  			}
  		}
		
		private function onClean() : void{
			desktop.menuBarCollection = null;
		}
		
		private function viewQuoteBySelectedProduct(productOption:ProductOption):void{
			desktop.makePartnerMenu = false;
			//productOptionSelected = productOption;
			desktop.quoteMain.endorsementOption = null;
			desktop.quoteMain.searchDocumentParameters = null;
			desktop.currentView = DesktopViewsList.VIEW_QUOTE_MAIN;
			desktop.quoteMain.productOption = productOption;
			desktop.quoteMain.partner = ApplicationFacade.getInstance().getSelectedPartner();
			desktop.quoteMain.channelListProvider = this.getChannelList(ApplicationFacade.getInstance().loggedUser.transientSelectedPartner);
			var wasRegistred : Boolean = ApplicationFacade.getInstance().registerOnlyNewMediator(new QuoteMainMediator(desktop.quoteMain));
			if(wasRegistred == false){
				desktop.quoteMain.initializeQuote();
			}
		}
		
		
		public function viewQuoteBySelectedEndorsementOption(endorsementOption:EndorsementOption):void{
			desktop.makePartnerMenu = false;
			
			if( endorsementOption.endorsement.issuingStatus == Endorsement.ISSUING_STATUS_EMITIDA )
				endorsementOption.endorsement.issuanceType = Endorsement.ISSUANCE_TYPE_ALTERACAO_TECNICA;

			desktop.quoteMain.endorsementOption = endorsementOption;
			desktop.quoteMain.isReloadEndorsement = true;
			desktop.currentView = DesktopViewsList.VIEW_QUOTE_MAIN;
			desktop.quoteMain.productOption = endorsementOption.productOption;
			setPartnerForQuotation( endorsementOption.endorsement.contract );
			desktop.quoteMain.channelListProvider = this.getChannelList(ApplicationFacade.getInstance().loggedUser.transientSelectedPartner);
			var wasRegistred : Boolean = ApplicationFacade.getInstance().registerOnlyNewMediator(new QuoteMainMediator(desktop.quoteMain));
			if(wasRegistred == false){
				desktop.quoteMain.initializeQuote();
			}
		}


		private function viewProposal(endorsementOption:EndorsementOption):void{
			desktop.makePartnerMenu = false;
			desktop.currentView = DesktopViewsList.VIEW_PROPOSAL_MAIN;
			desktop.proposal.endorsementOption = endorsementOption;
			desktop.proposal.endorsementOptionOld = ObjectUtil.copy(endorsementOption) as EndorsementOption;
			var wasRegistred : Boolean = ApplicationFacade.getInstance().registerOnlyNewMediator(new ProposalMediator(desktop.proposal));
			if(wasRegistred == false){
				desktop.proposal.onInicializarPage();
			}
		}
		
		private function viewProducts():void{
			enabledMenu();
			desktop.currentView = DesktopViewsList.VIEW_PRODUCTS;	
			desktop.productsForm.partnerId = ApplicationFacade.getInstance().loggedUser.transientSelectedPartner;
			desktop.productsForm.initializeProducts(null);
		}

		private function viewReports(pNotificationName : String):void{
			switch( pNotificationName){
				case NotificationViewsList.VIEW_ANNULMENT_REPORT:
					desktop.generalReportMain.notificationName = NotificationViewsList.VIEW_ANNULMENT_REPORT;
					break;
				case NotificationViewsList.VIEW_CHANNEL_REPORT: 
					desktop.generalReportMain.notificationName = NotificationViewsList.VIEW_CHANNEL_REPORT;
					break;
				case NotificationViewsList.VIEW_COMMISSION_REPORT:
					desktop.generalReportMain.notificationName = NotificationViewsList.VIEW_COMMISSION_REPORT;
					break;
				case NotificationViewsList.VIEW_LABOUR_REPORT:
					desktop.generalReportMain.notificationName = NotificationViewsList.VIEW_LABOUR_REPORT;
					break;
				case NotificationViewsList.VIEW_INSTALLMENT_REPORT:
					desktop.generalReportMain.notificationName = NotificationViewsList.VIEW_INSTALLMENT_REPORT;
					break;
				case NotificationViewsList.VIEW_PRODUCT_REPORT:
					desktop.generalReportMain.notificationName = NotificationViewsList.VIEW_PRODUCT_REPORT;
					break;
				case NotificationViewsList.VIEW_SALES_REPORT:
					desktop.generalReportMain.notificationName = NotificationViewsList.VIEW_SALES_REPORT;
					break;								
					
			}
			desktop.currentView = DesktopViewsList.VIEW_GENERAL_REPORT;
			desktop.generalReportMain.initializePage();
		}
		
		private function getChannelList(partnerId :int ) : ArrayCollection{
			var user: User = ApplicationFacade.getInstance().loggedUser;
			var channels : ArrayCollection = user.channels;
			var newChannels : ArrayCollection = new ArrayCollection();
			if(channels == null || channels.length == 0){
				return null;
			}else{
				for each(var channel : Channel in channels){
					if((channel.partnerId == partnerId) && (channel.active = true)){
						newChannels.addItem(channel);
					}
				}
				if(newChannels != null && newChannels.length > 0){
					return newChannels;
				}else{
					return null;
				}
			}
		}
		
		private function getChannelName(channelId : int) : String{
			for each(var channel : Channel in this.getChannelList(ApplicationFacade.getInstance().loggedUser.transientSelectedPartner)){
				if(channel.channelId == channelId){
					return channel.name;
				}
			}
			return null;
		}

		private function onStackViewChanged(event:Event):void{
			var desktopEvent:DesktopEvent = event as DesktopEvent;
			var indexEvent:IndexChangedEvent = desktopEvent.originalEvent as IndexChangedEvent;
			removeMediator(indexEvent.oldIndex);
		}

		private function onStackViewShow(event:Event):void{
			var desktopEvent:DesktopEvent = event as DesktopEvent;
			registerMediator(desktopEvent.newIndex);
		}
        
        /**
        * 
        * @param viewStack 
        * @return 
        * **/
        private function checkAbandonment(viewStack:ViewStack, module:Module):Boolean{
    	   	if(viewStack && (viewStack.selectedIndex > -1)){
	        	var functionName : String = "checkAbandonment";
	        	var currentView : int = viewStack.selectedIndex as int;
	        	var object : DisplayObject = viewStack.getChildAt(currentView) as DisplayObject;
	        	if(object){
	        		var implemented : Boolean = object.hasOwnProperty(functionName);
	        		if(implemented){
	        			object[functionName](module);
	        			return false;
	        		}
	        	}
        	}
        	return true;
        }
        
		public function selectView(module:Module):void{
			enabledMenu();
			switch (module.functionality.eventAction){
				case NotificationViewsList.VIEW_USER_MAIN:
					desktop.currentView = DesktopViewsList.VIEW_USER_MAIN;
					break;
				case NotificationViewsList.VIEW_ROLES_LIST:
					desktop.currentView = DesktopViewsList.VIEW_ROLES_LIST;
					break;
				case NotificationViewsList.VIEW_PARTNER_MAIN:
					desktop.currentView = DesktopViewsList.VIEW_PARTNER_MAIN;
					break;
				case NotificationViewsList.VIEW_BROKER_MAIN:
					desktop.currentView = DesktopViewsList.VIEW_BROKER_MAIN;
					break;
				case NotificationViewsList.VIEW_CHANNEL_MAIN:
					desktop.currentView = DesktopViewsList.VIEW_CHANNEL_MAIN;
					break;					
				case NotificationViewsList.VIEW_SUBSIDIARY_MAIN:
					desktop.currentView = DesktopViewsList.VIEW_SUBSIDIARY_MAIN;
					break;
				case NotificationViewsList.VIEW_PRODUCTS:
					this.viewProducts();
					break;
				case NotificationViewsList.VIEW_MASTER_POLICY_MAIN:
					desktop.currentView = DesktopViewsList.VIEW_MASTER_POLICY_MAIN;
					break;
				case NotificationViewsList.VIEW_SEARCH_DOCUMENT:
					desktop.searchDocument.selectedModule = module;
					desktop.currentView = DesktopViewsList.VIEW_SEARCH_DOCUMENT;
					break;
				case NotificationViewsList.VIEW_ANNULMENT_REPORT:
				case NotificationViewsList.VIEW_CHANNEL_REPORT:
				case NotificationViewsList.VIEW_PRODUCT_REPORT:
				case NotificationViewsList.VIEW_COMMISSION_REPORT:
				case NotificationViewsList.VIEW_LABOUR_REPORT:
				case NotificationViewsList.VIEW_INSTALLMENT_REPORT:	
				case NotificationViewsList.VIEW_SALES_REPORT:
					this.viewReports(module.functionality.eventAction);
					break;
				case NotificationViewsList.VIEW_PRODUCT_COPY:
					desktop.currentView = DesktopViewsList.VIEW_PRODUCT_COPY;
					break;
				case NotificationViewsList.VIEW_OPERATIONAL_MANUAL:
					var url:URLRequest=new URLRequest("/einsurance/manuals/einsurance_mo_web/Index.htm");
					navigateToURL(url);
					break;
				case NotificationViewsList.VIEW_TECHNICAL_MANUAL:
					var url:URLRequest=new URLRequest("/einsurance/manuals/einsurance_mt_web/Index.htm");
					navigateToURL(url);
					break;
				case NotificationViewsList.VIEW_EXTERNAL_INSTALLMENT_REPORT:
						desktop.currentView = DesktopViewsList.VIEW_EXTERNAL_INSTALLMENT_REPORT;
					break;
				case NotificationViewsList.VIEW_EXTERNAL_CUOTA_REPORT:
						desktop.currentView = DesktopViewsList.VIEW_EXTERNAL_CUOTA_REPORT;
					break;
				case NotificationViewsList.VIEW_POLICY_REACTIVATION_SUPPORT:
						desktop.currentView = DesktopViewsList.VIEW_POLICY_REACTIVATION_SUPPORT;
					break;
				case NotificationViewsList.VIEW_EXTERNAL_ADMINISTRATION_COLLECTION:
						desktop.currentView = DesktopViewsList.VIEW_EXTERNAL_ADMINISTRATION_COLLECTION;
					break;
				case NotificationViewsList.VIEW_ADMINISTRATION_INTERFACE:
					desktop.currentView = DesktopViewsList.VIEW_ADMINISTRATION_INTERFACE;
					break;
			}
		}

		private function registerMediator(stackIndex:int):void{
			switch(stackIndex){
				case DesktopViewsList.VIEW_USER_MAIN:
					ApplicationFacade.getInstance().registerOnlyNewMediator(new UserMainMediator(desktop.userMain));
					desktop.userMain.initializeUser();
					break;
				case DesktopViewsList.VIEW_ROLES_LIST:
					ApplicationFacade.getInstance().registerOnlyNewMediator(new RolesMainMediator(desktop.rolesMain));
					break;
				case DesktopViewsList.VIEW_PARTNER_MAIN:
					ApplicationFacade.getInstance().registerOnlyNewMediator(new PartnerMainMediator(desktop.partnerMain));
					break;
				case DesktopViewsList.VIEW_BROKER_MAIN:
					ApplicationFacade.getInstance().registerOnlyNewMediator(new BrokerMainMediator(desktop.brokerMain));
					break;
				case DesktopViewsList.VIEW_CHANNEL_MAIN:
					ApplicationFacade.getInstance().registerOnlyNewMediator(new ChannelMainMediator(desktop.channelMain));
					break;
				case DesktopViewsList.VIEW_SUBSIDIARY_MAIN:
					ApplicationFacade.getInstance().registerOnlyNewMediator(new SubsidiaryMainMediator(desktop.subsidiaryMain));
					break;
				case DesktopViewsList.VIEW_PRODUCTS:
					ApplicationFacade.getInstance().registerOnlyNewMediator(new ProductsMediator(desktop.productsForm));
					break;
				case DesktopViewsList.VIEW_MASTER_POLICY_MAIN:
					ApplicationFacade.getInstance().registerOnlyNewMediator(new MasterPolicyMainMediator(desktop.masterPolicyMain));
					break;
				case DesktopViewsList.VIEW_SEARCH_DOCUMENT:
					ApplicationFacade.getInstance().registerOnlyNewMediator(new SearchDocumentMediator(desktop.searchDocument));
					break;
				case DesktopViewsList.VIEW_ANNULMENT_REPORT:
				case DesktopViewsList.VIEW_GENERAL_REPORT:
				case DesktopViewsList.VIEW_CHANNEL_REPORT:
				case DesktopViewsList.VIEW_PRODUCT_REPORT:
				case DesktopViewsList.VIEW_SALES_REPORT:
				case DesktopViewsList.VIEW_INSTALLMENT_REPORT:
				case DesktopViewsList.VIEW_COMMISSION_REPORT:
				case DesktopViewsList.VIEW_LABOUR_REPORT:
					ApplicationFacade.getInstance().registerOnlyNewMediator(new GeneralReportMainMediator(desktop.generalReportMain));
					break;
				case DesktopViewsList.VIEW_PRODUCT_COPY:
					ApplicationFacade.getInstance().registerOnlyNewMediator(new ContractMainMediator(desktop.contractMain));
					break;
				case DesktopViewsList.VIEW_EXTERNAL_INSTALLMENT_REPORT:
					ApplicationFacade.getInstance().registerOnlyNewMediator(new CollectionReportMainMediator(desktop.collectionReportMain));
					break;
				case DesktopViewsList.VIEW_EXTERNAL_CUOTA_REPORT:
					ApplicationFacade.getInstance().registerOnlyNewMediator(new CuotaReportMainMediator(desktop.cuotaReportMain));
					break;
				case DesktopViewsList.VIEW_POLICY_REACTIVATION_SUPPORT:
					ApplicationFacade.getInstance().registerOnlyNewMediator(new SupportPolicyReactivationMediator(desktop.supportPolicyReactivation));
					break;
				case DesktopViewsList.VIEW_EXTERNAL_ADMINISTRATION_COLLECTION:
					ApplicationFacade.getInstance().registerOnlyNewMediator(new CollectionMainMediator(desktop.administrationReportMain));
					break;
				case DesktopViewsList.VIEW_ADMINISTRATION_INTERFACE:
					ApplicationFacade.getInstance().registerOnlyNewMediator(new InterfaceMainMediator(desktop.interfaceReportMain));
					break;
			}
		}

 		private function removeMediator(stackIndex:int):void{
			switch(stackIndex){
				case DesktopViewsList.VIEW_USER_MAIN:
					ApplicationFacade.getInstance().removeMediator(UserMainMediator.NAME);
					desktop.userMain.initializeUser();
					break;
				case DesktopViewsList.VIEW_ROLES_LIST:
					ApplicationFacade.getInstance().removeMediator(RolesMainMediator.NAME);
					break;
				case DesktopViewsList.VIEW_PARTNER_MAIN:
					ApplicationFacade.getInstance().removeMediator(PartnerMainMediator.NAME);
					break;
				case DesktopViewsList.VIEW_BROKER_MAIN:
					ApplicationFacade.getInstance().removeMediator(BrokerMainMediator.NAME);
					break;
				case DesktopViewsList.VIEW_CHANNEL_MAIN:
					ApplicationFacade.getInstance().removeMediator(ChannelMainMediator.NAME);
					break;
				case DesktopViewsList.VIEW_SUBSIDIARY_MAIN:
					ApplicationFacade.getInstance().removeMediator(SubsidiaryMainMediator.NAME);
					break;
				case DesktopViewsList.VIEW_PRODUCTS:
					ApplicationFacade.getInstance().removeMediator(ProductsMediator.NAME);
					break;
				case DesktopViewsList.VIEW_MASTER_POLICY_MAIN:
					ApplicationFacade.getInstance().removeMediator(MasterPolicyMainMediator.NAME);
					break;
				case DesktopViewsList.VIEW_ANNULMENT_REPORT:
				case DesktopViewsList.VIEW_GENERAL_REPORT:
				case DesktopViewsList.VIEW_CHANNEL_REPORT:
				case DesktopViewsList.VIEW_PRODUCT_REPORT:
				case DesktopViewsList.VIEW_SALES_REPORT:
				case DesktopViewsList.VIEW_INSTALLMENT_REPORT:
				case DesktopViewsList.VIEW_COMMISSION_REPORT:
				case DesktopViewsList.VIEW_LABOUR_REPORT:
					ApplicationFacade.getInstance().removeMediator(GeneralReportMainMediator.NAME);
					break;
				case DesktopViewsList.VIEW_PRODUCT_COPY:
					ApplicationFacade.getInstance().removeMediator(ContractMainMediator.NAME);
					break;
				case DesktopViewsList.VIEW_SEARCH_DOCUMENT:
					ApplicationFacade.getInstance().removeMediator(SearchDocumentMediator.NAME);
					break;
				case DesktopViewsList.VIEW_EXTERNAL_INSTALLMENT_REPORT:
					ApplicationFacade.getInstance().removeMediator(CollectionReportMainMediator.NAME);
					break;
				case DesktopViewsList.VIEW_EXTERNAL_CUOTA_REPORT:
					ApplicationFacade.getInstance().removeMediator(CuotaReportMainMediator.NAME);
					break;
				case DesktopViewsList.VIEW_POLICY_REACTIVATION_SUPPORT:
					ApplicationFacade.getInstance().removeMediator(SupportPolicyReactivationMediator.NAME);
					break;
				case DesktopViewsList.VIEW_EXTERNAL_ADMINISTRATION_COLLECTION:
					ApplicationFacade.getInstance().removeMediator(CollectionMainMediator.NAME);
					break;
				case DesktopViewsList.VIEW_ADMINISTRATION_INTERFACE:
					ApplicationFacade.getInstance().removeMediator(InterfaceMainMediator.NAME);
					break;
					
			}
		}
		
		private function setPartnerForQuotation( contract:Contract ):void
		{
			for each( var partner:Partner in desktop.partnerMenuBarCollection )
			{
				if( partner.partnerId == contract.partnerId )
				{
					var menuEvent:MenuEvent = new MenuEvent(MenuEvent.ITEM_CLICK);
					menuEvent.item = partner;
					desktop.quoteMain.partner = partner;
					changePartner( menuEvent );
					break;
				}
			}
		}
	}
}