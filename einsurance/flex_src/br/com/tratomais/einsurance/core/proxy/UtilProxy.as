package br.com.tratomais.einsurance.core.proxy
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.UtilitiesService;
	import br.com.tratomais.einsurance.core.business.UtilDelegateProxy;
	import br.com.tratomais.einsurance.useradm.model.vo.User;
	
	import mx.controls.Alert;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.proxy.Proxy;  
  
  	[Bindable]
	public class UtilProxy extends Proxy implements IProxy  
	{
		import mx.rpc.IResponder;  
  
	    public static const NAME:String = "UtilProxy"; 
	 	private var responder : IResponder;  
        private var service : Object;  

		public function UtilProxy( data:Object = null)  
        {  
            super(NAME, data );  
        	service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_USER);
        }  
  
        public function getStringHash(value: String ) : void{
		    var delegate:UtilDelegateProxy = new UtilDelegateProxy(new Responder(onGetStringHash, onFault));  
            delegate.getStringHash(value); 
		}
		
		private function onGetStringHash(pResultEvt:ResultEvent) : void  {  
	        var result : String = pResultEvt.result as String ;
        	sendNotification(NotificationList.UTIL_VERIFY_HASH, result);
        } 
        
        public function getLoggedUser() : void{
		    var delegate:UtilDelegateProxy = new UtilDelegateProxy(new Responder(onGetLoggedUser, onFault));  
            delegate.getLoggedUser(); 
		}
		
		private function onGetLoggedUser(pResultEvt:ResultEvent) : void  {  
	        var user : User = pResultEvt.result as User ;
        	sendNotification(NotificationList.UTIL_LOGGED_USER, user);
        } 
        
		private function onFault(pFaultEvt:FaultEvent):void{
			sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
		}  
	}
}  