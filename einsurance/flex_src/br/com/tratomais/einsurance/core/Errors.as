package br.com.tratomais.einsurance.core  
{
    public class Errors 
    {
    	/**
    	 * Internal error constant
    	 */
    	public static const INTERNAL_ERROR:String = "internalError";  
    	public static const WARNING:String = "warning";  
    }  
} 