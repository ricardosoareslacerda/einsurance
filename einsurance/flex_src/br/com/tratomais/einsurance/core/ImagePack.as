// ActionScript file
[Bindable]
[Embed(source="/assets/images/menu/mainData.png")]
public var mainData:Class;

[Bindable]
[Embed(source="/assets/images/menu/tools.png")]
public var configurations:Class;

[Bindable]
[Embed(source="/assets/images/menu/security.png")]
public var security:Class;

[Bindable]
[Embed(source="/assets/images/menu/users.png")]
public var users:Class;

[Bindable]
[Embed(source="/assets/images/menu/roles.png")]
public var roles:Class;

[Bindable]
[Embed(source="/assets/images/menu/application.png")]
public var application:Class;

[Bindable]
[Embed(source="/assets/images/menu/life.png")]
public var life:Class;

[Bindable]
[Embed(source="/assets/images/menu/endorsement.png")]
public var endorsement:Class;			

[Bindable]
[Embed(source="/assets/images/menu/comercialization.png")]
public var comercialization:Class;

[Bindable]
[Embed(source="/assets/images/menu/partner.png")]
public var partner:Class;

[Bindable]
[Embed(source="/assets/images/menu/filial.png")]
public var filial:Class;			

[Bindable]
[Embed(source="/assets/images/menu/channel.png")]
public var channel:Class;			

[Bindable]
[Embed(source="/assets/images/menu/broker.png")]
public var broker:Class;

[Bindable]
[Embed(source="/assets/images/menu/masterPolicy.png")]
public var masterPolicy:Class;

[Bindable]
[Embed(source="/assets/images/menu/report.png")]
public var reports:Class;

[Bindable]
[Embed(source="/assets/images/menu/certificate.png")]
public var certificate:Class;

[Bindable]
[Embed(source="/assets/images/menu/help.png")]
public var help:Class;

[Bindable]
[Embed(source="/assets/images/menu/info.png")]
public var info:Class;

[Bindable]
[Embed(source="/assets/images/menu/search.png")]
public var search:Class;

[Bindable]
[Embed(source="/assets/images/menu/manual.png")]
public var manual:Class;

[Bindable]
[Embed(source="/assets/images/menu/entityImage.png")]
public var entityImage:Class;

[Bindable]
[Embed(source="/assets/images/menu/product.png")]
public var product:Class;

[Bindable]
[Embed(source="/assets/images/menu/operational.png")]
public var operational:Class;

[Bindable]
[Embed(source="/assets/images/menu/cobranza.png")]
public var cobranza:Class;

[Bindable]
[Embed(source="/assets/images/menu/comission.png")]
public var comission:Class;

[Bindable]
[Embed(source="/assets/images/menu/unlock.png")]
public var unlock:Class;			
	
[Bindable]
[Embed(source="/assets/images/menu/annulment.png")]
public var annulment:Class;

[Bindable]
[Embed(source="/assets/images/menu/lot.png")]
public var lot:Class;

[Bindable]
[Embed(source="/assets/images/menu/cuota.png")]
public var cuota:Class;

[Bindable]
[Embed(source="/assets/images/menu/admCollection.png")]
public var admCollection:Class;

[Bindable]
[Embed(source="/assets/images/menu/support.png")]
public var support:Class;

[Bindable]
[Embed(source="/assets/images/menu/reactivation.png")]
public var reactivation:Class;

[Bindable]
[Embed(source="/assets/images/menu/interfacesLogo.png")]
public var interfacesLogo:Class;

[Bindable]
[Embed(source="/assets/images/menu/admInterface.png")]
public var admInterface:Class;