////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 12/01/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////
//		    	

package br.com.tratomais.einsurance.sales.view.mediator
{
	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.core.components.CityCombo.view.mediator.CityComboMediator;
	import br.com.tratomais.einsurance.core.components.CountryCombo.view.mediator.CountryComboMediator;
	import br.com.tratomais.einsurance.core.components.StateCombo.view.mediator.StateComboMediator;
	import br.com.tratomais.einsurance.customer.model.proxy.DataOptionProxy;
	import br.com.tratomais.einsurance.customer.model.proxy.FindCepProxy;
	import br.com.tratomais.einsurance.customer.model.vo.DataOption;
	import br.com.tratomais.einsurance.customer.model.vo.ZipCodeAddress;
	import br.com.tratomais.einsurance.products.model.proxy.ProductProxy;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	import br.com.tratomais.einsurance.products.model.vo.PersonalRiskOption;
	import br.com.tratomais.einsurance.products.model.vo.PlanKinship;
	import br.com.tratomais.einsurance.sales.model.proxy.ProposalProxy;
	import br.com.tratomais.einsurance.sales.view.components.InsuredForm;
	import br.com.tratomais.einsurance.sales.view.components.MotoristForm;
	
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.validators.Validator;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class InsuredFormMediator extends Mediator implements IMediator 
	{
		public static const NAME:String = 'InsuredFormMediator';
		public static const RENEWAL_TYPE:int = 91;
		public static const PERSONAL_RISK_TYPE:int = 293;
		
		private var proposalProxy:ProposalProxy;
		private var productProxy:ProductProxy;
		private var dataOptionProxy:DataOptionProxy;

        private var countryComboMediator:CountryComboMediator;
        private var stateComboMediator:StateComboMediator;
        private var cityComboMediator:CityComboMediator;
		private var findCepProxy : FindCepProxy;
			        
		public function InsuredFormMediator(viewComponent:Object)  
        {  
            super(NAME, viewComponent);
        }
        
        protected function get insuredForm():InsuredForm{  
            return viewComponent as InsuredForm;  
        } 
		/**
		 * Defines the list of Notification Interests
		 */            
        override public function listNotificationInterests():Array  
        {  
            return [NotificationList.CONTRACT_VALIDATIONS,
            		NotificationList.PRODUCT_LIST_PLAN_KINSHIP_RISK_TYPE,
            		NotificationList.DATA_OPTION_LOADED,
            		NotificationList.IDENTIFIED_CEP_FOUND];
        }
        
        /* (non-asDoc) 
         * see org.puremvc.as3.patterns.mediator.Mediator 
         */
        override public function handleNotification(notification:INotification):void  
        {  
	        switch (notification.getName()){ 
				case NotificationList.MAX_DOCUMENTS_PER_PRODUCT:
					var qtde:int = notification.getBody() as int;
					if ((insuredForm.maxContracts > 0 )&&(qtde >= insuredForm.maxContracts))
						Utilities.warningMessage("maximumNumberOfPolicy", [insuredForm.maxContracts, insuredForm.nomeProduto] );
					else
						Utilities.infoMessage("validNumberOfPolicies");
					break;
				case NotificationList.CONTRACT_VALIDATIONS:
					var mensagens: ArrayCollection = notification.getBody() as ArrayCollection;
					if (( mensagens != null ) && ( mensagens.length > 0 )){
						var mensagem:String = "";
						for each (var msg:String in mensagens) {
							mensagem += ( (mensagem==""?"":"\n") + msg );
						}
						Utilities.directWarningMessage(mensagem);
					}					
					else
						Utilities.infoMessage("validNumberOfPolicies");
					break;
	            case NotificationList.PRODUCT_LIST_PLAN_KINSHIP_RISK_TYPE:
	            	insuredForm.planKinship = (notification.getBody() as PlanKinship);
	            	break;
	            case NotificationList.DATA_OPTION_LOADED:
					var loadedDataOption : DataOption = notification.getBody() as DataOption;
	            
					if (loadedDataOption != null && loadedDataOption.dataOptionId == PERSONAL_RISK_TYPE){
		            	insuredForm.personalType = ((notification.getBody() as DataOption).fieldDescription);
					}
	            	break;	
	            case NotificationList.IDENTIFIED_CEP_FOUND:
					var idf:IdentifiedList = notification.getBody() as IdentifiedList;
					
					if(idf.nameComponent == insuredForm.id) {
						populateFormCepFound(idf.value as ZipCodeAddress);
					}
					break;				
	        }
	    }
	    
		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */	                
		override public function onRegister():void 
		{
			super.onRegister();
			
			proposalProxy = ProposalProxy ( facade.retrieveProxy(ProposalProxy.NAME));
			productProxy = ProductProxy(facade.retrieveProxy(ProductProxy.NAME));
			dataOptionProxy = DataOptionProxy(facade.retrieveProxy(DataOptionProxy.NAME));
			findCepProxy = FindCepProxy(facade.retrieveProxy(FindCepProxy.NAME));
			
			insuredForm.txtDocumentNumber.addEventListener(FocusEvent.FOCUS_OUT, onCPFFocusOutHandler, false, 0, true);
			
			insuredForm.addEventListener(InsuredForm.INSURED_TYPE_CHANGED, onChangePersonType);
			insuredForm.addEventListener(InsuredForm.CHECK_CONTRACT_VALIDATION_POLICY_HOLDER, checkNumberContracts);
			insuredForm.addEventListener(InsuredForm.INSURANCE_AGE, planKinshipByPersonalRiskType);

			insuredForm.addEventListener(InsuredForm.SET_CARD_HOLDER_NAME , setCardHolderName);
			insuredForm.addEventListener(InsuredForm.CLEAN_CARD_HOLDER_NAME , cleanCardHolderName);
			insuredForm.imgFindCep.addEventListener(MouseEvent.CLICK, findCep, false, 0, true);

			countryComboMediator = new CountryComboMediator(insuredForm.cmbxCountryInsured);
			stateComboMediator = new StateComboMediator(insuredForm.cmbxStateInsured);
			cityComboMediator = new CityComboMediator(insuredForm.cmbxCityInsured);
			
			ApplicationFacade.getInstance().registerMediator(countryComboMediator);
			ApplicationFacade.getInstance().registerMediator(stateComboMediator);
			ApplicationFacade.getInstance().registerMediator(cityComboMediator);			
    	}

		/**
		 * Starts the document check routine
		 */
		private function checkNumberContracts(event:Event):void {
			if (insuredForm.cmbxDocumentType.selectedIndex > -1 ){
				proposalProxy.validateContract(insuredForm.productId, insuredForm.txtDocumentNumber.text,(int) (insuredForm.cmbxDocumentType.selectedValue), insuredForm.contractId,insuredForm.referenceDate, insuredForm.dataInicio );
//				proposalProxy.getQuantityOfContracts(insuredForm.productId, insuredForm.txtDocumentNumber.text,(int) (insuredForm.cmbxDocumentType.selectedValue), insuredForm.contractId, insuredForm.dataInicio );
			}
		}
		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
			
			insuredForm.txtDocumentNumber.removeEventListener(FocusEvent.FOCUS_OUT, onCPFFocusOutHandler);
			
			insuredForm.removeEventListener(InsuredForm.INSURED_TYPE_CHANGED, onChangePersonType);
			insuredForm.removeEventListener(InsuredForm.INSURANCE_AGE, planKinshipByPersonalRiskType);
			insuredForm.removeEventListener(InsuredForm.CHECK_CONTRACT_VALIDATION_POLICY_HOLDER, checkNumberContracts);
			
			insuredForm.removeEventListener(InsuredForm.SET_CARD_HOLDER_NAME , setCardHolderName);
			insuredForm.removeEventListener(InsuredForm.CLEAN_CARD_HOLDER_NAME , cleanCardHolderName);
			insuredForm.imgFindCep.removeEventListener(MouseEvent.CLICK, findCep);
			
			ApplicationFacade.getInstance().removeMediator(countryComboMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(stateComboMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(cityComboMediator.getMediatorName());
			
			this.resetForm();						
		}

		private function onCPFFocusOutHandler(event:Event):void{
			if(insuredForm.cmbxDocumentType.selectedValue
					&& insuredForm.cmbxDocumentType.selectedValue == DataOption.DOCUMENT_TYPE_CPF){ //CPF
				if(insuredForm.txtDocumentNumber.text
						&& insuredForm.txtDocumentNumber.text != ''){
					if(!Utilities.validateCPF(insuredForm.txtDocumentNumber.text)){
						Utilities.errorMessage('invalidCPF');
						return;
					}
				}
   			}
		}

    	public function onChangePersonType(event:Event):void{
    		if(insuredForm.customer != null ){
    			if(insuredForm.customer.personType == 0){
    				insuredForm.customer.personType = insuredForm.cmbxPersonType.selectedValue;
    			}
    			if(insuredForm.customer.personType == PersonalRiskOption.LEGALLY_PERSON){
	    			proposalProxy.listAllInflowInsured(PersonalRiskOption.LEGALLY_PERSON_NAME);

    				//insuredForm.cmbxDocumentType.enabled = true;
    				insuredForm.cmbxDocumentType.dataProvider = insuredForm.listDocumentTypeLegally;
    				insuredForm.cmbxDocumentType.dropdown.dataProvider = insuredForm.listDocumentTypeLegally;

					insuredForm.txtLastName.text = "";
					insuredForm.txtFirstName.text = "";
					insuredForm.cmbxGender.selectedIndex = -1;
					//insuredForm.dfDatebirth.data = null;
					insuredForm.cmbxMaritalStatus.selectedIndex = -1;
					insuredForm.validatorRegister(true, false);
					insuredForm.enabledNaturalFields(false);
    			}else if(insuredForm.customer.personType == PersonalRiskOption.NATURAL_PERSON){
	    			proposalProxy.listAllInflowInsured(PersonalRiskOption.NATURAL_PERSON_NAME);

    				insuredForm.cmbxDocumentType.enabled = true;
    				insuredForm.cmbxDocumentType.dataProvider = insuredForm.listDocumentTypeNatural;
    				insuredForm.cmbxDocumentType.dropdown.dataProvider = insuredForm.listDocumentTypeNatural;
    				
    				insuredForm.txtTradeName.text = "";
    				insuredForm.txtCorporateName.text = "";
					insuredForm.validatorRegister(false, true);
					insuredForm.enabledNaturalFields(true);
    			}
    			
    			Utilities.redBorderField( insuredForm.cmbxPersonType, false );
    			Utilities.redBorderField( insuredForm.cmbxDocumentType, false );
    			Utilities.redBorderField( insuredForm.txtDocumentNumber, false );
    			Utilities.redBorderField( insuredForm.cmbxPhoneType1, false );
    			insuredForm.resetRedBorderForm();
    		}
    	}  	
    	
		public function resetForm():void{	
			Utilities.resetForm(insuredForm.formPersonType);
			Utilities.resetForm(insuredForm.formDetail);
			Utilities.resetForm(insuredForm.formAddress);
			Utilities.resetForm(insuredForm.formPhone);
		
			insuredForm.yes.selected = true;
			insuredForm.no.selected = false;

			insuredForm.validatorRegister(true, true); 
		}

	    private function planKinshipByPersonalRiskType(event:Event):void{
			productProxy.listPlanKinshipByPersonalRiskType(insuredForm.productId, insuredForm.riskPlanId, PERSONAL_RISK_TYPE, RENEWAL_TYPE, insuredForm.referenceDate);
			dataOptionProxy.getDataOptionById(PERSONAL_RISK_TYPE);
	    }
		
		public function validateFieldsInsured():Boolean {
        	var isValid:Boolean = true;
            var errors:Array = Validator.validateAll(insuredForm.validatorInsured);
            if (errors.length != 0) {
                isValid = false;
            }
            return isValid;
        }
        
        public function setCardHolderName(event:Event):void{
			var policyHolderName:String;
			
			policyHolderName = insuredForm.txtFirstName.text + " " +
							   insuredForm.txtLastName.text;
			
			sendNotification(NotificationList.SET_POLICY_HOLDER_NAME, policyHolderName);
			sendNotification(NotificationList.SET_CARD_HOLDER_NAME, policyHolderName);
        }
        
        public function cleanCardHolderName(event:Event):void{
			sendNotification(NotificationList.CLEAN_POLICY_HOLDER_NAME);
        }
        
        public function loadMotoristData(motoristForm:MotoristForm):void{        	
			insuredForm.cmbxGender.selectedValue = motoristForm.cmbxGender.selectedValue;
			insuredForm.customer.gender = motoristForm.cmbxGender.selectedValue; 
			insuredForm.dfDatebirth.selectedDate = motoristForm.dfDatebirth.selectedDate;
			insuredForm.customer.birthDate = motoristForm.dfDatebirth.selectedDate;
			insuredForm.cmbxMaritalStatus.selectedValue = motoristForm.cmbxMaritalStatus.selectedValue;
			insuredForm.customer.maritalStatus = motoristForm.cmbxMaritalStatus.selectedValue;
			Utilities.removeValidationError(insuredForm.cmbxGender);
			Utilities.removeValidationError(insuredForm.dfDatebirth);
			Utilities.removeValidationError(insuredForm.cmbxMaritalStatus);
        }
        
        public function disableMotoristFields():void{      
			insuredForm.cmbxGender.enabled = false;
			insuredForm.dfDatebirth.enabled = false;
			insuredForm.cmbxMaritalStatus.enabled = false;
        }
        
        public function cleanMotoristFields():void{      
			insuredForm.cmbxGender.selectedIndex = -1;
			insuredForm.dfDatebirth.selectedDate = null;
			insuredForm.dfDatebirth.text = "";
			insuredForm.cmbxMaritalStatus.selectedIndex = -1;
        }
        
        public function findCep(evt:MouseEvent):void{
			if(insuredForm.txtZipCode.length == 8){
				var idf:IdentifiedList = new IdentifiedList();
				idf.nameComponent = insuredForm.id;
				
				findCepProxy.identifiedListFindCep(idf, insuredForm.txtZipCode.text);
			}
			else{
				Utilities.warningMessage('zipCodeInvalid');
			}
		}
		
		private function populateFormCepFound(zipCodeAdress:ZipCodeAddress):void{
			if(zipCodeAdress && zipCodeAdress.stateId > 0) {
				insuredForm.cmbxStateInsured.CleanCombo();
				insuredForm.cmbxCityInsured.CleanCombo();
				
				// preencher Objetos de apoio, não mudar
				insuredForm.cmbxCountryInsured.countryId = zipCodeAdress.countryId;
				insuredForm.cmbxStateInsured.stateId = zipCodeAdress.stateId;
				insuredForm.cmbxCityInsured.cityId = zipCodeAdress.cityId;
				
				if(insuredForm.cmbxCountryInsured.address){
					insuredForm.cmbxCountryInsured.address.countryId = zipCodeAdress.countryId;
					insuredForm.cmbxCountryInsured.selectedValue = zipCodeAdress.countryId;
				}
				if(insuredForm.cmbxStateInsured.address)
					insuredForm.cmbxStateInsured.address.stateId = zipCodeAdress.stateId;
				if(insuredForm.cmbxCityInsured.address)
					insuredForm.cmbxCityInsured.address.cityId = zipCodeAdress.cityId;
				
				// recarrega os combos de Estado/Cidade
				insuredForm.cmbxStateInsured.countryId = zipCodeAdress.countryId;
				//insuredForm.cmbxCity.stateId = zipCodeAdress.stateId;
				
				insuredForm.txtDistrict.text = zipCodeAdress.districtName;
				insuredForm.txtAddress.text = zipCodeAdress.addressStreet;
			}
			else {
				Utilities.warningMessage('zipCodeInvalid');
			}
		}
	}
}