////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 12/01/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.sales.view.mediator
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.products.model.proxy.ProductProxy;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	import br.com.tratomais.einsurance.sales.view.components.InsuredForm;
	import br.com.tratomais.einsurance.sales.view.components.PurchaseProtectedForm;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class PurchaseProtectedFormMediator extends Mediator implements IMediator 
	{
		public static const NAME:String = 'PurchaseProtectedFormMediator';  
		private var productProxy:ProductProxy;
					        
		public function PurchaseProtectedFormMediator(viewComponent:Object)  
        {  
            super(NAME, viewComponent);
			productProxy = ProductProxy(facade.retrieveProxy(ProductProxy.NAME));
			var identifiedList:IdentifiedList = new IdentifiedList();
			identifiedList.nameComponent = "PurchaseProtectedForm";
			productProxy.getIdentifiedListApplicationPropertyValue( identifiedList, 1, "ToleranceToInsuredCardExpiration" );
        }
        
        protected function get purchaseProtectedForm():PurchaseProtectedForm{  
            return viewComponent as PurchaseProtectedForm;  
        } 
        
		public function loadInsuredData(insuredForm : InsuredForm):void {
			var policyHolderName:String;
			
			policyHolderName = insuredForm.txtFirstName.text + " " +
							   insuredForm.txtLastName.text;
			
			if (purchaseProtectedForm.cardHolderName.text.length <= 0) {
				purchaseProtectedForm.cardHolderName.text = policyHolderName;
				Utilities.removeValidationError(purchaseProtectedForm.cardHolderName);
			}
			purchaseProtectedForm.cardHolderName.enabled = true;
		}
 		
		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */	                
		override public function onRegister():void 
		{
			super.onRegister();
    	}
		 	        		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
			this.resetForm();						
		}
    	
		public function resetForm():void{
			purchaseProtectedForm.onClean();			
		}
		
        override public function listNotificationInterests():Array  
        {  
            return [NotificationList.SET_CARD_HOLDER_NAME,
            		NotificationList.APPLICATION_PROPERTY_IDENT_LISTED];
        }
        
		override public function handleNotification(notification:INotification):void  
        {  
            switch (notification.getName())  
            { 
				case NotificationList.SET_CARD_HOLDER_NAME:
					var cardHolderName : String = notification.getBody() as String;
					purchaseProtectedForm.cardHolderName.text = cardHolderName.substring(0, 30);
					break;
				case NotificationList.APPLICATION_PROPERTY_IDENT_LISTED:
					var identifiedList:IdentifiedList = notification.getBody() as IdentifiedList; 
					if( identifiedList && identifiedList.nameComponent == "PurchaseProtectedForm" ){
						var object:Object = identifiedList.value as Object;
						purchaseProtectedForm.setDueDate( int( object.value ) );
					}
					break;
            }
 		}
 		
		public function setCardHolderName(cardHolderName:String):void{
			purchaseProtectedForm.cardHolderName.text = cardHolderName;
		} 		
	}
}