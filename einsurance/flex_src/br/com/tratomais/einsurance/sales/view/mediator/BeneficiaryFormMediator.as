////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2009 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Valéria Sousa
// @version 1.0
// @lastModified 18/12/2009
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.sales.view.mediator
{
	import br.com.tratomais.einsurance.sales.view.components.BeneficiaryForm;
	
	import mx.collections.ArrayCollection;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class BeneficiaryFormMediator extends Mediator implements IMediator 
	{
		public static const NAME:String = 'BeneficiaryFormMediator';  
		
		public function BeneficiaryFormMediator(viewComponent:Object, object : Object = null)  
        {  
            super(NAME, viewComponent);
        } 
		        
        public function get beneficiaryForm():BeneficiaryForm{  
            return viewComponent as BeneficiaryForm;  
        }        
        
		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */	        
		override public function onRegister():void 
		{
			super.onRegister();
		} 
        		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
			this.resetForm();
		}
		
		
		private function initialize():void{
		}

          
        override public function listNotificationInterests():Array  
        {  
            return [];  
        }
        
		          
        override public function handleNotification(notification:INotification):void  
        { 
			                       
        	 
    	}
    	
		public function resetForm():void{
			beneficiaryForm.beneficiaries = new ArrayCollection();
			beneficiaryForm.resetInputForm();
		}  
	}
}