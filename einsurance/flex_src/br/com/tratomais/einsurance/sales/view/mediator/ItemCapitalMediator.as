package br.com.tratomais.einsurance.sales.view.mediator
{
	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.core.components.CityCombo.view.mediator.CityComboMediator;
	import br.com.tratomais.einsurance.core.components.CountryCombo.view.mediator.CountryComboMediator;
	import br.com.tratomais.einsurance.core.components.StateCombo.view.mediator.StateComboMediator;
	import br.com.tratomais.einsurance.customer.model.vo.City;
	import br.com.tratomais.einsurance.customer.model.vo.Country;
	import br.com.tratomais.einsurance.customer.model.vo.State;
	import br.com.tratomais.einsurance.policy.model.vo.ItemProperty;
	import br.com.tratomais.einsurance.sales.view.components.InsuredForm;
	import br.com.tratomais.einsurance.sales.view.components.ItemCapitalDetail;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class ItemCapitalMediator extends Mediator implements IMediator
	{
		
		public static const NAME:String = 'ItemCapitalMediator';  

        private var countryComboMediator:CountryComboMediator;
        private var stateComboMediator:StateComboMediator;
        private var cityComboMediator:CityComboMediator;
		
		public function ItemCapitalMediator(viewComponent:Object)  
        {  
            super(NAME, viewComponent);
        }

        protected function get itemCapitalDetail():ItemCapitalDetail{  
            return viewComponent as ItemCapitalDetail;  
        }
        
		override public function onRegister():void 
		{
			super.onRegister();
			
			countryComboMediator = new CountryComboMediator(itemCapitalDetail.cmbxCountryCapitalDetail);
			stateComboMediator = new StateComboMediator(itemCapitalDetail.cmbxStateCapitalDetail);
			cityComboMediator = new CityComboMediator(itemCapitalDetail.cmbxCityCapitalDetail);
			
			ApplicationFacade.getInstance().registerMediator(countryComboMediator);
			ApplicationFacade.getInstance().registerMediator(stateComboMediator);
			ApplicationFacade.getInstance().registerMediator(cityComboMediator);			
    	}
    	
		override public function onRemove():void 
		{
			super.onRemove();
			ApplicationFacade.getInstance().removeMediator(countryComboMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(stateComboMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(cityComboMediator.getMediatorName());
			this.resetForm();						
		}
		
		public function loadInsuredData(insuredForm : InsuredForm):void {		
			if (itemCapitalDetail.includeInLayout && itemCapitalDetail.AddressType.selectedValue==1) {
				if (itemCapitalDetail.itemProperty) {
					itemCapitalDetail.cmbxCountryCapitalDetail.selectedValue = insuredForm.cmbxCountryInsured.selectedValue;
					itemCapitalDetail.cmbxStateCapitalDetail.selectedValue = insuredForm.cmbxStateInsured.selectedValue;
					itemCapitalDetail.cmbxCityCapitalDetail.selectedValue = insuredForm.cmbxCityInsured.selectedValue;
					itemCapitalDetail.txtDistrict.text = insuredForm.txtDistrict.text;
					itemCapitalDetail.txtAddress.text = insuredForm.txtAddress.text;
				}
			}
		}		
		
		public function populateItemToForm(itemProperty : ItemProperty) : void{
 			var coutrySelected : Country = itemCapitalDetail.cmbxCountryCapitalDetail.selectedItem as Country;
			var stateSelectd : State = itemCapitalDetail.cmbxStateCapitalDetail.selectedItem as State;
			var citySeleted : City = itemCapitalDetail.cmbxCityCapitalDetail.selectedItem as City;
			itemProperty.cityId = new String(citySeleted.cityId);
			itemProperty.stateId = new String(stateSelectd.stateId);
			itemProperty.countryId = new String(coutrySelected.countryId);
			itemProperty.cityName = citySeleted.name;
			itemProperty.stateName = stateSelectd.name;
			itemProperty.countryName = coutrySelected.name;
			itemProperty.addressStreet = itemCapitalDetail.txtAddress.text;
			itemProperty.districtName = itemCapitalDetail.txtDistrict.text;
			itemProperty.acquisitionDate = itemCapitalDetail.constructionDate.newDate;
		}
		
		public function disableFields():void {			
			itemCapitalDetail.disableFields();	
		}
		
		public function resetForm():void{
			itemCapitalDetail.onClean();			
		}
	}
}