////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 12/01/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.sales.view.mediator
{
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.sales.view.components.InsuredForm;
	import br.com.tratomais.einsurance.sales.view.components.MotoristForm;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class MotoristFormMediator extends Mediator implements IMediator 
	{
		public static const NAME:String = 'MotoristFormMediator';  

		public function MotoristFormMediator(viewComponent:Object)  
        {  
            super(NAME, viewComponent);
        }
        
        protected function get motoristForm():MotoristForm{  
            return viewComponent as MotoristForm;  
        } 

		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */	                
		override public function onRegister():void 
		{
			super.onRegister();		
    	}
		 	        		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
		}
		
		public function loadInsuredData(insuredForm : InsuredForm):void {		
			
			if (motoristForm.includeInLayout && motoristForm.motoristType.selectedValue==1) {	
				motoristForm.cmbxDocumentType.selectedValue = insuredForm.cmbxDocumentType.selectedValue;
				motoristForm.motorist.documentType = insuredForm.cmbxDocumentType.selectedValue;
				motoristForm.txtDocumentNumber.text = insuredForm.txtDocumentNumber.text;
				motoristForm.txtFirstName.text = insuredForm.txtFirstName.text;
				motoristForm.txtLastName.text = insuredForm.txtLastName.text;
				Utilities.removeValidationError(motoristForm.txtDocumentNumber);
				Utilities.removeValidationError(motoristForm.txtFirstName);
				Utilities.removeValidationError(motoristForm.txtLastName);
				Utilities.removeValidationError(motoristForm.cmbxDocumentType);
			}
		}
		
		public function disableFields():void {			
			motoristForm.disableFields();	
		}
		
		public function cleanInsuredFields():void {						
			motoristForm.cmbxDocumentType.selectedIndex = -1;
			motoristForm.txtDocumentNumber.text = "";
			motoristForm.txtFirstName.text = "";
			motoristForm.txtLastName.text = "";
		}
	}
}