////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 12/01/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.sales.view.mediator
{
	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.components.CityCombo.view.mediator.CityComboMediator;
	import br.com.tratomais.einsurance.core.components.CountryCombo.view.mediator.CountryComboMediator;
	import br.com.tratomais.einsurance.core.components.StateCombo.view.mediator.StateComboMediator;
	import br.com.tratomais.einsurance.customer.model.vo.City;
	import br.com.tratomais.einsurance.customer.model.vo.Country;
	import br.com.tratomais.einsurance.customer.model.vo.State;
	import br.com.tratomais.einsurance.policy.model.vo.ItemProperty;
	import br.com.tratomais.einsurance.sales.view.components.ItemHabitatDetail;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class ItemHabitatMediator extends Mediator implements IMediator 
	{
		public static const NAME:String = 'ItemHabitatMediator';  

        private var countryComboMediator:CountryComboMediator;
        private var stateComboMediator:StateComboMediator;
        private var cityComboMediator:CityComboMediator;
			        
		public function ItemHabitatMediator(viewComponent:Object)  
        {  
            super(NAME, viewComponent);
        }
        
        protected function get itemHabitatDetail():ItemHabitatDetail{  
            return viewComponent as ItemHabitatDetail;  
        } 

		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */	                
		override public function onRegister():void 
		{
			super.onRegister();
			
			countryComboMediator = new CountryComboMediator(itemHabitatDetail.cmbxCountryHabitatDetail);
			stateComboMediator = new StateComboMediator(itemHabitatDetail.cmbxStateHabitatDetail);
			cityComboMediator = new CityComboMediator(itemHabitatDetail.cmbxCityHabitatDetail);
			
			ApplicationFacade.getInstance().registerMediator(countryComboMediator);
			ApplicationFacade.getInstance().registerMediator(stateComboMediator);
			ApplicationFacade.getInstance().registerMediator(cityComboMediator);			
    	}
		 	        		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
			ApplicationFacade.getInstance().removeMediator(countryComboMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(stateComboMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(cityComboMediator.getMediatorName());
			this.resetForm();						
		}
		
		public function populateItemToForm(itemProperty : ItemProperty) : void{
 			var coutrySelected : Country = itemHabitatDetail.cmbxCountryHabitatDetail.selectedItem as Country;
			var stateSelectd : State = itemHabitatDetail.cmbxStateHabitatDetail.selectedItem as State;
			var citySeleted : City = itemHabitatDetail.cmbxCityHabitatDetail.selectedItem as City;
			itemProperty.cityId = new String(citySeleted.cityId);
			itemProperty.stateId = new String(stateSelectd.stateId);
			itemProperty.countryId = new String(coutrySelected.countryId);
			itemProperty.cityName = citySeleted.name;
			itemProperty.stateName = stateSelectd.name;
			itemProperty.countryName = coutrySelected.name;
			itemProperty.addressStreet = itemHabitatDetail.txtAddress.text;
			itemProperty.districtName = itemHabitatDetail.txtDistrict.text;
			itemProperty.boundaryEast = itemHabitatDetail.txtBoundaryEast.text;
			itemProperty.boundaryNorth = itemHabitatDetail.txtBoundaryNorth.text;
			itemProperty.boundarySouth = itemHabitatDetail.txtBoundarySouth.text;
			itemProperty.boundaryWest = itemHabitatDetail.txtBoundaryWest.text;
			itemProperty.acquisitionDate = itemHabitatDetail.acquisitionDate.newDate;
			itemProperty.registrationName = itemHabitatDetail.txtRegistrationName.text;
			itemProperty.registrationDate =itemHabitatDetail.registrationDate.newDate;
			itemProperty.registrationNumber =itemHabitatDetail.txtRegistrationNumber.text;
			itemProperty.registrationTome =itemHabitatDetail.txtRegistrationTome.text;
			itemProperty.registrationProtocol =itemHabitatDetail.txtRegistrationProtocol.text;		
		}
    	
		public function resetForm():void{
			itemHabitatDetail.onClean();			
		}
	}
}