package br.com.tratomais.einsurance.sales.view.mediator
{
	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.Desktop;
	import br.com.tratomais.einsurance.core.ErrorWindow;
	import br.com.tratomais.einsurance.core.Errors;
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.NotificationViewsList;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.customer.model.proxy.ChannelProxy;
	import br.com.tratomais.einsurance.customer.model.proxy.DataOptionProxy;
	import br.com.tratomais.einsurance.customer.model.vo.Address;
	import br.com.tratomais.einsurance.customer.model.vo.Channel;
	import br.com.tratomais.einsurance.customer.model.vo.DataOption;
	import br.com.tratomais.einsurance.policy.model.vo.AutoBrand;
	import br.com.tratomais.einsurance.policy.model.vo.AutoModel;
	import br.com.tratomais.einsurance.policy.model.vo.AutoVersion;
	import br.com.tratomais.einsurance.policy.model.vo.Contract;
	import br.com.tratomais.einsurance.policy.model.vo.Endorsement;
	import br.com.tratomais.einsurance.policy.model.vo.EndorsementId;
	import br.com.tratomais.einsurance.policy.model.vo.EndorsementOption;
	import br.com.tratomais.einsurance.policy.model.vo.Item;
	import br.com.tratomais.einsurance.policy.model.vo.ItemAuto;
	import br.com.tratomais.einsurance.policy.model.vo.ItemCoverage;
	import br.com.tratomais.einsurance.policy.model.vo.ItemPersonalRisk;
	import br.com.tratomais.einsurance.policy.model.vo.ItemProperty;
	import br.com.tratomais.einsurance.policy.model.vo.ItemPurchaseProtected;
	import br.com.tratomais.einsurance.policy.model.vo.Motorist;
	import br.com.tratomais.einsurance.products.model.proxy.ProductProxy;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	import br.com.tratomais.einsurance.products.model.vo.PersonalRiskOption;
	import br.com.tratomais.einsurance.products.model.vo.PlanPersonalRisk;
	import br.com.tratomais.einsurance.products.model.vo.PlanPersonalRiskId;
	import br.com.tratomais.einsurance.products.model.vo.ProductOption;
	import br.com.tratomais.einsurance.products.model.vo.ProductPlan;
	import br.com.tratomais.einsurance.products.model.vo.ProductVersion;
	import br.com.tratomais.einsurance.products.model.vo.Question;
	import br.com.tratomais.einsurance.products.model.vo.Response;
	import br.com.tratomais.einsurance.products.model.vo.ResponseRelationship;
	import br.com.tratomais.einsurance.products.model.vo.SimpleCalcResult;
	import br.com.tratomais.einsurance.sales.model.proxy.CalculateProxy;
	import br.com.tratomais.einsurance.sales.model.proxy.ProposalProxy;
	import br.com.tratomais.einsurance.sales.model.vo.ProposalListData;
	import br.com.tratomais.einsurance.sales.view.components.ItemHabitat;
	import br.com.tratomais.einsurance.sales.view.components.QuoteForm;
	import br.com.tratomais.einsurance.useradm.model.vo.Partner;
	import br.com.tratomais.einsurance.useradm.model.vo.UserPartner;
	
	import flash.events.Event;
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.events.FaultEvent;
	import mx.utils.ObjectUtil;
	import mx.validators.Validator;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class QuoteMainMediator extends Mediator implements IMediator  
	{
	    public static const NAME:String = 'QuoteMainMediator';  
    
    	private var productProxy : ProductProxy;
    	private var calculateProxy : CalculateProxy;
    	private var dataOptionProxy : DataOptionProxy;
    	private var proposalProxy : ProposalProxy;
    	private var channelProxy : ChannelProxy;

    	private var productOption : ProductOption;
    	private var endorsementCalculated : Endorsement;
    	
    	private static const CONTENT_IDENTIFIED_RANGE_ID:String = "QUOTE_MAIN_MEDIATOR.CONTENT.86";
    	private static const STRUCTURE_IDENTIFIED_RANGE_ID:String = "QUOTE_MAIN_MEDIATOR.STRUCTURE.85";
    	private static const AUTO_IDENTIFIED_TRANSMISSION_TYPE_ID:String = "QUOTE_MAIN_MEDIATOR.AUTO.71";
    	private static const AUTO_IDENTIFIED_CATEGORY_TYPE_ID:String = "QUOTE_MAIN_MEDIATOR.AUTO.73";
    	private static const AUTO_IDENTIFIED_CLASS_TYPE_ID:String = "QUOTE_MAIN_MEDIATOR.AUTO.72";
    	private static const AUTO_IDENTIFIED_GROUP_TYPE_ID:String = "QUOTE_MAIN_MEDIATOR.AUTO.74";
    	private static const AUTO_IDENTIFIED_GENDER_TYPE_ID:String = "QUOTE_MAIN_MEDIATOR.AUTO.24";
    	private static const AUTO_IDENTIFIED_MARITAL_STATUS_TYPE_ID:String = "QUOTE_MAIN_MEDIATOR.AUTO.25";
    	private static const AUTO_IDENTIFIED_TONNE_QTT_TYPE_ID:String = "QUOTE_MAIN_MEDIATOR.AUTO.100";
    	private static const AUTO_IDENTIFIED_USE_TYPE_ID:String = "QUOTE_MAIN_MEDIATOR.AUTO.75";
    	
    	private static const CONDOMINIUM_IDENTIFIED_RENEWAL_TYPE_ID:String = "QUOTE_MAIN_MEDIATOR.CONDOMINIUM.19";
    	private static const CONDOMINIUM_IDENTIFIED_RENEWAL_INSURER_ID:String = "QUOTE_MAIN_MEDIATOR.CONDOMINIUM.102";
    	private static const CONDOMINIUM_IDENTIFIED_PROPERTY_RISK_ID:String = "QUOTE_MAIN_MEDIATOR.CONDOMINIUM.29";
    	private static const CONDOMINIUM_IDENTIFIED_REGION_CODE_ID:String = "QUOTE_MAIN_MEDIATOR.CONDOMINIUM.101";
    	
    	private static const PURCHASE_PROTECTED_IDENTIFIED_CARD_TYPE_ID:String = "QUOTE_MAIN_MEDIATOR.PURCHASE_PROTECTED.67";
    	private static const PURCHASE_PROTECTED_IDENTIFIED_CARD_BRAND_TYPE_ID:String = "QUOTE_MAIN_MEDIATOR.PURCHASE_PROTECTED.68";
    	
    	private static const CAPITAL_IDENTIFIED_COMPANY_TYPE_ID:String = "QUOTE_MAIN_MEDIATOR.CAPITAL.29";
    	private static const CAPITAL_IDENTIFIED_ACTIVITY_COMPANY_ID:String = "QUOTE_MAIN_MEDIATOR.CAPITAL.106";
    	
    	private static const HABITAT_IDENTIFIED_TYPE_HOUSING_ID:String = "QUOTE_MAIN_MEDIATOR.HABITAT.29";
		private var desktopInstance:Desktop = ApplicationFacade.getInstance().getEinsurance.desktop;
		    	
		public function QuoteMainMediator(viewComponent:Object)  
        {  
            super(NAME, viewComponent);  
        } 
        
        protected function get quoteForm():QuoteForm{  
           return viewComponent as QuoteForm;  
        }
		
				/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */
		override public function onRegister():void 
		{
			super.onRegister();
			// retrieve proxy
			productProxy = ProductProxy(facade.retrieveProxy(ProductProxy.NAME));
			calculateProxy = CalculateProxy(facade.retrieveProxy(CalculateProxy.NAME));
			dataOptionProxy = DataOptionProxy(facade.retrieveProxy(DataOptionProxy.NAME));
		 	proposalProxy = ProposalProxy(facade.retrieveProxy(ProposalProxy.NAME));
		 	channelProxy = ChannelProxy(facade.retrieveProxy(ChannelProxy.NAME));
		 	
		 	quoteForm.addEventListener(QuoteForm.EVENT_FIND_COVERAGE_BY_PLAN, findCoverageByPlan);
		 	quoteForm.addEventListener(QuoteForm.EVENT_LIST_COVERAGE_PLAN, listCoveragePlan);
			quoteForm.addEventListener(QuoteForm.EVENT_HIRING, hiring);
			quoteForm.addEventListener(QuoteForm.EVENT_CALCULATE, calculate);
			quoteForm.addEventListener(QuoteForm.EVENT_INITIALIZE_QUOTE, initializeQuote);
			quoteForm.addEventListener(QuoteForm.RELOAD_QUOTE, reloadQuote);
			quoteForm.addEventListener(QuoteForm.EVENT_COVERAGE_RANGE_VALUE, updateRiskValueHabitat);
			quoteForm.addEventListener(QuoteForm.EVENT_CALCULATE_LOAD, calculateLoad, false, 0, true);
			quoteForm.addEventListener(QuoteForm.EVENT_POPUP_ITEM_ADDITIONAL, listKinshipType, false, 0, true);
			quoteForm.addEventListener(QuoteForm.EVENT_SEARCH_AUTO_MODEL, searchAutoModel, false, 0, true);
			quoteForm.addEventListener(QuoteForm.EVENT_AUTO_MODEL_SELECTED, autoModelSelected, false, 0, true);
			quoteForm.addEventListener(QuoteForm.EVENT_AUTO_CATEGORY_CHANGED, autoCategoryChanged, false, 0, true);
			quoteForm.addEventListener(QuoteForm.EVENT_SEARCH_AUTO_COST, searchAutoCost, false, 0, true);
			quoteForm.addEventListener(QuoteForm.EVENT_SEARCH_TONNE, searchAutoTonne, false, 0, true);
			quoteForm.addEventListener(QuoteForm.EVENT_AUTO_OCCUPANT_CHANGED, autoOccupantNumberChanged, false, 0, true);
			quoteForm.addEventListener(QuoteForm.EVENT_PROFILE_RESPONSE_ADDED, profileResponseAdded, false, 0, true);
			quoteForm.addEventListener(QuoteForm.EVENT_LIST_PLAN, populatePlan, false, 0, true );
			quoteForm.addEventListener(QuoteForm.EVENT_RENEWAL_TYPE_SELECTED, renewalTypeSelected, false, 0, true );
			quoteForm.addEventListener(QuoteForm.EVENT_SELECTED_TYPE_COMPANY, typeCompanySelected, false, 0, true);
			quoteForm.addEventListener(QuoteForm.EVENT_CARD_TYPE_CHANGED, cardTypeChanged, false, 0, true);
		}
	
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			quoteForm.removeEventListener(QuoteForm.EVENT_FIND_COVERAGE_BY_PLAN, findCoverageByPlan);
			quoteForm.removeEventListener(QuoteForm.EVENT_LIST_COVERAGE_PLAN, listCoveragePlan);
			quoteForm.removeEventListener(QuoteForm.EVENT_HIRING, hiring);
			quoteForm.removeEventListener(QuoteForm.EVENT_CALCULATE, calculate);
			quoteForm.removeEventListener(QuoteForm.EVENT_INITIALIZE_QUOTE, initializeQuote);
			quoteForm.removeEventListener(QuoteForm.EVENT_COVERAGE_RANGE_VALUE, updateRiskValueHabitat);
			quoteForm.removeEventListener(QuoteForm.RELOAD_QUOTE, reloadQuote);
			quoteForm.removeEventListener(QuoteForm.EVENT_CALCULATE_LOAD, calculateLoad);
			quoteForm.removeEventListener(QuoteForm.EVENT_POPUP_ITEM_ADDITIONAL, listKinshipType);
			quoteForm.removeEventListener(QuoteForm.EVENT_SEARCH_AUTO_MODEL, searchAutoModel);
			quoteForm.removeEventListener(QuoteForm.EVENT_AUTO_MODEL_SELECTED, autoModelSelected);
			quoteForm.removeEventListener(QuoteForm.EVENT_AUTO_CATEGORY_CHANGED, autoCategoryChanged);
			quoteForm.removeEventListener(QuoteForm.EVENT_SEARCH_AUTO_COST, searchAutoCost);
			quoteForm.removeEventListener(QuoteForm.EVENT_SEARCH_TONNE, searchAutoTonne);
			quoteForm.removeEventListener(QuoteForm.EVENT_PROFILE_RESPONSE_ADDED, profileResponseAdded);
			quoteForm.removeEventListener(QuoteForm.EVENT_LIST_PLAN, populatePlan);
			quoteForm.removeEventListener(QuoteForm.EVENT_RENEWAL_TYPE_SELECTED, renewalTypeSelected);
			quoteForm.removeEventListener(QuoteForm.EVENT_SELECTED_TYPE_COMPANY, typeCompanySelected);
			setViewComponent(null);

			quoteForm.destroyListener();

			quoteForm.coverageListComponent.onClean();

			super.onRemove();
		}
		

          
        override public function listNotificationInterests():Array  
        {  
            return [
          			NotificationList.PRODUCT_LIST_COVERAGE_REFRESH,
          			NotificationList.PRODUCT_LIST_PLAN_REFRESH,
          			NotificationList.CALCULATED_QUOTATION,
          			NotificationList.ERROR_CALCULATED_QUOTATION,
        			NotificationList.OCCUPATION_LISTED,
        			NotificationList.RESTRICTION_RISK_LISTED,
        			NotificationList.CHANNEL_LIST_LISTED,
        			NotificationList.PARNTER_CHANGE,
        			NotificationList.PRODUCT_LIST_RISK_PLAN_REFRESH,
        			NotificationList.DATA_OPTION_LISTED,
        			NotificationList.DATA_OPTION_IDENT_LISTED,
        			NotificationList.PRODUCT_VERSION_PER_DATE_LISTED,
        			NotificationList.COVERAGE_RANGE_VALUE_LISTED,
        			NotificationList.PLAN_LOADED,
        			NotificationList.PAYMENT_OPTION_LISTED,
        			NotificationList.LOAD_ITEM_ADDITIONAL,
        			NotificationList.PRODUCT_LIST_AUTO_BRAND,
        			NotificationList.PRODUCT_LIST_AUTO_MODEL,
        			NotificationList.PRODUCT_LIST_AUTO_YEAR,
        			NotificationList.PRODUCT_LIST_AUTO_COST,
        			NotificationList.PRODUCT_FIND_AUTO_VERSION,
        			NotificationList.PRODUCT_VERSION_BETWEEN_DATE_LISTED,
        			NotificationList.PRODUCT_OPTION_LISTED,
        			NotificationList.PRODUCT_LIST_PLAN_KINSHIP_REFRESH_QUOTE,
        			NotificationList.PRODUCT_APPLY_COVERAGE_INSURED_VALUE,
        			NotificationList.PRODUCT_AUTO_DATA_CHANGED,
        			NotificationList.PRODUCT_LIST_PROFILE_REFRESH,
        			NotificationList.PRODUCT_RESPONSE_RELATIONSHIP_REFRESH,
        			NotificationList.PRODUCT_LIST_DEDUCTIBLE_REFRESH,
        			NotificationList.APPLICATION_PROPERTY_LISTED
             ];  
        }  
        
        override public function handleNotification(notification:INotification):void  
        {  
        	switch ( notification.getName() )  
            {  
	           	case NotificationList.PRODUCT_LIST_DEDUCTIBLE_REFRESH:
					this.populateDeductibleList(notification.getBody() as ArrayCollection);
	   	    		break;
	           	case NotificationList.PRODUCT_LIST_PLAN_REFRESH:
					this.populatePlanList(notification.getBody() as ArrayCollection);
	   	    		break;
	           	case NotificationList.PRODUCT_LIST_COVERAGE_REFRESH:
					this.populateCoverageList(notification.getBody() as ArrayCollection);
					quoteForm.enableCalcToQuotation();
	   	    		break;
				case NotificationList.PRODUCT_LIST_AUTO_BRAND:
					this.populateBrandList(notification.getBody() as ArrayCollection);
					break;
				case NotificationList.PRODUCT_LIST_AUTO_MODEL:
					this.populateModelList(notification.getBody() as ArrayCollection);
					break;
				case NotificationList.PRODUCT_LIST_AUTO_YEAR:
					this.populateYearList(notification.getBody() as ArrayCollection);
					break;
				case NotificationList.PRODUCT_LIST_AUTO_COST:
					this.populateAutoCost(notification.getBody() as Number);
					break;	
				case NotificationList.PRODUCT_FIND_AUTO_VERSION:
					this.populateAutoVersion(notification.getBody() as AutoVersion);	
			  		break; 
				case NotificationList.OCCUPATION_LISTED:
					this.populateOccupationList(notification.getBody() as ArrayCollection);
					break;
				case NotificationList.RESTRICTION_RISK_LISTED:
					this.populateRestrictionRiskList(notification.getBody() as ArrayCollection);
					break;			
				case NotificationList.PLAN_LOADED:
					if(this.productOption.objectId == Item.OBJECT_LIFE_CYCLE)
						quoteForm.itemLifeCycle.setProductPlan(notification.getBody() as ProductPlan);
					break;
				case NotificationList.PAYMENT_OPTION_LISTED:
					quoteForm.paymentListComponent.createItem( notification.getBody() as ArrayCollection );
					break;
				case NotificationList.CALCULATED_QUOTATION:
					if ( notification.getBody() instanceof SimpleCalcResult ){
						this.populateResultCalculate(notification.getBody() as SimpleCalcResult);
						
						var log:String;
						log = ApplicationFacade.getInstance().loggedUser.login;
						log += ":" + ApplicationFacade.getInstance().loggedUser.domain.name;
						log += ":CALCULATE QUOTATION";
						log += ":CALCULATE";
						if (endorsementCalculated && endorsementCalculated.id)
							log += ":" + endorsementCalculated.id.contractId;
						ApplicationFacade.getInstance().auditLogger = log;
					}
					else{
						showErrorMessage(notification.getBody());
					}
					break;
				case NotificationList.ERROR_CALCULATED_QUOTATION:
					showErrorMessage(notification.getBody());
					break;
				case NotificationList.CHANNEL_LIST_LISTED:
					var channelList : ArrayCollection = (notification.getBody() as ArrayCollection);
					quoteForm.itemSummary.listChannel( channelList );
					break;
				case NotificationList.PARNTER_CHANGE:
					quoteForm.partner = (notification.getBody() as Partner);
			  		break;
			  	case NotificationList.PRODUCT_LIST_RISK_PLAN_REFRESH:
			  		if(this.productOption.objectId == Item.OBJECT_HABITAT){
			  			quoteForm.itemHabitatForm.planRiskDataProvider = (notification.getBody() as ArrayCollection);
			  		}
			  		else{
			  			quoteForm.itemLifeCycle.setPlanRisk = (notification.getBody() as ArrayCollection);
			  		}
			  		break;
			  	case NotificationList.DATA_OPTION_LISTED:
			  		quoteForm.itemHabitatForm.housingDataProvider = (notification.getBody() as ArrayCollection);
			  		break;
			  	case NotificationList.DATA_OPTION_IDENT_LISTED:
			  		var identifiedList:IdentifiedList = (notification.getBody() as IdentifiedList);
			  		switch(identifiedList.groupId){
			  			case QuoteMainMediator.CONTENT_IDENTIFIED_RANGE_ID:
							quoteForm.itemHabitatForm.contentRangeValueDataProvider = identifiedList.resultArray as ArrayCollection;
							break;
			  			case QuoteMainMediator.STRUCTURE_IDENTIFIED_RANGE_ID:
							quoteForm.itemHabitatForm.structureRangeValueDataProvider = identifiedList.resultArray as ArrayCollection;
			  				break;
			  			case QuoteMainMediator.AUTO_IDENTIFIED_TRANSMISSION_TYPE_ID:
			  				quoteForm.itemVehicle.createTransmissionList(identifiedList.resultArray as ArrayCollection);
			  				break;
			  			case QuoteMainMediator.AUTO_IDENTIFIED_CATEGORY_TYPE_ID:
			  				quoteForm.itemVehicle.createCategoryList(identifiedList.resultArray as ArrayCollection);
			  				break;
			  			case QuoteMainMediator.AUTO_IDENTIFIED_CLASS_TYPE_ID:
			  				quoteForm.itemVehicle.createClassList(identifiedList.resultArray as ArrayCollection);
			  				break;
			  			case QuoteMainMediator.AUTO_IDENTIFIED_GROUP_TYPE_ID:
			  				quoteForm.itemVehicle.createGroupList(identifiedList.resultArray as ArrayCollection);
			  				break;
			  			case QuoteMainMediator.AUTO_IDENTIFIED_GENDER_TYPE_ID:
			  				quoteForm.itemMotorist.createGenderList(identifiedList.resultArray as ArrayCollection);
			  				break;
			  			case QuoteMainMediator.AUTO_IDENTIFIED_MARITAL_STATUS_TYPE_ID:
			  				quoteForm.itemMotorist.createMaritalStatusList(identifiedList.resultArray as ArrayCollection);
			  				break;
			  			case QuoteMainMediator.AUTO_IDENTIFIED_TONNE_QTT_TYPE_ID:
			  				quoteForm.itemVehicle.createTonneList(identifiedList.resultArray as ArrayCollection);
			  				break;
			  			case QuoteMainMediator.AUTO_IDENTIFIED_USE_TYPE_ID:
			  				quoteForm.itemVehicle.createAutoUseList(identifiedList.resultArray as ArrayCollection);
			  				break;
			  			case QuoteMainMediator.CONDOMINIUM_IDENTIFIED_RENEWAL_TYPE_ID:
			  				quoteForm.itemCondominium.createRenewalTypeList(identifiedList.resultArray as ArrayCollection);
			  				break;
			  			case QuoteMainMediator.CONDOMINIUM_IDENTIFIED_RENEWAL_INSURER_ID:
			  				quoteForm.itemCondominium.createInsurerList(identifiedList.resultArray as ArrayCollection);
			  				break;
			  			case QuoteMainMediator.CONDOMINIUM_IDENTIFIED_PROPERTY_RISK_ID:
			  				quoteForm.itemCondominium.createCondominiumTypeList(identifiedList.resultArray as ArrayCollection);
			  				break;
			  			case QuoteMainMediator.CONDOMINIUM_IDENTIFIED_REGION_CODE_ID:
			  				quoteForm.itemCondominium.createRegionCodeList(identifiedList.resultArray as ArrayCollection);
			  				break;			  				
			  			case QuoteMainMediator.PURCHASE_PROTECTED_IDENTIFIED_CARD_TYPE_ID:
			  				quoteForm.itemPurchaseProtected.createCardTypeList(identifiedList.resultArray as ArrayCollection);
			  				break;			  				
			  			case QuoteMainMediator.PURCHASE_PROTECTED_IDENTIFIED_CARD_BRAND_TYPE_ID:
			  				quoteForm.itemPurchaseProtected.createCardBrandTypeList(identifiedList.resultArray as ArrayCollection);
			  				break;
			  			case QuoteMainMediator.HABITAT_IDENTIFIED_TYPE_HOUSING_ID:
			  				quoteForm.itemHabitatForm.housingDataProvider = identifiedList.resultArray as ArrayCollection;
			  				break;
			  			case QuoteMainMediator.CAPITAL_IDENTIFIED_COMPANY_TYPE_ID:
			  				quoteForm.itemCapital.createTypeCompany(identifiedList.resultArray as ArrayCollection);
			  				break;
			  			case QuoteMainMediator.CAPITAL_IDENTIFIED_ACTIVITY_COMPANY_ID:
			  				quoteForm.itemCapital.createActivityCompany(identifiedList.resultArray as ArrayCollection);
			  				break;
			  		}
			  		break;
			  	case NotificationList.PRODUCT_VERSION_PER_DATE_LISTED:
			  		var productVersion:ProductVersion = (notification.getBody() as ProductVersion);
			  		if (productVersion != null) {
						quoteForm.itemHabitatForm.vldContent.minValue = productVersion.minimumInsuredValue;
						quoteForm.itemHabitatForm.vldContent.maxValue = productVersion.maximumInsuredValue;
						quoteForm.itemHabitatForm.vldEstructure.minValue = productVersion.minimumInsuredValue;
						quoteForm.itemHabitatForm.vldEstructure.maxValue = productVersion.maximumInsuredValue;
			  		}
			  		break;
			  	case NotificationList.COVERAGE_RANGE_VALUE_LISTED:
			  		var riskType:ArrayCollection = (notification.getBody() as ArrayCollection);
			  		
			  		if(riskType.getItemAt(0).id.coverageId == 14){
			  			quoteForm.itemHabitatForm.structureRangeValueDataProvider = (notification.getBody() as ArrayCollection);	
			  		}else{
			  			quoteForm.itemHabitatForm.contentRangeValueDataProvider = (notification.getBody() as ArrayCollection);
			  		}
					break;
				case NotificationList.LOAD_ITEM_ADDITIONAL:
					var proposalListData:ProposalListData = (notification.getBody() as ProposalListData);
					populateItemAdditional( proposalListData );
					break;
				case NotificationList.PRODUCT_VERSION_BETWEEN_DATE_LISTED:
					var productVersion:ProductVersion = ( notification.getBody() as ProductVersion);
					if( productVersion )
						productProxy.getProduct( quoteForm.endorsementOption.productOption.productId, productVersion.referenceDate );
					else
					{
						showErrorMessage(notification.getBody());
						quoteForm.imgCalculator.enabled = false;
					}					
					break;
				case NotificationList.PRODUCT_OPTION_LISTED:
					var productOption:ProductOption = ( notification.getBody() as ProductOption);
					
					if( productOption )
						quoteForm.productOption = productOption;
					else
					{
						showErrorMessage(notification.getBody());
						quoteForm.imgCalculator.enabled = false;
					}
					break;
				case NotificationList.PRODUCT_LIST_PLAN_KINSHIP_REFRESH_QUOTE:
					quoteForm.itemAdditional.proposalListData.listPlanKinship = notification.getBody() as ArrayCollection;
					break
			  	case NotificationList.PRODUCT_LIST_PROFILE_REFRESH:
			  		var profileList:ArrayCollection = (notification.getBody() as ArrayCollection);
			  		quoteForm.itemProfile.loadProfile(profileList);
			  		break;
			  	case NotificationList.PRODUCT_RESPONSE_RELATIONSHIP_REFRESH:
			  		var responseRelationshipList:ArrayCollection = (notification.getBody() as ArrayCollection);
			  		profileResponseRelationshipRefreshed(responseRelationshipList);
			  		break;
			  	case NotificationList.APPLICATION_PROPERTY_LISTED:
			  		var value:String = (notification.getBody() as String);
			  		var isVisible:Boolean = new Boolean(value);
			  		quoteForm.coverageListComponent.adgActions.visible = isVisible;
			  		break;
            }
    	}
    	
		private static const ID_INSURER: int = 1; // Zurich
		private static const ID_TERM_ANUAL: int = 1;// Anual
		private static const ID_TERM_PRORATA: int = 2;// Anual
		private static const ID_HIERARCHY_TYPE: int = 164; // corretor
		private static const ID_PLAN_RISK_DEFAULT= 1; 
		private static const ID_DATA_GROUP_HOUSING = 29; 
		private static const ID_DATA_GROUP_AUTO_TRANSMISSION = 71; 
		private static const ID_DATA_GROUP_AUTO_CLASS = 72; 
		private static const ID_DATA_GROUP_AUTO_CATEGORY = 73;
		private static const ID_DATA_GROUP_AUTO_GROUP = 74;  
		private static const ID_DATA_GROUP_STRUCTURE_RANGE = 85; 
		private static const ID_DATA_GROUP_CONTENT_RANGE = 86;
		private static const ID_DATA_GROUP_GENDER = 24 
		private static const ID_DATA_GROUP_MARITAL_STATUS = 25;  
		private static const ID_DATA_GROUP_TONNE_QTT = 100;  
		private static const ID_DATA_GROUP_RENEWAL_TYPE = 19;  
		private static const ID_DATA_GROUP_RENEWAL_INSURER = 102;  
		private static const ID_DATA_GROUP_PROPERTY_RISK = 29;  
		private static const ID_DATA_GROUP_REGION_CODE = 101;  
		private static const ID_DATA_GROUP_CARD_TYPE = 67;  
		private static const ID_DATA_GROUP_CARD_BRAND_TYPE = 68;
		private static const ID_DATA_GROUP_ACTIVITY_TYPE = 106;
		private static const ID_DATA_GROUP_AUTO_USE_TYPE:int = 75;

		public function initializeQuote(event : Event) : void{
			quoteForm.coverageListComponent.cleanPlan();

		 	productOption = quoteForm.productOption;
		 	endorsementCalculated = null;
		 	productProxy.getProductVersionByRefDate(productOption.productId, productOption.referenceDate);
		 	productProxy.getApplicationPropertyValue( 1, "RoadmapActive" );
		 	findProductProfile(true);
		 	quoteForm.itemSummary.productName.text = productOption.nickName;
		 	quoteForm.itemSummary.masterPolicyNumber.text = productOption.policyNumber.toString();
		 	quoteForm.itemSummary.partnerName.text = quoteForm.partner.nickName;
		 	quoteForm.coverageListComponent.commissionFactory = productOption.commissionFactory;
		 	quoteForm.coverageListComponent.cmbxPlan.enabled = true;
		 	
		 	if(productOption.objectId == Item.OBJECT_HABITAT){
			 	dataOptionProxy.findIdentifiedListDataOptionByDataGroupParentId(ID_DATA_GROUP_PROPERTY_RISK, productOption.objectType, HABITAT_IDENTIFIED_TYPE_HOUSING_ID); 		
		 		productProxy.findRiskPlanByProductId(productOption.productId, productOption.referenceDate);
		 		if ( this.productOption.riskValueType == 161 ) {
					quoteForm.itemHabitatForm.setValueType(ItemHabitat.VALUE_INPUT_COMBOBOX);
			 		//dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_STRUCTURE_RANGE,QuoteMainMediator.STRUCTURE_IDENTIFIED_RANGE_ID);
			 		//dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_CONTENT_RANGE,QuoteMainMediator.CONTENT_IDENTIFIED_RANGE_ID);				 		
			 	} else {
					quoteForm.itemHabitatForm.setValueType(ItemHabitat.VALUE_INPUT_EDIT);
			 	}
		 	}else{
				productProxy.findPlanByProductId(productOption.productId);
		 	}
		 	if(productOption.objectId == Item.OBJECT_LIFE_CYCLE){
		 		productProxy.listPersonalRiskPlanByProductId(productOption.productId, productOption.referenceDate);
		 	}		 	
		 	if(productOption.objectId == Item.OBJECT_PERSONAL_ACCIDENT){
			 	proposalProxy.listAllOccupation();
			 	productProxy.listPlanRiskTypeDescriptionByRefDate(productOption.productId, ID_PLAN_RISK_DEFAULT, productOption.referenceDate);
		 	}
		 	if(productOption.objectId == Item.OBJECT_AUTOMOVIL){
		 		productProxy.listAutoBrands();
				dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_GENDER, AUTO_IDENTIFIED_GENDER_TYPE_ID);
				dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_MARITAL_STATUS, AUTO_IDENTIFIED_MARITAL_STATUS_TYPE_ID);
				dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_AUTO_USE_TYPE, AUTO_IDENTIFIED_USE_TYPE_ID);
		 	}
		 	if(productOption.objectId == Item.OBJECT_CONDOMINIUM){		 		
				dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_RENEWAL_TYPE, CONDOMINIUM_IDENTIFIED_RENEWAL_TYPE_ID);
				dataOptionProxy.findIdentifiedListDataOptionByDataGroupParentId(ID_DATA_GROUP_PROPERTY_RISK, productOption.objectType, CONDOMINIUM_IDENTIFIED_PROPERTY_RISK_ID);
				dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_REGION_CODE, CONDOMINIUM_IDENTIFIED_REGION_CODE_ID);
		 	}
		 	if(productOption.objectId == Item.OBJECT_CAPITAL){
		 		dataOptionProxy.findIdentifiedListDataOptionByDataGroupParentId(ID_DATA_GROUP_PROPERTY_RISK, productOption.objectType, CAPITAL_IDENTIFIED_COMPANY_TYPE_ID);
		 		productProxy.listPlanRiskTypeDescriptionByRefDate(productOption.productId, ID_PLAN_RISK_DEFAULT, productOption.referenceDate);
		 	}
		 	if(productOption.objectId == Item.OBJECT_PURCHASE_PROTECTED){		 		
				dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_CARD_TYPE, PURCHASE_PROTECTED_IDENTIFIED_CARD_TYPE_ID);
		 	}
		 	
		 	this.populateChannelList();
		}
		
		private function populatePlan( event:Event ):void
		{
			if(productOption.objectId == Item.OBJECT_HABITAT)
				productProxy.findRiskPlanByProductId(productOption.productId, new Date());
			else
				productProxy.findPlanByProductId(productOption.productId);
		}
		
		private function renewalTypeSelected( event:Event ):void
		{
			var renewalType : DataOption = quoteForm.itemCondominium.cmbxRenewalType.selectedItem as DataOption;
			
			if (renewalType != null) {
				dataOptionProxy.findIdentifiedListDataOptionByDataGroupParentId(ID_DATA_GROUP_RENEWAL_INSURER, renewalType.dataOptionId, CONDOMINIUM_IDENTIFIED_RENEWAL_INSURER_ID);				
			}
		}
		
		private function typeCompanySelected( evt:Event ):void
		{
			dataOptionProxy.findIdentifiedListDataOptionByDataGroupParentId(ID_DATA_GROUP_ACTIVITY_TYPE, quoteForm.itemCapital.getTypeCompany().dataOptionId, CAPITAL_IDENTIFIED_ACTIVITY_COMPANY_ID);
		}
		
		private function cardTypeChanged( evt:Event ):void
		{
			dataOptionProxy.findIdentifiedListDataOptionByDataGroupParentId(ID_DATA_GROUP_CARD_BRAND_TYPE, quoteForm.itemPurchaseProtected.cmbxCardType.selectedValue, PURCHASE_PROTECTED_IDENTIFIED_CARD_BRAND_TYPE_ID);
		}
		
		public function reloadQuote(event : Event) : void{
			productOption = quoteForm.endorsementOption.productOption;
			productProxy.getProductVersionByBetweenRefDate( productOption.productId, new Date() );
			productProxy.getApplicationPropertyValue( 1, "RoadmapActive" );
			
			var itemRisk: Item = null;
			var items:ArrayCollection = null;
			
			endorsementCalculated = quoteForm.endorsementOption.endorsement;
			quoteForm.contractId = quoteForm.endorsementOption.endorsement.contract.contractId;
			
			quoteForm.itemInsuredComponent.endorsement = quoteForm.endorsementOption.endorsement;
			quoteForm.itemSummary.endorsementOption = quoteForm.endorsementOption;
			quoteForm.coverageListComponent.commissionFactory = productOption.commissionFactory;
			var item:Item = quoteForm.endorsementOption.endorsement.getMainItem();
			quoteForm.coverageListComponent.itemCoverageListed = item.itemCoverages;
			quoteForm.coverageListComponent.planId = item.coveragePlanId;
			listCoveragePlan( null );
			findProductProfile(false);
			channelProxy.listChannel( quoteForm.endorsementOption.endorsement.contract.partnerId, true);

			itemRisk = quoteForm.endorsementOption.endorsement.getMainItem();
		
			switch( productOption.objectId )
			{
				case Item.OBJECT_CREDIT_INSURANCE :
					productProxy.getProductVersionByRefDate(productOption.productId, endorsementCalculated.referenceDate);
					quoteForm.itemPersonalCreditInsurance.itemPersonalRisk = itemRisk as ItemPersonalRisk;
					break;
				case Item.OBJECT_HABITAT :
				 	dataOptionProxy.findIdentifiedListDataOptionByDataGroupParentId(ID_DATA_GROUP_PROPERTY_RISK, productOption.objectType, HABITAT_IDENTIFIED_TYPE_HOUSING_ID); 		
			 		productProxy.findRiskPlanByProductId(productOption.productId, endorsementCalculated.referenceDate);
			 		productProxy.getProductVersionByRefDate(productOption.productId, productOption.referenceDate);
			 		if ( this.productOption.riskValueType == 161 ) {
						quoteForm.itemHabitatForm.setValueType(ItemHabitat.VALUE_INPUT_COMBOBOX);
				 	} else {
						quoteForm.itemHabitatForm.setValueType(ItemHabitat.VALUE_INPUT_EDIT);
				 	}

				 	quoteForm.itemHabitatForm.itemProperty = itemRisk as ItemProperty;
					break;
				case Item.OBJECT_LIFE :
					quoteForm.itemLife.itemPersonalRisk = itemRisk as ItemPersonalRisk;
					break;
				case Item.OBJECT_PURCHASE_PROTECTED :
					quoteForm.itemPurchaseProtected.itemPurchaseProtected = itemRisk as ItemPurchaseProtected;
					dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_CARD_TYPE, PURCHASE_PROTECTED_IDENTIFIED_CARD_TYPE_ID);
					dataOptionProxy.findIdentifiedListDataOptionByDataGroupParentId(ID_DATA_GROUP_CARD_BRAND_TYPE, quoteForm.itemPurchaseProtected.itemPurchaseProtected.cardType, PURCHASE_PROTECTED_IDENTIFIED_CARD_BRAND_TYPE_ID);					
					break;
				case Item.OBJECT_LIFE_CYCLE : 
					quoteForm.itemLifeCycle.planRiskId = quoteForm.endorsementOption.endorsement.riskPlanId;
					quoteForm.itemLifeCycle.itemPersonalRisk = itemRisk as ItemPersonalRisk;
					productProxy.listPersonalRiskPlanByProductId(productOption.productId, endorsementCalculated.referenceDate);
					
					if( quoteForm.endorsementOption && quoteForm.endorsementOption.action == EndorsementOption.ACTION_CHANGE_TECHNICAL )
						quoteForm.itemLifeCycle.loadAdditional();
					break;
				case Item.OBJECT_PERSONAL_ACCIDENT :
					proposalProxy.listAllOccupation();
					productProxy.listPlanRiskTypeDescriptionByRefDate(item.endorsement.contract.productId, item.endorsement.riskPlanId, item.endorsement.referenceDate);
					quoteForm.itemPersonalAccident.itemPersonalRisk = itemRisk as ItemPersonalRisk;
					quoteForm.itemPersonalAccident.customer = quoteForm.endorsementOption.endorsement.customerByInsuredId;
					break;
				case Item.OBJECT_AUTOMOVIL:
					quoteForm.itemVehicle.itemAuto = itemRisk as ItemAuto;
					var motorist : Motorist = (itemRisk as ItemAuto).getMainMotorist();
					quoteForm.itemMotorist.motorist = motorist;
					quoteForm.itemMotorist.createBirthDate(motorist.birthDate);
					populateAutoCost(quoteForm.itemVehicle.itemAuto.valueRefer);
		 			productProxy.listAutoBrands();
					productProxy.listAutoYear( quoteForm.itemVehicle.itemAuto.autoModelId, productOption.referenceDate );
					productProxy.listAutoModelForBrand( quoteForm.itemVehicle.itemAuto.autoBrandId );		
					productProxy.findAutoVersion(quoteForm.itemVehicle.itemAuto.autoModelId, productOption.referenceDate );
					dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_AUTO_TRANSMISSION, AUTO_IDENTIFIED_TRANSMISSION_TYPE_ID);	
					dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_AUTO_CLASS, AUTO_IDENTIFIED_CLASS_TYPE_ID);
					dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_GENDER, AUTO_IDENTIFIED_GENDER_TYPE_ID);
					dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_MARITAL_STATUS, AUTO_IDENTIFIED_MARITAL_STATUS_TYPE_ID);
					dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_AUTO_USE_TYPE, AUTO_IDENTIFIED_USE_TYPE_ID);
					quoteForm.itemVehicle.enableFields();
					break;
				case Item.OBJECT_CONDOMINIUM:
					quoteForm.itemCondominium.itemProperty = itemRisk as ItemProperty;
					dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_RENEWAL_TYPE, CONDOMINIUM_IDENTIFIED_RENEWAL_TYPE_ID);
					dataOptionProxy.findIdentifiedListDataOptionByDataGroupParentId(ID_DATA_GROUP_PROPERTY_RISK, productOption.objectType, CONDOMINIUM_IDENTIFIED_PROPERTY_RISK_ID);
					dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_REGION_CODE, CONDOMINIUM_IDENTIFIED_REGION_CODE_ID);
					break;
				case Item.OBJECT_CAPITAL:
					quoteForm.itemCapital.itemProperty = itemRisk as ItemProperty;
			 		dataOptionProxy.findIdentifiedListDataOptionByDataGroupParentId(ID_DATA_GROUP_PROPERTY_RISK, productOption.objectType, CAPITAL_IDENTIFIED_COMPANY_TYPE_ID);
			 		dataOptionProxy.findIdentifiedListDataOptionByDataGroupParentId(ID_DATA_GROUP_ACTIVITY_TYPE, (itemRisk as ItemProperty).propertyType, CAPITAL_IDENTIFIED_ACTIVITY_COMPANY_ID);
			 		productProxy.listPlanRiskTypeDescriptionByRefDate(productOption.productId, endorsementCalculated.riskPlanId, endorsementCalculated.referenceDate);
					break;
			}
		}
		
		private function calculateLoad( event:Event ):void
		{
			calculateProxy.getPaymentOptionsDefault( quoteForm.endorsementOption.endorsement );			
		}
		
		public function listKinshipType( event:Event ):void
		{
	    	var items:int = int( quoteForm.itemLifeCycle.txtQuantityAdditional.text ) + int( quoteForm.itemLifeCycle.txtQuantityMinor.text );
	    	
	    	if( quoteForm.itemLifeCycle.itemPersonalRisk )
	    	{
		    	if( items > quoteForm.itemLifeCycle.itemPersonalRisk.endorsement.listNotHolder().length )
		    	{
		    		Alert.show(ResourceManager.getInstance().getString("messages","policyChange"), ResourceManager.getInstance().getString("messages", "warning"), 
					  Alert.OK|Alert.CANCEL, ApplicationFacade.getInstance().getEinsurance,
					  	function (event:CloseEvent):void{
							if(event.detail == Alert.OK)
								calculate( event );
							else
								quoteForm.itemLifeCycle.loadAdditional();
						}, null, Alert.OK);
		    	}
		    	else
		    	{
    				productProxy.loadItemAdditional();
    				
    				var renewalType:int = Item.RENEW_TYPE_NEW; //Seguro Novo
    				if( quoteForm.endorsementOption && quoteForm.endorsementOption.endorsement && quoteForm.endorsementOption.endorsement.contract.renewed)
    					renewalType = Item.RENEW_TYPE_RENEWAL; //Renovacao
    					
    				productProxy.listPlanKinshipByRiskPlan(productOption.productId, 
    													   quoteForm.itemLifeCycle.getRiskPlanSelected().id.planId,
    													   renewalType, quoteForm.itemLifeCycle.itemPersonalRisk.endorsement.referenceDate,
    													   productProxy.onListPlanKinshipByRiskPlanQuote);		    		
		    	}
		    }		
			
		}
		
		public function populateItemAdditional( proposalListData:ProposalListData ):void
		{
			quoteForm.itemAdditional.proposalListData = proposalListData;
			quoteForm.itemAdditional.listItemType = quoteForm.itemLifeCycle.listItem;
			quoteForm.itemAdditional.itemPersonalRisk = quoteForm.itemLifeCycle.itemPersonalRisk;
			
			PopUpManager.addPopUp( quoteForm.itemAdditional, ApplicationFacade.getInstance().getEinsurance, true );
			PopUpManager.centerPopUp( quoteForm.itemAdditional );
		}
		/** Listeners **/
		private function findCoverageByPlan(evt : Event) : void {
			var planSelected : ProductPlan = quoteForm.coverageListComponent.getPlanSelected();
			if(planSelected != null){
				switch ( productOption.objectId ) 
				{
					case Item.OBJECT_LIFE:
					case Item.OBJECT_LIFE_CYCLE:
					case Item.OBJECT_CREDIT_INSURANCE:
					case Item.OBJECT_PERSONAL_ACCIDENT:
					case Item.OBJECT_PURCHASE_PROTECTED:
						quoteForm.coverageListComponent.updateRiskValue1(planSelected.fixedInsuredValue);
						productProxy.listCoverageOptionByRefDate(productOption.productId
																, planSelected.id.planId
																, productOption.referenceDate);
						break;
					case Item.OBJECT_HABITAT:
						var planRisk : ProductPlan = quoteForm.itemHabitatForm.getRiskPlanSelected();
						if(planRisk != null){
							productProxy.listCoverageOptionByRefDate(productOption.productId, 
																	planSelected.id.planId, 
																	productOption.referenceDate);
						}
						break;
					default:
						productProxy.listCoverageOptionByRefDate(productOption.productId, planSelected.id.planId, productOption.referenceDate);
				}
			}
		}
		
		private function listCoveragePlan(evt : Event) : void {
			if(productOption.objectId == Item.OBJECT_HABITAT){
				var riskPlan : ProductPlan = quoteForm.itemHabitatForm.getRiskPlanSelected();
				if(riskPlan != null){
					productProxy.listCoveragePlanByProductId(productOption.productId, riskPlan.id.planId, productOption.referenceDate);
				}
			}else {
				productProxy.findPlanByProductId(productOption.productId, productOption.referenceDate);
			}
		}
		
		private function hiring(evt : Event) : void {
			var endorsementOption : EndorsementOption;
			
			if( quoteForm.endorsementOption )
				endorsementOption = quoteForm.endorsementOption;
			else
			{
				endorsementOption = new EndorsementOption();
				endorsementOption.action = EndorsementOption.ACTION_EFFECTIVE;
			}
			endorsementOption.endorsement = endorsementCalculated;
			endorsementOption.channelName = (quoteForm.itemSummary.channelList.selectedItem as Channel).nickName;
			endorsementOption.partnerName = ApplicationFacade.getInstance().getSelectedPartner().nickName;
			endorsementOption.productOption = quoteForm.productOption;
			
			if(productOption.objectId == Item.OBJECT_LIFE_CYCLE){
				var productPlan:ProductPlan = quoteForm.itemLifeCycle.cmbxPlanRisk.selectedItem as ProductPlan;
				var planPersonalRisk:PlanPersonalRisk = new PlanPersonalRisk();
				var planPersonalRiskId:PlanPersonalRiskId = new PlanPersonalRiskId();
				
				planPersonalRiskId.planId = productPlan.plan.planId; 
				planPersonalRiskId.productId = productPlan.product.productId;
				planPersonalRiskId.personType = PersonalRiskOption.NATURAL_PERSON;
				planPersonalRiskId.effectiveDate = endorsementOption.endorsement.referenceDate;
				
				planPersonalRisk.id = planPersonalRiskId; 
				planPersonalRisk.plan = productPlan.plan; 
				planPersonalRisk.quantityPersonalRisk = productPlan.quantityPersonalRisk;
				planPersonalRisk.quantityAdditional = productPlan.quantityAdditional;
				planPersonalRisk.additionalNewborn = productPlan.additionalNewborn;

				endorsementOption.planPersonalRisk = planPersonalRisk; 
			}
			
			desktopInstance.proposal.returnModule = quoteForm.returnModule;
			desktopInstance.proposal.searchDocumentParameters = quoteForm.searchDocumentParameters;
			
			ApplicationFacade.getInstance().sendNotification(NotificationViewsList.VIEW_PROPOSAL_MAIN, endorsementOption);
		}
		
		private function updateRiskValueHabitat(evt : Event):void{
			switch(quoteForm.itemHabitatForm.getRiskPlanSelected().id.planId){
				case QuoteForm.ID_PLAN_RISK_ALL:
			 		productProxy.listCoverageRangeValueByRefDate(productOption.productId, QuoteForm.ID_PLAN_RISK_ALL, ItemHabitat.COVERAGE_STRUCTURE, productOption.referenceDate);
			 		productProxy.listCoverageRangeValueByRefDate(productOption.productId, QuoteForm.ID_PLAN_RISK_ALL, ItemHabitat.COVERAGE_CONTENT, productOption.referenceDate);
					break;
				case QuoteForm.ID_PLAN_RISK_STRUCTURE:
					productProxy.listCoverageRangeValueByRefDate(productOption.productId, QuoteForm.ID_PLAN_RISK_STRUCTURE, ItemHabitat.COVERAGE_STRUCTURE, productOption.referenceDate);
					break;
				case QuoteForm.ID_PLAN_RISK_CONTENT:
					productProxy.listCoverageRangeValueByRefDate(productOption.productId, QuoteForm.ID_PLAN_RISK_CONTENT, ItemHabitat.COVERAGE_CONTENT, productOption.referenceDate);
					break;
			}
		}		

		/** Populates **/		
		private function populateCoverageList(list : ArrayCollection) : void {
			quoteForm.coverageListComponent.createListCoverage(list);
			productProxy.listAllDeductible();
		}
		
		private function populateDeductibleList(list : ArrayCollection) : void {
			quoteForm.coverageListComponent.createDeductibleList(list);
		}
		
		private function populateCoverageInsuredValueList(itemCoverageList : ArrayCollection) : void {
			
		 	for each(var itemCoverage : ItemCoverage in itemCoverageList){
				quoteForm.coverageListComponent.populateCoverageInsuredValueList(itemCoverage.id.coverageId, productOption.productId, itemCoverage.insuredValue);
		 	}
		}
		
		private function populateOccupationList(list : ArrayCollection) : void {
			quoteForm.itemPersonalAccident.createListOccupation(list);
		}	
		
		private function populateBrandList( list : ArrayCollection ):void
		{
			quoteForm.itemVehicle.createBrandList( list );
		}
		
		private function populateModelList( list : ArrayCollection ):void
		{
			quoteForm.itemVehicle.createModelList( list );
		}	
		
		private function populateYearList( list : ArrayCollection ):void
		{
			quoteForm.itemVehicle.createYearList( list );
		}
		
		private function populateAutoCost( cost:Number ):void
		{
			quoteForm.coverageListComponent.updateRiskValue1( cost );
			quoteForm.itemVehicle.createAutoCost( cost );
		}
		
		private function populateAutoVersion( autoVersion:AutoVersion ):void
		{
			
			if (autoVersion != null) {
				quoteForm.itemVehicle.createAutoVersion( autoVersion );
				dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_AUTO_CATEGORY, AUTO_IDENTIFIED_CATEGORY_TYPE_ID);
			
				if (autoVersion.autoCategoryType != 0) {
					findAutoGroupByCategory(autoVersion.autoCategoryType);			
				}
				else {
					dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_AUTO_GROUP, AUTO_IDENTIFIED_GROUP_TYPE_ID);
				}
			}
		}
		
		public function autoCategoryChanged( event:Event ):void
		{
			findAutoGroupByCategory(quoteForm.itemVehicle.cmbxCategory.selectedValue);
		}
		
		public function findAutoGroupByCategory( categoryType : int ):void
		{
			dataOptionProxy.findIdentifiedListDataOptionByDataGroupParentId(ID_DATA_GROUP_AUTO_GROUP, categoryType, AUTO_IDENTIFIED_GROUP_TYPE_ID);	
		}
		
		public function searchAutoModel( event:Event ):void
		{
			productProxy.listAutoModelForBrand( (quoteForm.itemVehicle.cmbxAutoBrand.selectedItem as AutoBrand).autoMarkId );
		}
		
		public function autoModelSelected( event:Event ):void
		{
			productProxy.listAutoYear( (quoteForm.itemVehicle.cmbxAutoModel.selectedItem as AutoModel).autoModelId, productOption.referenceDate );		
			productProxy.findAutoVersion( (quoteForm.itemVehicle.cmbxAutoModel.selectedItem as AutoModel).autoModelId, productOption.referenceDate ); 
			dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_AUTO_TRANSMISSION, AUTO_IDENTIFIED_TRANSMISSION_TYPE_ID);	
			dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_AUTO_CLASS, AUTO_IDENTIFIED_CLASS_TYPE_ID);	
		}
		
		public function searchAutoCost( event:Event ):void
		{
			var year:int = quoteForm.itemVehicle.cmbxAutoYear.selectedItem as int;
			
			/*if( year == new Date().getFullYear() && quoteForm.itemVehicle.chkZero.selected )
				year = 0;*/
				
			productProxy.getAutoCost( (quoteForm.itemVehicle.cmbxAutoModel.selectedItem as AutoModel).autoModelId, year, productOption.referenceDate );
		}		
				
		public function searchAutoTonne( event:Event ):void
		{
			dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_TONNE_QTT, AUTO_IDENTIFIED_TONNE_QTT_TYPE_ID);
		}		
		
		/**
		 * Populates Restriction Risk list
		 * @param list Restriction Risk list
		 */
		private function populateRestrictionRiskList(list : ArrayCollection) : void {
			switch( quoteForm.productOption.objectId )
			{
				case Item.OBJECT_CAPITAL:
					quoteForm.itemCapital.createRiskType( list );
					break;
				case Item.OBJECT_PERSONAL_ACCIDENT:
					quoteForm.itemPersonalAccident.createRestrictionRiskList(list);
					break;
			}
		}

		private function populatePlanList(list : ArrayCollection) : void {
			quoteForm.coverageListComponent.createListPlan(list);
		}

		private function populateChannelList() : void {
		 	var allChannel : Boolean = false;
		 	for each(var userPartner : UserPartner in ApplicationFacade.getInstance().loggedUser.userPartners){
				if(userPartner.id.partnerId == quoteForm.partner.partnerId){
					if(userPartner.allChannel == true){
						channelProxy.listChannel(quoteForm.partner.partnerId, true);
						allChannel = true;
					}
				}
		 	}
		 	if(allChannel == false){
			 	quoteForm.itemSummary.listChannel( quoteForm.channelListProvider );
		 	}

		}
		
		/**Calculate*/
		private function calculate(evt : Event) : void {
			var validation : Boolean = this.validationForm();
			var endorsement : Endorsement = null;
			
			if(validation){
				
				// create endorsement
				endorsement = createEndorsement();
				
				// cria item
				this.createItem(endorsement);
				
				// calc
				if(productOption.objectId == Item.OBJECT_HABITAT || productOption.objectId == Item.OBJECT_LIFE_CYCLE ){
					calculateProxy.calculate(endorsement, productOption.objectId, quoteForm.coverageListComponent.getAdgSource());
				}else{
					calculateProxy.calculate(endorsement, productOption.objectId, null);
				}
			}
		}
		
		private function createEndorsement():Endorsement{
				var contract : Contract = null;
				var endorsement : Endorsement = null;
				var endorsementId : EndorsementId = new EndorsementId();;
				var channelSelected : Channel = quoteForm.itemSummary.channelList.selectedItem as Channel;

				if( quoteForm.endorsementOption && quoteForm.endorsementOption.action == EndorsementOption.ACTION_CHANGE_TECHNICAL )
				{
					endorsement = ObjectUtil.copy( endorsementCalculated ) as Endorsement;
					contract = endorsement.contract;
					
					endorsement.termId = ID_TERM_PRORATA;
					endorsement.effectiveDate = new Date();
										
					quoteForm.itemInsuredComponent.createItem(endorsement.customerByInsuredId);
				}
				else if( quoteForm.contractId != 0 )
				{
					endorsement = endorsementCalculated;
					contract = endorsement.contract;
									
					if( !contract.renewed )
					{
						contract.effectiveDate = new Date();
						endorsement.effectiveDate = contract.effectiveDate;
						endorsement.termId = ID_TERM_ANUAL;
						contract.termId = ID_TERM_ANUAL;
						contract.expiryDate = Utilities.getDateExpireDate(contract.effectiveDate, contract.termId);
						if(productOption.objectId == Item.OBJECT_CREDIT_INSURANCE) {
							contract.renewalLimitDate = quoteForm.itemPersonalCreditInsurance.fechaCreditoDateItemRisk.newDate;
						}
						endorsement.channelId = channelSelected.channelId;
					}
					
					quoteForm.itemInsuredComponent.createItem(endorsement.customerByInsuredId);
					
					if( endorsement.customerByInsuredId.addresses && endorsement.customerByInsuredId.addresses.length > 0) 
					{
						var address:Address = endorsement.customerByInsuredId.addresses.getItemAt(0) as Address;
						
						if( address.countryId < 1 || address.stateId < 1 || address.cityId < 1 )
						{
							endorsement.customerByInsuredId.addresses = new ArrayCollection();
						}
					}
					
					if( endorsement.customerByPolicyHolderId && endorsement.customerByPolicyHolderId.addresses && endorsement.customerByPolicyHolderId.addresses.length > 0 ) 
					{
						var address:Address = endorsement.customerByPolicyHolderId.addresses.getItemAt(0) as Address;
						
						if( address.countryId < 1 || address.stateId < 1 || address.cityId < 1 )
						{
							endorsement.customerByInsuredId.addresses = new ArrayCollection();
						}
					}
				}
				else
				{
				  	contract = new Contract();
					endorsement = contract.addEndorsement();
					endorsementId.contractId =  quoteForm.contractId;
					endorsementId.endorsementId = 1;
					contract.partnerId = ApplicationFacade.getInstance().loggedUser.transientSelectedPartner;
					endorsement.issuanceType = Endorsement.ISSUANCE_TYPE_EMISSAO;
					endorsement.id = endorsementId;
					quoteForm.itemInsuredComponent.createItem(endorsement.addCustomer());
					
					contract.insurerId = ID_INSURER;
					contract.contractId = quoteForm.contractId;
					contract.subsidiaryId = channelSelected != null ? channelSelected.subsidiaryId : 0;
					contract.brokerId = productOption.brokerId;
					contract.referenceDate = productOption.referenceDate;
					
		 			contract.masterPolicyId = productOption.masterPolicyId;
		 			contract.productId = productOption.productId ;
					contract.currencyId = productOption.currencyId;
		
					contract.effectiveDate = new Date();
					endorsement.effectiveDate = contract.effectiveDate;
					endorsement.termId = ID_TERM_ANUAL;
					contract.termId = ID_TERM_ANUAL;
					contract.expiryDate = Utilities.getDateExpireDate(contract.effectiveDate, contract.termId);
					if(productOption.objectId == Item.OBJECT_CREDIT_INSURANCE) {
						contract.renewalLimitDate = quoteForm.itemPersonalCreditInsurance.fechaCreditoDateItemRisk.newDate;
					}
					endorsement.channelId = channelSelected.channelId; 
				}
				
				endorsement.expiryDate = contract.expiryDate; 
				endorsement.quotationDate = new Date();
				endorsement.referenceDate = productOption.referenceDate;
				endorsement.hierarchyType = ID_HIERARCHY_TYPE; 
				endorsement.userId = ApplicationFacade.getInstance().loggedUser.userId;
				
			return endorsement;
		}
		
		private function createItem(endorsement : Endorsement){
			var itemRisk: Item;

			// Obrigatórios do item específico, componente variável.
			if( endorsement.id.contractId != 0 )
			{
				itemRisk = endorsement.getMainItem();
			}
			else
			{
				itemRisk = endorsement.addItem(productOption.objectId);
				itemRisk.objectId = productOption.objectId;
			}
			
			switch(productOption.objectId){
				case Item.OBJECT_LIFE:
					quoteForm.itemLife.createItem(itemRisk, endorsement);
					endorsement.riskPlanId = ID_PLAN_RISK_DEFAULT;
					break;
				case Item.OBJECT_PERSONAL_ACCIDENT:
					quoteForm.itemPersonalAccident.createItem(itemRisk, endorsement);
					endorsement.riskPlanId = ID_PLAN_RISK_DEFAULT;
					break;
				case Item.OBJECT_CREDIT_INSURANCE:
					quoteForm.itemPersonalCreditInsurance.createItem(itemRisk, endorsement);
					endorsement.riskPlanId = ID_PLAN_RISK_DEFAULT;
					break;
				case Item.OBJECT_HABITAT:
					( itemRisk as ItemProperty ).propertyRiskType = quoteForm.itemHabitatForm.getRiskPlanSelected().getPlanRiskTypeDefault().id.riskType;
					quoteForm.itemHabitatForm.createItem(itemRisk);
					endorsement.riskPlanId = quoteForm.itemHabitatForm.getRiskPlanSelected().id.planId;
					break;
				case Item.OBJECT_LIFE_CYCLE:
					quoteForm.itemLifeCycle.createItem(itemRisk, endorsement);
					endorsement.riskPlanId = quoteForm.itemLifeCycle.getRiskPlanSelected().id.planId;
					break;
				case Item.OBJECT_AUTOMOVIL:
					quoteForm.itemVehicle.createItem(itemRisk, endorsement);
					quoteForm.itemMotorist.createItem((itemRisk as ItemAuto), endorsement);
					endorsement.riskPlanId = ID_PLAN_RISK_DEFAULT;
					break;
				case Item.OBJECT_CONDOMINIUM:
					quoteForm.itemCondominium.createItem(itemRisk);
					endorsement.riskPlanId = ID_PLAN_RISK_DEFAULT;
					break;
				case Item.OBJECT_CAPITAL:
					quoteForm.itemCapital.createItem(itemRisk);
					endorsement.riskPlanId = ID_PLAN_RISK_DEFAULT;
					break;
				case Item.OBJECT_PURCHASE_PROTECTED:
					quoteForm.itemPurchaseProtected.createItem(itemRisk, endorsement);
					endorsement.riskPlanId = ID_PLAN_RISK_DEFAULT;
					break;
			}
				
			// Obrigatórios do item pai
			itemRisk.coveragePlanId =  quoteForm.coverageListComponent.getPlanSelected().id.planId;
			if( !quoteForm.endorsementOption || quoteForm.endorsementOption.action != EndorsementOption.ACTION_CHANGE_TECHNICAL )
				itemRisk.effectiveDate = endorsement.effectiveDate; 
			
			var renewalType:int = Item.RENEW_TYPE_NEW; //Seguro Novo
			if( quoteForm.endorsementOption && quoteForm.endorsementOption.endorsement && quoteForm.endorsementOption.endorsement.contract.renewed)
				renewalType = Item.RENEW_TYPE_RENEWAL; //Renovacao
			
			itemRisk.renewalType = renewalType;
			
			if(productOption.objectId != Item.OBJECT_HABITAT && productOption.objectId != Item.OBJECT_LIFE_CYCLE){
				// Obrigatórios na lista de coberturas
				quoteForm.coverageListComponent.createItem(itemRisk);
			}
			
			if(productOption.profileRequired) {
				quoteForm.itemProfile.createItem(itemRisk, endorsement);
			}
		}
			
		public function validationForm () : Boolean {
			var isValid : Boolean  = Utilities.validateForm(quoteForm.itemInsuredComponent.validationArray, ResourceManager.getInstance().getString('resources', 'resumeInsuredData'));
			// Obrigatorios 
			if(isValid == false){
				return false;
				
			// Channel 
			}else if ((quoteForm.itemSummary.channelList.selectedItem as Channel) == null){
				Utilities.warningMessage("userNotFoundChannel");
				return false;
				
			// CoverageList	
			}else if(quoteForm.coverageListComponent.validateCoverageList() == false){
				return false;
				
			}else if(quoteForm.itemSummary.validationForm()){
				
				//  CoverageList form
				var messages : ArrayCollection = new ArrayCollection();
				
				// AccidentesPersonales
				if(quoteForm.accordionItemProductAccidentes.visible == true){
					var isValidPersonalAccident : Boolean = Utilities.validateForm(quoteForm.itemPersonalAccident.validatorForm, ResourceManager.getInstance().getString('resources', 'risk'));
					if(isValidPersonalAccident == false){
						return false;
					}
				}
				// Life
				else if(quoteForm.accordionItemProductZurich.visible == true){
					var isValidLife : Boolean = Utilities.validateForm(quoteForm.itemLife.validatorForm, ResourceManager.getInstance().getString('resources', 'resumeRiskData'));
					if(isValidLife == false){
						return false;
					}
				}
				// CreditInsurance
				else if(quoteForm.accordionItemCreditInsurance.visible == true){
					var isCreditInsurance : Boolean = Utilities.validateForm(quoteForm.itemPersonalCreditInsurance.validatorForm, ResourceManager.getInstance().getString('resources', 'risk'));
					
					if(isCreditInsurance == false){
						return false;
					}
					
					var workCreditDate:Date = quoteForm.itemPersonalCreditInsurance.fechaCreditoDateItemRisk.newDate;
					var now:Date = new Date();
					
					if(workCreditDate.getTime() < now.getTime()){
						Utilities.errorMessage("creditDate");
						quoteForm.itemPersonalCreditInsurance.fechaCreditoDateItemRisk.setFocus();
						return false;
					}
				}
				// Habitat
				else if(quoteForm.accordionItemHabitat.visible == true){
					var isValidHabitat : Boolean = Utilities.validateForm(quoteForm.itemHabitatForm.validatorHabitat, ResourceManager.getInstance().getString('resources', 'risk'));
					if(isValidHabitat == false){
						return false;
					}
				}
				// LifeCycle
				else if(quoteForm.accordionItemLifeCycle.visible == true && (quoteForm.itemLifeCycle.validateForm() == false)){
					return false;
				}

				//Automovil
				else if( quoteForm.accordionItemVehicle.visible )
				{
					var isValidateAuto : Boolean = Utilities.validateForm(quoteForm.itemVehicle.validatorForm, ResourceManager.getInstance().getString('resources', 'risk'));
					var isValidateMotorist : Boolean = Utilities.validateForm(quoteForm.itemMotorist.validatorForm, ResourceManager.getInstance().getString('resources', 'risk'));
					if( !isValidateAuto || !isValidateMotorist )
						return false;
				} 
				// Credit Card
				else if(quoteForm.accordionItemPurchaseProtected.visible == true){
					var isValidPurchaseProtected : Boolean = Utilities.validateForm(quoteForm.itemPurchaseProtected.validatorForm, ResourceManager.getInstance().getString('resources', 'resumeRiskData'));
					if(isValidPurchaseProtected == false){
						return false;
					}
				}
				//Capital
				else if( quoteForm.accordionItemCapital.visible )
				{
					var isValidateCapital : Boolean = Utilities.validateForm( quoteForm.itemCapital.validatorForm(), ResourceManager.getInstance().getString('resources', 'risk'));
					if( !isValidateCapital ){
						return false;
					}
				}
				
				messages.addItem(quoteForm.coverageListComponent.validationCombo());
				messages.addItem(quoteForm.coverageListComponent.validationForm());
				
				// Print validated messages
				 for each (var message : String in messages){
					if(message != null){
						Alert.show(message, ResourceManager.getInstance().getString("messages","warning"), Alert.OK, null, null, Utilities.warningMsg);
						return false;
					}
				} 
			}
			return true;
		}
		
		private function populateResultCalculate(result : SimpleCalcResult) : void {
			if(result != null && result.endorsement != null){
				if(result.endorsement.errorList != null && Utilities.validateEndorsementReturn(result.endorsement.errorList) == true){
					endorsementCalculated = result.endorsement;
					quoteForm.paymentListComponent.createItem(result.paymentOptions);
					quoteForm.itemSummary.contractNumber.text = result.endorsement.contract.contractId.toString();
					var itemIsCalulated : Boolean = true;
					for each (var item : Item in  endorsementCalculated.items){
						if(item.calculationValid == false){
							itemIsCalulated = false;
						}
					}	
					quoteForm.contractId = result.endorsement.contract.contractId;
					quoteForm.enableComponentsAfterAndBeforeCalculate(itemIsCalulated);	
					if(itemIsCalulated == false){
						Utilities.warningMessage("itemCalculateInvalid");
					}else{
						if( endorsementCalculated.getMainItem().objectId == Item.OBJECT_LIFE_CYCLE )
						{
							quoteForm.itemLifeCycle.itemPersonalRisk = endorsementCalculated.getMainItem() as ItemPersonalRisk;
						}
					}
					
					var item:Item = endorsementCalculated.getMainItem();
					quoteForm.coverageListComponent.updateItemCoverageList( item.itemCoverages );
					populateCoverageInsuredValueList(item.itemCoverages);
				}
			}else{
				if ((result.erros != null) && ( result.erros.length > 0 )) {
					var mensagens: String = "";
					for(var aux: int = 0; aux < result.erros.length; aux++ )
						mensagens += ((mensagens=="")?"":"\n") + result.erros[aux];
					quoteForm.enableComponentsAfterAndBeforeCalculate(false);
					ErrorWindow.createAndShowError(Errors.INTERNAL_ERROR, mensagens);					
				} 
				else Utilities.errorMessage(Errors.INTERNAL_ERROR);
			}
		}

		/**
		 * Show Error Message
		 * @param object body received
		 */ 
		private function showErrorMessage(object: Object): void {
			quoteForm.enableComponentsAfterAndBeforeCalculate(false);	
			if (object instanceof FaultEvent ) {
				var faultEvent: FaultEvent = (object as FaultEvent);
				ErrorWindow.createAndShowError(Errors.INTERNAL_ERROR, faultEvent.fault.getStackTrace());
			}
			else
				Utilities.errorMessage(Errors.INTERNAL_ERROR);
		}
		
		private function validateAuto() : Boolean {
			var validatorArr : ArrayCollection =  new ArrayCollection();
			var vehicleArr : ArrayCollection =  new ArrayCollection(quoteForm.itemVehicle.validatorForm);
			var motoristArr : ArrayCollection =  new ArrayCollection(quoteForm.itemMotorist.validatorForm);
			
			validatorArr.addAll(vehicleArr);
			validatorArr.addAll(motoristArr);
			
			return Validator.validateAll(validatorArr.source).length == 0;
		}
		
		private function profileResponseAdded( event:Event ) : void {
			var responseDictionary : Dictionary = quoteForm.itemProfile.responseDictionary;
			
			for (var questionObj:Object in responseDictionary)
			{
				var question : Question = questionObj as Question;
				var responseList : ArrayCollection = responseDictionary[question];
				
				for each (var response:Response in responseList)
				{
					productProxy.getResponseRelationship(productOption.productId, question.questionnaire.questionnaireId, 
						question.questionId, response.responseId, productOption.referenceDate);
				}
			}

		}
		
		private function profileResponseRelationshipRefreshed( responseRelationshipList:ArrayCollection ) : void {
			
			for each (var responseRelationship : ResponseRelationship in  responseRelationshipList){
				
				if (responseRelationship.id.responseRelationshipType == ResponseRelationship.RELATIONSHIP_TYPE_DISABLE) {
					quoteForm.itemProfile.disableQuestion(responseRelationship.id.questionnaireId, responseRelationship.id.questionIdrelationship, false);
				}
				
				if (responseRelationship.id.responseRelationshipType == ResponseRelationship.RELATIONSHIP_TYPE_ENABLE) {
					quoteForm.itemProfile.disableQuestion(responseRelationship.id.questionnaireId, responseRelationship.id.questionIdrelationship, true);
				}
			}
		}
				
		private function autoOccupantNumberChanged( event:Event ) : void {
		    quoteForm.coverageListComponent.updateNumberPassenger( quoteForm.itemVehicle.stepOccupants.value );
		}
		
		private function findProductProfile(loadProfile:Boolean) : void {
			
			if (productOption.profileRequired) {
				
				if (loadProfile)
					productProxy.findProfileList(productOption.productId, productOption.referenceDate);
					
				quoteForm.enableProfile(productOption.profileRequired);
			}
		}
	}
}