////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2009 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Valéria Sousa
// @version 1.0
// @lastModified 18/12/2009
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.sales.view.mediator
{
	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.Constant;
	import br.com.tratomais.einsurance.core.Desktop;
	import br.com.tratomais.einsurance.core.Erro;
	import br.com.tratomais.einsurance.core.ErrorWindow;
	import br.com.tratomais.einsurance.core.Errors;
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.NotificationViewsList;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.customer.model.proxy.DataOptionProxy;
	import br.com.tratomais.einsurance.customer.model.vo.Address;
	import br.com.tratomais.einsurance.customer.model.vo.Customer;
	import br.com.tratomais.einsurance.customer.model.vo.DataOption;
	import br.com.tratomais.einsurance.customer.model.vo.Phone;
	import br.com.tratomais.einsurance.policy.model.vo.Beneficiary;
	import br.com.tratomais.einsurance.policy.model.vo.Endorsement;
	import br.com.tratomais.einsurance.policy.model.vo.EndorsementOption;
	import br.com.tratomais.einsurance.policy.model.vo.Item;
	import br.com.tratomais.einsurance.policy.model.vo.ItemAuto;
	import br.com.tratomais.einsurance.policy.model.vo.ItemPersonalRisk;
	import br.com.tratomais.einsurance.policy.model.vo.ItemPersonalRiskGroup;
	import br.com.tratomais.einsurance.policy.model.vo.ItemProperty;
	import br.com.tratomais.einsurance.policy.model.vo.ItemPurchaseProtected;
	import br.com.tratomais.einsurance.policy.model.vo.Motorist;
	import br.com.tratomais.einsurance.products.model.proxy.ProductProxy;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	import br.com.tratomais.einsurance.products.model.vo.PaymentOption;
	import br.com.tratomais.einsurance.products.model.vo.PersonalRiskOption;
	import br.com.tratomais.einsurance.products.model.vo.PlanPersonalRisk;
	import br.com.tratomais.einsurance.products.model.vo.ProductPlan;
	import br.com.tratomais.einsurance.report.model.proxy.ReportProxy;
	import br.com.tratomais.einsurance.report.model.vo.Report;
	import br.com.tratomais.einsurance.sales.model.proxy.ProposalProxy;
	import br.com.tratomais.einsurance.sales.model.vo.PaymentFormVO;
	import br.com.tratomais.einsurance.sales.model.vo.ProposalListData;
	import br.com.tratomais.einsurance.sales.view.components.InsuredForm;
	import br.com.tratomais.einsurance.sales.view.components.ProposalForm;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.events.MenuEvent;
	import mx.resources.ResourceManager;
	import mx.rpc.events.FaultEvent;
	import mx.utils.ObjectUtil;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator; 

	public class ProposalMediator extends Mediator implements IMediator  
	{
	    public static const NAME:String = 'ProposalMediator'; 
		private static const ID_DATA_GROUP_COLOR:int = 77; 
		private static const ID_DATA_GROUP_PROPERTY:int = 29; 
		private static const ID_DATA_GROUP_AUTO_TRANSMISSION:int = 71; 
		private static const ID_DATA_GROUP_AUTO_CLASS:int = 72; 
		private static const ID_DATA_GROUP_AUTO_GROUP:int = 74; 
		private static const ID_DATA_GROUP_AUTO_USE_TYPE:int = 75;
		private static const ID_DATA_GROUP_RENEWAL_TYPE:int = 19;  
		private static const ID_DATA_GROUP_RENEWAL_INSURER:int = 102;  
		private static const ID_DATA_GROUP_PROPERTY_RISK:int = 29;  
		private static const ID_DATA_GROUP_REGION_CODE:int = 101;  
		private static const ID_DATA_GROUP_CARD_TYPE:int = 67;  
		private static const ID_DATA_GROUP_CARD_BRAND_TYPE:int = 68;
		private static const ID_DATA_GROUP_CARD_ACTIVITY_TYPE:int = 106;    
    	private static const AUTO_IDENTIFIED_COLOR_TYPE_ID:String = "PROPOSAL_MEDIATOR.COLOR.77";   
		
		private var proposalProxy:ProposalProxy;
		private var productProxy:ProductProxy;
		private var dataOptionProxy:DataOptionProxy;
		private var reportProxy:ReportProxy;

        private var insuredFormMediator:InsuredFormMediator;
       	private var policyHolderFormMediator:PolicyHolderFormMediator;
		private var beneficiaryFormMediator:BeneficiaryFormMediator;
		private var paymentFormMediator:PaymentFormMediator;
		private var itemHabitatMediator:ItemHabitatMediator;
		private var itemCondominiumMediator:ItemCondominiumMediator;		
		private var familyGroupMediator:FamilyGroupMediator;
		private var additionalMediator:AdditionalMediator;
		private var motoristFormMediator:MotoristFormMediator;
		private var purchaseProtectedFormMediator:PurchaseProtectedFormMediator;
		private var itemCapitalMediator:ItemCapitalMediator;
		
		private var paymentFormVO:PaymentFormVO;
		
		private var desktopInstance:Desktop = ApplicationFacade.getInstance().getEinsurance.desktop;		
		
		public function ProposalMediator(viewComponent:Object)  
        {  
            super(NAME, viewComponent);
        } 
        
        public function get proposalForm():ProposalForm{  
            return viewComponent as ProposalForm;  
        }  
                
		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */		
		override public function onRegister():void 
		{
			super.onRegister();
			
			proposalProxy = ProposalProxy(facade.retrieveProxy(ProposalProxy.NAME));
			productProxy = ProductProxy(facade.retrieveProxy(ProductProxy.NAME));
			dataOptionProxy = DataOptionProxy(facade.retrieveProxy(DataOptionProxy.NAME));
			reportProxy = ReportProxy(facade.retrieveProxy(ReportProxy.NAME));
			
			proposalForm.addEventListener(ProposalForm.PROPOSAL_INITIALIZE, this.initializeProposal, false, 0, true);
			proposalForm.addEventListener(ProposalForm.PROPOSAL_ACCEPT, this.effectivePolicy, false, 0, true);
			proposalForm.addEventListener(ProposalForm.PROPOSAL_UNLOCK, this.unlockProposal, false, 0, true);
			proposalForm.addEventListener(ProposalForm.PROPOSAL_CLOSED, this.resetForm, false, 0, true);
			proposalForm.addEventListener(MenuEvent.ITEM_CLICK, this.policyPrint, false, 0, true);
			proposalForm.addEventListener(ProposalForm.REPORT_GET, this.getReportId, false, 0, true);
		}
	        		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			this.resetForm();

			ApplicationFacade.getInstance().removeMediator(insuredFormMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(policyHolderFormMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(beneficiaryFormMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(paymentFormMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(itemHabitatMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(itemCondominiumMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(familyGroupMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(additionalMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(purchaseProtectedFormMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(itemCapitalMediator.getMediatorName());
			
			proposalForm.removeEventListener(ProposalForm.PROPOSAL_INITIALIZE, this.initializeProposal);
			proposalForm.removeEventListener(ProposalForm.PROPOSAL_ACCEPT, this.effectivePolicy);
			proposalForm.removeEventListener(ProposalForm.PROPOSAL_UNLOCK, this.unlockProposal);
			proposalForm.removeEventListener(ProposalForm.PROPOSAL_CLOSED, this.resetForm);
			proposalForm.removeEventListener(MenuEvent.ITEM_CLICK, this.policyPrint);
			proposalForm.removeEventListener(ProposalForm.REPORT_GET, this.getReportId);
			proposalForm.insuredForm.removeEventListener(InsuredForm.INSURED_DATA_CHANGED, this.insuredDataChanged);
			
			proposalForm.insuranceCreditWindow.visible = false; 
			proposalForm.insuranceCreditWindow.includeInLayout = false;
			
			proposalForm.habitatWindow.visible = false;
			proposalForm.habitatWindow.includeInLayout = false;

			proposalForm.capitalWindow.visible = false;
			proposalForm.capitalWindow.includeInLayout = false;
			
			proposalForm.motoristWindow.visible = false;
			proposalForm.motoristWindow.includeInLayout = false;
			
			proposalForm.vehicleWindow.visible = false;
			proposalForm.vehicleWindow.includeInLayout = false;
			
			proposalForm.purchaseProtectedWindow.visible = false;
			proposalForm.purchaseProtectedWindow.includeInLayout = false;
			super.onRemove();
		}
		          
		/**
		 * Defines the list of Notification Interests
		 */            
        override public function listNotificationInterests():Array  
        {  
            return [NotificationViewsList.VIEW_PROPOSAL_MAIN,
            		NotificationViewsList.VIEW_PRODUCTS,
            		NotificationList.LOAD_PROPOSAL_LIST,
					NotificationList.OPEN_POLICYHOLDER_DATA,
					NotificationList.POLICY_HOLDER_INFLOW_LISTED,
					NotificationList.CLOSE_POLICYHOLDER_DATA,
					NotificationList.PAYMENT_OPTION_LISTED,
					NotificationList.PAYMENT_TYPE_LISTED,
            		NotificationList.PAYMENT_OPTION_LIST,
            		NotificationList.INSURED_INFLOW_LISTED,
            		NotificationList.PERSONAL_RISK_OPTION_LISTED,
            		NotificationList.DATA_OPTION_LOADED,
            		NotificationList.PLAN_LOADED,
            		NotificationList.PRODUCT_LIST_PLAN_KINSHIP_REFRESH_PROPOSAL,
            		NotificationList.ENDORSEMENT_SAVE_SUCCESS,
            		NotificationList.POLICY_CHANGE_SUCCESS,
					NotificationList.PROPOSAL_EFFECTIVED_ENDORSEMENT,
					NotificationList.ENDORSEMENT_UNLOCKED,
					NotificationList.CERTIFICATE_ENDORSEMENT_CANCELED,
            		NotificationList.PROPOSAL_NOT_EFFECTIVED_ENDORSEMENT,
            		NotificationList.MAX_DOCUMENTS_PER_PRODUCT,
            		NotificationList.RESIZE_PROPOSAL_INSURED_FORM,
            		NotificationList.RESIZE_PROPOSAL_POLICY_HOLDER_FORM,
            		NotificationList.VIEW_PROPOSAL_MAIN_ABANDONMENT,
            		NotificationList.RETURN_QUOTE,
            		NotificationList.DATA_OPTION_IDENT_LISTED,
            		NotificationList.OPEN_MOTORIST_DATA,
            		NotificationList.CLOSE_MOTORIST_DATA,
            		NotificationList.OPEN_CAPITAL_DATA,
            		NotificationList.CLOSE_CAPITAL_DATA,
            		NotificationList.REPORT_GET,
            		NotificationList.REPORT_LISTED,
            		NotificationList.PAYMENT_FORM_RESIZE,
            		NotificationList.PAYMENT_FORM_VALIDATE_COMPLETED]; 
        }
		
		/**
        * handle Notification 
        */           
        override public function handleNotification(notification:INotification):void  
        {  
	        switch (notification.getName())  
	        { 
	            case NotificationList.LOAD_PROPOSAL_LIST:
	            	var propsalListDataResult: ProposalListData = notification.getBody() as ProposalListData;
					this.fillDataOfProposal(propsalListDataResult); 
					break;
				case NotificationList.CUSTOMER_SAVE_SUCCESS:
					var customer:Customer = notification.getBody() as Customer;						
					break;
	            case NotificationList.OPEN_POLICYHOLDER_DATA:
					proposalForm.policyHolderForm.customer = new Customer();
					//this.adjustmentsInitialCustomer(proposalForm.policyHolderForm.customer);
	            	proposalForm.policyHolderWindow.includeInLayout=true;
					proposalForm.policyHolderWindow.visible=true;
					proposalForm.policyHolderWindow.opened = true;
					proposalForm.policyHolderForm.riskPlanId = proposalForm.insuredForm.riskPlanId;
					proposalForm.policyHolderForm.productId = proposalForm.insuredForm.productId;
					proposalForm.policyHolderForm.referenceDate = proposalForm.insuredForm.referenceDate;
					proposalForm.policyHolderForm.effectiveDate = proposalForm.insuredForm.effectiveDate;					
					proposalForm.policyHolderForm.policyHolderAge();
					proposalForm.endorsement.customerByPolicyHolderId = proposalForm.policyHolderForm.customer;					
					break;
	            case NotificationList.CLOSE_POLICYHOLDER_DATA:
					proposalForm.policyHolderForm.customer = null;
					proposalForm.endorsement.customerByPolicyHolderId = null;
	            	proposalForm.policyHolderWindow.includeInLayout=false;
					proposalForm.policyHolderWindow.visible=false;
					break;
	        	case NotificationList.PAYMENT_TYPE_LISTED:
	            	var listPaymentTypeResult: ArrayCollection = notification.getBody() as ArrayCollection;
	            	//proposalForm.paymentForm.listPaymentType = listPaymentTypeResult;
	            	break;	
	            case NotificationList.PAYMENT_OPTION_LISTED:
	            	var listPaymentOptionsResult: ArrayCollection = notification.getBody() as ArrayCollection;
	            	//paymentFormMediator.populatePaymentOption(listPaymentOptionsResult);
	            	break;
	        	case NotificationList.INSURED_INFLOW_LISTED:
	        		var inflowListResult:ArrayCollection = notification.getBody() as ArrayCollection;
	                proposalForm.insuredForm.cmbxSalary.dataProvider = inflowListResult;
	                proposalForm.insuredForm.cmbxSalary.dropdown.dataProvider = inflowListResult;
	        		break;
        		case NotificationList.POLICY_HOLDER_INFLOW_LISTED:
	        		var inflowListResult:ArrayCollection = notification.getBody() as ArrayCollection;
                	proposalForm.policyHolderForm.cmbxSalary.dataProvider = inflowListResult;
                	proposalForm.policyHolderForm.cmbxSalary.dropdown.dataProvider = inflowListResult;
	        		break;  
				case NotificationList.PERSONAL_RISK_OPTION_LISTED:
					this.populatePersonTypeCombo(notification.getBody() as ArrayCollection);
					break;
				case NotificationList.PROPOSAL_EFFECTIVED_ENDORSEMENT:
					var endorsement : Endorsement = notification.getBody() as Endorsement;
					this.managedReturnEndorsement(endorsement);
					
					var log:String;
					log = ApplicationFacade.getInstance().loggedUser.login;
					log += ":" + ApplicationFacade.getInstance().loggedUser.domain.name;
					log += ":PROPOSAL EFECTIVED";
					log += ":SAVE";
					if (endorsement && endorsement.id)
						log += ":" + endorsement.id.contractId;
					ApplicationFacade.getInstance().auditLogger = log;
					break;
				case NotificationList.PROPOSAL_NOT_EFFECTIVED_ENDORSEMENT:
					this.enabledAllForm(true);
					proposalForm.setButtonsState(ProposalForm.STATE_EFFECTIVE_BEFORE);
					if (notification.getBody() instanceof FaultEvent){
						var pResultEvent : FaultEvent = notification.getBody() as FaultEvent;
						ErrorWindow.createAndShowError(Errors.INTERNAL_ERROR, pResultEvent.fault.getStackTrace());
					} else {
						Utilities.errorMessage(Errors.INTERNAL_ERROR);
					}
				    break;
				case NotificationList.PLAN_LOADED:
					proposalForm.habitatForm.txtTypeRisk.text = (notification.getBody() as ProductPlan).nickName;
					break;
				case NotificationList.DATA_OPTION_LOADED:
					var loadedDataOption : DataOption = notification.getBody() as DataOption;
					if (loadedDataOption != null) {
				  		switch(loadedDataOption.dataGroup.dataGroupId){
				  			case ID_DATA_GROUP_PROPERTY	:
								proposalForm.habitatForm.txtTypeHousing.text = loadedDataOption.fieldDescription;
								proposalForm.capitalForm.lblCompanyType.text = loadedDataOption.fieldDescription;
				  				proposalForm.condominiumForm.loadCondominiumType(loadedDataOption.fieldDescription);
								break;
							case ID_DATA_GROUP_CARD_ACTIVITY_TYPE:
								proposalForm.capitalForm.lblCompanyActivity.text = loadedDataOption.fieldDescription;
								break;
							case ID_DATA_GROUP_AUTO_CLASS :
								proposalForm.vehicleForm.loadAutoClass(loadedDataOption.fieldDescription);
								break;	
							case ID_DATA_GROUP_AUTO_GROUP :
								proposalForm.vehicleForm.loadAutoGroup(loadedDataOption.fieldDescription);
								break;	
							case ID_DATA_GROUP_AUTO_TRANSMISSION :
								proposalForm.vehicleForm.loadAutoTransmission(loadedDataOption.fieldDescription);
								break;		
							case ID_DATA_GROUP_AUTO_USE_TYPE :
								proposalForm.vehicleForm.loadAutoUseType(loadedDataOption.fieldDescription);
								break;		
							case ID_DATA_GROUP_RENEWAL_TYPE:
				  				proposalForm.condominiumForm.loadRenewalType(loadedDataOption.fieldDescription);
				  				break;
				  			case ID_DATA_GROUP_RENEWAL_INSURER:
				  				proposalForm.condominiumForm.loadInsurer(loadedDataOption.fieldDescription);
				  				break;
				  			case ID_DATA_GROUP_REGION_CODE:
				  				proposalForm.condominiumForm.loadRegionCode(loadedDataOption.fieldDescription);
				  				break;
							case ID_DATA_GROUP_CARD_BRAND_TYPE :
								proposalForm.purchaseProtectedForm.loadCardBrandType(loadedDataOption.fieldDescription);
								break;	
							case ID_DATA_GROUP_CARD_TYPE :
								proposalForm.purchaseProtectedForm.loadCardType(loadedDataOption.fieldDescription);
								break;	
				  		}
					}
					break;
				case NotificationViewsList.VIEW_PRODUCTS:
					this.resetForm();
					break;
				case NotificationList.PRODUCT_LIST_PLAN_KINSHIP_REFRESH_PROPOSAL:
					proposalForm.proposalListData.listPlanKinship = notification.getBody() as ArrayCollection;
					break;
				case NotificationList.PAYMENT_OPTION_LIST:
					/* this.proposalProxy.listPaymentOption(	proposalForm.endorsement.netPremium, 
															proposalForm.endorsement.taxRate,
															proposalForm.endorsement.contract.productId,
															notification.getBody() as int,//billing
															proposalForm.endorsement.referenceDate,
															proposalForm.endorsement.effectiveDate,
															proposalForm.endorsement.expiryDate
															); */
					break;
				case NotificationList.ENDORSEMENT_UNLOCKED:
					this.unlockProposalResult(notification.getBody() as Boolean);
					break;
				case NotificationList.POLICY_CHANGE_SUCCESS:
					var endorsement : Endorsement = notification.getBody() as Endorsement;
					this.managedReturnEndorsement(endorsement);
					
					var log:String;
					log = ApplicationFacade.getInstance().loggedUser.login;
					log += ":" + ApplicationFacade.getInstance().loggedUser.domain.name;
					log += ":POLICY CHANGED";
					log += ":SAVE";
					if (endorsement && endorsement.id)
						log += ":" + endorsement.id.contractId;
					ApplicationFacade.getInstance().auditLogger = log;
					break;
				case NotificationList.CERTIFICATE_ENDORSEMENT_CANCELED:
					this.cancellationProposalResult(notification.getBody() as Boolean);
					break;	
				case NotificationList.RESIZE_PROPOSAL_INSURED_FORM:
						if(proposalForm.insuredForm.customer.personType == PersonalRiskOption.NATURAL_PERSON || proposalForm.insuredForm.customer.personType == 0){
							Utilities.resizeFormToNaturalPerson(true, proposalForm.insuredWindow, 480);
						}else{
							Utilities.resizeFormToNaturalPerson(false, proposalForm.insuredWindow, 400);
						}
					break;
				case NotificationList.RESIZE_PROPOSAL_POLICY_HOLDER_FORM:
						if(proposalForm.policyHolderForm.customer.personType == PersonalRiskOption.NATURAL_PERSON || proposalForm.policyHolderForm.customer.personType == 0){
							Utilities.resizeFormToNaturalPerson(true, proposalForm.policyHolderWindow, 460);
						}else{
							Utilities.resizeFormToNaturalPerson(false, proposalForm.policyHolderWindow, 380);
						}				
					break;
				case NotificationList.VIEW_PROPOSAL_MAIN_ABANDONMENT:
					break;
				case NotificationList.RETURN_QUOTE:
					var endorsementOption:EndorsementOption = notification.getBody() as EndorsementOption;
					if( proposalForm.insuredForm.yes.selected )
						endorsementOption.endorsement.customerByPolicyHolderId = null;
						
					desktopInstance.quoteMain.returnModule = proposalForm.returnModule;
					desktopInstance.quoteMain.searchDocumentParameters = proposalForm.searchDocumentParameters;
					
					ApplicationFacade.getInstance().sendNotification(NotificationViewsList.VIEW_RELOAD_QUOTE, endorsementOption);					
					break;
				case NotificationList.DATA_OPTION_IDENT_LISTED:
			  		var identifiedList:IdentifiedList = (notification.getBody() as IdentifiedList);
			  		
			  		switch(identifiedList.groupId){
			  			case AUTO_IDENTIFIED_COLOR_TYPE_ID:
							proposalForm.vehicleForm.cmbxColor.dataProvider = identifiedList.resultArray as ArrayCollection;
							break;
			  		}
			  		break;
	            case NotificationList.OPEN_MOTORIST_DATA:
	            	motoristFormMediator.loadInsuredData(proposalForm.insuredForm);				
					motoristFormMediator.disableFields();
					insuredFormMediator.loadMotoristData(proposalForm.motoristForm);
					insuredFormMediator.disableMotoristFields();
					break;
	            case NotificationList.CLOSE_MOTORIST_DATA:
	            	var mainItem : Item = proposalForm.endorsementOption.endorsement.getMainItem();
					proposalForm.motoristForm.enableFildsAllowed(mainItem.objectId, proposalForm.endorsementOption.action );
					proposalForm.insuredForm.enableFildsAllowed(mainItem.objectId, proposalForm.endorsementOption.action );
					motoristFormMediator.cleanInsuredFields();
					insuredFormMediator.cleanMotoristFields();
					break;
				case NotificationList.OPEN_CAPITAL_DATA:
					itemCapitalMediator.loadInsuredData(proposalForm.insuredForm);
					proposalForm.capitalForm.validatorRegister(false);
					itemCapitalMediator.disableFields();
					break;
				case NotificationList.CLOSE_CAPITAL_DATA:
					var mainItem : Item = proposalForm.endorsementOption.endorsement.getMainItem();
					proposalForm.capitalForm.validatorRegister();
					proposalForm.capitalForm.enableFildsAllowed(mainItem.objectId, proposalForm.endorsementOption.action );
					break;
				case NotificationList.REPORT_GET:
                	var reportId:int = notification.getBody() as int;
					proposalForm.print(reportId.toString());
					break;
				case NotificationList.REPORT_LISTED:
                	var reportListResult:ArrayCollection = notification.getBody() as ArrayCollection;
					proposalForm.printMenuCollection = reportListResult;
					break;

				case NotificationList.PAYMENT_FORM_RESIZE:
					proposalForm.paymentWindow.newHeight = notification.getBody() as int;
					proposalForm.paymentWindow.resize();
					break;
					
				case NotificationList.PAYMENT_FORM_VALIDATE_COMPLETED:
					this.notificationPaymentValidateCompleted(notification.getBody() as Boolean);
					break;
			 }
    	}
    	
		private function notificationPaymentValidateCompleted(isValid:Boolean):void{
			if(isValid){
	 			var paymentOptionSelected : PaymentOption = paymentFormVO.paymentOption;
				 				
				proposalForm.endorsement.customerByInsuredId = proposalForm.insuredForm.customer;

				if(proposalForm.policyHolderForm.customer == null){
					proposalForm.endorsement.customerByPolicyHolderId = proposalForm.endorsement.customerByInsuredId;
				}else{
					proposalForm.endorsement.customerByPolicyHolderId = proposalForm.policyHolderForm.customer;
				}

				switch(proposalForm.endorsement.getMainItem().objectId){
					case Item.OBJECT_HABITAT:
					case Item.OBJECT_CAPITAL:
					case Item.OBJECT_CONDOMINIUM:
						var itemProperty : ItemProperty = proposalForm.endorsement.getMainItem() as ItemProperty;
						if(itemProperty.singleAddress == true){
							this.riskAddressEqualInsured(proposalForm.endorsement.customerByInsuredId);
						}	
						break;
				}

				var item:Item = proposalForm.endorsement.getMainItem();
				item.beneficiaries = new ArrayCollection();
				for each(var beneficiary : Beneficiary in proposalForm.beneficiaryForm.beneficiaries) {
					item.beneficiaries.addItem(beneficiary);
					beneficiary.item = item;
				}
 				
 				this.adjustmentsEndorsement(proposalForm.endorsement);
 				
 				proposalForm.setButtonsState(ProposalForm.STATE_EFFECTIVE_DISABLE);

				if(proposalForm.endorsementOption.action == EndorsementOption.ACTION_CHANGE_REGISTER){
				 	proposalProxy.policyChangeRegister(proposalForm.endorsement, new Date());
			 	} else if(proposalForm.endorsementOption.action == EndorsementOption.ACTION_CHANGE_TECHNICAL) {
			 		proposalProxy.policyChangeTechnical(proposalForm.endorsement, new Date(), false, true, paymentOptionSelected);
				} else {
					proposalProxy.effectiveProposal(proposalForm.endorsement, paymentOptionSelected);
				}
			}
		}
		
    	private function insuredDataChanged(event:Event):void{
	    	motoristFormMediator.loadInsuredData(proposalForm.insuredForm); 
	    	purchaseProtectedFormMediator.loadInsuredData(proposalForm.insuredForm);
	    	itemCapitalMediator.loadInsuredData(proposalForm.insuredForm);
    	}
    	
    	/**
    	 * Lista os dados necessários para inicializar a proposta
    	 * **/
    	private function initializeProposal(event:Event):void{
			productProxy.loadProposalList();
		}
	
		/**
		 * Preenche os campos com os dados de inicialização da proposta
		 * **/
    	public function fillDataOfProposal(propsalListDataResult: ProposalListData) : void{
    		
			insuredFormMediator = new InsuredFormMediator(proposalForm.insuredForm); 
			policyHolderFormMediator = new PolicyHolderFormMediator(proposalForm.policyHolderForm);
			beneficiaryFormMediator = new BeneficiaryFormMediator(proposalForm.beneficiaryForm);
			paymentFormMediator = new PaymentFormMediator(proposalForm.paymentForm);
			itemHabitatMediator = new ItemHabitatMediator(proposalForm.habitatForm);
			itemCondominiumMediator = new ItemCondominiumMediator(proposalForm.condominiumForm);
			familyGroupMediator = new FamilyGroupMediator(proposalForm.familyGroupForm);
			additionalMediator = new AdditionalMediator(proposalForm.additionalForm);
			motoristFormMediator = new MotoristFormMediator(proposalForm.motoristForm);
			purchaseProtectedFormMediator = new PurchaseProtectedFormMediator(proposalForm.purchaseProtectedForm);
			itemCapitalMediator = new ItemCapitalMediator(proposalForm.capitalForm);
			
			ApplicationFacade.getInstance().registerOnlyNewMediator(insuredFormMediator);
			ApplicationFacade.getInstance().registerOnlyNewMediator(policyHolderFormMediator);
			ApplicationFacade.getInstance().registerOnlyNewMediator(beneficiaryFormMediator);
			ApplicationFacade.getInstance().registerOnlyNewMediator(paymentFormMediator);
			ApplicationFacade.getInstance().registerOnlyNewMediator(itemHabitatMediator);
			ApplicationFacade.getInstance().registerOnlyNewMediator(familyGroupMediator);
			ApplicationFacade.getInstance().registerOnlyNewMediator(additionalMediator);
			ApplicationFacade.getInstance().registerOnlyNewMediator(motoristFormMediator);
			ApplicationFacade.getInstance().registerOnlyNewMediator(itemCondominiumMediator);
			ApplicationFacade.getInstance().registerOnlyNewMediator(purchaseProtectedFormMediator);
			ApplicationFacade.getInstance().registerOnlyNewMediator(itemCapitalMediator);

			proposalForm.insuredForm.addEventListener(InsuredForm.INSURED_DATA_CHANGED, this.insuredDataChanged, false, 0, true);
	       	proposalForm.proposalListData = propsalListDataResult;
	       	proposalForm.onCreationComplete();
				
			// Item is life
			//according to the product and risk plan, bring the valid types of insured.
			proposalProxy.findPersonalRiskOptions(	proposalForm.endorsement.contract.productId,
												 	proposalForm.endorsement.riskPlanId,
												 	proposalForm.endorsement.referenceDate);

	       	/* proposalProxy.listPaymentType(		proposalForm.endorsement.contract.productId, 
	       										proposalForm.endorsement.referenceDate); */

			this.loadInfoQuotation(proposalForm.endorsementOption);
			this.dispatchPaymentParameters();
			
    	}

		private function dispatchPaymentParameters():void{
			//ApplicationFacade.getInstance().registerMediator(new PaymentFormMediator(proposalForm.paymentForm));
			
			paymentFormVO = new PaymentFormVO;
			paymentFormVO.endorsement = proposalForm.endorsementOption.endorsement;
			//paymentFormVO.actionType = actionType;
			
			sendNotification(NotificationList.PAYMENT_FORM_INITIALIZE_PARAMETERS, paymentFormVO);
		}
		
    	/**
    	 * Preenche objetos dos componentes dependentes necessários para inicialização
    	 * **/
    	private function loadInfoQuotation(endorsementOption:EndorsementOption):void{

			this.resetForm();
			this.initializeLayout();

			this.setAction(endorsementOption.action);

			var endorsement : Endorsement = endorsementOption.endorsement;

			var item:Item = endorsement.getMainItem();
			
			proposalForm.dataInicio = endorsement.effectiveDate;
			proposalForm.dataFinal = endorsement.expiryDate;
			proposalForm.productId = endorsement.contract.productId;
			proposalForm.contractId = endorsement.contract.contractId;
			proposalForm.maxContracts = proposalForm.endorsementOption.productOption.maxOccurrences;
			proposalForm.nomeProduto = proposalForm.endorsementOption.productOption.nickName;
			proposalForm.referenceDate = proposalForm.endorsement.referenceDate;
			
			if(item != null){
				item.beneficiaries = Utilities.initializeList(item.beneficiaries);
				item.itemClauses = Utilities.initializeList(item.itemClauses);
				item.itemProfiles = Utilities.initializeList(item.itemProfiles);
				endorsement.installments = Utilities.initializeList(endorsement.installments);
	
				if( endorsement.customerByInsuredId == null) {
					endorsement.customerByInsuredId = endorsement.addCustomer();
				}
		
				proposalForm.insuredForm.customer =  endorsement.customerByInsuredId;
				
				/* proposalForm.paymentForm.cardHolderName.text = (endorsement.customerByInsuredId.firstName + " " +
														  	   endorsement.customerByInsuredId.lastName).substring(0, 30); */
				
				// insured form and plan kinship
				proposalForm.insuredForm.productId = endorsement.contract.productId;
				proposalForm.insuredForm.riskPlanId = endorsement.riskPlanId;
				proposalForm.insuredForm.referenceDate = endorsement.referenceDate;	
				proposalForm.insuredForm.effectiveDate = endorsement.effectiveDate;
				proposalForm.insuredForm.insuranceAge();
				
				//adjust insuranceForm to type person
				var isNaturalPerson:Boolean = endorsement.customerByInsuredId.personType == PersonalRiskOption.LEGALLY_PERSON? false : true;
				proposalForm.insuredForm.enabledNaturalFields(isNaturalPerson);
				proposalForm.insuredForm.validatorRegister(!isNaturalPerson, isNaturalPerson);
				
				if(isNaturalPerson){
					Utilities.resizeFormToNaturalPerson(isNaturalPerson, proposalForm.insuredWindow, 480);
				} else{
					Utilities.resizeFormToNaturalPerson(isNaturalPerson, proposalForm.insuredWindow, 400);
				}
				
				if( endorsement.customerByPolicyHolderId == null) {
					endorsement.customerByPolicyHolderId = endorsement.customerByInsuredId;
					proposalForm.insuredForm.yes.selected = true;
					proposalForm.insuredForm.no.selected = false;
				}else if(endorsement.customerByInsuredId.customerId != endorsement.customerByPolicyHolderId.customerId){
					proposalForm.policyHolderWindow.visible = true;
					proposalForm.policyHolderWindow.includeInLayout = true;
					proposalForm.policyHolderForm.productId = endorsement.contract.productId;
					proposalForm.policyHolderForm.riskPlanId = endorsement.riskPlanId;
					proposalForm.policyHolderForm.referenceDate = endorsement.referenceDate;
					proposalForm.policyHolderForm.effectiveDate = endorsement.effectiveDate;
					proposalForm.policyHolderForm.policyHolderAge();
					proposalForm.insuredForm.no.selected = true;
					proposalForm.insuredForm.yes.selected = false;
					proposalForm.policyHolderForm.customer = endorsement.customerByPolicyHolderId;
				}

				//proposalForm.paymentForm.endorsement = endorsement;
				proposalForm.beneficiaryForm.item = item;
				proposalForm.resumeProposal.endorsement = endorsementOption;	
								
				/**
				 * Adjustments the initialize Endorsement
				 * 
				 * 
				 * **/
				//adjustmentsInitialEndorsement(endorsement);
				
				proposalForm.insuredForm.insuredID = endorsement.customerByInsuredId.customerId;
				proposalForm.insuredForm.policyHolderID = endorsement.customerByPolicyHolderId.customerId;
				proposalForm.insuredForm.enableFildsAllowed( item.objectId, endorsementOption.action );
				
 	 			proposalForm.policyHolderForm.enableFildsAllowed( item.objectId, endorsementOption.action );
 	 			proposalForm.beneficiaryForm.enableFildsAllowed( item.objectId, endorsementOption.action );
 	 			//proposalForm.paymentForm.enableFildsAllowed( item.objectId, endorsementOption.action );
 	 			 	 			
 	 			if ( endorsementOption.action == EndorsementOption.ACTION_CHANGE_REGISTER ) {
 	 				proposalForm.paymentWindow.visible = false;
 	 				proposalForm.resumeProposal.frmTotalPremium.visible = false;
 	 			}
 	 			else if (( endorsementOption.action == EndorsementOption.ACTION_CHANGE_TECHNICAL ) &&
 	 			    	 ( endorsementOption.endorsement.endorsementType != Endorsement.ENDORSEMENT_TYPE_COLLECTION)) {
 	 				proposalForm.paymentWindow.visible = false;
 	 			}
 	 			else {
 	 				proposalForm.paymentWindow.visible = true;
 	 				proposalForm.resumeProposal.frmTotalPremium.visible = true;
 	 			}
 	 			
				switch(item.objectId){
					case Item.OBJECT_PERSONAL_ACCIDENT: 
		 				var itemPersonalRisk : ItemPersonalRisk = item as ItemPersonalRisk;
						proposalForm.resumeProposal.setCoverages(false, itemPersonalRisk.itemCoverages);
						break;
		 			case Item.OBJECT_LIFE: 
		 				var itemPersonalRisk : ItemPersonalRisk = item as ItemPersonalRisk;
						proposalForm.resumeProposal.setCoverages(false, itemPersonalRisk.itemCoverages);
						break;
		 			case Item.OBJECT_CREDIT_INSURANCE: 
	 	 				var itemPersonalRisk : ItemPersonalRisk = item as ItemPersonalRisk;
						proposalForm.resumeProposal.setCoverages(false, itemPersonalRisk.itemCoverages);
						proposalForm.insuranceCreditForm.itemPersonalRisk = itemPersonalRisk;
			      		proposalForm.insuranceCreditWindow.visible = true; 
			          	proposalForm.insuranceCreditWindow.includeInLayout=true;
 	 					proposalForm.insuranceCreditForm.enableFildsAllowed( Item.OBJECT_CREDIT_INSURANCE, endorsementOption.action );
						break;
	 	 			case Item.OBJECT_HABITAT: 
	 	 				var itemProperty : ItemProperty = item as ItemProperty;
 	 					proposalForm.habitatForm.enableFildsAllowed( Item.OBJECT_HABITAT, endorsementOption.action );
						this.loadInfoQuotationObjectHabitat(itemProperty)
						break;
					case Item.OBJECT_LIFE_CYCLE:
	 	 				var itemPersonalRisk : ItemPersonalRisk = item as ItemPersonalRisk;
 	 					proposalForm.familyGroupForm.enableFildsAllowed( Item.OBJECT_LIFE_CYCLE, endorsementOption.action );
 	 					proposalForm.additionalForm.enableFildsAllowed( Item.OBJECT_LIFE_CYCLE, endorsementOption.action );
						this.loadInfoQuotationObjectLifeCycle(endorsement, itemPersonalRisk, endorsementOption.planPersonalRisk)
						break;
					case Item.OBJECT_AUTOMOVIL:
	 	 				var itemAuto : ItemAuto = item as ItemAuto;
 	 					proposalForm.vehicleForm.enableFildsAllowed( Item.OBJECT_AUTOMOVIL, endorsementOption.action );
 	 					proposalForm.motoristForm.enableFildsAllowed( Item.OBJECT_AUTOMOVIL, endorsementOption.action );
						proposalForm.resumeProposal.setCoverages(false, itemAuto.itemCoverages);
	 	 				this.loadInfoQuotationObjectVehicle(endorsement, itemAuto);
						break;
					case Item.OBJECT_CONDOMINIUM:
	 	 				var itemProperty : ItemProperty = item as ItemProperty;
 	 					proposalForm.condominiumForm.enableFildsAllowed( Item.OBJECT_CONDOMINIUM, endorsementOption.action );
						this.loadInfoQuotationObjectCondominium(itemProperty);
						break;
					case Item.OBJECT_CAPITAL:
						var itemProperty : ItemProperty = item as ItemProperty;
						this.loadInfoQuotationObjectCapital( itemProperty );
						break;
					case Item.OBJECT_PURCHASE_PROTECTED:
	 	 				var itemPurchaseProtected : ItemPurchaseProtected = item as ItemPurchaseProtected;
	 	 				proposalForm.purchaseProtectedForm.enableFildsAllowed( Item.OBJECT_PURCHASE_PROTECTED, endorsementOption.action );
						proposalForm.resumeProposal.setCoverages(false, itemPurchaseProtected.itemCoverages);
	 	 				this.loadInfoQuotationObjectPurchaseProtected(endorsement, itemPurchaseProtected);
						break;
				}
	
			}else{
				trace("Item not found");
			}
		} 
		
		private function loadInfoQuotationObjectCondominium(itemProperty : ItemProperty) : void{
 			proposalForm.resumeProposal.setCoverages(true, itemProperty.coverageOptions);	
 			proposalForm.condominiumWindow.visible = true;
 			proposalForm.condominiumWindow.includeInLayout = true;
 			proposalForm.condominiumForm.itemProperty = itemProperty;
 			
			dataOptionProxy.getDataOptionById(itemProperty.renewalType);
			//dataOptionProxy.getDataOptionById(parseInt(itemProperty.renewalInsurerId));
			dataOptionProxy.getDataOptionById(itemProperty.propertyType);
			dataOptionProxy.getDataOptionById(itemProperty.regionCode);
		}
		
		private function loadInfoQuotationObjectHabitat(itemProperty : ItemProperty) : void{
 			proposalForm.resumeProposal.setCoverages(true, itemProperty.coverageOptions);
			dataOptionProxy.getDataOptionById(itemProperty.propertyType);
			productProxy.getRiskPlanById(itemProperty.endorsement.contract.productId, itemProperty.endorsement.riskPlanId, itemProperty.endorsement.referenceDate);			
			proposalForm.habitatForm.itemProperty = itemProperty;
			proposalForm.habitatWindow.visible = true;
			proposalForm.habitatWindow.includeInLayout = true;
			proposalForm.habitatForm.singleAddress(itemProperty.singleAddress);			
		}
		
		private function loadInfoQuotationObjectCapital(itemProperty : ItemProperty) : void{
 			proposalForm.resumeProposal.setCoverages(true, itemProperty.coverageOptions);
 			proposalForm.capitalForm.onClean();
			proposalForm.capitalForm.itemProperty = itemProperty;
			proposalForm.capitalWindow.visible = true;
			proposalForm.capitalWindow.includeInLayout = true;
			proposalForm.capitalForm.singleAddress(itemProperty.singleAddress);
			dataOptionProxy.getDataOptionById(itemProperty.propertyType);
			dataOptionProxy.getDataOptionById(itemProperty.activityType);
		}
		
		private function loadInfoQuotationObjectLifeCycle(endorsement : Endorsement, itemPersonalRisk : ItemPersonalRisk, planPersonalRisk:PlanPersonalRisk) : void{
			proposalForm.resumeProposal.setCoverages(false, itemPersonalRisk.itemCoverages);
			itemPersonalRisk.itemPersonalRiskGroups = Utilities.initializeList(itemPersonalRisk.itemPersonalRiskGroups);
			productProxy.listPlanKinshipByRiskPlan(endorsement.contract.productId, endorsement.riskPlanId, itemPersonalRisk.renewalType, endorsement.referenceDate, productProxy.onListPlanKinshipByRiskPlanProposal);

			if(planPersonalRisk.quantityPersonalRisk > 1){
				proposalForm.familyGroupWindow.visible = true; 
	      		proposalForm.familyGroupWindow.includeInLayout=true;
			}else{
				proposalForm.familyGroupWindow.visible = false; 
	      		proposalForm.familyGroupWindow.includeInLayout=false;
			}          	
          	
			proposalForm.familyGroupForm.listDocumentTypeNatural = proposalForm.proposalListData.listDocumentTypeNatural;
			proposalForm.familyGroupForm.listMaritalStatus = proposalForm.proposalListData.listMaritalStatus;
			proposalForm.familyGroupForm.listGender = proposalForm.proposalListData.listGender;
			proposalForm.familyGroupForm.proposalListData = proposalForm.proposalListData;
			proposalForm.familyGroupForm.itemPersonalRisk = itemPersonalRisk;
			proposalForm.familyGroupForm.effectiveDate = endorsement.effectiveDate;
			
			var enableAdditionaWindow: Boolean = (endorsement.listNotHolder().length > 0);
			proposalForm.additionalWindow.visible = enableAdditionaWindow;
         	proposalForm.additionalWindow.includeInLayout= enableAdditionaWindow;
			
			proposalForm.additionalForm.listDocumentTypeNatural = proposalForm.proposalListData.listDocumentTypeNatural;
			proposalForm.additionalForm.listMaritalStatus = proposalForm.proposalListData.listMaritalStatus;
			proposalForm.additionalForm.listGender = proposalForm.proposalListData.listGender;
			proposalForm.additionalForm.proposalListData = proposalForm.proposalListData;
			proposalForm.additionalForm.additionalItens = endorsement.listNotHolder();
			proposalForm.additionalForm.effectiveDate = endorsement.effectiveDate;
		}
		
		private function loadInfoQuotationObjectPurchaseProtected(endorsement : Endorsement, itemPurchaseProtected : ItemPurchaseProtected) : void {
			proposalForm.purchaseProtectedForm.endorsement = endorsement;
			proposalForm.purchaseProtectedForm.itemPurchaseProtected = itemPurchaseProtected;
			proposalForm.purchaseProtectedWindow.visible = true;
			proposalForm.purchaseProtectedWindow.includeInLayout = true;
	    	purchaseProtectedFormMediator.loadInsuredData(proposalForm.insuredForm);
			Utilities.removeValidationError(proposalForm.purchaseProtectedForm.frmDueDate);
			Utilities.removeValidationError(proposalForm.purchaseProtectedForm.frmCardNumber);
			Utilities.removeValidationError(proposalForm.purchaseProtectedForm.frmCardHolderName);
				
			dataOptionProxy.getDataOptionById(itemPurchaseProtected.cardType);
			dataOptionProxy.getDataOptionById(itemPurchaseProtected.cardBrandType);
		}
		
		private function loadInfoQuotationObjectVehicle(endorsement : Endorsement, itemAuto : ItemAuto) : void {
			var mainMotorist : Motorist = getMainMotorist(itemAuto);
			
			proposalForm.vehicleForm.endorsement = endorsement;
			proposalForm.vehicleForm.itemAuto = itemAuto;
			proposalForm.vehicleWindow.visible = true;
			proposalForm.vehicleWindow.includeInLayout = true;	
			proposalForm.motoristWindow.visible = true;
			proposalForm.motoristWindow.includeInLayout = true;
			proposalForm.motoristForm.motorist = mainMotorist;	
			proposalForm.motoristForm.listDocumentTypeNatural = proposalForm.proposalListData.listDocumentTypeNatural;
			proposalForm.motoristForm.listMaritalStatus = proposalForm.proposalListData.listMaritalStatus;
			proposalForm.motoristForm.listGender = proposalForm.proposalListData.listGender;
			Utilities.removeValidationError(proposalForm.motoristForm.dfDatebirth);
				
			dataOptionProxy.listIdentifiedDataOption(ID_DATA_GROUP_COLOR,AUTO_IDENTIFIED_COLOR_TYPE_ID);
			//dataOptionProxy.getDataOptionById(itemAuto.autoGroupType);
			dataOptionProxy.getDataOptionById(itemAuto.autoClassType);
			dataOptionProxy.getDataOptionById(itemAuto.autoUseType);
			//dataOptionProxy.getDataOptionById(itemAuto.autoTransmissionType);
			
			proposalForm.vehicleForm.enableZeroKmVehicle(itemAuto.zeroKm);
			
			if (itemAuto.tonneQuantity > 0) {
				proposalForm.vehicleForm.enableTonneQuantity();
			}
		}
		
		private function getMainMotorist(itemAuto : ItemAuto) : Motorist{
			var mainMotorist : Motorist = null;
			
			for (var i : int = 0;  i < itemAuto.motorists.length; i++) {
				var currMotorist : Motorist = itemAuto.motorists.getItemAt(i) as Motorist;
				
				if (Motorist.MOTORIST_TYPE_MAIN == currMotorist.motoristType) {
					mainMotorist = currMotorist;
					break;
				}
			}
			
			return mainMotorist;
		}
		
		private function populatePersonTypeCombo(personalRiskOptionList : ArrayCollection):void{
			proposalForm.insuredForm.listPersonType = personalRiskOptionList;
			proposalForm.policyHolderForm.listPersonType = proposalForm.proposalListData.listPersonType;
			proposalForm.beneficiaryForm.cmbxPersonType.dataProvider = proposalForm.proposalListData.listPersonType;
			proposalForm.beneficiaryForm.cmbxPersonType.dropdown.dataProvider = proposalForm.proposalListData.listPersonType;
			
			if(proposalForm.endorsement.getMainItem().objectId == Item.OBJECT_LIFE_CYCLE){
				proposalForm.additionalForm.listPersonRisk = personalRiskOptionList;
				proposalForm.familyGroupForm.listPersonRisk = personalRiskOptionList;
			} 
		}
		
		
		/**
		 * Efetiva a proposta
		 * **/
		private function effectivePolicy(event : Event):void{
			
			proposalForm.insuredForm.createCustomer();
			proposalForm.policyHolderForm.createCustomerPH();
			
			var item:Item = proposalForm.endorsement.getMainItem();

			if(this.validationForm(item.objectId) == true){
				if (!(( proposalForm.endorsementOption.action == EndorsementOption.ACTION_CHANGE_REGISTER) ||
			     (( proposalForm.endorsementOption.action == EndorsementOption.ACTION_CHANGE_TECHNICAL ) &&
 	 			  ( proposalForm.endorsementOption.endorsement.endorsementType != Endorsement.ENDORSEMENT_TYPE_COLLECTION)))) {
		 		
			 		sendNotification(NotificationList.PAYMENT_FORM_VALIDATE);
				}
  			}
		}
		
		/**
		 * Gerencia o resultado da solicitação de efetivação da proposta
		 * **/
    	private function managedReturnEndorsement(endorsement : Endorsement) : void{
   			if ((endorsement != null) && 
   				((endorsement.issuingStatus == Endorsement.ISSUING_STATUS_PROPOSTA ) ||	
   				 (endorsement.issuingStatus == Endorsement.ISSUING_STATUS_EMITIDA )) &&
   				(Utilities.validateEndorsementReturn(endorsement.errorList) == true )){
   				this.enabledAllForm(false);
				proposalForm.endorsementOption.endorsement = endorsement;
				proposalForm.endorsementOptionOld = ObjectUtil.copy(proposalForm.endorsementOption) as EndorsementOption;
				if(proposalForm.endorsementOption.productOption.beneficiaryRequired){
					if(proposalForm.beneficiaryForm.beneficiaries.length == 0){
						Utilities.infoMessage("noBeneficiaryAssigned");
					}	
				}
				switch( proposalForm.endorsementOption.action ){
					case EndorsementOption.ACTION_UNLOCK:
	    				proposalForm.setButtonsState(ProposalForm.STATE_UNLOCK_AFTER_EFFECTIVE);
	            		Utilities.successMessage("effectivedProposal");
	    				break;
	    			case EndorsementOption.ACTION_CHANGE_REGISTER:
	    				proposalForm.setButtonsState( ProposalForm.STATE_CHANGE_AFTER );
	    				Utilities.successMessage("policyChangeRegister");
	    				break;
	    			case EndorsementOption.ACTION_CHANGE_TECHNICAL:
	    				proposalForm.setButtonsState( ProposalForm.STATE_EFFECTIVE_AFTER );
	    				Utilities.successMessage("policyChangeTechnical");
	    				break;
	    			default:
	    				proposalForm.setButtonsState(ProposalForm.STATE_EFFECTIVE_AFTER);
	    				Utilities.successMessage("effectivedProposal");
				}
				// set list de Reportes
				this.listReportId();
   			}else{
   				this.enabledAllForm(true);
				proposalForm.setButtonsState(ProposalForm.STATE_EFFECTIVE_BEFORE);
	   			var minhaMsg: String = new String();
	   			var minhaListErros: String = new String();
	   			var aux: int = 0, numErro: int = endorsement.errorList.errorCount;
	   			var erro: Erro = null;
	   			for (aux=0;aux<numErro; aux++){
	   				erro = endorsement.errorList.StringError(aux);
	   				if ( erro.descricao != "" )
	   					minhaMsg += ((minhaMsg.length > 0 )?"\n":"") + erro.descricao;
	   			}
	   			if (minhaMsg == "" )
		            Utilities.errorMsg(Errors.INTERNAL_ERROR);
		        else
		        	Utilities.directWarningMessage(minhaMsg);
   			}
    	}
    	    	
    	/**
    	 * Set action
    	 * */
		private function setAction(action : int) : void{
			if(action == EndorsementOption.ACTION_EFFECTIVE || action == EndorsementOption.ACTION_CHANGE_TECHNICAL){
				proposalForm.setButtonsState(ProposalForm.STATE_EFFECTIVE_BEFORE);
				this.enabledAllForm(true);
	    	}else if (action == EndorsementOption.ACTION_UNLOCK){
				proposalForm.setButtonsState(ProposalForm.STATE_UNLOCK_BEFORE);
				this.enabledAllForm(false);
	    	}else if (action == EndorsementOption.ACTION_ANULLATION){
				proposalForm.setButtonsState(ProposalForm.STATE_CANCELLATION_BEFORE);
				this.enabledAllForm(false);
	    	}else if (action == EndorsementOption.ACTION_CHANGE_REGISTER){
	    		proposalForm.setButtonsState(ProposalForm.STATE_CHANGE_BEFORE)
	    	}
		}

		/**
		 * 
		 * */
		 private function unlockProposal(event : Event) : void{
		 	if(proposalForm.endorsementOption.action == EndorsementOption.ACTION_UNLOCK){
			 	proposalProxy.unlockEndorsement(proposalForm.endorsement.id.endorsementId, proposalForm.endorsement.id.contractId);	
		 	}
		 }
		 
		 /**
		 * Return of unlock
		 * 
		 * **/
		 private function unlockProposalResult(isUnlocked : Boolean) : void{
		 	if(isUnlocked){
		 		this.enabledAllForm(true);
				proposalForm.endorsement.proposalDate = null;
				proposalForm.endorsement.issuingStatus = Endorsement.ISSUING_STATUS_COTACAO;
				proposalForm.setButtonsState(ProposalForm.STATE_UNLOCK_AFTER);
		 	}else{
		 		this.enabledAllForm(false);
				proposalForm.setButtonsState(ProposalForm.STATE_UNLOCK_BEFORE);
				Utilities.errorMessage("billingFail");
		 	}
		 }
		 
		 /**
		 * Return of unlock
		 * 
		 * **/
		 private function policyRegistralResult( endorsement:Endorsement ) : void{
		 	if( endorsement )
		 	{
				proposalForm.setButtonsState( ProposalForm.STATE_CHANGE_AFTER );
				Utilities.successMessage("effectivedPolicy");
		 	}else
		 	{
				proposalForm.setButtonsState(ProposalForm.STATE_CHANGE_BEFORE);
				Utilities.errorMessage("billingFail");
		 	}
		 	this.enabledAllForm(false);
		 }		 
		  /**
		 * Return of unlock
		 * 
		 * **/
		 private function cancellationProposalResult(isCancelled : Boolean) : void{
		 	if(isCancelled){
				proposalForm.justificationForm.close();
				proposalForm.annulationMessage();
			}else{
				proposalForm.justificationForm.close();
				Utilities.errorMessage("billingFail");
		 	}
		 }

		/**
		 * Valida os forms e dependentes
		 * **/
		private function validationForm(objectId : int):Boolean{
		
 		 	// insuredForm
			var validationInsured : Boolean = Utilities.validateForm(proposalForm.insuredForm.validatorInsured, ResourceManager.getInstance().getString('resources', 'insured'));
			if(validationInsured == false){
				proposalForm.insuredWindow.opened = true;
				return false;
			}else{
				if(Utilities.validateRangeDate(proposalForm.insuredForm.dfDatebirth.newDate) == false 
					&& (((proposalForm.insuredForm.cmbxPersonType.selectedItem) as PersonalRiskOption).personType == PersonalRiskOption.NATURAL_PERSON)){
					proposalForm.insuredWindow.opened = true;
					Utilities.warningMessage('pleaseSelectDate');
					return false;
				}
				if(((proposalForm.insuredForm.cmbxPersonType.selectedItem) as PersonalRiskOption).personType == PersonalRiskOption.NATURAL_PERSON){
					if(proposalForm.insuredForm.validateInsuranceAgeByDate() == false){
						proposalForm.insuredWindow.opened = true;
						return false;
					}
				}
			}
			// policyholdersType
			if(proposalForm.insuredForm.policyholdersType.selectedValue==0){
				var validationPolicy : Boolean = Utilities.validateForm(proposalForm.policyHolderForm.validatorPolicyHolder, ResourceManager.getInstance().getString('resources', 'policyHolders'));
				if(validationPolicy == false){
					proposalForm.policyHolderWindow.opened = true;
					return false;
				}
				if(proposalForm.policyHolderForm.txtDocumentNumber.text == proposalForm.insuredForm.txtDocumentNumber.text){
					Utilities.warningMessage("sameDocumentNumber");
					proposalForm.policyHolderWindow.opened = true;
					return false;
				}		
				if((Utilities.validateRangeDate(proposalForm.policyHolderForm.dfDatebirth.newDate) == false) 
					&& (((proposalForm.policyHolderForm.cmbxPersonType.selectedItem) as PersonalRiskOption).personType == PersonalRiskOption.NATURAL_PERSON)){
					proposalForm.policyHolderWindow.opened = true;
					Utilities.warningMessage('pleaseSelectDate');
					return false;
				}
				if(((proposalForm.policyHolderForm.cmbxPersonType.selectedItem) as PersonalRiskOption).personType == PersonalRiskOption.NATURAL_PERSON){
					if(proposalForm.policyHolderForm.validatePolicyHolderAgeByDate() == false){
						proposalForm.policyHolderWindow.opened = true;
						return false;
					}
				}
			}

			// beneficiaryForm
			var beneficiaries : ArrayCollection = proposalForm.beneficiaryForm.beneficiaries as ArrayCollection;
			
			if((beneficiaries != null) && (beneficiaries.length > 0)){
				var participationPercentageTotal = 0;
				for each(var benef : Beneficiary in beneficiaries){
					participationPercentageTotal += benef.participationPercentage;
				}
				if(participationPercentageTotal > 100){
					Utilities.warningMessage("maximumBeneficiaryPercentage");
					proposalForm.beneficiaryWindow.opened = true;
					return false;
				}else if (participationPercentageTotal < 100){
					Utilities.warningMessage("minimumBeneficiaryPercentage");
					proposalForm.beneficiaryWindow.opened = true;
					return false;
				}
			}
			
			// auto
			if(objectId == Item.OBJECT_AUTOMOVIL){
				var validationMotorist : Boolean = Utilities.validateForm(proposalForm.motoristForm.validatorMotorist, ResourceManager.getInstance().getString('resources', 'driver'));
				
				if(validationMotorist == false){
					proposalForm.motoristWindow.opened = true;
					return false;
				}
				
				var validationAuto : Boolean = Utilities.validateForm(proposalForm.vehicleForm.validatorVehicle, ResourceManager.getInstance().getString('resources', 'riskData'));
				
				if(validationAuto == false){
					proposalForm.vehicleWindow.opened = true;
					return false;
				}
			}	
			
			// habitat
			if(objectId == Item.OBJECT_HABITAT){
				var validationHabitat : Boolean = Utilities.validateForm(proposalForm.habitatForm.validatorForm, ResourceManager.getInstance().getString('resources', 'riskData'));
				if(validationHabitat == false){
					proposalForm.habitatWindow.opened = true;
					return false;
				}
				if(Utilities.validateRangeDate(proposalForm.habitatForm.acquisitionDate.newDate) == false && proposalForm.habitatForm.acquisitionDate.visible){
					proposalForm.habitatWindow.opened = true;
					Utilities.warningMessage('pleaseSelectDate');
					return false;
				}
				if(proposalForm.habitatForm.registrationDate.newDate != null && Utilities.validateRangeDate(proposalForm.habitatForm.registrationDate.newDate) == false){
					proposalForm.habitatWindow.opened = true;
					Utilities.warningMessage('pleaseSelectDate');
					return false;
				}
			}	
			
			
			// habitat
			if(objectId == Item.OBJECT_CONDOMINIUM){
				var validationCondominium : Boolean = Utilities.validateForm(proposalForm.condominiumForm.validatorForm, ResourceManager.getInstance().getString('resources', 'riskData'));
				if(validationCondominium == false){
					proposalForm.condominiumWindow.opened = true;
					return false;
				}
			}	
			
			if(objectId == Item.OBJECT_CAPITAL){
				var validationHabitat : Boolean = Utilities.validateForm(proposalForm.capitalForm.validatorForm, ResourceManager.getInstance().getString('resources', 'riskData'));
				if(validationHabitat == false){
					proposalForm.capitalWindow.opened = true;
					return false;
				}
			}

			// purchase protected
			if(objectId == Item.OBJECT_PURCHASE_PROTECTED){
				var validationPurchaseProtected : Boolean = Utilities.validateForm(proposalForm.purchaseProtectedForm.validatorPurchaseProtected, ResourceManager.getInstance().getString('resources', 'riskData'));
				if(validationPurchaseProtected == false){
					proposalForm.purchaseProtectedWindow.opened = true;
					return false;
				}
				if( !proposalForm.purchaseProtectedForm.vldDueDate() ){
					Utilities.warningMessage("toleranceToInsuredCardExpiration", [proposalForm.purchaseProtectedForm.toleranceCardExpiration]);
					proposalForm.purchaseProtectedWindow.opened = true;
					return false;
				}
			}	
			
			// LifeCicle
			if(objectId == Item.OBJECT_LIFE_CYCLE){
				// grupo familiar 
				for each (var itemGroup : ItemPersonalRiskGroup in proposalForm.familyGroupForm.groupItens){
					// Na lista do grupo familiar não pode conter um documento igual ao do segurado. Exceto menor.
					if(		itemGroup.documentNumber 	!= 	null 
						&& 	itemGroup.documentNumber 	!= 	""
						&& 	itemGroup.personalRiskType 	!= 	ItemPersonalRisk.PERSONAL_RISK_TYPE_NEWBORN
						&& 	itemGroup.documentNumber 	== 	proposalForm.insuredForm.txtDocumentNumber.text)
						{
							proposalForm.additionalWindow.opened = true;
							proposalForm.familyGroupWindow.opened = true;
							Utilities.warningMessage("documentNumberExclusiveGroupAndInsured");
							return false;	
					}
					// adicionais
					for each (var additional : ItemPersonalRisk in proposalForm.additionalForm.additionalItens){
						// Não pode repetir o documento na lista de grupo familiar e na lista de adicionais.
						if(		itemGroup.documentNumber 	!=	null 
							&& 	itemGroup.documentNumber 	!= 	""
							&& 	itemGroup.documentNumber 	== 	additional.documentNumber)
							{
								proposalForm.additionalWindow.opened = true;
								proposalForm.familyGroupWindow.opened = true;
								Utilities.warningMessage("documentNumberExclusiveAdditionalAndGroup");
								return false;
						}
					}
				}
				
				// adicionais
					for each (var additional : ItemPersonalRisk in proposalForm.additionalForm.additionalItens){
					// Na lista de adicionais não pode conter um documento igual ao do segurado. Exceto menor.
					if(		additional.documentNumber 	!= 	null 
						&& 	additional.documentNumber 	!= 	""
						&& 	additional.personalRiskType	!= 	ItemPersonalRisk.PERSONAL_RISK_TYPE_NEWBORN
						&& 	additional.documentNumber 	== 	proposalForm.insuredForm.txtDocumentNumber.text)
						{
							proposalForm.additionalWindow.opened = true;
							Utilities.warningMessage("documentNumberExclusiveAdditionalAndInsured");
							return false;
					} 
					// É obrigatório o preenchimento dos dados pessoais de todos adicionais
					if(		additional.firstName 	== 	ResourceManager.getInstance().getString("resources", "fill")
						||	additional.firstName	== 	null
						||	additional.firstName	== 	"")
						{
							proposalForm.additionalWindow.opened = true;
							Utilities.warningMessage("pleaseEnterAllValueAdditional");
							return false;
					}
				}
			}
			
			if(objectId == Item.OBJECT_CREDIT_INSURANCE){
				var validationCreditInsurance : Boolean = Utilities.validateForm(proposalForm.insuranceCreditForm.validatorInsuranceCredit, ResourceManager.getInstance().getString('resources', 'riskData'));
				if(!validationCreditInsurance){
					proposalForm.insuredWindow.opened = false;
					proposalForm.policyHolderWindow.opened = false;
					proposalForm.beneficiaryWindow.opened = false;
					proposalForm.beneficiaryWindow.opened = false;
					proposalForm.insuranceCreditWindow.opened = true;
					return false;
				}
			}
			
			return true;
		}
		/**
		 * Adjustments the Endorsement
		 * **/
		private function adjustmentsEndorsement(workEndorsement : Endorsement) : void{
			if(workEndorsement){
				this.adjustmentsCustomer( workEndorsement.customerByInsuredId);
				this.adjustmentsCustomer( workEndorsement.customerByPolicyHolderId);
				
				/**
				 * countryId fixed in 1 "venezuela" 
				 * **/
				 
				switch(workEndorsement.getMainItem().objectId){
	 	 			case Item.OBJECT_HABITAT: 
					case Item.OBJECT_CONDOMINIUM:
					case Item.OBJECT_CAPITAL:
						var itemProperty : ItemProperty = workEndorsement.getMainItem() as ItemProperty;
						itemProperty.countryId = new String(1);
						break;
					case Item.OBJECT_AUTOMOVIL:
						break;
					default:
						var itemPersonalRisk : ItemPersonalRisk = workEndorsement.getMainItem() as ItemPersonalRisk;
						itemPersonalRisk.birthDate = workEndorsement.customerByInsuredId.birthDate;
						itemPersonalRisk.maritalStatus = workEndorsement.customerByInsuredId.maritalStatus;
						itemPersonalRisk.gender = workEndorsement.customerByInsuredId.gender;
						itemPersonalRisk.documentIssuer = workEndorsement.customerByInsuredId.documentIssuer;
						itemPersonalRisk.documentNumber = workEndorsement.customerByInsuredId.documentNumber;
						itemPersonalRisk.documentType = workEndorsement.customerByInsuredId.documentType;
						itemPersonalRisk.documentValidity = workEndorsement.customerByInsuredId.documentValidity;
						itemPersonalRisk.professionId = workEndorsement.customerByInsuredId.professionId;
						itemPersonalRisk.occupationId = workEndorsement.customerByInsuredId.occupationId;
						itemPersonalRisk.kinshipType = 294;
						break;
				}
				
			}
		} 
		
		/**
		 * Adjustments the initialize Endorsement
		 * 
		 * countryId fixed in 1 "venezuela" 
		 * **/
		private function adjustmentsInitialEndorsement(initialEndorsement : Endorsement) : void{
			if(initialEndorsement){
				this.adjustmentsInitialCustomer(initialEndorsement.customerByInsuredId);
				this.adjustmentsInitialCustomer(initialEndorsement.customerByPolicyHolderId);
				
				 if(initialEndorsement.getMainItem().objectId == Item.OBJECT_HABITAT ||
				 	initialEndorsement.getMainItem().objectId == Item.OBJECT_CONDOMINIUM){
					var itemProperty : ItemProperty = initialEndorsement.getMainItem() as ItemProperty;
						itemProperty.countryId = new String(1);
				 }
			}
		} 		
		
		private function adjustmentsInitialCustomer(initialCustomer : Customer) : void{
			if(initialCustomer){
				if(initialCustomer.addresses != null && initialCustomer.addresses.length > 0){
					var initialAddress : Address = initialCustomer.addresses.getItemAt(0) as Address;
						initialAddress.countryId = 1;	
				}					
			}
		}
		
		/**
		 * Adjustments the Customer
		 * **/
		private function adjustmentsCustomer(workCustomer : Customer) : void{
			if(workCustomer){
				workCustomer.registerDate = new Date;
				if((workCustomer.name == null || workCustomer.name == "") && (workCustomer.firstName != null || workCustomer.lastName != null)){
					workCustomer.name = workCustomer.firstName + " " + workCustomer.lastName; 
				}
				if(workCustomer.addresses != null && workCustomer.addresses.length > 0){
					var workAddress : Address = workCustomer.addresses.getItemAt(0) as Address;
					workAddress.active = true;
			
					if(workAddress.phones != null){
						var phone1 : Phone = workAddress.phones.getItemAt(0) as Phone;
						phone1.active = true;
						if(workAddress.phones.length > 1){
							var phone2 : Phone = workAddress.phones.getItemAt(1) as Phone;
							phone2.active = true;
							if(phone2.phoneNumber == null || phone2.phoneNumber == "" || phone2.phoneType == 0){
								workAddress.phones.removeItemAt(1);
								workAddress.phones.refresh();
							}
						}
					}
				}
			}
		}

		/**
		 * fields of insured address to risk address of item habitat detail
		 * **/
		private function riskAddressEqualInsured(addressCustomer:Customer):void{
			if(addressCustomer){
				if(addressCustomer.addresses != null && addressCustomer.addresses.length > 0){
					var address:Address = addressCustomer.addresses.getItemAt(0) as Address;
					var itemProperty:ItemProperty = null;
					
					switch(proposalForm.endorsement.getMainItem().objectId){
						case Item.OBJECT_HABITAT:
							itemProperty = proposalForm.habitatForm.itemProperty
							proposalForm.habitatForm.cmbxStateHabitatDetail.selectedItemId = address.stateId;
							break;
						case Item.OBJECT_CAPITAL:
							itemProperty = proposalForm.capitalForm.itemProperty;
							break;
						case Item.OBJECT_CONDOMINIUM:
							itemProperty = proposalForm.condominiumForm.itemProperty;
							proposalForm.condominiumForm.cmbxStateCondominiumDetail.selectedItemId = address.stateId;
							break;
					}
					
					if (itemProperty != null) {
						itemProperty.stateId = new String(address.stateId);
						itemProperty.stateName = address.stateName;
						itemProperty.cityId = new String(address.cityId);
						itemProperty.cityName = address.cityName;
						itemProperty.countryId = new String(address.countryId);
						itemProperty.countryName = address.countryName;
						itemProperty.addressStreet = address.addressStreet;
						itemProperty.districtName = address.districtName;
					}
				}
			}
		}
		
		/**
		* Set layout for initialize
		* **/
		private function initializeLayout() : void{
			this.initializeLayoutState();
			
			proposalForm.insuranceCreditWindow.visible = false;
			proposalForm.insuranceCreditWindow.includeInLayout = false;
			
			proposalForm.policyHolderWindow.visible = false;
			proposalForm.policyHolderWindow.includeInLayout = false;
			
			proposalForm.habitatWindow.visible = false;
			proposalForm.habitatWindow.includeInLayout = false;
			
			proposalForm.condominiumWindow.visible = false;
			proposalForm.condominiumWindow.includeInLayout = false;
			
			proposalForm.additionalWindow.visible = false;
			proposalForm.additionalWindow.includeInLayout = false;
			proposalForm.familyGroupWindow.visible = false;
			proposalForm.familyGroupWindow.includeInLayout = false;
			
			proposalForm.resumeProposal.dgCoverages.visible = false;
			proposalForm.resumeProposal.dgCoverages.includeInLayout = false;
			
			proposalForm.resumeProposal.dgPropertyCoverages.visible = false;
			proposalForm.resumeProposal.dgPropertyCoverages.includeInLayout=false;
			
			proposalForm.motoristWindow.visible = false;
			proposalForm.motoristWindow.includeInLayout = false;
			
			proposalForm.vehicleWindow.visible = false;
			proposalForm.vehicleWindow.includeInLayout = false;
			
			proposalForm.capitalWindow.visible = false;
			proposalForm.capitalWindow.includeInLayout = false;
			
			proposalForm.purchaseProtectedWindow.visible = false;
			proposalForm.purchaseProtectedWindow.includeInLayout = false;
		}
		
		private function initializeLayoutState() : void{
			proposalForm.insuredWindow.opened = true; 
			proposalForm.policyHolderWindow.opened = false;
			proposalForm.beneficiaryWindow.opened = false;
			proposalForm.insuranceCreditWindow.opened = false;
			proposalForm.habitatWindow.opened = false;
			proposalForm.condominiumWindow.opened = false;
			proposalForm.familyGroupWindow.opened = false;
			proposalForm.additionalWindow.opened = false;
			proposalForm.paymentWindow.opened = false;
			proposalForm.vehicleWindow.opened = false;
			proposalForm.motoristWindow.opened = false;
			proposalForm.capitalWindow.opened = false;
			proposalForm.purchaseProtectedWindow.opened = false;
		}
		
		/**
		 * Habilita e desabilita os componentes que mudam de acordo com a situação da apolice.
		 * 
		 * **/
		private function enabledAllForm(boolean : Boolean) : void{
			this.initializeLayoutState();
    		proposalForm.beneficiaryWindow.enabled = boolean;
    		proposalForm.insuredWindow.enabled = boolean;
    		proposalForm.policyHolderWindow.enabled = boolean;
    		proposalForm.paymentWindow.enabled = boolean;
    		proposalForm.resumeProposal.enabled = boolean;
    		proposalForm.imgAccept.enabled = boolean;
    		proposalForm.habitatWindow.enabled = boolean;
    		proposalForm.condominiumWindow.enabled = boolean;
    		proposalForm.insuranceCreditWindow.enabled = boolean;
    		proposalForm.familyGroupWindow.enabled = boolean;
    		proposalForm.additionalWindow.enabled = boolean;
    		proposalForm.motoristWindow.enabled = boolean;
    		proposalForm.vehicleWindow.enabled = boolean;
    		proposalForm.capitalForm.enabled = boolean;
    		proposalForm.purchaseProtectedWindow.enabled = boolean;
    		proposalForm.insuredForm.btnDocumentValidation.enabled = boolean;
    	}
    	
    	/**
    	 * Limpa os forms
    	 * */
		public function resetForm(event:Event=null):void{
			enabledAllForm(true);
			proposalForm.setButtonsState(ProposalForm.STATE_EFFECTIVE_AFTER);
			insuredFormMediator.resetForm();
			policyHolderFormMediator.resetForm();
			beneficiaryFormMediator.resetForm();
			//paymentFormMediator.resetForm();
			itemHabitatMediator.resetForm();
			itemCondominiumMediator.resetForm();
			familyGroupMediator.resetForm();
			additionalMediator.resetForm();
			purchaseProtectedFormMediator.resetForm();
			itemCapitalMediator.resetForm();
		}
		
		private function getReportId(event:Event):void{
			if (proposalForm.endorsement != null)
				reportProxy.getReportId(proposalForm.endorsement.id.contractId, proposalForm.endorsement.id.endorsementId, Constant.REPORT_TYPE_CERTIFICATE);
		}
		
		private function listReportId():void{
			if (proposalForm.endorsement != null)
				reportProxy.listReportId(proposalForm.endorsement.id.contractId, proposalForm.endorsement.id.endorsementId);
		}
		
		private function policyPrint(event:MenuEvent):void{
  	  		var reporte:Report = event.item as Report;
  	  		if(reporte != null){
				proposalForm.print(reporte.reportId.toString());
  	  		}
		}
	}  
}