////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2009 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 12/01/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.sales.view.mediator
{
	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.core.components.CityCombo.view.mediator.CityComboMediator;
	import br.com.tratomais.einsurance.core.components.CountryCombo.view.mediator.CountryComboMediator;
	import br.com.tratomais.einsurance.core.components.StateCombo.view.mediator.StateComboMediator;
	import br.com.tratomais.einsurance.customer.model.proxy.DataOptionProxy;
	import br.com.tratomais.einsurance.customer.model.proxy.FindCepProxy;
	import br.com.tratomais.einsurance.customer.model.vo.Customer;
	import br.com.tratomais.einsurance.customer.model.vo.DataOption;
	import br.com.tratomais.einsurance.products.model.proxy.ProductProxy;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	import br.com.tratomais.einsurance.products.model.vo.PersonalRiskOption;
	import br.com.tratomais.einsurance.products.model.vo.PlanKinship;
	import br.com.tratomais.einsurance.customer.model.vo.ZipCodeAddress;
	import br.com.tratomais.einsurance.sales.model.proxy.ProposalProxy;
	import br.com.tratomais.einsurance.sales.view.components.PolicyHolderForm;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.resources.ResourceManager;
	import mx.validators.Validator;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class PolicyHolderFormMediator extends Mediator implements IMediator 
	{
		public static const NAME:String = 'PolicyHolderFormMediator';  
		private var proposalProxy:ProposalProxy;
		private var productProxy:ProductProxy;
		private var dataOptionProxy:DataOptionProxy;
		
        private var countryComboMediator:CountryComboMediator;
        private var stateComboMediator:StateComboMediator;
        private var cityComboMediator:CityComboMediator;		
		private var findCepProxy : FindCepProxy;
		
		public static const KINSHIP_TYPE:int = 145;
		public static const RENEWAL_TYPE:int = 91;
		public static const PERSONAL_RISK_TYPE:int = 599;
		
		public function PolicyHolderFormMediator(viewComponent:Object)  
        {  
            super(viewComponent.id, viewComponent);
        } 
        
		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */	        
		override public function onRegister():void 
		{
			super.onRegister();
			proposalProxy = ProposalProxy ( facade.retrieveProxy(ProposalProxy.NAME));
			productProxy = ProductProxy(facade.retrieveProxy(ProductProxy.NAME));
			dataOptionProxy = DataOptionProxy(facade.retrieveProxy(DataOptionProxy.NAME));
			findCepProxy = FindCepProxy(facade.retrieveProxy(FindCepProxy.NAME));
						
			policyHolderForm.addEventListener(PolicyHolderForm.POLICY_HOLDER_TYPE_CHANGED, this.onChangePersonType);
			policyHolderForm.addEventListener(PolicyHolderForm.POLICY_HOLDER_AGE, this.policyHolderAge);	
			
			policyHolderForm.addEventListener(PolicyHolderForm.SET_CARD_HOLDER_NAME , this.setCardHolderName);
			policyHolderForm.imgFindCep.addEventListener(MouseEvent.CLICK, findCep, false, 0, true);
			
			countryComboMediator = new CountryComboMediator(policyHolderForm.cmbxCountryPH);
			stateComboMediator = new StateComboMediator(policyHolderForm.cmbxStatePH);
			cityComboMediator = new CityComboMediator(policyHolderForm.cmbxCityPH);
				
			ApplicationFacade.getInstance().registerMediator(countryComboMediator);
			ApplicationFacade.getInstance().registerMediator(stateComboMediator);
			ApplicationFacade.getInstance().registerMediator(cityComboMediator);				 
		}
		
        public function get policyHolderForm():PolicyHolderForm{  
            return viewComponent as PolicyHolderForm;  
        } 
        		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
			policyHolderForm.removeEventListener(PolicyHolderForm.POLICY_HOLDER_TYPE_CHANGED, this.onChangePersonType);
			policyHolderForm.removeEventListener(PolicyHolderForm.POLICY_HOLDER_AGE, this.policyHolderAge);
			
			policyHolderForm.removeEventListener(PolicyHolderForm.SET_CARD_HOLDER_NAME , this.setCardHolderName);
			policyHolderForm.imgFindCep.removeEventListener(MouseEvent.CLICK, findCep);
			
			ApplicationFacade.getInstance().removeMediator(countryComboMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(stateComboMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(cityComboMediator.getMediatorName());
			this.resetForm();
		}

		/**
		 * Defines the list of Notification Interests
		 */            
        override public function listNotificationInterests():Array  
        {  
            return [NotificationList.PRODUCT_LIST_PLAN_KINSHIP_RISK_TYPE,
            		NotificationList.DATA_OPTION_LOADED,
            		NotificationList.IDENTIFIED_CEP_FOUND];
        }

		/**
        * handle Notification 
        */           
        override public function handleNotification(notification:INotification):void  
        {  
	        switch (notification.getName())  
	        { 
	            case NotificationList.PRODUCT_LIST_PLAN_KINSHIP_RISK_TYPE:
	            	policyHolderForm.planKinship = (notification.getBody() as PlanKinship);
	            break;
	            case NotificationList.DATA_OPTION_LOADED:
	            	var dataOption:DataOption = notification.getBody() as DataOption;
	            	if( dataOption )
	            	{
		            	if( dataOption.dataOptionId == PERSONAL_RISK_TYPE){
		            		policyHolderForm.personalType = ((notification.getBody() as DataOption).fieldDescription);
		            	}
		            }
	            break;
	            case NotificationList.IDENTIFIED_CEP_FOUND:
					var idf:IdentifiedList = notification.getBody() as IdentifiedList;
					
					if(idf.nameComponent == policyHolderForm.id) {
						populateFormCepFound(idf.value as ZipCodeAddress);
					}
				break;
	        }
		}

    	public function onChangePersonType(event:Event):void{
    		if(policyHolderForm.customer != null){
    			if(policyHolderForm.customer.personType == 0){
    				policyHolderForm.customer.personType = policyHolderForm.cmbxPersonType.selectedValue;
    			}
    			if(policyHolderForm.customer.personType == PersonalRiskOption.LEGALLY_PERSON){
	    			proposalProxy.listAllInflowPolicyHolder(PersonalRiskOption.LEGALLY_PERSON_NAME);

    				//policyHolderForm.cmbxDocumentType.enabled = true;
    				policyHolderForm.cmbxDocumentType.dataProvider = policyHolderForm.listDocumentTypeLegally;
    				policyHolderForm.cmbxDocumentType.dropdown.dataProvider = policyHolderForm.listDocumentTypeLegally;

    				policyHolderForm.txtLastName.text = "";
					policyHolderForm.txtFirstName.text = "";
					policyHolderForm.cmbxGender.selectedIndex = -1;
					policyHolderForm.dfDatebirth.data = null;
					policyHolderForm.cmbxMaritalStatus.selectedIndex = -1;
					policyHolderForm.frmTradeName.visible=true;
					policyHolderForm.frmTradeName.includeInLayout=true;
					policyHolderForm.frmCorporateName.visible=true;
					policyHolderForm.frmCorporateName.includeInLayout=true;
					policyHolderForm.frmSalaryRange.label=ResourceManager.getInstance().getString("resources", "salaryRangePJ");
					policyHolderForm.validatorRegister(true, false);
					this.enabledNaturalFields(false);
    			}else if(policyHolderForm.customer.personType == PersonalRiskOption.NATURAL_PERSON){
	    			proposalProxy.listAllInflowPolicyHolder(PersonalRiskOption.NATURAL_PERSON_NAME);

    				//policyHolderForm.cmbxDocumentType.enabled = true;
    				policyHolderForm.cmbxDocumentType.dataProvider = policyHolderForm.listDocumentTypeNatural;
    				policyHolderForm.cmbxDocumentType.dropdown.dataProvider = policyHolderForm.listDocumentTypeNatural;    				

    				policyHolderForm.txtTradeName.text = "";
					policyHolderForm.frmTradeName.visible=false;
					policyHolderForm.frmTradeName.includeInLayout=false;
    				policyHolderForm.txtCorporateName.text = "";
					policyHolderForm.frmCorporateName.visible=false;
					policyHolderForm.frmCorporateName.includeInLayout=false;
					policyHolderForm.frmSalaryRange.label=ResourceManager.getInstance().getString("resources", "salaryRangePF");
					this.enabledNaturalFields(true);
					policyHolderForm.validatorRegister(false, true);
    			}
    			
    			Utilities.redBorderField( policyHolderForm.cmbxPersonType, false );
    			Utilities.redBorderField( policyHolderForm.cmbxDocumentType, false );
    			Utilities.redBorderField( policyHolderForm.txtDocumentNumber, false );
    			Utilities.redBorderField( policyHolderForm.cmbxPhoneType1, false );
    			policyHolderForm.resetRedBorderForm();    			
    		}
    	}

		private function enabledNaturalFields(pData:Boolean):void{
			policyHolderForm.frmFirstName.visible=pData;
			policyHolderForm.frmFirstName.includeInLayout=pData;
			policyHolderForm.frmLastName.visible=pData;
			policyHolderForm.frmLastName.includeInLayout=pData;					
			policyHolderForm.frmGender.visible=pData;
			policyHolderForm.frmGender.includeInLayout=pData;
			policyHolderForm.frmDatebirth.visible=pData;
			policyHolderForm.frmDatebirth.includeInLayout=pData;
			policyHolderForm.frmMaritalStatus.visible=pData;
			policyHolderForm.frmMaritalStatus.includeInLayout=pData;
			policyHolderForm.frmProfession.visible=pData;
			policyHolderForm.frmProfession.includeInLayout=pData;
			policyHolderForm.frmProfessionType.visible=pData;
			policyHolderForm.frmProfessionType.includeInLayout=pData;
			policyHolderForm.frmOccupation.visible=pData;
			policyHolderForm.frmOccupation.includeInLayout=pData;
		}

		public function resetForm():void{		
			Utilities.resetForm(policyHolderForm.formPersonType);
			Utilities.resetForm(policyHolderForm.formDetail);
			Utilities.resetForm(policyHolderForm.formAddress);
			Utilities.resetForm(policyHolderForm.formPhone);			
			
			//policyHolderForm.cmbxDocumentType.enabled = false;
			//policyHolderForm.cmbxSalary.enabled = false;
			
			policyHolderForm.customer = null;
			
			policyHolderForm.validatorRegister(true, true);
		}

	    private function policyHolderAge(event:Event):void{
			productProxy.listPlanKinshipByPersonalRiskType(policyHolderForm.productId, policyHolderForm.riskPlanId, PERSONAL_RISK_TYPE, RENEWAL_TYPE, policyHolderForm.referenceDate);
			dataOptionProxy.getDataOptionById(PERSONAL_RISK_TYPE);
	    }
		
/* 		public function populateCustomerToForm(customerPolicyHolder : Customer) : void{
			
			customerPolicyHolder.personType = (policyHolderForm.cmbxPersonType.selectedItem as PersonalRiskOption).personType;
			customerPolicyHolder.documentType = (policyHolderForm.cmbxDocumentType.selectedItem as DataOption).dataOptionId;
			customerPolicyHolder.documentNumber = policyHolderForm.txtDocumentNumber.text;
			customerPolicyHolder.emailAddress = policyHolderForm.txtEmail.text;
			customerPolicyHolder.registerDate = new Date();
			
			if(customerPolicyHolder.personType == PersonalRiskOption.NATURAL_PERSON){
				customerPolicyHolder.firstName = policyHolderForm.txtFirstName.text;
				customerPolicyHolder.lastName = policyHolderForm.txtLastName.text;
				customerPolicyHolder.gender = (policyHolderForm.cmbxGender.selectedItem as DataOption).dataOptionId;
				customerPolicyHolder.birthDate = policyHolderForm.dfDatebirth.newDate;
				customerPolicyHolder.maritalStatus = (policyHolderForm.cmbxMaritalStatus.selectedItem as DataOption).dataOptionId;
				customerPolicyHolder.name = customerPolicyHolder.firstName + " " + customerPolicyHolder.lastName;
			}else{
				customerPolicyHolder.tradeName = policyHolderForm.txtTradeName.text;
				customerPolicyHolder.name = customerPolicyHolder.tradeName;
			}
			
			
			if(policyHolderForm.cmbxProfessionType.selectedItem != null){
				customerPolicyHolder.professionType = (policyHolderForm.cmbxProfessionType.selectedItem as DataOption).dataOptionId;
			}
			if(policyHolderForm.cmbxProfession.selectedItem != null){
				customerPolicyHolder.professionId = new String((policyHolderForm.cmbxProfession.selectedItem as Profession).professionId);
			}
			if(policyHolderForm.cmbxOccupation.selectedItem != null){
				customerPolicyHolder.occupationId = (policyHolderForm.cmbxOccupation.selectedItem as Occupation).occupationId.toString();
			}
			if(policyHolderForm.cmbxActivity.selectedItem != null){
				customerPolicyHolder.activityId = (policyHolderForm.cmbxActivity.selectedItem as Activity).activityId.toString();
			}
 			if(policyHolderForm.cmbxSalary.selectedItem != null){
				customerPolicyHolder.inflowId = new String((policyHolderForm.cmbxSalary.selectedItem as Inflow).inflowId);
			}
 			
			var address : Address = customerPolicyHolder.addAddress();
			address.addressType = (policyHolderForm.cmbxAddressType.selectedItem as DataOption).dataOptionId;
			address.districtName = policyHolderForm.txtDistrict.text;
			address.addressStreet = policyHolderForm.txtAddress.text;
			address.zipCode = policyHolderForm.txtZipCode.text;

			address.countryId = (policyHolderForm.cmbxCountryPH.selectedItem as Country).countryId;
			address.countryName = (policyHolderForm.cmbxCountryPH.selectedItem as Country).name;
			address.stateId = (policyHolderForm.cmbxStatePH.selectedItem as State).stateId;
			address.stateName = (policyHolderForm.cmbxStatePH.selectedItem as State).name;
			address.cityId = (policyHolderForm.cmbxCityPH.selectedItem as City).cityId;
			address.cityName = (policyHolderForm.cmbxCityPH.selectedItem as City).name;
			
			var phone : Phone = address.addPhone(true);
			phone.phoneType = (policyHolderForm.cmbxPhoneType1.selectedItem as DataOption).dataOptionId;
			phone.phoneNumber = policyHolderForm.txtPhone.text;
			
			if(policyHolderForm.txtPhone2.text != ""){
				var phone : Phone = address.addPhone(false);
				phone.phoneType = (policyHolderForm.cmbxPhoneType2.selectedItem as DataOption).dataOptionId;
				phone.phoneNumber = policyHolderForm.txtPhone2.text;
			}
			
		}
 */		
		public function validateFieldsPolicyHolder():Boolean {
        	var isValid:Boolean = true;
            var errors:Array = Validator.validateAll(policyHolderForm.validatorPolicyHolder);
            if (errors.length != 0) {
                isValid = false;
            }
            return isValid;
        }
        
		public function get customer():Customer{
			return policyHolderForm.customer;
		}

		public function set customer(pData:Customer):void{
			policyHolderForm.customer=pData;
		}
		
		public function setCardHolderName(event:Event):void{
			var policyHolderName:String;
			
			policyHolderName = policyHolderForm.txtFirstName.text + " " +
							   policyHolderForm.txtLastName.text;
			
			sendNotification(NotificationList.SET_POLICY_HOLDER_NAME, policyHolderName);
		}
		
		public function findCep(evt:MouseEvent):void {
			if(policyHolderForm.txtZipCode.length == 8) {
				var idf:IdentifiedList = new IdentifiedList();
				idf.nameComponent = policyHolderForm.id;
				
				findCepProxy.identifiedListFindCep(idf, policyHolderForm.txtZipCode.text);
			}
			else {
				Utilities.warningMessage('zipCodeInvalid');
			}
		}
		
		private function populateFormCepFound(zipCodeAdress:ZipCodeAddress):void {
			if(zipCodeAdress && zipCodeAdress.stateId > 0) {
				policyHolderForm.cmbxStatePH.CleanCombo();
				policyHolderForm.cmbxCityPH.CleanCombo();
				
				// preencher Objetos de apoio, não mudar
				policyHolderForm.cmbxStatePH.stateId = zipCodeAdress.stateId;
				policyHolderForm.cmbxCityPH.cityId = zipCodeAdress.cityId;
				
				if(policyHolderForm.cmbxStatePH.address)
					policyHolderForm.cmbxStatePH.address.stateId = zipCodeAdress.stateId;
				if(policyHolderForm.cmbxCityPH.address)
					policyHolderForm.cmbxCityPH.address.cityId = zipCodeAdress.cityId;
				
				// recarrega os combos de Estado/Cidade
				policyHolderForm.cmbxStatePH.countryId = zipCodeAdress.countryId;
				//policyHolderForm.cmbxCity.stateId = zipCodeAdress.stateId;
				
				policyHolderForm.txtDistrict.text = zipCodeAdress.districtName;
				policyHolderForm.txtAddress.text = zipCodeAdress.addressStreet;
			}
			else {
				Utilities.warningMessage('zipCodeInvalid');
			}
		}
		
	}
}