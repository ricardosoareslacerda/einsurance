package br.com.tratomais.einsurance.sales.view.mediator
{
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.customer.model.proxy.DataOptionProxy;
	import br.com.tratomais.einsurance.sales.model.proxy.ProposalProxy;
	import br.com.tratomais.einsurance.sales.view.components.JustificationForm;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
		
	/**
	 * Mediator used with the PasswordChangeForm
	 * @see br.com.tratomais.einsurance.useradm.view.components.PasswordChangeForm
	 */
	[ResourceBundle("messages")]
	[ResourceBundle("resources")]
	public class JustificationProposalMediator extends Mediator implements IMediator
	{
		/**
		 * Mediator name
		 */
		public static const NAME:String = 'JustificationProposalMediator';
		/**
		 * Constructor
		 * @param viewComponent The PasswordChangeForm instance
		 */
		public function JustificationProposalMediator(viewComponent:JustificationForm, endorsement:int, contract : int)
		{
            super(NAME, viewComponent);
            proposalProxy = ProposalProxy (facade.retrieveProxy(ProposalProxy.NAME));
            dataOptionProxy = DataOptionProxy (facade.retrieveProxy(DataOptionProxy.NAME));
            _endorsementId = endorsement;
            _contractId = contract;
		}
		
		/**
		 * Proxy
		 * */
		private var proposalProxy : ProposalProxy;
		private var dataOptionProxy : DataOptionProxy;
					
		/**
		 * 
		 * */
		private var _contractId : int;
		
		/**
		 * 
		 * **/
		private var _endorsementId : int;
		
		public function set endorsementId( pData : int){
			this._endorsementId = pData;
		}

		public function get endorsementId() : int{
			return this._endorsementId;
		}

		public function set contractId( pData : int){
			this._contractId = pData;
		}

		public function get contractId() : int{
			return this._contractId;
		}

		/**
		 * Return the password form
		 */		
        protected function get justificationForm():JustificationForm{  
          return viewComponent as JustificationForm;  
        }

   		override public function listNotificationInterests():Array  
        {  
            return [ NotificationList.DATA_OPTION_LISTED ];
        }
            
        override public function handleNotification(notification:INotification):void  
        {  
            switch ( notification.getName() )  
            {  
        		case NotificationList.DATA_OPTION_LISTED:
        			var collection : ArrayCollection = notification.getBody() as ArrayCollection;
        			justificationForm.cmbxJustifications.dataProvider = collection;
        			justificationForm.cmbxJustifications.dropdown.dataProvider = collection;
        			justificationForm.cmbxJustifications.selectedIndex = -1;
        		break;
            }
        }
        		
		/**
		 * During the Register Phrase, register the Events that the form depends on. Initialize variables needed.
		 */        
		override public function onRegister():void 
		{
           	super.onRegister();
            justificationForm.addEventListener(JustificationForm.SAVE_CHANGES, saveChanges);
            justificationForm.addEventListener(JustificationForm.CANCEL_CHANGES, cancelChanges);
            this.initialize();
		}
		 
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
		   	justificationForm.removeEventListener(JustificationForm.CANCEL_CHANGES, cancelChanges);
           	justificationForm.removeEventListener(JustificationForm.SAVE_CHANGES, saveChanges);
		}
		
		private function initialize() : void{
			dataOptionProxy.listDataOption(87);//Motivo de la anulación	
			
		}
		
		// ----------------------------------------------------------------------------------------------------------
		
		/**
		 * Saves changes, validating the field values
		 */
		private function saveChanges(event: Event):void{
			if (Utilities.validateForm([justificationForm.vldJustification], ResourceManager.getInstance().getString('resources', 'reasonCancel')) && justificationForm.justificationSelectedId > 0) {
				proposalProxy.cancellationProposal(endorsementId,contractId,justificationForm.justificationSelectedId);
			} 
		}
		
		/**
		 * Close form without changes
		 * @param event E
		 */
		private function cancelChanges(event: Event):void{
	        justificationForm.close();
		}
	}
}