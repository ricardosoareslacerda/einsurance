////////////////////////////////////////////////////////////////////////////////
//
// TRATO+ SOLUÇÕES TECNOLOGICAS
// Copyright 2006-2010 Trato+ Soluções Tecnologicas
// All Rights Reserved.
//
// @autor Eduardo Venâncio
// @version 1.0
// @lastModified 12/01/2010
//
// NOTICE: TRATO+ permits you to use, no modify, and no distribute this file
// in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package br.com.tratomais.einsurance.sales.view.mediator
{
	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.components.CityCombo.view.mediator.CityComboMediator;
	import br.com.tratomais.einsurance.core.components.CountryCombo.view.mediator.CountryComboMediator;
	import br.com.tratomais.einsurance.core.components.StateCombo.view.mediator.StateComboMediator;
	import br.com.tratomais.einsurance.customer.model.vo.City;
	import br.com.tratomais.einsurance.customer.model.vo.Country;
	import br.com.tratomais.einsurance.customer.model.vo.State;
	import br.com.tratomais.einsurance.policy.model.vo.ItemProperty;
	import br.com.tratomais.einsurance.sales.view.components.ItemCondominiumDetail;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class ItemCondominiumMediator extends Mediator implements IMediator 
	{
		public static const NAME:String = 'ItemCondominiumMediator';  

        private var countryComboMediator:CountryComboMediator;
        private var stateComboMediator:StateComboMediator;
        private var cityComboMediator:CityComboMediator;
			        
		public function ItemCondominiumMediator(viewComponent:Object)  
        {  
            super(NAME, viewComponent);
        }
        
        protected function get itemCondominiumDetail():ItemCondominiumDetail{  
            return viewComponent as ItemCondominiumDetail;  
        } 

		/**
		 * Handle the registration state by setting up mediators for all children 
		 * of this application and retrieving any dependencies such as proxies.
		 */	                
		override public function onRegister():void 
		{
			super.onRegister();
			
			countryComboMediator = new CountryComboMediator(itemCondominiumDetail.cmbxCountryCondominiumDetail);
			stateComboMediator = new StateComboMediator(itemCondominiumDetail.cmbxStateCondominiumDetail);
			cityComboMediator = new CityComboMediator(itemCondominiumDetail.cmbxCityCondominiumDetail);
			
			ApplicationFacade.getInstance().registerMediator(countryComboMediator);
			ApplicationFacade.getInstance().registerMediator(stateComboMediator);
			ApplicationFacade.getInstance().registerMediator(cityComboMediator);			
    	}
		 	        		
		/**
		 * Handle the removal state by cleaning up and removing any dependencies 
		 * and event handlers.
		 */
		override public function onRemove():void 
		{
			super.onRemove();
			ApplicationFacade.getInstance().removeMediator(countryComboMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(stateComboMediator.getMediatorName());
			ApplicationFacade.getInstance().removeMediator(cityComboMediator.getMediatorName());
			this.resetForm();						
		}
		
		public function populateItemToForm(itemProperty : ItemProperty) : void{
 			var coutrySelected : Country = itemCondominiumDetail.cmbxCountryCondominiumDetail.selectedItem as Country;
			var stateSelectd : State = itemCondominiumDetail.cmbxStateCondominiumDetail.selectedItem as State;
			var citySeleted : City = itemCondominiumDetail.cmbxCityCondominiumDetail.selectedItem as City;
			itemProperty.cityId = new String(citySeleted.cityId);
			itemProperty.stateId = new String(stateSelectd.stateId);
			itemProperty.countryId = new String(coutrySelected.countryId);
			itemProperty.cityName = citySeleted.name;
			itemProperty.stateName = stateSelectd.name;
			itemProperty.countryName = coutrySelected.name;
			itemProperty.addressStreet = itemCondominiumDetail.txtAddress.text;
			itemProperty.districtName = itemCondominiumDetail.txtDistrict.text;	
		}
    	
		public function resetForm():void{
			itemCondominiumDetail.onClean();			
		}
	}
}