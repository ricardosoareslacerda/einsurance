package br.com.tratomais.einsurance.sales.view.mediator
{
	import br.com.tratomais.einsurance.ApplicationFacade;
	import br.com.tratomais.einsurance.core.Constant;
	import br.com.tratomais.einsurance.core.NotificationList;
	import br.com.tratomais.einsurance.core.Utilities;
	import br.com.tratomais.einsurance.core.model.vo.ApplicationProperty;
	import br.com.tratomais.einsurance.policy.model.vo.Endorsement;
	import br.com.tratomais.einsurance.policy.model.vo.Installment;
	import br.com.tratomais.einsurance.policy.model.vo.ItemPersonalRisk;
	import br.com.tratomais.einsurance.products.model.proxy.ProductProxy;
	import br.com.tratomais.einsurance.products.model.vo.BillingMethod;
	import br.com.tratomais.einsurance.products.model.vo.IdentifiedList;
	import br.com.tratomais.einsurance.products.model.vo.PaymentOption;
	import br.com.tratomais.einsurance.products.model.vo.ProductPaymentTerm;
	import br.com.tratomais.einsurance.sales.model.proxy.ProposalProxy;
	import br.com.tratomais.einsurance.sales.model.vo.FullAccessAccountInfo;
	import br.com.tratomais.einsurance.sales.model.vo.PaymentFormVO;
	import br.com.tratomais.einsurance.sales.view.components.PaymentForm;
	import br.com.tratomais.einsurance.useradm.model.vo.Application;
	import br.com.tratomais.einsurance.useradm.model.vo.User;
	
	import flash.errors.IllegalOperationError;
	import flash.events.Event;
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.resources.ResourceManager;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;

	public class PaymentFormMediator extends Mediator implements IMediator
	{
		public static const NAME:String = 'PaymentFormMediator';

		private var paymentFormVO:PaymentFormVO;
		private var firstInstallment:Installment;
		private var paymentTermIdLoaded:Boolean;
		private var paymentObject:int;
		private var arrProductPaymentTerm:ArrayCollection;
		private var productPaymentTermSelected:ProductPaymentTerm;
		private var toleranceCardExpiration:int;
		private var cardHolderName:String;
		private var cardExpirationDate:int;
		private var validatorPayment:Array;
		private var dictMaxCharsAgencyNumber:Dictionary;
		private var _dictBank:Dictionary;
		private var fullAccessAccountInfo:FullAccessAccountInfo;
		private var changeAllowedBillingMethod:Boolean;
		private var disableFields:Array;
		private var actionType:String;
		private var endorsement:Endorsement;
		private var isInvoice:Boolean;
		private var invoiceType:int;
		private var isBilled:Boolean;

		private var productProxy:ProductProxy;
		private var proposalProxy:ProposalProxy;

		public function PaymentFormMediator(viewComponent:Object){
			super(NAME, viewComponent);
		}

		private function get paymentForm():PaymentForm{
			return viewComponent as PaymentForm;
		}

		override public function onRegister():void{
			super.onRegister();
			
			productProxy = ProductProxy(facade.retrieveProxy(ProductProxy.NAME));
			proposalProxy = ProposalProxy(facade.retrieveProxy(ProposalProxy.NAME));
			
			productProxy.findApplicationProperty(new IdentifiedList(NAME), Application.DEFAULT, ApplicationProperty.TOLERANCE_TO_PAYMENT_CARD_EXPIRATION);
		}

		override public function onRemove():void{
			super.onRemove();
			
			onClean();
		}

		override public function listNotificationInterests():Array{
			return [
					NotificationList.PAYMENT_FORM_INITIALIZE_PARAMETERS,
					NotificationList.APPLICATION_PROPERTY_FINDED,
					NotificationList.PAYMENT_TYPE_LISTED,
					NotificationList.PAYMENT_OPTION_LISTED,
					NotificationList.PRODUCT_PAYMENT_TERM_LISTED,
					NotificationList.INSURED_NAME_SELECTED,
					NotificationList.GET_INSTALLMENT_LISTED,
					NotificationList.PAYMENT_FORM_VALIDATE,
					NotificationList.VALIDATE_BANK_INFO_RESULT,
					NotificationList.FAULT_VALIDATE_BANK_INFO,
					NotificationList.ENDORSEMENT_UNLOCKED
					];
		}

		override public function handleNotification(notification:INotification):void{
			switch(notification.getName()){
				case NotificationList.PAYMENT_FORM_INITIALIZE_PARAMETERS:
					paymentFormVO = notification.getBody() as PaymentFormVO;
					
					if(paymentForm.initialized){
						initializePaymentForm(paymentFormVO);
					}
					else{
						paymentForm.addEventListener(FlexEvent.UPDATE_COMPLETE, handleUpdateComplete, false, 0, true);
					}
					break;
					
				case NotificationList.APPLICATION_PROPERTY_FINDED:
					this.notificationApplicationPropertyFinded(notification.getBody() as IdentifiedList);
					break;
					
				case NotificationList.PAYMENT_TYPE_LISTED:
					setListBillingMethod(notification.getBody() as ArrayCollection);
					break;
					
				case NotificationList.PAYMENT_OPTION_LISTED:
					setListPaymentOption(notification.getBody() as ArrayCollection);
					break;
					
				case NotificationList.PRODUCT_PAYMENT_TERM_LISTED:
					setListProductPaymentTerm(notification.getBody() as ArrayCollection);
					break;
					
				case NotificationList.INSURED_NAME_SELECTED:
					setCardHolderName(notification.getBody() as String);
					break;
					
				case NotificationList.GET_INSTALLMENT_LISTED:
					this.notificationGetInstallment(notification.getBody() as Installment);
					break;
					
				case NotificationList.PAYMENT_FORM_VALIDATE:
					this.notificationPaymentValidate();
					break;
					
				case NotificationList.VALIDATE_BANK_INFO_RESULT:
					this.notificationValidateBankinfo(notification.getBody() as Boolean);
					break;
					
				case NotificationList.FAULT_VALIDATE_BANK_INFO:
					this.notificationFaultValidateBankInfo(notification.getBody() as String);
					break;
					
				case NotificationList.ENDORSEMENT_UNLOCKED:
					this.unlockProposalResult(notification.getBody() as Boolean);
					break;
					
				default:
					super.handleNotification(notification);
					break;
			}
		}

		/** Invocando quando o componente PaymentForm termina de renderizar **/
		private function handleUpdateComplete(event:FlexEvent):void{
			paymentForm.removeEventListener(FlexEvent.UPDATE_COMPLETE, handleUpdateComplete);
			
			initializePaymentForm(paymentFormVO);
		}

		/** Invocado para adicionar listeners, listas de ArrayCollection e etc... */
		private function initializePaymentForm(pData:PaymentFormVO):void{
			onClean();
			
			createListeners();
			
			paymentFormVO = pData;
			actionType = paymentFormVO.actionType;
			endorsement = paymentFormVO.endorsement;
			invoiceType = endorsement.termId;
			isBilled = (invoiceType != 0);
			isInvoice = (endorsement.issuanceType == Endorsement.ISSUANCE_TYPE_INVOICE);
			
			changeAllowedBillingMethod = (!isInvoice || (endorsement.paymentTermId == 0) || (endorsement.isFlexible() && (ApplicationFacade.getInstance().loggedUser.authorizationType < User.AUTHORIZATION_TYPE_BROKER)));
			
			enableForm();
			
			validatorPayment = [ paymentForm.vldAgencyNumber, paymentForm.vldDigitAgencyNumber, paymentForm.vldAccountNumber, paymentForm.vldDigitAccountNumber, paymentForm.vldCardHolderName, paymentForm.vldCardNumber, 
								 paymentForm.vldCheckNumber, paymentForm.vldOtherAccountNumber, paymentForm.vldPaymentType, paymentForm.vldCardDateMonth, paymentForm.vldCardDateYear ];
			
			setCardHolderName(endorsement.customerByInsuredId.name);
			paymentForm.frmDueDay.visible = !isInvoice;
			
			if(paymentFormVO){
				productProxy.listProductPaymentTermByRefDate(endorsement.contract.productId, endorsement.referenceDate);
				proposalProxy.listPaymentType(endorsement.contract.productId, (isBilled ? ProductPaymentTerm.PAYMENT_TERM_TYPE_INVOICE : ProductPaymentTerm.PAYMENT_TERM_TYPE_POLICY), endorsement.referenceDate);
				
				firstInstallment = endorsement.installmentById(1);
				if(firstInstallment){
					paymentForm.nmsDueDay.value = firstInstallment.dueDay;
					getListPaymentOption(firstInstallment.billingMethodId);
				}
			}
		}

		private function notificationGetInstallment(pData:Installment):void{
			firstInstallment = pData;
			
			if(firstInstallment){
				paymentForm.nmsDueDay.value = firstInstallment.dueDay;
				changeAllowedBillingMethod = !isInvoice;
				endorsement.paymentTermId = firstInstallment.paymentTermId;
				sendNotification(NotificationList.PREVIEW_DATE_INVOICE, firstInstallment.paymentTermId);
				getListPaymentOption(firstInstallment.billingMethodId);
				configPayment(endorsement.paymentTermId);
			}
			else if(paymentFormVO.invoiceHolder){
				onCleanForms();
				
				var installment:Installment = new Installment();
				paymentForm.cmbBillingMethod.selectedItemId = paymentFormVO.invoiceHolder.billingMethodId;
				getListPaymentOption(paymentFormVO.invoiceHolder.billingMethodId);
				
				selectStatePayment(0, paymentFormVO.invoiceHolder.billingMethodId);
				
				installment.billingMethodId = paymentFormVO.invoiceHolder.billingMethodId;
				installment.bankNumber = paymentFormVO.invoiceHolder.bankNumber;
				installment.bankAgencyNumber = paymentFormVO.invoiceHolder.bankAgencyNumber;
				installment.bankAccountNumber = paymentFormVO.invoiceHolder.bankAccountNumber;
				
				if(paymentFormVO.invoiceHolder.billingMethodId){
					changeAllowedBillingMethod = !isInvoice;
					installment.paymentType = getBillingMethod(paymentFormVO.invoiceHolder.billingMethodId).paymentType;
					setPaymentForm(installment);
				}
			}
		}

		private function notificationPaymentValidate():void{
			var isValid:Boolean = validateForm();
			
			if(isValid){
				
				var mainPaymentType:int = productPaymentTermSelected.paymentTerm.firstPaymentType;
				if(productPaymentTermSelected.paymentTerm.nextPaymentType){
					mainPaymentType = productPaymentTermSelected.paymentTerm.nextPaymentType;
				}
				
				switch(mainPaymentType){
					case Constant.PAYMENT_TYPE_CHECK:
						break;
						
					case Constant.PAYMENT_TYPE_CC:
					case Constant.PAYMENT_TYPE_CP:
						fullAccessAccountInfo = getFullAccessAccount(int(paymentForm.txtBankNumber.text));
						fullAccessAccountInfo.bankNumber = paymentForm.txtBankNumber.text;
						fullAccessAccountInfo.agencyNumber = paymentForm.txtAgencyNumber.text;
						var maxCharsAgency:int = getFullAccessAccount(int(paymentForm.txtBankNumber.text)).maxCharsAgency;
						
						if(fullAccessAccountInfo.agencyContainsDigit){
							fullAccessAccountInfo.agencyNumber += paymentForm.txtDigitAgencyNumber.text;
							maxCharsAgency++;
						}
						
						fullAccessAccountInfo.agencyNumber = Utilities.padValue(fullAccessAccountInfo.agencyNumber, '0', maxCharsAgency, 'l');
						fullAccessAccountInfo.accountNumber = (paymentForm.txtAccountNumber.text + paymentForm.txtDigitAccountNumber.text).toUpperCase();
						fullAccessAccountInfo.accountNumber = Utilities.padValue(fullAccessAccountInfo.accountNumber, '0', getFullAccessAccount(int(paymentForm.txtBankNumber.text)).maxCharsAccount, 'l');
						
						proposalProxy.validateBankInfo(fullAccessAccountInfo);
						break;
						
					case Constant.PAYMENT_TYPE_CARD:
						break;
						
					case Constant.PAYMENT_TYPE_FINANCING:
					case Constant.PAYMENT_TYPE_FI:
					case Constant.PAYMENT_TYPE_OT:
						break;
						
					case Constant.PAYMENT_TYPE_BILLET:
					case Constant.PAYMENT_TYPE_BILLET_LOOSE:
					case Constant.PAYMENT_TYPE_CARNE:
					case Constant.PAYMENT_TYPE_TEF:
						setPaymentOptionToCalc(paymentForm.paymentOptionSelected);
						paymentFormVO.paymentOption = paymentForm.paymentOptionSelected;
						sendNotification(NotificationList.PAYMENT_FORM_VALIDATE_COMPLETED, isValid);
						break;
				}
			}
			else{
				sendNotification(NotificationList.PAYMENT_FORM_VALIDATE_COMPLETED, false);
			}
		}

		private function notificationValidateBankinfo(isValid:Boolean):void{
			if(isValid){
				setPaymentOptionToCalc(paymentForm.paymentOptionSelected);
				paymentFormVO.paymentOption = paymentForm.paymentOptionSelected;
			}
			
			sendNotification(NotificationList.PAYMENT_FORM_VALIDATE_COMPLETED, isValid);
		}

		private function notificationFaultValidateBankInfo(erro:String):void{
			Alert.buttonWidth = 90;
			Alert.show(erro.split(':')[1].toString(), ResourceManager.getInstance().getString("messages", 'warning'), Alert.OK, null, null, Utilities.warningMsg);
		}

		private function configPayment(paymentTermId:int):void{
			var installments:ArrayCollection = new ArrayCollection;
				installments.addItem(firstInstallment);
			
			productPaymentTermSelected = getProductPaymentTerm(paymentTermId, firstInstallment.billingMethodId);
			
			if(productPaymentTermSelected){
				
				if(productPaymentTermSelected.paymentTerm.firstPaymentType != productPaymentTermSelected.paymentTerm.nextPaymentType
						&& productPaymentTermSelected.paymentTerm.nextPaymentType > 0){
					installments.addItem(endorsement.installmentById(2));
				}
				
				for each(var inst:Installment in installments){
					setPaymentForm(inst);
				}
			}
			
			selectStatePayment(paymentTermId, firstInstallment.billingMethodId);
		}

		private function configFieldsByActionType():void{
			switch(actionType){
				case Constant.CHANGE_REGISTER:
					disableFields = [ paymentForm.cmbBillingMethod, paymentForm.dgPayment, paymentForm.txtAgencyNumber,
									  paymentForm.txtDigitAgencyNumber, paymentForm.txtAccountNumber, paymentForm.txtDigitAccountNumber,
									  paymentForm.txtCheckNumber, paymentForm.txtCardHolderName, paymentForm.txtCardNumber,
									  paymentForm.txtCardDateMonth, paymentForm.txtCardDateYear, paymentForm.txtOtherAccountNumber ];
					break;
					
				case Constant.CHANGE_TECHNICAL:
					disableFields = [];
					break;
			}
			Utilities.enabledFields(new ArrayCollection(disableFields), false);
		}

		private function fieldIsEnabled(field:Object):Boolean{
			for each(var object:Object in disableFields){
				if(object == field)
					return false;
			}
			return true;
		}

		/** Carrega os valores dos campos dos formularios quando a Endorsement possue Installments */
		private function verifyPayment(paymentTermId:int):void{
			onCleanForms();
			
			if(endorsement.installments && (endorsement.installments.length > 0) && (paymentTermId > 0) && (paymentObject > 0) && arrProductPaymentTerm && (arrProductPaymentTerm.length > 0)){
				configPayment(paymentTermId);
			}
			else if(endorsement.installments && (endorsement.id.endorsementId > 1) && (endorsement.paymentTermId != 0) && (endorsement.installments.length == 0) && (paymentObject > 0) && arrProductPaymentTerm && (arrProductPaymentTerm.length > 0)){
				proposalProxy.getInstallment(endorsement.id.contractId, endorsement.endorsedId, 1);
			}
			else if(endorsement.installments && (endorsement.installments.length > 0) && (endorsement.paymentTermId == 0) && firstInstallment && (paymentObject > 0) && arrProductPaymentTerm && (arrProductPaymentTerm.length > 0)){
				selectStatePayment(0, firstInstallment.billingMethodId);
			}
			else if(endorsement.issuanceType == Endorsement.ISSUANCE_TYPE_INVOICE && (endorsement.paymentTermId == 0) && (paymentObject > 0) && arrProductPaymentTerm && (arrProductPaymentTerm.length > 0)){
				notificationGetInstallment(null);
			}
		}

		private function enableDueDay(paymentOption:PaymentOption, enabled:Boolean):void{
			paymentForm.nmsDueDay.enabled = (enabled && paymentOption && (paymentOption.quantityInstallment > 1));
			if (paymentOption
					&& paymentOption.quantityInstallment <= 1)
				paymentForm.nmsDueDay.value = 0;
		}

		private function enableForm():void{
			var enabled:Boolean = ((endorsement.issuingStatus == Endorsement.ISSUING_STATUS_QUOTATION || actionType == Constant.CHANGE_TECHNICAL) && (!isInvoice || !paymentTermIdLoaded));
			
			changeAllowedBillingMethod = (enabled && changeAllowedBillingMethod );
			
			Utilities.enabledFields(new ArrayCollection([ paymentForm.cmbBillingMethod ]), changeAllowedBillingMethod);
			
			Utilities.enabledFields(new ArrayCollection([ paymentForm.dgPayment, paymentForm.txtAccountNumber, paymentForm.txtAgencyNumber,
															paymentForm.txtBankNumber, paymentForm.txtCardDateMonth, paymentForm.txtCardHolderName, paymentForm.txtCardNumber,
															paymentForm.txtCheckNumber, paymentForm.txtDigitAccountNumber, paymentForm.txtDigitAgencyNumber, paymentForm.txtOtherAccountNumber ]), enabled);
			
			configFieldsByActionType();
			
			enableDueDay(paymentForm.paymentOptionSelected, enabled);
		}

		private function setValidateAccount(billingMethodId:int):void{
			var billingMethod:BillingMethod = getBillingMethod(billingMethodId);
			paymentForm.txtAgencyNumber.maxChars = getFullAccessAccount(billingMethod.bankNumber).maxCharsAgency;
			paymentForm.vldAgencyNumber.maxLength = getFullAccessAccount(billingMethod.bankNumber).maxCharsAgency;
			paymentForm.txtAccountNumber.maxChars = (getFullAccessAccount(billingMethod.bankNumber).maxCharsAccount - 1);
			paymentForm.vldAccountNumber.maxLength = (getFullAccessAccount(billingMethod.bankNumber).maxCharsAccount - 1);
		}

		/** Seta a PaymentOption pra enviar pra calcular e criar as Installments */
		private function setPaymentOptionToCalc(paymentOption:PaymentOption):void{
			var billingMethod:BillingMethod = paymentForm.cmbBillingMethod.selectedItem as BillingMethod;
			
			paymentOption.paymentType = billingMethod.paymentType;
			paymentOption.billingMethodId = billingMethod.billingMethodId;
			
			paymentOption.bankAccountNumber = null;
			paymentOption.bankNumber = 0;
			paymentOption.bankAgencyNumber =  null;
			paymentOption.bankCheckNumber = null;
			paymentOption.cardHolderName = null;
			paymentOption.cardNumber = null;
			paymentOption.cardExpirationDate = 0;
			paymentOption.cardBrandType = 0;
			paymentOption.cardType = 0;
			paymentOption.otherAccountNumber = null;
			paymentOption.dueDay = paymentForm.nmsDueDay.value;
			
			productPaymentTermSelected = getProductPaymentTerm(paymentOption.paymentId);
			
			var mainPaymentType:int = productPaymentTermSelected.paymentTerm.firstPaymentType;
			if(productPaymentTermSelected.paymentTerm.nextPaymentType){
				mainPaymentType = productPaymentTermSelected.paymentTerm.nextPaymentType;
			}
			
			switch(mainPaymentType){
				case Constant.PAYMENT_TYPE_TEF:
					switch(productPaymentTermSelected.paymentTerm.nextPaymentType){
						case Constant.PAYMENT_TYPE_CHECK:
							paymentOption.bankCheckNumber = paymentForm.txtCheckNumber.text;
						case Constant.PAYMENT_TYPE_CC:
						case Constant.PAYMENT_TYPE_CP:
							if(paymentObject == BillingMethod.PAYMENT_OBJECT_BRAZIL){
								paymentOption.bankNumber = int(fullAccessAccountInfo.bankNumber);
								paymentOption.bankAgencyNumber = fullAccessAccountInfo.agencyNumber;
								paymentOption.bankAccountNumber = fullAccessAccountInfo.accountNumber;
							}
							else{
								paymentOption.bankAccountNumber = paymentForm.txtAccountNumber.text;
								paymentOption.bankNumber = int(paymentForm.txtAccountNumber.text.substring(0, 4));
								paymentOption.bankAgencyNumber = paymentForm.txtAccountNumber.text.substring(4, 8);
							}
							break;
							
						case Constant.PAYMENT_TYPE_CARD:
							paymentOption.cardHolderName = paymentForm.txtCardHolderName.text;
							paymentOption.cardNumber = paymentForm.txtCardNumber.text;
							paymentOption.cardExpirationDate = (paymentForm.txtCardDateMonth.value + (paymentForm.txtCardDateYear.value * 100));
							paymentOption.cardBrandType = billingMethod.cardBrandType;
							paymentOption.cardType = billingMethod.cardType;
							break;
							
						case Constant.PAYMENT_TYPE_FINANCING:
						case Constant.PAYMENT_TYPE_FI:
						case Constant.PAYMENT_TYPE_OT:
							paymentOption.bankNumber = billingMethod.bankNumber;
							paymentOption.otherAccountNumber = paymentForm.txtOtherAccountNumber.text;
							break;
							
						case Constant.PAYMENT_TYPE_TEF:
						default:
							break;
					}
					break;
					
				case Constant.PAYMENT_TYPE_CHECK:
					paymentOption.bankCheckNumber = paymentForm.txtCheckNumber.text;
				case Constant.PAYMENT_TYPE_CC:
				case Constant.PAYMENT_TYPE_CP:
					if(paymentObject == BillingMethod.PAYMENT_OBJECT_BRAZIL){
						paymentOption.bankNumber = int(fullAccessAccountInfo.bankNumber);
						paymentOption.bankAgencyNumber = fullAccessAccountInfo.agencyNumber;
						paymentOption.bankAccountNumber = fullAccessAccountInfo.accountNumber;
					}
					else{
						paymentOption.bankAccountNumber = paymentForm.txtAccountNumber.text;
						paymentOption.bankNumber = int(paymentForm.txtAccountNumber.text.substring(0, 4));
						paymentOption.bankAgencyNumber =  paymentForm.txtAccountNumber.text.substring(4, 8);
					}
					break;
					
				case Constant.PAYMENT_TYPE_CARD:
					paymentOption.cardHolderName = paymentForm.txtCardHolderName.text;
					paymentOption.cardNumber = paymentForm.txtCardNumber.text;
					paymentOption.cardExpirationDate = (paymentForm.txtCardDateMonth.value + (paymentForm.txtCardDateYear.value * 100));
					paymentOption.cardBrandType = billingMethod.cardBrandType;
					paymentOption.cardType = billingMethod.cardType;
					break;
					
				case Constant.PAYMENT_TYPE_FINANCING:
				case Constant.PAYMENT_TYPE_FI:
				case Constant.PAYMENT_TYPE_OT:
					paymentOption.bankNumber = billingMethod.bankNumber;
					paymentOption.otherAccountNumber = paymentForm.txtOtherAccountNumber.text;
					break;
				break;
			}
		}

		private function setPaymentForm(installment:Installment):void{
			if(!installment)
				return;
			
			paymentForm.cmbBillingMethod.selectedItemId = installment.billingMethodId;
			switch(installment.paymentType){
				case Constant.PAYMENT_TYPE_CHECK:
					paymentForm.txtCheckNumber.text = installment.bankCheckNumber;
				case Constant.PAYMENT_TYPE_CC:
				case Constant.PAYMENT_TYPE_CP:
					if(paymentObject == BillingMethod.PAYMENT_OBJECT_BRAZIL){
						var billingMethod:BillingMethod = getBillingMethod(installment.billingMethodId);
						var fullAccess:FullAccessAccountInfo = getFullAccessAccount(billingMethod.bankNumber);
						var agencyNumber:String = String(installment.bankAgencyNumber);
						
						if(fullAccess.agencyContainsDigit){
							agencyNumber = agencyNumber.substr(0, (agencyNumber.length - 1));
						}
						paymentForm.txtBankNumber.text = Utilities.padValue(billingMethod.bankNumber, '0', 3, 'l');
						paymentForm.txtAgencyNumber.text = Utilities.padValue(agencyNumber,'0', fullAccess.maxCharsAgency, 'l');
						paymentForm.txtDigitAgencyNumber.text = String(installment.bankAgencyNumber).substr(String(installment.bankAgencyNumber).length - 1);
						paymentForm.txtAccountNumber.text = Utilities.padValue(installment.bankAccountNumber.substr(0, installment.bankAccountNumber.length - 1),'0', getFullAccessAccount(int(paymentForm.txtBankNumber.text)).maxCharsAccount - 1, 'l');
						paymentForm.txtDigitAccountNumber.text = installment.bankAccountNumber.substr(installment.bankAccountNumber.length-1).toUpperCase();
						setValidateAccount(installment.billingMethodId);
					}
					else{
						paymentForm.txtAccountNumber.text = installment.bankAccountNumber;
					}
					break;
					
				case Constant.PAYMENT_TYPE_CARD:
					setCardHolderName(installment.cardHolderName);
					paymentForm.txtCardNumber.text = installment.cardNumber;
					cardExpirationDate = installment.cardExpirationDate;
					break;
					
				case Constant.PAYMENT_TYPE_FINANCING:
				case Constant.PAYMENT_TYPE_FI:
				case Constant.PAYMENT_TYPE_OT:
					paymentForm.txtBankNumber.text = installment.bankNumber.toString();
					paymentForm.txtOtherAccountNumber.text = installment.otherAccountNumber;
					break;
					
				case Constant.PAYMENT_TYPE_BILLET:
				case Constant.PAYMENT_TYPE_TEF:
					break;
			}
		}

		/** Seleciona o State da forma de pagamento escolhida */
		private function selectStatePayment(paymentTermId:int, billingMethodId:int = 0):void{
			productPaymentTermSelected = getProductPaymentTerm(paymentTermId, billingMethodId);
			var valueResize:int = 250;
			
			if(productPaymentTermSelected){
				var mainPaymentType:int = productPaymentTermSelected.paymentTerm.firstPaymentType;
				if(productPaymentTermSelected.paymentTerm.nextPaymentType){
					mainPaymentType = productPaymentTermSelected.paymentTerm.nextPaymentType;
				}
				
				switch(mainPaymentType){
					case Constant.PAYMENT_TYPE_TEF:
						switch(productPaymentTermSelected.paymentTerm.nextPaymentType){
							case Constant.PAYMENT_TYPE_CC:
							case Constant.PAYMENT_TYPE_CP:
								if(paymentObject == BillingMethod.PAYMENT_OBJECT_BRAZIL){
									paymentForm.currentState = (getFullAccessAccount(getBillingMethod(billingMethodId).bankNumber).agencyContainsDigit ? 'CORRENT_ACCOUNT_AGENCY_DIGIT_BR' : 'CORRENT_ACCOUNT_BR');
									paymentForm.cmbBillingMethod.selectedItemId = billingMethodId;
									paymentForm.txtBankNumber.text = Utilities.padValue(getBillingMethod(billingMethodId).bankNumber, '0', 3, 'l');
									setValidateAccount(billingMethodId);
								}
								else{
									paymentForm.currentState = 'CORRENT_ACCOUNT_VE';
								}
								valueResize = 380;
								break;
								
							case Constant.PAYMENT_TYPE_CARD:
								paymentForm.currentState = 'CREDIT_CARD';
								valueResize = 380;
								configCreditCard();
								break;
								
							case Constant.PAYMENT_TYPE_CHECK:
								paymentForm.currentState = 'CHECK_VE';
								break;
								
							case Constant.PAYMENT_TYPE_FINANCING:
							case Constant.PAYMENT_TYPE_FI:
							case Constant.PAYMENT_TYPE_OT:
								paymentForm.currentState = 'OTHER_ACCOUNTS';
								valueResize = 380;
								break;
								
							case Constant.PAYMENT_TYPE_BILLET:
							case Constant.PAYMENT_TYPE_BILLET_LOOSE:
							case Constant.PAYMENT_TYPE_CARNE:
							case Constant.PAYMENT_TYPE_TEF:
							default: 
								paymentForm.currentState = 'PAYMENT_OPTION'; 
								valueResize = 250;
								break;
						}
						break;
						
					case Constant.PAYMENT_TYPE_CC:
					case Constant.PAYMENT_TYPE_CP:
						if(paymentObject == BillingMethod.PAYMENT_OBJECT_BRAZIL){
							paymentForm.currentState = (getFullAccessAccount(getBillingMethod(billingMethodId).bankNumber).agencyContainsDigit ? 'CORRENT_ACCOUNT_AGENCY_DIGIT_BR' : 'CORRENT_ACCOUNT_BR');
							paymentForm.cmbBillingMethod.selectedItemId = billingMethodId;
							paymentForm.txtBankNumber.text = Utilities.padValue(getBillingMethod(billingMethodId).bankNumber, '0', 3, 'l');
							setValidateAccount(billingMethodId);
						}
						else{
							paymentForm.currentState = 'CORRENT_ACCOUNT_VE';
						}
						valueResize = 380;
						break;
						
					case Constant.PAYMENT_TYPE_CARD:
						paymentForm.currentState = 'CREDIT_CARD';
						valueResize = 380;
						configCreditCard();
						break;
						
					case Constant.PAYMENT_TYPE_CHECK:
						paymentForm.currentState = 'CHECK_VE';
						break;
						
					case Constant.PAYMENT_TYPE_FINANCING:
					case Constant.PAYMENT_TYPE_FI:
					case Constant.PAYMENT_TYPE_OT:
						paymentForm.currentState = 'OTHER_ACCOUNTS';
						valueResize = 380;
						break;
						
					case Constant.PAYMENT_TYPE_BILLET:
					case Constant.PAYMENT_TYPE_BILLET_LOOSE:
					case Constant.PAYMENT_TYPE_CARNE:
					case Constant.PAYMENT_TYPE_TEF:
					default: 
						paymentForm.currentState = 'PAYMENT_OPTION'; 
						valueResize = 250;
						break;
				}
			}
			else{
				paymentForm.currentState = 'PAYMENT_OPTION'; 
				valueResize = 250;
			}
			
			sendNotification(NotificationList.PAYMENT_FORM_RESIZE, valueResize);
		}

		/** Define as regras de expiracao do cartao de credito */
		private function configCreditCard():void{
			var datePayment:Date = Utilities.sumDateMonth(new Date(), toleranceCardExpiration);
			
			paymentForm.txtCardDateYear.minimum = datePayment.getFullYear();
			paymentForm.txtCardDateYear.maximum = (datePayment.getFullYear() + 10);
			paymentForm.vldCardDateYear.minValue = paymentForm.txtCardDateYear.minimum;
			paymentForm.vldCardDateYear.maxValue = paymentForm.txtCardDateYear.maximum;
			
			if(endorsement.issuingStatus == Endorsement.ISSUING_STATUS_QUOTATION){
				paymentForm.txtCardDateMonth.value = (datePayment.getMonth() + 1);
				paymentForm.txtCardDateYear.value = datePayment.getFullYear();
			}
			else{
				paymentForm.txtCardDateMonth.value = (cardExpirationDate - ((int( cardExpirationDate / 100)) * 100));
				paymentForm.txtCardDateYear.value = (cardExpirationDate / 100);
			}
			
			setCardHolderName(cardHolderName);
		}

		/** Seta o numero do banco */
		private function setBankNumber():void{
			if(paymentForm.cmbBillingMethod.selectedItem){
				paymentForm.txtBankNumber.text = Utilities.padValue((paymentForm.cmbBillingMethod.selectedItem as BillingMethod).bankNumber, '0', 3, 'l');
			}
		}

		private function getBillingMethod(billingMethodId:int):BillingMethod{
			for each(var billingMethod:BillingMethod in paymentForm.arrBillingMethod){
				if(billingMethod.billingMethodId == billingMethodId){
					return billingMethod;
				}
			}
			throw new IllegalOperationError(ResourceManager.getInstance().getString('messages', 'dataNotFound'));
		}

		/** Retorna um Objeto ProductPaymentTerm */
		private function getProductPaymentTerm(paymentTermId:int, billingMethodId:int = 0):ProductPaymentTerm{
			for each(var productPaymentTerm:ProductPaymentTerm in arrProductPaymentTerm){
				if(paymentTermId > 0
						&& billingMethodId > 0){
					if(productPaymentTerm.id.paymentTermId == paymentTermId
							&& productPaymentTerm.id.billingMethodId == billingMethodId)
						return productPaymentTerm;
				}
				else if(productPaymentTerm.id.paymentTermId == paymentTermId){
					return productPaymentTerm;
				}
				else if(productPaymentTerm.id.billingMethodId == billingMethodId){
					return productPaymentTerm;
				}
			}
			return null;
		}

		private function notificationApplicationPropertyFinded(identified:IdentifiedList):void{
			if(identified && identified.nameComponent == NAME){
				switch(identified.groupId){
					case ApplicationProperty.TOLERANCE_TO_PAYMENT_CARD_EXPIRATION:
						toleranceCardExpiration = int(identified.value);
						break;
				}
			}
		}

		/** Recebe uma lista de BillingMethod vinculados ao produto e seta o combo caso a Endorsement possua installments */
		private function setListBillingMethod(pData:ArrayCollection):void{
			paymentForm.arrBillingMethod = pData;
			
			if(firstInstallment
					&& paymentForm.arrBillingMethod){
				paymentForm.cmbBillingMethod.selectedItemId = firstInstallment.billingMethodId;
			}
			
			if(paymentForm.arrBillingMethod.length > 0){
				paymentObject = (paymentForm.arrBillingMethod.getItemAt(0) as BillingMethod).paymentObject;
				paymentObject ||= BillingMethod.PAYMENT_OBJECT_BRAZIL;
				
				verifyPayment(endorsement.paymentTermId);
			}
		}

		/** Recebe uma lista de ProductPaymentTerm que sera usado pra verificar o FirstPaymentType e o NextPaymentType
		 *  pra selecionar os States do componente */
		private function setListProductPaymentTerm(pData:ArrayCollection):void{
			arrProductPaymentTerm = pData;
			if(arrProductPaymentTerm){
				verifyPayment(endorsement.paymentTermId);
			}
		}

		/** Recebe uma lista contendo PaymentOption para preecher o DataGrid e o usuario escolher a frequencia de pagamento e
		 *  selecionar o checkbox caso a endorsement ja contenha alguma frequencia de pagamento selecionada */
		private function setListPaymentOption(pData:ArrayCollection):void{
			paymentForm.arrPaymentOption = pData;
			if(!paymentForm.arrPaymentOption
					|| paymentForm.arrPaymentOption.length == 0){
				Utilities.warningMessage('dueDateOrEffectiveOutOfTime');
				return;
			}
			
			if(!paymentTermIdLoaded
					&& (endorsement.paymentTermId > 0
							|| paymentForm.arrPaymentOption.length == 1)){
				for each(var paymentOption:PaymentOption in paymentForm.arrPaymentOption){
					paymentOption.selected = false;
					if(endorsement.paymentTermId == paymentOption.paymentId
							|| paymentForm.arrPaymentOption.length == 1){
						paymentOption.selected = true;
						paymentTermIdLoaded = true;
						paymentForm.paymentOptionSelected = paymentOption;
						sendNotification(NotificationList.PREVIEW_DATE_INVOICE, paymentOption.paymentId);
						break;
					}
				}
			}
			
			enableForm();
		}

		/** Seta o nome do titular do cartao de credito */
		private function setCardHolderName(pData:String):void{
			cardHolderName = pData;
			paymentForm.txtCardHolderName.text = cardHolderName.substring(0, 30);
		}

		/** Solicita a lista de PaymentOption para o usuario escolher a frequencia de pagamento */
		private function getListPaymentOption(billingMethodId:int):void{
			proposalProxy.listPaymentOption(endorsement.totalPremium, endorsement.netPremium,
											endorsement.policyCost, endorsement.fractioningAdditional,
											endorsement.taxValue, endorsement.taxRate,
											endorsement.contract.productId, billingMethodId,
											endorsement.referenceDate, endorsement.effectiveDate, 
											endorsement.expiryDate, paymentForm.nmsDueDay.value,
											endorsement.getMainItem().renewalType,
											endorsement.getMainItem().renewalInsurerId, endorsement.issuanceType,
											(isBilled ? ProductPaymentTerm.PAYMENT_TERM_TYPE_INVOICE : ProductPaymentTerm.PAYMENT_TERM_TYPE_POLICY),
											invoiceType);
		}

		/** Invocando quando o usuario seleciona uma forma de pagamento */
		private function handleBillingMethodSelected(event:ListEvent):void{
			onCleanForms();
			
			if(paymentForm.cmbBillingMethod.selectedItemId > 0){
				getListPaymentOption(paymentForm.cmbBillingMethod.selectedItemId);
				
				selectStatePayment(0, paymentForm.cmbBillingMethod.selectedItemId);
			}
			else{
				paymentForm.paymentOptionSelected = null;
				paymentForm.arrPaymentOption = null;
				paymentForm.nmsDueDay.value = 0;
			}
		}

		/** Adiciona as acoes dos componentes */
		private function createListeners():void{
			paymentForm.addEventListener(PaymentForm.PREVIEW_DATE_INVOICE, previewDateInvoice);
			paymentForm.cmbBillingMethod.addEventListener(ListEvent.CHANGE, handleBillingMethodSelected, false, 0, true);
		}

		/** Remove as acaos dos componentes */
		private function destroyListeners():void{
			paymentForm.removeEventListener(PaymentForm.PREVIEW_DATE_INVOICE, previewDateInvoice);
			paymentForm.cmbBillingMethod.removeEventListener(ListEvent.CHANGE, handleBillingMethodSelected);
		}

		private function previewDateInvoice(event:Event):void{
			if(paymentForm.paymentOptionSelected != null)
				sendNotification(NotificationList.PREVIEW_DATE_INVOICE, paymentForm.paymentOptionSelected.paymentId);
			else
				sendNotification(NotificationList.PREVIEW_DATE_INVOICE, null);
			
			enableDueDay(paymentForm.paymentOptionSelected, true);
		}

		public function getFullAccessAccount(bankNumber:int):FullAccessAccountInfo{
			if(!_dictBank){
				_dictBank = new Dictionary;
				
				var fullAccess:FullAccessAccountInfo = new FullAccessAccountInfo;
				fullAccess.bankNumber = String(BillingMethod.BANK_NUMBER_BRASIL);
				fullAccess.maxCharsAgency = 4;
				fullAccess.maxCharsAccount = 9;
				fullAccess.agencyContainsDigit = true;
				_dictBank[BillingMethod.BANK_NUMBER_BRASIL] = fullAccess;
				
				fullAccess = new FullAccessAccountInfo;
				fullAccess.bankNumber = String(BillingMethod.BANK_NUMBER_SANTANDER);
				fullAccess.maxCharsAgency = 4;
				fullAccess.maxCharsAccount = 9;
				_dictBank[BillingMethod.BANK_NUMBER_SANTANDER] = fullAccess;
				
				fullAccess = new FullAccessAccountInfo;
				fullAccess.bankNumber = String(BillingMethod.BANK_NUMBER_BANESE);
				fullAccess.maxCharsAgency = 3;
				fullAccess.maxCharsAccount = 8;
				_dictBank[BillingMethod.BANK_NUMBER_BANESE] = fullAccess;
				
				fullAccess = new FullAccessAccountInfo;
				fullAccess.bankNumber = String(BillingMethod.BANK_NUMBER_BRADESCO);
				fullAccess.maxCharsAgency = 4;
				fullAccess.maxCharsAccount = 8;
				fullAccess.agencyContainsDigit = true;
				_dictBank[BillingMethod.BANK_NUMBER_BRADESCO] = fullAccess;
				
				fullAccess = new FullAccessAccountInfo;
				fullAccess.bankNumber = String(BillingMethod.BANK_NUMBER_ITAU_UNIBANCO);
				fullAccess.maxCharsAgency = 4;
				fullAccess.maxCharsAccount = 6;
				_dictBank[BillingMethod.BANK_NUMBER_ITAU_UNIBANCO] = fullAccess;
				
				fullAccess = new FullAccessAccountInfo;
				fullAccess.bankNumber = String(BillingMethod.BANK_NUMBER_HSBC);
				fullAccess.maxCharsAgency = 4;
				fullAccess.maxCharsAccount = 7;
				_dictBank[BillingMethod.BANK_NUMBER_HSBC] = fullAccess;
				
				fullAccess = new FullAccessAccountInfo;
				fullAccess.bankNumber = String(BillingMethod.BANK_NUMBER_SAFRA);
				fullAccess.maxCharsAgency = 4;
				fullAccess.maxCharsAccount = 7;
				_dictBank[BillingMethod.BANK_NUMBER_SAFRA] = fullAccess;
				
				fullAccess = new FullAccessAccountInfo;
				fullAccess.bankNumber = String(BillingMethod.BANK_NUMBER_BANCOOB);
				fullAccess.maxCharsAgency = 4;
				fullAccess.maxCharsAccount = 10;
				fullAccess.agencyContainsDigit = true;
				_dictBank[BillingMethod.BANK_NUMBER_BANCOOB] = fullAccess;
			}
			
			return _dictBank[bankNumber] as FullAccessAccountInfo;
		}

		/** Limpa os componentes da tela e reinicia variaveis de apoio */
		private function onClean():void{
			paymentForm.arrBillingMethod = null;
			paymentForm.arrPaymentOption = null;
			paymentForm.cmbBillingMethod.selectedItemId = 0;
			paymentForm.cmbBillingMethod.selectedItem = null;
			paymentForm.paymentOptionSelected = null;
			paymentForm.nmsDueDay.value = 0;
			fullAccessAccountInfo = null;
			arrProductPaymentTerm = null;
			paymentFormVO = null;
			firstInstallment = null;
			paymentTermIdLoaded = false;
			changeAllowedBillingMethod = true;
			paymentObject = 0;
			cardExpirationDate = 0;
			cardHolderName = '';
			validatorPayment = [];
			
			onCleanForms();
			
			paymentForm.currentState = 'PAYMENT_OPTION';
			
			sendNotification(NotificationList.PAYMENT_FORM_RESIZE, 250);
			
			destroyListeners();
			
			Utilities.enabledFields(new ArrayCollection(disableFields));
		}

		/** Limpa os dados dos formularios */
		private function onCleanForms():void{
			Utilities.resetForm(paymentForm.frmCorrentAccount);
			Utilities.resetForm(paymentForm.frmAccount);
			Utilities.resetForm(paymentForm.frmOtherForms);
			Utilities.resetField([paymentForm.txtDigitAgencyNumber, paymentForm.txtDigitAccountNumber]);
		}

		public function validateForm():Boolean{
			// Verifica se foi selecionada alguma forma de  pagamento ex: Conta Corrente, Boleto, Cheque e  etc...
			if(!Utilities.validateForm([paymentForm.vldPaymentType], ResourceManager.getInstance().getString('resources', 'paymentList'))){
				return false;
			}
			
			// Verifica se foi selecionado alguma opcao de pagamento ex: A vista, 1 + 2 e etc...
			if(!paymentForm.paymentOptionSelected){
				Utilities.warningMessage("pleaseSelectedPayment");
				return false;
			}
			
			// Verifica os campos de preenchimento obrigatorio ex: Agencia, Conta, Digito e etc...
			if(!Utilities.validateForm(validatorPayment, ResourceManager.getInstance().getString('resources', 'paymentList'))){
				return false;
			}
			
			return true;
		}

		private function unlockProposalResult(isUnlocked:Boolean):void{
			if (isUnlocked){
				enableForm();
			}
		}
	}
}