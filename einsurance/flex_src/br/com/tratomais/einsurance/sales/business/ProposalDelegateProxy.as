package br.com.tratomais.einsurance.sales.business
{  
    import br.com.tratomais.einsurance.core.UtilitiesService;
    import br.com.tratomais.einsurance.customer.model.vo.Customer;
    import br.com.tratomais.einsurance.policy.model.vo.Endorsement;
    import br.com.tratomais.einsurance.products.model.vo.PaymentOption;
    import br.com.tratomais.einsurance.sales.model.vo.FullAccessAccountInfo;
    
    import mx.rpc.IResponder;  
      
    public class ProposalDelegateProxy  
    {  
        private var responder : IResponder;  
        private var service : Object;  
          
        public function ProposalDelegateProxy(pResponder : IResponder )  
        {  
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_PROPOSAL);
			responder = pResponder;
        } 
        
 		public function listAllActivity( ):void{  
            var call:Object = service.listAllActivity( );  
            call.addResponder(responder);  
        } 
         
 		public function listAllOccupation( ):void{  
            var call:Object = service.listAllOccupation( );  
            call.addResponder(responder);  
        }
        
        public function listAllRestrictionRiskTypes(): void {
            var call:Object = service.findDataOptionByDataGroup( 84 );
            call.addResponder(responder);
        }

		public function listAllProfession():void{
            var call:Object = service.listAllProfession( );  
            call.addResponder(responder); 			
		}
		
		public function listAllInflow(inflowType:String):void{
            var call:Object = service.listAllInflow(inflowType);  
            call.addResponder(responder); 			
		}		

		public function buscarEndorsement( endorsementId :int ):void{
            var call:Object = service.buscarEndorsement( endorsementId );  
            call.addResponder(responder); 			
		}
		
		public function saveEndorsement( endorsement:Endorsement ):void{
		
            var call:Object = service.saveEndorsement( endorsement );  
            call.addResponder(responder); 			
		}
		
		public function findPersonalRiskOptions(productId:int, riskPlanId:int, referenceDate:Date):void
		{
			var call:Object = service.findPersonalRiskOptions(productId, riskPlanId, referenceDate);
			call.addResponder(responder);
		}

		public function saveCustomer( customer:Customer ):void{
			
			var call:Object = service.saveCustomer( customer );
			call.addResponder(responder);
		}
		
		public function listPaymentOption(totalPremium: Number, netPremium: Number, policyCost:Number, fractioningAdditional:Number, taxValue:Number, taxRate:Number, productId:int, billingMethod:int, refDate:Date, startDate:Date, finishDate:Date, dueDay:int, renewalType:int, renewalInsurerId:int, issuanceType:int, paymentTermType:int, invoiceType:int, firstPaymentType:int = 0, qtInstallment:int = 0, proposalDate:Date = null):void{
			var call:Object = service.getPaymentOptionsByPaymentType(totalPremium, netPremium, policyCost, fractioningAdditional, taxValue, taxRate, productId, billingMethod, refDate, startDate, finishDate, dueDay, renewalType, renewalInsurerId, issuanceType, paymentTermType, invoiceType, firstPaymentType, qtInstallment, proposalDate);
			call.addResponder( responder );		
 		}
 		
 		public function effectiveProposal(endorsement : Endorsement, paymentOption: PaymentOption):void{
			var call:Object = service.effectiveProposal(endorsement, paymentOption);
			call.addResponder( responder );		
 		}   

 		public function loadUnlockableEndorsement( endorsementId :int , contractId: int):void{
            var call:Object = service.loadUnlockableEndorsement( endorsementId, contractId );  
            call.addResponder(responder); 			
		}
		
		public function unlockEndorsement (endorsementId : int, contractId : int) : void {
			var call : Object = service.unlockEndorsement(endorsementId, contractId);
			call.addResponder(responder);
		}     
		
		public function policyChangeRegister(endorsement:Endorsement, changeDate:Date):void{
			var call:Object = service.policyChangeRegister(endorsement, changeDate);
			call.addResponder(responder);
		}
		
		public function policyChangeTechnical(endorsement:Endorsement, changeDate:Date, isRecalculation:Boolean, isProposal:Boolean, paymentOption:PaymentOption):void{
			var call:Object = service.policyChangeTechnical(endorsement, changeDate, isRecalculation, isProposal, paymentOption);
			call.addResponder(responder);
		}
		
   		public function cancellationProposal(endorsementId : int, contractId : int, reason : int) : void {
			var call : Object = service.cancellationProposal(endorsementId, contractId, reason);
			call.addResponder(responder);
   		}
 
		public function getInstallment(contractId:int, endorsementId:int, installmentId:int):void{
			var call:Object = service.getInstallment(contractId, endorsementId, installmentId);
			call.addResponder(responder);
		}
		
		public function validateBankInfo(fullAccessAccountInfo:FullAccessAccountInfo):void{
			var call:Object = service.validateBankInfo(fullAccessAccountInfo);
			call.addResponder(responder);
		}
		
		/**
		 * Returns the number of occurrences for valid contracts
		 * @param productId Product Identificator
		 * @param docNumber Document number
		 * @param docType Document type
		 * @paran dateIni Beggining date
		 * @param dateFin Ending date
		 * @param contractId Contract identificator to be excluded from query
		 * @return number of occurrences for valid contracts
		 */
		 public function getQuantityOfContracts(productId:int, docNumber:String, docType:int,dateIni:Date, dateFin:Date, contractId:int):void {
            var call:Object = service.numActivePoliciesPerProductDocument(productId, docNumber, docType, dateIni, dateFin, contractId);
            call.addResponder(responder);  
 		}    
 		
		/**
		 * Call contract validation info 
		 * @param productId Product Identificator
		 * @param docNumber Document number
		 * @param docType Document type
		 * @paran dateIni Beggining date
		 * @param referenceDate Reference Date
		 * @param contractId Contract identificator to be excluded from query
		 */
		 public function validateContract(productId:int, docNumber:String, docType:int, dateIni:Date, referenceDate:Date, contractId:int):void {
            var call:Object = service.validateContract(productId, docNumber, docType, dateIni, referenceDate, contractId);
            call.addResponder(responder);  
 		}
 		
 		/**
 		 * Returns the payment form
		 * @param productId Product Identificator
		 * @param referenceDate
 		 * */
 		public function listPaymentType(productId:int,  paymentTermType:int, referenceDate:Date):void{
			var call:Object =  service.listPaymentType(productId, paymentTermType, referenceDate);		
			call.addResponder(responder); 	
		}
 	}  
}


