package br.com.tratomais.einsurance.sales.business
{  
    import br.com.tratomais.einsurance.core.UtilitiesService;
    import br.com.tratomais.einsurance.policy.model.vo.Endorsement;
    
    import mx.collections.ArrayCollection;
    import mx.rpc.IResponder;  
      
    public class CalculateDelegateProxy  
    {  
        private var responder : IResponder;  
        private var service : Object;  
          
        public function CalculateDelegateProxy(pResponder : IResponder )  
        {  
			service = UtilitiesService.getInstance().getService(UtilitiesService.SERVICE_CALC);
			responder = pResponder;
        }     
      
 		public function simpleCalc(endorsement : Endorsement):void
        {  
            var call:Object = service.simpleCalc(endorsement);  
            call.addResponder(responder);  
        }  

 		public function calculate(endorsement : Endorsement, objectId:int, coverageOption:ArrayCollection):void
        {  
            var call:Object = service.calculate(endorsement, objectId, coverageOption);  
            call.addResponder(responder);  
        }  
 
 		public function lifeCycleCalc(endorsement : Endorsement, coverageOption:ArrayCollection):void
        {  
            var call:Object = service.lifeCycleCalc(endorsement, coverageOption);  
            call.addResponder(responder);  
        } 
		
		public function getPaymentOptionsDefault( endorsement:Endorsement ):void
		{
			var call:Object =  service.getPaymentOptionsDefault(endorsement);		
			call.addResponder(responder);
		}        
    }  
}