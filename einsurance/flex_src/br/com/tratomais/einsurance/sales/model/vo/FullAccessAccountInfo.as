package br.com.tratomais.einsurance.sales.model.vo
{
	import net.digitalprimates.persistence.hibernate.HibernateBean;

	[RemoteClass(alias="br.com.tratomais.validation.bank.FullAcessAccountInfo")]
	[Managed]
	public class FullAccessAccountInfo extends HibernateBean
	{
		private var _bankNumber:String;
		private var _agencyNumber:String;
		private var _accountNumber:String;
		private var _maxCharsAgency:int;
		private var _agencyContainsDigit:Boolean;
		private var _maxCharsAccount:int;
		private var _bankAccountNumberDigit:Boolean;

		public function get bankNumber():String{
			return _bankNumber;
		}

		public function set bankNumber(value:String):void{
			_bankNumber = value;
		}

		public function get agencyNumber():String{
			return _agencyNumber;
		}

		public function set agencyNumber(value:String):void{
			_agencyNumber = value;
		}

		public function get accountNumber():String{
			return _accountNumber;
		}

		public function set accountNumber(value:String):void{
			_accountNumber = value;
		}

		public function get maxCharsAgency():int{
			return _maxCharsAgency;
		}

		public function set maxCharsAgency(value:int):void{
			_maxCharsAgency = value;
		}

		public function get maxCharsAccount():int{
			return _maxCharsAccount;
		}

		public function set maxCharsAccount(value:int):void{
			_maxCharsAccount = value;
		}

		public function get agencyContainsDigit():Boolean{
			return _agencyContainsDigit;
		}

		public function set agencyContainsDigit(value:Boolean):void{
			_agencyContainsDigit = value;
		}

		public function get bankAccountNumberDigit():Boolean{
			return _bankAccountNumberDigit;
		}

		public function set bankAccountNumberDigit(pData:Boolean):void{
			_bankAccountNumberDigit=pData;
		}
	}
}