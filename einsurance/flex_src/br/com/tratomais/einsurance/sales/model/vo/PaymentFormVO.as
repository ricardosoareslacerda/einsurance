package br.com.tratomais.einsurance.sales.model.vo
{
	import br.com.tratomais.einsurance.policy.model.vo.Endorsement;
	import br.com.tratomais.einsurance.policy.model.vo.InvoiceHolder;
	import br.com.tratomais.einsurance.products.model.vo.PaymentOption;

	public class PaymentFormVO
	{
		[Bindable] private var _endorsement:Endorsement;
		[Bindable] private var _paymentOption:PaymentOption;
		private var _invoiceHolder:InvoiceHolder;
		private var _actionType:String;

		public function get endorsement():Endorsement{
			return _endorsement;
		}

		public function set endorsement(pData:Endorsement):void{
			_endorsement = pData;
		}

		public function get paymentOption():PaymentOption{
			return _paymentOption;
		}

		public function set paymentOption(pData:PaymentOption):void{
			_paymentOption = pData;
		}

		public function get invoiceHolder():InvoiceHolder{
			return _invoiceHolder;
		}

		public function set invoiceHolder(pData:InvoiceHolder):void{
			_invoiceHolder = pData;
		}

		public function get actionType():String{
			return _actionType;
		}

		public function set actionType(pData:String):void{
			_actionType = pData;
		}
	}
}