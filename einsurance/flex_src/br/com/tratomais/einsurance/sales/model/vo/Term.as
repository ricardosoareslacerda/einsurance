package br.com.tratomais.einsurance.sales.model.vo
{
	import br.com.tratomais.einsurance.core.Utilities;

	import net.digitalprimates.persistence.hibernate.HibernateBean;

	[RemoteClass(alias="br.com.tratomais.core.model.product.Term")]
	[Managed]
	public class Term extends HibernateBean
	{
		/**
		 * Constantes
		 */
		public static const DEFAULT:int = 1; // Anual
		public static const PRORATA:int = 2; // Pró-Rata

		/**
		 * Multiple Type
		 */
		public static const MULTIPLE_TYPE_YEAR:int = 60;	// Ano
		public static const MULTIPLE_TYPE_MONTH:int = 61;	// Mes
		public static const MULTIPLE_TYPE_DAY:int = 62;		// Dia

		/**
		 * Invoice Type
		 */
		public static const INVOICE_TYPE_FLEXIBLE:int = 824;	//Fatura Flexível

		/**
		 * Variables
		 */
		private var _termId:int;
		private var _termCode:String;
		private var _name:String;
		private var _invoiceType:int;
		private var _multipleType:int;
		private var _multipleQuantity:int;
		private var _modifyPermit:Boolean;
		private var _maximumQuantity:int;
		private var _registred:Date;
		private var _updated:Date;

		/**
		 * Transients
		 */
		private var _beginMonth:Boolean;

		public function Term(termId:int = 0){
			_termId = termId;
		}

		public function get termId():int{
			return _termId;
		}

		public function set termId(pData:int):void{
			_termId = pData;
		}

		public function get termCode():String{
			return _termCode;
		}

		public function set termCode(pData:String):void{
			_termCode = pData;
		}

		public function get name():String{
			return _name;
		}

		public function set name(pData:String):void{
			_name = pData;
		}

		public function get invoiceType():int{
			return _invoiceType;
		}

		public function set invoiceType(pData:int):void{
			_invoiceType = pData;
		}

		public function get multipleType():int{
			return _multipleType;
		}

		public function set multipleType(pData:int):void{
			_multipleType = pData;
		}

		public function get multipleQuantity():int{
			return _multipleQuantity;
		}

		public function set multipleQuantity(pData:int):void{
			_multipleQuantity = pData;
		}

		public function get modifyPermit():Boolean{
			return _modifyPermit;
		}

		public function set modifyPermit(pData:Boolean):void{
			_modifyPermit = pData;
		}

		public function get maximumQuantity():int{
			return _maximumQuantity;
		}

		public function set maximumQuantity(pData:int):void{
			_maximumQuantity = pData;
		}

		public function get registred():Date{
			return _registred;
		}

		public function set registred(pData:Date):void{
			_registred = Utilities.convertTimeZone(pData);
		}

		public function get updated():Date{
			return _updated;
		}

		public function set updated(pData:Date):void{
			_updated = Utilities.convertTimeZone(pData);
		}

		public function get beginMonth():Boolean{
			return _beginMonth;
		}

		public function set beginMonth(pData:Boolean):void{
			_beginMonth = pData;
		}
	}
}