package br.com.tratomais.einsurance.sales.model.proxy
{
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.policy.model.vo.Endorsement;
    import br.com.tratomais.einsurance.products.model.vo.SimpleCalcResult;
    import br.com.tratomais.einsurance.sales.business.CalculateDelegateProxy;
    
    import mx.collections.ArrayCollection;
    import mx.rpc.Responder;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    
    import org.puremvc.as3.interfaces.IProxy;
    import org.puremvc.as3.patterns.proxy.Proxy;  

    [Bindable]  
    public class CalculateProxy extends Proxy implements IProxy   
    {  
      
        public static const NAME:String = "CalculateProxy"; 
    	
        public function CalculateProxy( data:Object = null)  
        {  
            super(NAME, data );  
        }  
 
 		public function simpleCalc(endorsement : Endorsement):void
		{
			var delegate:CalculateDelegateProxy = new CalculateDelegateProxy(new Responder(onCalcResult, onFault));  
			delegate.simpleCalc(endorsement);
 		}

 		public function calculate(endorsement : Endorsement, objectId:int, coverageOptionList:ArrayCollection):void
		{
			var delegate:CalculateDelegateProxy = new CalculateDelegateProxy(new Responder(onCalcResult, onFault));  
			delegate.calculate(endorsement, objectId, coverageOptionList);
 		}

 		public function lifeCycleCalc(endorsement : Endorsement, coverageDataOptionList:ArrayCollection):void
		{
			var delegate:CalculateDelegateProxy = new CalculateDelegateProxy(new Responder(onCalcResult, onFault));  
			delegate.lifeCycleCalc(endorsement, coverageDataOptionList);
 		}

		private function onCalcResult(pResultEvt:ResultEvent):void  {  
			var result:SimpleCalcResult = pResultEvt.result as SimpleCalcResult;
        	sendNotification(NotificationList.CALCULATED_QUOTATION, result);
        }        

		public function getPaymentOptionsDefault(endorsement:Endorsement):void
		{
			var delegate:CalculateDelegateProxy = new CalculateDelegateProxy(new Responder(onGetPaymentOptionsDefault, onFault));  
			delegate.getPaymentOptionsDefault(endorsement);				
		}
		
		private function onGetPaymentOptionsDefault(pResultEvt:ResultEvent):void
		{
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.PAYMENT_OPTION_LISTED, collection);
		}

		private function onFault(pFaultEvt:FaultEvent):void  {  
			if(pFaultEvt.fault.faultString.indexOf( "sessionExpired", 0) != -1 ) {
				sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
			}else{
				sendNotification(NotificationList.ERROR_CALCULATED_QUOTATION, pFaultEvt);  
			}
        }
    }  
}  