package br.com.tratomais.einsurance.sales.model.proxy
{
    import br.com.tratomais.einsurance.core.NotificationList;
    import br.com.tratomais.einsurance.core.Utilities;
    import br.com.tratomais.einsurance.customer.business.DataOptionDelegateProxy;
    import br.com.tratomais.einsurance.customer.model.vo.Customer;
    import br.com.tratomais.einsurance.policy.model.vo.Endorsement;
    import br.com.tratomais.einsurance.policy.model.vo.EndorsementOption;
    import br.com.tratomais.einsurance.policy.model.vo.Installment;
    import br.com.tratomais.einsurance.products.model.vo.PaymentOption;
    import br.com.tratomais.einsurance.sales.business.ProposalDelegateProxy;
    import br.com.tratomais.einsurance.sales.model.vo.FullAccessAccountInfo;
    
    import mx.collections.ArrayCollection;
    import mx.rpc.Responder;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    
    import org.puremvc.as3.interfaces.IProxy;
    import org.puremvc.as3.patterns.proxy.Proxy;  

    [Bindable]  
    public class ProposalProxy extends Proxy implements IProxy   
    {  

        public static const NAME:String = "ProposalProxy"; 
    	
        public function ProposalProxy( data:Object = null)  
        {  
            super(NAME, data );  
        }  
        
		[Embed(source="/assets/images/error.png")]
		public static var errorMsg:Class;
        
 		public function listPersonType( dataGroupId:int ):void{
 			var delegate:DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder( onListPersonType, onFault));
			delegate.findDataOptionByDataGroup( dataGroupId );
 		}
 						
		private function onListPersonType(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.PERSON_TYPE_LISTED, collection);
        }
                		
 		public function listAllInflowInsured(inflowType:String):void{
			var delegate:ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onListAllInflowInsured, onFault));  
			delegate.listAllInflow(inflowType); 		
 		}
 						
		private function onListAllInflowInsured(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.INSURED_INFLOW_LISTED, collection);
        }

 		public function listAllInflowPolicyHolder(inflowType:String):void{
			var delegate:ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onListAllInflowPolicyHolder, onFault));  
			delegate.listAllInflow(inflowType); 		
 		}
 						
		private function onListAllInflowPolicyHolder(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.POLICY_HOLDER_INFLOW_LISTED, collection);
        }
        
 		public function listMaritalStatus( dataGroupId:int ):void{
			var delegate:DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder(onListMaritalStatus, onFault));  
			delegate.findDataOptionByDataGroup( dataGroupId );		
 		}
 						
		private function onListMaritalStatus(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.MARITAL_STATUS_LISTED, collection);
        }

 		public function listDocumentType( dataGroupId:int ):void{
			var delegate:DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder(onListDocumentType, onFault));  
			delegate.findDataOptionByDataGroup( dataGroupId );		
 		}
 						
		private function onListDocumentType(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.DOCUMENT_TYPE_LISTED, collection);
        }

 		public function listAddressType( dataGroupId:int ):void{
			var delegate:DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder(onListAddressType, onFault));  
			delegate.findDataOptionByDataGroup( dataGroupId );	
 		}
 						
		private function onListAddressType(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.ADDRESS_TYPE_LISTED, collection);
        }

 		public function listBeneficiaryType( dataGroupId:int ):void{
			var delegate:DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder(onListBeneficiaryType, onFault));  
			delegate.findDataOptionByDataGroup( dataGroupId );	
 		}
 						
		private function onListBeneficiaryType(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.BENEFICIARY_TYPE_LISTED, collection);
        }

 		public function listKinshipType( dataGroupId:int ):void{
			var delegate:DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder(onListKinshipType, onFault));  
			delegate.findDataOptionByDataGroup( dataGroupId );	 		
 		}
		
		private function onListKinshipType(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.KINSHIP_TYPE_LISTED, collection);
        }

 		public function listGenderType(dataGroupId:int):void{
			var delegate:DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder(onListGenderType, onFault));  
			delegate.findDataOptionByDataGroup( dataGroupId );			
 		}
 						
		private function onListGenderType(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.GENDER_TYPE_LISTED, collection);
        }        

 		public function listProfessionType(dataGroupId:int):void{
			var delegate:DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder(onListProfessionType, onFault));  
			delegate.findDataOptionByDataGroup( dataGroupId );			
 		}
 						
		private function onListProfessionType(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.PROFESSION_TYPE_LISTED, collection);
        }        

 		public function listPhoneType(dataGroupId:int):void{
			var delegate:DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder(onListPhoneType, onFault));  
			delegate.findDataOptionByDataGroup( dataGroupId );			
 		}
 						
		private function onListPhoneType(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.PHONE_TYPE_LISTED, collection);
        } 

 		public function listContactType(dataGroupId:int):void{
			var delegate:DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder(onListContactType, onFault));  
			delegate.findDataOptionByDataGroup( dataGroupId );			
 		}
 						
		private function onListContactType(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.CONTACT_TYPE_LISTED, collection);
        } 
        

 		public function listPolicyType(dataGroupId:int):void{
			var delegate:DataOptionDelegateProxy = new DataOptionDelegateProxy(new Responder(onListPolicyType, onFault));  
			delegate.findDataOptionByDataGroup( dataGroupId );			
 		}
 						
		private function onListPolicyType(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.POLICY_TYPE_LISTED, collection);
        } 

 		public function listActivity():void{
			var delegate:ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onListActivity, onFault));  
			delegate.listAllActivity();			
 		}
 						
		private function onListActivity(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.ACTIVITY_LISTED, collection);
        }        
        
 		public function listAllOccupation():void{
			var delegate:ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onListOccupation, onFault));  
			delegate.listAllOccupation();			
 		}
 		
 		/**
 		 * 
 		 */
 		public function listAllRestrictionRiskTypes(): void {
			var delegate:ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onListRestrictionRiskTypes, onFault));  
			delegate.listAllRestrictionRiskTypes();			
 		}

		private function onListRestrictionRiskTypes(pResultEvt:ResultEvent):void  {
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.RESTRICTION_RISK_LISTED, collection);
		}
		
		private function onListOccupation(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.OCCUPATION_LISTED, collection);
        }         

 		public function listProfession():void{
			var delegate:ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onListProfession, onFault));  
			delegate.listAllProfession();			
 		}
 						
		private function onListProfession(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.PROFESSION_LISTED, collection);
        }         

 		public function buscarEndorsement( endorsementId :int):void{
			var delegate:ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onResultListEndorsement, onFault));  
			delegate.buscarEndorsement( endorsementId );			
 		}
 						
		private function onResultListEndorsement(pResultEvt:ResultEvent):void  {  
			var collection:ArrayCollection = pResultEvt.result as ArrayCollection
        	sendNotification(NotificationList.ENDORSEMENT_LISTED, collection);
        }         

 		public function saveEndorsement( endorsement:Endorsement):void{
			var delegate:ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onResultEndorsements, onFault));  
			delegate.saveEndorsement( endorsement );			
 		}
 						
		private function onResultEndorsements(pResultEvt:ResultEvent):void  {  
			var endorsement:Endorsement = pResultEvt.result as Endorsement ; 
        	sendNotification(NotificationList.ENDORSEMENT_SAVE_SUCCESS, endorsement);
        }         

 		public function saveCustomer( customer :Customer):void{
			var delegate:ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onResultCustomer, onFault));  
			delegate.saveCustomer( customer );			
 		}
 						
		private function onResultCustomer(pResultEvt:ResultEvent):void  { 
			var customer:Customer =pResultEvt.result as Customer ; 
        	sendNotification(NotificationList.CUSTOMER_SAVE_SUCCESS, customer);
        }         

		public function listPaymentOption(totalPremium: Number, netPremium:Number, policyCost:Number, fractioningAdditional:Number, taxValue:Number, taxRate: Number, productId:int, billingMethod:int, refDate:Date, startDate:Date, finishDate:Date, dueDay:int, renewalType:int, renewalInsurerId:int, issuanceType:int, paymentTermType:int, invoiceType:int, firstPaymentType:int = 0, qtInstallment:int = 0, proposalDate:Date = null):void{
			var delegate:ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onListPaymentOption, onFault));
			delegate.listPaymentOption(totalPremium, netPremium, policyCost, fractioningAdditional, taxValue, taxRate, productId, billingMethod, refDate, startDate, finishDate, dueDay, renewalType, renewalInsurerId, issuanceType, paymentTermType, invoiceType, firstPaymentType, qtInstallment, proposalDate);
		}
		
		private function onListPaymentOption(pResultEvt:ResultEvent):void
		{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.PAYMENT_OPTION_LISTED, result);
		}
		
		public function findPersonalRiskOptions(productId:int, riskPlanId:int, referenceDate:Date):void{
			var delegate : ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onFindPersonalRiskOptionResult, onFault));
			delegate.findPersonalRiskOptions(productId, riskPlanId, referenceDate);			
		}

		private function onFindPersonalRiskOptionResult(pResultEvt:ResultEvent):void
		{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.PERSONAL_RISK_OPTION_LISTED, result);
		}

		public function effectiveProposal(endorsement : Endorsement, paymentOption : PaymentOption):void{
			var delegate : ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onEffectiveProposalResult, onFaultEffectiveProposal));
			delegate.effectiveProposal(endorsement, paymentOption);			
		}

		private function onEffectiveProposalResult(pResultEvt:ResultEvent):void
		{
			var result:Endorsement=pResultEvt.result as Endorsement ;
         	sendNotification(NotificationList.PROPOSAL_EFFECTIVED_ENDORSEMENT, result);
		}

 		public function loadUnlockabeEndorsement(endorsementId : int, contractId : int) : void {
        	var delegate : ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onLoadUnlockabeEndorsement, onFault));
        	delegate.loadUnlockableEndorsement(endorsementId, contractId);
        }
        
        private function onLoadUnlockabeEndorsement(pResultEvent:ResultEvent):void  {  
        	var result : EndorsementOption = pResultEvent.result as EndorsementOption;
         	sendNotification(NotificationList.ENDORSEMENT_UNLOCKABLE_LISTED, result);
         }

 		public function unlockEndorsement(endorsementId : int, contractId : int) : void {
        	var delegate : ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onUnlockEndorsement, onFault));
        	delegate.unlockEndorsement(endorsementId, contractId);
        }
        
        private function onUnlockEndorsement(pResultEvent:ResultEvent):void  {  
        	var result : Boolean = pResultEvent.result as Boolean;
         	sendNotification(NotificationList.ENDORSEMENT_UNLOCKED, result);
         }
         
        public function policyChangeRegister(endorsement:Endorsement, changeDate:Date):void {
        	var delegate : ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onPolicyChangeRegister, onFault));
        	delegate.policyChangeRegister(endorsement, changeDate);
        }
        
        private function onPolicyChangeRegister( pResultEvent:ResultEvent ):void{
        	var result : Endorsement = pResultEvent.result as Endorsement;
         	sendNotification(NotificationList.POLICY_CHANGE_SUCCESS, result);
        }
        
		public function policyChangeTechnical(endorsement:Endorsement, changeDate:Date, isRecalculation:Boolean, isProposal:Boolean, paymentOption: PaymentOption):void {
        	var delegate : ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onPolicyChangeTechnical, onFaultEffectiveProposal));
        	delegate.policyChangeTechnical(endorsement, changeDate, isRecalculation, isProposal, paymentOption);
        }
        
        private function onPolicyChangeTechnical(pResultEvent:ResultEvent):void{
        	var result : Endorsement = pResultEvent.result as Endorsement;
         	sendNotification(NotificationList.POLICY_CHANGE_SUCCESS, result);
        }
        
   		public function cancellationProposal(endorsementId : int, contractId : int, reason : int) : void {
        	var delegate : ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onCancellationProposal, onFault));
        	delegate.cancellationProposal(endorsementId, contractId, reason);
        }
        
        private function onCancellationProposal(pResultEvent:ResultEvent):void  {  
        	var result : Boolean = pResultEvent.result as Boolean;
         	sendNotification(NotificationList.CERTIFICATE_ENDORSEMENT_CANCELED, result);
        }
        
		private function onFaultEffectiveProposal(pFaultEvt:FaultEvent):void  {  
			if(pFaultEvt.fault.faultString.indexOf( "sessionExpired", 0) != -1){
				sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);
			} else {
	         	sendNotification(NotificationList.PROPOSAL_NOT_EFFECTIVED_ENDORSEMENT, pFaultEvt);
			}	
         }

		/** 
		 * Searches for number of contracts and maximum occurences
		 */        
        public function getQuantityOfContracts(productId:int, docNumber:String, docType:int, contractId:int, dateIni:Date=null, dateFin: Date=null):void{
        	var delegate : ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onNumActivePoliciesPerProductDocument, onFault));
        	var dataInicio: Date, dataFinal:Date;
        	dataInicio = dateIni==null?new Date():dateIni;
        	dataFinal = dateFin==null?new Date():dateFin;
        	delegate.getQuantityOfContracts(productId, docNumber, docType,dataInicio , dataFinal, contractId);        	
        }

        /**
         * 
         */ 
        private function onNumActivePoliciesPerProductDocument(pResultEvent:ResultEvent):void {
        	var result: int = pResultEvent.result as int;
         	sendNotification(NotificationList.MAX_DOCUMENTS_PER_PRODUCT, result);        	
        }
        
		/** 
		 * Validate Contract
		 * @param productId Product identificator
		 * @param docNumber Document number
		 * @param docType Document type
		 * @param contractId contract identificator
		 * @param dateIni Starting date
		 * @see br.com.tratomais.einsurance.sales.model.proxy.ProposalProxy.onNumActivePoliciesPerProductDocument
		 */        
        public function validateContract(productId:int, docNumber:String, docType:int, contractId:int, referenceDate:Date , dateIni:Date):void{
        	var delegate : ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onValidateContract, onFault));
//        	var dataInicio: Date = ( (dateIni==null)?new Date():dateIni );
        	delegate.validateContract(productId, docNumber, docType, dateIni, referenceDate, contractId);        	
        }
        
        /**
         * Sends notification after receiving information from server
		 * @see br.com.tratomais.einsurance.sales.model.proxy.ProposalProxy.validateContract
		 * @see br.com.tratomais.einsurance.core.NotificationList.CONTRACT_VALIDATIONS
         */ 
        private function onValidateContract(pResultEvent:ResultEvent):void {
        	var result: ArrayCollection = pResultEvent.result as ArrayCollection;
         	sendNotification(NotificationList.CONTRACT_VALIDATIONS, result);        	
        }
        
        // --------------------------------------------------------------------------------------------------
        
        /** 
		 * Searches the payment form
		 */  
		public function listPaymentType(productId:int, paymentTermType:int, referenceDate:Date):void{
			var delegate:ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onListPaymentType, onFault));
			delegate.listPaymentType(productId, paymentTermType, referenceDate);
		}
		
		private function onListPaymentType(pResultEvt:ResultEvent):void
		{
			var result:ArrayCollection=pResultEvt.result as ArrayCollection ;
         	sendNotification(NotificationList.PAYMENT_TYPE_LISTED, result);
		}
        
        

	public function validateBankInfo(fullAccessAccountInfo:FullAccessAccountInfo):void{
		var delegate:ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onValidateBankInfo, onFaultValidateBankInfo));
		delegate.validateBankInfo(fullAccessAccountInfo);
	}

	private function onValidateBankInfo(resultEvent:ResultEvent):void{
		sendNotification(NotificationList.VALIDATE_BANK_INFO_RESULT, true);
	}

	private function onFaultValidateBankInfo(faultEvent:FaultEvent):void{
		sendNotification(NotificationList.FAULT_VALIDATE_BANK_INFO, Utilities.getErrorDescription(faultEvent));
	}

	public function getInstallment(contractId:int, endorsementId:int, installmentId:int):void{
		var delegate:ProposalDelegateProxy = new ProposalDelegateProxy(new Responder(onGetInstallment, onFault));
		delegate.getInstallment(contractId, endorsementId, installmentId);
	}

	private function onGetInstallment(resultEvent:ResultEvent):void{
		var result:Installment = resultEvent.result as Installment;
		sendNotification(NotificationList.GET_INSTALLMENT_LISTED, result);
	}
	
        private function onFault(pFaultEvt:FaultEvent):void  {  
            sendNotification(NotificationList.FAULT_BLAZE, pFaultEvt);  
        }
    }  
}  