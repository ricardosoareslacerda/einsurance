package br.com.tratomais.einsurance.sales.model.vo
{
	import br.com.tratomais.einsurance.products.model.vo.PlanKinship;
	
	import mx.collections.ArrayCollection;
	  
    [RemoteClass(alias="br.com.tratomais.core.model.product.ProposalListData")]      
    [Managed]  
    	
	public class ProposalListData
	{
		public function ProposalListData()
		{
		}
		
		public var listPersonType:ArrayCollection;
		public var listGender:ArrayCollection;
		public var listMaritalStatus:ArrayCollection;
		public var listDocumentTypeNatural:ArrayCollection;
		public var listDocumentTypeLegally:ArrayCollection;
		public var listAddressType:ArrayCollection;
		public var listPhoneType:ArrayCollection;  			
  		public var listProfessionType:ArrayCollection;
		public var listProfession:ArrayCollection;			
		public var listKinshipType:ArrayCollection;
		public var listActivity:ArrayCollection;
		public var listOccupation:ArrayCollection;
		public var listPlanKinship:ArrayCollection;
					
		public function listPlanKinshipByPersonalRisk(personalRiskId : int) : ArrayCollection{
			var tmpListPlanKinship : ArrayCollection = new ArrayCollection();
			for each ( var planKinshipCourse: PlanKinship in listPlanKinship){
				if(planKinshipCourse.id.personalRiskType == personalRiskId){
					tmpListPlanKinship.addItem(planKinshipCourse);
				}
			}
			return tmpListPlanKinship;
		}
	}
}








