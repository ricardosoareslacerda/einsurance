﻿var whTextLabels = {
	NoTopicsFoundMsg : 'No hay tópicos encontrados.'
}

var whColors = {
	SelTopicNodeBackgroundColor : '#d4d4d4'
}

var whFrameHrefs = {
	toc : 'HelpContents.htm'
}