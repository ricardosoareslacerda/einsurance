package br.com.tratomais.ws.listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import br.com.tratomais.core.util.datatrans.ECollectionJMSTransmitter;
import br.com.tratomais.core.util.jms.EInsuranceJMSException;

/**
 * Inicializa os objetos que devem ser enviados
 * @author luiz.alberoni
 */
public class JMSQueuePreparer implements ServletContextListener{
	/** Transmite as mensagens JMS */
	private ECollectionJMSTransmitter collectionJMSTransmitter;

	/** Logger */
	private Logger logger = Logger.getLogger(JMSQueuePreparer.class);

	/** {@inheritDoc} */
	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
	}

	/** {@inheritDoc} */
	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		try {
			collectionJMSTransmitter = ECollectionJMSTransmitter.getInstance();
		}
		catch (EInsuranceJMSException e) {
			logger.error("Problem preparing Collection transmition Step - Objects could not be send ",e);
			e.printStackTrace();
		}
		collectionJMSTransmitter.start();
	}
}
