package br.com.tratomais.ws.rest;

import java.lang.reflect.InvocationTargetException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.datatype.DatatypeConfigurationException;

import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.util.AppContextHelper;
import br.com.tratomais.ws.eproduct.EProductDomainEndpoint;
import br.com.tratomais.ws.eproduct.EProductEndpoint;
import br.com.tratomais.ws.eproduct.entities.GetBillingMethodRequest;
import br.com.tratomais.ws.eproduct.entities.GetBillingMethodResponse;
import br.com.tratomais.ws.eproduct.entities.GetCoverageRequest;
import br.com.tratomais.ws.eproduct.entities.GetCoverageResponse;
import br.com.tratomais.ws.eproduct.entities.GetPlanRequest;
import br.com.tratomais.ws.eproduct.entities.GetPlanResponse;
import br.com.tratomais.ws.eproduct.entities.GetProductRequest;
import br.com.tratomais.ws.eproduct.entities.GetProductResponse;
import br.com.tratomais.ws.eproduct.entities.GetTermRequest;
import br.com.tratomais.ws.eproduct.entities.GetTermResponse;
import br.com.tratomais.ws.eproduct.entities.GetVersionRequest;
import br.com.tratomais.ws.eproduct.entities.GetVersionResponse;
import br.com.tratomais.ws.eproduct.entities.ListActivityRequest;
import br.com.tratomais.ws.eproduct.entities.ListActivityResponse;
import br.com.tratomais.ws.eproduct.entities.ListBillingMethodRequest;
import br.com.tratomais.ws.eproduct.entities.ListBillingMethodResponse;
import br.com.tratomais.ws.eproduct.entities.ListCoverageRequest;
import br.com.tratomais.ws.eproduct.entities.ListCoverageResponse;
import br.com.tratomais.ws.eproduct.entities.ListDocumentTypeRequest;
import br.com.tratomais.ws.eproduct.entities.ListDocumentTypeResponse;
import br.com.tratomais.ws.eproduct.entities.ListGenderRequest;
import br.com.tratomais.ws.eproduct.entities.ListGenderResponse;
import br.com.tratomais.ws.eproduct.entities.ListMaritalStatusRequest;
import br.com.tratomais.ws.eproduct.entities.ListMaritalStatusResponse;
import br.com.tratomais.ws.eproduct.entities.ListPersonTypeRequest;
import br.com.tratomais.ws.eproduct.entities.ListPersonTypeResponse;
import br.com.tratomais.ws.eproduct.entities.ListPhoneTypeRequest;
import br.com.tratomais.ws.eproduct.entities.ListPhoneTypeResponse;
import br.com.tratomais.ws.eproduct.entities.ListPlanRequest;
import br.com.tratomais.ws.eproduct.entities.ListPlanResponse;
import br.com.tratomais.ws.eproduct.entities.ListProductRequest;
import br.com.tratomais.ws.eproduct.entities.ListProductResponse;
import br.com.tratomais.ws.eproduct.entities.ListRenewalTypeRequest;
import br.com.tratomais.ws.eproduct.entities.ListRenewalTypeResponse;
import br.com.tratomais.ws.eproduct.entities.ListTermRequest;
import br.com.tratomais.ws.eproduct.entities.ListTermResponse;
import br.com.tratomais.ws.eproduct.entities.ListVersionRequest;
import br.com.tratomais.ws.eproduct.entities.ListVersionResponse;
import br.com.tratomais.ws.exceptions.Fault;
import br.com.tratomais.ws.exceptions.ServerException;

@Path("/json/product")
public class ProductWSRest {
	
	private EProductEndpoint eProductEndpoint;
	private EProductDomainEndpoint eProductDomainEndpoint;
	
	public ProductWSRest(){
		eProductEndpoint = (EProductEndpoint)AppContextHelper.getApplicationContext().getBean("eProductEndpoint");
		eProductDomainEndpoint = (EProductDomainEndpoint)AppContextHelper.getApplicationContext().getBean("eProductDomainEndpoint");
	}

	@POST
    @Path("/listProduct")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public ListProductResponse listProductPost(ListProductRequest listProductRequest) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, DatatypeConfigurationException, Fault, ServiceException{
		return eProductEndpoint.listProduct(listProductRequest);
	}
	
	@POST
    @Path("/getProduct")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public GetProductResponse getProduct(GetProductRequest request) throws ServiceException, ServerException, SecurityException, IllegalArgumentException, NoSuchMethodException, ClassNotFoundException, IllegalAccessException, InvocationTargetException{
		return eProductEndpoint.getProduct(request);
	}
	
	@POST
    @Path("/listVersion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public ListVersionResponse listVersion(ListVersionRequest request) throws ServiceException, ServerException, DatatypeConfigurationException{
		return eProductEndpoint.listVersion(request);
	}
	
	@POST
    @Path("/getVersion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public GetVersionResponse getVersion(GetVersionRequest request) throws ServerException, DatatypeConfigurationException{
		return eProductEndpoint.getVersion(request);
	}
	
	@POST
    @Path("/listTerm")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public ListTermResponse listTerm(ListTermRequest request) throws ServiceException, ServerException{
		return eProductEndpoint.listTerm(request);
	}
	
	@POST
    @Path("/getTerm")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public GetTermResponse getTerm(GetTermRequest request) throws ServiceException, ServerException{
		return eProductEndpoint.getTerm(request);
	}
	
	@POST
    @Path("/listRenewalType")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public ListRenewalTypeResponse listRenewalType(ListRenewalTypeRequest request) throws ServiceException, ServerException{
		return eProductDomainEndpoint.listRenewalType(request);
	}
	
	@POST
    @Path("/listPersonType")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public ListPersonTypeResponse listPersonType(ListPersonTypeRequest listPersonTypeRequest) throws ServiceException, ServerException{
		return eProductDomainEndpoint.listPersonType(listPersonTypeRequest);
	}
	
	@POST
    @Path("/listDocumentType")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public ListDocumentTypeResponse listDocumentType(ListDocumentTypeRequest listDocumentTypeRequest) throws ServiceException, ServerException{
		return eProductDomainEndpoint.listDocumentType(listDocumentTypeRequest);
	}
	
	@POST
    @Path("/listGender")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public ListGenderResponse listGender(ListGenderRequest listGenderRequest) throws ServiceException, ServerException{
		return eProductDomainEndpoint.listGender(listGenderRequest);
	}
	
	@POST
    @Path("/listMaritalStatus")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public ListMaritalStatusResponse listMaritalStatus(ListMaritalStatusRequest listMaritalStatusRequest) throws ServiceException, ServerException{
		return eProductDomainEndpoint.listMaritalStatus(listMaritalStatusRequest);
	}
	
	@POST
    @Path("/listActivity")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public ListActivityResponse listActivity(ListActivityRequest listActivityRequest) throws ServiceException, ServerException {
		return eProductDomainEndpoint.listActivity(listActivityRequest);
	}
	
	@POST
    @Path("/listPlan")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ListPlanResponse listPlan(ListPlanRequest request) throws ServiceException, ServerException{
		return eProductEndpoint.listPlan(request);
	}
	
	@POST
    @Path("/getPlan")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public GetPlanResponse getPlan(GetPlanRequest request) throws ServiceException, ServerException{
		return eProductEndpoint.getPlan(request);
	}
	
	@POST
    @Path("/listCoverage")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public ListCoverageResponse listCoverage(ListCoverageRequest request) throws ServiceException, ServerException{
		return eProductEndpoint.listCoverage(request);
	}
	
	@POST
    @Path("/getCoverage")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public GetCoverageResponse getCoverage(GetCoverageRequest request) throws ServiceException, ServerException{
		return eProductEndpoint.getCoverage(request);
	}
	
	@POST
    @Path("/listBillingMethod")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public ListBillingMethodResponse listBillingMethod(ListBillingMethodRequest request) throws ServiceException, ServerException{
		return eProductEndpoint.listBillingMethod(request);
	}
	
	@POST
    @Path("/getBillingMethod")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public GetBillingMethodResponse getBillingMethod(GetBillingMethodRequest request) throws ServiceException, ServerException{
		return eProductEndpoint.getBillingMethod(request);
	}
	
	@POST
    @Path("/listPhoneType")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public ListPhoneTypeResponse listPhoneType(ListPhoneTypeRequest listPhoneTypeRequest) throws ServiceException, ServerException{
		return eProductDomainEndpoint.listPhoneType(listPhoneTypeRequest);
	}
	
}