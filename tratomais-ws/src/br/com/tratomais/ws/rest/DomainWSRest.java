package br.com.tratomais.ws.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.util.AppContextHelper;
import br.com.tratomais.ws.einsurance.EInsuranceDomainEndpoint;
import br.com.tratomais.ws.einsurance.entities.UserAuthenticationRequest;
import br.com.tratomais.ws.einsurance.entities.UserAuthenticationResponse;
import br.com.tratomais.ws.einsurance.entities.UserType;
import br.com.tratomais.ws.exceptions.ServerException;

@Path("/json/domain")
public class DomainWSRest {
	
	private EInsuranceDomainEndpoint eInsuranceDomainEndpoint;
	
	public DomainWSRest(){
		eInsuranceDomainEndpoint = (EInsuranceDomainEndpoint)AppContextHelper.getApplicationContext().getBean("eInsuranceDomainEndpoint");
	}

	@POST
    @Path("/userAuthentication")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public UserAuthenticationResponse userAuthentication(UserAuthenticationRequest userAuthenticationRequest) {
		try {
			return eInsuranceDomainEndpoint.userAuthentication(userAuthenticationRequest);
		} catch (ServerException e) {
		} catch (ServiceException e) {
		}
		UserType userType = new UserType();
		userType.setUserID(0);
		UserAuthenticationResponse userAuthenticationResponse = new UserAuthenticationResponse();
		userAuthenticationResponse.setUser(userType);
		return userAuthenticationResponse;
	}

	
}