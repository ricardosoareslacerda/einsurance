package br.com.tratomais.ws.rest;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.jms.JMSException;
import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.datatype.DatatypeConfigurationException;

import br.com.tratomais.core.exception.DataAccessException;
import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.util.AppContextHelper;
import br.com.tratomais.core.util.jms.EInsuranceJMSException;
import br.com.tratomais.ws.einsurance.EInsuranceDomainEndpoint;
import br.com.tratomais.ws.einsurance.EInsuranceEndpoint;
import br.com.tratomais.ws.einsurance.entities.ListBrokerRequest;
import br.com.tratomais.ws.einsurance.entities.ListBrokerResponse;
import br.com.tratomais.ws.einsurance.entities.ListChannelRequest;
import br.com.tratomais.ws.einsurance.entities.ListChannelResponse;
import br.com.tratomais.ws.einsurance.entities.ListSubsidiaryRequest;
import br.com.tratomais.ws.einsurance.entities.ListSubsidiaryResponse;
import br.com.tratomais.ws.einsurance.entities.PolicyLoadRequest;
import br.com.tratomais.ws.einsurance.entities.PolicyLoadResponse;
import br.com.tratomais.ws.einsurance.entities.PolicyPrintRequest;
import br.com.tratomais.ws.einsurance.entities.PolicyPrintResponse;
import br.com.tratomais.ws.einsurance.entities.PolicyProposalRequest;
import br.com.tratomais.ws.einsurance.entities.PolicyProposalResponse;
import br.com.tratomais.ws.einsurance.entities.PolicyQuotationRequest;
import br.com.tratomais.ws.einsurance.entities.PolicyQuotationResponse;
import br.com.tratomais.ws.exceptions.ErrorListException;
import br.com.tratomais.ws.exceptions.Fault;
import br.com.tratomais.ws.exceptions.ServerException;

@Path("/json/policy")
public class PolicyWSRest {
	
	private EInsuranceEndpoint eInsuranceEndpoint;
	private EInsuranceDomainEndpoint eInsuranceDomainEndpoint;
	
	public PolicyWSRest(){
		eInsuranceEndpoint = (EInsuranceEndpoint)AppContextHelper.getApplicationContext().getBean("eInsuranceEndpoint");
		eInsuranceDomainEndpoint = (EInsuranceDomainEndpoint)AppContextHelper.getApplicationContext().getBean("eInsuranceDomainEndpoint");
	}

	@GET
    @Path("/policyLoad")
    @Produces(MediaType.APPLICATION_JSON)
	public PolicyLoadResponse policyLoadGet(
			@QueryParam("contractId") Integer contractId,
			@QueryParam("endorsementId") Integer endorsementId) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, DatatypeConfigurationException, Fault{

		eInsuranceEndpoint = (EInsuranceEndpoint)AppContextHelper.getApplicationContext().getBean("eInsuranceEndpoint");
		
		PolicyLoadRequest policyLoadRequest = new PolicyLoadRequest();
		policyLoadRequest.setContractID(contractId);
		policyLoadRequest.setEndorsementID(endorsementId);
		
		return eInsuranceEndpoint.policyLoad(policyLoadRequest);
	}

	@POST
    @Path("/listBroker")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public ListBrokerResponse listBroker(ListBrokerRequest listBrokerRequest) throws ServiceException, ServerException {
		return eInsuranceDomainEndpoint.listBroker(listBrokerRequest);
	}
	
	@POST
    @Path("/listSubsidiary")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public ListSubsidiaryResponse listSubsidiary(ListSubsidiaryRequest listSubsidiaryRequest) throws ServiceException, ServerException {
		return eInsuranceDomainEndpoint.listSubsidiary(listSubsidiaryRequest);
	}
	
	@POST
    @Path("/listChannel")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public ListChannelResponse listChannel(ListChannelRequest listChannelRequest) throws ServiceException, ServerException {
		return eInsuranceDomainEndpoint.listChannel(listChannelRequest);
	}

	@POST
    @Path("/policyLoad")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public PolicyLoadResponse policyLoadPost(PolicyLoadRequest policyLoadRequest) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, DatatypeConfigurationException, Fault{
		return eInsuranceEndpoint.policyLoad(policyLoadRequest);
	}
	
	@POST
    @Path("/policyQuotation")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public PolicyQuotationResponse policyQuotation(PolicyQuotationRequest policyQuotationRequest) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, DatatypeConfigurationException, ServiceException, ErrorListException, Fault {
		return eInsuranceEndpoint.policyQuotation(policyQuotationRequest);
	}
	
	@POST
    @Path("/policyProposal")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public PolicyProposalResponse policyProposal(PolicyProposalRequest policyProposalRequest) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, DatatypeConfigurationException, ServiceException, ErrorListException, Fault, FileNotFoundException, JMSException, NamingException, IOException, EInsuranceJMSException {
		return eInsuranceEndpoint.policyProposal(policyProposalRequest);
	}

}
