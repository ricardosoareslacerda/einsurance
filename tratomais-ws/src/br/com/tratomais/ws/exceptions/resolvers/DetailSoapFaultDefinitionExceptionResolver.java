package br.com.tratomais.ws.exceptions.resolvers;

import java.io.IOException;

import org.springframework.oxm.Marshaller;
import org.springframework.oxm.XmlMappingException;
import org.springframework.stereotype.Component;
import org.springframework.ws.soap.SoapFault;
import org.springframework.ws.soap.SoapFaultDetail;
import org.springframework.ws.soap.server.endpoint.SoapFaultMappingExceptionResolver;

import br.com.tratomais.core.service.erros.Erro;
import br.com.tratomais.core.service.erros.ErrorList;
import br.com.tratomais.ws.exceptions.ErrorListException;
import br.com.tratomais.ws.exceptions.Fault;
/**
 * Realiza a conversão de exceções de sistema para ficar de acordo com as especificações de fault
 * @author Luiz
 */
@Component
public class DetailSoapFaultDefinitionExceptionResolver extends
		SoapFaultMappingExceptionResolver {

	/** Conversor para XML */
	private Marshaller marshaller;
	
	/**
	 * Ajusta o conversor de objetos para Xml
	 * @param marshaller conversor de objetos para Xml
	 */
	public void setMarshaller(Marshaller marshaller) {
		this.marshaller = marshaller;
	}
	
	/** {@inheritDoc} */
	@Override
	protected void customizeFault(Object endpoint, Exception ex, SoapFault fault) {
		if (ex instanceof Fault){
			doOnFault(endpoint, (Fault)ex, fault);
		}
		else if (ex instanceof ErrorListException){
			doOnErrorListException(endpoint, (ErrorListException)ex, fault);
		}
		else {
			System.out.println(ex);
		}
	}
	
	private void doOnErrorListException(Object endpoint, ErrorListException ex,
			SoapFault fault) {
		SoapFaultDetail detail = fault.addFaultDetail();

		ErrorList errorList = ex.getErrorList();
		for(Erro erro: errorList.getListaErros()){
			br.com.tratomais.ws.faults.Error error = new br.com.tratomais.ws.faults.Error();
			
			error.setErrorID(erro.getCodigo());
			error.setErrorTypeID(773); // erro.getTipoErro()
			error.setErrorMessage(erro.getDescricao());
			
			try {
				marshaller.marshal(error, detail.getResult());
			} catch (XmlMappingException e) {
				throw new RuntimeException(e);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		
	}

	private void doOnFault(Object endpoint, Fault ex, SoapFault fault) {
		SoapFaultDetail detail = fault.addFaultDetail();
		
		br.com.tratomais.ws.faults.Error error = new br.com.tratomais.ws.faults.Error();
		
		error.setErrorID(ex.getErrorID());
		error.setErrorTypeID(ex.getErrorTypeID());
		error.setErrorMessage(ex.getMessage());

		try {
			marshaller.marshal(error, detail.getResult());
		} catch (XmlMappingException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}