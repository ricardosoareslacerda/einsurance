package br.com.tratomais.ws.exceptions;

/**
 * Representa um erro
 * @author Luiz
 */
public class Fault extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/*
	 ========================================== 
	   P r o p r i e d a d e s  
	 ========================================== 
	 */
	/** Representa o identificador do erro */
	private int errorID;
	
	/** C�digo do tipo de erro */
	private int errorTypeID;
	
	/**
	 * Erro
	 * @param errorID C�digo do erro
	 * @param errorTypeID C�digo do tipo do erro
	 * @param message Mensagem de erro
	 * @param cause causa do erro
	 */
	public Fault(int errorID, int errorTypeID,String message, Throwable cause) {
		super(message, cause);
		this.errorID = errorID;
		this.errorTypeID = errorTypeID;
	}
	
	/**
	 * Erro
	 * @param errorID C�digo do erro
	 * @param errorTypeID C�digo do tipo do erro
	 * @param message Mensagem de erro
	 */
	public Fault(int errorID, int errorTypeID, String message) {
		super(message);
		this.errorID = errorID;
		this.errorTypeID = errorTypeID;
	}
	
	/**
	 * Erro
	 * @param errorID C�digo do erro
	 * @param errorTypeID C�digo do tipo do erro
	 * @param cause causa do erro
	 */
	public Fault(int errorID, int errorTypeID, Throwable cause) {
		super(cause);
		this.errorID = errorID;
		this.errorTypeID = errorTypeID;
	}
	
	/**
	 * Retorna o identificador do erro
	 * @return identificador do erro
	 */
	public int getErrorID() {
		return errorID;
	}
	
	/**
	 * Retorna o identificador do tipo de erro
	 * @return
	 */
	public int getErrorTypeID() {
		return errorTypeID;
	}
}
