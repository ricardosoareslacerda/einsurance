package br.com.tratomais.ws.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import br.com.tratomais.core.service.erros.Erro;
import br.com.tratomais.ws.faults.Error;

@Provider
public class ExceptionMapperImpl implements ExceptionMapper<Throwable> {
	
	public ExceptionMapperImpl() {
	}
	
	@Override
	public Response toResponse(Throwable error) {
		Error errorWS = new Error();
		String message = "";
		if(error instanceof ErrorListException){
			Erro erro = ((ErrorListException)error).getErrorList().getListaErros().get(0);
			message = erro.getDescricao(); 
		}
		else{
			message = error.getMessage(); 
		}
		errorWS.setErrorMessage(message);
		
		return Response.status(Status.OK).entity(errorWS).build();
	}

}
