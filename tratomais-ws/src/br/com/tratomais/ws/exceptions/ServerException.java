package br.com.tratomais.ws.exceptions;

import org.springframework.ws.soap.server.endpoint.annotation.FaultCode;
import org.springframework.ws.soap.server.endpoint.annotation.SoapFault;

@SoapFault(faultCode = FaultCode.CLIENT,faultStringOrReason="SERVICE-ERR",locale="en")
public class ServerException extends Fault {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** {@inheritDoc} */
	public ServerException(int errorID, int errorTypeID, String message,
			Throwable cause) {
		super(errorID, errorTypeID, message, cause);
	}
	/** {@inheritDoc} */
	public ServerException(int errorID, int errorTypeID, String message) {
		super(errorID, errorTypeID, message);
	}
	/** {@inheritDoc} */
	public ServerException(int errorID, int errorTypeID, Throwable cause) {
		super(errorID, errorTypeID, cause);
	}
}
