package br.com.tratomais.ws.exceptions;

import br.com.tratomais.core.service.erros.ErrorList;

public class ErrorListException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ErrorList errorList;

	public ErrorListException(ErrorList errorList) {
		this.errorList = errorList;
	}
	
	public ErrorList getErrorList() {
		return errorList;
	}

}
