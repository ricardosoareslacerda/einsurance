package br.com.tratomais.ws.exceptions;

import org.springframework.ws.soap.server.endpoint.annotation.FaultCode;
import org.springframework.ws.soap.server.endpoint.annotation.SoapFault;

@SoapFault(faultCode = FaultCode.SERVER,faultStringOrReason="SERVICE-ERR",locale="en")
public class ClientException extends Fault {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ClientException(int errorID, int errorTypeID, String message,
			Throwable cause) {
		super(errorID, errorTypeID, message, cause);
	}

	public ClientException(int errorID, int errorTypeID, String message) {
		super(errorID, errorTypeID, message);
	}

	public ClientException(int errorID, int errorTypeID, Throwable cause) {
		super(errorID, errorTypeID, cause);
	}
}
