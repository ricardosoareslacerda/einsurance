package br.com.tratomais.ws.einsurance;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.infrastructure.AuthenticationHelper;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.customer.Broker;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Insurer;
import br.com.tratomais.core.model.customer.Partner;
import br.com.tratomais.core.model.customer.Subsidiary;
import br.com.tratomais.core.model.policy.MasterPolicy;
import br.com.tratomais.core.model.product.Product;
import br.com.tratomais.core.service.IServiceEinsurance;
import br.com.tratomais.core.service.erros.ErrorTypes;
import br.com.tratomais.core.util.Utilities;
import br.com.tratomais.general.utilities.DateUtilities;
import br.com.tratomais.ws.einsurance.entities.BrokerType;
import br.com.tratomais.ws.einsurance.entities.ChannelType;
import br.com.tratomais.ws.einsurance.entities.InsurerType;
import br.com.tratomais.ws.einsurance.entities.ListBrokerRequest;
import br.com.tratomais.ws.einsurance.entities.ListBrokerResponse;
import br.com.tratomais.ws.einsurance.entities.ListChannelRequest;
import br.com.tratomais.ws.einsurance.entities.ListChannelResponse;
import br.com.tratomais.ws.einsurance.entities.ListInsurerRequest;
import br.com.tratomais.ws.einsurance.entities.ListInsurerResponse;
import br.com.tratomais.ws.einsurance.entities.ListPartnerRequest;
import br.com.tratomais.ws.einsurance.entities.ListPartnerResponse;
import br.com.tratomais.ws.einsurance.entities.ListPolicyRequest;
import br.com.tratomais.ws.einsurance.entities.ListPolicyResponse;
import br.com.tratomais.ws.einsurance.entities.ListSubsidiaryRequest;
import br.com.tratomais.ws.einsurance.entities.ListSubsidiaryResponse;
import br.com.tratomais.ws.einsurance.entities.ListUserRequest;
import br.com.tratomais.ws.einsurance.entities.ListUserResponse;
import br.com.tratomais.ws.einsurance.entities.PartnerType;
import br.com.tratomais.ws.einsurance.entities.PolicyType;
import br.com.tratomais.ws.einsurance.entities.SubsidiaryType;
import br.com.tratomais.ws.einsurance.entities.UserAuthenticationRequest;
import br.com.tratomais.ws.einsurance.entities.UserAuthenticationResponse;
import br.com.tratomais.ws.einsurance.entities.UserType;
import br.com.tratomais.ws.exceptions.ServerException;

@Endpoint
public class EInsuranceDomainEndpoint {
	private static final String NAMESPACE = "http://www.tratomais.com.br/services/eInsurance";
	private IServiceEinsurance serviceEinsurance;
	
	public void setServiceEinsurance(IServiceEinsurance serviceEinsurance){
		this.serviceEinsurance = serviceEinsurance;
	}

	@PayloadRoot(namespace=EInsuranceDomainEndpoint.NAMESPACE, localPart="listInsurerRequest")
	public ListInsurerResponse listInsurer(ListInsurerRequest listInsurerRequest) throws ServiceException, ServerException {
		ListInsurerResponse response = new ListInsurerResponse();
		List<Insurer> listInsurer = serviceEinsurance.listInsurerByRefDate(DateUtilities.xmlGregorianCalendarToDate(listInsurerRequest.getRefDate()));
		
		if(listInsurer != null && listInsurer.size() > 0){
			for(Insurer insurer : listInsurer){
				InsurerType insurerType = new InsurerType();
				Utilities.copyToObjectDifferent(insurer, insurerType);
				insurerType.setInsurerCode((String)Utilities.convertNullValue(insurer.getExternalCode(),""));
				insurerType.setDocumentTypeID((Integer)Utilities.convertNullValue(insurer.getDocumentType(),"0"));
				if(insurer.getDocumentNumber() != null && insurer.getDocumentNumber().trim().equals("")){
					insurer.setDocumentNumber(null);
				}
				insurerType.setDocumentNumber(new BigDecimal((String)Utilities.convertNullValue(insurer.getDocumentNumber(),"0")));
				insurerType.setMain(insurer.isInsurerMain());
				response.getInsurer().add(insurerType);
			}
		} else {
			throw new ServerException(412, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		return response;
	}

	@PayloadRoot(namespace=EInsuranceDomainEndpoint.NAMESPACE, localPart="listSubsidiaryRequest")
	public ListSubsidiaryResponse listSubsidiary(ListSubsidiaryRequest listSubsidiaryRequest) throws ServiceException, ServerException {
		ListSubsidiaryResponse response = new ListSubsidiaryResponse();
		List<Subsidiary> listSubsidiary = serviceEinsurance.listSubsidiary(listSubsidiaryRequest.getInsurerID(), DateUtilities.xmlGregorianCalendarToDate(listSubsidiaryRequest.getRefDate()));
		
		if(listSubsidiary != null && listSubsidiary.size() > 0){
			for(Subsidiary subsidiary : listSubsidiary){
				SubsidiaryType subsidiaryType = new SubsidiaryType();
				Utilities.copyToObjectDifferent(subsidiary, subsidiaryType);
				subsidiaryType.setSubsidiaryCode((String)Utilities.convertNullValue(subsidiary.getExternalCode(),""));
				response.getSubsidiary().add(subsidiaryType);
			}
		} else {
			throw new ServerException(422, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		return response;
	}

	@PayloadRoot(namespace=NAMESPACE, localPart="listBrokerRequest")
	public ListBrokerResponse listBroker(ListBrokerRequest listBrokerRequest) throws ServiceException, ServerException {
		ListBrokerResponse response = new ListBrokerResponse();
		List<Broker> listBroker = serviceEinsurance.listBroker(listBrokerRequest.getInsurerID(), listBrokerRequest.getRegulatoryCode(), DateUtilities.xmlGregorianCalendarToDate(listBrokerRequest.getRefDate()));
		
		if(listBroker != null && listBroker.size() > 0){
			for(Broker broker : listBroker){
				BrokerType brokerType = new BrokerType();
				Utilities.copyToObjectDifferent(broker, brokerType);
				brokerType.setBrokerCode((String)Utilities.convertNullValue(broker.getExternalCode(),""));
				brokerType.setDocumentTypeID((Integer)Utilities.convertNullValue(broker.getDocumentType(),"0"));
				if(broker.getDocumentNumber() != null && broker.getDocumentNumber().trim().equals(""))
				{
					broker.setDocumentNumber(null);
				}
				brokerType.setDocumentNumber(new BigDecimal((String)Utilities.convertNullValue(broker.getDocumentNumber(),"0")));
				response.getBroker().add(brokerType);
			}
		} else {
			throw new ServerException(432, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		return response;
	}

	@PayloadRoot(namespace=NAMESPACE, localPart="listPartnerRequest")
	public ListPartnerResponse listPartner(ListPartnerRequest listPartnerRequest) throws ServiceException, ServerException {
		ListPartnerResponse response = new ListPartnerResponse();
		List<Partner> listPartner = serviceEinsurance.listPartner(listPartnerRequest.getInsurerID(), DateUtilities.xmlGregorianCalendarToDate(listPartnerRequest.getRefDate()));
		
		if(listPartner != null && listPartner.size() > 0){
			for(Partner partner : listPartner){
				PartnerType partnerType = new PartnerType();
				Utilities.copyToObjectDifferent(partner, partnerType);
				partnerType.setPartnerCode((String)Utilities.convertNullValue(partner.getExternalCode(),""));
				partnerType.setDocumentTypeID((Integer)Utilities.convertNullValue(partner.getDocumentType(), 0));
				if(partner.getDocumentNumber()!= null && partner.getDocumentNumber().trim().equals("")){
					partner.setDocumentNumber(null);
				}
				partnerType.setDocumentNumber(new BigDecimal((String)Utilities.convertNullValue(partner.getDocumentNumber(), "0")));
				partnerType.setDomain((partner.getDomain()!=null?partner.getDomain().getName():""));
				response.getPartner().add(partnerType);
			}
		} else {
			throw new ServerException(442, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		return response;
	}

	@PayloadRoot(namespace=NAMESPACE, localPart="listChannelRequest")
	public ListChannelResponse listChannel(ListChannelRequest listChannelRequest) throws ServiceException, ServerException {
		ListChannelResponse response = new ListChannelResponse();
		List<Channel> listChannel = serviceEinsurance.listChannel(listChannelRequest.getInsurerID(), listChannelRequest.getPartnerID(), DateUtilities.xmlGregorianCalendarToDate(listChannelRequest.getRefDate()));
		
		if(listChannel != null && listChannel.size() > 0){
			for(Channel channel : listChannel){
				ChannelType channelType = new ChannelType();
				Utilities.copyToObjectDifferent(channel, channelType);
				channelType.setChannelCode((String)Utilities.convertNullValue(channel.getExternalCode(),""));
				response.getChannel().add(channelType);
			}
		} else {
			throw new ServerException(452, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		return response;
	}

	@PayloadRoot(namespace=NAMESPACE, localPart="listUserRequest")
	public ListUserResponse listUser(ListUserRequest listUserRequest) throws ServiceException, ServerException {
		ListUserResponse response = new ListUserResponse();
		List<User> listUser = serviceEinsurance.listUser(listUserRequest.getInsurerID(), listUserRequest.getPartnerID(), listUserRequest.getChannelID(), DateUtilities.xmlGregorianCalendarToDate(listUserRequest.getRefDate()));
		
		if(listUser != null && listUser.size() > 0){
			for(User user : listUser){
				UserType userType = new UserType();
				Utilities.copyToObjectDifferent(user, userType);
				userType.setUserCode((String)Utilities.convertNullValue(user.getEmployeeCode(),"0"));
				response.getUser().add(userType);
			}
		} else {
			throw new ServerException(462, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		return response;
	}

	@PayloadRoot(namespace=NAMESPACE, localPart="listPolicyRequest")
	public ListPolicyResponse listPolicy(ListPolicyRequest listPolicyRequest) throws ServiceException, ServerException {
		ListPolicyResponse response = new ListPolicyResponse();
		List<MasterPolicy> listMasterPolicy = serviceEinsurance.listMasterPolicy(listPolicyRequest.getInsurerID(), listPolicyRequest.getPartnerID(), DateUtilities.xmlGregorianCalendarToDate(listPolicyRequest.getRefDate()));
		
		if(listMasterPolicy != null && listMasterPolicy.size() > 0){
			for(MasterPolicy masterPolicy : listMasterPolicy){
				PolicyType policyType = new PolicyType();
				Utilities.copyToObjectDifferent(masterPolicy, policyType);
				policyType.setPolicyID(masterPolicy.getMasterPolicyId());
				Product product = serviceEinsurance.getProductById(masterPolicy.getProductId());
				if (product  != null) {
					policyType.setProductNickName(product.getNickName());
				}
				policyType.setPolicyNumber(masterPolicy.getPolicyNumber().longValue());
				response.getPolicy().add(policyType);
			}
		} else {
			throw new ServerException(472, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		return response;
	}
	/**
	 * Aut�ntica os dados do Usu�rio (login e senha)
	 * @param userAuthenticationRequest UserAuthenticationRequest
	 * @return UserAuthenticationResponse
	 */
	@PayloadRoot(namespace=NAMESPACE, localPart="userAuthenticationRequest")
	public UserAuthenticationResponse userAuthentication(UserAuthenticationRequest userAuthenticationRequest) throws ServiceException, ServerException {
		try {
			User user = new AuthenticationHelper().authenticatePrincipalException(userAuthenticationRequest.getLogin(), new String(userAuthenticationRequest.getPassword()));
			if(user != null){
				return new UserAuthenticationHelper().transform(user); 
			} else {
				throw new ServerException(492, 772, "Login inv�lido!");
			}
		} catch (Exception e) {
			throw new ServerException(492, 772,  "Login inv�lido!");
		}
	}	

}
