package br.com.tratomais.ws.einsurance;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import br.com.tratomais.ws.einsurance.entities.AutoCompleteType;
import br.com.tratomais.ws.einsurance.entities.BeneficiaryType;
import br.com.tratomais.ws.einsurance.entities.CoverageType;
import br.com.tratomais.ws.einsurance.entities.ItemType;
import br.com.tratomais.ws.einsurance.entities.PersonBasicType;
import br.com.tratomais.ws.einsurance.entities.PropertyType;

class AdapterItem {
	private ItemType actualItemType;

	public AdapterItem(ItemType actualItemType) {
		this.actualItemType = actualItemType;
	}

	@SuppressWarnings("unchecked")
	private <T> T getValue(String getter){
		try {
			Method method = this.actualItemType.getClass().getMethod(getter);
			return (T) method.invoke(this.actualItemType);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	} 
	
	public List<CoverageType> getCoverage() {
		return getValue("getCoverage");
	}
	
	public List<BeneficiaryType> getBeneficiary() {
		return getValue("getBeneficiary");
	}
	
	public PersonBasicType getPerson() {
		return getValue("getPerson");
	}

	public PropertyType getProperty() {
		return getValue("getProperty");
	}

	public AutoCompleteType getAutoCompleteType() {
		return getValue("getAuto");
	}
	
	public Long getRenewalPolicyNumber() {
		return getValue("getRenewalPolicyNumber");
	}

	public Integer getBonusTypeID() {
		return getValue("getBonusTypeID");
	}

	public XMLGregorianCalendar getEffectiveDate() {
		return getValue("getEffectiveDate");
	}

	public Integer getRenewalInsurerID() {
		return getValue("getRenewalInsurerID");
	}

	public int getRenewalTypeID() {
		return getValue("getRenewalTypeID");
	}

	public int getPlanID() {
		return getValue("getPlanID");
	}

	public int getItemID() {
		return getValue("getItemID");
	}
}
