package br.com.tratomais.ws.einsurance;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.Resource;
import javax.jms.JMSException;
import javax.naming.NamingException;
import javax.xml.datatype.DatatypeConfigurationException;

import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

import br.com.tratomais.core.dao.IDaoCity;
import br.com.tratomais.core.dao.IDaoCountry;
import br.com.tratomais.core.dao.IDaoCoveragePlan;
import br.com.tratomais.core.dao.IDaoDataOption;
import br.com.tratomais.core.dao.IDaoInsurer;
import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.dao.IDaoProductVersion;
import br.com.tratomais.core.dao.IDaoReportJasper;
import br.com.tratomais.core.dao.IDaoState;
import br.com.tratomais.core.dao.impl.DaoContract;
import br.com.tratomais.core.dao.impl.DaoEndorsementImpl;
import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.model.City;
import br.com.tratomais.core.model.Country;
import br.com.tratomais.core.model.Inflow;
import br.com.tratomais.core.model.State;
import br.com.tratomais.core.model.customer.Activity;
import br.com.tratomais.core.model.customer.Address;
import br.com.tratomais.core.model.customer.Broker;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Customer;
import br.com.tratomais.core.model.customer.Insurer;
import br.com.tratomais.core.model.customer.Occupation;
import br.com.tratomais.core.model.customer.Partner;
import br.com.tratomais.core.model.customer.Phone;
import br.com.tratomais.core.model.customer.Profession;
import br.com.tratomais.core.model.customer.Subsidiary;
import br.com.tratomais.core.model.policy.Beneficiary;
import br.com.tratomais.core.model.policy.Contract;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.EndorsementId;
import br.com.tratomais.core.model.policy.Installment;
import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.policy.ItemAuto;
import br.com.tratomais.core.model.policy.ItemCoverage;
import br.com.tratomais.core.model.policy.ItemPersonalRisk;
import br.com.tratomais.core.model.policy.ItemPersonalRiskGroup;
import br.com.tratomais.core.model.policy.ItemProperty;
import br.com.tratomais.core.model.policy.ItemPurchaseProtected;
import br.com.tratomais.core.model.policy.ItemRiskType;
import br.com.tratomais.core.model.policy.MasterPolicy;
import br.com.tratomais.core.model.product.BillingMethod;
import br.com.tratomais.core.model.product.Coverage;
import br.com.tratomais.core.model.product.CoveragePlan;
import br.com.tratomais.core.model.product.Currency;
import br.com.tratomais.core.model.product.DataOption;
import br.com.tratomais.core.model.product.InsuredObject;
import br.com.tratomais.core.model.product.PaymentOption;
import br.com.tratomais.core.model.product.Product;
import br.com.tratomais.core.model.product.ProductPaymentTerm;
import br.com.tratomais.core.model.product.ProductTerm;
import br.com.tratomais.core.model.product.ProductVersion;
import br.com.tratomais.core.model.product.Term;
import br.com.tratomais.core.model.search.InstallmentParameters;
import br.com.tratomais.core.model.search.InstallmentResult;
import br.com.tratomais.core.model.search.SearchDocumentParameters;
import br.com.tratomais.core.model.search.SearchDocumentResult;
import br.com.tratomais.core.report.ReportFieldTO;
import br.com.tratomais.core.report.ReportTO;
import br.com.tratomais.core.service.ICalc;
import br.com.tratomais.core.service.IServiceCollection;
import br.com.tratomais.core.service.IServiceEinsurance;
import br.com.tratomais.core.service.IServicePolicy;
import br.com.tratomais.core.service.IServiceSearchDocument;
import br.com.tratomais.core.service.erros.ErrorList;
import br.com.tratomais.core.service.erros.ErrorTypes;
import br.com.tratomais.core.service.impl.ReportService;
import br.com.tratomais.core.util.Utilities;
import br.com.tratomais.core.util.jms.EInsuranceJMSException;
import br.com.tratomais.general.utilities.DateUtilities;
import br.com.tratomais.general.utilities.NumberUtilities;
import br.com.tratomais.ws.einsurance.entities.AddressCompleteType;
import br.com.tratomais.ws.einsurance.entities.AddressType;
import br.com.tratomais.ws.einsurance.entities.Auto;
import br.com.tratomais.ws.einsurance.entities.AutoCompleteType;
import br.com.tratomais.ws.einsurance.entities.BeneficiaryType;
import br.com.tratomais.ws.einsurance.entities.CardType;
import br.com.tratomais.ws.einsurance.entities.CheckType;
import br.com.tratomais.ws.einsurance.entities.ContractCompleteType;
import br.com.tratomais.ws.einsurance.entities.ContractCompleteDescriptiveType;
import br.com.tratomais.ws.einsurance.entities.ContractType;
import br.com.tratomais.ws.einsurance.entities.CoverageCompleteType;
import br.com.tratomais.ws.einsurance.entities.CoverageType;
import br.com.tratomais.ws.einsurance.entities.EndorsementCompleteType;
import br.com.tratomais.ws.einsurance.entities.EndorsementType;
import br.com.tratomais.ws.einsurance.entities.GetContractRequest;
import br.com.tratomais.ws.einsurance.entities.GetContractResponse;
import br.com.tratomais.ws.einsurance.entities.GetInstallmentRequest;
import br.com.tratomais.ws.einsurance.entities.GetInstallmentResponse;
import br.com.tratomais.ws.einsurance.entities.HolderCompleteType;
import br.com.tratomais.ws.einsurance.entities.HolderType;
import br.com.tratomais.ws.einsurance.entities.InstallmentResultType;
import br.com.tratomais.ws.einsurance.entities.InstallmentType;
import br.com.tratomais.ws.einsurance.entities.InsuredCompleteType;
import br.com.tratomais.ws.einsurance.entities.InsuredType;
import br.com.tratomais.ws.einsurance.entities.ItemCompleteType;
import br.com.tratomais.ws.einsurance.entities.ItemType;
import br.com.tratomais.ws.einsurance.entities.PaymentOptionDataType;
import br.com.tratomais.ws.einsurance.entities.PaymentOptionRequestedType;
import br.com.tratomais.ws.einsurance.entities.PersonBasicType;
import br.com.tratomais.ws.einsurance.entities.PersonCompleteType;
import br.com.tratomais.ws.einsurance.entities.PersonGroupCompleteType;
import br.com.tratomais.ws.einsurance.entities.PersonGroupType;
import br.com.tratomais.ws.einsurance.entities.PersonWithCardType;
import br.com.tratomais.ws.einsurance.entities.PersonWithGroupType;
import br.com.tratomais.ws.einsurance.entities.PhoneType;
import br.com.tratomais.ws.einsurance.entities.PolicyCalculateRequest;
import br.com.tratomais.ws.einsurance.entities.PolicyCalculateResponse;
import br.com.tratomais.ws.einsurance.entities.PolicyCancellationByErrorRequest;
import br.com.tratomais.ws.einsurance.entities.PolicyCancellationByErrorResponse;
import br.com.tratomais.ws.einsurance.entities.PolicyCancellationByInsuredRequest;
import br.com.tratomais.ws.einsurance.entities.PolicyCancellationByInsuredResponse;
import br.com.tratomais.ws.einsurance.entities.PolicyChangeRegisterRequest;
import br.com.tratomais.ws.einsurance.entities.PolicyChangeRegisterResponse;
import br.com.tratomais.ws.einsurance.entities.PolicyChangeTechnicalRequest;
import br.com.tratomais.ws.einsurance.entities.PolicyChangeTechnicalResponse;
import br.com.tratomais.ws.einsurance.entities.PolicyCompleteType;
import br.com.tratomais.ws.einsurance.entities.PolicyInstallmentPayRequest;
import br.com.tratomais.ws.einsurance.entities.PolicyInstallmentPayResponse;
import br.com.tratomais.ws.einsurance.entities.PolicyInstallmentSearchRequest;
import br.com.tratomais.ws.einsurance.entities.PolicyInstallmentSearchResponse;
import br.com.tratomais.ws.einsurance.entities.PolicyLoadRequest;
import br.com.tratomais.ws.einsurance.entities.PolicyLoadResponse;
import br.com.tratomais.ws.einsurance.entities.PolicyPaymentRequest;
import br.com.tratomais.ws.einsurance.entities.PolicyPaymentResponse;
import br.com.tratomais.ws.einsurance.entities.PolicyPrintRequest;
import br.com.tratomais.ws.einsurance.entities.PolicyPrintResponse;
import br.com.tratomais.ws.einsurance.entities.PolicyProposalRequest;
import br.com.tratomais.ws.einsurance.entities.PolicyProposalResponse;
import br.com.tratomais.ws.einsurance.entities.PolicyProposalUnlockRequest;
import br.com.tratomais.ws.einsurance.entities.PolicyProposalUnlockResponse;
import br.com.tratomais.ws.einsurance.entities.PolicyQuotationRequest;
import br.com.tratomais.ws.einsurance.entities.PolicyQuotationResponse;
import br.com.tratomais.ws.einsurance.entities.PolicySearchRequest;
import br.com.tratomais.ws.einsurance.entities.PolicySearchResponse;
import br.com.tratomais.ws.einsurance.entities.PropertyCompleteType;
import br.com.tratomais.ws.einsurance.entities.PropertyType;
import br.com.tratomais.ws.einsurance.entities.RiskType;
import br.com.tratomais.ws.exceptions.ErrorListException;
import br.com.tratomais.ws.exceptions.ServerException;

@Endpoint
public class EInsuranceEndpoint {
	private static final String NAMESPACE = "http://www.tratomais.com.br/services/eInsurance";

	private static int ID_HIERARCHY_TYPE_DEFAULT = 164; //Corretor
	private static int ID_RESTRICTION_RISK_TYPE_DEFAULT = 537; //Accidentes que ocurran durante las 24 horas del d�a
	private static int REPORT_TYPE_INSTALLMENT = 766;  // Parcelas

	private DaoEndorsementImpl daoEndorsement;
	public void setDaoEndorsement(DaoEndorsementImpl daoEndorsement) {
		this.daoEndorsement = daoEndorsement;
	}

	@SuppressWarnings("unused")
	private String reportOutput = "PDF";
	public void setReportOutput(String reportOutput) {
		this.reportOutput = reportOutput;
	}

	@Resource(name="reportService")
	private ReportService serviceReport;
	public void setServiceReport(ReportService serviceReport) {
		this.serviceReport = serviceReport;
	}

	private ICalc serviceBasicCalc;
	public void setServiceBasicCalc(ICalc serviceBasicCalc) {
		this.serviceBasicCalc = serviceBasicCalc;
	}

	private DaoContract daoContract;
	public void setDaoContract(DaoContract daoContract) {
		this.daoContract = daoContract;
	}

	private IDaoCoveragePlan daoCoveragePlan;
	public void setDaoCoveragePlan(IDaoCoveragePlan daoCoveragePlan) {
		this.daoCoveragePlan = daoCoveragePlan;
	}

	private IDaoDataOption daoDataOption;
	public void setDaoDataOption(IDaoDataOption daoDataOption) {
		this.daoDataOption = daoDataOption;
	}

	private IDaoInsurer daoInsurer;
	public void setDaoInsurer(IDaoInsurer daoInsurer) {
		this.daoInsurer = daoInsurer;
	}

	private IDaoCountry daoCountry;
	public void setDaoCountry(IDaoCountry daoCountry) {
		this.daoCountry = daoCountry;
	}

	private IDaoState daoState;
	public void setDaoState(IDaoState daoState) {
		this.daoState = daoState;
	}

	private IDaoCity daoCity;
	public void setDaoCity(IDaoCity daoCity) {
		this.daoCity = daoCity;
	}
	
	private IDaoProductVersion daoProductVersion;
	public void setDaoProductVersion(IDaoProductVersion daoProductVersion) {
		this.daoProductVersion = daoProductVersion;
	}

	private IDaoPolicy daoPolicy;
	public void setDaoPolicy(IDaoPolicy daoPolicy) {
		this.daoPolicy = daoPolicy;
	}

	private IServicePolicy servicePolicy;
	public void setServicePolicy(IServicePolicy servicePolicy){
		this.servicePolicy = servicePolicy;
	}	

	private IServiceEinsurance serviceEinsurance;
	public void setServiceEinsurance(IServiceEinsurance serviceEinsurance){
		this.serviceEinsurance = serviceEinsurance;
	}

	private IServiceCollection serviceCollection;
	public void setServiceCollection(IServiceCollection serviceCollection) {
		this.serviceCollection = serviceCollection;
	}

	private Locale locale = Locale.getDefault();
	public void setLocale(Locale locale) {
		this.locale = locale;

	}

	private IDaoReportJasper daoReportJasper;
	public void setDaoReportJasper(IDaoReportJasper daoReportJasper) {
		this.daoReportJasper = daoReportJasper;
	}

	private String pastaBase;
	public void setPastaBase(String pastaBase) {
		this.pastaBase = pastaBase;
	}

	private IServiceSearchDocument serviceSearchDocument;
	public void setServiceSearchDocument(IServiceSearchDocument serviceSearchDocument){
		this.serviceSearchDocument = serviceSearchDocument;
	}

	@PayloadRoot(namespace=NAMESPACE, localPart="policyCalculateRequest")
	public PolicyCalculateResponse calculate(PolicyCalculateRequest policyCalculateRequest) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ServerException, DatatypeConfigurationException, ServiceException, ErrorListException {
		
		Endorsement endorsement = this.dataFillCalculate(policyCalculateRequest.getContainerXML().getContract());
		
		endorsement = serviceBasicCalc.simpleCalc(endorsement);
		
		if ((endorsement.getErrorList() != null && endorsement.getErrorList().getListaErros().size() > 0)) {
			throw new ErrorListException(endorsement.getErrorList());
		}
		
		PolicyCalculateResponse policyCalculateResponse = new PolicyCalculateResponse();
		policyCalculateResponse.setContract(this.convertToContract(endorsement));
		
		return policyCalculateResponse;
	}

	@Transactional(propagation=Propagation.REQUIRED)
	@PayloadRoot(namespace=NAMESPACE, localPart="policyQuotationRequest")
	public PolicyQuotationResponse policyQuotation(PolicyQuotationRequest policyQuotationRequest) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ServerException, DatatypeConfigurationException, ServiceException, ErrorListException {
		
		Endorsement endorsement = this.dataFillQuotation(policyQuotationRequest.getContainerXML().getContract());
		
		endorsement = serviceBasicCalc.simpleCalc(endorsement);
		
		if ((endorsement.getErrorList() != null && endorsement.getErrorList().getListaErros().size() > 0)) {
			throw new ErrorListException(endorsement.getErrorList());
		}
		
		// Save insured
		endorsement.setCustomerByInsuredId(daoPolicy.saveCustomer(endorsement.getCustomerByInsuredId()));
		
		// Save policy
		daoPolicy.saveEndorsement(endorsement);
		
		// Update contractId after save
		endorsement.getContract().setContractId(endorsement.getId().getContractId());
		daoPolicy.updateContract(endorsement.getContract());
		
		PolicyQuotationResponse policyQuotationResponse = new PolicyQuotationResponse();
		policyQuotationResponse.setContract(this.convertToContract(endorsement));
		
		return policyQuotationResponse;
	}

	@PayloadRoot(namespace = NAMESPACE, localPart = "policyPaymentRequest")
	public PolicyPaymentResponse policyPayment(PolicyPaymentRequest policyPaymentRequest) throws ServerException{
		PolicyPaymentResponse policyPaymentResponse = new PolicyPaymentResponse();
		
		EndorsementId id = new EndorsementId();
		id.setContractId(policyPaymentRequest.getContractID());
		id.setEndorsementId(policyPaymentRequest.getEndorsementID());
		Endorsement endorsement = daoEndorsement.findById(id);
		if (endorsement == null) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_CONTRACT_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_CONTRACT_INVALID.getMessage(null));
		}
		
		List<PaymentOption> paymentOptionsByBillingMethodId = serviceBasicCalc.getPaymentOptionsByBillingMethodId(endorsement, policyPaymentRequest.getBillingMethodID(), endorsement.getEffectiveDate(), endorsement.getExpiryDate());
		if (paymentOptionsByBillingMethodId == null || paymentOptionsByBillingMethodId.size() == 0) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_PAYMENT_OPTION_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PAYMENT_OPTION_INVALID.getMessage(null));
		}
		for (PaymentOption paymentOption : paymentOptionsByBillingMethodId) {
			PaymentOptionRequestedType paymentOptionRequested = new PaymentOptionRequestedType();
			paymentOptionRequested.setBillingMethodID(paymentOption.getBillingMethodId());
			paymentOptionRequested.setPaymentTermID(paymentOption.getPaymentId());
			paymentOptionRequested.setDescription(paymentOption.getPaymentTermName());
			paymentOptionRequested.setQuantity(paymentOption.getQuantityInstallment());
			paymentOptionRequested.setFirstValue(new BigDecimal(paymentOption.getFirstInstallmentValue()).setScale(2,RoundingMode.HALF_EVEN));
			paymentOptionRequested.setNextValue(new BigDecimal(paymentOption.getNextInstallmentValue()).setScale(2,RoundingMode.HALF_EVEN));
			paymentOptionRequested.setNetPremium(new BigDecimal(endorsement.getNetPremium()).setScale(2,RoundingMode.HALF_EVEN));
			paymentOptionRequested.setFractioningAdditional((new BigDecimal(paymentOption.getFractioningAdditional())).setScale(2,RoundingMode.HALF_EVEN));
			paymentOptionRequested.setTaxValue((new BigDecimal(endorsement.getTaxValue())).setScale(2,RoundingMode.HALF_EVEN));
			paymentOptionRequested.setTotalPremium(new BigDecimal(paymentOption.getTotalPremium()).setScale(2,RoundingMode.HALF_EVEN));
			paymentOptionRequested.setStandard(paymentOption.isDefault());
			
			policyPaymentResponse.getPaymentOption().add(paymentOptionRequested);
		}
		
		return policyPaymentResponse;
	}

	@Transactional(propagation=Propagation.REQUIRED)
	@PayloadRoot(namespace=NAMESPACE, localPart="policyProposalRequest")
	public PolicyProposalResponse policyProposal(PolicyProposalRequest policyProposalRequest) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ServerException, DatatypeConfigurationException, ServiceException, ErrorListException {
		
		Endorsement endorsement = this.dataFillProposal(policyProposalRequest.getContainerXML().getContract());
		PaymentOption paymentOption = this.dataFillPaymentOption(policyProposalRequest.getPaymentOption(), endorsement);
		
		endorsement = serviceBasicCalc.effectiveProposal(endorsement, paymentOption);
		
		if ((endorsement.getErrorList() != null && endorsement.getErrorList().getListaErros().size() > 0)) {
			throw new ErrorListException(endorsement.getErrorList());
		}
		
		// Save insured y holder
		// Check insured equals policy holder
		if (endorsement.getCustomerByInsuredId() == endorsement.getCustomerByPolicyHolderId()) {
			endorsement.setCustomerByInsuredId(daoPolicy.saveCustomer(endorsement.getCustomerByInsuredId()));
			endorsement.setCustomerByPolicyHolderId(endorsement.getCustomerByInsuredId());
		}
		else {
			endorsement.setCustomerByInsuredId(daoPolicy.saveCustomer(endorsement.getCustomerByInsuredId()));
			endorsement.setCustomerByPolicyHolderId(daoPolicy.saveCustomer(endorsement.getCustomerByPolicyHolderId()));
		}
		
		// Save policy
		daoPolicy.saveEndorsement(endorsement);
		
		// Update contractId after save
		endorsement.getContract().setContractId(endorsement.getId().getContractId());
		daoPolicy.updateContract(endorsement.getContract());
		
		try {
			serviceCollection.dispachCollection(endorsement, false);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EInsuranceJMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PolicyProposalResponse policyProposalResponse = new PolicyProposalResponse();
		policyProposalResponse.setContract(this.convertToContract(endorsement));
		
		return policyProposalResponse;
	}

	@PayloadRoot(namespace=NAMESPACE, localPart="policyPrintRequest")
	public PolicyPrintResponse policyPrint(PolicyPrintRequest policyPrintRequest) throws ServerException {
		PolicyPrintResponse resposta = new PolicyPrintResponse();
		
		// validate Print
		Endorsement endorsement = daoEndorsement.getStatusById(policyPrintRequest.getContractID(), policyPrintRequest.getEndorsementID());
		if (endorsement == null) {
			throw new ServerException(352, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		else {
			if (endorsement.getIssuingStatus() == Endorsement.ISSUING_STATUS_COTACAO) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PRINT_NOT_ALLOWED.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PRINT_NOT_ALLOWED.getMessage(null));
			}
		}
		
		// select ReportID
		Integer reportId = null;
		if (policyPrintRequest.getReportTypeID() == REPORT_TYPE_INSTALLMENT)
			reportId = 8; // query_Installment.jasper
		else
			reportId = daoReportJasper.getReportIdByType(policyPrintRequest.getContractID(), policyPrintRequest.getEndorsementID(), policyPrintRequest.getReportTypeID());
		
		// validate reportID
		if (reportId == null || reportId.equals(0)) {
			throw new ServerException(3512, 772, ErrorTypes.VALIDATION_ACTION_IS_NOT_ALLOWED.getMessage(null));
		}
		
		ReportTO rto = serviceReport.getReport(reportId.longValue());
		
		// create a list of fields from form with selected values and set into ReportTO
        List<ReportFieldTO> formFields = new ArrayList<ReportFieldTO>();
        
	    @SuppressWarnings("unchecked")
		List<ReportFieldTO> fields = serviceReport.getReportFields(rto.getSqlStement());
	    
	    
		String valorCampoEndorsement = String.format("EndorsementID_%d", reportId);
	    String valorCampoContract = String.format("ContractID_%d", reportId);
	    
	    
	    if (fields != null) {
	    	for(ReportFieldTO fieldTO:fields) {
	    		String 	value = null,
	    				fieldName = fieldTO.getFieldToHtml(rto);
	    		
				if (valorCampoEndorsement.equals(fieldName))
	                value = policyPrintRequest.getEndorsementID() + "";
	    		else if (valorCampoContract.equals(fieldName))
	                value = policyPrintRequest.getContractID() + "";
	    		
                if (!value.equals("") || fieldTO.getReportFieldType().equals(ReportFieldTO.TYPE_OBJECT)) {
                	fieldTO.setValue(value);
		            formFields.add(fieldTO);	                    
                }
            }
            rto.setFormFieldsValues(formFields);
        }
	    rto.setLocale(this.locale);
        rto.setHandler("einsurance");
        
    	try {
    		String pathReports = pastaBase + (rto.getProfile().charAt(0) == File.separatorChar ? "" : File.separatorChar) + rto.getProfile();
    		byte[] reportStream = serviceReport.performJasperReport(rto, pathReports);
    		if (reportStream!=null) {
    			resposta.setStreamingPDF(reportStream);
    		}
        }
    	catch (Exception e) {
        	e.printStackTrace();
        }
        
		return resposta;
	}

	@PayloadRoot(namespace=NAMESPACE, localPart="policySearchRequest")
	public PolicySearchResponse policySearch(PolicySearchRequest policySearchRequest) throws ServerException{
		PolicySearchResponse policySearchResponse = new PolicySearchResponse();
		
		PolicySearchRequest.ParamsXML paramsXML = policySearchRequest.getParamsXML();
		if (paramsXML.getContractID() != null && !(new Integer(0)).equals(paramsXML.getContractID())) {
			// clean Other
			paramsXML.setPolicyNumber(null);
			paramsXML.setCertificateNumber(null);
			paramsXML.setDocumentTypeID(null);
			paramsXML.setDocumentNumber(null);
		}
		else if ((paramsXML.getPolicyNumber() != null && !(new Long(0)).equals(paramsXML.getPolicyNumber())) ||
				 (paramsXML.getCertificateNumber() != null && !(new Integer(0)).equals(paramsXML.getCertificateNumber()))) {
			
			if (paramsXML.getPolicyNumber() == null || (new Long(0)).equals(paramsXML.getPolicyNumber()))
				throw new ServerException(new Long(ErrorTypes.VALIDATION_POLICYNUMBER_REQUIRED.getErrorCode()).intValue(), ErrorTypes.VALIDATION_POLICYNUMBER_REQUIRED.getErrorType(), ErrorTypes.VALIDATION_POLICYNUMBER_REQUIRED.getMessage(null));
			
			if (paramsXML.getCertificateNumber() == null || (new Integer(0)).equals(paramsXML.getCertificateNumber()))
				throw new ServerException(new Long(ErrorTypes.VALIDATION_CERTIFICATE_NUMBER_REQUIRED.getErrorCode()).intValue(), ErrorTypes.VALIDATION_CERTIFICATE_NUMBER_REQUIRED.getErrorType(), ErrorTypes.VALIDATION_CERTIFICATE_NUMBER_REQUIRED.getMessage(null));
			
			// clean Other
			paramsXML.setContractID(null);
			paramsXML.setDocumentTypeID(null);
			paramsXML.setDocumentNumber(null);
		}
		else if ((paramsXML.getDocumentTypeID() != null && !(new Integer(0)).equals(paramsXML.getDocumentTypeID())) ||
				 (paramsXML.getDocumentNumber() != null && !(new BigDecimal(0)).equals(paramsXML.getDocumentNumber()))) {

			if (paramsXML.getDocumentTypeID() == null || (new Integer(0)).equals(paramsXML.getDocumentTypeID()))
				throw new ServerException(new Long(ErrorTypes.VALIDATION_DOCUMENT_TYPE_INVALID.getErrorCode()).intValue(), ErrorTypes.VALIDATION_DOCUMENT_TYPE_INVALID.getErrorType(), ErrorTypes.VALIDATION_DOCUMENT_TYPE_INVALID.getMessage(null));
		
			if (paramsXML.getDocumentNumber() == null || (new BigDecimal(0)).equals(paramsXML.getDocumentNumber()))
				throw new ServerException(new Long(ErrorTypes.VALIDATION_DOCUMENT_NUMBER_REQUIRED.getErrorCode()).intValue(), ErrorTypes.VALIDATION_DOCUMENT_NUMBER_REQUIRED.getErrorType(), ErrorTypes.VALIDATION_DOCUMENT_NUMBER_REQUIRED.getMessage(null));
			
			// clean Other
			paramsXML.setContractID(null);
			paramsXML.setPolicyNumber(null);
			paramsXML.setCertificateNumber(null);
		}
		else {
			throw new ServerException(new Long(ErrorTypes.VALIDATION_CONTRACT_REQUIRED.getErrorCode()).intValue(), ErrorTypes.VALIDATION_CONTRACT_REQUIRED.getErrorType(), ErrorTypes.VALIDATION_CONTRACT_REQUIRED.getMessage(null));					
		}
		
		SearchDocumentParameters parameters = new SearchDocumentParameters();
		
		Utilities.copyToObjectDifferent(policySearchRequest, parameters);
		Utilities.copyToObjectDifferent(policySearchRequest.getParamsXML(), parameters);
		
		parameters.setPage(policySearchRequest.getPageNumber());
		parameters.setTotalPage(policySearchRequest.getRecordPerPage());
		
		List<SearchDocumentResult> listSearchDocument = serviceSearchDocument.listSearchDocument(parameters, false);
		if (listSearchDocument != null && listSearchDocument.size() > 0) {
			for (SearchDocumentResult searchDocumentResult : listSearchDocument) {
				PolicyCompleteType policyCompleteType = new PolicyCompleteType();
				
				Utilities.copyToObjectDifferent(searchDocumentResult, policyCompleteType);
				policyCompleteType.setChannelCode(searchDocumentResult.getChannelExternalCode());
				policyCompleteType.setContractStatusDescription(searchDocumentResult.getContractStatus());
				policyCompleteType.setIssuanceStatusID(searchDocumentResult.getIssuingStatusId());
				policyCompleteType.setIssuanceStatusDescription(searchDocumentResult.getIssuingStatus());
				policyCompleteType.setIssuanceTypeDescription(searchDocumentResult.getIssuanceType());
				policyCompleteType.setProductNickName(searchDocumentResult.getProductName());
				
				policySearchResponse.getResult().add(policyCompleteType);
			}
			policySearchResponse.setRecordCount(listSearchDocument.size());
		}
		else {
			throw new ServerException(362, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return policySearchResponse;
	}

	@PayloadRoot(namespace=NAMESPACE, localPart="policyLoadRequest")
	public PolicyLoadResponse policyLoad(PolicyLoadRequest policyLoadRequest) throws SecurityException, IllegalArgumentException, ServerException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, DatatypeConfigurationException{
		PolicyLoadResponse policyLoadResponse = new PolicyLoadResponse();
		
		Endorsement endorsement = daoPolicy.getEndosement(policyLoadRequest.getContractID(), policyLoadRequest.getEndorsementID());
		if (endorsement == null) {
			throw new ServerException(372, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		else {
			// remove Object from the Session
			daoPolicy.evict(endorsement);
			policyLoadResponse.setContract(this.convertToContract(endorsement));
		}
		
		return policyLoadResponse;
	}

	@PayloadRoot(namespace=NAMESPACE, localPart="policyInstallmentSearchRequest")
	public PolicyInstallmentSearchResponse policyInstallmentSearch(PolicyInstallmentSearchRequest policyInstallmentSearchRequest) throws ServerException{
		PolicyInstallmentSearchResponse policyInstallmentSearchResponse = new PolicyInstallmentSearchResponse();
		
		PolicyInstallmentSearchRequest.ParamsXML paramsXML = policyInstallmentSearchRequest.getParamsXML();
		if (paramsXML.getContractID() != null && !(new Integer(0)).equals(paramsXML.getContractID())) {
			// clean Other
			paramsXML.setPolicyNumber(null);
			paramsXML.setCertificateNumber(null);
			paramsXML.setDocumentTypeID(null);
			paramsXML.setDocumentNumber(null);
		}
		else if ((paramsXML.getPolicyNumber() != null && !(new Long(0)).equals(paramsXML.getPolicyNumber())) ||
				 (paramsXML.getCertificateNumber() != null && !(new Integer(0)).equals(paramsXML.getCertificateNumber()))) {
			
			if (paramsXML.getPolicyNumber() == null || (new Long(0)).equals(paramsXML.getPolicyNumber()))
				throw new ServerException(new Long(ErrorTypes.VALIDATION_POLICYNUMBER_REQUIRED.getErrorCode()).intValue(), ErrorTypes.VALIDATION_POLICYNUMBER_REQUIRED.getErrorType(), ErrorTypes.VALIDATION_POLICYNUMBER_REQUIRED.getMessage(null));
			
			if (paramsXML.getCertificateNumber() == null || (new Integer(0)).equals(paramsXML.getCertificateNumber()))
				throw new ServerException(new Long(ErrorTypes.VALIDATION_CERTIFICATE_NUMBER_REQUIRED.getErrorCode()).intValue(), ErrorTypes.VALIDATION_CERTIFICATE_NUMBER_REQUIRED.getErrorType(), ErrorTypes.VALIDATION_CERTIFICATE_NUMBER_REQUIRED.getMessage(null));
			
			// clean Other
			paramsXML.setContractID(null);
			paramsXML.setDocumentTypeID(null);
			paramsXML.setDocumentNumber(null);
		}
		else if ((paramsXML.getDocumentTypeID() != null && !(new Integer(0)).equals(paramsXML.getDocumentTypeID())) ||
				 (paramsXML.getDocumentNumber() != null && !(new BigDecimal(0)).equals(paramsXML.getDocumentNumber()))) {

			if (paramsXML.getDocumentTypeID() == null || (new Integer(0)).equals(paramsXML.getDocumentTypeID()))
				throw new ServerException(new Long(ErrorTypes.VALIDATION_DOCUMENT_TYPE_INVALID.getErrorCode()).intValue(), ErrorTypes.VALIDATION_DOCUMENT_TYPE_INVALID.getErrorType(), ErrorTypes.VALIDATION_DOCUMENT_TYPE_INVALID.getMessage(null));
		
			if (paramsXML.getDocumentNumber() == null || (new BigDecimal(0)).equals(paramsXML.getDocumentNumber()))
				throw new ServerException(new Long(ErrorTypes.VALIDATION_DOCUMENT_NUMBER_REQUIRED.getErrorCode()).intValue(), ErrorTypes.VALIDATION_DOCUMENT_NUMBER_REQUIRED.getErrorType(), ErrorTypes.VALIDATION_DOCUMENT_NUMBER_REQUIRED.getMessage(null));
			
			// clean Other
			paramsXML.setContractID(null);
			paramsXML.setPolicyNumber(null);
			paramsXML.setCertificateNumber(null);
		}
		else {
			throw new ServerException(new Long(ErrorTypes.VALIDATION_CONTRACT_REQUIRED.getErrorCode()).intValue(), ErrorTypes.VALIDATION_CONTRACT_REQUIRED.getErrorType(), ErrorTypes.VALIDATION_CONTRACT_REQUIRED.getMessage(null));					
		}
		
		//check Status
		if (paramsXML.isInArrears() != null && paramsXML.isInArrears()) {
			paramsXML.setStatusID(null);
		} 
		else if (paramsXML.getStatusID() != null && !(new Integer(0)).equals(paramsXML.getStatusID())) {
			DataOption dataOption = daoDataOption.getByFieldName("InstallmentStatus", paramsXML.getStatusID());
			if (dataOption == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_INSTALLMENT_STATUS_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_INSTALLMENT_STATUS_INVALID.getMessage(null));
			}
		}
		
		InstallmentParameters installmentParameters = new InstallmentParameters();
		
		Utilities.copyToObjectDifferent(policyInstallmentSearchRequest.getParamsXML(), installmentParameters);
		if (paramsXML.isInArrears() != null && paramsXML.isInArrears())
			installmentParameters.setInArrears(true);
		
		installmentParameters.setPage(policyInstallmentSearchRequest.getPageNumber());
		installmentParameters.setTotalPage(policyInstallmentSearchRequest.getRecordPerPage());
		
		List<InstallmentResult> listInstallment = serviceSearchDocument.listInstallment(installmentParameters);
		if (listInstallment.size() > 0) {
			for (InstallmentResult installmentResult : listInstallment) {
				InstallmentResultType installmentResultType = new InstallmentResultType();
				
				Utilities.copyToObjectDifferent(installmentResult, installmentResultType);
				
				policyInstallmentSearchResponse.getContract().add(installmentResultType);
			}
			policyInstallmentSearchResponse.setRecordCount(listInstallment.size());
		}
		else {
			throw new ServerException(382, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return policyInstallmentSearchResponse;
	}

	@Transactional(propagation=Propagation.REQUIRED)
	@PayloadRoot(namespace=NAMESPACE, localPart="policyProposalUnlockRequest")
	public PolicyProposalUnlockResponse policyProposalUnlock(PolicyProposalUnlockRequest policyProposalUnlockRequest) throws ServerException {
		
		// validate basic
		if (policyProposalUnlockRequest.getEndorsementID() != 1) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_PROPOSAL_UNLOCK_NOT_ALLOWED.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PROPOSAL_UNLOCK_NOT_ALLOWED.getMessage(null));
		}
		
		Endorsement endorsement = daoEndorsement.getStatusById(policyProposalUnlockRequest.getContractID(), policyProposalUnlockRequest.getEndorsementID());
		if (endorsement == null) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		else {
			if (endorsement.getIssuingStatus() !=  Endorsement.ISSUING_STATUS_PROPOSTA) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PROPOSAL_UNLOCK_NOT_ALLOWED.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PROPOSAL_UNLOCK_NOT_ALLOWED.getMessage(null));
			}
		}
		
		Boolean unlocked = servicePolicy.unlockEndorsement(policyProposalUnlockRequest.getEndorsementID(), policyProposalUnlockRequest.getContractID(), DateUtilities.xmlGregorianCalendarToDate(policyProposalUnlockRequest.getUnlockDate()));
		
		PolicyProposalUnlockResponse policyProposalUnlockResponse = new PolicyProposalUnlockResponse();
		policyProposalUnlockResponse.setUnlocked(unlocked);
		
		return policyProposalUnlockResponse;
	}

	@Transactional(propagation=Propagation.REQUIRED)
	@PayloadRoot(namespace=EInsuranceEndpoint.NAMESPACE, localPart="policyCancellationByErrorRequest")
	public PolicyCancellationByErrorResponse policyCancellationByError(PolicyCancellationByErrorRequest policyCancellationByErrorRequest) throws SecurityException, IllegalArgumentException, ServerException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, DatatypeConfigurationException, ServiceException {
		
		//check Cancel Reason
		DataOption dataOption  = daoDataOption.getByFieldName("CancelReason", policyCancellationByErrorRequest.getCancelReasonID());
		if (dataOption == null) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_CANCEL_REASON_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_CANCEL_REASON_INVALID.getMessage(null));
		}
		
		Endorsement endorsement = servicePolicy.policyCancellationByError(policyCancellationByErrorRequest.getContractID(), DateUtilities.xmlGregorianCalendarToDate(policyCancellationByErrorRequest.getCancelDate()), policyCancellationByErrorRequest.getCancelReasonID());
		ContractCompleteType contract = this.convertToContract(endorsement);
		
		PolicyCancellationByErrorResponse policyCancellationByErrorResponse = new PolicyCancellationByErrorResponse();
		policyCancellationByErrorResponse.setContract(contract);
		
		return policyCancellationByErrorResponse;
	}

	@Transactional(propagation=Propagation.REQUIRED)
	@PayloadRoot(namespace=NAMESPACE, localPart="policyCancellationByInsuredRequest")
	public PolicyCancellationByInsuredResponse policyCancellationByInsured(PolicyCancellationByInsuredRequest policyCancellationByInsuredRequest) throws ServerException, SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, DatatypeConfigurationException, ServiceException {
		
		//check Cancel Reason
		DataOption dataOption  = daoDataOption.getByFieldName("CancelReason", policyCancellationByInsuredRequest.getCancelReasonID());
		if (dataOption == null) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_CANCEL_REASON_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_CANCEL_REASON_INVALID.getMessage(null));
		}
		
		Endorsement endorsement = servicePolicy.policyCancellationByInsured(policyCancellationByInsuredRequest.getContractID(), DateUtilities.xmlGregorianCalendarToDate(policyCancellationByInsuredRequest.getCancelDate()), policyCancellationByInsuredRequest.getCancelReasonID());
		ContractCompleteType contract = this.convertToContract(endorsement);
		
		PolicyCancellationByInsuredResponse policyCancellationByInsuredResponse = new PolicyCancellationByInsuredResponse(); 
		policyCancellationByInsuredResponse.setContract(contract);
		
		return policyCancellationByInsuredResponse;
	}

	@Transactional(propagation=Propagation.REQUIRED)
	@PayloadRoot(namespace=NAMESPACE, localPart="policyChangeRegisterRequest")
	public PolicyChangeRegisterResponse policyChangeRegister(PolicyChangeRegisterRequest policyChangeRegisterRequest) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ServerException, DatatypeConfigurationException, ServiceException, ErrorListException {
		
		Endorsement endorsement = this.dataFillChangeRegister(policyChangeRegisterRequest.getContainerXML().getContract());
		
		endorsement = servicePolicy.policyChangeRegister(endorsement, DateUtilities.xmlGregorianCalendarToDate(policyChangeRegisterRequest.getChangeDate()));
		
		if ((endorsement.getErrorList() != null && endorsement.getErrorList().getListaErros().size() > 0)) {
			throw new ErrorListException(endorsement.getErrorList());
		}
		
		PolicyChangeRegisterResponse policyChangeRegisterResponse = new PolicyChangeRegisterResponse();
		policyChangeRegisterResponse.setContract(this.convertToContract(endorsement));
		
		return policyChangeRegisterResponse; 
	}

	@Transactional(propagation=Propagation.REQUIRED)
	@PayloadRoot(namespace=NAMESPACE, localPart="policyChangeTechnicalRequest")
	public PolicyChangeTechnicalResponse policyChangeTechnical(PolicyChangeTechnicalRequest policyChangeTechnicalRequest) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ServerException, DatatypeConfigurationException, ServiceException, ErrorListException {
		
		Endorsement endorsement = this.dataFillChangeTechnical(policyChangeTechnicalRequest.getContainerXML().getContract());
		
		// only Quotation
		endorsement = servicePolicy.policyChangeTechnical(endorsement, DateUtilities.xmlGregorianCalendarToDate(policyChangeTechnicalRequest.getChangeDate()), false, false, null);
		if ((endorsement.getErrorList() != null && endorsement.getErrorList().getListaErros().size() > 0)) {
			throw new ErrorListException(endorsement.getErrorList());
		}		
		
		PaymentOption paymentOption = this.dataFillPaymentOption(policyChangeTechnicalRequest.getPaymentOption(), endorsement);
		
		// complete Issue
		endorsement = servicePolicy.policyChangeTechnical(endorsement, DateUtilities.xmlGregorianCalendarToDate(policyChangeTechnicalRequest.getChangeDate()), true, true, paymentOption);
		if ((endorsement.getErrorList() != null && endorsement.getErrorList().getListaErros().size() > 0)) {
			throw new ErrorListException(endorsement.getErrorList());
		}
		
		PolicyChangeTechnicalResponse policyChangeTechnicalResponse = new PolicyChangeTechnicalResponse();
		policyChangeTechnicalResponse.setContract(this.convertToContract(endorsement));
		
		return policyChangeTechnicalResponse;
	}

	@PayloadRoot(namespace=NAMESPACE, localPart="policyInstallmentPayRequest")
	public PolicyInstallmentPayResponse policyInstallmentPay(PolicyInstallmentPayRequest policyInstallmentPayRequest) throws ServerException, EInsuranceJMSException, SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, DatatypeConfigurationException {
		PolicyInstallmentPayResponse response = new PolicyInstallmentPayResponse();
		
		//fill Installment
		Installment installment = this.dataFillCollectionPay(policyInstallmentPayRequest.getContainerXML().getInstallment());
		
		//fill Action
		installment.setPaymentNotified(policyInstallmentPayRequest.isOnlyNotification());
		if (!policyInstallmentPayRequest.isOnlyNotification())
			installment.setInstallmentStatus(Installment.INSTALLMENT_STATUS_PAID);
		
		if (!servicePolicy.dispachCollectionPay(installment)) {
			throw new ServerException(3142, 772, ErrorTypes.PROCESSING_ERROR1.getMessage(null));
		}
		
		// response Installment
		InstallmentType installmentResponse = this.convertToInstallment(installment);
		response.setInstallment(new PolicyInstallmentPayResponse.Installment());
		BeanUtils.copyProperties(installmentResponse, response.getInstallment());
		
		// response Complement
		response.getInstallment().setContractID(installment.getId().getContractId());
		response.getInstallment().setEndorsementID(installment.getId().getEndorsementId());
		response.getInstallment().setCurrencyRate((BigDecimal)Utilities.convertNullValue(NumberUtilities.doubleToBigDecimal(installment.getConversionRate()), 1.0));
		response.getInstallment().setAuthorizationCode(installment.getCreditAuthorizationCode().toString());
		response.getInstallment().setStatusID(installment.getInstallmentStatus());
		
		return response;
	}

	@PayloadRoot(namespace=NAMESPACE, localPart="getContractRequest")
	public GetContractResponse getContract(GetContractRequest request) throws ServerException, ServiceException, DatatypeConfigurationException {
		GetContractResponse resposta = new GetContractResponse();
		
		//find Contract
		Contract contract = this.daoContract.findById(request.getContractID());
		if (contract == null) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		else {
			ContractCompleteDescriptiveType contractResponse = new ContractCompleteDescriptiveType();
			Utilities.copyToObjectDifferent(contract, contractResponse);
			
			//fill Contract
			if (contract.getMasterPolicyId() != null)
				contractResponse.setPolicyID(contract.getMasterPolicyId());
			if (contract.getReferenceDate() != null)
				contractResponse.setVersionDate(DateUtilities.dateToXMLGregorianCalendar(contract.getReferenceDate()));
			if (contract.getRenewalLimitDate() != null)
				contractResponse.setPermanencyDate(DateUtilities.dateToXMLGregorianCalendar(contract.getRenewalLimitDate()));
			contractResponse.setStatusID((Integer)Utilities.convertNullValue(contract.getContractStatus(), 0));
			
			//fill Insurer
			Insurer insurer = daoInsurer.findById(contract.getInsurerId());
			if (insurer != null) {
				contractResponse.setInsurerCode(insurer.getExternalCode());
				contractResponse.setInsurerNickName(insurer.getNickName());
			}
			
			//fill Subsidiary
			Subsidiary subsidiary = serviceEinsurance.findSubsidiaryById(contract.getSubsidiaryId());
			if (subsidiary != null) {
				contractResponse.setSubsidiaryCode(subsidiary.getExternalCode());
				contractResponse.setSubsidiaryNickName(subsidiary.getNickName());
			}
			
			//fill Broker
			Broker broker = serviceEinsurance.findBrokerById(contract.getBrokerId());
			if (broker != null) {
				contractResponse.setBrokerCode(broker.getExternalCode());
				contractResponse.setBrokerNickName(broker.getNickName());
			}
			
			//fill Partner
			Partner partner = serviceEinsurance.findPartnerById(contract.getPartnerId());
			if (partner != null) {
				contractResponse.setPartnerCode(partner.getExternalCode());
				contractResponse.setPartnerNickName(partner.getNickName());
			}
			
			//fill Product
			ProductVersion productVersion = serviceEinsurance.getProductVersionByRefDate(contract.getProductId(), contract.getReferenceDate());			
			if (productVersion != null) {
				Product product = productVersion.getProduct();
				if (product != null) {
					contractResponse.setProductCode(product.getProductCode());
					contractResponse.setProductNickName(product.getName());
				}
			}
			
			//fill Currency
			Currency currency = serviceEinsurance.getCurrency(contract.getCurrencyId(), contract.getEffectiveDate());
			if (currency != null) {
				contractResponse.setCurrencyCode(currency.getIsocode());
				contractResponse.setCurrencySymbol(currency.getSymbol());
				contractResponse.setCurrencyNickName(currency.getName());
			}
			
			//fill Term
			ProductTerm productTerm = serviceEinsurance.getProductTermById(contract.getProductId(), contract.getTermId(), contract.getReferenceDate());
			if (productTerm != null) {
				Term term = productTerm.getTerm();
				if (term != null) {
					contractResponse.setTermCode(term.getTermCode());
					contractResponse.setTermNickName(term.getName());
				}
			}
			
			//fill Status
			DataOption status = serviceEinsurance.getDataOptionById(contract.getContractStatus());
			if (status != null) {
				contractResponse.setStatusCode(status.getFieldValue());
				contractResponse.setStatusDescription(status.getFieldDescription());
			}
			
			resposta.setContract(contractResponse);
		}
		
		return resposta;
	}

	@PayloadRoot(namespace=NAMESPACE, localPart="getInstallmentRequest")
	public GetInstallmentResponse getInstallment(GetInstallmentRequest request) throws ServerException, SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, DatatypeConfigurationException {
		GetInstallmentResponse resposta = new GetInstallmentResponse();
		
		//find Installment
		Installment installment = daoPolicy.findInstallment(request.getContractID(), request.getEndorsementID(), request.getInstallmentID());
		if (installment == null) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		else {
			resposta.setInstallment(this.convertToInstallment(installment));
		}
		
		return resposta;
	}

	private Installment dataFillCollectionPay(PolicyInstallmentPayRequest.ContainerXML.Installment installmentPay) throws ServerException {
		//find Endorsement
		Endorsement endorsement = daoPolicy.getEndosement(installmentPay.getContractID(), installmentPay.getEndorsementID());
		if (endorsement == null) {
			throw new ServerException(3142, 772, ErrorTypes.VALIDATION_CONTRACT_INVALID.getMessage(null));
		}
		
		// remove Object from the Session
		daoPolicy.evict(endorsement);
		
		//find Installment
		Installment installment = daoPolicy.findInstallment(endorsement.getProposalNumber(), installmentPay.getInstallmentID());
		if (installment == null) {
			throw new ServerException(3142, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		else if (installment.getInstallmentStatus() != Installment.INSTALLMENT_STATUS_PENDING) {
			throw new ServerException(3142, 772, ErrorTypes.VALIDATION_ACTION_IS_NOT_ALLOWED.getMessage(null));
		}
		else if (!installment.getInstallmentValue().equals((Double)(installmentPay.getPaidValue()!=null?installmentPay.getPaidValue().doubleValue():0.0))) {
			throw new ServerException(3142, 772, ErrorTypes.VALIDATION_ACTION_IS_NOT_ALLOWED.getMessage(null));
		}
		else if (installment.isPaymentNotified()) {
			throw new ServerException(3142, 772, ErrorTypes.VALIDATION_ACTION_IS_NOT_ALLOWED.getMessage(null));
		}
		
		//fill Complement
		installment.setPaymentDate(DateUtilities.xmlGregorianCalendarToDate(installmentPay.getPaymentDate()));
		installment.setPaidValue((Double)installmentPay.getPaidValue().doubleValue());
		installment.setCurrencyDate(DateUtilities.xmlGregorianCalendarToDate(installmentPay.getCurrencyDate()));
		installment.setConversionRate((Double)(installmentPay.getCurrencyRate()!=null?installmentPay.getCurrencyRate().doubleValue():1.0));
		installment.setCreditAuthorizationCode(new Long(installmentPay.getAuthorizationCode()));
		
		return installment;
	}

	private ContractCompleteType convertToContract(Endorsement endorsement) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ServerException, DatatypeConfigurationException {
		ContractCompleteType contractResponse = new ContractCompleteType();
		Contract contract = endorsement.getContract();
		
		//response Contract
		Utilities.copyToObjectDifferent(contract, contractResponse);
		
		//response Complement
		if (contract.getMasterPolicyId() != null)
			contractResponse.setPolicyID(contract.getMasterPolicyId());
		
		if (contract.getRenewalLimitDate() != null)
			contractResponse.setPermanencyDate(DateUtilities.dateToXMLGregorianCalendar(contract.getRenewalLimitDate()));
		
		contractResponse.setStatusID((Integer)Utilities.convertNullValue(contract.getContractStatus(), 0));
		
		//response Endorsement
		contractResponse.setEndorsement(this.convertToEndorsement(endorsement));
		
		return contractResponse;
	}
	
	private EndorsementCompleteType convertToEndorsement(Endorsement endorsement) throws ServerException, DatatypeConfigurationException, SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException{
		EndorsementCompleteType endorsementResponse = new EndorsementCompleteType();
		
		//response Endorsement
		Utilities.copyToObjectDifferent(endorsement.getId(), endorsementResponse);
		Utilities.copyToObjectDifferent(endorsement, endorsementResponse);
		
		//response Complement
		endorsementResponse.setVersionDate(DateUtilities.dateToXMLGregorianCalendar(endorsement.getReferenceDate()));
		endorsementResponse.setExemptionTypeID((Integer)Utilities.convertNullValue(endorsement.getTaxExemptionType(), 0));
		endorsementResponse.setComissionValue((BigDecimal)Utilities.convertNullValue(NumberUtilities.doubleToBigDecimal(endorsement.getCommission()), 0));
		endorsementResponse.setLabourValue((BigDecimal)Utilities.convertNullValue(NumberUtilities.doubleToBigDecimal(endorsement.getLabour()), 0));
		endorsementResponse.setCurrencyRate((BigDecimal)Utilities.convertNullValue(NumberUtilities.doubleToBigDecimal(endorsement.getConversionRate()), 0));
		endorsementResponse.setStatusID((Integer)Utilities.convertNullValue(endorsement.getIssuingStatus(), 0));
		
		//response Insured
		endorsementResponse.setInsured(this.convertToInsured(endorsement.getCustomerByInsuredId()));
		
		//response Holder
		if (endorsement.getCustomerByPolicyHolderId() != null && 
				endorsement.getCustomerByPolicyHolderId() != endorsement.getCustomerByInsuredId()) {
			endorsementResponse.setHolder(this.convertToHolder(endorsement.getCustomerByPolicyHolderId()));
		}
		
		//response Items
		endorsementResponse.getItem().addAll(this.convertToItems(endorsement.getItems()));
		
		//response Installments
		if (endorsement.getInstallments() != null && endorsement.getInstallments().size() > 0) {
			endorsementResponse.getInstallment().addAll(this.convertToInstallments(endorsement.getInstallments()));
		}
		
		return endorsementResponse;
	}

	private List<ItemCompleteType> convertToItems(Set<Item> items) throws DatatypeConfigurationException, SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		List<ItemCompleteType> itemsResponse = new ArrayList<ItemCompleteType>();
		
		// order the Item
		TreeSet<Item> treeItem = this.orderItem(items);
		
		//response Items
		for (Item item : treeItem) {
			ItemCompleteType itemResponse = new ItemCompleteType();
			
			//response Item
			Utilities.copyToObjectDifferent(item.getId(), itemResponse);
			Utilities.copyToObjectDifferent(item, itemResponse);
			
			//response Complement
			itemResponse.setPlanID((Integer)Utilities.convertNullValue(item.getCoveragePlanId(), 0));
			itemResponse.setCommissionValue(NumberUtilities.doubleToBigDecimal(item.getCommission()));
			itemResponse.setLabourValue(NumberUtilities.doubleToBigDecimal(item.getLabour()));
			itemResponse.setStatusID((Integer)Utilities.convertNullValue(item.getItemStatus(), 0));
			itemResponse.setValid(item.isCalculationValid());
			
			//response Complement
			switch (item.getObjectId()) {
				case Item.OBJECT_VIDA:
				case Item.OBJECT_CREDITO:
				case Item.OBJECT_CICLO_VITAL:
				case Item.OBJECT_PURCHASE_PROTECTED:
				case Item.OBJECT_ACCIDENTES_PERSONALES:
					//response Person
					itemResponse.setPerson(this.convertToPerson((ItemPersonalRisk)item));
					break;
					
				case Item.OBJECT_HABITAT:
				case Item.OBJECT_CAPITAL:
					//response Property
					itemResponse.setProperty(this.convertToProperty((ItemProperty)item));
					break;
					
				case Item.OBJECT_AUTO:
					itemResponse.setAuto(this.convertToAuto((ItemAuto)item));
					break;
			}
			
			//response Coverage
			itemResponse.getCoverage().addAll(this.convertToCoverages(item.getItemCoverages()));
			
			//response Beneficiary
			if (item.getBeneficiaries() != null && item.getBeneficiaries().size() > 0) {
				itemResponse.getBeneficiary().addAll(this.convertToBeneficiaries(item.getBeneficiaries()));
			}
			
			itemsResponse.add(itemResponse);
		}
		
		return itemsResponse;
	}

	private AutoCompleteType convertToAuto(ItemAuto item) {
		AutoCompleteType autoCompleteType = new AutoCompleteType();
		Utilities.copyToObjectDifferent(item, autoCompleteType);
		try {
			autoCompleteType.setInvoiceDate(DateUtilities.dateToXMLGregorianCalendar(item.getInvoiceDate()));
			autoCompleteType.setInspectionDate(DateUtilities.dateToXMLGregorianCalendar(item.getInspectionDate()));
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		autoCompleteType.setAutoMarkID(item.getAutoBrandId());
		return autoCompleteType;
	}

	private PersonCompleteType convertToPerson(ItemPersonalRisk item) throws DatatypeConfigurationException {
		PersonCompleteType personResponse = new PersonCompleteType();
		
		//response Person
		Utilities.copyToObjectDifferent(item.getId(), personResponse);
		Utilities.copyToObjectDifferent(item, personResponse);
		
		//response Complement
		personResponse.setRiskTypeID((Integer)Utilities.convertNullValue(item.getPersonalRiskType(), 0));
		personResponse.setRiskValue((BigDecimal)NumberUtilities.doubleToBigDecimal(item.getPersonalRiskValue()));
		
		switch (item.getObjectId()) {
			case Item.OBJECT_PURCHASE_PROTECTED:
				//response Card
				personResponse.setCard(this.convertToPurchase((ItemPurchaseProtected)item));
				
				break;
				
			case Item.OBJECT_CICLO_VITAL:
				//response Group
				if (item.getItemPersonalRiskGroups() != null && item.getItemPersonalRiskGroups().size() > 0) {
					personResponse.getGroup().addAll(this.convertToPersonsGroup(item.getItemPersonalRiskGroups()));
				}
				break;
		}
		
		return personResponse;
	}
	
	private List<PersonGroupCompleteType> convertToPersonsGroup(Set<ItemPersonalRiskGroup> personsRiskGroup) throws DatatypeConfigurationException {
		List<PersonGroupCompleteType> personGroupResponseList = new ArrayList<PersonGroupCompleteType>();
		
		// Order the item Risk Group
		TreeSet<ItemPersonalRiskGroup> treePersonsRiskGroup = this.orderItemRiskGroup(personsRiskGroup);
		
		//response Groups
		for (ItemPersonalRiskGroup personRiskGroup : treePersonsRiskGroup) {
			PersonGroupCompleteType personGroup = new PersonGroupCompleteType();
			
			//response Group
			Utilities.copyToObjectDifferent(personRiskGroup.getId(), personGroup);
			Utilities.copyToObjectDifferent(personRiskGroup, personGroup);
			
			//response Complement
			personGroup.setGroupID(personRiskGroup.getId().getRiskGroupId());
			personGroup.setRiskTypeID((Integer)Utilities.convertNullValue(personRiskGroup.getPersonalRiskType(), 0));
			if (personRiskGroup.getExpiryDate() != null)
				personGroup.setCancelDate(DateUtilities.dateToXMLGregorianCalendar(personRiskGroup.getExpiryDate()));
			
			personGroupResponseList.add(personGroup);
		}
		
		return personGroupResponseList;
	}

	private CardType convertToPurchase(ItemPurchaseProtected item) throws DatatypeConfigurationException {
		CardType cardType = new CardType();
		
		//response Card
		Utilities.copyToObjectDifferent(item, cardType);
		
		return cardType;
	}

	private PropertyCompleteType convertToProperty(ItemProperty item) throws DatatypeConfigurationException {
		PropertyCompleteType propertyResponse = new PropertyCompleteType();
		
		//response Property
		Utilities.copyToObjectDifferent(item.getId(), propertyResponse);
		Utilities.copyToObjectDifferent(item, propertyResponse);
		
		//response Address
		propertyResponse.setSingleAddress((item.getSingleAddress() == null || !item.getSingleAddress())?false:true);
		if (!propertyResponse.isSingleAddress()) {
			Address address = new Address();
			String[] ignoreProperties = { "id", "customer", "endorsement", "itemClauses", "itemCoverages", "itemProfiles", "beneficiaries", "itemRiskTypes", "coverageOptions" };
			BeanUtils.copyProperties(item, address, ignoreProperties);
			
			//response Address
			AddressType addressType = new AddressType();
			
			Utilities.copyToObjectDifferent(address, addressType);
			
			propertyResponse.setAddress(addressType);
		}
		
		//response Complement
		RiskType riskType = null;
		switch (item.getObjectId()) {
			case Item.OBJECT_HABITAT:
				//response Risk value from Type
				switch (item.getPropertyRiskType()) {
					case Coverage.RISKTYPE_CONTENT:
						riskType = new RiskType();
						riskType.setRiskTypeID(Coverage.RISKTYPE_CONTENT);
						riskType.setRiskTypeValue((BigDecimal)NumberUtilities.doubleToBigDecimal(item.getContentValue()));
						//??? riskType.setRiskTypeName("Contenido");
						propertyResponse.getRisk().add(riskType);
						break;
					case Coverage.RISKTYPE_STRUCTURE:
						riskType = new RiskType();
						riskType.setRiskTypeID(Coverage.RISKTYPE_STRUCTURE);
						riskType.setRiskTypeValue((BigDecimal)NumberUtilities.doubleToBigDecimal(item.getStructureValue()));
						//??? riskType.setRiskTypeName("Estructura");
						propertyResponse.getRisk().add(riskType);
						break;
					case Coverage.RISKTYPE_BOTH:
						riskType = new RiskType();
						riskType.setRiskTypeID(Coverage.RISKTYPE_CONTENT);
						riskType.setRiskTypeValue((BigDecimal)NumberUtilities.doubleToBigDecimal(item.getContentValue()));
						//??? riskType.setRiskTypeName("Contenido");
						propertyResponse.getRisk().add(riskType);
						
						riskType = new RiskType();
						riskType.setRiskTypeID(Coverage.RISKTYPE_STRUCTURE);
						riskType.setRiskTypeValue((BigDecimal)NumberUtilities.doubleToBigDecimal(item.getStructureValue()));
						//??? riskType.setRiskTypeName("Estructura");
						propertyResponse.getRisk().add(riskType);
						break;
				}
				break;
				
			case Item.OBJECT_CAPITAL:
				//response Risks
				if (item.getItemRiskTypes() != null && item.getItemRiskTypes().size() > 0) {
					propertyResponse.getRisk().addAll(this.convertToPropertyRisk(item.getItemRiskTypes()));
				}
				break;
		}
		
		return propertyResponse;
	}

	private List<RiskType> convertToPropertyRisk(Set<ItemRiskType> propertyRisks) throws DatatypeConfigurationException {
		List<RiskType> propertyRiskResponseList = new ArrayList<RiskType>();
		
		//response Risks
		for (ItemRiskType propertyRisk : propertyRisks) {
			RiskType riskType = new RiskType();
			
			//response Risk
			riskType.setRiskTypeID((Integer)Utilities.convertNullValue(propertyRisk.getId().getRiskType(), 0));
			riskType.setRiskTypeValue((BigDecimal)NumberUtilities.doubleToBigDecimal(propertyRisk.getRiskTypeValue()));
			//??? riskType.setRiskTypeName(propertyRisk.getRiskTypeName());
			
			propertyRiskResponseList.add(riskType);
		}
		
		return propertyRiskResponseList;
	}

	private List<CoverageCompleteType> convertToCoverages(Set<ItemCoverage> coverages) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		List<CoverageCompleteType> coverageResponseList = new LinkedList<CoverageCompleteType>();
		
		// Order the Coverage
		TreeSet<ItemCoverage> treeCoverages = this.orderItemCoverage(coverages);
		
		//response Coverages
		for (ItemCoverage coverage : treeCoverages) {
			CoverageCompleteType coverageResponse = new CoverageCompleteType();
			
			//response Coverage
			Utilities.copyToObjectDifferent(coverage.getId(), coverageResponse);
			Utilities.copyToObjectDifferent(coverage, coverageResponse);
			
			//response Complement
			coverageResponse.setNickName(coverage.getCoverageName());
			coverageResponse.setBasic(coverage.isBasicCoverage());
			coverageResponse.setCompulsory(coverage.isBasicCoverage());
			coverageResponse.setCommissionValue(NumberUtilities.doubleToBigDecimal(coverage.getCommission()));
			coverageResponse.setLabourValue(NumberUtilities.doubleToBigDecimal(coverage.getLabour()));
			coverageResponse.setBonusValue(NumberUtilities.doubleToBigDecimal(coverage.getBonus()));
			if (coverage.getRangeValueId() != null && (new Integer(-1)).equals(coverage.getRangeValueId()))
				coverageResponse.setRangeValueID(0);
			
			coverageResponseList.add(coverageResponse);
		}
		
		return coverageResponseList;
	}

	private List<BeneficiaryType> convertToBeneficiaries(Set<Beneficiary> beneficiaries) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		List<BeneficiaryType> beneficiaryResponseList = new LinkedList<BeneficiaryType>();
		
		// Order the Beneficiary
		TreeSet<Beneficiary> treeBeneficiaries = this.orderBeneficiary(beneficiaries);
		
		//response Beneficiaries
		for (Beneficiary beneficiary : treeBeneficiaries) {
			BeneficiaryType beneficiaryResponse = new BeneficiaryType();
			
			//response Beneficiary
			Utilities.copyToObjectDifferent(beneficiary.getId(), beneficiaryResponse);
			Utilities.copyToObjectDifferent(beneficiary, beneficiaryResponse);
			
			//response Complement
			beneficiaryResponse.setPercentage(beneficiary.getParticipationPercentage().intValue());
			
			beneficiaryResponseList.add(beneficiaryResponse);
		}
		
		return beneficiaryResponseList;
	}

	private InsuredCompleteType convertToInsured(Customer customer) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, DatatypeConfigurationException {
		InsuredCompleteType insuredResponse = new InsuredCompleteType();
		
		//response Insured
		Utilities.copyToObjectDifferent(customer, insuredResponse);
		
		//response Complement
		insuredResponse.setInsuredID(customer.getCustomerId());
		
		if (!customer.getPersonType().equals(Customer.PERSON_TYPE_NATURAL)) {
			insuredResponse.setFirstName(null);
			insuredResponse.setLastName(null);
			
			if (customer.getName() != null)
				insuredResponse.setCorporateName(customer.getName());
			
			insuredResponse.setBirthDate(null);
			insuredResponse.setGenderID(null);
			insuredResponse.setMaritalStatusID(null);
		}
		
		if (customer.getPhoneNumber() != null)
			insuredResponse.setPhoneContact(customer.getPhoneNumber());
		
		if (customer.getEmailAddress() != null)
			insuredResponse.setEmail(customer.getEmailAddress());
		
		//response Address
		insuredResponse.setAddress(this.convertToAddress(customer.getFirstAddress()));
		
		return insuredResponse;
	}

	private HolderCompleteType convertToHolder(Customer customer) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, DatatypeConfigurationException {
		HolderCompleteType holderResponse = new HolderCompleteType();
		
		//response Insured
		Utilities.copyToObjectDifferent(customer, holderResponse);
		
		//response Complement
		holderResponse.setHolderID(customer.getCustomerId());
		
		if (!customer.getPersonType().equals(Customer.PERSON_TYPE_NATURAL)) {
			holderResponse.setFirstName(null);
			holderResponse.setLastName(null);
			
			if (customer.getName() != null)
				holderResponse.setCorporateName(customer.getName());
			
			holderResponse.setBirthDate(null);
			holderResponse.setGenderID(null);
			holderResponse.setMaritalStatusID(null);
		}
		
		if (customer.getPhoneNumber() != null)
			holderResponse.setPhoneContact(customer.getPhoneNumber());
		
		if (customer.getEmailAddress() != null)
			holderResponse.setEmail(customer.getEmailAddress());
		
		//response Address
		holderResponse.setAddress(this.convertToAddress(customer.getFirstAddress()));
		
		return holderResponse;
	}	

	private AddressCompleteType convertToAddress(Address address) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, DatatypeConfigurationException {
		AddressCompleteType addressResponse = null;
		
		//response Address
		if (address != null) {
			addressResponse = new AddressCompleteType();
			
			Utilities.copyToObjectDifferent(address, addressResponse);
			
			//response Complement
			addressResponse.setStreetName(address.getAddressStreet());
			
			//response Phones
			if (address.getPhones() != null || address.getPhones().size() > 0) {
				addressResponse.getPhone().addAll(this.convertToPhone(address.getPhones()));
			}
		}
		
		return addressResponse;
	}

	private List<PhoneType> convertToPhone(Set<Phone> phones) {
		List<PhoneType> phoneResponseList = new ArrayList<PhoneType>();
		
		//response Phones
		for (Phone phone : phones) {
			PhoneType phoneResponse = new PhoneType();
			
			//response Phone
			Utilities.copyToObjectDifferent(phone, phoneResponse);
			
			phoneResponseList.add(phoneResponse);
		}
		
		return phoneResponseList;
	}

	private List<InstallmentType> convertToInstallments(Set<Installment> installments) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, DatatypeConfigurationException {
		List<InstallmentType> installmentResponseList = new ArrayList<InstallmentType>();
		
		// Order the Beneficiary
		TreeSet<Installment> treeInstallments = this.orderInstallment(installments);
		
		//response Installments
		for (Installment installment : treeInstallments) {
			//response Installment
			installmentResponseList.add(this.convertToInstallment(installment));
		}
		
		return installmentResponseList;
	}

	private InstallmentType convertToInstallment(Installment installment) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, DatatypeConfigurationException {
		InstallmentType installmentResponse = new InstallmentType();
		
		//response Installment
		Utilities.copyToObjectDifferent(installment.getId(), installmentResponse);
		Utilities.copyToObjectDifferent(installment, installmentResponse);
		
		//response Complement
		installmentResponse.setDueDate(DateUtilities.dateToXMLGregorianCalendar(installment.getIssueDate()));
		installmentResponse.setLimitDate(DateUtilities.dateToXMLGregorianCalendar(installment.getDueDate()));
		installmentResponse.setStatusID((Integer)Utilities.convertNullValue(installment.getInstallmentStatus(), 0));
		
		return installmentResponse;
	}
	// -----------------------------------------------------------------------------------

	private Endorsement dataFillCalculate(ContractType contractType) throws ServerException, SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, ServiceException {
		Contract contract = new Contract();
		Endorsement endorsement = contract.addEndorsement((contractType.getEndorsement().getIssuanceTypeID() == Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL)?contractType.getEndorsement().getEndorsementID():1);
		
		//fill Complement
		if ((contractType.getEndorsement().getIssuanceTypeID() != Endorsement.ISSUANCE_TYPE_POLICY_EMISSION) &&
				(contractType.getEndorsement().getIssuanceTypeID() != Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL)) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_ISSUANCE_TYPE_NOT_ALLOWED.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_ISSUANCE_TYPE_NOT_ALLOWED.getMessage(null));
		}
		
		//fill Contract
		this.dataFillContract(contractType, contract);
		
		//fill Endorsement
		EndorsementType endorsementType = contractType.getEndorsement();
		this.dataFillEndorsement(endorsementType, endorsement);
		
		//fill Contract
		if (contract.getReferenceDate() == null && endorsement.getReferenceDate() != null)
			contract.setReferenceDate(endorsement.getReferenceDate());
		if (contract.getEffectiveDate() == null && endorsement.getEffectiveDate() != null)
			contract.setEffectiveDate(endorsement.getEffectiveDate());
		
		//fill Insured
		Customer insured = new Customer();
		endorsement.setCustomerByInsuredId(insured);
		InsuredType insuredType = endorsementType.getInsured();
		this.dataFillCustomer(insuredType, insured);
		
		//fill Address
		AddressType addressType = insuredType.getAddress();
		if (addressType != null) {
			Address address = null;
			if (insured.getAddresses().size() > 0) {
				address = insured.getFirstAddress();
			}
			else {
				insured.getAddresses().clear();
				address = new Address();
				insured.getAddresses().add(address);
			}
			this.dataFillAddress(addressType, address);
			
			//fill Phone - Insured
//			address.getPhones().clear();
			for (PhoneType phoneType: addressType.getPhone()) {
				if (phoneType.getPhoneNumber() != null && !"".equals(phoneType.getPhoneNumber()) && !(new BigDecimal(0)).equals(phoneType.getPhoneNumber())) {
					Phone phone = address.getFirstPhone();
					if (phone == null) {
						phone = address.addPhone();
					}
					this.dataFillPhone(phoneType, phone);
				}
			}
			if (address.getPhones() == null || address.getPhones().size() == 0) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PHONE_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PHONE_NOT_FOUND.getMessage(null));
			}
		}
		
		//fill Item
		endorsement.getItems().clear();
		int objectId = this.getObjectId(contract.getProductId(), endorsement.getReferenceDate());
		for (int itemId = 1; itemId <= endorsementType.getItem().size(); itemId++) {
			//find Item
			AdapterItem adapterItem = this.findItemType(endorsementType.getItem(), itemId);
			if (adapterItem == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_ITEM_OUT_OF_SEQUENCE.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_ITEM_OUT_OF_SEQUENCE.getMessage(null));
			}
			
			//add Item
			Item item = endorsement.addItem(objectId);
			
			//fill Item
			this.dataFillItem(adapterItem, item);
			
			//fill Complement
			item.setRenewalType(Item.ITEM_RENEW_TYPE_EMISSION);
			item.setEffectiveDate(item.getEndorsement().getEffectiveDate());
			
			//fill Item Object
			switch (item.getObjectId()) {
				case Item.OBJECT_VIDA:
				case Item.OBJECT_CREDITO:
				case Item.OBJECT_CICLO_VITAL:
				case Item.OBJECT_PURCHASE_PROTECTED:
				case Item.OBJECT_ACCIDENTES_PERSONALES:
					PersonBasicType personType = adapterItem.getPerson();
					ItemPersonalRisk itemPersonalRisk = (ItemPersonalRisk)item;
					
					if (insured.getPersonType() == null || !insured.getPersonType().equals(Customer.PERSON_TYPE_NATURAL))
						throw new ServerException((new Long(ErrorTypes.VALIDATION_PERSON_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PERSON_TYPE_INVALID.getMessage(null));
					
					//fill Personal - ALL
					this.dataFillPersonal(personType, itemPersonalRisk);
					
					switch (item.getObjectId()) {
						//fill Personal - AP
						case Item.OBJECT_ACCIDENTES_PERSONALES:
							this.dataFillPersonalAP(personType, itemPersonalRisk);
							break;
							
							//fill Personal - Purchase
						case Item.OBJECT_PURCHASE_PROTECTED:
							this.dataFillPersonalPurchase(((PersonWithCardType)personType).getCard(), (ItemPurchaseProtected)itemPersonalRisk, false);
							break;
							
						//fill Risk Group
						case Item.OBJECT_CICLO_VITAL:
							itemPersonalRisk.getItemPersonalRiskGroups().clear();
							if (item.getId().getItemId() == 1) {
								for (int groupId = 1; groupId <= ((PersonWithGroupType)personType).getGroup().size(); groupId++) {
									//find Group
									PersonGroupType personGroupType = this.findPersonGroupType(((PersonWithGroupType)personType).getGroup(), groupId);
									if (personGroupType == null) {
										throw new ServerException((new Long(ErrorTypes.VALIDATION_RISK_GROUP_OUT_OF_SEQUENCE.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_RISK_GROUP_OUT_OF_SEQUENCE.getMessage(null));
									}
									
									//add Group
									ItemPersonalRiskGroup itemPersonalRiskGroup = itemPersonalRisk.addItemPersonalRiskGroup();
									
									//fill Personal Group
									this.dataFillPersonalGroup(personGroupType, itemPersonalRiskGroup);
									
									//fill Complement
									itemPersonalRiskGroup.setEffectiveDate(itemPersonalRisk.getEffectiveDate());
								}
							}
							break;
					}
					
					//fill Complement - item Principal
					if (item.getId().getItemId() == 1) {
						//fill Personal
						insured.setBirthDate(itemPersonalRisk.getBirthDate());
						insured.setGender(itemPersonalRisk.getGender());
						
						//fill Personal - AP
						if (item.getObjectId() == Item.OBJECT_ACCIDENTES_PERSONALES) {
							insured.setOccupationId(itemPersonalRisk.getOccupationId());
						}
					}
					break;
					
				case Item.OBJECT_HABITAT:
				case Item.OBJECT_CAPITAL:
					PropertyType propertyType = adapterItem.getProperty();
					ItemProperty itemProperty = (ItemProperty)item;
					
					//fill Property - ALL
					this.dataFillProperty(propertyType, itemProperty, false);
					
					//fill Property - Address
					this.dataFillPropertyAddress(propertyType, itemProperty, insured.getFirstAddress());
					
					//fill Property - Risk
					if (propertyType.getRisk() == null || propertyType.getRisk().size() == 0)
						throw new ServerException((new Long(ErrorTypes.VALIDATION_PROPERTY_RISK_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PROPERTY_RISK_NOT_FOUND.getMessage(null));
					
					this.dataFillPropertyRisk(propertyType.getRisk(), itemProperty);
					
					break;
					
				default:
					throw new IllegalArgumentException();
			}
			
			//fill Coverage
			this.dataFillCoverage(adapterItem.getCoverage(), item);
		}
		return endorsement;
	}

	private Endorsement dataFillQuotation(ContractType contractType) throws ServerException, SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, ServiceException {
		Contract contract = null;
		Endorsement endorsement = null;
		
		if (contractType.getContractID() != null && !(new Integer(0)).equals(contractType.getContractID())) {
			// find Endorsement
			endorsement = daoPolicy.loadEndorsement(contractType.getEndorsement().getEndorsementID(), contractType.getContractID());
			if (endorsement == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_ENDORSEMENT_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_ENDORSEMENT_NOT_FOUND.getMessage(null));
			}
			else {
				if (endorsement.getIssuingStatus() != Endorsement.ISSUING_STATUS_COTACAO) {
					throw new ServerException((new Long(ErrorTypes.VALIDATION_ACTION_IS_NOT_ALLOWED.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_ACTION_IS_NOT_ALLOWED.getMessage(null));
				}
				
				// remove Object from the Session
				daoPolicy.evict(endorsement);
				contract = endorsement.getContract();
			}
		}
		else {
			contract = new Contract();
			endorsement = contract.addEndorsement(1);
		}
		
		//fill Contract
		this.dataFillContract(contractType, contract);
		
		//fill Endorsement
		EndorsementType endorsementType = contractType.getEndorsement();
		this.dataFillEndorsement(endorsementType, endorsement);
		
		//fill Complement
		endorsement.setIssuanceType(Endorsement.ISSUANCE_TYPE_POLICY_EMISSION);
		
		//fill Contract
		if (contract.getReferenceDate() == null && endorsement.getReferenceDate() != null)
			contract.setReferenceDate(endorsement.getReferenceDate());
		if (contract.getEffectiveDate() == null && endorsement.getEffectiveDate() != null)
			contract.setEffectiveDate(endorsement.getEffectiveDate());
		
		//fill Insured
		Customer insured = null;
		if (endorsement.getCustomerByInsuredId() == null) {
			insured = new Customer();
			endorsement.setCustomerByInsuredId(insured);
		}
		else {
			insured = endorsement.getCustomerByInsuredId();
		}
		InsuredType insuredType = endorsementType.getInsured();
		this.dataFillCustomer(insuredType, insured);
		
		//fill Address - Insured
		AddressType addressType = insuredType.getAddress();
		if (addressType != null) {
			Address address = null;
			if (insured.getAddresses().size() > 0) {
				address = insured.getFirstAddress();
			}
			else {
				insured.getAddresses().clear();
				address = new Address();
				insured.getAddresses().add(address);
			}
			this.dataFillAddress(addressType, address);
			
			//fill Phone - Insured
//			address.getPhones().clear();
			for (PhoneType phoneType: addressType.getPhone()) {
				if (phoneType.getPhoneNumber() != null && !"".equals(phoneType.getPhoneNumber()) && !(new BigDecimal(0)).equals(phoneType.getPhoneNumber())) {
					Phone phone = address.getFirstPhone();
					if (phone == null) {
						phone = address.addPhone();
					}
					this.dataFillPhone(phoneType, phone);
				}
			}
			if (address.getPhones() == null || address.getPhones().size() == 0) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PHONE_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PHONE_NOT_FOUND.getMessage(null));
			}
		}
		
		//fill Item
		endorsement.getItems().clear();
		int objectId = this.getObjectId(contract.getProductId(), endorsement.getReferenceDate());
		for (int itemId = 1; itemId <= endorsementType.getItem().size(); itemId++) {
			//find Item
			AdapterItem adapterItem = this.findItemType(endorsementType.getItem(), itemId);
			if (adapterItem == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_ITEM_OUT_OF_SEQUENCE.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_ITEM_OUT_OF_SEQUENCE.getMessage(null));
			}
			
			//add Item
			Item item = endorsement.addItem(objectId);
			
			//fill Item
			this.dataFillItem(adapterItem, item);
			
			//fill Complement
			item.setRenewalType(Item.ITEM_RENEW_TYPE_EMISSION);
			item.setEffectiveDate(item.getEndorsement().getEffectiveDate());
			
			//fill Item Object
			switch (item.getObjectId()) {
				case Item.OBJECT_VIDA:
				case Item.OBJECT_CREDITO:
				case Item.OBJECT_CICLO_VITAL:
				case Item.OBJECT_PURCHASE_PROTECTED:
				case Item.OBJECT_ACCIDENTES_PERSONALES:
					PersonBasicType personType = adapterItem.getPerson();
					ItemPersonalRisk itemPersonalRisk = (ItemPersonalRisk)item;
					
					if (insured.getPersonType() == null || !insured.getPersonType().equals(Customer.PERSON_TYPE_NATURAL))
						throw new ServerException((new Long(ErrorTypes.VALIDATION_PERSON_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PERSON_TYPE_INVALID.getMessage(null));
					
					//fill Personal - ALL
					this.dataFillPersonal(personType, itemPersonalRisk);
					
					switch (item.getObjectId()) {
						//fill Personal - AP
						case Item.OBJECT_ACCIDENTES_PERSONALES:
							this.dataFillPersonalAP(personType, itemPersonalRisk);
							break;
							
							//fill Personal - Purchase
						case Item.OBJECT_PURCHASE_PROTECTED:
							this.dataFillPersonalPurchase(((PersonWithCardType)personType).getCard(), (ItemPurchaseProtected)itemPersonalRisk, false);
							break;
							
						//fill Risk Group
						case Item.OBJECT_CICLO_VITAL:
							itemPersonalRisk.getItemPersonalRiskGroups().clear();
							if (item.getId().getItemId() == 1) {
								for (int groupId = 1; groupId <= ((PersonWithGroupType)personType).getGroup().size(); groupId++) {
									//find Group
									PersonGroupType personGroupType = this.findPersonGroupType(((PersonWithGroupType)personType).getGroup(), groupId);
									if (personGroupType == null) {
										throw new ServerException((new Long(ErrorTypes.VALIDATION_RISK_GROUP_OUT_OF_SEQUENCE.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_RISK_GROUP_OUT_OF_SEQUENCE.getMessage(null));
									}
									
									//add Group
									ItemPersonalRiskGroup itemPersonalRiskGroup = itemPersonalRisk.addItemPersonalRiskGroup();
									
									//fill Personal Group
									this.dataFillPersonalGroup(personGroupType, itemPersonalRiskGroup);
									
									//fill Complement
									itemPersonalRiskGroup.setEffectiveDate(itemPersonalRisk.getEffectiveDate());
								}
							}
							break;
					}
					
					//fill Complement - item Principal
					if (item.getId().getItemId() == 1) {
						//fill Personal
						insured.setBirthDate(itemPersonalRisk.getBirthDate());
						insured.setGender(itemPersonalRisk.getGender());
						
						//fill Personal - AP
						if (item.getObjectId() == Item.OBJECT_ACCIDENTES_PERSONALES) {
							insured.setOccupationId(itemPersonalRisk.getOccupationId());
						}
					}
					break;
					
				case Item.OBJECT_HABITAT:
				case Item.OBJECT_CAPITAL:
					PropertyType propertyType = adapterItem.getProperty();
					ItemProperty itemProperty = (ItemProperty)item;
					
					//fill Property - ALL
					this.dataFillProperty(propertyType, itemProperty, false);
					
					//fill Property - Address
					this.dataFillPropertyAddress(propertyType, itemProperty, insured.getFirstAddress());
					
					//fill Property - Risk
					if (propertyType.getRisk() == null || propertyType.getRisk().size() == 0)
						throw new ServerException((new Long(ErrorTypes.VALIDATION_PROPERTY_RISK_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PROPERTY_RISK_NOT_FOUND.getMessage(null));
					
					this.dataFillPropertyRisk(propertyType.getRisk(), itemProperty);
					
					break;
					
					
				case Item.OBJECT_AUTO:
					
					ItemAuto itemAuto = (ItemAuto)item;
					AutoCompleteType autoCompleteType = adapterItem.getAutoCompleteType();
					
					Utilities.copyToObjectDifferent(autoCompleteType, itemAuto);
					itemAuto.setInvoiceDate(DateUtilities.xmlGregorianCalendarToDate(autoCompleteType.getInvoiceDate()));
					itemAuto.setInspectionDate(DateUtilities.xmlGregorianCalendarToDate(autoCompleteType.getInspectionDate()));
					itemAuto.setAutoBrandId(autoCompleteType.getAutoMarkID());
					
					break;
					
				default:
					throw new IllegalArgumentException();
			}
			
			//fill Coverage
			this.dataFillCoverage(adapterItem.getCoverage(), item);
		}
		return endorsement;
	}
	
	private Endorsement dataFillProposal(ContractType contractType) throws ServerException, SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, ServiceException {
		// find Endorsement
		Endorsement endorsement = daoPolicy.loadEndorsement(contractType.getEndorsement().getEndorsementID(), contractType.getContractID());
		if (endorsement == null) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_ENDORSEMENT_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_ENDORSEMENT_NOT_FOUND.getMessage(null));
		}
		else {
			if (endorsement.getIssuingStatus() != Endorsement.ISSUING_STATUS_COTACAO) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_ACTION_IS_NOT_ALLOWED.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_ACTION_IS_NOT_ALLOWED.getMessage(null));
			}
			
			// remove Object from the Session
			daoPolicy.evict(endorsement);
		}
		
		//fill Endorsement
		EndorsementType endorsementType = contractType.getEndorsement();
		
		//fill Insured
		Customer insured = endorsement.getCustomerByInsuredId();
		InsuredType insuredType = endorsementType.getInsured();
		this.dataFillCustomer(insuredType, insured);
		
		//fill Address - Insured
		AddressType addressInsured = insuredType.getAddress();
		if (addressInsured != null) {
			Address address = null;
			if (insured.getAddresses().size() > 0) {
				address = insured.getFirstAddress();
			}
			else {
				insured.getAddresses().clear();
				address = new Address();
				insured.getAddresses().add(address);
			}
			this.dataFillAddress(addressInsured, address);
			
			//fill Phone - Insured
//			address.getPhones().clear();
			for (PhoneType phoneType: addressInsured.getPhone()) {
				if (phoneType.getPhoneNumber() != null && !"".equals(phoneType.getPhoneNumber()) && !(new BigDecimal(0)).equals(phoneType.getPhoneNumber())) {
					Phone phone = address.getFirstPhone();
					if (phone == null) {
						phone = address.addPhone();
					}
					this.dataFillPhone(phoneType, phone);
				}
			}
			if (address.getPhones() == null || address.getPhones().size() == 0) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PHONE_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PHONE_NOT_FOUND.getMessage(null));
			}
		}
		
		//fill Holder
		if (endorsementType.getHolder() != null) {
			Customer holder = new Customer();
			if (endorsement.getCustomerByPolicyHolderId() != null &&
					endorsement.getCustomerByPolicyHolderId().getCustomerId() != endorsement.getCustomerByInsuredId().getCustomerId()) {
				holder = endorsement.getCustomerByPolicyHolderId();
			}
			endorsement.setCustomerByPolicyHolderId(holder);
			HolderType holderType = endorsementType.getHolder();
			this.dataFillCustomer(new InsuredTypeProxy(holderType), holder);
			
			//fill Address - Holder
			AddressType addressHolder = holderType.getAddress();
			if (addressHolder != null) {
				Address address = null;
				if (holder.getAddresses().size() > 0) {
					address = holder.getFirstAddress();
				}
				else {
					holder.getAddresses().clear();
					address = new Address();
					holder.getAddresses().add(address);
				}
				this.dataFillAddress(addressHolder, address);
				
				//fill Phone - Holder
//				address.getPhones().clear();
				for (PhoneType phoneType: addressHolder.getPhone()) {
					if (phoneType.getPhoneNumber() != null && !"".equals(phoneType.getPhoneNumber()) && !(new BigDecimal(0)).equals(phoneType.getPhoneNumber())) {
						Phone phone = address.getFirstPhone();
						if (phone == null) {
							phone = address.addPhone();
						}
						this.dataFillPhone(phoneType, phone);
					}
				}
				if (address.getPhones() == null || address.getPhones().size() == 0) {
					throw new ServerException((new Long(ErrorTypes.VALIDATION_PHONE_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PHONE_NOT_FOUND.getMessage(null));
				}
			}
		}
		else {
			endorsement.setCustomerByPolicyHolderId(endorsement.getCustomerByInsuredId());
		}
		
		//fill Item
			//find Item
		for (Item item : endorsement.getItems()) {
			AdapterItem adapterItem = this.findItemType(endorsementType.getItem(), item.getId().getItemId());
			if (adapterItem == null) {
				continue;
			}
			
			//fill Item Object
			switch (item.getObjectId()) {
				case Item.OBJECT_VIDA:
				case Item.OBJECT_CREDITO:
				case Item.OBJECT_CICLO_VITAL:
				case Item.OBJECT_PURCHASE_PROTECTED:
				case Item.OBJECT_ACCIDENTES_PERSONALES:
					PersonBasicType personType = adapterItem.getPerson();
					ItemPersonalRisk itemPersonalRisk = (ItemPersonalRisk)item;
					
					if (insured.getPersonType() == null || !insured.getPersonType().equals(Customer.PERSON_TYPE_NATURAL))
						throw new ServerException((new Long(ErrorTypes.VALIDATION_PERSON_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PERSON_TYPE_INVALID.getMessage(null));
					
					//fill Personal - ALL
					if (item.getId().getItemId() != 1) {
						this.dataFillPersonal(personType, itemPersonalRisk);
					}
					
					switch (item.getObjectId()) {
						//fill Personal - Purchase
						case Item.OBJECT_PURCHASE_PROTECTED:
							this.dataFillPersonalPurchase(((PersonWithCardType)personType).getCard(), (ItemPurchaseProtected)itemPersonalRisk, true);
							break;
							
						//fill Risk Group
						case Item.OBJECT_CICLO_VITAL:
							itemPersonalRisk.getItemPersonalRiskGroups().clear();
							if (item.getId().getItemId() == 1) {
								for (int groupId = 1; groupId <= ((PersonWithGroupType)personType).getGroup().size(); groupId++) {
									//find Group
									PersonGroupType personGroupType = this.findPersonGroupType(((PersonWithGroupType)personType).getGroup(), groupId);
									if (personGroupType == null) {
										throw new ServerException((new Long(ErrorTypes.VALIDATION_RISK_GROUP_OUT_OF_SEQUENCE.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_RISK_GROUP_OUT_OF_SEQUENCE.getMessage(null));
									}
									
									//add Group
									ItemPersonalRiskGroup itemPersonalRiskGroup = itemPersonalRisk.addItemPersonalRiskGroup();
									
									//fill Personal Group
									this.dataFillPersonalGroup(personGroupType, itemPersonalRiskGroup);
									
									//fill Complement
									itemPersonalRiskGroup.setEffectiveDate(itemPersonalRisk.getEffectiveDate());
								}
							}
							break;
					}
					
					//fill Complement - item Principal
					if (item.getId().getItemId() == 1) {
						//fill Beneficiary
						int percentage = 0;
						itemPersonalRisk.getBeneficiaries().clear();
						for (int beneficiaryId = 1; beneficiaryId <= adapterItem.getBeneficiary().size(); beneficiaryId++) {
							//find Beneficiary
							BeneficiaryType beneficiaryType = this.findBeneficiaryType(adapterItem.getBeneficiary(), beneficiaryId);
							if (beneficiaryType == null) {
								throw new ServerException((new Long(ErrorTypes.VALIDATION_BENEFICIARY_OUT_OF_SEQUENCE.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_BENEFICIARY_OUT_OF_SEQUENCE.getMessage(null));
							}
							
							//add Beneficiary
							Beneficiary beneficiary = itemPersonalRisk.addBeneficiary();
							
							//fill Beneficiary
							this.dataFillBeneficiary(beneficiaryType, beneficiary);
							
							percentage += beneficiaryType.getPercentage();
						}
						
						if (adapterItem.getBeneficiary().size() > 0 && percentage != 100) {
							throw new ServerException((new Long(ErrorTypes.VALIDATION_BENEFICIARY_PARTICIPATION_DISPARITY.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_BENEFICIARY_PARTICIPATION_DISPARITY.getMessage(null));
						}
						
						//fill Personal
						itemPersonalRisk.setUndocumented(false);
						itemPersonalRisk.setDocumentType(insured.getDocumentType());
						itemPersonalRisk.setDocumentNumber(insured.getDocumentNumber());
						
						insured.setBirthDate(itemPersonalRisk.getBirthDate());
						insured.setGender(itemPersonalRisk.getGender());
						
						//fill Personal - AP
						if (item.getObjectId() == Item.OBJECT_ACCIDENTES_PERSONALES) {
							insured.setOccupationId(itemPersonalRisk.getOccupationId());
						}
					}
					break;
					
				case Item.OBJECT_HABITAT:
				case Item.OBJECT_CAPITAL:
					PropertyType propertyType = adapterItem.getProperty();
					ItemProperty itemProperty = (ItemProperty)item;
					
					//fill Property - ALL
					this.dataFillProperty(propertyType, itemProperty, true);
					
					//fill Property - Address
					this.dataFillPropertyAddress(propertyType, itemProperty, insured.getFirstAddress());
					
					break;
				
				case Item.OBJECT_AUTO:
					
					ItemAuto itemAuto = (ItemAuto)item;
					AutoCompleteType autoCompleteType = adapterItem.getAutoCompleteType();
					

					
					break;
					
				default:
					throw new IllegalArgumentException();
			}
		}
		return endorsement;
	}

	private Endorsement dataFillChangeTechnical(ContractType contractType) throws ServerException, SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, ServiceException {
		// find Endorsement
		Endorsement endorsement = daoPolicy.loadEndorsement(contractType.getEndorsement().getEndorsementID(), contractType.getContractID());
		if (endorsement == null) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_ENDORSEMENT_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_ENDORSEMENT_NOT_FOUND.getMessage(null));
		}
		else {
			if (endorsement.getIssuingStatus() != Endorsement.ISSUING_STATUS_EMITIDA) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_ACTION_IS_NOT_ALLOWED.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_ACTION_IS_NOT_ALLOWED.getMessage(null));
			}
			
			// remove Object from the Session
			daoPolicy.evict(endorsement);
		}
		
		//fill Contract
		Contract contract = endorsement.getContract();
		
		//fill Endorsement
		EndorsementType endorsementType = contractType.getEndorsement();
		
		//fill Endorsed
		endorsement.setEndorsedId(endorsement.getId().getEndorsementId());
		
		//fill Complement
		if (endorsementType.getUserID() != 0)
			endorsement.setUserId(endorsementType.getUserID());
		
		if (endorsementType.getRiskPlanID() != 0)
			endorsement.setRiskPlanId(endorsementType.getRiskPlanID());
		
		if (endorsementType.getVersionDate() != null)
			endorsement.setReferenceDate(DateUtilities.xmlGregorianCalendarToDate(endorsementType.getVersionDate()));
		
		//fill Insured
		Customer insured = endorsement.getCustomerByInsuredId();
		InsuredType insuredType = endorsementType.getInsured();
		this.dataFillCustomer(insuredType, insured);
		
		//fill Address - Insured
		AddressType addressInsured = insuredType.getAddress();
		if (addressInsured != null) {
			Address address = insured.getFirstAddress();
			this.dataFillAddress(addressInsured, address);
			
			//fill Phone - Insured
//			address.getPhones().clear();
			for (PhoneType phoneType: addressInsured.getPhone()) {
				if (phoneType.getPhoneNumber() != null && !"".equals(phoneType.getPhoneNumber()) && !(new BigDecimal(0)).equals(phoneType.getPhoneNumber())) {
					Phone phone = address.getFirstPhone();
					if (phone == null) {
						phone = address.addPhone();
					}
					this.dataFillPhone(phoneType, phone);
				}
			}
			if (address.getPhones() == null || address.getPhones().size() == 0) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PHONE_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PHONE_NOT_FOUND.getMessage(null));
			}
		}
		
		//fill Holder
		if (endorsementType.getHolder() != null) {
			int customerId = endorsement.getCustomerByPolicyHolderId().getCustomerId();
			int addressId = endorsement.getCustomerByPolicyHolderId().getFirstAddress().getAddressId();
			
			Customer holder = new Customer();
			endorsement.setCustomerByPolicyHolderId(holder);
			HolderType holderType = endorsementType.getHolder();
			this.dataFillCustomer(new InsuredTypeProxy(holderType), holder);
			holder.setCustomerId(customerId);
			
			//fill Address - Holder
			AddressType addressHolder = holderType.getAddress();
			if (addressHolder != null) {
				Address address = null;
				if (holder.getAddresses().size() > 0) {
					address = holder.getFirstAddress();
				}
				else {
					holder.getAddresses().clear();
					address = new Address();
					holder.getAddresses().add(address);
				}
				this.dataFillAddress(addressHolder, address);
				address.setAddressId(addressId);
				
				//fill Phone - Holder
//				address.getPhones().clear();
				for (PhoneType phoneType: addressHolder.getPhone()) {
					if (phoneType.getPhoneNumber() != null && !"".equals(phoneType.getPhoneNumber()) && !(new BigDecimal(0)).equals(phoneType.getPhoneNumber())) {
						Phone phone = address.getFirstPhone();
						if (phone == null) {
							phone = address.addPhone();
						}
						this.dataFillPhone(phoneType, phone);
					}
				}
				if (address.getPhones() == null || address.getPhones().size() == 0) {
					throw new ServerException((new Long(ErrorTypes.VALIDATION_PHONE_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PHONE_NOT_FOUND.getMessage(null));
				}
			}
		}
		else {
			endorsement.setCustomerByPolicyHolderId(endorsement.getCustomerByInsuredId());
		}
		
		//fill Item
		endorsement.getItems().clear();
		int objectId = this.getObjectId(contract.getProductId(), endorsement.getReferenceDate());
		for (int itemId = 1; itemId <= endorsementType.getItem().size(); itemId++) {
			//find Item
			AdapterItem adapterItem = this.findItemType(endorsementType.getItem(), itemId);
			if (adapterItem == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_ITEM_OUT_OF_SEQUENCE.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_ITEM_OUT_OF_SEQUENCE.getMessage(null));
			}
			
			//add Item
			Item item = endorsement.addItem(objectId);
			this.dataFillItem(adapterItem, item);
			
			//fill Item Object
			switch (item.getObjectId()) {
				case Item.OBJECT_VIDA:
				case Item.OBJECT_CREDITO:
				case Item.OBJECT_CICLO_VITAL:
				case Item.OBJECT_PURCHASE_PROTECTED:
				case Item.OBJECT_ACCIDENTES_PERSONALES:
					PersonBasicType personType = adapterItem.getPerson();
					ItemPersonalRisk itemPersonalRisk = (ItemPersonalRisk)item;
					
					if (insured.getPersonType() == null || !insured.getPersonType().equals(Customer.PERSON_TYPE_NATURAL))
						throw new ServerException((new Long(ErrorTypes.VALIDATION_PERSON_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PERSON_TYPE_INVALID.getMessage(null));
					
					//fill Personal - ALL
					this.dataFillPersonal(personType, itemPersonalRisk);
					
					switch (item.getObjectId()) {
						//fill Personal - AP
						case Item.OBJECT_ACCIDENTES_PERSONALES:
							this.dataFillPersonalAP(personType, itemPersonalRisk);
							break;
							
							//fill Personal - Purchase
						case Item.OBJECT_PURCHASE_PROTECTED:
							this.dataFillPersonalPurchase(((PersonWithCardType)personType).getCard(), (ItemPurchaseProtected)itemPersonalRisk, true, true);
							break;
							
						//fill Risk Group
						case Item.OBJECT_CICLO_VITAL:
							itemPersonalRisk.getItemPersonalRiskGroups().clear();
							if (item.getId().getItemId() == 1) {
								for (int groupId = 1; groupId <= ((PersonWithGroupType)personType).getGroup().size(); groupId++) {
									//find Group
									PersonGroupType personGroupType = this.findPersonGroupType(((PersonWithGroupType)personType).getGroup(), groupId);
									if (personGroupType == null) {
										throw new ServerException((new Long(ErrorTypes.VALIDATION_RISK_GROUP_OUT_OF_SEQUENCE.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_RISK_GROUP_OUT_OF_SEQUENCE.getMessage(null));
									}
									
									//add Group
									ItemPersonalRiskGroup itemPersonalRiskGroup = itemPersonalRisk.addItemPersonalRiskGroup();
									
									//fill Personal Group
									this.dataFillPersonalGroup(personGroupType, itemPersonalRiskGroup);
									
									//fill Complement
									itemPersonalRiskGroup.setEffectiveDate(itemPersonalRisk.getEffectiveDate());
								}
							}
							break;
					}
					
					//fill Complement - item Principal
					if (item.getId().getItemId() == 1) {
						//fill Beneficiary
						int percentage = 0;
						itemPersonalRisk.getBeneficiaries().clear();
						for (int beneficiaryId = 1; beneficiaryId <= adapterItem.getBeneficiary().size(); beneficiaryId++) {
							//find Beneficiary
							BeneficiaryType beneficiaryType = this.findBeneficiaryType(adapterItem.getBeneficiary(), beneficiaryId);
							if (beneficiaryType == null) {
								throw new ServerException((new Long(ErrorTypes.VALIDATION_BENEFICIARY_OUT_OF_SEQUENCE.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_BENEFICIARY_OUT_OF_SEQUENCE.getMessage(null));
							}
							
							//add Beneficiary
							Beneficiary beneficiary = itemPersonalRisk.addBeneficiary();
							
							//fill Beneficiary
							this.dataFillBeneficiary(beneficiaryType, beneficiary);
							
							percentage += beneficiaryType.getPercentage();
						}
						
						if (adapterItem.getBeneficiary().size() > 0 && percentage != 100) {
							throw new ServerException((new Long(ErrorTypes.VALIDATION_BENEFICIARY_PARTICIPATION_DISPARITY.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_BENEFICIARY_PARTICIPATION_DISPARITY.getMessage(null));
						}
						
						//fill Personal
						itemPersonalRisk.setUndocumented(false);
						itemPersonalRisk.setDocumentType(insured.getDocumentType());
						itemPersonalRisk.setDocumentNumber(insured.getDocumentNumber());
						
						insured.setBirthDate(itemPersonalRisk.getBirthDate());
						insured.setGender(itemPersonalRisk.getGender());
						
						//fill Personal - AP
						if (item.getObjectId() == Item.OBJECT_ACCIDENTES_PERSONALES) {
							insured.setOccupationId(itemPersonalRisk.getOccupationId());
						}
					}
					break;
					
				case Item.OBJECT_HABITAT:
				case Item.OBJECT_CAPITAL:
					PropertyType propertyType = adapterItem.getProperty();
					ItemProperty itemProperty = (ItemProperty)item;
					
					//fill Property - ALL
					this.dataFillProperty(propertyType, itemProperty, false);
					
					//fill Property - Address
					this.dataFillPropertyAddress(propertyType, itemProperty, insured.getFirstAddress());
					
					//fill Property - Risk
					if (propertyType.getRisk() == null || propertyType.getRisk().size() == 0)
						throw new ServerException((new Long(ErrorTypes.VALIDATION_PROPERTY_RISK_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PROPERTY_RISK_NOT_FOUND.getMessage(null));
					
					this.dataFillPropertyRisk(propertyType.getRisk(), itemProperty);
					
					break;
					
				default:
					throw new IllegalArgumentException();
			}
			
			//fill Coverage
			this.dataFillCoverage(adapterItem.getCoverage(), item);
		}
		return endorsement;
	}

	private Endorsement dataFillChangeRegister(ContractType contractType) throws ServerException, SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, ServiceException {
		// find Endorsement
		Endorsement endorsement = daoPolicy.loadEndorsement(contractType.getEndorsement().getEndorsementID(), contractType.getContractID());
		if (endorsement == null) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_ENDORSEMENT_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_ENDORSEMENT_NOT_FOUND.getMessage(null));
		}
		else {
			if (endorsement.getIssuingStatus() != Endorsement.ISSUING_STATUS_EMITIDA) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_ACTION_IS_NOT_ALLOWED.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_ACTION_IS_NOT_ALLOWED.getMessage(null));
			}
			
			// remove Object from the Session
			daoPolicy.evict(endorsement);
		}
		
		//fill Endorsement
		EndorsementType endorsementType = contractType.getEndorsement();
		
		//fill Insured
		Customer insured = endorsement.getCustomerByInsuredId();
		InsuredType insuredType = endorsementType.getInsured();
		this.dataFillCustomer(insuredType, insured, true);
		
		//fill Address - Insured
		AddressType addressInsured = insuredType.getAddress();
		if (addressInsured != null) {
			Address address = insured.getFirstAddress();
			this.dataFillAddress(addressInsured, address);
			
			//fill Phone - Insured
//			address.getPhones().clear();
			for (PhoneType phoneType: addressInsured.getPhone()) {
				if (phoneType.getPhoneNumber() != null && !"".equals(phoneType.getPhoneNumber()) && !(new BigDecimal(0)).equals(phoneType.getPhoneNumber())) {
					Phone phone = address.getFirstPhone();
					if (phone == null) {
						phone = address.addPhone();
					}
					this.dataFillPhone(phoneType, phone);
				}
			}
			if (address.getPhones() == null || address.getPhones().size() == 0) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PHONE_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PHONE_NOT_FOUND.getMessage(null));
			}
		}
		
		//fill Holder
		if (endorsement.getCustomerByPolicyHolderId().getCustomerId() != endorsement.getCustomerByInsuredId().getCustomerId()) {
			if (endorsementType.getHolder() != null) {
				Customer holder = endorsement.getCustomerByPolicyHolderId();
				HolderType holderType = endorsementType.getHolder();
				this.dataFillCustomer(new InsuredTypeProxy(holderType), holder, true);
				
				//fill Address - Holder
				AddressType addressHolder = holderType.getAddress();
				if (addressHolder != null) {
					Address address = holder.getFirstAddress();
					this.dataFillAddress(addressHolder, address);
					
					//fill Phone - Holder
//					address.getPhones().clear();
					for (PhoneType phoneType: addressHolder.getPhone()) {
						if (phoneType.getPhoneNumber() != null && !"".equals(phoneType.getPhoneNumber()) && !(new BigDecimal(0)).equals(phoneType.getPhoneNumber())) {
							Phone phone = address.getFirstPhone();
							if (phone == null) {
								phone = address.addPhone();
							}
							this.dataFillPhone(phoneType, phone);
						}
					}
					if (address.getPhones() == null || address.getPhones().size() == 0) {
						throw new ServerException((new Long(ErrorTypes.VALIDATION_PHONE_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PHONE_NOT_FOUND.getMessage(null));
					}
				}
			}
		}
		
		//fill Item
		for (Item item : endorsement.getItems()) {
			//find Item
			AdapterItem adapterItem = this.findItemType(endorsementType.getItem(), item.getId().getItemId());
			if (adapterItem == null) {
				continue;
			}
			
			//fill Item Object
			switch (item.getObjectId()) {
				case Item.OBJECT_VIDA:
				case Item.OBJECT_CREDITO:
				case Item.OBJECT_CICLO_VITAL:
				case Item.OBJECT_PURCHASE_PROTECTED:
				case Item.OBJECT_ACCIDENTES_PERSONALES:
					PersonBasicType personType = adapterItem.getPerson();
					ItemPersonalRisk itemPersonalRisk = (ItemPersonalRisk)item;
					
					if (insured.getPersonType() == null || !insured.getPersonType().equals(Customer.PERSON_TYPE_NATURAL))
						throw new ServerException((new Long(ErrorTypes.VALIDATION_PERSON_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PERSON_TYPE_INVALID.getMessage(null));
					
					//fill Personal - ALL
					if (item.getId().getItemId() != 1) {
						this.dataFillPersonal(personType, itemPersonalRisk, true);
					}
					
					switch (item.getObjectId()) {
						//fill Personal - Purchase
						case Item.OBJECT_PURCHASE_PROTECTED:
							this.dataFillPersonalPurchase(((PersonWithCardType)personType).getCard(), (ItemPurchaseProtected)itemPersonalRisk, true, false);
							break;
							
						//fill Risk Group
						case Item.OBJECT_CICLO_VITAL:
							if (item.getId().getItemId() == 1) {
								for (ItemPersonalRiskGroup itemPersonalRiskGroup : itemPersonalRisk.getItemPersonalRiskGroups()) {
									//find Group
									PersonGroupType personGroupType = this.findPersonGroupType(((PersonWithGroupType)personType).getGroup(), itemPersonalRiskGroup.getId().getRiskGroupId());
									if (personGroupType == null) {
										continue;
									}
									
									//fill Personal Group
									this.dataFillPersonalGroup(personGroupType, itemPersonalRiskGroup, true);
								}
							}
							break;
					}
					
					//fill Complement - item Principal
					if (item.getId().getItemId() == 1) {
						//fill Beneficiary
						int percentage = 0;
						itemPersonalRisk.getBeneficiaries().clear();
						for (int beneficiaryId = 1; beneficiaryId <= adapterItem.getBeneficiary().size(); beneficiaryId++) {
							//find Beneficiary
							BeneficiaryType beneficiaryType = this.findBeneficiaryType(adapterItem.getBeneficiary(), beneficiaryId);
							if (beneficiaryType == null) {
								throw new ServerException((new Long(ErrorTypes.VALIDATION_BENEFICIARY_OUT_OF_SEQUENCE.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_BENEFICIARY_OUT_OF_SEQUENCE.getMessage(null));
							}
							
							//add Beneficiary
							Beneficiary beneficiary = itemPersonalRisk.addBeneficiary();
							
							//fill Beneficiary
							this.dataFillBeneficiary(beneficiaryType, beneficiary);
							
							percentage += beneficiaryType.getPercentage();
						}
						
						if (adapterItem.getBeneficiary().size() > 0 && percentage != 100) {
							throw new ServerException((new Long(ErrorTypes.VALIDATION_BENEFICIARY_PARTICIPATION_DISPARITY.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_BENEFICIARY_PARTICIPATION_DISPARITY.getMessage(null));
						}
						
						//fill Personal
						itemPersonalRisk.setUndocumented(false);
						itemPersonalRisk.setDocumentType(insured.getDocumentType());
						itemPersonalRisk.setDocumentNumber(insured.getDocumentNumber());
						
						insured.setBirthDate(itemPersonalRisk.getBirthDate());
						insured.setGender(itemPersonalRisk.getGender());
						
						//fill Personal - AP
						if (item.getObjectId() == Item.OBJECT_ACCIDENTES_PERSONALES) {
							insured.setOccupationId(itemPersonalRisk.getOccupationId());
						}
					}
					break;
					
				case Item.OBJECT_HABITAT:
				case Item.OBJECT_CAPITAL:
					PropertyType propertyType = adapterItem.getProperty();
					ItemProperty itemProperty = (ItemProperty)item;
					
					//fill Property - ALL
					this.dataFillProperty(propertyType, itemProperty, true);
					
					//fill Property - Address
					this.dataFillPropertyAddress(propertyType, itemProperty, insured.getFirstAddress());
					
					break;
					
				default:
					throw new IllegalArgumentException();
			}
		}
		return endorsement;
	}

	private PaymentOption dataFillPaymentOption(PaymentOptionDataType paymentOptionType, Endorsement endorsement)  throws ServerException, ServiceException {
		PaymentOption paymentOption = null;
		if (paymentOptionType != null) {
			// validate Payment Term
			ProductPaymentTerm productPaymentTerm = serviceEinsurance.getProductPaymentTerm(endorsement.getContract().getProductId(), paymentOptionType.getPaymentTermID(), paymentOptionType.getBillingMethodID(), endorsement.getReferenceDate());
			if (productPaymentTerm == null)
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PAYMENT_OPTION_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PAYMENT_OPTION_INVALID.getMessage(null));
			
			// find Payment Option
			paymentOption = serviceBasicCalc.getPaymentOptionByPaymentTermId(endorsement.getTotalPremium(), endorsement.getTaxRate(), endorsement.getContract().getProductId(), paymentOptionType.getBillingMethodID(), paymentOptionType.getPaymentTermID(), endorsement.getReferenceDate(), endorsement.getEffectiveDate(), endorsement.getExpiryDate());
			if (paymentOption == null && endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_EMISSION)
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PAYMENT_OPTION_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PAYMENT_OPTION_INVALID.getMessage(null));
			
			if (paymentOption != null) {
				//fill Billing Method
				BillingMethod billingMethod = productPaymentTerm.getBillingMethod();
				switch (billingMethod.getPaymentType()) {
					case BillingMethod.PAYMENT_TYPE_DEBIT_ACCOUNT:
					case BillingMethod.PAYMENT_TYPE_ACCOUNT_SAVINGS:
						PaymentOptionDataType.Bank bank = paymentOptionType.getBank();
						if (bank != null) {
							paymentOption.setBankNumber(billingMethod.getBankNumber());
							
							if (bank.getAgencyNumber() != null)
								paymentOption.setBankAgencyNumber(bank.getAgencyNumber());
							
							if (bank.getAccountNumber() == null || "".equals(bank.getAccountNumber()) || (new BigDecimal(0)).equals(bank.getAccountNumber())) {
								throw new ServerException((new Long(ErrorTypes.VALIDATION_BANK_ACCOUNT_NUMBER_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_BANK_ACCOUNT_NUMBER_INVALID.getMessage(null));
							}
							paymentOption.setBankAccountNumber(bank.getAccountNumber());
							
							if (billingMethod.getValidationClassMethod() != null && "checkBankAccount".equalsIgnoreCase(billingMethod.getValidationClassMethod())) {
								if (!Utilities.validateBankAccountVE(paymentOption.getBankNumber(), paymentOption.getBankAccountNumber())) {
									throw new ServerException((new Long(ErrorTypes.VALIDATION_BANK_ACCOUNT_NUMBER_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_BANK_ACCOUNT_NUMBER_INVALID.getMessage(null));
								}
								else {
									paymentOption.setBankAgencyNumber(paymentOption.getBankAccountNumber().substring(4,8));
								}
							}
						}
						else {
							throw new ServerException((new Long(ErrorTypes.VALIDATION_PAYMENT_OPTION_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PAYMENT_OPTION_INVALID.getMessage(null));
						}
						break;
						
					case BillingMethod.PAYMENT_TYPE_CHECK:
						CheckType check = paymentOptionType.getCheck();
						if (check != null) {
							paymentOption.setBankNumber(billingMethod.getBankNumber());
							
							if (check.getAgencyNumber() != 0)
								paymentOption.setBankAgencyNumber(NumberUtilities.parseNull(check.getAgencyNumber()).toString());
							
							if (check.getAccountNumber() == null || "".equals(check.getAccountNumber()) || (new BigDecimal(0)).equals(check.getAccountNumber())) {
								throw new ServerException((new Long(ErrorTypes.VALIDATION_BANK_ACCOUNT_NUMBER_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_BANK_ACCOUNT_NUMBER_INVALID.getMessage(null));
							}
							paymentOption.setBankAccountNumber(check.getAccountNumber());
							
							if (billingMethod.getValidationClassMethod() != null && "checkBankAccount".equalsIgnoreCase(billingMethod.getValidationClassMethod())) {
								if (!Utilities.validateBankAccountVE(paymentOption.getBankNumber(), paymentOption.getBankAccountNumber())) {
									throw new ServerException((new Long(ErrorTypes.VALIDATION_BANK_ACCOUNT_NUMBER_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_BANK_ACCOUNT_NUMBER_INVALID.getMessage(null));
								}
								else {
									paymentOption.setBankAgencyNumber(paymentOption.getBankAccountNumber().substring(4,8));
								}
							}
							
							if (check.getCheckNumber() == null || "".equals(check.getCheckNumber())) {
								throw new ServerException((new Long(ErrorTypes.VALIDATION_BANK_CHECK_NUMBER_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_BANK_CHECK_NUMBER_INVALID.getMessage(null));
							}
							paymentOption.setBankCheckNumber(check.getCheckNumber());
						}
						else {
							throw new ServerException((new Long(ErrorTypes.VALIDATION_PAYMENT_OPTION_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PAYMENT_OPTION_INVALID.getMessage(null));
						}
						break;
						
					case BillingMethod.PAYMENT_TYPE_CREDIT_CARD:
					case BillingMethod.PAYMENT_TYPE_DEBIT_CARD:
						PaymentOptionDataType.Card card = paymentOptionType.getCard();
						if (card != null) {
							paymentOption.setCardType(billingMethod.getCardType());
							paymentOption.setCardBrandType(billingMethod.getCardBrandType());
							
							if (card.getCardNumber() == null || "".equals(card.getCardNumber()) || (new BigDecimal(0)).equals(card.getCardNumber())) {
								throw new ServerException((new Long(ErrorTypes.VALIDATION_CARD_NUMBER_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_CARD_NUMBER_INVALID.getMessage(null));
							}
							paymentOption.setCardNumber(card.getCardNumber());
							
							if (card.getCardExpirationDate() == 0 || Utilities.parseDate((new Integer(card.getCardExpirationDate())).toString() + "01") == null) {
								throw new ServerException((new Long(ErrorTypes.VALIDATION_CARD_EXPIRATION_DATE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_CARD_EXPIRATION_DATE_INVALID.getMessage(null));
							}
							paymentOption.setCardExpirationDate(card.getCardExpirationDate());
							
							if (card.getCardHolderName() == null || "".equalsIgnoreCase(card.getCardHolderName())) {
								throw new ServerException((new Long(ErrorTypes.VALIDATION_CARD_HOLDER_NAME_ABSENT.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_CARD_HOLDER_NAME_ABSENT.getMessage(null));
							}
							paymentOption.setCardHolderName(card.getCardHolderName());
						}
						else {
							throw new ServerException((new Long(ErrorTypes.VALIDATION_PAYMENT_OPTION_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PAYMENT_OPTION_INVALID.getMessage(null));
						}
						break;
						
					default:
						PaymentOptionDataType.Other other = paymentOptionType.getOther();
						if (other != null) {
							if (other.getOtherNumber() == null || "".equals(other.getOtherNumber()) || (new BigDecimal(0)).equals(other.getOtherNumber())) {
								throw new ServerException((new Long(ErrorTypes.VALIDATION_OTHER_ACCOUNT_NUMBER_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_OTHER_ACCOUNT_NUMBER_INVALID.getMessage(null));
							}
							paymentOption.setOtherAccountNumber(other.getOtherNumber());
						}
						break;
				}
			}
		}
		else {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_PAYMENT_OPTION_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PAYMENT_OPTION_INVALID.getMessage(null));
		}
		return paymentOption;
	}

	private void dataFillContract(ContractType contractType, Contract contract) throws ServerException, SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		MasterPolicy masterPolicy = daoContract.getMasterPolicyById(contractType.getPolicyID());
		if (masterPolicy == null)
			throw new ServerException((new Long(ErrorTypes.VALIDATION_MASTER_POLICY_ID_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_MASTER_POLICY_ID_INVALID.getMessage(null));
		
		Channel channel = daoContract.getChannelById(masterPolicy.getPartnerId(), contractType.getEndorsement().getChannelID());
		if (channel == null)
			throw new ServerException((new Long(ErrorTypes.VALIDATION_CHANNEL_ID_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_CHANNEL_ID_INVALID.getMessage(null));
		
		//fill Contract
		if (contractType.getExternalKey() != null)
			contract.setExternalKey(contractType.getExternalKey());
		
		if (contractType.getTermID() != 0)
			contract.setTermId(contractType.getTermID());
		
		//fill Complement
		contract.setInsurerId(masterPolicy.getInsurerId());
		contract.setSubsidiaryId(channel.getSubsidiaryId());
		contract.setBrokerId(masterPolicy.getBrokerId());
		contract.setPartnerId(masterPolicy.getPartnerId());
		contract.setMasterPolicyId(masterPolicy.getMasterPolicyId());
		contract.setPolicyNumber(masterPolicy.getPolicyNumber());
		contract.setProductId(masterPolicy.getProductId());
		
		if (contractType.getPermanencyDate() != null)
			contract.setRenewalLimitDate(DateUtilities.xmlGregorianCalendarToDate(contractType.getPermanencyDate()));
	}

	private void dataFillEndorsement(EndorsementType endorsementType, Endorsement endorsement) throws ServerException {
		ErrorList errorList = new ErrorList();
		endorsement.setErrorList(errorList);
		
		//fill Endorsement
		if (endorsementType.getIssuanceTypeID() != 0)
			endorsement.setIssuanceType(endorsementType.getIssuanceTypeID());
		
		if (endorsementType.getChannelID() != 0)
			endorsement.setChannelId(endorsementType.getChannelID());
		
		if (endorsementType.getUserID() != 0)
			endorsement.setUserId(endorsementType.getUserID());
		
		if (endorsementType.getRiskPlanID() != 0)
			endorsement.setRiskPlanId(endorsementType.getRiskPlanID());
		
		if (endorsementType.getEffectiveDate() != null)
			endorsement.setEffectiveDate(DateUtilities.xmlGregorianCalendarToDate(endorsementType.getEffectiveDate()));
		
		if (endorsementType.getExpiryDate() != null)
			endorsement.setExpiryDate(DateUtilities.xmlGregorianCalendarToDate(endorsementType.getExpiryDate()));
		
		//fill Complement
		if (endorsementType.getVersionDate() != null)
			endorsement.setReferenceDate(DateUtilities.xmlGregorianCalendarToDate(endorsementType.getVersionDate()));
		
		if (endorsementType.getExemptionTypeID() != 0)
			endorsement.setTaxExemptionType(endorsementType.getExemptionTypeID());
		
		endorsement.setHierarchyType(ID_HIERARCHY_TYPE_DEFAULT);
	}
	
	private int getObjectId(int productId, Date versionDate) throws ServerException {
		//get version to effective date
		ProductVersion productVersion = daoProductVersion.getProductVersionByBetweenRefDate(productId, versionDate);
		if (productVersion != null) {
			return productVersion.getProduct().getObjectId();
		}
		else {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_PRODUCT_ID_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PRODUCT_ID_INVALID.getMessage(null));
		}
	}
	
	private void dataFillItem(AdapterItem itemType, Item item) throws ServerException {		
		//fill Item
		if (itemType.getPlanID() != 0)
			item.setCoveragePlanId(itemType.getPlanID());
		
		if (itemType.getRenewalTypeID() != 0)
			item.setRenewalType(itemType.getRenewalTypeID());
		
		if (itemType.getRenewalInsurerID() != null && !itemType.getRenewalInsurerID().equals(0))
			item.setRenewalInsurerId(itemType.getRenewalInsurerID());
		
		if (itemType.getRenewalPolicyNumber() != null && !itemType.getRenewalPolicyNumber().equals(0))
			item.setRenewalPolicyNumber(itemType.getRenewalPolicyNumber().toString());
		
		if (itemType.getBonusTypeID() != null && !itemType.getBonusTypeID().equals(0))
			item.setBonusType(itemType.getBonusTypeID());
		
		if (itemType.getEffectiveDate() != null)
			item.setEffectiveDate(DateUtilities.xmlGregorianCalendarToDate(itemType.getEffectiveDate()));
		
		if (item.getEffectiveDate() == null)
			item.setEffectiveDate(item.getEndorsement().getEffectiveDate());
	}

	private AdapterItem findItemType(List<ItemType> itemTypeList, int itemId) {
		//fill Item
		for (ItemType itemType: itemTypeList) {
			AdapterItem adapterItem = new AdapterItem(itemType);
			if (adapterItem.getItemID() == itemId) {
				return adapterItem;
			}
		}
		return null;
	}

	private void dataFillPersonal(PersonBasicType personType, ItemPersonalRisk itemPersonalRisk) throws ServerException {
		this.dataFillPersonal(personType, itemPersonalRisk, false);
	}

	private void dataFillPersonal(PersonBasicType personType, ItemPersonalRisk itemPersonalRisk, boolean isChange) throws ServerException {
		DataOption dataOption = null;
		
		//fill Personal - ALL
		if (!isChange) {
			dataOption = daoDataOption.getByFieldName("PersonalRiskType", personType.getRiskTypeID());
			if (dataOption == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PERSONAL_RISK_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PERSONAL_RISK_TYPE_INVALID.getMessage(null));
			}
			if (itemPersonalRisk.getId().getItemId() == 1 && personType.getRiskTypeID() != ItemPersonalRisk.PERSONAL_RISK_TYPE_TITULAR) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PERSONAL_RISK_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PERSONAL_RISK_TYPE_INVALID.getMessage(null));
			}
			else if (itemPersonalRisk.getId().getItemId() > 1 && personType.getRiskTypeID() != ItemPersonalRisk.PERSONAL_RISK_TYPE_ADDITIONAL) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PERSONAL_RISK_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PERSONAL_RISK_TYPE_INVALID.getMessage(null));
			}
			itemPersonalRisk.setPersonalRiskType(personType.getRiskTypeID());
			
			itemPersonalRisk.setUndocumented(personType.isUndocumented());
			
			if (personType.getDocumentTypeID() != null && !(new Integer(0)).equals(personType.getDocumentTypeID())) {
				dataOption = daoDataOption.getByFieldName("DocumentType", personType.getDocumentTypeID());
				if (dataOption == null) {
					throw new ServerException((new Long(ErrorTypes.VALIDATION_DOCUMENT_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_DOCUMENT_TYPE_INVALID.getMessage(null));
				}
				itemPersonalRisk.setUndocumented(false);
				itemPersonalRisk.setDocumentType(personType.getDocumentTypeID());
			}
			
			if (personType.getDocumentNumber() != null && !(new BigDecimal(0)).equals(personType.getDocumentNumber()))
				itemPersonalRisk.setDocumentNumber(personType.getDocumentNumber().toString());
		}
		
		if (personType.getFirstName() != null)
			itemPersonalRisk.setFirstName(personType.getFirstName());
		
		if (personType.getLastName() != null)
			itemPersonalRisk.setLastName(personType.getLastName());
		
		if (!isChange) {
			if (personType.getBirthDate() != null)
				itemPersonalRisk.setBirthDate(DateUtilities.xmlGregorianCalendarToDate(personType.getBirthDate()));
			
			if (personType.getGenderID() != null && !(new Integer(0)).equals(personType.getGenderID())) {
				dataOption = daoDataOption.getByFieldName("Gender", personType.getGenderID());
				if (dataOption == null) {
					throw new ServerException((new Long(ErrorTypes.VALIDATION_GENDER_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_GENDER_INVALID.getMessage(null));
				}
				itemPersonalRisk.setGender(personType.getGenderID());
			}
		}
		
		if (personType.getMaritalStatusID() != null && !(new Integer(0)).equals(personType.getMaritalStatusID())) {
			dataOption = daoDataOption.getByFieldName("MaritalStatus", personType.getMaritalStatusID());
			if (dataOption == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_MARITAL_STATUS_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_MARITAL_STATUS_INVALID.getMessage(null));
			}
			itemPersonalRisk.setMaritalStatus(personType.getMaritalStatusID());
		}
		
		if (!isChange) {
			itemPersonalRisk.setSmoker(personType.isSmoker());
		}
		
		if (personType.getKinshipTypeID() != null && !(new Integer(0)).equals(personType.getKinshipTypeID())) {
			dataOption = daoDataOption.getByFieldName("KinshipType", personType.getKinshipTypeID());
			if (dataOption == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PERSONAL_KINSHIP_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PERSONAL_KINSHIP_TYPE_INVALID.getMessage(null));
			}
			itemPersonalRisk.setKinshipType(personType.getKinshipTypeID());
		}
	}

	private void dataFillPersonalAP(PersonBasicType personType, ItemPersonalRisk itemPersonalRisk) throws ServerException {
		DataOption dataOption = null;
		
		//fill Personal - AP
		Occupation occupation = daoInsurer.getOccupationById(personType.getOccupationID());
		if (occupation == null) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_OCCUPATION_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_OCCUPATION_INVALID.getMessage(null));
		}
		itemPersonalRisk.setOccupationId(personType.getOccupationID());
		itemPersonalRisk.setOccupationRiskType(occupation.getOccupationRiskType());
		
		if (personType.getRestrictionTypeID() != null && !personType.getRestrictionTypeID().equals(0)) {
			dataOption = daoDataOption.getByFieldName("RestrictionRiskType", personType.getRestrictionTypeID());
			if (dataOption == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_RESTRICTION_RISK_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_RESTRICTION_RISK_TYPE_INVALID.getMessage(null));
			}
			itemPersonalRisk.setRestrictionRiskType(personType.getRestrictionTypeID());
		}
		else {
			itemPersonalRisk.setRestrictionRiskType(ID_RESTRICTION_RISK_TYPE_DEFAULT);
		}
	}

	private void dataFillPersonalPurchase(CardType cardType, ItemPurchaseProtected itemPurchaseProtected, boolean isProposal) throws ServerException {
		this.dataFillPersonalPurchase(cardType, itemPurchaseProtected, isProposal, !isProposal);
	}

	private void dataFillPersonalPurchase(CardType cardType, ItemPurchaseProtected itemPurchaseProtected, boolean isProposal, boolean isChange) throws ServerException {
		DataOption dataOption = null;
		
		// fill Personal - Purchase
		if (isChange) {
			dataOption = daoDataOption.getByFieldName("CardType", cardType.getCardTypeID());
			if (dataOption == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_CARD_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_CARD_TYPE_INVALID.getMessage(null));
			}
			itemPurchaseProtected.setCardType(cardType.getCardTypeID());
			
			dataOption = daoDataOption.getByFieldName("CardBrandType", cardType.getCardBrandTypeID(), cardType.getCardTypeID());
			if (dataOption == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_CARD_BRANDE_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_CARD_BRANDE_TYPE_INVALID.getMessage(null));
			}
			itemPurchaseProtected.setCardBrandType(cardType.getCardBrandTypeID());
		}
		
		if (cardType.getCardNumber() != null && !"".equals(cardType.getCardNumber()) && !(new BigDecimal(0)).equals(cardType.getCardNumber())) {
			itemPurchaseProtected.setCardNumber(cardType.getCardNumber());
		} 
		else {
			if (isProposal) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_CARD_NUMBER_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_CARD_NUMBER_INVALID.getMessage(null));
			}
		}
		
		if (cardType.getCardExpirationDate() != null && !"".equals(cardType.getCardExpirationDate()) && Utilities.parseDate(cardType.getCardExpirationDate() + "01") != null) {
			itemPurchaseProtected.setCardExpirationDate(new Integer(cardType.getCardExpirationDate()));
		}
		else {
			if (isProposal) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_CARD_EXPIRATION_DATE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_CARD_EXPIRATION_DATE_INVALID.getMessage(null));
			}
		}
		
		if (cardType.getCardHolderName() != null && !"".equals(cardType.getCardHolderName())) {
			itemPurchaseProtected.setCardHolderName(cardType.getCardHolderName());
		}
		else {
			if (isProposal) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_CARD_HOLDER_NAME_ABSENT.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_CARD_HOLDER_NAME_ABSENT.getMessage(null));
			}
		}
	}

	private void dataFillPersonalGroup(PersonGroupType personGroupType, ItemPersonalRiskGroup itemPersonalRiskGroup) throws ServerException {
		this.dataFillPersonalGroup(personGroupType, itemPersonalRiskGroup, false);
	}

	private void dataFillPersonalGroup(PersonGroupType personGroupType, ItemPersonalRiskGroup itemPersonalRiskGroup, boolean isChange) throws ServerException {
		DataOption dataOption = null;
		
		//fill Personal Group
		if (!isChange) {
			dataOption = daoDataOption.getByFieldName("PersonalRiskType", personGroupType.getRiskTypeID());
			if (dataOption == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PERSONAL_RISK_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PERSONAL_RISK_TYPE_INVALID.getMessage(null));
			}
			if (personGroupType.getRiskTypeID() != ItemPersonalRisk.PERSONAL_RISK_TYPE_FAMILY_GROUP) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PERSONAL_RISK_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PERSONAL_RISK_TYPE_INVALID.getMessage(null));
			}
			itemPersonalRiskGroup.setPersonalRiskType(personGroupType.getRiskTypeID());
			
			itemPersonalRiskGroup.setUndocumented(personGroupType.isUndocumented());
			
			if (personGroupType.getDocumentTypeID() != null && !(new Integer(0)).equals(personGroupType.getDocumentTypeID())) {
				dataOption = daoDataOption.getByFieldName("DocumentType", personGroupType.getDocumentTypeID());
				if (dataOption == null) {
					throw new ServerException((new Long(ErrorTypes.VALIDATION_DOCUMENT_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_DOCUMENT_TYPE_INVALID.getMessage(null));
				}
				itemPersonalRiskGroup.setUndocumented(false);
				itemPersonalRiskGroup.setDocumentType(personGroupType.getDocumentTypeID());
			}
			
			if (personGroupType.getDocumentNumber() != null && !(new BigDecimal(0)).equals(personGroupType.getDocumentNumber()))
				itemPersonalRiskGroup.setDocumentNumber(personGroupType.getDocumentNumber().toString());
		}
		
		if (personGroupType.getFirstName() != null && !"".equals(personGroupType.getFirstName()))
			itemPersonalRiskGroup.setFirstName(personGroupType.getFirstName());
		
		if (personGroupType.getLastName() != null && !"".equals(personGroupType.getLastName()))
			itemPersonalRiskGroup.setLastName(personGroupType.getLastName());
		
		if (personGroupType.getBirthDate() != null)
			itemPersonalRiskGroup.setBirthDate(DateUtilities.xmlGregorianCalendarToDate(personGroupType.getBirthDate()));
		
		if (personGroupType.getGenderID() != 0) {
			dataOption = daoDataOption.getByFieldName("Gender", personGroupType.getGenderID());
			if (dataOption == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_GENDER_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_GENDER_INVALID.getMessage(null));
			}
			itemPersonalRiskGroup.setGender(personGroupType.getGenderID());
		}
		
		if (personGroupType.getMaritalStatusID() != 0) {
			dataOption = daoDataOption.getByFieldName("MaritalStatus", personGroupType.getMaritalStatusID());
			if (dataOption == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_MARITAL_STATUS_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_MARITAL_STATUS_INVALID.getMessage(null));
			}
			itemPersonalRiskGroup.setMaritalStatus(personGroupType.getMaritalStatusID());
		}
		
		if (personGroupType.getKinshipTypeID() != 0) {
			dataOption = daoDataOption.getByFieldName("KinshipType", personGroupType.getKinshipTypeID());
			if (dataOption == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PERSONAL_KINSHIP_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PERSONAL_KINSHIP_TYPE_INVALID.getMessage(null));
			}
			itemPersonalRiskGroup.setKinshipType(personGroupType.getKinshipTypeID());
		}
		
		if (personGroupType.getEffectiveDate() != null)
			itemPersonalRiskGroup.setEffectiveDate(DateUtilities.xmlGregorianCalendarToDate(personGroupType.getEffectiveDate()));
		
		if (itemPersonalRiskGroup.getEffectiveDate() == null)
			itemPersonalRiskGroup.setEffectiveDate(itemPersonalRiskGroup.getItemPersonalRisk().getEffectiveDate());
	}

	private void dataFillProperty(PropertyType propertyType, ItemProperty itemProperty, boolean isChange) throws ServerException, ServiceException {
		DataOption dataOption = null;
		
		//fill Property - ALL
		if (!isChange) {
			InsuredObject object = serviceEinsurance.getObject(itemProperty.getObjectId(), itemProperty.getEndorsement().getReferenceDate());
			
			dataOption = daoDataOption.getByFieldName("PropertyType", propertyType.getPropertyTypeID(), object.getObjectType());
			if (dataOption == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PROPERTY_RISK_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PROPERTY_RISK_TYPE_INVALID.getMessage(null));
			}
			itemProperty.setPropertyType(propertyType.getPropertyTypeID());
			
			if (itemProperty.getObjectId() == Item.OBJECT_CAPITAL) {
				dataOption = daoDataOption.getByFieldName("ActivityType", propertyType.getActivityTypeID(), itemProperty.getPropertyType());
				if (dataOption == null) {
					throw new ServerException((new Long(ErrorTypes.VALIDATION_PROPERTY_ACTIVITY_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PROPERTY_ACTIVITY_TYPE_INVALID.getMessage(null));
				}
				itemProperty.setActivityType(propertyType.getActivityTypeID());
			}
		}
		
		if (propertyType.getPropertyValue() != null && !(new BigDecimal(0)).equals(propertyType.getPropertyValue()))
			itemProperty.setPropertyValue(propertyType.getPropertyValue().doubleValue());
		
		if (propertyType.getAcquisitionDate() != null)
			itemProperty.setAcquisitionDate(DateUtilities.xmlGregorianCalendarToDate(propertyType.getAcquisitionDate()));
		
		if (propertyType.getRegistrationDate() != null)
			itemProperty.setRegistrationDate(DateUtilities.xmlGregorianCalendarToDate(propertyType.getRegistrationDate()));
		
		if (propertyType.getRegistrationName() != null && !"".equals(propertyType.getRegistrationName()))
			itemProperty.setRegistrationName(propertyType.getRegistrationName());
		
		if (propertyType.getRegistrationTome() != null && !"".equals(propertyType.getRegistrationTome()))
			itemProperty.setRegistrationTome(propertyType.getRegistrationTome());
		
		if (propertyType.getRegistrationNumber() != null && !"".equals(propertyType.getRegistrationNumber()))
			itemProperty.setRegistrationNumber(propertyType.getRegistrationNumber());
		
		if (propertyType.getRegistrationProtocol() != null && !"".equals(propertyType.getRegistrationProtocol()))
			itemProperty.setRegistrationProtocol(propertyType.getRegistrationProtocol());
		
		itemProperty.setSingleAddress(propertyType.isSingleAddress());
	}

	private void dataFillPropertyAddress(PropertyType propertyType, ItemProperty itemProperty, Address insuredAddress) throws ServerException {
		//fill Property - Address
		String[] ignoreProperties = { "addressId", "customer", "addressType", "customers", "phones", "active" };
		if (propertyType.isSingleAddress()) {
			if (insuredAddress != null) {
				//fill Property - Single
				BeanUtils.copyProperties(insuredAddress, itemProperty, ignoreProperties);
			}
		}
		else {
			if (propertyType.getAddress() != null) {
				//fill Property - Other
				Address address = new Address();
				this.dataFillAddress(propertyType.getAddress(), address);
				BeanUtils.copyProperties(address, itemProperty, ignoreProperties);
			}
		}
	}

	private void dataFillPropertyRisk(List<RiskType> riskTypeList, ItemProperty item) throws ServerException, ServiceException {
		DataOption dataOption = null;
		InsuredObject object = serviceEinsurance.getObject(item.getObjectId(), item.getEndorsement().getReferenceDate());
		
		//fill Risks
		item.getItemRiskTypes().clear();
		if (riskTypeList != null && riskTypeList.size() > 0) {
			for (RiskType riskType : riskTypeList) {
				dataOption = daoDataOption.getByFieldName("PropertyRiskType", riskType.getRiskTypeID(), object.getObjectType());
				if (dataOption == null || dataOption.equals(Coverage.RISKTYPE_BOTH)) {
					throw new ServerException((new Long(ErrorTypes.VALIDATION_PROPERTY_RISK_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PROPERTY_RISK_TYPE_INVALID.getMessage(null));
				}
				
				//fill Item Object
				switch (item.getObjectId()) {
					case Item.OBJECT_HABITAT:
						//fill Risk value from Type
						switch (riskType.getRiskTypeID()) {
							case Coverage.RISKTYPE_CONTENT:
								item.setContentValue((Double)(riskType.getRiskTypeValue()!=null?riskType.getRiskTypeValue().doubleValue():0.0));
								item.setPropertyRiskType(Coverage.RISKTYPE_CONTENT);
								break;
							case Coverage.RISKTYPE_STRUCTURE:
								item.setStructureValue((Double)(riskType.getRiskTypeValue()!=null?riskType.getRiskTypeValue().doubleValue():0.0));
								item.setPropertyRiskType(Coverage.RISKTYPE_STRUCTURE);
								break;
						}
						if ((item.getContentValue() != null && !(new Double(0)).equals(item.getContentValue())) &&
								(item.getStructureValue() != null && !(new Double(0)).equals(item.getStructureValue()))) {
							item.setPropertyRiskType(Coverage.RISKTYPE_BOTH);
						}
						break;
						
					case Item.OBJECT_CAPITAL:
						//fill Risk value from List
						ItemRiskType propertyRisk = item.addRiskType(riskType.getRiskTypeID());
						propertyRisk.setRiskTypeValue((Double)(riskType.getRiskTypeValue()!=null?riskType.getRiskTypeValue():0.0));
						break;
				}
			}
		}
	}

	private void dataFillCoverage(List<CoverageType> coverageTypeList, Item item) throws ServerException {
		Endorsement endorsement = item.getEndorsement();
		Contract contract = endorsement.getContract();
		ItemCoverage itemCoverage = null;
		
		//fill Coverage
		item.getItemCoverages().clear();
		if (coverageTypeList != null && coverageTypeList.size() > 0) {
			for (CoverageType coverageType : coverageTypeList) {
				CoveragePlan coveragePlan = daoCoveragePlan.getCoveragePlanByRefDate(contract.getProductId(), item.getCoveragePlanId(), coverageType.getCoverageID(), endorsement.getReferenceDate());
				if (coveragePlan != null) {
					Coverage coverage = coveragePlan.getCoverage();
					
					itemCoverage = this.findCoverage(item.getItemCoverages(), coverageType.getCoverageID());
					if (itemCoverage == null) {
						itemCoverage = item.addCoverage(coverageType.getCoverageID());
					} else continue;
					
					itemCoverage.setCoverageCode(coverage.getCoverageCode());
					itemCoverage.setGoodsCode(coverage.getGoodsCode());
					itemCoverage.setCoverageName(coveragePlan.getNickName());
					itemCoverage.setBranchId(coveragePlan.getSubBranchId());
					itemCoverage.setBasicCoverage(coverage.isBasic());
					itemCoverage.setInsuredValue(0.0);
					if (coverage.getRiskType() != null)
						itemCoverage.setGroupCoverageId(coverage.getRiskType());
					itemCoverage.setRangeValueId(0);
					if (coverageType.getRangeValueID() != 0)
						itemCoverage.setRangeValueId(coverageType.getRangeValueID());
					if (coveragePlan.getInsuredValueType() == CoveragePlan.INSURED_VALUE_TYPE_CSA) {
						if ((new Double(0.0)).equals(coverageType.getInsuredValue())) {
				 			String additionalName = "(" + coverage.getCoverageCode() + ":" + coverage.getGoodsCode() + ")";
							throw new ServerException((new Long(ErrorTypes.VALIDATION_COVERAGE_VALUE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_COVERAGE_VALUE_INVALID.getMessage(new String[] {coveragePlan.getNickName(), additionalName}));
						}
						itemCoverage.setInsuredValue((Double)(coverageType.getInsuredValue()!=null?coverageType.getInsuredValue():0.0));
					}
					itemCoverage.setDisplayOrder(coveragePlan.getDisplayOrder());
				}
				else {
					throw new ServerException((new Long(ErrorTypes.VALIDATION_COVERAGE_ID_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_COVERAGE_ID_INVALID.getMessage(null));
				}
			}
			
			// verify Compulsory
			List<CoveragePlan> listCoveragePlan = daoCoveragePlan.getCoveragePlanByRefDate(contract.getProductId(), item.getCoveragePlanId(), endorsement.getReferenceDate());
			for (CoveragePlan coveragePlan : listCoveragePlan) {
				Coverage coverage = coveragePlan.getCoverage();
				if (coverage.isCompulsory() || coveragePlan.isCompulsory()) {
					if (this.findCoverage(item.getItemCoverages(), coverage.getCoverageId()) == null) {
						String additionalName = "(" + coverage.getCoverageCode() + ":" + coverage.getGoodsCode() + ")";
						throw new ServerException((new Long(ErrorTypes.VALIDATION_COVERAGE_COMPULSORY_NOT_FOUND.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_COVERAGE_COMPULSORY_NOT_FOUND.getMessage(new String[] { coveragePlan.getNickName(), additionalName }));
					}
				}
			}
		}
		else {
			List<CoveragePlan> listCoveragePlan = daoCoveragePlan.getCoveragePlanByRefDate(contract.getProductId(), item.getCoveragePlanId(), endorsement.getReferenceDate());
			for (CoveragePlan coveragePlan : listCoveragePlan) {
				Coverage coverage = coveragePlan.getCoverage();
				
				if (coverage.isCompulsory() || coveragePlan.isCompulsory()) {
					itemCoverage = this.findCoverage(item.getItemCoverages(), coverage.getCoverageId());
					if (itemCoverage == null) {
						itemCoverage = item.addCoverage(coverage.getCoverageId());
					} else continue;
					
					itemCoverage.setGoodsCode(coverage.getGoodsCode());
					itemCoverage.setCoverageCode(coverage.getCoverageCode());
					itemCoverage.setCoverageName(coveragePlan.getNickName());
					itemCoverage.setBranchId(coveragePlan.getSubBranchId());
					itemCoverage.setBasicCoverage(coverage.isBasic());
					itemCoverage.setRangeValueId(0);
					if (coverage.getRiskType() != null)
						itemCoverage.setGroupCoverageId(coverage.getRiskType());
					itemCoverage.setInsuredValue(0.0);
					itemCoverage.setDisplayOrder(coveragePlan.getDisplayOrder());
				}
			}
		}
	}

	private void dataFillCustomer(InsuredType insuredType, Customer customer) throws ServerException {
		this.dataFillCustomer(insuredType, customer, false);
	}

	private void dataFillCustomer(InsuredType insuredType, Customer customer, boolean isChange) throws ServerException {
		DataOption dataOption = null;
		
		//fill Insured
		if (!isChange) {
			dataOption = daoDataOption.getByFieldName("PersonType", insuredType.getPersonTypeID());
			if (dataOption == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PERSON_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PERSON_TYPE_INVALID.getMessage(null));
			}
			customer.setPersonType(insuredType.getPersonTypeID());
			
			if (insuredType.getDocumentTypeID() != 0) {
				dataOption = daoDataOption.getByFieldName("DocumentType", insuredType.getDocumentTypeID());
				if (dataOption == null) {
					throw new ServerException((new Long(ErrorTypes.VALIDATION_DOCUMENT_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_DOCUMENT_TYPE_INVALID.getMessage(null));
				}
				customer.setDocumentType(insuredType.getDocumentTypeID());
			}
			
			if (insuredType.getDocumentNumber() != null && !(new BigDecimal(0)).equals(insuredType.getDocumentNumber()))
				customer.setDocumentNumber(insuredType.getDocumentNumber().toString());
		}
		
		if (customer.getPersonType().equals(Customer.PERSON_TYPE_NATURAL)) {
			if (insuredType.getFirstName() != null && !"".equals(insuredType.getFirstName()))
				customer.setFirstName(insuredType.getFirstName());
			
			if (insuredType.getLastName() != null && !"".equals(insuredType.getLastName()))
				customer.setLastName(insuredType.getLastName());
			
			if (insuredType.getBirthDate() != null)
				customer.setBirthDate(DateUtilities.xmlGregorianCalendarToDate(insuredType.getBirthDate()));
			
			if (insuredType.getGenderID() != null) {
				dataOption = daoDataOption.getByFieldName("Gender", insuredType.getGenderID());
				if (dataOption == null) {
					throw new ServerException((new Long(ErrorTypes.VALIDATION_GENDER_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_GENDER_INVALID.getMessage(null));
				}
				customer.setGender(insuredType.getGenderID());
			}
			
			if (insuredType.getMaritalStatusID() != null) {
				dataOption = daoDataOption.getByFieldName("MaritalStatus", insuredType.getMaritalStatusID());
				if (dataOption == null) {
					throw new ServerException((new Long(ErrorTypes.VALIDATION_MARITAL_STATUS_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_MARITAL_STATUS_INVALID.getMessage(null));
				}
				customer.setMaritalStatus(insuredType.getMaritalStatusID());
			}
		}
		else {
			if (insuredType.getCorporateName() != null && !"".equals(insuredType.getCorporateName()))
				customer.setName(insuredType.getCorporateName());
			
			if (insuredType.getTradeName() != null && !"".equals(insuredType.getTradeName()))
				customer.setTradeName(insuredType.getTradeName());
			
			customer.setBirthDate(null);
			customer.setGender(null);
			customer.setMaritalStatus(null);
		}
		
		if (insuredType.getProfessionTypeID() != null) {
			dataOption = daoDataOption.getByFieldName("ProfessionType", insuredType.getProfessionTypeID());
			if (dataOption == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PROFESSION_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PROFESSION_TYPE_INVALID.getMessage(null));
			}
			customer.setProfessionType(insuredType.getProfessionTypeID());
		}
		
		if (insuredType.getProfessionID() != null) {
			Profession profession = daoInsurer.getProfessionById(insuredType.getProfessionID());
			if (profession == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_PROFESSION_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PROFESSION_INVALID.getMessage(null));
			}
			customer.setProfessionId(insuredType.getProfessionID());
		}
		
		if (insuredType.getOccupationID() != null) {
			Occupation occupation = daoInsurer.getOccupationById(insuredType.getOccupationID());
			if (occupation == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_OCCUPATION_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_OCCUPATION_INVALID.getMessage(null));
			}
			customer.setOccupationId(insuredType.getOccupationID());
		}
		
		if (insuredType.getActivityID() != null) {
			Activity activity = daoInsurer.getActivityById(insuredType.getActivityID());
			if (activity == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_ACTIVITY_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_ACTIVITY_INVALID.getMessage(null));
			}
			customer.setActivityId(insuredType.getActivityID());
		}
		
		if (insuredType.getInflowID() != null) {
			Inflow inflow = daoInsurer.getInflowById(insuredType.getInflowID());
			if (inflow == null || !(customer.getPersonType().equals(Customer.PERSON_TYPE_NATURAL)?"F":"J").equalsIgnoreCase(inflow.getInflowType())) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_INFLOW_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_INFLOW_INVALID.getMessage(null));
			}
			customer.setInflowId(insuredType.getInflowID());
		}
		
		if (insuredType.getPhoneContact() != null && !"".equals(insuredType.getPhoneContact()))
			customer.setPhoneNumber(insuredType.getPhoneContact());
		
		if (insuredType.getEmail() != null && !"".equals(insuredType.getEmail()))
			customer.setEmailAddress(insuredType.getEmail());
		
		if (customer.getCustomerId() == 0)
			customer.setRegisterDate(new Date());
		
		customer.setActive(true);
	}
	
	private void dataFillAddress(AddressType addressType, Address address) throws ServerException {
		DataOption dataOption = null;
		
		//fill Address
		dataOption = daoDataOption.getByFieldName("AddressType", addressType.getAddressTypeID());
		if (dataOption == null) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_ADDRESS_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_ADDRESS_TYPE_INVALID.getMessage(null));
		}
		address.setAddressType(addressType.getAddressTypeID());
		
		Country country = daoCountry.findById(addressType.getCountryID());
		if (country == null) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_COUNTRY_ID_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_COUNTRY_ID_INVALID.getMessage(null));
		}
		address.setCountryId(addressType.getCountryID());
		address.setCountryName(country.getName());
		
		State state = daoState.findById(addressType.getStateID());
		if (state == null) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_STATE_ID_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_STATE_ID_INVALID.getMessage(null));
		}
		address.setStateId(addressType.getStateID());
		address.setStateName(state.getName());
		
		City city = daoCity.findById(addressType.getCityID());
		if (city == null) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_CITY_ID_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_CITY_ID_TYPE_INVALID.getMessage(null));
		}
		address.setCityId(addressType.getCityID());
		address.setCityName(city.getName());
		
		if (addressType.getDistrictName() != null && !"".equals(addressType.getDistrictName()))
			address.setDistrictName(addressType.getDistrictName());
		
		if (addressType.getStreetName() != null && !"".equals(addressType.getStreetName()))
			address.setAddressStreet(addressType.getStreetName());
		
		if (addressType.getZipCode() != null && !"".equals(addressType.getZipCode()))
			address.setZipCode(addressType.getZipCode());
	}
	
	private void dataFillPhone(PhoneType phoneType, Phone phone) throws ServerException {
		DataOption dataOption = null;
		
		//fill Phone
		dataOption = daoDataOption.getByFieldName("PhoneType", phoneType.getPhoneTypeID());
		if (dataOption == null) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_PHONE_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PHONE_TYPE_INVALID.getMessage(null));
		}
		phone.setPhoneType(phoneType.getPhoneTypeID());
		
		phone.setPhoneNumber(phoneType.getPhoneNumber());
	}
	
	private void dataFillBeneficiary(BeneficiaryType beneficiaryType, Beneficiary beneficiary) throws ServerException {
		DataOption dataOption = null;
		
		//fill Beneficiary
		beneficiary.setBeneficiaryType(Beneficiary.BENEFICIARY_TYPE_DESIGNATED);
		
		dataOption = daoDataOption.getByFieldName("PersonType", beneficiaryType.getPersonTypeID());
		if (dataOption == null) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_PERSON_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PERSON_TYPE_INVALID.getMessage(null));
		}
		beneficiary.setPersonType(beneficiaryType.getPersonTypeID());
		
		beneficiary.setUndocumented(beneficiaryType.isUndocumented());
		
		if (beneficiaryType.getDocumentTypeID() != null && !(new Integer(0)).equals(beneficiaryType.getDocumentTypeID())) {
			dataOption = daoDataOption.getByFieldName("DocumentType", beneficiaryType.getDocumentTypeID());
			if (dataOption == null) {
				throw new ServerException((new Long(ErrorTypes.VALIDATION_DOCUMENT_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_DOCUMENT_TYPE_INVALID.getMessage(null));
			}
			beneficiary.setUndocumented(false);
			beneficiary.setDocumentType(beneficiaryType.getDocumentTypeID());
		}
		
		if (beneficiaryType.getDocumentNumber() != null && !(new BigDecimal(0)).equals(beneficiaryType.getDocumentNumber()))
			beneficiary.setDocumentNumber(beneficiaryType.getDocumentNumber().toString());
		
		if (beneficiary.getPersonType() == Customer.PERSON_TYPE_NATURAL) {
			if (beneficiaryType.getFirstName() != null && !"".equals(beneficiaryType.getFirstName()))
				beneficiary.setFirstName(beneficiaryType.getFirstName());
			
			if (beneficiaryType.getLastName() != null && !"".equals(beneficiaryType.getLastName()))
				beneficiary.setLastName(beneficiaryType.getLastName());
			
			if (beneficiaryType.getBirthDate() != null)
				beneficiary.setBirthDate(DateUtilities.xmlGregorianCalendarToDate(beneficiaryType.getBirthDate()));
			
			if (beneficiaryType.getGenderID() != null) {
				dataOption = daoDataOption.getByFieldName("Gender", beneficiaryType.getGenderID());
				if (dataOption == null) {
					throw new ServerException((new Long(ErrorTypes.VALIDATION_GENDER_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_GENDER_INVALID.getMessage(null));
				}
				beneficiary.setGender(beneficiaryType.getGenderID());
			}
			if (beneficiaryType.getMaritalStatusID() != null) {
				dataOption = daoDataOption.getByFieldName("MaritalStatus", beneficiaryType.getMaritalStatusID());
				if (dataOption == null) {
					throw new ServerException((new Long(ErrorTypes.VALIDATION_MARITAL_STATUS_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_MARITAL_STATUS_INVALID.getMessage(null));
				}
				beneficiary.setMaritalStatus(beneficiaryType.getMaritalStatusID());
			}
		}
		else {
			if (beneficiaryType.getCorporateName() != null && !"".equals(beneficiaryType.getCorporateName()))
				beneficiary.setFirstName(beneficiaryType.getCorporateName());
			
			beneficiary.setBirthDate(null);
			beneficiary.setGender(null);
			beneficiary.setMaritalStatus(null);
		}
		
		dataOption = daoDataOption.getByFieldName("KinshipType", beneficiaryType.getKinshipTypeID());
		if (dataOption == null) {
			throw new ServerException((new Long(ErrorTypes.VALIDATION_PERSONAL_KINSHIP_TYPE_INVALID.getErrorCode())).intValue(), 772, ErrorTypes.VALIDATION_PERSONAL_KINSHIP_TYPE_INVALID.getMessage(null));
		}
		beneficiary.setKinshipType(beneficiaryType.getKinshipTypeID());
		
		beneficiary.setParticipationPercentage(new Double(beneficiaryType.getPercentage()));
	}

	private PersonGroupType findPersonGroupType(List<PersonGroupType> personGroupTypeList, int groupId) {
		//fill Person Group
		for (PersonGroupType personGroupType: personGroupTypeList) {
			if (personGroupType.getGroupID() == groupId) {
				return personGroupType;
			}
		}
		return null;
	}

	private BeneficiaryType findBeneficiaryType(List<BeneficiaryType> beneficiaryTypeList, int beneficiaryId) {
		//fill Beneficiary
		for (BeneficiaryType beneficiaryType: beneficiaryTypeList) {
			if (beneficiaryType.getBeneficiaryID() == beneficiaryId) {
				return beneficiaryType;
			}
		}
		return null;
	}

	/**
	 * Used to do the comparator y order item
	 */
	private Comparator<Item> comparatorItem = new Comparator<Item>() {
		/** {@inheritDoc} */
		@Override
		public int compare(Item arg0, Item arg1) {
			/* Regra b�sica: retorna -1 se menor, 0 se igual, 1 se maior */
			int result = arg0.getId().getItemId() - arg1.getId().getItemId();
			if (result != 0)
				return result;
			return result;
		}
	};
	
	/**
	 * Used to do the order item
	 */
	private TreeSet<Item> orderItem(Set<Item> itens) {
		TreeSet<Item> treeSet = null;
		treeSet = new TreeSet<Item>(comparatorItem);
		treeSet.addAll(itens);
		return treeSet;
	}

	/**
	 * Used to do the comparator y order item risk group
	 */
	private Comparator<ItemPersonalRiskGroup> comparatorItemRiskGroup = new Comparator<ItemPersonalRiskGroup>() {
		/** {@inheritDoc} */
		@Override
		public int compare(ItemPersonalRiskGroup arg0, ItemPersonalRiskGroup arg1) {
			/* Regra b�sica: retorna -1 se menor, 0 se igual, 1 se maior */
			int result = arg0.getId().getRiskGroupId() - arg1.getId().getRiskGroupId();
			if (result != 0)
				return result;
			return result;
		}
	};
	
	/**
	 * Used to do the order item risk group
	 */
	private TreeSet<ItemPersonalRiskGroup> orderItemRiskGroup(Set<ItemPersonalRiskGroup> itemRiskGroups) {
		TreeSet<ItemPersonalRiskGroup> treeSet = null;
		treeSet = new TreeSet<ItemPersonalRiskGroup>(comparatorItemRiskGroup);
		treeSet.addAll(itemRiskGroups);
		return treeSet;
	}

	/**
	 * Used to do the comparator y order coverage
	 */
	private Comparator<ItemCoverage> comparatorItemCoverage = new Comparator<ItemCoverage>() {
		/** {@inheritDoc} */
		@Override
		public int compare(ItemCoverage arg0, ItemCoverage arg1) {
			/* Regra b�sica: retorna -1 se menor, 0 se igual, 1 se maior */
			int result = arg0.getDisplayOrder() - arg1.getDisplayOrder();
			if (result != 0)
				return result;
			result = arg0.getId().getCoverageId() - arg1.getId().getCoverageId();
			if (result != 0)
				return result;
			return result;
		}
	};
	
	/**
	 * Used to do the order coverage
	 */
	private TreeSet<ItemCoverage> orderItemCoverage(Set<ItemCoverage> itemCoverages) {
		TreeSet<ItemCoverage> treeSet = null;
		treeSet = new TreeSet<ItemCoverage>(comparatorItemCoverage);
		treeSet.addAll(itemCoverages);
		return treeSet;
	}

	/**
	 * Used to find the coverage
	 */
	private ItemCoverage findCoverage(Set<ItemCoverage> itemCoverages, int coverageId) {
		for(ItemCoverage itemCoverage : itemCoverages) {
			if (itemCoverage.getId().getCoverageId() == coverageId) {
				return itemCoverage;
			}
		}
		return null;
	}

	/**
	 * Used to do the comparator y order beneficiary
	 */
	private Comparator<Beneficiary> comparatorBeneficiary = new Comparator<Beneficiary>() {
		/** {@inheritDoc} */
		@Override
		public int compare(Beneficiary arg0, Beneficiary arg1) {
			/* Regra b�sica: retorna -1 se menor, 0 se igual, 1 se maior */
			int result = arg0.getId().getBeneficiaryId() - arg1.getId().getBeneficiaryId();
			if (result != 0)
				return result;
			return result;
		}
	};

	/**
	 * Used to do the order beneficiary
	 */
	private TreeSet<Beneficiary> orderBeneficiary(Set<Beneficiary> beneficiaries) {
		TreeSet<Beneficiary> treeSet = null;
		treeSet = new TreeSet<Beneficiary>(comparatorBeneficiary);
		treeSet.addAll(beneficiaries);
		return treeSet;
	}

	/**
	 * Used to do the comparator y order Installment
	 */
	private Comparator<Installment> comparatorInstallment = new Comparator<Installment>() {
		/** {@inheritDoc} */
		@Override
		public int compare(Installment arg0, Installment arg1) {
			/* Regra b�sica: retorna -1 se menor, 0 se igual, 1 se maior */
			int result = arg0.getId().getInstallmentId() - arg1.getId().getInstallmentId();
			if (result != 0)
				return result;
			return result;
		}
	};

	/**
	 * Used to do the order Installment
	 */
	private TreeSet<Installment> orderInstallment(Set<Installment> installments) {
		TreeSet<Installment> treeSet = null;
		treeSet = new TreeSet<Installment>(comparatorInstallment);
		treeSet.addAll(installments);
		return treeSet;
	}
}
