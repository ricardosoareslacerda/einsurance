package br.com.tratomais.ws.einsurance;

import java.math.BigDecimal;

import javax.xml.datatype.XMLGregorianCalendar;

import br.com.tratomais.ws.einsurance.entities.AddressType;
import br.com.tratomais.ws.einsurance.entities.HolderType;
import br.com.tratomais.ws.einsurance.entities.InsuredType;

public class InsuredTypeProxy extends InsuredType {
	private HolderType holderType;

	public InsuredTypeProxy(HolderType holderType){
		this.holderType = holderType;
	}

	@Override
	public int hashCode() {
		return holderType.hashCode();
	}

	@Override
	public int getPersonTypeID() {
		return holderType.getPersonTypeID();
	}

	@Override
	public void setPersonTypeID(int value) {
		holderType.setPersonTypeID(value);
	}

	@Override
	public int getDocumentTypeID() {
		return holderType.getDocumentTypeID();
	}

	@Override
	public void setDocumentTypeID(int value) {
		holderType.setDocumentTypeID(value);
	}

	@Override
	public BigDecimal getDocumentNumber() {
		return holderType.getDocumentNumber();
	}

	@Override
	public void setDocumentNumber(BigDecimal value) {
		holderType.setDocumentNumber(value);
	}

	@Override
	public String getFirstName() {
		return holderType.getFirstName();
	}

	@Override
	public void setFirstName(String value) {
		holderType.setFirstName(value);
	}

	@Override
	public String getLastName() {
		return holderType.getLastName();
	}

	@Override
	public void setLastName(String value) {
		holderType.setLastName(value);
	}

	@Override
	public String getCorporateName() {
		return holderType.getCorporateName();
	}

	@Override
	public void setCorporateName(String value) {
		holderType.setCorporateName(value);
	}

	@Override
	public String getTradeName() {
		return holderType.getTradeName();
	}

	@Override
	public String toString() {
		return holderType.toString();
	}

	@Override
	public void setTradeName(String value) {
		holderType.setTradeName(value);
	}

	@Override
	public XMLGregorianCalendar getBirthDate() {
		return holderType.getBirthDate();
	}
	
	@Override
	public void setBirthDate(XMLGregorianCalendar value) {
		holderType.setBirthDate(value);
	}

	@Override
	public Integer getGenderID() {
		return holderType.getGenderID();
	}

	@Override
	public void setGenderID(Integer value) {
		holderType.setGenderID(value);
	}

	@Override
	public Integer getMaritalStatusID() {
		return holderType.getMaritalStatusID();
	}

	@Override
	public void setMaritalStatusID(Integer value) {
		holderType.setMaritalStatusID(value);
	}

	@Override
	public Integer getProfessionTypeID() {
		return holderType.getProfessionTypeID();
	}

	@Override
	public void setProfessionTypeID(Integer value) {
		holderType.setProfessionTypeID(value);
	}

	@Override
	public Integer getProfessionID() {
		return holderType.getProfessionID();
	}

	@Override
	public void setProfessionID(Integer value) {
		holderType.setProfessionID(value);
	}

	@Override
	public Integer getOccupationID() {
		return holderType.getOccupationID();
	}

	@Override
	public void setOccupationID(Integer value) {
		holderType.setOccupationID(value);
	}

	@Override
	public Integer getActivityID() {
		return holderType.getActivityID();
	}

	@Override
	public void setActivityID(Integer value) {
		holderType.setActivityID(value);
	}

	@Override
	public Integer getInflowID() {
		return holderType.getInflowID();
	}

	@Override
	public void setInflowID(Integer value) {
		holderType.setInflowID(value);
	}

	@Override
	public String getPhoneContact() {
		return holderType.getPhoneContact();
	}

	@Override
	public void setPhoneContact(String value) {
		holderType.setPhoneContact(value);
	}

	@Override
	public String getEmail() {
		return holderType.getEmail();
	}

	@Override
	public void setEmail(String value) {
		holderType.setEmail(value);
	}

	@Override
	public AddressType getAddress() {
		return holderType.getAddress();
	}
	
	@Override
	public void setAddress(AddressType value) {
		holderType.setAddress(value);
	}

	@Override
	public int getInsuredID() {
		return this.holderType.getHolderID();
	}

	@Override
	public void setInsuredID(int value) {
		this.holderType.setHolderID(value);
	}
}
