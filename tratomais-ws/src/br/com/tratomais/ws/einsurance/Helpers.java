package br.com.tratomais.ws.einsurance;

import java.util.List;
import java.util.Set;

import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.service.erros.ErrorTypes;
import br.com.tratomais.core.util.Utilities;
import br.com.tratomais.ws.einsurance.entities.ChannelType;
import br.com.tratomais.ws.einsurance.entities.ListUserResponse;
import br.com.tratomais.ws.einsurance.entities.UserAuthenticationResponse;
import br.com.tratomais.ws.einsurance.entities.UserAuthenticationType;
import br.com.tratomais.ws.einsurance.entities.UserType;
import br.com.tratomais.ws.exceptions.ServerException;

abstract class UserFacilities {
	protected UserType fromUser(User user) {
		UserType userType = new UserType();
		Utilities.copyToObjectDifferent(user, userType);
		userType.setUserCode(user.getEmployeeCode());
		return userType;
	}
	
	protected UserAuthenticationType fromUserAuthentication(User user) {
		UserAuthenticationType userType = new UserAuthenticationType();
		Utilities.copyToObjectDifferent(user, userType);
		userType.setDomainID(user.getDomain().getDomainId());
		userType.setUserCode(user.getEmployeeCode());
		return userType;
	}
	
	protected ChannelType fromChannel(Channel channel) {
		ChannelType channelType = new ChannelType();
		Utilities.copyToObjectDifferent(channel, channelType);
		channelType.setChannelCode(channel.getExternalCode());
		return channelType;
	}
}

class UserAuthenticationHelper extends UserFacilities {
	public UserAuthenticationResponse transform(User user) {
		UserAuthenticationResponse response = new UserAuthenticationResponse();
		response.setUser(fromUser(user));
		return response;
	}
}

class ListUserHelper extends UserFacilities {
	public ListUserResponse transform(List<User> listUser) throws ServerException{
		ListUserResponse response = new ListUserResponse();
		if(listUser != null && listUser.size() > 0){
			for(User user : listUser){
				UserType userType = new UserType();
				Utilities.copyToObjectDifferent(user, userType);
				userType.setUserCode((String)Utilities.convertNullValue(user.getEmployeeCode(),"0"));
				response.getUser().add(userType);
			}
		} else {
			throw new ServerException(462, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		return response;
	}
}




class GetChannelHelper extends UserFacilities {
	public UserAuthenticationType.Channels transform(Set<Channel> channels) {
		UserAuthenticationType.Channels response = new UserAuthenticationType.Channels();
		for (Channel channel : channels) {
			response.getChannel().add(fromChannel(channel));
		}
		return response;
	}
}