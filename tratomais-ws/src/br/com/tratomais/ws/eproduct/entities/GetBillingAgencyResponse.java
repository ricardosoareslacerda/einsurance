//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.08.18 at 06:22:25 PM BRT 
//


package br.com.tratomais.ws.eproduct.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="billingAgency" type="{http://www.tratomais.com.br/services/eProductTypes}BillingAgencyType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "billingAgency"
})
@XmlRootElement(name = "getBillingAgencyResponse")
public class GetBillingAgencyResponse {

    @XmlElement(required = true)
    protected BillingAgencyType billingAgency;

    /**
     * Gets the value of the billingAgency property.
     * 
     * @return
     *     possible object is
     *     {@link BillingAgencyType }
     *     
     */
    public BillingAgencyType getBillingAgency() {
        return billingAgency;
    }

    /**
     * Sets the value of the billingAgency property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingAgencyType }
     *     
     */
    public void setBillingAgency(BillingAgencyType value) {
        this.billingAgency = value;
    }

}
