package br.com.tratomais.ws.eproduct;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.model.product.BillingAgency;
import br.com.tratomais.core.model.product.BillingMethod;
import br.com.tratomais.core.model.product.Branch;
import br.com.tratomais.core.model.product.ConversionRate;
import br.com.tratomais.core.model.product.CoveragePlan;
import br.com.tratomais.core.model.product.CoverageRangeValue;
import br.com.tratomais.core.model.product.Currency;
import br.com.tratomais.core.model.product.InsuredObject;
import br.com.tratomais.core.model.product.Product;
import br.com.tratomais.core.model.product.ProductPaymentTerm;
import br.com.tratomais.core.model.product.ProductPlan;
import br.com.tratomais.core.model.product.ProductTerm;
import br.com.tratomais.core.model.product.ProductVersion;
import br.com.tratomais.core.service.IServiceEinsurance;
import br.com.tratomais.core.service.erros.ErrorTypes;
import br.com.tratomais.core.util.Utilities;
import br.com.tratomais.general.utilities.DateUtilities;
import br.com.tratomais.general.utilities.NumberUtilities;
import br.com.tratomais.ws.eproduct.entities.BillingAgencyType;
import br.com.tratomais.ws.eproduct.entities.BillingMethodType;
import br.com.tratomais.ws.eproduct.entities.BranchType;
import br.com.tratomais.ws.eproduct.entities.CoverageType;
import br.com.tratomais.ws.eproduct.entities.CurrencyType;
import br.com.tratomais.ws.eproduct.entities.GetBillingAgencyRequest;
import br.com.tratomais.ws.eproduct.entities.GetBillingAgencyResponse;
import br.com.tratomais.ws.eproduct.entities.GetBillingMethodRequest;
import br.com.tratomais.ws.eproduct.entities.GetBillingMethodResponse;
import br.com.tratomais.ws.eproduct.entities.GetBranchRequest;
import br.com.tratomais.ws.eproduct.entities.GetBranchResponse;
import br.com.tratomais.ws.eproduct.entities.GetConditionedRequest;
import br.com.tratomais.ws.eproduct.entities.GetConditionedResponse;
import br.com.tratomais.ws.eproduct.entities.GetCoverageRequest;
import br.com.tratomais.ws.eproduct.entities.GetCoverageResponse;
import br.com.tratomais.ws.eproduct.entities.GetCurrencyRateRequest;
import br.com.tratomais.ws.eproduct.entities.GetCurrencyRateResponse;
import br.com.tratomais.ws.eproduct.entities.GetCurrencyRequest;
import br.com.tratomais.ws.eproduct.entities.GetCurrencyResponse;
import br.com.tratomais.ws.eproduct.entities.GetObjectRequest;
import br.com.tratomais.ws.eproduct.entities.GetObjectResponse;
import br.com.tratomais.ws.eproduct.entities.GetPlanRequest;
import br.com.tratomais.ws.eproduct.entities.GetPlanResponse;
import br.com.tratomais.ws.eproduct.entities.GetProductRequest;
import br.com.tratomais.ws.eproduct.entities.GetProductResponse;
import br.com.tratomais.ws.eproduct.entities.GetRangeValueRequest;
import br.com.tratomais.ws.eproduct.entities.GetRangeValueResponse;
import br.com.tratomais.ws.eproduct.entities.GetRiskPlanRequest;
import br.com.tratomais.ws.eproduct.entities.GetRiskPlanResponse;
import br.com.tratomais.ws.eproduct.entities.GetTermRequest;
import br.com.tratomais.ws.eproduct.entities.GetTermResponse;
import br.com.tratomais.ws.eproduct.entities.GetVersionRequest;
import br.com.tratomais.ws.eproduct.entities.GetVersionResponse;
import br.com.tratomais.ws.eproduct.entities.ListBillingMethodRequest;
import br.com.tratomais.ws.eproduct.entities.ListBillingMethodResponse;
import br.com.tratomais.ws.eproduct.entities.ListBranchRequest;
import br.com.tratomais.ws.eproduct.entities.ListBranchResponse;
import br.com.tratomais.ws.eproduct.entities.ListCoverageRequest;
import br.com.tratomais.ws.eproduct.entities.ListCoverageResponse;
import br.com.tratomais.ws.eproduct.entities.ListCurrencyRequest;
import br.com.tratomais.ws.eproduct.entities.ListCurrencyResponse;
import br.com.tratomais.ws.eproduct.entities.ListObjectRequest;
import br.com.tratomais.ws.eproduct.entities.ListObjectResponse;
import br.com.tratomais.ws.eproduct.entities.ListPlanRequest;
import br.com.tratomais.ws.eproduct.entities.ListPlanResponse;
import br.com.tratomais.ws.eproduct.entities.ListProductRequest;
import br.com.tratomais.ws.eproduct.entities.ListProductResponse;
import br.com.tratomais.ws.eproduct.entities.ListRangeValueRequest;
import br.com.tratomais.ws.eproduct.entities.ListRangeValueResponse;
import br.com.tratomais.ws.eproduct.entities.ListRiskPlanRequest;
import br.com.tratomais.ws.eproduct.entities.ListRiskPlanResponse;
import br.com.tratomais.ws.eproduct.entities.ListTermRequest;
import br.com.tratomais.ws.eproduct.entities.ListTermResponse;
import br.com.tratomais.ws.eproduct.entities.ListVersionRequest;
import br.com.tratomais.ws.eproduct.entities.ListVersionResponse;
import br.com.tratomais.ws.eproduct.entities.ObjectType;
import br.com.tratomais.ws.eproduct.entities.PlanType;
import br.com.tratomais.ws.eproduct.entities.ProductType;
import br.com.tratomais.ws.eproduct.entities.RangeValueType;
import br.com.tratomais.ws.eproduct.entities.RiskplanType;
import br.com.tratomais.ws.eproduct.entities.TermType;
import br.com.tratomais.ws.eproduct.entities.VersionType;
import br.com.tratomais.ws.exceptions.ServerException;

@Endpoint
public class EProductEndpoint {
	private IServiceEinsurance serviceEinsurance;
	
	public void setServiceEinsurance(IServiceEinsurance serviceEinsurance){
		this.serviceEinsurance = serviceEinsurance;
	}

	private static final String EPRODUCT_NAMESPACE="http://www.tratomais.com.br/services/eProduct";

	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="listObjectRequest")
	public ListObjectResponse listObject(ListObjectRequest request) throws ServiceException, ServerException{
		ListObjectResponse response = new ListObjectResponse();
		List<InsuredObject> listObject = serviceEinsurance.listObject(DateUtilities.xmlGregorianCalendarToDate(request.getRefDate()));
		
		if(listObject != null){
			for(InsuredObject object : listObject){
				ObjectType objectType = new ObjectType();
				Utilities.copyToObjectDifferent(object, objectType);
				objectType.setNickName(object.getNickname());
				response.getObject().add(objectType);
			}
		} else {
			throw new ServerException(312, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}
	
	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="getObjectRequest")
	public GetObjectResponse getObject(GetObjectRequest request) throws ServiceException, ServerException{
		GetObjectResponse response = new GetObjectResponse();
		InsuredObject object = serviceEinsurance.getObject(request.getObjectID(), DateUtilities.xmlGregorianCalendarToDate(request.getRefDate()));
		if(object != null){
			ObjectType objectType = new ObjectType();
			Utilities.copyToObjectDifferent(object, objectType);
			objectType.setNickName(object.getNickname());
			response.setObject(objectType);
		} else {
			throw new ServerException(322, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		return response;		
	}

	
	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="listProductRequest")
	public ListProductResponse listProduct(ListProductRequest request) throws ServiceException, ServerException{
		ListProductResponse response = new ListProductResponse();
		List<Product> listProduct = serviceEinsurance.listProduct(request.getObjectID(), DateUtilities.xmlGregorianCalendarToDate(request.getRefDate()));
		if(listProduct != null) {
			for(Product product : listProduct){
				ProductType productType = new ProductType();
				Utilities.copyToObjectDifferent(product, productType);
				response.getProduct().add(productType);
			}
		} else {
			throw new ServerException(332, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		return response;		
	} 

	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="getProductRequest")
	public GetProductResponse getProduct(GetProductRequest request) throws ServiceException, ServerException, SecurityException, IllegalArgumentException, NoSuchMethodException, ClassNotFoundException, IllegalAccessException, InvocationTargetException{
		GetProductResponse response = new GetProductResponse();
		ProductVersion productVersion = serviceEinsurance.getProductVersionByRefDate(request.getProductID(), DateUtilities.xmlGregorianCalendarToDate(request.getVersionDate()));
		ProductType productType = new ProductType();
		
		if(productVersion != null){
			Utilities.copyToObjectDifferent(productVersion.getProduct(), productType);
			response.setProduct(productType);
		} else {
			throw new ServerException(342, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		return response;		
	} 

	
	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="listVersionRequest")
	public ListVersionResponse listVersion(ListVersionRequest request) throws ServiceException, ServerException, DatatypeConfigurationException{
		ListVersionResponse response = new ListVersionResponse();
		List<ProductVersion> listProduct = serviceEinsurance.listProductVersion(request.getProductID(), DateUtilities.xmlGregorianCalendarToDate(request.getRefDate()));
		if(listProduct != null){
			for(ProductVersion productVersion : listProduct){
				VersionType versionType = new VersionType();
				Utilities.copyToObjectDifferent(productVersion, versionType);
				versionType.setProductID(productVersion.getProduct().getProductId());
				versionType.setEffectiveDate(DateUtilities.dateToXMLGregorianCalendar(productVersion.getId().getEffectiveDate()));
				versionType.setVersionDate(DateUtilities.dateToXMLGregorianCalendar(productVersion.getReferenceDate()));
				response.getVersion().add(versionType);
			}
		} else {
			throw new ServerException(352, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		return response;
	} 
	
	@SuppressWarnings("unused")
	private DatatypeFactory datatypeFactory;
	
	public EProductEndpoint() {
		try {
			datatypeFactory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			throw new RuntimeException(e);
		}
	}
	
	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="getVersionRequest")
	public GetVersionResponse getVersion(GetVersionRequest request) throws ServerException, DatatypeConfigurationException{
		GetVersionResponse response = new GetVersionResponse();
		ProductVersion productVersion = serviceEinsurance.getProductVersionByRefDate(request.getProductID(), DateUtilities.xmlGregorianCalendarToDate(request.getVersionDate()));
		if(productVersion != null){
			VersionType versionType = new VersionType();
			Utilities.copyToObjectDifferent(productVersion, versionType);
			versionType.setProductID(productVersion.getProduct().getProductId());
			versionType.setEffectiveDate(DateUtilities.dateToXMLGregorianCalendar(productVersion.getId().getEffectiveDate()));
			versionType.setVersionDate(DateUtilities.dateToXMLGregorianCalendar(productVersion.getReferenceDate()));
			response.setVersion(versionType);
		} else {
			throw new ServerException(362, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;		
	} 

	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="listBranchRequest")
	public ListBranchResponse listBranch(ListBranchRequest request) throws ServiceException, ServerException{
		ListBranchResponse response = new ListBranchResponse();
		List<Branch> listBranch = serviceEinsurance.listBranch(DateUtilities.xmlGregorianCalendarToDate(request.getRefDate()));
		
		if(listBranch != null){
			for(Branch branch : listBranch){
				BranchType branchType = new BranchType();
				Utilities.copyToObjectDifferent(branch, branchType);
				if (branch.getParentID() != null) branchType.setParentID(branch.getParentID());
				response.getBranch().add(branchType);
			}
		} else {
			throw new ServerException(372, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}

		return response ;		
	} 

	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="getBranchRequest")
	public GetBranchResponse getBranch(GetBranchRequest request) throws ServiceException, ServerException{
		GetBranchResponse response = new GetBranchResponse();
		Branch branch = serviceEinsurance.getBranch(request.getBranchID(), DateUtilities.xmlGregorianCalendarToDate(request.getRefDate()));
		if(branch != null){
			BranchType branchType = new BranchType();
			Utilities.copyToObjectDifferent(branch, branchType);
			if (branch.getParentID() != null) branchType.setParentID(branch.getParentID());
			response.setBranch(branchType);
		} else {
			throw new ServerException(382, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		return response;
	}

	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="listCurrencyRequest")
	public ListCurrencyResponse listCurrency(ListCurrencyRequest request) throws ServerException, ServiceException{
		ListCurrencyResponse response = new ListCurrencyResponse();
		List<Currency> listCurrency = serviceEinsurance.listCurrency(request.isIndexer(), DateUtilities.xmlGregorianCalendarToDate(request.getRefDate()));

		if(listCurrency != null){
			for(Currency currency : listCurrency){
				CurrencyType currencyType = new CurrencyType();
				Utilities.copyToObjectDifferent(currency, currencyType);
				currencyType.setIsoCode(currency.getIsocode());
				response.getCurrency().add(currencyType);
			}
		} else {
			throw new ServerException(392, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}

		return response ;
	}
	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="getCurrencyRequest")
	public GetCurrencyResponse getCurrency(GetCurrencyRequest request) throws ServiceException, ServerException{
		GetCurrencyResponse response = new GetCurrencyResponse();
		Currency currency = serviceEinsurance.getCurrency(request.getCurrencyID(), DateUtilities.xmlGregorianCalendarToDate(request.getRefDate()));
		
		if(currency != null){
			CurrencyType currencyType = new CurrencyType();
			Utilities.copyToObjectDifferent(currency, currencyType);
			currencyType.setIsoCode(currency.getIsocode());
			response.setCurrency(currencyType);
		} else {
			throw new ServerException(3102, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;		
	}
	
	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="getCurrencyRateRequest")
	public GetCurrencyRateResponse getCurrencyRate(GetCurrencyRateRequest request) throws ServiceException, ServerException{
		GetCurrencyRateResponse response = new GetCurrencyRateResponse();
		ConversionRate conversionRate = serviceEinsurance.findConversionRateByRefDate(request.getCurrencyID(), DateUtilities.xmlGregorianCalendarToDate(request.getRefDate()));
		if(conversionRate != null){
			Utilities.copyToObjectDifferent(conversionRate, response);
			response.getCurrency().setCurrencyID(conversionRate.getCurrencyId());
			response.getCurrency().setCurrencyRate(new BigDecimal(conversionRate.getMultipleRateBy().toString()));
		} else {
			throw new ServerException(3112, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		return response ;		
	} 

	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="listRiskPlanRequest")
	public ListRiskPlanResponse listRiskPlan(ListRiskPlanRequest request) throws ServiceException, ServerException{
		ListRiskPlanResponse response = new ListRiskPlanResponse();
		List<ProductPlan> listPlan = serviceEinsurance.findRiskPlanByProductId(request.getProductID(), DateUtilities.xmlGregorianCalendarToDate(request.getVersionDate()));
		if(listPlan != null && listPlan.size() > 0) {
			for(ProductPlan productPlan: listPlan){
				RiskplanType riskPlan = new RiskplanType();
				Utilities.copyToObjectDifferent(productPlan.getId(), riskPlan);
				Utilities.copyToObjectDifferent(productPlan.getPlan(), riskPlan);
				riskPlan.setRiskPlanID(productPlan.getId().getPlanId());
				riskPlan.setNickName(productPlan.getNickName());
				riskPlan.setDisplayOrder((Short)Utilities.convertNullValue(productPlan.getDisplayOrder(),0));
				response.getRiskplan().add(riskPlan);
			}
		} else {
			throw new ServerException(3122, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}

		return response;		
	} 

	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="getRiskPlanRequest")
	public GetRiskPlanResponse getRiskPlan(GetRiskPlanRequest request) throws ServerException{
		GetRiskPlanResponse response = new GetRiskPlanResponse();
		ProductPlan productPlan = serviceEinsurance.getRiskPlanById(request.getProductID(), request.getRiskPlantID(), DateUtilities.xmlGregorianCalendarToDate(request.getVersionDate()));
		if(productPlan != null){
			RiskplanType riskPlan = new RiskplanType();
			Utilities.copyToObjectDifferent(productPlan.getId(), riskPlan);
			Utilities.copyToObjectDifferent(productPlan.getPlan(), riskPlan);
			riskPlan.setRiskPlanID(productPlan.getId().getPlanId());
			riskPlan.setNickName(productPlan.getNickName());
			riskPlan.setDisplayOrder((Short)Utilities.convertNullValue(productPlan.getDisplayOrder(),0));
			response.setRiskPlan(riskPlan);
		} else {
			throw new ServerException(3132, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	} 

	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="listPlanRequest")
	public ListPlanResponse listPlan(ListPlanRequest request) throws ServiceException, ServerException{
		ListPlanResponse response = new ListPlanResponse();
		List<ProductPlan> listProductPlan = serviceEinsurance.listCoveragePlanByProductId(request.getProductID(), request.getRiskPlanID(), DateUtilities.xmlGregorianCalendarToDate(request.getVersionDate()));
		if(listProductPlan != null && listProductPlan.size() > 0){
			for(ProductPlan productPlan : listProductPlan){
				PlanType planType = new PlanType();
				Utilities.copyToObjectDifferent(productPlan.getId(), planType);
				Utilities.copyToObjectDifferent(productPlan.getPlan(), planType);
				planType.setRiskPlanID(request.getRiskPlanID());
				planType.setNickName(productPlan.getNickName());
				planType.setFixedValue(NumberUtilities.doubleToBigDecimal(productPlan.getFixedInsuredValue()));
				planType.setDisplayOrder((Short)Utilities.convertNullValue(productPlan.getDisplayOrder(),0));
				response.getPlan().add(planType);
			}
		} else {
			throw new ServerException(3142, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}

		return response;
	} 
	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="getPlanRequest")
	public GetPlanResponse getPlan(GetPlanRequest request) throws ServiceException, ServerException{
		GetPlanResponse response = new GetPlanResponse();
		ProductPlan productPlan = serviceEinsurance.getPlanById(request.getProductID(), request.getPlanID(), DateUtilities.xmlGregorianCalendarToDate(request.getVersionDate()));
		if(productPlan != null) {
			PlanType planType = new PlanType();
			Utilities.copyToObjectDifferent(productPlan.getId(), planType);
			planType.setRiskPlanID(request.getRiskPlanID());
			planType.setNickName(productPlan.getNickName());
			planType.setFixedValue(NumberUtilities.doubleToBigDecimal(productPlan.getFixedInsuredValue()));
			planType.setDisplayOrder((Short)Utilities.convertNullValue(productPlan.getDisplayOrder(),0));
			response.setPlan(planType);
		} else {
			throw new ServerException(3152, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		return response;
	} 

	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="listCoverageRequest")
	public ListCoverageResponse listCoverage(ListCoverageRequest request) throws ServiceException, ServerException{
		ListCoverageResponse response = new ListCoverageResponse();
		List<CoveragePlan> listCoveragePlan = serviceEinsurance.listCoveragePlanByRefDate(request.getProductID(), request.getPlanID(), DateUtilities.xmlGregorianCalendarToDate(request.getVersionDate()));
		if(listCoveragePlan != null && listCoveragePlan.size() > 0){
			for(CoveragePlan coveragePlan : listCoveragePlan){
				CoverageType coverageType = new CoverageType();

				coverageType.setProductID((Integer)Utilities.convertNullValue(coveragePlan.getId().getProductId(),0));
				coverageType.setPlanID((Integer)Utilities.convertNullValue(coveragePlan.getId().getPlanId(),0));
				coverageType.setCoverageID((Integer)Utilities.convertNullValue(coveragePlan.getId().getCoverageId(),0));
				coverageType.setCoverageCode((String)Utilities.convertNullValue(coveragePlan.getCoverage().getCoverageCode(),""));
				coverageType.setGoodsCode((String)Utilities.convertNullValue(coveragePlan.getCoverage().getGoodsCode(),""));
				coverageType.setName((String)Utilities.convertNullValue(coveragePlan.getCoverage().getName(),""));
				coverageType.setNickName((String)Utilities.convertNullValue(coveragePlan.getNickName(),""));
				coverageType.setBranchID((Integer)Utilities.convertNullValue(coveragePlan.getSubBranchId(),0));
				coverageType.setBasic((Boolean)Utilities.convertNullValue(coveragePlan.getCoverage().isBasic(),false));
				coverageType.setCompulsory((Boolean)Utilities.convertNullValue(coveragePlan.isCompulsory(),false));
				coverageType.setRiskTypeID((Integer)Utilities.convertNullValue(coveragePlan.getCoverage().getRiskType(),0));
				coverageType.setValueTypeID((Integer)Utilities.convertNullValue(coveragePlan.getInsuredValueType(),0));
				coverageType.setFactorRiskValue(NumberUtilities.doubleToBigDecimal(coveragePlan.getFactorRiskValue()));
				coverageType.setFixedValue(NumberUtilities.doubleToBigDecimal(coveragePlan.getFixedInsuredValue()));
				coverageType.setChangeValue((Boolean)Utilities.convertNullValue(coveragePlan.isChangeInsuredValue(),false));
				coverageType.setDisplayOrder((Short)Utilities.convertNullValue(coveragePlan.getDisplayOrder(),0));

				response.getCoverage().add(coverageType);
			}
		} else {
			throw new ServerException(3162, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}

		return response;
	} 

	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="getCoverageRequest")
	public GetCoverageResponse getCoverage(GetCoverageRequest request) throws ServiceException, ServerException{
		GetCoverageResponse response = new GetCoverageResponse();
		CoveragePlan coveragePlan = serviceEinsurance.getCoveragePlanByRefDate(request.getProductID(), request.getPlanID(), request.getCoverageID(), DateUtilities.xmlGregorianCalendarToDate(request.getVersionDate()));
		
		if(coveragePlan != null){
			CoverageType coverageType = new CoverageType();

			coverageType.setProductID((Integer)Utilities.convertNullValue(coveragePlan.getId().getProductId(),0));
			coverageType.setPlanID((Integer)Utilities.convertNullValue(coveragePlan.getId().getPlanId(),0));
			coverageType.setCoverageID((Integer)Utilities.convertNullValue(coveragePlan.getId().getCoverageId(),0));
			coverageType.setCoverageCode((String)Utilities.convertNullValue(coveragePlan.getCoverage().getCoverageCode(),""));
			coverageType.setGoodsCode((String)Utilities.convertNullValue(coveragePlan.getCoverage().getGoodsCode(),""));
			coverageType.setName((String)Utilities.convertNullValue(coveragePlan.getCoverage().getName(),""));
			coverageType.setNickName((String)Utilities.convertNullValue(coveragePlan.getNickName(),""));
			coverageType.setBranchID((Integer)Utilities.convertNullValue(coveragePlan.getSubBranchId(),0));
			coverageType.setBasic((Boolean)Utilities.convertNullValue(coveragePlan.getCoverage().isBasic(),false));
			coverageType.setCompulsory((Boolean)Utilities.convertNullValue(coveragePlan.isCompulsory(),false));
			coverageType.setRiskTypeID((Integer)Utilities.convertNullValue(coveragePlan.getCoverage().getRiskType(),0));
			coverageType.setValueTypeID((Integer)Utilities.convertNullValue(coveragePlan.getInsuredValueType(),0));
			coverageType.setFactorRiskValue(NumberUtilities.doubleToBigDecimal(coveragePlan.getFactorRiskValue()));
			coverageType.setFixedValue(NumberUtilities.doubleToBigDecimal(coveragePlan.getFixedInsuredValue()));
			coverageType.setChangeValue((Boolean)Utilities.convertNullValue(coveragePlan.isChangeInsuredValue(),false));
			coverageType.setDisplayOrder((Short)Utilities.convertNullValue(coveragePlan.getDisplayOrder(),0));

			response.setCoverage(coverageType);
		} else {
			throw new ServerException(3172, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		return response;
	}

	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="listRangeValueRequest")
	public ListRangeValueResponse listRangeValue(ListRangeValueRequest request) throws ServerException{
		ListRangeValueResponse response = new ListRangeValueResponse();
		List<CoverageRangeValue> listCoverage = serviceEinsurance.listCoverageRangeValueByRefDate(request.getProductID(), request.getPlanID(), request.getCoverageID(), DateUtilities.xmlGregorianCalendarToDate(request.getVersionDate()));
		if(listCoverage != null && listCoverage.size() > 0){
			for(CoverageRangeValue coverage : listCoverage){
				RangeValueType rangeValueType = new RangeValueType();
				Utilities.copyToObjectDifferent(coverage, rangeValueType);
				Utilities.copyToObjectDifferent(coverage.getId(), rangeValueType);
				rangeValueType.setBaseValue(NumberUtilities.doubleToBigDecimal(coverage.getRangeValue()));
				rangeValueType.setStartValue(NumberUtilities.doubleToBigDecimal(coverage.getStartValue()));
				rangeValueType.setEndValue(NumberUtilities.doubleToBigDecimal(coverage.getEndValue()));
				rangeValueType.setRangeValueID(coverage.getId().getRangeId());
				rangeValueType.setDisplayOrder(coverage.getDisplayOrder());
				response.getRangeValue().add(rangeValueType);
			}
		} else {
			throw new ServerException(3182, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}

		return response;
	}

	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="getRangeValueRequest")
	public GetRangeValueResponse getRangeValue(GetRangeValueRequest request) throws ServiceException, ServerException{
		GetRangeValueResponse response = new GetRangeValueResponse();
		CoverageRangeValue coverage = serviceEinsurance.getCoverageRangeValue(request.getProductID(), request.getPlanID(), request.getCoverageID(), request.getRangeValueID(), DateUtilities.xmlGregorianCalendarToDate(request.getVersionDate()));
		if(coverage != null){
			RangeValueType rangeValueType = new RangeValueType();
			Utilities.copyToObjectDifferent(coverage, rangeValueType);
			Utilities.copyToObjectDifferent(coverage.getId(), rangeValueType);
			rangeValueType.setBaseValue(NumberUtilities.doubleToBigDecimal(coverage.getRangeValue()));
			rangeValueType.setStartValue(NumberUtilities.doubleToBigDecimal(coverage.getStartValue()));
			rangeValueType.setEndValue(NumberUtilities.doubleToBigDecimal(coverage.getEndValue()));
			rangeValueType.setRangeValueID(coverage.getId().getRangeId());
			rangeValueType.setDisplayOrder(coverage.getDisplayOrder());
			response.setRangeValue(rangeValueType);
		} else {
			throw new ServerException(3192, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}
	
	// -----
	
	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="getConditionedRequest")
	public GetConditionedResponse getConditioned(GetConditionedRequest request){
		GetConditionedResponse response = new GetConditionedResponse();
		
		response.setCoverageID(12);
		response.setProductID(2);
		response.setResume("Resumo???");
		return response;
	}
	
	// ------
	
	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="listTermRequest")
	public ListTermResponse listTerm(ListTermRequest request) throws ServiceException, ServerException{
		ListTermResponse response = new ListTermResponse();
		List<ProductTerm> listProduct = serviceEinsurance.listProductTermByRefDate(request.getProductID(), DateUtilities.xmlGregorianCalendarToDate(request.getVersionDate()));
		if(listProduct != null && listProduct.size() > 0){
			for(ProductTerm productTerm : listProduct){
				TermType termType = new TermType();
				Utilities.copyToObjectDifferent(productTerm, termType);
				Utilities.copyToObjectDifferent(productTerm.getTerm(), termType);
				Utilities.copyToObjectDifferent(productTerm.getId(), termType);
				response.getTerm().add(termType);
			}
		} else {
			throw new ServerException(3212, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}

		return response;
	}
	
	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="getTermRequest")
	public GetTermResponse getTerm(GetTermRequest request) throws ServiceException, ServerException{
		GetTermResponse response = new GetTermResponse();
		ProductTerm productTerm = serviceEinsurance.getProductTermById(request.getProductID(), request.getTermID(), DateUtilities.xmlGregorianCalendarToDate(request.getVersionDate()));
		if(productTerm != null){
			TermType termType = new TermType();
			Utilities.copyToObjectDifferent(productTerm, termType);
			Utilities.copyToObjectDifferent(productTerm.getTerm(), termType);
			Utilities.copyToObjectDifferent(productTerm.getId(), termType);
			response.setTerm(termType);
		} else {
			throw new ServerException(3222, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}
	
	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="listBillingMethodRequest")
	public ListBillingMethodResponse listBillingMethod(ListBillingMethodRequest request) throws ServiceException, ServerException{
		ListBillingMethodResponse response = new ListBillingMethodResponse();
		List<BillingMethod> listBillingMethod = serviceEinsurance.listBillingMethod(request.getProductID(), DateUtilities.xmlGregorianCalendarToDate(request.getVersionDate()));
		if(listBillingMethod != null && listBillingMethod.size() > 0){
			for(BillingMethod billingMethod : listBillingMethod){
				BillingMethodType billingMethoType = new BillingMethodType();
				billingMethoType.setProductID(request.getProductID());
				billingMethoType.setBillingAgencyID((Integer)Utilities.convertNullValue(billingMethod.getBillingAgencyId(),0));
				billingMethoType.setBillingMethodID((Integer)Utilities.convertNullValue(billingMethod.getBillingMethodId(),0));
				billingMethoType.setBillingMethodCode(serviceEinsurance.getDataOptionById(billingMethod.getPaymentType()).getFieldValue());
				billingMethoType.setSwiftCode((String)Utilities.convertNullValue(billingMethod.getSwiftCode(),""));
				billingMethoType.setDescription((String)Utilities.convertNullValue(billingMethod.getDescription(),""));
				billingMethoType.setPaymentTypeID((Integer)Utilities.convertNullValue(billingMethod.getPaymentType(),0));
				billingMethoType.setBankNumber((Integer)Utilities.convertNullValue(billingMethod.getBankNumber(),0));
				billingMethoType.setCardTypeID((Integer)Utilities.convertNullValue(billingMethod.getCardType(),0));
				billingMethoType.setCardBrandTypeID((Integer)Utilities.convertNullValue(billingMethod.getCardBrandType(),0));
				
				response.getBillingMethod().add(billingMethoType);
			}
		} else {
			throw new ServerException(3232, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}

	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="getBillingMethodRequest")
	public GetBillingMethodResponse getBillingMethod(GetBillingMethodRequest request) throws ServiceException, ServerException{
		GetBillingMethodResponse response = new GetBillingMethodResponse();
		ProductPaymentTerm productPaymentTerm = serviceEinsurance.getProductPaymentTerm(request.getProductID(), request.getBillingMethodID(), DateUtilities.xmlGregorianCalendarToDate(request.getVersionDate()));
		
		if(productPaymentTerm != null){
			BillingMethodType billingMethodType = new BillingMethodType();
			BillingMethod billingMethod = productPaymentTerm.getBillingMethod();
			billingMethodType.setProductID((Integer)Utilities.convertNullValue(productPaymentTerm.getId().getProductId(),0));
			billingMethodType.setBillingAgencyID((Integer)Utilities.convertNullValue(billingMethod.getBillingAgencyId(),0));
			billingMethodType.setBillingMethodID((Integer)Utilities.convertNullValue(billingMethod.getBillingMethodId(),0));
			billingMethodType.setBillingMethodCode(serviceEinsurance.getDataOptionById(billingMethod.getPaymentType()).getFieldValue());
			billingMethodType.setSwiftCode((String)Utilities.convertNullValue(billingMethod.getSwiftCode(),""));
			billingMethodType.setDescription((String)Utilities.convertNullValue(billingMethod.getDescription(),""));
			billingMethodType.setPaymentTypeID((Integer)Utilities.convertNullValue(billingMethod.getPaymentType(),0));
			billingMethodType.setBankNumber((Integer)Utilities.convertNullValue(billingMethod.getBankNumber(),0));
			billingMethodType.setCardTypeID((Integer)Utilities.convertNullValue(billingMethod.getCardType(),0));
			billingMethodType.setCardBrandTypeID((Integer)Utilities.convertNullValue(billingMethod.getCardBrandType(),0));			

			response.setBillingMethod(billingMethodType);
		} else {
			throw new ServerException(3242, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}

	@PayloadRoot(namespace=EProductEndpoint.EPRODUCT_NAMESPACE, localPart="getBillingAgencyRequest")
	public GetBillingAgencyResponse getBillingAgency(GetBillingAgencyRequest request) throws ServiceException, ServerException{
		GetBillingAgencyResponse response = new GetBillingAgencyResponse();
		BillingAgency billingAgency = serviceEinsurance.getBillingAgencyById(request.getBillingAgencyID(), DateUtilities.xmlGregorianCalendarToDate(request.getRefDate()));
		if (billingAgency != null) {
			BillingAgencyType billingAgencyType = new BillingAgencyType();
			billingAgencyType.setBillingAgencyID((Integer)Utilities.convertNullValue(billingAgency.getBillingAgencyId(),0));
			billingAgencyType.setBillingAgencyCode((String)Utilities.convertNullValue(billingAgency.getExternalCode(),""));
			billingAgencyType.setName((String)Utilities.convertNullValue(billingAgency.getName(),""));
			billingAgencyType.setNickName((String)Utilities.convertNullValue(billingAgency.getNickName(),""));
			billingAgencyType.setDocumentTypeID((Integer)Utilities.convertNullValue(billingAgency.getDocumentType(),0));
			if(billingAgency.getDocumentNumber() != null && billingAgency.getDocumentNumber().trim().equals("")){
				billingAgency.setDocumentNumber(null);
			}
			billingAgencyType.setDocumentNumber(new BigDecimal((String)Utilities.convertNullValue(billingAgency.getDocumentNumber(),"0")));
			
			response.setBillingAgency(billingAgencyType);			
		} else {
			throw new ServerException(3252, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		return response;
	}
}
