package br.com.tratomais.ws.eproduct;

import java.lang.reflect.Method;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.model.City;
import br.com.tratomais.core.model.Country;
import br.com.tratomais.core.model.Inflow;
import br.com.tratomais.core.model.State;
import br.com.tratomais.core.model.customer.Activity;
import br.com.tratomais.core.model.customer.Occupation;
import br.com.tratomais.core.model.customer.Profession;
import br.com.tratomais.core.model.product.DataOption;
import br.com.tratomais.core.model.product.InsuredObject;
import br.com.tratomais.core.service.IServiceEinsurance;
import br.com.tratomais.core.service.erros.ErrorTypes;
import br.com.tratomais.general.utilities.DateUtilities;
import br.com.tratomais.general.utilities.NumberUtilities;
import br.com.tratomais.ws.eproduct.entities.*;
import br.com.tratomais.ws.exceptions.ServerException;
import edu.emory.mathcs.backport.java.util.Arrays;

@Endpoint
public class EProductDomainEndpoint{
	private static final String EPRODUCT_NAMESPACE="http://www.tratomais.com.br/services/eProduct";
	private static final int FIELD_VALUE = 1;
	private static final int EXTERNAL_CODE = 2; 
	
	private IServiceEinsurance serviceEinsurance;
	
	public void setServiceEinsurance(IServiceEinsurance serviceEinsurance){
		this.serviceEinsurance = serviceEinsurance;
	}

	@SuppressWarnings("unchecked")
	private <T> List <T> setDataObject(Class <T> source, String fieldValue, XMLGregorianCalendar refDate, 
			int referField, List<T> listObject, int methodSearch, int parameter) throws ServiceException{
		Method setter = null;
		List<DataOption> listDataOption = null;
		
		switch(methodSearch){
		case 0 : listDataOption = serviceEinsurance.listDataOptionByFieldValue(fieldValue, DateUtilities.xmlGregorianCalendarToDate(refDate)); break;
		case 1 : listDataOption = serviceEinsurance.listDataOptionByFieldValue(fieldValue, parameter, DateUtilities.xmlGregorianCalendarToDate(refDate)); break;
		}
				
		List<Method> listMethod = Arrays.asList(source.getDeclaredMethods());

		try{
			for(DataOption dataOption : listDataOption){
				T object = source.newInstance();
				for(Method method : listMethod){
					if(method.getName().startsWith("set")){
						setter = object.getClass().getMethod(method.getName(), method.getParameterTypes()[0]);
						if(method.getName().toLowerCase().endsWith("code")){
							String value = "";
							
							switch(referField){
							case FIELD_VALUE : value = dataOption.getFieldValue(); break;
							case EXTERNAL_CODE : value = dataOption.getExternalCode(); break;
							}
							
							value = value == null? " ": value;
							setter.invoke(object, value);
						} else if(method.getName().toLowerCase().endsWith("id")){
							if(object.getClass().toString().toLowerCase().contains(method.getName().toLowerCase().substring(3, method.getName().length()-2))){
								setter.invoke(object, dataOption.getDataOptionId());
							} else {
								int parentID = 0;
								if(dataOption.getParent() != null){
									parentID = dataOption.getParent().getDataOptionId();
								}
								setter.invoke(object, parentID);
							}
						} else if(method.getName().toLowerCase().endsWith("description") || 
								(method.getName().toLowerCase().substring(3).equals("name"))){
							String value = dataOption.getFieldDescription() == null ? " " : dataOption.getFieldDescription();
							setter.invoke(object, value);
						} else if(method.getName().toLowerCase().endsWith("text")){
							String value = dataOption.getDescription() == null ? " " : dataOption.getDescription();
							setter.invoke(object, value);
						}

					}
				}
				listObject.add(object);
			}
		} catch (Exception ex){ex.printStackTrace();}
		
		return listObject;
	}
	
	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listErrorTypeRequest")
	public ListErrorTypeResponse listErrorType(ListErrorTypeRequest listErrorTypeRequest) throws ServiceException, ServerException{
		ListErrorTypeResponse response = new ListErrorTypeResponse();
		setDataObject(ErrorTypeType.class, "ErrorType", listErrorTypeRequest.getRefDate(), FIELD_VALUE, response.getErrorType(), 0, 0);
		
		if(response.getErrorType().size() == 0){
			throw new ServerException(412, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}
	
	
	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listRiskTypeRequest")
	public ListRiskTypeResponse listRiskType(ListRiskTypeRequest listRiskTypeRequest) throws ServiceException, ServerException{
		ListRiskTypeResponse response = new ListRiskTypeResponse();
		InsuredObject object = serviceEinsurance.getObjectByID(listRiskTypeRequest.getObjectID());
		
		if(object != null){
			setDataObject(RiskTypeType.class, "RiskType", listRiskTypeRequest.getRefDate(), EXTERNAL_CODE, response.getRiskType(), 1, object.getObjectType());
		}
		
		if(response.getRiskType().size() == 0){
			throw new ServerException(422, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}

	
	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listValueTypeRequest")
	public ListValueTypeResponse listValueType(ListValueTypeRequest listValueTypeRequest) throws ServiceException, ServerException{
		ListValueTypeResponse response = new ListValueTypeResponse();
		setDataObject(ValueTypeType.class, "InsuredValueType", listValueTypeRequest.getRefDate(), FIELD_VALUE, response.getValueType(), 0, 0);
		
		if(response.getValueType().size() == 0){
			throw new ServerException(432, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}
	
	
	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listPaymentTypeRequest")
	public ListPaymentTypeResponse listPaymentType(ListPaymentTypeRequest listPaymentTypeRequest) throws ServiceException, ServerException{
		ListPaymentTypeResponse response = new ListPaymentTypeResponse();
		setDataObject(PaymentType.class, "PaymentType", listPaymentTypeRequest.getRefDate(), EXTERNAL_CODE, response.getPaymentType(), 0, 0);
		
		if(response.getPaymentType().size() == 0){
			throw new ServerException(442, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}
	
		
	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listCardTypeRequest")
	public ListCardTypeResponse listCardType(ListCardTypeRequest listCardTypeRequest) throws ServiceException, ServerException{
		ListCardTypeResponse response = new ListCardTypeResponse();
		setDataObject(CardTypeType.class, "CardType", listCardTypeRequest.getRefDate(), EXTERNAL_CODE, response.getCardType(), 0, 0);
		
		if(response.getCardType().size() == 0){
			throw new ServerException(452, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}
	
	
	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listCardBrandTypeRequest")
	public ListCardBrandTypeResponse listCardBrandType(ListCardBrandTypeRequest listCardBrandTypeRequest) throws ServiceException, ServerException{
		ListCardBrandTypeResponse response = new ListCardBrandTypeResponse();
		setDataObject(CardBrandTypeType.class, "CardBrandType", listCardBrandTypeRequest.getRefDate(), EXTERNAL_CODE, response.getCardBrandType(), 1, listCardBrandTypeRequest.getCardTypeID());
		
		if(response.getCardBrandType().size() == 0){
			throw new ServerException(462, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listDocumentTypeRequest")
	public ListDocumentTypeResponse listDocumentType(ListDocumentTypeRequest listDocumentTypeRequest) throws ServiceException, ServerException{
		ListDocumentTypeResponse response = new ListDocumentTypeResponse();
		setDataObject(DocumentTypeType.class, "DocumentType", listDocumentTypeRequest.getRefDate(), EXTERNAL_CODE, response.getDocumentType(), 0, 0);
		
		if(response.getDocumentType().size() == 0){
			throw new ServerException(472, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listPersonTypeRequest")
	public ListPersonTypeResponse listPersonType(ListPersonTypeRequest listPersonTypeRequest) throws ServiceException, ServerException{
		ListPersonTypeResponse response = new ListPersonTypeResponse();
		setDataObject(PersonTypeType.class, "PersonType", listPersonTypeRequest.getRefDate(), EXTERNAL_CODE, response.getPersonType(), 0, 0);
		
		if(response.getPersonType().size() == 0){
			throw new ServerException(482, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listGenderRequest")
	public ListGenderResponse listGender(ListGenderRequest listGenderRequest) throws ServiceException, ServerException{
		ListGenderResponse response = new ListGenderResponse();
		setDataObject(GenderType.class, "Gender", listGenderRequest.getRefDate(), EXTERNAL_CODE, response.getGender(), 0, 0);
		
		if(response.getGender().size() == 0){
			throw new ServerException(492, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listMaritalStatusRequest")
	public ListMaritalStatusResponse listMaritalStatus(ListMaritalStatusRequest listMaritalStatusRequest) throws ServiceException, ServerException{
		ListMaritalStatusResponse response = new ListMaritalStatusResponse();
		setDataObject(MaritalStatusType.class, "MaritalStatus", listMaritalStatusRequest.getRefDate(), EXTERNAL_CODE, response.getMaritalStatus(), 0, 0);
		
		if(response.getMaritalStatus().size() == 0){
			throw new ServerException(4102, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}

	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listKinshipTypeRequest")
	public ListKinshipTypeResponse listKinshipType(ListKinshipTypeRequest listKinshipTypeRequest) throws ServiceException, ServerException{
		ListKinshipTypeResponse response = new ListKinshipTypeResponse();
		setDataObject(KinshipTypeType.class, "KinshipType", listKinshipTypeRequest.getRefDate(), EXTERNAL_CODE, response.getKinshipType(), 0, 0);
		
		if(response.getKinshipType().size() == 0){
			throw new ServerException(4112, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listPropertyTypeRequest")
	public ListPropertyTypeResponse listPropertyType(ListPropertyTypeRequest listPropertyTypeRequest) throws ServiceException, ServerException{
		ListPropertyTypeResponse response = new ListPropertyTypeResponse();
		InsuredObject object = serviceEinsurance.getObjectByID(listPropertyTypeRequest.getObjectID());
		
		if(object != null){
			setDataObject(PropertyTypeType.class, "PropertyType", listPropertyTypeRequest.getRefDate(), EXTERNAL_CODE, response.getPropertyType(), 1, object.getObjectType());
		}
		
		if(response.getPropertyType().size() == 0){
			throw new ServerException(4122, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listAddressTypeRequest")
	public ListAddressTypeResponse listAddressType(ListAddressTypeRequest listAddressTypeRequest) throws ServiceException, ServerException{
		ListAddressTypeResponse response = new ListAddressTypeResponse();
		setDataObject(AddressTypeType.class, "AddressType", listAddressTypeRequest.getRefDate(), EXTERNAL_CODE, response.getAddressType(), 0, 0);
		
		if(response.getAddressType().size() == 0){
			throw new ServerException(4132, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listPhoneTypeRequest")
	public ListPhoneTypeResponse listPhoneType(ListPhoneTypeRequest listPhoneTypeRequest) throws ServiceException, ServerException{
		ListPhoneTypeResponse response = new ListPhoneTypeResponse();
		setDataObject(PhoneTypeType.class, "PhoneType", listPhoneTypeRequest.getRefDate(), EXTERNAL_CODE, response.getPhoneType(), 0, 0);
		
		if(response.getPhoneType().size() == 0){
			throw new ServerException(4142, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}

	
	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listProfessionRequest")
	public ListProfessionResponse listProfession(ListProfessionRequest listProfessionRequest) throws ServiceException, ServerException{
		ListProfessionResponse response = new ListProfessionResponse();
		List<Profession> listProfession = serviceEinsurance.listProfessionByRefDate(DateUtilities.xmlGregorianCalendarToDate(listProfessionRequest.getRefDate()));

		if(listProfession != null && listProfession.size() > 0){
			for(Profession profession : listProfession){
				ProfessionType professionType = new ProfessionType();
				professionType.setProfessionID(profession.getProfessionId());
				professionType.setProfessionCode(profession.getExternalCode());
				professionType.setDescription(profession.getName());
				
				response.getProfession().add(professionType);
			}
		} else {
			throw new ServerException(4152, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listProfessionTypeRequest")
	public ListProfessionTypeResponse listProfessionType(ListProfessionTypeRequest listProfessionTypeRequest) throws ServiceException, ServerException{
		ListProfessionTypeResponse response = new ListProfessionTypeResponse();
		setDataObject(ProfessionTypeType.class, "ProfessionType", listProfessionTypeRequest.getRefDate(), EXTERNAL_CODE, response.getProfessionType(), 0, 0);
		
		if(response.getProfessionType().size() == 0){
			throw new ServerException(4162, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listOccupationRequest")
	public ListOccupationResponse listOccupation(ListOccupationRequest listOccupationRequest) throws ServiceException, ServerException{
		ListOccupationResponse response = new ListOccupationResponse();
		List<Occupation> listOcuppation = serviceEinsurance.listOccupationByRefDate(DateUtilities.xmlGregorianCalendarToDate(listOccupationRequest.getRefDate()));

		if(listOcuppation != null && listOcuppation.size() > 0){
			for(Occupation occupation : listOcuppation){
				OccupationType occupationType = new OccupationType();
				occupationType.setOccupationID(occupation.getOccupationId());
				occupationType.setOccupationCode(occupation.getExternalCode());
				occupationType.setDescription(occupation.getName());
				
				response.getOccupation().add(occupationType);
			}
		} else {
			throw new ServerException(4172, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listActivityRequest")
	public ListActivityResponse listActivity(ListActivityRequest listActivityRequest) throws ServiceException, ServerException{
		ListActivityResponse response = new ListActivityResponse();
		List<Activity> listActiveType = serviceEinsurance.listActivityByRefDate(DateUtilities.xmlGregorianCalendarToDate(listActivityRequest.getRefDate()));
		
		if(listActiveType != null && listActiveType.size() > 0){
			for(Activity activity : listActiveType){
				ActivityType activityType = new ActivityType();
				activityType.setActivityID(activity.getActivityId());
				activityType.setActivityCode(activity.getExternalCode());
				activityType.setDescription(activity.getName());
				
				response.getActivity().add(activityType);
			}
		} else {
			throw new ServerException(4182, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listInflowRequest")
	public ListInflowResponse listInflow(ListInflowRequest listInflowRequest) throws ServiceException, ServerException{
		ListInflowResponse response = new ListInflowResponse();
		List<Inflow> listInflow = serviceEinsurance.listInflowByRefDate(DateUtilities.xmlGregorianCalendarToDate(listInflowRequest.getRefDate()));

		if(listInflow != null && listInflow.size() > 0){
			for(Inflow inflow : listInflow){
				InflowType inflowType = new InflowType();
				inflowType.setInflowOf(NumberUtilities.doubleToBigDecimal(inflow.getInflowOf()));
				inflowType.setInflowUntil(NumberUtilities.doubleToBigDecimal(inflow.getInflowUntil()));
				inflowType.setDescription(inflow.getDescription());
				inflowType.setInflowCode(inflow.getExternalCode());
				inflowType.setInflowID(inflow.getInflowId());
				
				response.getInflow().add(inflowType);
			}
		} else {
			throw new ServerException(4192, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listCountryRequest")
	public ListCountryResponse listCountry(ListCountryRequest listCountryRequest) throws ServiceException, ServerException{
		ListCountryResponse response = new ListCountryResponse();
		List<Country> listCountry = serviceEinsurance.listCountryByRefDate(DateUtilities.xmlGregorianCalendarToDate(listCountryRequest.getRefDate()));

		if(listCountry != null && listCountry.size() > 0){
			for(Country country : listCountry){
				CountryType countryType = new CountryType();
				countryType.setDescription(country.getName());
				countryType.setCountryCode(country.getExternalCode());
				countryType.setCountryID(country.getCountryId());
				
				response.getCountry().add(countryType);
			}
		} else {
			throw new ServerException(4202, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listLocalityRequest")
	public ListLocalityResponse listLocality(ListLocalityRequest listLocalityRequest) throws ServiceException, ServerException{
		ListLocalityResponse response = new ListLocalityResponse();
		setDataObject(LocalityType.class, "LocalityCode", listLocalityRequest.getRefDate(), EXTERNAL_CODE, response.getLocality(), 0, 0);
		
		if(response.getLocality().size() == 0){
			throw new ServerException(4212, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listStateRequest")
	public ListStateResponse listState(ListStateRequest listStateRequest) throws ServiceException, ServerException{
		ListStateResponse response = new ListStateResponse();
		List<State> listState = serviceEinsurance.listStateByRefDate(listStateRequest.getCountryID(), DateUtilities.xmlGregorianCalendarToDate(listStateRequest.getRefDate()));

		if(listState != null && listState.size() > 0){
			for(State state : listState){
				StateType stateType = new StateType();
				stateType.setCountryID(state.getCountryId());
				stateType.setStateID(state.getStateId());
				stateType.setStateCode(state.getExternalCode());
				stateType.setName(state.getName());
				stateType.setLocalityID(state.getLocalityCode());
				
				response.getState().add(stateType);
			}
		} else {
			throw new ServerException(4222, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listCityRequest")
	public ListCityResponse listCity(ListCityRequest listCityRequest) throws ServiceException, ServerException{
		ListCityResponse response = new ListCityResponse();
		List<City> listCity = serviceEinsurance.listCityByRefDate(listCityRequest.getCountryID(), listCityRequest.getStateID(), DateUtilities.xmlGregorianCalendarToDate(listCityRequest.getRefDate()));

		if(listCity != null && listCity.size() > 0){
			for(City city : listCity){
				CityType cityType = new CityType();
				cityType.setCountryID(city.getState().getCountryId());
				cityType.setStateID(city.getStateId());
				cityType.setCityID(city.getCityId());
				cityType.setCityCode(city.getExternalCode());
				cityType.setName(city.getName());
				
				response.getCity().add(cityType);
			}
		} else {
			throw new ServerException(4232, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listActivityTypeRequest")
	public ListActivityTypeResponse listActivityType(ListActivityTypeRequest listActivityTypeRequest) throws ServiceException, ServerException{
		ListActivityTypeResponse response = new ListActivityTypeResponse();
		setDataObject(ActivityTypeType.class, "", listActivityTypeRequest.getRefDate(), FIELD_VALUE, response.getActivityType(), 1, listActivityTypeRequest.getPropertyTypeID());
		
		if(response.getActivityType().size() == 0){
			throw new ServerException(4242, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listContractStatusRequest")
	public ListContractStatusResponse listContractStatus(ListContractStatusRequest listContractStatusRequest) throws ServiceException, ServerException{
		ListContractStatusResponse response = new ListContractStatusResponse();
		setDataObject(ContractStatusType.class, "ContractStatus", listContractStatusRequest.getRefDate(), FIELD_VALUE, response.getContractStatus(), 0, 0);
		
		if(response.getContractStatus().size() == 0){
			throw new ServerException(4252, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listIssuanceTypeRequest")
	public ListIssuanceTypeResponse listIssuanceType(ListIssuanceTypeRequest listIssuanceTypeRequest) throws ServiceException, ServerException{
		ListIssuanceTypeResponse response = new ListIssuanceTypeResponse();
		setDataObject(IssuanceTypeType.class, "IssuanceType", listIssuanceTypeRequest.getRefDate(), FIELD_VALUE, response.getIssuanceType(), 0, 0);
		
		if(response.getIssuanceType().size() == 0){
			throw new ServerException(4262, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listIssuanceStatusRequest")
	public ListIssuanceStatusResponse listIssuanceStatus(ListIssuanceStatusRequest listIssuanceStatusRequest) throws ServiceException, ServerException{
		ListIssuanceStatusResponse response = new ListIssuanceStatusResponse();
		setDataObject(IssuanceStatusType.class, "IssuingStatus", listIssuanceStatusRequest.getRefDate(), FIELD_VALUE, response.getIssuanceStatus(), 0, 0);
		
		if(response.getIssuanceStatus().size() == 0){
			throw new ServerException(4272, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listEndorsementTypeRequest")
	public ListEndorsementTypeResponse listEndorsementType(ListEndorsementTypeRequest listEndorsementTypeRequest) throws ServiceException, ServerException{
		ListEndorsementTypeResponse response = new ListEndorsementTypeResponse();
		setDataObject(EndorsementTypeType.class, "EndorsementType", listEndorsementTypeRequest.getRefDate(), FIELD_VALUE, response.getEndorsementType(), 0, 0);
		
		if(response.getEndorsementType().size() == 0){
			throw new ServerException(4282, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listExemptionTypeRequest")
	public ListExemptionTypeResponse listExemptionType(ListExemptionTypeRequest listExemptionTypeRequest) throws ServiceException, ServerException{
		ListExemptionTypeResponse response = new ListExemptionTypeResponse();
		setDataObject(ExemptionTypeType.class, "TaxExemptionType", listExemptionTypeRequest.getRefDate(), FIELD_VALUE, response.getExemptionType(), 0, 0);
		
		if(response.getExemptionType().size() == 0){
			throw new ServerException(4292, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}


	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listRenewalTypeRequest")
	public ListRenewalTypeResponse listRenewalType(ListRenewalTypeRequest listRenewalTypeRequest) throws ServiceException, ServerException{
		ListRenewalTypeResponse response = new ListRenewalTypeResponse();
		setDataObject(RenewalTypeType.class, "RenewalType", listRenewalTypeRequest.getRefDate(), FIELD_VALUE, response.getRenewalType(), 0, 0);
		
		if(response.getRenewalType().size() == 0){
			throw new ServerException(4302, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}
	
	
	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listItemStatusRequest")
	public ListItemStatusResponse listItemStatus(ListItemStatusRequest listItemStatusRequest) throws ServiceException, ServerException{
		ListItemStatusResponse response = new ListItemStatusResponse();
		setDataObject(ItemStatusType.class, "ItemStatus", listItemStatusRequest.getRefDate(), FIELD_VALUE, response.getItemStatus(), 0, 0);
		
		if(response.getItemStatus().size() == 0){
			throw new ServerException(4312, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}
	
	
	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listRestrictionTypeRequest")
	public ListRestrictionTypeResponse listRestrictionType(ListRestrictionTypeRequest listRestrictionTypeRequest) throws ServiceException, ServerException{
		ListRestrictionTypeResponse response = new ListRestrictionTypeResponse();
		InsuredObject object = serviceEinsurance.getObjectByID(listRestrictionTypeRequest.getObjectID());
		
		if(object != null && object.getObjectId() == 2){
			setDataObject(RestrictionTypeType.class, "RestrictionRiskType", listRestrictionTypeRequest.getRefDate(), FIELD_VALUE, response.getRestrictionType(), 0, 0);
		}
		
		if(response.getRestrictionType().size() == 0){
			throw new ServerException(4322, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}
	
	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listInstallmentTypeRequest")
	public ListInstallmentTypeResponse listInstallmentType(ListInstallmentTypeRequest listInstallmentTypeRequest) throws ServiceException, ServerException{
		ListInstallmentTypeResponse response = new ListInstallmentTypeResponse();
		setDataObject(InstallmentType.class, "InstallmentType", listInstallmentTypeRequest.getRefDate(), FIELD_VALUE, response.getInstallmentType(), 0, 0);
		
		if(response.getInstallmentType().size() == 0){
			throw new ServerException(4332, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}
	
	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listInstallmentStatusRequest")
	public ListInstallmentStatusResponse listInstallmentStatus(ListInstallmentStatusRequest listInstallmentStatusRequest) throws ServiceException, ServerException {
		ListInstallmentStatusResponse response = new ListInstallmentStatusResponse();
		setDataObject(InstallmentStatusType.class, "InstallmentStatus", listInstallmentStatusRequest.getRefDate(), FIELD_VALUE, response.getInstallmentStatus(), 0, 0);
		
		if(response.getInstallmentStatus().size() == 0){
			throw new ServerException(4342, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}
	
	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listCancelReasonRequest")
	public ListCancelReasonResponse listCancelReasonRequest(ListCancelReasonRequest listCancelReasonRequest) throws ServiceException, ServerException{
		ListCancelReasonResponse response = new ListCancelReasonResponse();
		setDataObject(CancelReasonType.class, "CancelReason", listCancelReasonRequest.getRefDate(), FIELD_VALUE, response.getCancelReason(), 0, 0);
		
		if(response.getCancelReason().size() == 0){
			throw new ServerException(4352, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}
	
	@PayloadRoot(namespace=EProductDomainEndpoint.EPRODUCT_NAMESPACE, localPart="listReportTypeRequest")
	public ListReportTypeResponse listReportTypeRequest(ListReportTypeRequest listReportTypeRequest) throws ServiceException, ServerException{
		ListReportTypeResponse response = new ListReportTypeResponse();
		setDataObject(ReportTypeType.class, "ReportType", listReportTypeRequest.getRefDate(), FIELD_VALUE, response.getReportType(), 0, 0);
		
		if(response.getReportType().size() == 0){
			throw new ServerException(4362, 772, ErrorTypes.VALIDATION_RECORD_NOT_FOUND.getMessage(null));
		}
		
		return response;
	}
}
