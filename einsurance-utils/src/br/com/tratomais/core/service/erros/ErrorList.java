package br.com.tratomais.core.service.erros;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Erro list
 * @author luiz.alberoni
 *
 */
public class ErrorList implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2674204372037933160L;
	/**
	 * 
	 */
	private List<Erro> lista = new ArrayList<Erro>();
	
	/**
	 * 
	 * @return
	 */
	public Erro add() {
		Erro retorno = new Erro();
		retorno.setNrSeq(lista.size()+1);
		lista.add(retorno);
		return retorno;
	}
	
	/**
	 * 
	 * @return
	 */
	public Erro add(Erro erro) {
		erro.setNrSeq(lista.size()+1);
		lista.add(erro);
		return erro;
	}

	/**
	 * @return Error list
	 */
	public List<Erro> getListaErros() {
		return this.lista;
	}
	
	/**
	 * 
	 * @param code
	 * @param description
	 * @param localClass
	 * @param errorType
	 * @param origin
	 * @param itemNumber
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Erro add(long code, String description, Class localClass, int errorType, String origin, int itemNumber, int riskGroupNumber, int coverageNumber) {
		Erro retorno = new Erro(code, 
								description, 
								localClass, 
								errorType, 
								origin, 
								lista.size()+1, 
								itemNumber,
								riskGroupNumber,
								coverageNumber);
		lista.add(retorno);
		return retorno;
	}

	@SuppressWarnings("unchecked")
	public Erro add(long code, String description, Class localClass, int errorType, String origin, int itemNumber, int riskGroupNumber) {
		return add(code, description, localClass, errorType, origin, itemNumber, riskGroupNumber, 0);
	}
	
	@SuppressWarnings("unchecked")
	public Erro add(long code, String description, Class localClass, int errorType, String origin, int itemNumber) {
		return add(code, description, localClass, errorType, origin, itemNumber, 0);
	}
	
	@SuppressWarnings("unchecked")
	public Erro add(long code, String description, Class localClass, int errorType, String origin) {
		return add(code, description, localClass, errorType, origin, 0);
	}
	
	@SuppressWarnings("unchecked")
	public Erro add(ErrorTypes tipoErro, Object[] params, Class localClass, String origin) {
		return add(tipoErro, params, localClass, origin, 0);
	}
	
	@SuppressWarnings("unchecked")
	public Erro add(ErrorTypes tipoErro, Object[] params, Class localClass, String origin, int itemNumber) {
		return add(tipoErro, params, localClass, origin, itemNumber, 0);
	}
	
	@SuppressWarnings("unchecked")
	public Erro add(ErrorTypes tipoErro, Object[] params, Class localClass, String origin, int itemNumber, int riskGroupNumber) {
		return add(tipoErro, params, localClass, origin, itemNumber, riskGroupNumber, 0);
	}
	
	@SuppressWarnings("unchecked")
	public Erro add(ErrorTypes tipoErro, Object[] params, Class localClass, String origin, int itemNumber, int riskGroupNumber, int coverageNumber) {
		Erro retorno = new Erro(tipoErro.getErrorCode(),
								tipoErro.getMessage(params),
								localClass, 
								tipoErro.getErrorType(), 
								origin, 
								lista.size()+1, 
								itemNumber, 
								riskGroupNumber,
								coverageNumber);
		lista.add(retorno);
		return retorno;
	}

	@SuppressWarnings("unchecked")
	public Erro add(ErrorTypes tipoErro, String personalRiskType, String personalName, Double minimumAge, Double maximumAge, Class localClass, String origin, int itemNumber) {
		return add(tipoErro, personalRiskType, personalName, minimumAge, maximumAge, localClass, origin, itemNumber, 0);
	}

	@SuppressWarnings("unchecked")
	public Erro add(ErrorTypes tipoErro, String personalRiskType, String personalName, Double minimumAge, Double maximumAge, Class localClass, String origin, int itemNumber, int riskGroupNumber) {
		String message;
		
		//type message
		if(minimumAge == 0){
			message = "menor que ";
		}else if(maximumAge == 0){
			message = "maior que ";
		}else{
			message = "entre ";
		}
		
		//age type
		if(minimumAge > 0 && minimumAge < 1){
			Double _minimumAge = (minimumAge * 12);
			message += _minimumAge.intValue() + " meses";	
		}else if(minimumAge > 0){
			message += minimumAge.intValue() + " anos";
		}
		
		if(minimumAge > 0 && maximumAge > 0){
			message += " e ";
		}
		
		if(maximumAge > 0 && maximumAge < 1){
			Double _maximumAge = (maximumAge * 12);
			message += _maximumAge.intValue() + " meses";
		}else if(maximumAge > 0){
			message += maximumAge.intValue() + " anos";
		}
		
		Object[] params = {personalRiskType, personalName, message};
		
		Erro retorno = new Erro(
				tipoErro.getErrorCode(),
				tipoErro.getMessage(params),
				localClass, tipoErro.getErrorType(), origin, lista.size()+1, itemNumber, riskGroupNumber, 0);
		
		lista.add(retorno);
		return retorno;
	}	
	
	public int getErrorCount() {
		return this.lista.size();
	}
	
	public Erro getStringError(int number){
		return this.lista.get(number);
	}
}