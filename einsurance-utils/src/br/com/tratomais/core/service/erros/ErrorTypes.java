/**
 * 
 */
package br.com.tratomais.core.service.erros;

import java.text.MessageFormat;
import java.util.HashMap;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;

import br.com.tratomais.core.util.AppContextHelper;

/**
 * @author luiz.alberoni
 */
public enum ErrorTypes implements MessageSourceAware {
	VALIDATION_PRODUCT_ID_INVALID(1, "error.validation.productid.invalid", 1),
	VALIDATION_DATE_OUT_OF_RANGE(2, "error.validation.date.out.range", 2),
	VALIDATION_ITEM_COVERAGE_ABSENT(3, "error.validation.item.coverage.absent", 3),
	VALIDATION_ITEM_COVERAGE_INVALID(4, "error.validation.item.coverage.invalid",4),
	VALIDATION_CONTRACT_INVALID(5,"error.validation.contract.invalid",5),
	VALIDATION_ITEM_ABSENT(6,"error.validation.item.absent",6),
	VALIDATION_ITEM_INVALID(7,"error.validation.item.invalid",7),
	VALIDATION_ITEM_COVERAGE_BASIC_ABSENT(8,"error.validation.item.coverage.basic.absent",8),
	VALIDATION_INSURER_ID_DISPARITY(9,"error.validation.contract.insurerid.disparity",9),
	VALIDATION_BROKER_ID_DISPARITY(10,"error.validation.contract.brokerid.disparity",10),
	VALIDATION_PARTNER_ID_DISPARITY(11,"error.validation.contract.partnerid.disparity",11),
	VALIDATION_POLICY_TYPE_INVALID(12,"error.validation.contract.policytype.invalid",12),
	VALIDATION_POLICY_NUMBER_DISPARITY(13,"error.validation.contract.policynumber.disparity",13),
	VALIDATION_POLICY_INFORMED_DISPARITY(14,"error.validation.contract.policyinformed.disparity",14),
	VALIDATION_PRODUCT_VERSION_INVALID(15,"error.validation.contract.productversion.invalid",15),
	VALIDATION_SUBSIDIARY_DISPARITY(16,"error.validation.contract.subsidiary.disparity",16),
	PROCESSING_ERROR(17,"error.processing.error",17),
	PROCESSING_ERROR_FACTOR_NOT_FOUND(18,"error.processing.error.factor.absent",18),
	VALIDATION_ITEM_CREDIT_VALUE_OUT_OF_RANGE(19,"error.validation.item.credit.value.out.of.range",19),
	VALIDATION_ITEM_CREDIT_VALUE_LESS_THAN_MINIMUM(20,"error.validation.item.credit.value.less.than.minimum",20),
	VALIDATION_ITEM_CREDIT_VALUE_GRATER_THAN_MAXIMUM(21,"error.validation.item.credit.value.greater.than.maximum",21),
	VALIDATION_COVERAGE_VALUE_LESS_THAN_MINIMUM(22,"error.validation.coverage.value.less.than.minimum",22),
	VALIDATION_COVERAGE_VALUE_GRATER_THAN_MAXIMUM(23,"error.validation.coverage.value.greater.than.maximum",23),
	PROCESSING_MAX_POLICY(24,"error.processing.maxpolicy",24),
	VALIDATION_RISK_VALUE_LESS_THAN_MINIMUM(25,"error.validation.risk.value.less.than.minimum",25),
	VALIDATION_RISK_VALUE_GRATER_THAN_MAXIMUM(26,"error.validation.risk.value.greater.than.maximum",26),
	VALIDATION_CONTRACT_POLICY_SUM_GREATER_THAN_MAXIMUM(27,"error.validation.policy.sum.greater.than.maximum",27),
	VALIDATION_POLICY_NUMBER_GREATER_THAN_MAXIMUM(28,"error.validation.policy.number.greater.than.maximum",28),
	VALIDATION_INSURED_AGE_INVALID(29,"error.validation.insured.age.out.of.range",29),
	PROCESSING_RATE_NOT_FOUND(30, "error.processing.rate.not.found", 30),
	VALIDATION_MASTER_POLICY_ID_INVALID(31, "error.validation.contract.masterpolicy.invalid", 31),
	VALIDATION_INSURER_ID_INVALID(32, "error.validation.contract.insurerid.invalid", 32),
	VALIDATION_SUBSIDIARY_ID_INVALID(33, "error.validation.contract.subsidiaryid.invalid", 33),
	VALIDATION_ENDORSEMENT_INVALID(34, "error.validation.endorsement.invalid", 34),
	VALIDATION_CHANNEL_ID_INVALID(35, "error.validation.contract.channelid.invalid", 35),
	VALIDATION_BROKER_ID_INVALID(36, "error.validation.contract.brokerid.invalid", 36),
	VALIDATION_PARTNER_ID_INVALID(37, "error.validation.contract.partnerid.invalid", 37),
	VALIDATION_TERM_ID_INVALID(38, "error.validation.endorsement.termid.invalid", 38),
	VALIDATION_REFERENCE_DATE_INVALID(39, "error.validation.endorsement.referencedate.invalid", 39),
	VALIDATION_EFFECTIVE_DATE_INVALID(40, "error.validation.endorsement.effectivedate.invalid", 40),
	VALIDATION_TERM_ID_DISPARITY(41, "error.validation.endorsement.termid.disparity", 41),
	VALIDATION_BIRTH_DATE_INVALID(42, "error.validation.item.personal.birthdate.invalid", 42),
	VALIDATION_PERSONAL_RISK_TYPE_INVALID(43, "error.validation.item.personal.risktype.invalid", 43),
	VALIDATION_PERSONAL_KINSHIP_TYPE_INVALID(44, "error.validation.item.personal.kinshiptype.invalid", 44),
	VALIDATION_COVERAGE_ID_INVALID(45, "error.validation.coverageid.invalid", 45),
	VALIDATION_PRODUTCT_PLAN_COVERAGE_INVALID(46, "error.validation.product.plan.coverage.invalid", 46),
	VALIDATION_COVERAGE_VALUE_RANGE_ID_INVALID(47, "error.validation.product.plan.coverage.value.rangeid.invalid", 47),
	VALIDATION_COVERAGE_VALUE_INVALID(48, "error.validation.coverage.value.invalid", 48),
	VALIDATION_COMMISSION_FACTOR_LESS_THAN_MINIMUM(49, "error.validation.commission.factor.less.than.minimum", 49),
	VALIDATION_COMMISSION_FACTOR_GRATER_THAN_MAXIMUM(49, "error.validation.commission.factor.greater.than.maximum", 49),
	VALIDATION_EXPIRY_DATE_INVALID(50, "error.validation.endorsement.expirydate.invalid", 50),
	VALIDATION_PAYMENT_TERM_ID_INVALID(51, "error.validation.endorsement.paymenttermid.invalid", 51),
	VALIDATION_PAYMENT_TERM_ID_DISPARITY(52, "error.validation.endorsement.paymenttermid.disparity", 52),
	VALIDATION_POLICY_CHANGE_REGISTER_ERROR(53, "error.validation.policy.change.register", 53),
	VALIDATION_POLICY_CHANGE_TECHNICAL_ERROR(54, "error.validation.policy.change.technical", 54),
	VALIDATION_POLICY_CHANGE_CLAIM_ERROR(55, "error.validation.policy.change.claim", 55),
	VALIDATION_POLICY_CANCEL_CLAIM_ERROR(56, "error.validation.policy.cancel.claim", 56),
	VALIDATION_INSURED_ADDRESS_ABSENT(57, "error.validation.insured.address.absent", 57),
	VALIDATION_POLICY_HOLDER_ADDRESS_ABSENT(58, "error.validation.policy.holder.address.absent", 58),
	
	VALIDATION_ATTRIBUTES_RULE_DEDUCIBLE_NOT_OK(60, "error.validation.deducible.attributes.not.ok", 60),
	VALIDATION_ATTRIBUTES_RULE_COVERAGE_NOT_OK(61, "error.validation.coverage.attributes.not.ok", 61),
	VALIDATION_ATTRIBUTES_RULE_PROFILE_NOT_OK(62, "error.validation.profile.attributes.not.ok", 62),
	VALIDATION_ATTRIBUTES_RULE_ITEM_NOT_OK(63, "error.validation.item.attributes.not.ok", 63),
	VALIDATION_ATTRIBUTES_RULE_INSTALLMENT_NOT_OK(64, "error.validation.installment.attributes.not.ok", 64),
	
	VALIDATION_ITEM_PROFILE_INVALID(65, "error.validation.item.profile.invalid", 65),
	VALIDATION_PAYMENT_OPTION_INVALID(66, "error.validation.payment.option.invalid", 66),
	VALIDATION_PAYMENT_OPTION_ABSENT(67, "error.validation.payment.option.absent", 67),
	VALIDATION_RULE_TYPE_COVERAGE_BLOCKED(68, "error.validation.ruletype.coverage.blocked", 68),
	VALIDATION_RULE_TYPE_INSURED_VALUE_BLOCKED(69, "error.validation.ruletype.insuredvalue.blocked", 69),
	VALIDATION_RULE_TYPE_DEDUCTIBLE_BLOCKED(70, "error.validation.ruletype.deductible.blocked", 70),
	VALIDATION_RULE_TYPE_PROFILE_BLOCKED(71, "error.validation.ruletype.profile.blocked", 71),
	VALIDATION_RULE_TYPE_PAYMENT_TERM_BLOCKED(72, "error.validation.ruletype.payment.term.blocked", 72),
	VALIDATION_RULE_TYPE_PAYMENT_TYPE_BLOCKED(73, "error.validation.ruletype.payment.type.blocked", 73),
	VALIDATION_COVERAGE_MUTUALLY_EXCLUSIVE(74, "error.validation.coverage.mutually.exclusive.ok", 74),
	VALIDATION_POLICY_NUMBER_OBJECT_RISK_FOUND(75, "error.validation.policy.object.risk.found", 75),
	
	VALIDATION_ITEM_PROPERTY_STATE_ABSENT(76, "error.validation.item.property.state.absent", 76),
	VALIDATION_ITEM_PROPERTY_CITY_ABSENT(77, "error.validation.item.property.city.absent", 77),
	VALIDATION_ITEM_PROPERTY_DISTRICT_ABSENT(78, "error.validation.item.property.district.absent", 78),
	VALIDATION_ITEM_PROPERTY_STREET_ABSENT(79, "error.validation.item.property.street.absent", 79),
	
	VALIDATION_COVERAGE_DEPENDENCE_AND(80, "error.validation.coverage.dependence.and", 80),
	VALIDATION_COVERAGE_DEPENDENCE_OR(81, "error.validation.coverage.dependence.or", 81),

	VALIDATION_PROPERTY_RISK_TYPE_INVALID(82, "error.validation.item.property.risktype.invalid", 82),
	VALIDATION_POLICY_RENEWAL_INVALID(83, "error.validation.policy.renewal.invalid", 83),

	VALIDATION_PERSON_TYPE_INVALID(84,"error.validation.person.type.invalid",84),
	VALIDATION_GENDER_INVALID(85,"error.validation.gender.invalid",85),
	VALIDATION_MARITAL_STATUS_INVALID(86,"error.validation.marital.status.invalid",86),
	VALIDATION_DOCUMENT_TYPE_INVALID(87,"error.validation.document.type.invalid",87),
	VALIDATION_OCCUPATION_INVALID(88,"error.validation.occupation.invalid",88),
	VALIDATION_ACTIVITY_INVALID(89,"error.validation.activity.invalid",89),
	VALIDATION_PROFESSION_INVALID(90,"error.validation.profession.invalid",90),
	VALIDATION_PROFESSION_TYPE_INVALID(91,"error.validation.profession.type.invalid",91),
	VALIDATION_INFLOW_INVALID(92,"error.validation.inflow.invalid",92),
	VALIDATION_RESTRICTION_RISK_TYPE_INVALID(93,"error.validation.restriction.risk.type.invalid",93),
	VALIDATION_MASTER_POLICY_ID_DISPARITY(94, "error.validation.contract.masterpolicy.disparity", 94),
	
	VALIDATION_CONTRACT_REQUIRED(95, "error.validation.contractid.required", 95),
	VALIDATION_POLICYNUMBER_REQUIRED(96, "error.validation.policynumber.required", 96),
	VALIDATION_CERTIFICATE_NUMBER_REQUIRED(97, "error.validation.certificatenumber.required", 97),
	VALIDATION_DOCUMENT_NUMBER_REQUIRED(98, "error.validation.documentnumber.required", 98),
	
	VALIDATION_PHONE_TYPE_INVALID(99,"error.validation.phone.type.invalid",99),
	VALIDATION_ADDRESS_TYPE_INVALID(100,"error.validation.address.type.invalid",100),
	VALIDATION_COUNTRY_ID_INVALID(101,"error.validation.country.id.invalid",101),
	VALIDATION_STATE_ID_INVALID(102,"error.validation.state.id.invalid",102),
	VALIDATION_CITY_ID_TYPE_INVALID(103,"error.validation.city.id.invalid",103),
	VALIDATION_ITEM_OUT_OF_SEQUENCE(104,"error.validation.item.out.of.sequence",104),
	VALIDATION_ENDORSEMENT_NOT_FOUND(105,"error.validation.endorsement.not.found",105),
	
	VALIDATION_BANK_ACCOUNT_NUMBER_INVALID(106,"error.validation.bank.account.number.invalid",106),
	VALIDATION_BANK_CHECK_NUMBER_INVALID(107,"error.validation.bank.check.number.invalid",107),
	VALIDATION_CARD_NUMBER_INVALID(108,"error.validation.card.number.invalid",108),
	VALIDATION_CARD_EXPIRATION_DATE_INVALID(109,"error.validation.card.expiration.date.invalid",109),
	VALIDATION_CARD_HOLDER_NAME_ABSENT(110,"error.validation.card.holder.name.absent",110),
	VALIDATION_OTHER_ACCOUNT_NUMBER_INVALID(111,"error.validation.other.account.number.invalid",111),
	
	VALIDATION_BENEFICIARY_OUT_OF_SEQUENCE(112,"error.validation.beneficiary.out.of.sequence",112),
	VALIDATION_BENEFICIARY_PARTICIPATION_DISPARITY(113,"error.validation.beneficiary.participation.disparity",113),
	
	VALIDATION_RISK_GROUP_OUT_OF_SEQUENCE(114,"error.validation.risk.group.out.of.sequence",114),
	VALIDATION_RECORD_NOT_FOUND(115,"error.validation.record.not.found",115),
	VALIDATION_CANCEL_REASON_INVALID(116,"error.validation.cancel.reason.invalid",116),
	VALIDATION_PROPOSAL_UNLOCK_NOT_ALLOWED(117,"error.validation.proposal.unlock.not.allowed",117),
	VALIDATION_ISSUANCE_TYPE_NOT_ALLOWED(118,"error.validation.issuance.type.not.allowed",118),
	VALIDATION_PRINT_NOT_ALLOWED(119,"error.validation.print.not.allowed",119),
	VALIDATION_INSTALLMENT_STATUS_INVALID(120,"error.validation.installment.status.invalid",120),
	VALIDATION_PHONE_NOT_FOUND(121,"error.validation.phone.not.found", 121),
	VALIDATION_ACTION_IS_NOT_ALLOWED(122,"error.validation.action.is.not.allowed", 122),
	
	VALIDATION_CARD_TYPE_INVALID(123,"error.validation.card.type.invalid",123),
	VALIDATION_CARD_BRANDE_TYPE_INVALID(124,"error.validation.card.brand.type.invalid",124),
	
	VALIDATION_PROPERTY_ACTIVITY_TYPE_INVALID(125, "error.validation.property.activity.type.invalid", 125),
	VALIDATION_PROPERTY_RISK_NOT_FOUND(126,"error.validation.property.risk.not.found", 126),
	VALIDATION_COVERAGE_COMPULSORY_NOT_FOUND(127, "error.validation.coverage.compulsory.not.found", 127),
	
	VALIDATION_POLICY_CANCEL_NOT_ALLOWED(128, "error.validation.policy.cancel.not.allowed", 128),
	VALIDATION_POLICY_REACTIVATION_NOT_ALLOWED(129, "error.validation.policy.reactivation.not.allowed", 129),
	
	PROCESSING_ERROR1(999,"error.processing.error",999);
	
	/**
	 * C�digo do erro
	 */
	private long errorCode;
	
	/**
	 * Error type
	 */
	private int errorType;
	
	/**
	 * Error Key in file
	 */
	private String key;
	
	/**
	 * Message Source, received from springframework
	 */
	private static MessageSource messageSource;

	/**
	 * 
	 * @param codigo Error code
	 * @param key Error key(To lookup in bundle
	 * @param tipoErro ErrorType
	 */
	ErrorTypes(long codigo, String key, int tipoErro){
		this.errorCode = codigo;
		this.key = key;
		this.errorType=tipoErro;
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.context.MessageSourceAware#setMessageSource(org.springframework.context.MessageSource)
	 */
	@Override
	public void setMessageSource(MessageSource messageSource) {
		ErrorTypes.messageSource = messageSource;
	}
	
	/**
	 * Returns message correspondent to messageSource
	 * @param params
	 * @return
	 */
	public String getMessage(Object [] params) {
		MessageFormat mf = new MessageFormat(mensagens.get(key));
		return mf.format( params );
	}
	
	private static HashMap<String, String>  mensagens = new HashMap<String, String>();
	
	static {
		//msg_esVE(mensagens);
		msg_ptBR(mensagens);
	}
	
	private static void msg_esVE(HashMap<String, String>  mensagens_esVE){
		mensagens_esVE.put("error.validation.contract.invalid", "Contrato inv�lido o con error!");
		mensagens_esVE.put("error.validation.contract.productversion.invalid", "Versi�n del producto inv�lida o inexistente!");
		mensagens_esVE.put("error.validation.contract.masterpolicy.invalid", "P�liza maestra inv�lida o inexistente!");
		mensagens_esVE.put("error.validation.contract.insurerid.invalid", "Aseguradora inv�lida o inexistente!");
		mensagens_esVE.put("error.validation.contract.subsidiaryid.invalid", "Sucursal inv�lida o inexistente!");
		mensagens_esVE.put("error.validation.contract.subsidiary.disparity", "Hay diferencias de sucursal!");
		mensagens_esVE.put("error.validation.contract.channelid.invalid", "Canal de venta inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.contract.brokerid.invalid", "Corredor inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.contract.brokerid.disparity", "Hay diferencias de corredor!");
		mensagens_esVE.put("error.validation.contract.partnerid.invalid", "Aliado inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.contract.partnerid.disparity", "Hay diferencias de aliado!");
		mensagens_esVE.put("error.validation.contract.policynumber.disparity", "Hay diferencias de numero de p�liza!");
		mensagens_esVE.put("error.validation.endorsement.invalid", "Endoso inv�lido o con error!");
		mensagens_esVE.put("error.validation.endorsement.referencedate.invalid", "Fecha de referencia inv�lida!");
		mensagens_esVE.put("error.validation.endorsement.effectivedate.invalid", "Fecha de in�cio de vigencia inv�lido!");
		mensagens_esVE.put("error.validation.endorsement.expirydate.invalid", "Fecha de fin de vigencia inv�lido!");
		mensagens_esVE.put("error.validation.endorsement.termid.invalid", "Plazo de vig�ncia inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.endorsement.termid.disparity", "Plazo de vig�ncia inv�lido o inexistente para o producto!");
		mensagens_esVE.put("error.validation.item.absent", "No hay item en la emisi�n!");
		mensagens_esVE.put("error.validation.item.invalid", "Item inv�lido o con error!");
		mensagens_esVE.put("error.validation.item.personal.birthdate.invalid", "Fecha de nacimiento inv�lida!");
		mensagens_esVE.put("error.validation.item.personal.risktype.invalid", "Tipo de riesgo inv�lido!");
		mensagens_esVE.put("error.validation.item.personal.kinshiptype.invalid", "Tipo de parentesco inv�lido!");
		mensagens_esVE.put("error.validation.item.credit.value.out.of.range", "Valor del cr�dito fuera de rango");
		mensagens_esVE.put("error.validation.item.credit.value.less.than.minimum","Valor del cr�dito inferior al minimo de \"{0}\" ");
		mensagens_esVE.put("error.validation.item.credit.value.greater.than.maximum","Valor del cr�dito superior al m�ximo de \"{0}\" ");
		mensagens_esVE.put("error.validation.item.coverage.invalid", "Cobertura inv�lida o con error!");
		mensagens_esVE.put("error.validation.item.coverage.absent", "No hay cobertura en el �tem!");
		mensagens_esVE.put("error.validation.item.coverage.basic.absent", "No hay cobertura b�sica en el �tem!");
		mensagens_esVE.put("error.validation.productid.invalid", "Producto inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.coverageid.invalid", "Cobertura inv�lida o inexistente!");
		mensagens_esVE.put("error.validation.coverage.value.invalid", "La suma asegurada de la cobertura \"{1} {0}\" debe ser informada!");
		mensagens_esVE.put("error.validation.product.plan.coverage.invalid", "Cobertura inv�lida para el producto e/o plan!");
		mensagens_esVE.put("error.validation.product.plan.coverage.value.rangeid.invalid", "Rango de valor inv�lido o inexistente para la cobertura \"{1} {0}\" ");
		mensagens_esVE.put("error.validation.date.out.range", "Fecha de nacimiento fuera del rango configurado en el producto!");
		mensagens_esVE.put("error.validation.contract.policytype.invalid", "Tipo de policy type inv�lido");
		mensagens_esVE.put("error.validation.contract.policyinformed.disparity", "Hay diferencias de Policy Informed");
		mensagens_esVE.put("error.processing.error", "Error de procesamiento");
		mensagens_esVE.put("error.processing.error.factor.absent", "Sin factor");
		mensagens_esVE.put("error.processing.maxpolicy", "El asegurado ya tiene el l�mite m�ximo de \"{0}\" p�lizas de este producto");
		mensagens_esVE.put("error.validation.coverage.value.less.than.minimum","La suma asegurada de la cobertura \"{3} {2}\" a contratar debe ser mayor a \"{0}\" " );
		mensagens_esVE.put("error.validation.coverage.value.greater.than.maximum", "La suma asegurada de la cobertura \"{3} {2}\" a contratar debe ser menor a \"{0}\" ");
		mensagens_esVE.put("error.validation.risk.value.less.than.minimum","Suma asegura del riesgo de \"{1}\" inferior al minimo de \"{0}\" ");
		mensagens_esVE.put("error.validation.risk.value.greater.than.maximum","Suma asegura del riesgo de \"{1}\" superior al maximo de \"{0}\" " );
		mensagens_esVE.put("error.validation.policy.sum.greater.than.maximum", "Suma de ap�lizas superior al m�ximo de \"{0}\" por persona");
		mensagens_esVE.put("error.validation.policy.number.greater.than.maximum", "�El asegurado ya tiene el l�mite de \"{0}\" p�lizas del producto \"{1}\"!");
		mensagens_esVE.put("error.validation.insured.age.out.of.range", "�La edad del {0} \"{1}\" se encuentra fuera del rango establecido ({2})!");
		mensagens_esVE.put("error.processing.rate.not.found", "Encuadramiento del factor \"{0}\" sin tasa para la cobertura \"{1}\"");
		mensagens_esVE.put("error.validation.commission.factor.less.than.minimum","Factor de comisi�n inferior al minimo de \"{0}\" ");
		mensagens_esVE.put("error.validation.commission.factor.greater.than.maximum","Factor de comisi�n de superior al maximo de \"{0}\" ");
		mensagens_esVE.put("error.validation.endorsement.paymenttermid.invalid", "Forma de pago inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.endorsement.paymenttermid.disparity", "Forma de pago inv�lido o inexistente para o producto!");
		mensagens_esVE.put("error.validation.policy.change.register", "No es posible realizar la alteraci�n registral de la p�liza!");
		mensagens_esVE.put("error.validation.policy.change.technical", "No es posible realizar la alteraci�n t�cnica de la p�liza!");
		mensagens_esVE.put("error.validation.policy.change.claim", "No es permitido alterar la p�liza, debido la existencia de siniestro!");
		mensagens_esVE.put("error.validation.policy.cancel.claim", "No es permitido anular la p�liza, debido la existencia de siniestro!");
		mensagens_esVE.put("error.validation.insured.address.absent", "No fue informado la dirrecci�n del asegurado!");
		mensagens_esVE.put("error.validation.policy.holder.address.absent", "No fue informado la dirrecci�n del tomador!");
		
		mensagens_esVE.put("error.validation.deducible.attributes.not.ok", "Atributos para o Deducible n�o est�o ok! Franquia:{0}, Cobertura:{1}");
		mensagens_esVE.put("error.validation.coverage.attributes.not.ok", "Atributos para o Coverage n�o est�o ok! Cobertura: {0}");
		mensagens_esVE.put("error.validation.profile.attributes.not.ok", "Atributos para o Profile n�o est�o Ok! Pergunta:{0}, Resposta: {1}");
		mensagens_esVE.put("error.validation.item.attributes.not.ok", "Atributos para o Item n�o est�o Ok!");
		mensagens_esVE.put("error.validation.installment.attributes.not.ok", "Valida��o de Forma de Pagamento n�o Ok. {0}");
		
		mensagens_esVE.put("error.validation.item.profile.invalid", "Perfil inv�lido o con error!");
		mensagens_esVE.put("error.validation.payment.option.invalid", "Medio de Pago inv�lido o con error!");
		mensagens_esVE.put("error.validation.payment.option.absent", "No hay medio de pago en la emisi�n!");
		mensagens_esVE.put("error.validation.ruletype.coverage.blocked", "La cobertura \"{1} {0}\" esta fuera de las reglas de aceptaci�n!");
		mensagens_esVE.put("error.validation.ruletype.insuredvalue.blocked", "La cobertura \"{2} {1}\" con suma asegura de \"{0}\" fuera de los lim�tes de aceptaci�n!");
		mensagens_esVE.put("error.validation.ruletype.deductible.blocked", "La cobertura \"{2} {1}\" con deducible \"{0}\" fuera de las reglas de aceptaci�n!");
		mensagens_esVE.put("error.validation.ruletype.profile.blocked", "La pregunta \"{0}\" con la respuesta \"{1}\" esta fuera de las reglas de aceptaci�n!");
		mensagens_esVE.put("error.validation.ruletype.payment.term.blocked", "Medio de pago \"{0}\" fuera de las reglas de aceptaci�n!");
		mensagens_esVE.put("error.validation.ruletype.payment.type.blocked", "Tipo de pago \"{0}\" fuera de las reglas de aceptaci�n!");
		mensagens_esVE.put("error.validation.coverage.mutually.exclusive.ok", "La cobertura \"{0}\" es mutuamente exclusiva con la cobertura \"{1}\"!");
		mensagens_esVE.put("error.validation.policy.object.risk.found", "�Este objeto de riesgo ya se encuentra asegurado!");
		
		mensagens_esVE.put("error.validation.item.property.state.absent", "No fue informado el estado del riesgo!");
		mensagens_esVE.put("error.validation.item.property.city.absent", "No fue informada la ciudad del riesgo!");
		mensagens_esVE.put("error.validation.item.property.district.absent", "No fue informada la urbanizaci�n del riesgo");
		mensagens_esVE.put("error.validation.item.property.street.absent", "No fue informada la dirrecci�n del riesgo!");
		
		mensagens_esVE.put("error.validation.item.property.risktype.invalid", "Tipo de propiedad/riesgo inv�lido!");
		
		mensagens_esVE.put("error.validation.coverage.dependence.and", "La cobertura \"{0}\" tiene dependencia \"Y\" con la cobertura \"{1}\"!");
		mensagens_esVE.put("error.validation.coverage.dependence.or", "La cobertura \"{0}\" tiene dependencia \"O\" con la(s) cobertura(s): {1}!");
		mensagens_esVE.put("error.validation.policy.renewal.invalid", "El certificado \"{1}\" de la p�liza \"{0}\" ya fue renovado!");
		
		mensagens_esVE.put("error.validation.person.type.invalid", "Tipo de persona inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.gender.invalid", "Tipo de persona inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.marital.status.invalid", "Tipo de estado civil inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.document.type.invalid", "Tipo de documento inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.occupation.invalid", "C�digo de ocupaci�n inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.activity.invalid", "C�digo de actividad econ�mica inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.profession.invalid", "C�digo de profesi�n es inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.profession.type.invalid", "C�digo de tipo de profesi�n es inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.inflow.invalid", "C�digo de capacidad econ�mica es inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.restriction.risk.type.invalid", "C�digo de tipo de restricci�n del riesgo es inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.contract.masterpolicy.disparity", "Hay diferencias de p�liza maestra");
		
		mensagens_esVE.put("error.validation.contractid.required", "El numero de contrato es necesario");
		mensagens_esVE.put("error.validation.policynumber.required", "El numero de p�liza es necesario");
		mensagens_esVE.put("error.validation.certificatenumber.required", "El numero del certificado es necesario");
		mensagens_esVE.put("error.validation.documenttype.required", "El numero de tipo de documento es necesario");
		mensagens_esVE.put("error.validation.documentnumber.required", "El numero de documento es necesario");
		
		mensagens_esVE.put("error.validation.phone.type.invalid", "Tipo de tel�fono es inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.address.type.invalid", "Tipo de direcci�n es inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.country.id.invalid", "Pa�s es inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.state.id.invalid", "Estado es inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.city.id.invalid", "Ciudad es inv�lida o inexistente!");
		
		mensagens_esVE.put("error.validation.item.out.of.sequence", "Item fuera de secuencia!");
		mensagens_esVE.put("error.validation.endorsement.not.found", "Emisi�n y/o endoso es inv�lido o inexistente!");
		
		mensagens_esVE.put("error.validation.bank.account.number.invalid", "N�mero de la cuenta es inv�lida o no informada!");
		mensagens_esVE.put("error.validation.bank.check.number.invalid", "N�mero de cheque es inv�lido o no informado!");
		mensagens_esVE.put("error.validation.card.number.invalid", "N�mero de la tarjeta es inv�lida o no informada!");
		mensagens_esVE.put("error.validation.card.expiration.date.invalid", "Vencimiento de la tarjeta es inv�lida o no informada!");
		mensagens_esVE.put("error.validation.card.holder.name.absent", "Nombre del titular de la tarjeta no informado!");
		mensagens_esVE.put("error.validation.other.account.number.invalid", "N�mero de externo/otros es inv�lido o no informado!");
		
		mensagens_esVE.put("error.validation.beneficiary.out.of.sequence", "Beneficiario fuera de secuencia!");
		mensagens_esVE.put("error.validation.beneficiary.participation.disparity", "Porcentual de participaci�n de beneficiario(s) difiere del 100%!");
		
		mensagens_esVE.put("error.validation.risk.group.out.of.sequence", "Grupo familiar fuera de secuencia!");
		mensagens_esVE.put("error.validation.record.not.found", "Registro no localizado o inexistente!");
		mensagens_esVE.put("error.validation.cancel.reason.invalid", "Motivo de anulaci�n es inv�lida o inexistente!");
		mensagens_esVE.put("error.validation.proposal.unlock.not.allowed", "Desbloqueo de propuesta no permitido!");
		mensagens_esVE.put("error.validation.issuance.type.not.allowed", "Tipo de emisi�n/endoso no permitido!");
		mensagens_esVE.put("error.validation.print.not.allowed", "Impresi�n no permitida!");
		mensagens_esVE.put("error.validation.installment.status.invalid", "Estatus de la cuota es inv�lido o inexistente!");
		mensagens_esVE.put("error.validation.phone.not.found", "Tel�fono no informado para la dirrecci�n!");
		mensagens_esVE.put("error.validation.action.is.not.allowed", "Acci�n no permitida!");
		
		mensagens_esVE.put("error.validation.card.type.invalid", "Tipo de la tarjeta es inv�lida o no informada!");
		mensagens_esVE.put("error.validation.card.brand.type.invalid", "Bandera de la tarjeta es inv�lida o no informada!");
		
		mensagens_esVE.put("error.validation.property.activity.type.invalid", "Tipo de actividad empresarial es inv�lido o no informado!");
		mensagens_esVE.put("error.validation.property.risk.not.found", "Riesgo(s) asegurado inv�lido o no informado!");
		
		mensagens_esVE.put("error.validation.policy.cancel.not.allowed", "No es posible anular la p�liza!");
		mensagens_esVE.put("error.validation.policy.reactivation.not.allowed", "No es permitido reactivar la poliza!");
		
		mensagens_esVE.put("error.validation.coverage.compulsory.not.found", "La cobertura \"{1} {0}\" debe ser contratada!");		
	}
	
	@SuppressWarnings("unused")
	private static void msg_ptBR(HashMap<String, String>  mensagens_ptBR) {
		mensagens_ptBR.put("error.validation.contract.invalid", "Contrato inv�lido ou com erro!");
		mensagens_ptBR.put("error.validation.contract.productversion.invalid", "Vers�o do produto inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.contract.masterpolicy.invalid", "Ap�lice m�e inv�lida ou inexistente!");
		mensagens_ptBR.put("error.validation.contract.insurerid.invalid", "Seguradora inv�lida ou inexistente!");
		mensagens_ptBR.put("error.validation.contract.subsidiaryid.invalid", "Sucursal inv�lida ou inexistente!");
		mensagens_ptBR.put("error.validation.contract.subsidiary.disparity", "H� diferen�as de sucursal!");
		mensagens_ptBR.put("error.validation.contract.channelid.invalid", "Canal de venda inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.contract.brokerid.invalid", "Corretor inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.contract.brokerid.disparity", "H� diferen�as de corretor!");
		mensagens_ptBR.put("error.validation.contract.partnerid.invalid", "Aliado inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.contract.partnerid.disparity", "H� diferen�as de aliado!");
		mensagens_ptBR.put("error.validation.contract.policynumber.disparity", "H� diferen�as de n�mero de ap�lice!");
		mensagens_ptBR.put("error.validation.endorsement.invalid", "Endosso inv�lido ou com erro!");
		mensagens_ptBR.put("error.validation.endorsement.referencedate.invalid", "Data de refer�ncia inv�lida!");
		mensagens_ptBR.put("error.validation.endorsement.effectivedate.invalid", "Data de in�cio de vig�ncia inv�lida!");
		mensagens_ptBR.put("error.validation.endorsement.expirydate.invalid", "Data de fim de vig�ncia inv�lida!");
		mensagens_ptBR.put("error.validation.endorsement.termid.invalid", "Prazo de vig�ncia inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.endorsement.termid.disparity", "Prazo de vig�ncia inv�lido ou inexistente para o produto!");
		mensagens_ptBR.put("error.validation.item.absent", "N�o h� �tem na emisi�n!");
		mensagens_ptBR.put("error.validation.item.invalid", "Item inv�lido ou com erro!");
		mensagens_ptBR.put("error.validation.item.personal.birthdate.invalid", "Data de nascimento inv�lido!");
		mensagens_ptBR.put("error.validation.item.personal.risktype.invalid", "Tipo de risco inv�lido!");
		mensagens_ptBR.put("error.validation.item.personal.kinshiptype.invalid", "Tipo de parentesco inv�lido!");
		mensagens_ptBR.put("error.validation.item.credit.value.out.of.range", "Valor do cr�dito fora da faixa");
		mensagens_ptBR.put("error.validation.item.credit.value.less.than.minimum","Valor do cr�dito abaixo do m�nimo de \"{0}\" ");
		mensagens_ptBR.put("error.validation.item.credit.value.greater.than.maximum","Valor do cr�dito acima do m�ximo de \"{0}\" ");
		mensagens_ptBR.put("error.validation.item.coverage.invalid", "Cobertura inv�lida ou com erro!");
		mensagens_ptBR.put("error.validation.item.coverage.absent", "N�o h� cobertura no �tem!");
		mensagens_ptBR.put("error.validation.item.coverage.basic.absent", "N�o h� cobertura b�sica no �tem!");
		mensagens_ptBR.put("error.validation.productid.invalid", "Produto inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.coverageid.invalid", "Cobertura inv�lida ou inexistente!");
		mensagens_ptBR.put("error.validation.coverage.value.invalid", "L.M.I da cobertura \"{1} {0}\" deve ser informada!");
		mensagens_ptBR.put("error.validation.product.plan.coverage.invalid", "Cobertura inv�lida para o produto e/ou plano!");
		mensagens_ptBR.put("error.validation.product.plan.coverage.value.rangeid.invalid", "Faixa de valor inv�lido ou inexistente para a cobertura \"{1} {0}\" ");
		mensagens_ptBR.put("error.validation.date.out.range", "Data de nascimento fora da faixa configurada para o produto!");
		mensagens_ptBR.put("error.validation.contract.policytype.invalid", "Tipo de ap�lice inv�lido");
		mensagens_ptBR.put("error.validation.contract.policyinformed.disparity", "H� diferen�as de ap�lice informada");
		mensagens_ptBR.put("error.processing.error", "Erro de processamento");
		mensagens_ptBR.put("error.processing.error.factor.absent", "Sem fator");
		mensagens_ptBR.put("error.processing.maxpolicy", "O segurado j� tem o l�mite m�ximo de \"{0}\" ap�lices deste produto");
		mensagens_ptBR.put("error.validation.coverage.value.less.than.minimum","L.M.I da cobertura \"{3} {2}\" a contratar deve ser superior a \"{0}\" " );
		mensagens_ptBR.put("error.validation.coverage.value.greater.than.maximum", "L.M.I da cobertura \"{3} {2}\" a contratar deve ser inferior a \"{0}\" ");
		mensagens_ptBR.put("error.validation.risk.value.less.than.minimum","Valor do risco de \"{1}\" inferior ao m�nimo de \"{0}\" ");
		mensagens_ptBR.put("error.validation.risk.value.greater.than.maximum","Valor do risco de \"{1}\" superior ao m�ximo de \"{0}\" " );
		mensagens_ptBR.put("error.validation.policy.sum.greater.than.maximum", "Lim�te segurado superior ao m�ximo de \"{0}\" por pessoa");
		mensagens_ptBR.put("error.validation.policy.number.greater.than.maximum", "O segurado j� tem o lim�te de \"{0}\" ap�lices do producto \"{1}\"!");
		mensagens_ptBR.put("error.validation.insured.age.out.of.range", "A idade do {0} \"{1}\" encontra-se fora da faixa estabelecida ({2})!");
		mensagens_ptBR.put("error.processing.rate.not.found", "Enquadramento do fator \"{0}\" sem taxa para a cobertura \"{1}\"");
		mensagens_ptBR.put("error.validation.commission.factor.less.than.minimum","Fator de comiss�o inferior ao m�nimo de \"{0}\" ");
		mensagens_ptBR.put("error.validation.commission.factor.greater.than.maximum","Fator de comiss�o superior ao m�ximo de \"{0}\" ");
		mensagens_ptBR.put("error.validation.endorsement.paymenttermid.invalid", "Forma de pagto inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.endorsement.paymenttermid.disparity", "Forma de pagto inv�lido ou inexistente para o produto!");
		mensagens_ptBR.put("error.validation.policy.change.register", "N�o � poss�vel realizar a altera��o cadastral da ap�lice!");
		mensagens_ptBR.put("error.validation.policy.change.technical", "N�o � poss�vel realizar a altera��o t�cnica da p�lice!");
		mensagens_ptBR.put("error.validation.policy.change.claim", "N�o � permitido alterar a ap�lice, devido a exist�ncia de sinistro!");
		mensagens_ptBR.put("error.validation.policy.cancel.claim", "N�o � permitido anular a ap�lice, devido a exist�ncia de sinistro!");
		mensagens_ptBR.put("error.validation.insured.address.absent", "N�o foi informado o endere�o do segurado!");
		mensagens_ptBR.put("error.validation.policy.holder.address.absent", "N�o foi informado o endere�o do tomador!");
		
		mensagens_ptBR.put("error.validation.deducible.attributes.not.ok", "Atributos para o dedut�veis n�o est�o ok! Franquia:{0}, Cobertura:{1}");
		mensagens_ptBR.put("error.validation.coverage.attributes.not.ok", "Atributos para as coberturas n�o est�o ok! Cobertura: {0}");
		mensagens_ptBR.put("error.validation.profile.attributes.not.ok", "Atributos para o perfil n�o est�o Ok! Pergunta:{0}, Resposta: {1}");
		mensagens_ptBR.put("error.validation.item.attributes.not.ok", "Atributos para o grupo n�o est�o Ok!");
		mensagens_ptBR.put("error.validation.installment.attributes.not.ok", "Valida��o de Forma de Pagamento n�o Ok. {0}");
		
		mensagens_ptBR.put("error.validation.item.profile.invalid", "Perfil inv�lido ou com erro!");
		mensagens_ptBR.put("error.validation.payment.option.invalid", "Forma de pagamento inv�lida ou com erro!");
		mensagens_ptBR.put("error.validation.payment.option.absent", "Forma de pagamento n�o informada para a emiss�o!");
		mensagens_ptBR.put("error.validation.ruletype.coverage.blocked", "A cobertura \"{1} {0}\" est� fora das regras da seguradora!");
		mensagens_ptBR.put("error.validation.ruletype.insuredvalue.blocked", "A cobertura \"{2} {1}\" com L.M.I de \"{0}\" fora dos limites permitidos pela seguradora!");
		mensagens_ptBR.put("error.validation.ruletype.deductible.blocked", "A cobertura \"{2} {1}\" com franquia \"{0}\" fora das regras da seguradora!");
		mensagens_ptBR.put("error.validation.ruletype.profile.blocked", "A pergunta \"{0}\" com a resposta \"{1}\" est� fora das regras da seguradora!");
		mensagens_ptBR.put("error.validation.ruletype.payment.term.blocked", "Forma de pagamento \"{0}\" fora das regras da seguradora!");
		mensagens_ptBR.put("error.validation.ruletype.payment.type.blocked", "Tipo de pagamento \"{0}\" fora das regras da seguradora!");
		mensagens_ptBR.put("error.validation.coverage.mutually.exclusive.ok", "A cobertura \"{0}\" � mutuamente exclusiva com a cobertura \"{1}\"!");
		mensagens_ptBR.put("error.validation.policy.object.risk.found", "Este objeto de risco j� esta segurado!");
		
		mensagens_ptBR.put("error.validation.item.property.state.absent", "N�o foi informado o estado do risco!");
		mensagens_ptBR.put("error.validation.item.property.city.absent", "N�o foi informada a cidade do risco!");
		mensagens_ptBR.put("error.validation.item.property.district.absent", "N�o foi informado o bairro do risco!");
		mensagens_ptBR.put("error.validation.item.property.street.absent", "N�o foi informado o logradouro do risco!");
		
		mensagens_ptBR.put("error.validation.item.property.risktype.invalid", "Tipo de risco inv�lido!");
		
		mensagens_ptBR.put("error.validation.coverage.dependence.and", "A cobertura \"{0}\" tem depend�ncia \"E\" com a cobertura \"{1}\"!");
		mensagens_ptBR.put("error.validation.coverage.dependence.or", "A cobertura \"{0}\" tem depend�ncia \"OU\" com a(s) cobertura(s): {1}!");
		mensagens_ptBR.put("error.validation.policy.renewal.invalid", "O certificado \"{1}\" da ap�lice \"{0}\" ja foi renovado!");

		mensagens_ptBR.put("error.validation.person.type.invalid", "Tipo de pessoa inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.gender.invalid", "Tipo de pessoa inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.marital.status.invalid", "Tipo de estado civil inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.document.type.invalid", "Tipo de documento inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.occupation.invalid", "C�digo de ocupacao inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.activity.invalid", "C�digo de atividade econ�mica inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.profession.invalid", "C�digo de profiss�o inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.profession.type.invalid", "C�digo de tipo de profiss�o inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.inflow.invalid", "C�digo de capacidade econ�mica inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.restriction.risk.type.invalid", "C�digo de tipo de restri��o do risco inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.contract.masterpolicy.disparity", "H� diferen�as na Ap�olice m�e!");
		
		mensagens_ptBR.put("error.validation.contractid.required", "O n�mero do contrato � requerido");
		mensagens_ptBR.put("error.validation.policynumber.required", "O n�mero da ap�lice � requerido");
		mensagens_ptBR.put("error.validation.certificatenumber.required", "O n�mero do certificado � requerido");
		mensagens_ptBR.put("error.validation.documenttype.required", "O n�mero do tipo de documento � requerido");
		mensagens_ptBR.put("error.validation.documentnumber.required", "O n�mero do documento � requerido");
		
		mensagens_ptBR.put("error.validation.phone.type.invalid", "Tipo de telefone inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.address.type.invalid", "Tipo de endere�o inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.country.id.invalid", "Pa�s inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.state.id.invalid", "Estado inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.city.id.invalid", "Cidade inv�lida ou inexistente!");
		
		mensagens_ptBR.put("error.validation.item.out.of.sequence", "Item fora de sequencia!");
		mensagens_ptBR.put("error.validation.endorsement.not.found", "Emiss�o e/ou endosso inv�lida ou inexistente!");
		
		mensagens_ptBR.put("error.validation.bank.account.number.invalid", "N�mero da conta � inv�lido ou n�o informado!");
		mensagens_ptBR.put("error.validation.bank.check.number.invalid", "N�mero do cheque � inv�lido ou n�o informado!");
		mensagens_ptBR.put("error.validation.card.number.invalid", "N�mero do cart�o � inv�lido ou n�o informado!");
		mensagens_ptBR.put("error.validation.card.expiration.date.invalid", "Vencimento do cart�o � inv�lido ou n�o informado!");
		mensagens_ptBR.put("error.validation.card.holder.name.absent", "Nome do titular do cart�o n�o informado!");
		mensagens_ptBR.put("error.validation.other.account.number.invalid", "N�mero de externo/outros � inv�lido ou n�o informado!");
		
		mensagens_ptBR.put("error.validation.beneficiary.out.of.sequence", "Benefici�rio fora de sequencia!");
		mensagens_ptBR.put("error.validation.beneficiary.participation.disparity", "Porcentagem de participa��o de benefici�rio(s) difere de 100%!");
		
		mensagens_ptBR.put("error.validation.risk.group.out.of.sequence", "Grupo familiar fora de sequencia!");
		mensagens_ptBR.put("error.validation.record.not.found", "Registro n�o localizado ou inexistente!");
		mensagens_ptBR.put("error.validation.cancel.reason.invalid", "Motivo de anula��o � inv�lido o inexistente!");
		
		mensagens_ptBR.put("error.validation.proposal.unlock.not.allowed", "Desbloqueio de proposta n�o permitido!");
		mensagens_ptBR.put("error.validation.issuance.type.not.allowed", "Tipo de emiss�o/endoso n�o permitido!");
		mensagens_ptBR.put("error.validation.print.not.allowed", "Impress�o no permitida!");
		mensagens_ptBR.put("error.validation.installment.status.invalid", "Status da parcela � inv�lido ou inexistente!");
		mensagens_ptBR.put("error.validation.phone.not.found", "Telefone n�o informado para o endere�o!");
		mensagens_ptBR.put("error.validation.action.is.not.allowed", "A��o n�o permitida!");
		
		mensagens_ptBR.put("error.validation.card.type.invalid", "Tipo do cart�o � inv�lido ou n�o informado!");
		mensagens_ptBR.put("error.validation.card.brand.type.invalid", "Bandeira � do cart�o inv�lida ou n�o informada!");
		
		mensagens_ptBR.put("error.validation.property.activity.type.invalid", "Tipo de atividade empresarial � inv�lido ou n�o informado!");
		mensagens_ptBR.put("error.validation.property.risk.not.found", "Risco(s) segurado inv�lido ou n�o informado!");
		
		mensagens_ptBR.put("error.validation.policy.cancel.not.allowed", "N�o � poss�vel anular a ap�lice!");
		mensagens_ptBR.put("error.validation.policy.reactivation.not.allowed", "N�o � permitido reativar a ap�lice!");

		mensagens_ptBR.put("error.validation.coverage.compulsory.not.found", "A cobertura \"{1} {0}\" deve ser contratada!");
	}
	
	/**
	 * @return Gets error code
	 */
	public long getErrorCode() {
		return this.errorCode;
	}
	
	/**
	 * @return Return error types
	 */
	public int getErrorType() {
		return this.errorType;
	}

	/**
	 * @return the messageSource
	 */
	public static MessageSource getMessageSource() {
		if ( messageSource == null ) {
			messageSource = AppContextHelper.getApplicationContext();
		}
		return messageSource;
	}
}
