package br.com.tratomais.core.service.erros;

import java.io.Serializable;

/**
 * Representa um Erro
 * @author luiz.alberoni
 *
 */
public class Erro implements Serializable {
	/**
	 * @param codigo
	 * @param descricao
	 * @param local
	 * @param tipoErro
	 * @param origem
	 * @param nrSeq
	 * @param numItem
	 * @param numRiskGroup
	 * @param numCoverage
	 */
	@SuppressWarnings("unchecked")
	public Erro(long codigo, String descricao, Class local, int tipoErro, String origem, int nrSeq, int numItem, int numRiskGroup, int numCoverage) {
		super();
		this.codigo = codigo;
		this.descricao = descricao;
		this.local = local;
		this.tipoErro = tipoErro;
		this.origem = origem;
		this.nrSeq = nrSeq;
		this.numItem = numItem;
		this.numRiskGroup = numRiskGroup;
		this.numCoverage = numCoverage;
	}
	
	/**
	 * @param codigo
	 * @param descricao
	 * @param local
	 * @param tipoErro
	 * @param origem
	 * @param nrSeq
	 * @param numItem
	 * @param numRiskGroup
	 */
	@SuppressWarnings("unchecked")
	public Erro(long codigo, String descricao, Class local, int tipoErro, String origem, int nrSeq, int numItem, int numRiskGroup) {
		super();
		this.codigo = codigo;
		this.descricao = descricao;
		this.local = local;
		this.tipoErro = tipoErro;
		this.origem = origem;
		this.nrSeq = nrSeq;
		this.numItem = numItem;
		this.numRiskGroup = numRiskGroup;
	}
	
	/**
	 * @param codigo
	 * @param descricao
	 * @param local
	 * @param tipoErro
	 * @param origem
	 * @param nrSeq
	 * @param numItem
	 */	
	@SuppressWarnings("unchecked")
	public Erro(long codigo, String descricao, Class local, int tipoErro, String origem, int nrSeq, int numItem) {
		super();
		this.codigo = codigo;
		this.descricao = descricao;
		this.local = local;
		this.tipoErro = tipoErro;
		this.origem = origem;
		this.nrSeq = nrSeq;
		this.numItem = numItem;
	}
	
	/**
	 * @param codigo
	 * @param descricao
	 * @param local
	 * @param tipoErro
	 * @param origem
	 * @param nrSeq
	 */	
	@SuppressWarnings("unchecked")
	public Erro(long codigo, String descricao, Class local, int tipoErro, String origem, int nrSeq) {
		super();
		this.codigo = codigo;
		this.descricao = descricao;
		this.local = local;
		this.tipoErro = tipoErro;
		this.origem = origem;
		this.nrSeq = nrSeq;
	}
	
	Erro() {}
	/**
	 * 
	 */
	private static final long serialVersionUID = -3233203278771534141L;
	private long codigo;
	private String descricao;
	@SuppressWarnings("unchecked")
	private Class local;
	private int tipoErro;
	private String origem;
	private int nrSeq;
	private int numItem;
	private int numRiskGroup;
	private int numCoverage;
	
	/**
	 * @return the codigo
	 */
	public long getCodigo() {
		return codigo;
	}
	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}
	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/**
	 * @return the local
	 */
	@SuppressWarnings("unchecked")
	public Class getLocal() {
		return local;
	}
	/**
	 * @param local the local to set
	 */
	@SuppressWarnings("unchecked")
	public void setLocal(Class local) {
		this.local = local;
	}
	/**
	 * @return the tipoErro
	 */
	public int getTipoErro() {
		return tipoErro;
	}
	/**
	 * @param tipoErro the tipoErro to set
	 */
	public void setTipoErro(int tipoErro) {
		this.tipoErro = tipoErro;
	}
	/**
	 * @return the origem
	 */
	public String getOrigem() {
		return origem;
	}
	/**
	 * @param origem the origem to set
	 */
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	/**
	 * @return the nrSeq
	 */
	public int getNrSeq() {
		return nrSeq;
	}
	/**
	 * @param nrSeq the nrSeq to set
	 */
	public void setNrSeq(int nrSeq) {
		this.nrSeq = nrSeq;
	}
	/**
	 * @return the numItem
	 */
	public int getNumItem() {
		return numItem;
	}
	/**
	 * @param numItem the numItem to set
	 */
	public void setNumItem(int numItem) {
		this.numItem = numItem;
	}
	/**
	 * @return the numRiskGroup
	 */
	public int getNumRiskGroup() {
		return numRiskGroup;
	}
	/**
	 * @param numRiskGroup the numRiskGroup to set
	 */
	public void setNumRiskGroup(int numRiskGroup) {
		this.numRiskGroup = numRiskGroup;
	}
	/**
	 * @return the numCoverage
	 */
	public int getNumCoverage() {
		return numCoverage;
	}
	/**
	 * @param numCoverage the numCoverage to set
	 */
	public void setNumCoverage(int numCoverage) {
		this.numCoverage = numCoverage;
	}
}
