package br.com.tratomais.general.utilities;

import java.io.File;

public class FileSystemUtils {
	public static final void kdir(String file){
		kdir(new File(file));
	}
	
	public static final void kdir(File file){
		if (file.exists()){
			for(File arquivo: file.listFiles()){
				if(	( arquivo.isDirectory() ) &&
						( (! ".".equals(arquivo.getName())) ||
						 (! "..".equals(arquivo.getName()))) 
						){
					kdir(arquivo);
				}
				else
					arquivo.delete();
			}
		}
		file.delete();
	}
}
