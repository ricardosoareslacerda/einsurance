package br.com.tratomais.general.utilities;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.regex.Pattern;

import javax.swing.text.MaskFormatter;

public class StringUtilities {
	public static boolean isNumber(String string){
		for(int x=0 ; x<string.length(); x++){
			if ("0123456789.,".indexOf("" + string.charAt(x)) >=0 )
				return false;
		}
		return true;
	}
	
	public static String getHashSha(String msg)  {

		MessageDigest md;
		StringBuffer msgRet = new StringBuffer() ;
		try {
	        md = MessageDigest.getInstance("sha");
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalArgumentException();
			//throw new SelogException("Erro ao buscar o algoritmo de criptografia MD5 \n" + e.getMessage());			// TODO: handle exception
		}

        md.update(msg.getBytes());

        byte[] msgHash = md.digest();
        
        for (int i = 0; i < msgHash.length; i++) {
			msgRet.append( new String("0" + Integer.toHexString(msgHash[i])).substring(Integer.toHexString(msgHash[i]).length()-1) );
		}
        
		return  msgRet.toString() ;
	}

	/**
     * Creates a string with the size given, filled with a specific content and 
     * aligned on right or left.
     */
    public static String fill(String content, String fillChar, int size, boolean isRightAlign) {
    	String results = "";
    	String addContent = "";
    	if (content == null) {
			content="";
		}
   		
    	for (int k=0; k<size - content.length(); k++) {
   			addContent += String.valueOf(fillChar.charAt(0));
   		} 
   		
    	if (isRightAlign) {
    		results = addContent + content;    		
    	} else {
    		results = content + addContent;
    	}
    	
		results = results.substring(0, size);

		return results;
    }

    public static String parseNull(String inputValue) {
		if (inputValue != null)
			return inputValue.trim();
		return "";
	}

	public static String parseNullOrOtherValue(String inputValue, String otherValue) {
		if (inputValue != null && !inputValue.trim().equalsIgnoreCase(""))
			return inputValue.trim();
		return (otherValue != null ? otherValue : "");
	}

	public static Boolean validateYasudaPI(String externalCodePI) {
		if(externalCodePI == null || externalCodePI.length() != 11 || !Pattern.compile("[0-9]+").matcher(externalCodePI.substring(0, 10)).matches()) {
			return false;
		}
		String digit = externalCodePI.substring(10, 11);
		externalCodePI = externalCodePI.substring(0, 10);
		
		int lPeso = 2;
		int lTotal = 0;
	
		//3. Aplica pesos.
		for(int i = 0; i < externalCodePI.length() ; i++){
			int lAuxCalculo = Integer.parseInt(Character.toString(externalCodePI.charAt(externalCodePI.length() - 1 - i)));
			if(i % 6 == 0){
				lPeso = 2;
			}else{
				lPeso++;
			}
			String lResultado = new DecimalFormat("00").format(lAuxCalculo * lPeso); //Resultado.
			lTotal += Integer.parseInt(Character.toString(lResultado.charAt(0))) + Integer.parseInt(Character.toString(lResultado.charAt(1)));
		}
	
		//4. Divide resultado por 11 e obt�m resto.
		int lResto = lTotal % 11; //Resto.
		String lDigit;
	
	    //5. Posiciona d�gito.
	    switch(lResto){
			case 0:
				lDigit = "A";
				break;
			case 1:
				lDigit = "0";
				break;
			default :
				lDigit = String.valueOf(new DecimalFormat("0").format(11 - lResto));
				break;
	    }
	    return digit.equalsIgnoreCase(lDigit);
	}

    /**
     * <p>Checks if a String is whitespace, empty ("") or null.</p>
     *
     * <pre>
     * StringUtils.isBlank(null)      = true
     * StringUtils.isBlank("")        = true
     * StringUtils.isBlank(" ")       = true
     * StringUtils.isBlank("bob")     = false
     * StringUtils.isBlank("  bob  ") = false
     * </pre>
     *
     * @param str  the String to check, may be null
     * @return <code>true</code> if the String is null, empty or whitespace
     * @since 2.0
     */
    public static boolean isBlank(String str) {
        int strLen;
        if (str == null || (strLen = str.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if ((Character.isWhitespace(str.charAt(i)) == false)) {
                return false;
            }
        }
        return true;
    }

    /**
     * <p>Checks if a String is not empty (""), not null and not whitespace only.</p>
     *
     * <pre>
     * StringUtils.isNotBlank(null)      = false
     * StringUtils.isNotBlank("")        = false
     * StringUtils.isNotBlank(" ")       = false
     * StringUtils.isNotBlank("bob")     = true
     * StringUtils.isNotBlank("  bob  ") = true
     * </pre>
     *
     * @param str  the String to check, may be null
     * @return <code>true</code> if the String is
     *  not empty and not null and not whitespace
     * @since 2.0
     */
    public static boolean isNotBlank(String str) {
        return !isBlank(str);
    }

	/**
	 * Strips out non-numeric characters
	 * @param texto
	 * @return
	 */
	public static final String ignoreNoNumerics(String texto){
		if (texto != null) {
			Pattern pattern = Pattern.compile("[^\\d]*");
			return pattern.matcher(texto).replaceAll("").trim();
		}
		else return "";
	}

    public static boolean isOnlyText(String string){
    	return Pattern.compile("[^0-9]+").matcher(string).matches();
	}

    /**
     * Efetua a formata��o do c�digo de atividade
     * @param code String c�digo de atividade
     * @return String c�digo formatado
     */
    public static String formatActivityCode(String code) {
    	if (isNotBlank(code)) {
    		if (code.length() > 5) {
    			code = format("##.##-#/##", code);
    		}
    		else if (code.length() == 5) {
    			code = format("##.##-#", code);
    		}
    		else if (code.length() == 3) {
    			code = format("##.#", code);
    		}
    	}
    	return code;
    }

	/**
	 * Formatador em String do valor com o padr�o passado
	 * @param pattern String
	 * @param value Object
	 * @return String
	 */
	public static String format(String pattern, Object value) {
    	MaskFormatter mask;
    	try {
	    	mask = new MaskFormatter(pattern);
	    	mask.setValueContainsLiteralCharacters(false);
	    	return mask.valueToString(value);
	    } catch (ParseException e) {
	    	throw new RuntimeException(e);
	    }
    }
}