package br.com.tratomais.general.utilities;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Hashtable;
import java.util.Map;

/**
 * Utilit�rios em torno de beans
 * @author Luiz
 */
public class BeanUtilities {
	/**
	 * Realiza c�pia entre dois beans
	 * @param source Dados de origem
	 * @param target Dados de destino
	 * @param ignoreProperties Propriedades a serem ignoradas
	 * @param equivalenceTable Tabela de equivalencia de nomes origem/destino
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	public static void copyBean(Object source, Object target, String [] ignoreProperties, Map<String, String> equivalenceTable ) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Class<?> sourceClass = source.getClass(),
				 targetClass = target.getClass();
		for(Method getter:sourceClass.getMethods()){
			if(getter.getName().matches("^(get|is)[A-Z].*")){
				if ("getClass".equals(getter.getName()))
					continue;
				String parteNomeCampo = getter.getName().startsWith("is")?getter.getName().substring(2):getter.getName().substring(3); 
				char[] charArray = parteNomeCampo.toCharArray();
				charArray[0] = Character.toLowerCase(charArray[0]);
				String nomeCampo = new String(charArray);
				if (! ignorar(ignoreProperties, nomeCampo)){
					if( equivalenceTable.keySet().contains(nomeCampo)){
						charArray = equivalenceTable.get(nomeCampo).toCharArray();
						charArray[0] = Character.toUpperCase(charArray[0]);
						parteNomeCampo = new String(charArray);
					}
					Method setter = targetClass.getDeclaredMethod("set"+parteNomeCampo, getter.getReturnType());
					setter.invoke(target, getter.invoke(source));
				}
			} 
		}
	}
	
	/**
	 * Apenas verifica se a string se encontra dentro do array
	 * @param ignoreProperties lista dos valores 
	 * @param nomeCampo Valos procurado
	 * @return se se encontra no array
	 */
	private static boolean ignorar(String[] ignoreProperties, String nomeCampo) {
		if (ignoreProperties != null)
			for(String nome: ignoreProperties)
				if(nomeCampo.equals(nome))
					return true;
		return false;
	}

	/**
	 * Realiza c�pia entre dois beans
	 * @param source Dados de origem
	 * @param target Dados de destino
	 * @param ignoreProperties Propriedades a serem ignoradas
	 * @param equivalenceTable Tabela de equivalencia de nomes origem/destino. Cada elemento deve ser formado pela igualdade campoOrigem=campoDestino. Exemplo: new String[]{"genero=sexo"}
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	public static void copyBean(Object source, Object target, String [] ignoreProperties, String[] equivalenceTable ) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Hashtable<String, String> hashtable = new Hashtable<String, String>();
		for(String equivalence: equivalenceTable){
			if("".equals(equivalence))
				continue;
			String[] split = equivalence.split("=");
			hashtable.put(split[0].trim(), split[1].trim());
		}
		copyBean(source, target, ignoreProperties, hashtable);
	}

	/**
	 * Realiza c�pia entre dois beans
	 * @param source Dados de origem
	 * @param target Dados de destino
	 * @param ignoreProperties Propriedades a serem ignoradas
	 * @param equivalenceTable Tabela de equivalencia de nomes origem/destino. Cada elemento deve ser formado pela igualdade campoOrigem=campoDestino, separados por ';'. Exemplo: "genero=sexo;rua=endereco"
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	public static void copyBean(Object source, Object target, String [] ignoreProperties, String equivalenceTable ) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		String[] split = equivalenceTable.split(";");
		for(int x=0; x<split.length; x++)
			split[x] = split[x].trim();
		copyBean(source, target, ignoreProperties, split);
	}
}
