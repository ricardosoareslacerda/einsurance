package br.com.tratomais.general.utilities;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

/**
 * Operations with Numbers
 * @author luiz.alberoni
 */
public class NumberUtilities {

	/**
	 * Divides the value with same proportion from values in array
	 * @param valor
	 * @param valores
	 * @return
	 */
	public static final double[] dividesWithBiggestLast(double value, double[] values, int decimalPalces){
		double [] proportion = null;
		if(values.length > 0) {
			proportion = new double[values.length + 1];
			double sum1 = sumValues(values, decimalPalces);
			if (sum1 != 0.0) {
				for(int aux=0; aux<values.length; aux++) {
					proportion[aux] = roundDecimalPlaces(value * roundDecimalPlaces(values[aux]/sum1, 9), decimalPalces);
				}
				double sum2 = sumValues(proportion, decimalPalces);
				proportion[values.length] = roundDecimalPlaces(value - sum2, decimalPalces);
			}
		}
		return proportion;
	}

	/**
	 * Divides the value with same proportion from values in array
	 * @param valor
	 * @param valores
	 * @return
	 */
	public static final double[] dividesWithDifference(double value, double[] values, int decimalPalces){
		double [] proportion = null;
		if(values.length > 0) {
			proportion = new double[values.length + 1];
			double sum1 = sumValues(values, decimalPalces);
			if (sum1 != 0.0) {
				for(int aux=0; aux<values.length; aux++) {
					proportion[aux] = roundDecimalPlaces(value * roundDecimalPlaces(values[aux]/sum1, 9), decimalPalces);
				}
				proportion[values.length] = roundDecimalPlaces(value - sum1, decimalPalces);
			}
		}
		return proportion;
	}

	/**
	 * Soma os valores do array
	 * @param valores Valores
	 * @param decimalPalces N�mero de casas decimais
	 * @return Somat�ria
	 */
	private static double sumValues(double[] values, int decimalPalces) {
		double sum = 0d;
		for (double val: values)
			sum = roundDecimalPlaces(sum + val, decimalPalces);
		return sum;
	}

	/**
	 * @param oldValue
	 * @param decimalPalces
	 * @return
	 */
	public static double roundDecimalPlaces(double oldValue, int decimalPalces) {
		return roundDecimalPlaces(oldValue, decimalPalces, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * @param oldValue
	 * @param decimalPalces
	 * @param roundingMode
	 * @return
	 */
	public static double roundDecimalPlaces(double oldValue, int decimalPalces, int roundingMode) {
		Double valDouble = new Double(oldValue);
		BigDecimal valBig = new BigDecimal(valDouble.toString());
		BigDecimal newValue = valBig.setScale(decimalPalces, roundingMode);
		return newValue.doubleValue();
	}

	/**
	 * Format double to BigDecimal, with 2 decimal
	 * @param oldValue
	 * @param decimalPalces
	 * @return
	 */
	public static BigDecimal roundBigDecimal(double oldValue, int decimalPalces) {
		Double valDouble = new Double(oldValue);
		BigDecimal valBig = new BigDecimal(valDouble.toString());
		BigDecimal newValue = valBig.setScale(decimalPalces, BigDecimal.ROUND_HALF_UP);
		return newValue;
	}

	/**
	 * @param oldValue
	 * @param decimalPalces
	 * @return
	 */
	public static float roundFloat(double oldValue, int decimalPalces) {
		Double valDouble = new Double(oldValue);
		BigDecimal valBig = new BigDecimal(valDouble.toString());
		BigDecimal newValue = valBig.setScale(decimalPalces, BigDecimal.ROUND_HALF_UP);
		return newValue.floatValue();
	}

	public static String formatCurrency(double oldValue) {
		return formatCurrency(oldValue, 2);
	}

	public static String formatCurrency(double oldValue, int decimalPalces) {
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(decimalPalces);
		nf.setMinimumFractionDigits(decimalPalces);
		nf.setGroupingUsed(true);
		return nf.format(oldValue);
	}

	public static Double convertValueDefault(Double value, Double valueDefault){
		return value == null || Double.isNaN(value)? valueDefault : value;
	}

	public static BigDecimal doubleToBigDecimal(Double doubleValue){
		String value = doubleValue == null? "0.0" : doubleValue.toString();
		BigDecimal newValue = new BigDecimal(value);
		return newValue;
	}

	/**
	 * @param doubleValue
	 * @return String value
	 */
	public static String formatDoubleToIntegerPercent(Double doubleValue){
		StringBuilder value = new StringBuilder("0");
		if (doubleValue != null && doubleValue >= 0.0d) {
			DecimalFormat decimalFormat = new DecimalFormat("#####0");
			value = new StringBuilder(decimalFormat.format(doubleValue * 100));
		}
		value.append("%");
		return value.toString();
	}

	/**
	 * @param doubleValue
	 * @param _default
	 * @return String value
	 */
	public static String formatDoubleToIntegerPercent(Double doubleValue, String _default){
		StringBuilder value = new StringBuilder("0");
		if (doubleValue != null && doubleValue >= 0.0d) {
			DecimalFormat decimalFormat = new DecimalFormat(_default);
			value = new StringBuilder(decimalFormat.format(doubleValue * 100));
		}
		value.append("%");
		return value.toString();
	}

	/**
	 * @param doubleValue
	 * @return String value
	 */
	public static Number formatToNumber(Double doubleValue){
		if (doubleValue != null && doubleValue >= 0.0d) {
			DecimalFormat decimalFormat = new DecimalFormat("#####0.0");
			try {
				return decimalFormat.parse(decimalFormat.format(doubleValue));
			} catch (ParseException e) {
				return 0.0d;
			}
		}
		return 0.0d;
	}

	public static Double parseNullOrMinValue(Double inputValue, double otherValue) {
		// devolution
		if (inputValue != null && inputValue < 0d) {
			if (inputValue < (otherValue * (-1)))
				return inputValue;
			return (otherValue * (-1));
		}
		// collection
		else if (inputValue != null && inputValue > 0d) {
			if (inputValue > (otherValue * (+1)))
				return inputValue;
			return (otherValue * (+1));
		}
		// no movement
		return 0d;
	}

	public static Double parseNullOrMinValue(Double inputValue, double otherValue, int periodsTerm) {
		// devolution
		if (periodsTerm < 0) {
			if (inputValue != null && inputValue != 0d && inputValue < (otherValue * periodsTerm))
				return inputValue;
			return (otherValue * periodsTerm);
		}
		// collection
		else if (periodsTerm > 0) {
			if (inputValue != null && inputValue != 0d && inputValue > (otherValue * periodsTerm))
				return inputValue;
			return (otherValue * periodsTerm);
		}
		// no movement
		return 0d;
	}

	public static Double parseNullOrOtherValue(Double inputValue, Double otherValue) {
		if (inputValue != null && inputValue != 0d)
			return inputValue;
		return (otherValue != null ? otherValue : 0d);
	}

	public static Integer parseNullOrOtherValue(Integer inputValue, Integer otherValue) {
		if (inputValue != null && inputValue != 0)
			return inputValue;
		return (otherValue != null ? otherValue : 0);
	}

	public static Short parseNullOrOtherValue(Short inputValue, Short otherValue) {
		if (inputValue != null && inputValue != 0)
			return inputValue;
		return (otherValue != null ? otherValue : 0);
	}

	public static Double parseNull(Double inputValue) {
		if (inputValue != null)
			return inputValue;
		return 0d;
	}

	public static Integer parseNull(Integer inputValue) {
		if (inputValue != null)
			return inputValue;
		return 0;
	}

	public static BigInteger parseNull(BigInteger inputValue) {
		if (inputValue != null)
			return inputValue;
		return BigInteger.valueOf(0);
	}

	public static BigDecimal parseNull(BigDecimal inputValue) {
		if (inputValue != null)
			return inputValue;
		return BigDecimal.valueOf(0);
	}

	public static Long parseNull(Long inputValue) {
		if (inputValue != null)
			return inputValue;
		return 0L;
	}

	public static Short parseNull(Short inputValue) {
		if (inputValue != null)
			return inputValue;
		return 0;
	}
}