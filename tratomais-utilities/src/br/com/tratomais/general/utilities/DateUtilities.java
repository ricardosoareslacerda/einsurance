package br.com.tratomais.general.utilities;

import java.math.BigDecimal;
import java.security.InvalidParameterException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.joda.time.Years;

public class DateUtilities {

	public static final String PATTERN_YEAR_MONTH_DAY = "yyyyMMdd";
	public static final String PATTERN_DEFAULT = "dd/MM/yyyy";

	/**
	 * All minutes have this many milliseconds except the last minute of the day on a day defined with
	 * a leap second.
	 */
	public static final long MILLISECS_PER_MINUTE = 60*1000;

	/**
	 * Number of milliseconds per hour, except when a leap second is inserted.
	 */
	public static final long MILLISECS_PER_HOUR   = 60*MILLISECS_PER_MINUTE;

	/**
	 * Number of leap seconds per day expect on 
	 * <BR/>1. days when a leap second has been inserted, e.g. 1999 JAN  1.
	 * <BR/>2. Daylight-savings "spring forward" or "fall back" days.
	 */
	protected static final long MILLISECS_PER_DAY = 24*MILLISECS_PER_HOUR;
	/**
	 * Represents the number of miliseconds in a day
	 */
	public static final int DAY_MILI = 1000*24*60*60;
	public static final double YEAR_MILI = DAY_MILI * 365.25;

	public static int extractYear(Date data){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		return calendar.get(Calendar.YEAR);
	}

	public static int calculateAge(Date birthDate){
		Calendar dob = Calendar.getInstance();
		dob.setTime(birthDate);
		Calendar today = Calendar.getInstance();
		int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

		if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR))
			age--;

		return age;
	}

	/**
	 * Diference between two dates, in years
	 * @param dataNascimento First date
	 * @param dataReferencia Second date
	 * @return Diference in years
	 */
	public static int actuarialAgeByDate(Date dataNascimento, Date dataReferencia){
		int idade = getYear(dataReferencia) - getYear(dataNascimento);
		Date dataAtual = (new GregorianCalendar(getYear(dataNascimento) + idade, getMes(dataNascimento)-1, getDia(dataNascimento))).getTime();
		long diff = (dataReferencia.getTime() - dataAtual.getTime())/DAY_MILI;
		if (diff<0) // O aniversário ainda vai ser
			idade--;
		// Modificação Actuarial
		if (((diff>=-182)&&(diff<0))||((diff>=182)&&(diff<365))) {
			idade++;
		}
		return idade;
	}

	/**
	 * Years between two dates
	 * @param dataReferencia
	 * @param dataFinal
	 * @return
	 */
	public static int ageByDate(Date dataNascimento, Date dataReferencia){
		int idade = getYear(dataReferencia) - getYear(dataNascimento);
		Date dataAtual = (new GregorianCalendar(getYear(dataNascimento) + idade, getMes(dataNascimento)-1, getDia(dataNascimento))).getTime();
		long diff = (dataReferencia.getTime() - dataAtual.getTime());
		if (diff<0) // O aniversário ainda vai ser
			idade--;
		return idade;
	}

	/**
	 * Years and Month between two dates
	 * @param dataNascimento
	 * @param dataReferencia
	 * @return
	 */
	public static double agePartByDate(Date dataNascimento, Date dataReferencia){
		DiferencaData agePart = getDateDifference(dataNascimento, dataReferencia);
		double ageYear = agePart.getAnos();
		double ageMonth = roundDecimalPlaces(((agePart.getMeses() % 12d) / 100.0), 2);
		double idade = ageYear + ageMonth;
		return idade;
	}

	public static int getYear(Date data){
		return new LocalDate(data).getYear();
	}

	public static int getMes(Date data){
		return new LocalDate(data).getMonthOfYear();
	}

	public static int getDia(Date data){
		return new LocalDate(data).getDayOfMonth();
	}

	public final static int ANUAL=60;
	public final static int MENSAL=61;
	public final static int DIARIO=62;

	/**
	 * Adds a determinied number of periods to a date
	 * @param data Reference date to work with
	 * @param multipleType Multiple type to be used
	 * @param qtde Quantity of periods to add
	 * @return Final date
	 */
	public static Date dateInFuture(Date data, int multipleType, int qtde) {
		Calendar retorno = GregorianCalendar.getInstance();
		retorno.setTime(data);
		switch (multipleType){
			case ANUAL:
				retorno.add(Calendar.YEAR, qtde);
				break;
				
			case MENSAL:
				retorno.add(Calendar.MONTH, qtde);
				break;
				
			case DIARIO:
				retorno.add(Calendar.DAY_OF_MONTH, qtde);
				break;
				
			default:
				throw new InvalidParameterException("multipleType not found: " + multipleType);
		}
		return retorno.getTime();
	}

	public static Date truncDate(Date inputDate){
		Calendar workCalendar = GregorianCalendar.getInstance();
		workCalendar.setTime(inputDate);
		workCalendar.set(Calendar.HOUR_OF_DAY, 0);
		workCalendar.set(Calendar.MINUTE, 0);
		workCalendar.set(Calendar.SECOND, 0);
		workCalendar.set(Calendar.MILLISECOND, 0);
		return workCalendar.getTime();
	}

	/**
	 * Calculate the number of days between two dates
	 */
	public static int diffDateInDays(Date iniDate, Date finalDate){
		iniDate = truncDate(iniDate);
		finalDate = truncDate(finalDate);
		
		int response = 0;
		if (finalDate.after(iniDate)){
			long diff = (finalDate.getTime() - iniDate.getTime());
			response = (int)(diff / MILLISECS_PER_DAY);
		}
		else{
			long diff = (iniDate.getTime() - finalDate.getTime());
			response = (int)(diff / MILLISECS_PER_DAY);
		}
		return response;
	}

	public static DateTime toDateTime(Date date) {
		Calendar startCal = Calendar.getInstance();
		startCal.setTime(date);
		return new DateTime(startCal.get(Calendar.YEAR), (startCal.get(Calendar.MONTH) + 1), startCal.get(Calendar.DAY_OF_MONTH), 0, 0, 0, 0, DateTimeZone.UTC);
	}

	/**
	 * Returns absolutes values for diferences between two dates
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static DiferencaData getDateDifference(Date date1, Date date2) {
		DateTime startDate;
		DateTime endDate;

		if (date1.compareTo(date2) <= 0) {
			startDate = toDateTime(date1);
			endDate = toDateTime(date2);
		}
		else {
			startDate = toDateTime(date2);
			endDate = toDateTime(date1);
		}

		return fillDateDifference(startDate, endDate);
	}

	private static DiferencaData fillDateDifference(DateTime startDate, DateTime endDate) {
		int days = Days.daysBetween(startDate, endDate).getDays();
		int months = Months.monthsBetween(startDate, endDate).getMonths();
		int years = Years.yearsBetween(startDate, endDate).getYears();
		return new DiferencaData(days, months, years);
	}

	public static final class DiferencaData{
		int dias, meses, anos;
		/**
		 * Carrier for data diferences routines
		 * @param dias number of days
		 * @param meses Number of months
		 * @param anos Number of years
		 */
		public DiferencaData(int dias, int meses, int anos) {
			this.dias = dias;
			this.meses = meses;
			this.anos = anos;
		}
		/**
		 * @return the dias
		 */
		public int getDias() {
			return dias;
		}
		/**
		 * @return the meses
		 */
		public int getMeses() {
			return meses;
		}
		/**
		 * @return the anos
		 */
		public int getAnos() {
			return anos;
		}
	}

	@Deprecated
	public static Date addDay(Date data, int numDias){
		return truncDate(new Date(data.getTime() + (DateUtilities.MILLISECS_PER_DAY * numDias))); 
	}

	public static Date addDays(Date data, int numDias){
		DateTime dataWrk = DateUtilities.toDateTime(data);
		return DateUtilities.mkDate(dataWrk.getYear(), dataWrk.getMonthOfYear() -1, dataWrk.getDayOfMonth()  + numDias);
	}

	public static Date addMonths(Date data, int months) {
		return addMonths(data, months, false);
	}

	public static Date addMonths(Date data, int months, boolean checkLastDayOfMonth) {
		Calendar instance = GregorianCalendar.getInstance();
		instance.setTime(data);
		instance.add(Calendar.MONTH, months);
		return truncDate((checkLastDayOfMonth ? fillLastDayOfMonth(instance.getTime(), checkLastDayOfMonth(data)) : instance.getTime()));
	}

	private static Date fillLastDayOfMonth(Date data, boolean fillLastDayOfMonth) {
		Date result = data;
		if (fillLastDayOfMonth) {
			DateTime dataWrk = DateUtilities.toDateTime(data);
			result = DateUtilities.mkDate(dataWrk.getYear(), dataWrk.getMonthOfYear() -1, dataWrk.dayOfMonth().withMaximumValue().getDayOfMonth());
		}
		return truncDate(result);
	}

	public static Boolean checkLastDayOfMonth(Date data) {
		DateTime dataWrk = toDateTime(data);
		return (dataWrk.getDayOfMonth() == dataWrk.dayOfMonth().withMaximumValue().getDayOfMonth());
	}

	public static Boolean checkFirstDayOfMonth(Date data) {
		DateTime dataWrk = toDateTime(data);
		return (dataWrk.getDayOfMonth() == dataWrk.dayOfMonth().withMinimumValue().getDayOfMonth());
	}

	public static Date addYears(Date data, int numYears){
		Calendar instance = GregorianCalendar.getInstance();
		instance.setTime(data);
		instance.add(Calendar.YEAR, numYears);
		return truncDate(instance.getTime());
	}

	private static double roundDecimalPlaces(double oldValue, int decimalPalces) {
		BigDecimal valBig = new BigDecimal(oldValue);
		BigDecimal newValue = valBig.setScale(decimalPalces, BigDecimal.ROUND_HALF_UP);
		return newValue.doubleValue();
	}

	public static Date mkDate(int year, int month, int day){
		Calendar workCalendar = GregorianCalendar.getInstance();
		workCalendar.set(Calendar.DAY_OF_MONTH, day);
		workCalendar.set(Calendar.MONTH, month);
		workCalendar.set(Calendar.YEAR, year);
		workCalendar.set(Calendar.HOUR_OF_DAY, 0);
		workCalendar.set(Calendar.MINUTE, 0);
		workCalendar.set(Calendar.SECOND, 0);
		workCalendar.set(Calendar.MILLISECOND, 0);
		return workCalendar.getTime();
	}

	/**
	 * Returns one object XMLGregorianCalendar converted to Date
	 * @param date
	 * @return Date
	 */
	public static Date xmlGregorianCalendarToDate(XMLGregorianCalendar date){
		return (isValid(date) ? date.toGregorianCalendar().getTime() : null);
	}

	public static XMLGregorianCalendar dateToXMLGregorianCalendar(Date date) throws DatatypeConfigurationException{
		if (date==null)
			return null;
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(date);
		XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		return date2;
	}

	public static Date parseNullOrOtherValue(Date inputValue, Date otherValue) {
		if (inputValue != null)
			return inputValue;
		return (otherValue != null ? otherValue : null);
	}
	
	public static boolean isValid(XMLGregorianCalendar date) {
		if (date == null)
			return false;
		return !(mkDate(1, 0, 1).compareTo(mkDate(date.getYear(), (date.getMonth()-1), date.getDay())) == 0);
	}

	/**
	 * Calcula o "periodicidade" entre duas datas
	 * @return Periodicidade do prazo
	 */
	public static int periodBetweenDates(Date dateFind, Date dateCalc, int termMonth) {
		int periods = 0;
		while (dateCalc.compareTo(dateFind) > 0) {
			dateCalc = addMonths(dateCalc, termMonth * -1);
			periods++;
		}
		return periods;
	}

	/**
	 * Calcula o ultimo fim de vigência da "periodicidade" entre duas datas
	 * @return Fim de vigência
	 */
	public static Date lastExpiryDatePeriod(Date dateFind, Date dateCalc, int termMonth) {
		int periods = 0;
		Date dateFim = addDay(addMonths(dateCalc, (termMonth * ++periods), false), -1);
		while (dateFind.compareTo(dateFim) > 0) {
			dateFim = addDay(addMonths(dateCalc, (termMonth * ++periods), false), -1);
		}
		return dateFim;
	}
}