package br.com.tratomais.validation.bank;

/**
 * Informa��es sobre a conta banc�ria
 */
public interface IValidateBankInfo {
	/**
	 * Valida a conta
	 * @param bankNumber C�digo do banco
	 * @param accountNumber C�digo da conta
	 * @param accountType Tipo da conta
	 * @return Se a conta for v�lida
	 */
	boolean validateAccount(String bankNumber, String accountNumber, BankAccountType accountType);
	
	/**
	 * Valida o n�mero da ag�ncia
	 * @param bankNumber N�mero do banco
	 * @param agencyNumber N�mero da ag�ncia
	 * @param accountType Tipo da conta
	 * @return Se o n�mero da ag�ncia for v�lido
	 */
	boolean validateAgencyNumber(String bankNumber, String agencyNumber, BankAccountType accountType);
	
	/**
	 * Valida todas as informa��es banc�rias
	 * @param info Informa��es banc�rias 
	 * @throws BankValidationException Caso haja algum tipo de erro
	 */
	void validateInfo(IAccountInfo info) throws BankValidationException;
}

