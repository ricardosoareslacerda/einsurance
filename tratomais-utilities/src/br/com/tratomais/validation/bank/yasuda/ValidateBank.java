package br.com.tratomais.validation.bank.yasuda;

import br.com.tratomais.validation.bank.AccountInfo;
import br.com.tratomais.validation.bank.BankAccountType;
import br.com.tratomais.validation.bank.IAccountInfo;
import br.com.tratomais.validation.bank.BankValidationException;
import br.com.tratomais.validation.bank.IValidateBankInfo;
import br.com.yasuda.controlador.DVBanco;

/** Valida bancos Yasuda */
public class ValidateBank implements IValidateBankInfo {
	/** Inst�ncia da classe que far� a valida��o */
	private DVBanco dvBanco;
	
	// -------------------------------------------------
	
	public ValidateBank() {
		dvBanco = new DVBanco();
	}
	
	// -------------------------------------------------

	/** 
	 * Valida os dados banc�rios
	 * @param info informa��o
	 * @return Verdadeiro se os dados forem v�lidos
	 */
	private boolean validate(IAccountInfo info) {
		dvBanco.setCodigoBanco(info.getBankNumber());
		dvBanco.setAgenciaComDigito(info.getAgencyNumber());
		dvBanco.setContaCorrenteComDigito(info.getAccountNumber());
		return dvBanco.isAgenciaContaValidos();
	}

	
	/** {@inheritDoc} */
	@Override
	public void validateInfo(IAccountInfo info) throws BankValidationException {
		if (!validate(info))
			throw new BankValidationException("N�mero da agencia/conta � inv�lida ou n�o informada!");
	}

	/** {@inheritDoc} */
	@Override
	public boolean validateAccount(String bankNumber, String accountNumber, BankAccountType accountType) {
		return validate(new AccountInfo(accountType, bankNumber, null, accountNumber));
	}

	/** {@inheritDoc} */
	@Override
	public boolean validateAgencyNumber(String bankNumber, String agencyNumber, BankAccountType accountType) {
		return validate(new AccountInfo(accountType, bankNumber, agencyNumber, null));
	}
}
