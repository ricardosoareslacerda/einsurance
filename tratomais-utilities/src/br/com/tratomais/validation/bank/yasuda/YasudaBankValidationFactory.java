package br.com.tratomais.validation.bank.yasuda;

import br.com.tratomais.validation.bank.BankValidationFactory;
import br.com.tratomais.validation.bank.IValidateBankInfo;

/**
 * Validador baseado no jar da Yasuda
 */
public class YasudaBankValidationFactory extends BankValidationFactory {
	static {
		BankValidationFactory.classe = YasudaBankValidationFactory.class;
	}

	/** {@inheritDoc} */
	@Override
	public IValidateBankInfo createValidator() {
		return new ValidateBank();
	}
}
