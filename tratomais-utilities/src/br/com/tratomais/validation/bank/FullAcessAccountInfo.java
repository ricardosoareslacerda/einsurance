package br.com.tratomais.validation.bank;

public class FullAcessAccountInfo extends AccountInfo {
	public FullAcessAccountInfo() {
		super(null,null,null,null);
	}
	
	/** {@inheritDoc} */
	@Override
	public void setAccountNumber(String accountNumber) {
		super.setAccountNumber(accountNumber);
	}
	
	/** {@inheritDoc} */
	@Override
	public void setAccountType(BankAccountType accountType) {
		super.setAccountType(accountType);
	}
	
	/** {@inheritDoc} */
	@Override
	public void setAgencyNumber(String agencyNumber) {
		super.setAgencyNumber(agencyNumber);
	}
	
	/** {@inheritDoc} */
	@Override
	public void setBankNumber(String bankNumber) {
		super.setBankNumber(bankNumber);
	}
}
