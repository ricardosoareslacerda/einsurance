package br.com.tratomais.validation.bank;

public enum BankAccountType {
	SAVINGS,
	CHECKINGS,
	INVESTMENTS
}
