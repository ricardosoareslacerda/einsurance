package br.com.tratomais.validation.bank;

/** Excess�o de valida��o de dados banc�rios */
public class BankValidationException extends Exception {
	/** Vers�o da classe */
	private static final long serialVersionUID = 1L;

	/** {@inheritDoc} */
	public BankValidationException() {
		super();
	}

	/** {@inheritDoc} */
	public BankValidationException(String message, Throwable cause) {
		super(message, cause);
	}

	/** {@inheritDoc} */
	public BankValidationException(String message) {
		super(message);
	}

	/** {@inheritDoc} */
	public BankValidationException(Throwable cause) {
		super(cause);
	}
}

