package br.com.tratomais.validation.bank;

/** Interface com os dados da conta banc�ria */
public class AccountInfo implements IAccountInfo {
	/** Tipo da conta */
	private BankAccountType accountType;
	/** {@inheritDoc} */
	@Override
	public BankAccountType getAccountType() {
		return accountType;
	}
	/**
	 * Ajusta o tipo da conta
	 * @param accountType Tipo da conta
	 */
	protected void setAccountType(BankAccountType accountType) {
		this.accountType = accountType;
	}
	
	/** N�mero do banco */
	private String bankNumber;
	/** {@inheritDoc} */
	@Override
	public String getBankNumber() {
		return bankNumber;
	}
	/**
	 * Ajusta o n�mero do banco
	 * @param bankNumber N�mero do banco
	 */
	protected void setBankNumber(String bankNumber) {
		this.bankNumber = bankNumber;
	}
	

	/** N�mero da ag�ncia */
	private String agencyNumber;
	/** {@inheritDoc} */
	@Override
	public String getAgencyNumber() {
		return this.agencyNumber;
	}
	/**
	 * Ajusta o n�mero da ag�ncia
	 * @param agencyNumber N�mero da ag�ncia
	 */
	protected void setAgencyNumber(String agencyNumber) {
		this.agencyNumber = agencyNumber;
	}

	/** N�mero da conta */
	private String accountNumber;
	/** {@inheritDoc} */
	@Override
	public String getAccountNumber() {
		return accountNumber;
	}
	/**
	 * Ajusta o n�mero da conta
	 * @param accountNumber N�mero da conta
	 */
	protected void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	// ---------------------------------------------------------------------
	
	public AccountInfo(BankAccountType accountType, String bankNumber,
			String agencyNumber, String accountNumber) {
		super();
		this.accountType = accountType;
		this.bankNumber = bankNumber;
		this.agencyNumber = agencyNumber;
		this.accountNumber = accountNumber;
	}
}
