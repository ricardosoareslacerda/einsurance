package br.com.tratomais.validation.bank;

/** Interface com os dados da conta banc�ria */
public interface IAccountInfo {
	/**
	 * Retorna o tipo de conta
	 * @return
	 */
	BankAccountType getAccountType();
	/**
	 * Retorna o n�mero do banco
	 * @return N�mero do banco
	 */
	String getBankNumber();
	
	/**
	 * Retorna o n�mero da ag�ncia
	 * @return N�mero da ag�ncia
	 */
	String getAgencyNumber();
	
	/** 
	 * Retorna o n�mero da conta
	 * @return N�mero da conta
	 */
	String getAccountNumber();
}
