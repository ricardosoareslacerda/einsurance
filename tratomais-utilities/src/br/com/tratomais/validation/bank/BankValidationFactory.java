package br.com.tratomais.validation.bank;

public abstract class BankValidationFactory {
	/** Classe de valida��o de banco */
	protected static Class<? extends BankValidationFactory> classe;
	
	/** Retorna valida��o de banco */
	public BankValidationFactory getInstance() throws InstantiationException, IllegalAccessException {
		return classe.newInstance();
	}
	
	public abstract IValidateBankInfo createValidator();
}
