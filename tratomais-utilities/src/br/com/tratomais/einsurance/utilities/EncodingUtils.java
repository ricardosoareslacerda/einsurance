package br.com.tratomais.einsurance.utilities;

import java.math.BigInteger;

public class EncodingUtils {
	/**
	 * Divides the Billing Reference into it's parts
	 * @param billingReference Billing Reference string
	 * @return Billing Reference's parts
	 */
	public static BillingReferenceDNA decodeBillingReference(String billingReference){
		return  new BillingReferenceDNA(Long.valueOf(billingReference.substring(0, 6).trim()), Integer.valueOf(billingReference.substring(6, 8).trim()));
	}
	
	/**
	 * Encode the
	 * @param proposalNumber
	 * @param installmentId
	 * @return
	 */
	public static String encodeBillingReference(long proposalNumber, int installmentId){
		if  ( (installmentId > 99) || (installmentId<0))
			throw new IllegalArgumentException("Illegal InstallmentId");
		if ((proposalNumber > 999999) || (proposalNumber <1))
			throw new IllegalArgumentException("Illegal proposal number");
		return String.format("%06d%02d", proposalNumber, installmentId);
	}
	
	/**
	 * Represents the Billing Reference's parts. Can't have it's values changed
	 */
	public static class BillingReferenceDNA {
		private Long proposalNumber;
		private Integer installmentId;
		
		public BillingReferenceDNA(Long proposalNumber, Integer installmentId) {
			super();
			this.proposalNumber = proposalNumber;
			this.installmentId = installmentId;
		}
		public Long getProposalNumber() {
			return proposalNumber;
		}
		public Integer getInstallmentId() {
			return installmentId;
		}
	}
	
	public static String encodeClientReference(BigInteger policyNumber, Long certificateNumber){
		return String.format("%013d%06d", policyNumber, certificateNumber);
	}
	
	public static ClientReferenceDNA decodeClientReference(String clientReference){
		return new ClientReferenceDNA(new BigInteger(clientReference.substring(0, 13)), Long.valueOf(clientReference.substring(13)));
	}
	
	public static class ClientReferenceDNA {
		private BigInteger policyNumber;
		private Long certificateNumber;
		
		public ClientReferenceDNA(BigInteger policyNumber,
				Long certificateNumber) {
			this.policyNumber = policyNumber;
			this.certificateNumber = certificateNumber;
		}
		public BigInteger getPolicyNumber() {
			return policyNumber;
		}
		public void setPolicyNumber(BigInteger policyNumber) {
			this.policyNumber = policyNumber;
		}
		public Long getCertificateNumber() {
			return certificateNumber;
		}
		public void setCertificateNumber(Long certificateNumber) {
			this.certificateNumber = certificateNumber;
		}
	}

	public static CreditCardExpiration decodeCreditCardValidTroughYM(int value){
		String data = Integer.toString(value);
		return new CreditCardExpiration(Integer.parseInt(data.substring(0,4)), Integer.parseInt(data.substring(4,6)));
	}
	
	public static CreditCardExpiration decodeCreditCardValidTroughYM(String data){
		return new CreditCardExpiration(Integer.parseInt(data.substring(0,4)), Integer.parseInt(data.substring(4,6)));
	}
	
	public static int encodeCreditCardValidThroughYM(int year, int month) {
		return Integer.parseInt(String.format("%04d%02d", year, month));
	}
	
	public static class CreditCardExpiration {
		private int year, month;

		public CreditCardExpiration(int year, int month) {
			super();
			this.year = year;
			this.month = month;
		}

		public int getYear() {
			return year;
		}

		public int getMonth() {
			return month;
		}
	}
}
