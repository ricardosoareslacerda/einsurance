package br.com.tratomais.esb.collections.model.edi.citibank;

import java.io.Serializable;

/**
 * Contenido para entrada de los datos de detalle de los registros de Recaudaci�n de Domiciliaci�n de Cobranza Citibank.
 * @author luiz.alberoni
 */
public class Detalle implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** Construtor, ajusta o tipo de registro para o default, R1 */
	public Detalle() {
		this.tipoRegistro = "R2";
		this.codigoRespuesta = 99;
	}
	
	/**
	 * Construtor, ajusta o tipo de registro
	 * @param tipoDeRegistro tipo de registro
	 */
	public Detalle(String tipoDeRegistro) {
		this.tipoRegistro = tipoDeRegistro;
		this.codigoRespuesta = 99;
	}

	/**
	 * Construtor, ajusta o tipo de registro
	 * @param tipoDeRegistro tipo de registro
	 * @param codigoRespuesta C�digo de respuesta
	 */
	public Detalle(String tipoDeRegistro, int codigoRespuesta) {
		this.tipoRegistro = tipoDeRegistro;
		this.codigoRespuesta = codigoRespuesta;
	}
	
	/**
	 * Identificador do tipo de registro de detalle (fijo: R2)
	 */
	private String tipoRegistro;
	public String getTipoRegistro() {
		return tipoRegistro;
	}
	
	/**
	 * Referencia interna del certificado del cliente (ej.: I8518000000001000001)
	 */
	private String certificadoCliente;
	public String getCertificadoCliente() {
		return certificadoCliente;
	}
	public void setCertificadoCliente(String certificadoCliente) {
		this.certificadoCliente = certificadoCliente;
	}
	
	
	/**
	 * Referencia interna del b�bito (ej.: 00016901)
	 */
	private String referenciaDebito;
	public String getReferenciaDebito() {
		return referenciaDebito;
	}
	public void setReferenciaDebito(String referenciaDebito) {
		this.referenciaDebito = referenciaDebito;
	}
	
	/**
	 * Medio de Pago: TA = Tarjeta o CB = Cargo en Cuenta
	 */
	private String medioPago;
	public String getMedioPago() {
		return medioPago;
	}
	public void setMedioPago(String medioPago) {
		this.medioPago = medioPago;
	}
	
	/**
	 * Fecha expiraci�n de la tarjeta (formato: AAAMM)
	 */
	private int expTarjeta;
	public int getExpTarjeta() {
		return expTarjeta;
	}
	public void setExpTarjeta(int expTarjeta) {
		this.expTarjeta = expTarjeta;
	}
	
	/**
	 * N�mero de la Cuenta o Tarjeta donde se debitar�, sin formatar
	 */
	private String cuenta;
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	
	/**
	 * RIF del cliente, sin formatar (ej.: V1234567)
	 */
	private String rif;
	public String getRif() {
		return rif;
	}
	public void setRif(String rif) {
		this.rif = rif;
	}
	
	/**
	 * Monto a debitar en D�lar, con dos decimales, separadas por punto (ej.: 123.45)
	 */
	private double montoDolares;
	public double getMontoDolares() {
		return montoDolares;
	}
	public void setMontoDolares(double montoDolares) {
		this.montoDolares = montoDolares;
	}
	
	/**
	 * Tasa del D�lar, con dos decimales, separadas por punto (ej.: 4.15)
	 */
	private double tasaConversion;
	public double getTasaConversion() {
		return tasaConversion;
	}
	public void setTasaConversion(double tasaConversion) {
		this.tasaConversion = tasaConversion;
	}
	
	/**
	 * Monto a debitar em Bolivar, con dos decimales, separadas por punto (ej.: 123.45)
	 */
	private double montoBol;
	public double getMontoBol() {
		return montoBol;
	}
	public void setMontoBol(double montoBol) {
		this.montoBol = montoBol;
	}
	
	/**
	 * C�digo de respuesta (fijo: 99)
	 */
	private Integer codigoRespuesta;
	public Integer getCodigoRespuesta() {
		return codigoRespuesta;
	}
	
	/**
	 * Indicador de Pago Facil: 0 = No y 1 = Si
	 */
	private Integer pagoFacil;
	public Integer getPagoFacil() {
		return pagoFacil;
	}
	public void setPagoFacil(Integer pagoFacil) {
		this.pagoFacil = pagoFacil;
	}
	
	/**
	 * C�digo interno del producto de seguro (ej.: 839 = Zurich Life)
	 */
	private String codigoInterno;
	public String getCodigoInterno() {
		return codigoInterno;
	}
	public void setCodigoInterno(String codigoInterno) {
		this.codigoInterno = codigoInterno;
	}
}
