package br.com.tratomais.esb.collections.model.edi;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EdiAffiliationFile implements Serializable {
	private String	tipoRegistro;		//  2	A1
	private String	uNBNr;				// 14	UNB Number
	private String	messageNbr;			// 15	Message Nbr
	private Date	fechaDebito;		//  8	Fecha generacion mensaje de debito
	private String	nroAfiliacion;		// 15	Lote Respuesta
	private String	rifOrdenante;		// 17	Rif  Empresa
	private String	nombreOrdenante; 	// 35	Nombre Empresa
	private String	nroEdiEnvia;		// 18	Nro Edi de quien env�a (ver abajo)
	private String	nroEdiRecibe;		// 18	Nro Edi de quien recibe (ver abajo)
	private String	version;			//  4	Versi�n documento (sugerido D96A)

	private List<EdiAffiliationRecord> affiliationRecords = new ArrayList<EdiAffiliationRecord>();	
	
	public String getTipoRegistro() {
		return tipoRegistro;
	}
	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}
	public String getuNBNr() {
		return uNBNr;
	}
	public void setuNBNr(String uNBNr) {
		this.uNBNr = uNBNr;
	}
	public String getMessageNbr() {
		return messageNbr;
	}
	public void setMessageNbr(String messageNbr) {
		this.messageNbr = messageNbr;
	}
	public Date getFechaDebito() {
		return fechaDebito;
	}
	public void setFechaDebito(Date fechaDebito) {
		this.fechaDebito = fechaDebito;
	}
	public String getNroAfiliacion() {
		return nroAfiliacion;
	}
	public void setNroAfiliacion(String nroAfiliacion) {
		this.nroAfiliacion = nroAfiliacion;
	}
	public String getRifOrdenante() {
		return rifOrdenante;
	}
	public void setRifOrdenante(String rifOrdenante) {
		this.rifOrdenante = rifOrdenante;
	}
	public String getNombreOrdenante() {
		return nombreOrdenante;
	}
	public void setNombreOrdenante(String nombreOrdenante) {
		this.nombreOrdenante = nombreOrdenante;
	}
	public String getNroEdiEnvia() {
		return nroEdiEnvia;
	}
	public void setNroEdiEnvia(String nroEdiEnvia) {
		this.nroEdiEnvia = nroEdiEnvia;
	}
	public String getNroEdiRecibe() {
		return nroEdiRecibe;
	}
	public void setNroEdiRecibe(String nroEdiRecibe) {
		this.nroEdiRecibe = nroEdiRecibe;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public void setAffiliationRecords(List<EdiAffiliationRecord> affiliationRecords) {
		this.affiliationRecords = affiliationRecords;
	}
	public List<EdiAffiliationRecord> getAffiliationRecords() {
		return affiliationRecords;
	}
	
	public void createTestInstance(){
		tipoRegistro = "A1";
		uNBNr = "28            ";
		messageNbr = "013410110028   ";
		fechaDebito = new Date();
		nroAfiliacion = "01340028       ";
		rifOrdenante = "J0000000000340242";
		nombreOrdenante = "ZURICH SEGUROS, S.A.               ";
		nroEdiEnvia = "ZURICHSEGVE       ";
		nroEdiRecibe = "BANESCOVE         ";
		version = "D96A";
		
		affiliationRecords = new ArrayList<EdiAffiliationRecord>();
		EdiAffiliationRecord affiliationRecord = new EdiAffiliationRecord();
		
		affiliationRecord.createTestInstance();
		affiliationRecords.add(affiliationRecord);
	}
}
