package br.com.tratomais.esb.collections.model.edi;

import java.io.Serializable;
import java.util.Date;

public class EdiAffiliationRecord implements Serializable {
	private String tipoRegistro;			//  2	A2
	private String tipoAfiliacion;			//  3	Codigo retorno banco
	private String rifCliente;				// 17	Rif Cliente
	private String nombreCliente;			// 35	Nombre Cliente
	private String refCliente;				// 20	Nro Contrato (referencia interna del cliente) ??? deveria ser 20 posiciones
	private String fechaExpiracionTarjeta;	//  6	Date(AAAAMM)Fecha expiraci�n tarjeta
	private Date   fechaComienzo;			//  8	Fecha comienzo cobro
	private String formaPago;				//  3	Forma, TA: Tarjeta, CB: Cargo en cuenta
	private String nroCtaTarjeta;			// 20	Cuenta � tarjeta donde se aplicar� el d�bito
	private String codBanco;				// 12	Banco donde se aplicar� el d�bito (tabla SWIFT)

	public String getTipoRegistro() {
		return tipoRegistro;
	}
	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}
	public String getTipoAfiliacion() {
		return tipoAfiliacion;
	}
	public void setTipoAfiliacion(String tipoAfiliacion) {
		this.tipoAfiliacion = tipoAfiliacion;
	}
	public String getRifCliente() {
		return rifCliente;
	}
	public void setRifCliente(String rifCliente) {
		this.rifCliente = rifCliente;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public String getRefCliente() {
		return refCliente;
	}
	public void setRefCliente(String refCliente) {
		this.refCliente = refCliente;
	}
	public String getFechaExpiracionTarjeta() {
		return fechaExpiracionTarjeta;
	}
	public void setFechaExpiracionTarjeta(String fechaExpiracionTarjeta) {
		this.fechaExpiracionTarjeta = fechaExpiracionTarjeta;
	}
	public Date getFechaComienzo() {
		return fechaComienzo;
	}
	public void setFechaComienzo(Date fechaComienzo) {
		this.fechaComienzo = fechaComienzo;
	}
	public String getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	public String getNroCtaTarjeta() {
		return nroCtaTarjeta;
	}
	public void setNroCtaTarjeta(String nroCtaTarjeta) {
		this.nroCtaTarjeta = nroCtaTarjeta;
	}
	public String getCodBanco() {
		return codBanco;
	}
	public void setCodBanco(String codBanco) {
		this.codBanco = codBanco;
	}

	public void createTestInstance(){
		tipoRegistro = "A2";
		tipoAfiliacion = "052";
		rifCliente = "V0000000007949732";
		nombreCliente = "ADDYS COROMOTO AULAR TORRES        ";
		refCliente = "I8935000000002000028";
		fechaExpiracionTarjeta = "      ";
		fechaComienzo = new Date();
		formaPago = "CB ";
		nroCtaTarjeta = "01020189680000008882";
		codBanco = "VZLAVECA    ";
	}
}
