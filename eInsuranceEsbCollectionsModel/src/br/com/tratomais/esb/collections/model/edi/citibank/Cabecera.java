package br.com.tratomais.esb.collections.model.edi.citibank;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.tratomais.general.utilities.NumberUtilities;

/**
 * Contenido para entrada de los datos de cabecera de los registros de Recaudaci�n de Domiciliaci�n de Cobranza Citibank.
 * @author luiz.alberoni
 */
public class Cabecera implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** Construtor, ajusta o tipo de registro para o default, R1 */
	public Cabecera() {
		this.tipoRegistro = "R1";
	}
	
	/**
	 * Construtor, ajusta o tipo de registro
	 * @param tipoDeRegistro
	 */
	public Cabecera(String tipoDeRegistro) {
		this.tipoRegistro = tipoDeRegistro;
	}
	
	/**
	 * Identificador do tipo de registro de cabecera.
	 */
	private String	tipoRegistro;	//  2	D1
	public String getTipoRegistro() {
		return tipoRegistro;
	}

	/**
	 * N�mero UBN - Referencia interna del n�mero de lote del origen
	 */
	private String	uNBNr;          //  4	UNB Number (nnnn)
	public String getuNBNr() {
		return uNBNr;
	}
	public void setuNBNr(String uNBNr) {
		this.uNBNr = uNBNr;
	}

	/**
	 * Referencia interna del n�mero de lot del origen (exchange)
	 */
	private String	messageNr;      // 12	Message Nbr (prefijo+AAMMCCCC)
	public String getMessageNr() {
		return messageNr;
	}
	public void setMessageNr(String messageNr) {
		this.messageNr = messageNr;
	}

	/**
	 * Fecha de generaci�n del arquivo (formato: AAAMMDD)
	 */
	private Date	fechaDebito;    //  8	Fecha generaci�n mensaje de debito
	public Date getFechaDebito() {
		return fechaDebito;
	}
	public void setFechaDebito(Date fechaDebito) {
		this.fechaDebito = fechaDebito;
	}

	/**
	 * Retorna la cantidad de registros
	 * @return la cantidad de registros
	 */
	private int cantidadRegistros;
	public int getCantidadRegistros() {
		return this.cantidadRegistros;
	}
	public void setCantidadRegistros(int cantidadRegistros) {
		this.cantidadRegistros = cantidadRegistros;
	}
	
	/**
	 * Monto total en d�lares, con dos decimales, separadas por punto (ej.: 123.45)
	 */
	private double totalDolares;
	public double getTotalDolares() {
		return totalDolares;
	}
	public void setTotalDolares(double totalDolares) {
		this.totalDolares = totalDolares;
	}

	/**
	 * Monto total en bol�vares, con dos decimales, separadas por punto  (ej.: 123.45)
	 */
	private double montoTotal;
	public double getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(double montoTotal) {
		this.montoTotal = montoTotal;
	}

	/**
	 *  
	 */
	private List<Detalle> debitRecords = new ArrayList<Detalle>();
	public List<Detalle> getDebitRecords() {
		return debitRecords;
	}
	public void setDebitRecords(List<Detalle> debitRecords) {
		this.debitRecords = debitRecords;
	}
	
	public Detalle addBlank() {
		Detalle detalle = new Detalle();
		this.debitRecords.add(detalle);
		return detalle;
	}
	
	public void atualizaValores() {
		double montoTotal = 0,totalDolares = 0;
		for(Detalle detalle : debitRecords){
			montoTotal = NumberUtilities.roundDecimalPlaces(montoTotal + NumberUtilities.roundDecimalPlaces(detalle.getMontoBol(),2),2);
			totalDolares = NumberUtilities.roundDecimalPlaces(totalDolares + NumberUtilities.roundDecimalPlaces(detalle.getMontoDolares(),2),2);
		}
		this.cantidadRegistros = this.debitRecords.size();
		this.montoTotal = montoTotal;
		this.totalDolares = totalDolares;
	}
	
	public void createTestInstance(){

//		private String	tipoRegistro;	//  2	D1
//		private String	uNBNr;            // 14	UNB Number (nnnn)
//		private String	messageNr;       // 35	Message Nbr (prefijo+AAMMCCCC)
//		private Date	fechaDebito;       //  8	Fecha generaci�n mensaje de debito
//		private String	funcionNegocio;  //  3	SUB: Suscripci�n
//		private String	rifOrdenante;    // 17	Rif  Empresa
//		private String	nombreOrdenante; // 35	Nombre Empresa
//		private String	nroEdiEnvia;     // 18	Nro Edi de quien env�a (ver abajo)
//		private String	nroEdiRecibe;    // 18	Nro Edi de quien recibe (ver abajo)
//		private String	version;		    //  4	Versi�n documento (sugerido D96A)

		
		tipoRegistro ="D1";	
		uNBNr = "00000000000001";          
		messageNr = "999999910042023                    ";      
		fechaDebito = new Date();    
	}
}
