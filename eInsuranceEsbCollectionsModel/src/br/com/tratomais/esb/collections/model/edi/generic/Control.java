package br.com.tratomais.esb.collections.model.edi.generic;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Control data - C1_Control
 * File Header, gives information about the s
 */
public class Control<H,D> implements Serializable {

	private static final long serialVersionUID = 1L;	

	public static final String RECORD_TYPE = "C1";

	public static final String CONTROL_TYPE = "CBR";

	public static final String FILE_VERSION = "00100";

	public Control() {
		this.setRecordType(RECORD_TYPE);
		this.setControlType(CONTROL_TYPE);
		this.setFileVersion(FILE_VERSION);
	}

	private String recordType;
	
	private String controlType;

	private String fileVersion;

	private String partnerCode;

	private String documentType;

	private Integer documentNumber;	

	private H header;

	private List<D> details = new LinkedList<D>();

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getControlType() {
		return controlType;
	}

	public void setControlType(String controlType) {
		this.controlType = controlType;
	}

	public String getFileVersion() {
		return fileVersion;
	}

	public void setFileVersion(String fileVersion) {
		this.fileVersion = fileVersion;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public Integer getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(Integer documentNumber) {
		this.documentNumber = documentNumber;
	}

	public H getHeader() {
		return header;
	}

	public void setHeader(H header) {
		this.header = header;
	}

	public List<D> getDetails() {
		return details;
	}

	public void setDetails(List<D> details) {
		this.details = details;
	}
}
