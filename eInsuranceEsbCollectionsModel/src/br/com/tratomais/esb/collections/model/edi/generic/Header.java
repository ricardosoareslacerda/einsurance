package br.com.tratomais.esb.collections.model.edi.generic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import br.com.tratomais.general.utilities.NumberUtilities;

public class Header implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String RECORD_TYPE = "E1";

	public static final String HEADER_TYPE_SEND = "ZCR";

	public static final String HEADER_TYPE_RECEIVE = "ZCC";

	public static final String CURRENCY_DEFAULT = "VEF";

	public Header() {
		this.setRecordType(RECORD_TYPE);
		this.setHeaderType(HEADER_TYPE_SEND);
		this.setCurrencyCode(CURRENCY_DEFAULT);
	}

	private String recordType;

	private String headerType;

	private Integer lotCode;

	private Date lotDate;

	private String lotReference;

	private String currencyCode;

	private String creditAccount;

	private String documentType;

	private Integer documentNumber;

	private String creditorName;

	private List<Detail> details = new LinkedList<Detail>();

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getHeaderType() {
		return headerType;
	}

	public void setHeaderType(String headerType) {
		this.headerType = headerType;
	}

	public Integer getLotCode() {
		return lotCode;
	}

	public void setLotCode(Integer lotCode) {
		this.lotCode = lotCode;
	}

	public Integer getRecordCount() {
		return (2 + this.details.size()); //control + header + details
	}

	public Date getLotDate() {
		return lotDate;
	}

	public void setLotDate(Date lotDate) {
		this.lotDate = lotDate;
	}

	public String getLotReference() {
		return lotReference;
	}

	public void setLotReference(String lotReference) {
		this.lotReference = lotReference;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	
	public Double getCreditAmount() {
		double creditAmount = 0d;
		for (Detail detail : this.details) {
			creditAmount = NumberUtilities.roundDecimalPlaces(creditAmount + detail.getCreditValue(), 2);
		}
		return creditAmount;
	}

	public String getCreditAccount() {
		return creditAccount;
	}

	public void setCreditAccount(String creditAccount) {
		this.creditAccount = creditAccount;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public Integer getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(Integer documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getCreditorName() {
		return creditorName;
	}

	public void setCreditorName(String creditorName) {
		this.creditorName = creditorName;
	}

	public List<Detail> getDetails() {
		return new ArrayList<Detail>(this.details);
	}

	public Detail addDetail() {
		Detail detail = new Detail();
		this.details.add(detail);
		return detail;
	}
}
