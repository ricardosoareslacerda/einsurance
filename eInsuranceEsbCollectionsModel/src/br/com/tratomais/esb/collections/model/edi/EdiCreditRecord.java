package br.com.tratomais.esb.collections.model.edi;

import java.io.Serializable;
import java.util.Date;

public class EdiCreditRecord  implements Serializable {

	private String	tipoRegistro;  //  2	D2
	private Date	fechaValor;    //  8	Fecha valor
	private String	refCredito;    // 15	Referencia del Cr�dito
	private Double	montoCredito;  // 17	15 enteros, 2 decimales sin punto
	private String	monedaCredito; //  3	VEB: Bol�vares
	private String	nroCtaCredito; // 20	Cuenta a acreditar
	private String	codBanco;      // 12	Banco (tabla SWIFT)  	


	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public Date getFechaValor() {
		return fechaValor;
	}

	public void setFechaValor(Date fechaValor) {
		this.fechaValor = fechaValor;
	}

	public String getRefCredito() {
		return refCredito;
	}

	public void setRefCredito(String refCredito) {
		this.refCredito = refCredito;
	}

	public Double getMontoCredito() {
		return montoCredito;
	}

	public void setMontoCredito(Double montoCredito) {
		this.montoCredito = montoCredito;
	}

	public String getMonedaCredito() {
		return monedaCredito;
	}

	public void setMonedaCredito(String monedaCredito) {
		this.monedaCredito = monedaCredito;
	}

	public String getNroCtaCredito() {
		return nroCtaCredito;
	}

	public void setNroCtaCredito(String nroCtaCredito) {
		this.nroCtaCredito = nroCtaCredito;
	}

	public String getCodBanco() {
		return codBanco;
	}

	public void setCodBanco(String codBanco) {
		this.codBanco = codBanco;
	}

	public void createTestInstance() {
//		private String	tipoRegistro;  //  2	D2
//		private Date	fechaValor;    //  8	Fecha valor
//		private String	refCredito;    // 15	Referencia del Cr�dito
//		private Double	montoCredito;  // 17	15 enteros, 2 decimales sin punto
//		private String	monedaCredito; //  3	VEB: Bol�vares
//		private String	nroCtaCredito; // 20	Cuenta a acreditar
//		private String	codBanco;      // 12	Banco (tabla SWIFT)  	
//
//		private List<EdiDebitRecord> debitRecords;

		tipoRegistro = "D2"; 
		fechaValor = new Date();   
		refCredito = "10042023       ";   
		montoCredito = 3000.30; 
		monedaCredito = "VEB";
		nroCtaCredito = "01050011111111447130";
		codBanco = "BAMRVECA    ";     
		
		
		
	}
	
}
