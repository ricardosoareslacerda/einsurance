package br.com.tratomais.esb.collections.model.request;

import java.io.Serializable;

/**
 * @author leo.costa
 */
public class DetailAttribute implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5161686268921472846L;
	private String name;
	private String value;

	public static final String CERTIFICATE_NUMBER 			= "CertificateNumber";
	public static final String POLICY_NUMBER 				= "PolicyNumber";
	public static final String PARTNER_ID					= "PartnerID";
	public static final String ENDORSEMENT_ID 				= "EndorsementId";
	public static final String CONTRACT_ID 					= "ContractId";
	public static final String CHANNEL_CODE 				= "ChannelCode";
	public static final String PHONE_TYPE 					= "PhoneType";
	public static final String PHONE_NUMBER 				= "PhoneNumber";
	public static final String PARTNER_NAME 				= "PartnerName";
	public static final String POLICY_HOLDER_PERSON_TYPE 	= "PolicyHolderPersonType";
	public static final String INSTALLMENT_ID 				= "InstallmentId";
	public static final String PROPOSAL_NUMBER 				= "ProposalNumber";
	public static final String PRODUCT_CODE 				= "ProductCode";
	public static final String EXTERNAL_KEY 				= "ExternalKey";
	public static final String COMMISSION_VALUE 			= "CommissionValue";
	public static final String LABOUR_VALUE 				= "LabourValue";
	
	public DetailAttribute() {
	}

	public DetailAttribute(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public DetailAttribute(String name, double value) {
		this.name = name;
		this.value = value + "";
	}

	public DetailAttribute(String name, Double value) {
		this.name = name;
		if (value != null) {
			this.value = value.toString();
		}
	}

	public DetailAttribute(String name, int value) {
		this.name = name;
		this.value = value + "";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
