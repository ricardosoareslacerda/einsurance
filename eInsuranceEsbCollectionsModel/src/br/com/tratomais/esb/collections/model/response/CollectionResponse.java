package br.com.tratomais.esb.collections.model.response;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class CollectionResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3554056274907884166L;
	private List<CollectionResponseItem> lista = new LinkedList<CollectionResponseItem>();
	/**
	 * @return the lista
	 */
	public List<CollectionResponseItem> getLista() {
		return lista;
	}

	/**
	 * @param lista the lista to set
	 */
	public void setLista(List<CollectionResponseItem> lista) {
		this.lista = lista;
	}
}
