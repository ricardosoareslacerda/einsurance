package br.com.tratomais.esb.collections.model.edi;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EdiDebitFile implements Serializable {
	private String	tipoRegistro;	//  2	D1
	private String	uNBNr;            // 14	UNB Number (nnnn)
	private String	messageNr;       // 35	Message Nbr (prefijo+AAMMCCCC)
	private Date	fechaDebito;       //  8	Fecha generaci�n mensaje de debito
	private String	funcionNegocio;  //  3	SUB: Suscripci�n
	private String	rifOrdenante;    // 17	Rif  Empresa
	private String	nombreOrdenante; // 35	Nombre Empresa
	private String	nroEdiEnvia;     // 18	Nro Edi de quien env�a (ver abajo)
	private String	nroEdiRecibe;    // 18	Nro Edi de quien recibe (ver abajo)
	private String	version;		    //  4	Versi�n documento (sugerido D96A)
	
	private EdiCreditRecord creditRecord = new EdiCreditRecord();
	private List<EdiDebitRecord> debitRecords = new ArrayList<EdiDebitRecord>();

	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public String getuNBNr() {
		return uNBNr;
	}

	public void setuNBNr(String uNBNr) {
		this.uNBNr = uNBNr;
	}

	public String getMessageNr() {
		return messageNr;
	}

	public void setMessageNr(String messageNr) {
		this.messageNr = messageNr;
	}

	public Date getFechaDebito() {
		return fechaDebito;
	}

	public void setFechaDebito(Date fechaDebito) {
		this.fechaDebito = fechaDebito;
	}

	public String getFuncionNegocio() {
		return funcionNegocio;
	}

	public void setFuncionNegocio(String funcionNegocio) {
		this.funcionNegocio = funcionNegocio;
	}

	public String getRifOrdenante() {
		return rifOrdenante;
	}

	public void setRifOrdenante(String rifOrdenante) {
		this.rifOrdenante = rifOrdenante;
	}

	public String getNombreOrdenante() {
		return nombreOrdenante;
	}

	public void setNombreOrdenante(String nombreOrdenante) {
		this.nombreOrdenante = nombreOrdenante;
	}

	public String getNroEdiEnvia() {
		return nroEdiEnvia;
	}

	public void setNroEdiEnvia(String nroEdiEnvia) {
		this.nroEdiEnvia = nroEdiEnvia;
	}

	public String getNroEdiRecibe() {
		return nroEdiRecibe;
	}

	public void setNroEdiRecibe(String nroEdiRecibe) {
		this.nroEdiRecibe = nroEdiRecibe;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public EdiCreditRecord getCreditRecord() {
		return creditRecord;
	}

	public void setCreditRecord(EdiCreditRecord creditRecord) {
		this.creditRecord = creditRecord;
	}

	public List<EdiDebitRecord> getDebitRecords() {
		return debitRecords;
	}

	public void setDebitRecords(List<EdiDebitRecord> debitRecords) {
		this.debitRecords = debitRecords;
	}

	
	public void createTestInstance(){

//		private String	tipoRegistro;	//  2	D1
//		private String	uNBNr;            // 14	UNB Number (nnnn)
//		private String	messageNr;       // 35	Message Nbr (prefijo+AAMMCCCC)
//		private Date	fechaDebito;       //  8	Fecha generaci�n mensaje de debito
//		private String	funcionNegocio;  //  3	SUB: Suscripci�n
//		private String	rifOrdenante;    // 17	Rif  Empresa
//		private String	nombreOrdenante; // 35	Nombre Empresa
//		private String	nroEdiEnvia;     // 18	Nro Edi de quien env�a (ver abajo)
//		private String	nroEdiRecibe;    // 18	Nro Edi de quien recibe (ver abajo)
//		private String	version;		    //  4	Versi�n documento (sugerido D96A)

		
		tipoRegistro ="D1";	
		uNBNr = "00000000000001";          
		messageNr = "999999910042023                    ";      
		fechaDebito = new Date();    
		funcionNegocio = "SUB"; 
		rifOrdenante = "J0000000000666667";   
		nombreOrdenante = "INVERSORA SSSSSSSSD, C.A.          ";
		nroEdiEnvia = "ZURICHVE          ";    
		nroEdiRecibe = "7591470000000     ";   
		version = "D96A";		

		creditRecord = new EdiCreditRecord();
		creditRecord.createTestInstance();

		debitRecords = new ArrayList<EdiDebitRecord>();
		
		EdiDebitRecord debitRecord = new EdiDebitRecord();
		debitRecord.createTestInstance1();
		debitRecords.add(debitRecord);
		debitRecord = new EdiDebitRecord();
		debitRecord.createTestInstance2();
		debitRecords.add(debitRecord);
		
		
	}
}
