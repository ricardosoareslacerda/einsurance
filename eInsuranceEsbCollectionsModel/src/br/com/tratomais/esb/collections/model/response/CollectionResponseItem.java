package br.com.tratomais.esb.collections.model.response;

import java.io.Serializable;
import java.util.Date;

public class CollectionResponseItem implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4137306343741100565L;

	/** Pago */
	public static int TYPE_PAYMENT 	= 151;
	/** Cancelado */
	public static int TYPE_CANCEL 	= 152;
	/** Pendente */
	public static int TYPE_PENDENT	= 150;
	/** Bloqueado */
	public static int TYPE_BLOCKED  = 569;
	/** Em cobran�a */
	public static int TYPE_IN_COLLECTION = 594;

	private int type;
	private String clientReference;
	private String billingReference;
	private Integer proposalNumber;
	private Integer installmentId;
	private Double paidValue;
	private Date paymentDate;
	private Date cancelDate;
	private Date currencyDate;
	private Double conversionRate;
	private Long bankCheckNumber;
	private Integer invoiceNumber;
	private Long debitAuthorizationCode;
	private Long creditAuthorizationCode;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getClientReference() {
		return clientReference;
	}

	public void setClientReference(String clientReference) {
		this.clientReference = clientReference;
	}

	public String getBillingReference() {
		return billingReference;
	}

	public void setBillingReference(String billingReference) {
		this.billingReference = billingReference;
	}

	public Integer getProposalNumber() {
		return proposalNumber;
	}

	public void setProposalNumber(Integer proposalNumber) {
		this.proposalNumber = proposalNumber;
	}

	public Integer getInstallmentId() {
		return installmentId;
	}

	public void setInstallmentId(Integer installmentId) {
		this.installmentId = installmentId;
	}

	public Double getPaidValue() {
		return paidValue;
	}

	public void setPaidValue(Double paidValue) {
		this.paidValue = paidValue;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public Date getCurrencyDate() {
		return currencyDate;
	}

	public void setCurrencyDate(Date currencyDate) {
		this.currencyDate = currencyDate;
	}

	public Double getConversionRate() {
		return conversionRate;
	}

	public void setConversionRate(Double conversionRate) {
		this.conversionRate = conversionRate;
	}

	public Long getBankCheckNumber() {
		return bankCheckNumber;
	}

	public void setBankCheckNumber(Long bankCheckNumber) {
		this.bankCheckNumber = bankCheckNumber;
	}

	public Integer getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(Integer invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Long getDebitAuthorizationCode() {
		return debitAuthorizationCode;
	}

	public void setDebitAuthorizationCode(Long debitAuthorizationCode) {
		this.debitAuthorizationCode = debitAuthorizationCode;
	}

	public Long getCreditAuthorizationCode() {
		return creditAuthorizationCode;
	}

	public void setCreditAuthorizationCode(Long creditAuthorizationCode) {
		this.creditAuthorizationCode = creditAuthorizationCode;
	}
}
