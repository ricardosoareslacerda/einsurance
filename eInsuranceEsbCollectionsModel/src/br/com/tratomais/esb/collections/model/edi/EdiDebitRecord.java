package br.com.tratomais.esb.collections.model.edi;

import java.io.Serializable;

public class EdiDebitRecord  implements Serializable {
	private String tipoRegistro;            //  2	D3
	private Double montoDebito;             // 17	15 enteros 2 decimales sin punto
	private String monedaDebito;            //  3	VEB = Bol�vares
	private String fechaExpiracionTarjeta;  //  6	Date(AAAAMM)Fecha expiraci�n tarjeta
	private String refCliente;              // 35	Referencia interna del cliente   
	private String refDebito;               //  8	Referencia del d�bito
	private String formaPago;               //  3	Forma, TA: Tarjeta, CB: Cargo en cuenta
	private String nroCtaTarjeta;           // 20	Cuenta � tarjeta donde se aplicar� el d�bito
	private String codBanco;                // 12	Banco donde se aplicar� el d�bito (tabla SWIFT)
	private String rifCliente;          	// 17	Rif Cliente
	private String nombreCliente; 			// 35	Nombre Cliente
	private String phoneNumber;				// 12	N�mero do Celular do Cliente: Opcional

	public String getTipoRegistro() {
		return tipoRegistro;
	}
	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}
	public Double getMontoDebito() {
		return montoDebito;
	}
	public void setMontoDebito(Double montoDebito) {
		this.montoDebito = montoDebito;
	}
	public String getMonedaDebito() {
		return monedaDebito;
	}
	public void setMonedaDebito(String monedaDebito) {
		this.monedaDebito = monedaDebito;
	}
	public String getFechaExpiracionTarjeta() {
		return fechaExpiracionTarjeta;
	}
	public void setFechaExpiracionTarjeta(String fechaExpiracionTarjeta) {
		this.fechaExpiracionTarjeta = fechaExpiracionTarjeta;
	}
	public String getRefCliente() {
		return refCliente;
	}
	public void setRefCliente(String refCliente) {
		this.refCliente = refCliente;
	}
	public String getRefDebito() {
		return refDebito;
	}
	public void setRefDebito(String refDebito) {
		this.refDebito = refDebito;
	}
	public String getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	public String getNroCtaTarjeta() {
		return nroCtaTarjeta;
	}
	public void setNroCtaTarjeta(String nroCtaTarjeta) {
		this.nroCtaTarjeta = nroCtaTarjeta;
	}
	public String getCodBanco() {
		return codBanco;
	}
	public void setCodBanco(String codBanco) {
		this.codBanco = codBanco;
	}
	public String getRifCliente() {
		return rifCliente;
	}
	public void setRifCliente(String rifCliente) {
		this.rifCliente = rifCliente;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void createTestInstance1() {
//		private String tipoRegistro;            //  2	D3
//		private Double montoDebito;             // 17	15 enteros 2 decimales sin punto
//		private String monedaDebito;            //  3	VEB = Bol�vares
//		private String fechaExpiracionTarjeta;  //  6	Date(AAAAMM)Fecha expiraci�n tarjeta
//		private String refCliente;              // 35	Referencia interna del cliente   
//		private String refDebito;               //  8	Referencia del d�bito
//		private String formaPago;               //  3	Forma, TA: Tarjeta, CB: Cargo en cuenta
//		private String nroCtaTarjeta;           // 20	Cuenta � tarjeta donde se aplicar� el d�bito
//		private String codBanco;                // 12	Banco donde se aplicar� el d�bito (tabla SWIFT)
//		private String rifCliente;          	// 17	Rif Cliente
//		private String nombreCliente; 			// 35	Nombre Cliente

		tipoRegistro = "D3";
		montoDebito = 1000.10;
		monedaDebito = "VEB";
		fechaExpiracionTarjeta = "201404";
		refCliente = "19600372133                        ";
		refDebito = "37213304";
		formaPago = "CB ";
		nroCtaTarjeta = "01051111111111111889";
		codBanco = "BAMRVECA    ";
		rifCliente = "V0000000016111114";
		nombreCliente = "VALENCIA DE CCCCCCCC, OLGA         ";
	}

	public void createTestInstance2() {
//		private String tipoRegistro;            //  2	D3
//		private Double montoDebito;             // 17	15 enteros 2 decimales sin punto
//		private String monedaDebito;            //  3	VEB = Bol�vares
//		private String fechaExpiracionTarjeta;  //  6	Date(AAAAMM)Fecha expiraci�n tarjeta
//		private String refCliente;              // 35	Referencia interna del cliente   
//		private String refDebito;               //  8	Referencia del d�bito
//		private String formaPago;               //  3	Forma, TA: Tarjeta, CB: Cargo en cuenta
//		private String nroCtaTarjeta;           // 20	Cuenta � tarjeta donde se aplicar� el d�bito
//		private String codBanco;                // 12	Banco donde se aplicar� el d�bito (tabla SWIFT)
//		private String rifCliente;          	// 17	Rif Cliente
//		private String nombreCliente; 			// 35	Nombre Cliente

		tipoRegistro = "D3";
		montoDebito = 1000.10;
		monedaDebito = "VEB";
		fechaExpiracionTarjeta = "201405";
		refCliente = "19600372139                        ";
		refDebito = "37213904";
		formaPago = "CB ";
		nroCtaTarjeta = "01051111111111152695";
		codBanco = "BAMRVECA    ";
		rifCliente = "V0000000010111111";
		nombreCliente = "FERRARI CCCCCC, CCCCCC CCCCA       ";
	}
}
