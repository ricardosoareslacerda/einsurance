package br.com.tratomais.esb.collections.model.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CollectionEnvelope implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7196554893680845479L;

	/**
	 * Action Type
	 */
	public static final int ACTION_CANCELLATION = 0;
	public static final int ACTION_EFFECTIVE = 1;
	public static final int ACTION_RESPONSE = 2;
	public static final int ACTION_PAY = 3;
	public static final int ACTION_NOTIFY = 4;

	private int action;
	private int applicationId;
	private String lotCode;
	private String lotVersion;
	private Date lotDate;
	private Double amountValue;
	private Double quantityValue;

	private List<CollectionDetail> collectionDetails = new ArrayList<CollectionDetail>();

	public CollectionEnvelope() {
	}

	/**
	 * @param isCancellation
	 */
	public CollectionEnvelope(int action) {
		this.action = action;
	}

	public int getAction() {
		return action;
	}

	public void setAction(int action) {
		this.action = action;
	}

	public int getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public String getLotCode() {
		return lotCode;
	}

	public void setLotCode(String lotCode) {
		this.lotCode = lotCode;
	}

	public String getLotVersion() {
		return lotVersion;
	}

	public void setLotVersion(String lotVersion) {
		this.lotVersion = lotVersion;
	}

	public Date getLotDate() {
		return lotDate;
	}

	public void setLotDate(Date lotDate) {
		this.lotDate = lotDate;
	}

	public Double getAmountValue() {
		return amountValue;
	}

	public void setAmountValue(Double amountValue) {
		this.amountValue = amountValue;
	}

	public Double getQuantityValue() {
		return quantityValue;
	}

	public void setQuantityValue(Double quantityValue) {
		this.quantityValue = quantityValue;
	}

	public List<CollectionDetail> getCollectionDetails() {
		return collectionDetails;
	}

	public void setCollectionDetails(List<CollectionDetail> collectionDetails) {
		this.collectionDetails = collectionDetails;
	}

	/**
	 * add to list collectionDetails
	 * 
	 * @return {@link CollectionDetail}
	 */
	public CollectionDetail addCollectionDetail() {
		CollectionDetail newCollectionDetail = new CollectionDetail();
		
		if (this.getCollectionDetails() == null) {
			this.setCollectionDetails(new ArrayList<CollectionDetail>());
		}
		
		this.getCollectionDetails().add(newCollectionDetail);
		return newCollectionDetail;
	}
}
