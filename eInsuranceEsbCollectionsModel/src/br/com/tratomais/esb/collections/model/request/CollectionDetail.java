package br.com.tratomais.esb.collections.model.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CollectionDetail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1469753666000888997L;

	private int billingMethodId;
	private int installmentType;
	private Integer paymentType;
	private String clientReference;
	private String billingReference;
	private Date issueDate;
	private Date dueDate;
	private Date limitDate;
	private Date paymentDate;
	private Date cancelDate;
	private Double installmentValue;
	private Double paidValue;
	private Integer currencyId;
	private Date currencyDate;
	private Double conversionRate;
	private Date paidCurrencyDate;
	private Double paidConversionRate;
	private String holderName;
	private int holderDocumentType;
	private String holderDocumentNumber;
	private Integer bankNumber;
	private String bankAgencyNumber;
	private String bankAccountNumber;
	private String bankCheckNumber;
	private Integer cardType;
	private Integer cardBrandType;
	private String cardNumber;
	private Integer cardExpirationDate;
	private String cardHolderName;
	private Integer invoiceNumber;
	private String otherAccountNumber;
	private Long debitAuthorizationCode;
	private Long creditAuthorizationCode;
	private Boolean paymentNotified;

	private List<DetailAttribute> detailAttributes = new ArrayList<DetailAttribute>();

	CollectionDetail() {
	}

	public int getBillingMethodId() {
		return billingMethodId;
	}

	public void setBillingMethodId(int billingMethodId) {
		this.billingMethodId = billingMethodId;
	}

	public int getInstallmentType() {
		return installmentType;
	}

	public void setInstallmentType(int installmentType) {
		this.installmentType = installmentType;
	}

	public Integer getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}

	public String getClientReference() {
		return clientReference;
	}

	public void setClientReference(String clientReference) {
		this.clientReference = clientReference;
	}

	public String getBillingReference() {
		return billingReference;
	}

	public void setBillingReference(String billingReference) {
		this.billingReference = billingReference;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getLimitDate() {
		return limitDate;
	}

	public void setLimitDate(Date limitDate) {
		this.limitDate = limitDate;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public Double getInstallmentValue() {
		return installmentValue;
	}

	public void setInstallmentValue(Double installmentValue) {
		this.installmentValue = installmentValue;
	}

	public Double getPaidValue() {
		return paidValue;
	}

	public void setPaidValue(Double paidValue) {
		this.paidValue = paidValue;
	}

	public Integer getCurrencyId() {
		if (currencyId == null) return 0;
		return currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public Date getCurrencyDate() {
		return currencyDate;
	}

	public void setCurrencyDate(Date currencyDate) {
		this.currencyDate = currencyDate;
	}

	public Double getConversionRate() {
		return conversionRate;
	}

	public void setConversionRate(Double conversionRate) {
		this.conversionRate = conversionRate;
	}

	public Date getPaidCurrencyDate() {
		return paidCurrencyDate;
	}

	public void setPaidCurrencyDate(Date paidCurrencyDate) {
		this.paidCurrencyDate = paidCurrencyDate;
	}

	public Double getPaidConversionRate() {
		return paidConversionRate;
	}

	public void setPaidConversionRate(Double paidConversionRate) {
		this.paidConversionRate = paidConversionRate;
	}

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public int getHolderDocumentType() {
		return holderDocumentType;
	}

	public void setHolderDocumentType(int holderDocumentType) {
		this.holderDocumentType = holderDocumentType;
	}

	public String getHolderDocumentNumber() {
		return holderDocumentNumber;
	}

	public void setHolderDocumentNumber(String holderDocumentNumber) {
		this.holderDocumentNumber = holderDocumentNumber;
	}

	public Integer getBankNumber() {
		if (bankNumber == null) return 0;
		return bankNumber;
	}

	public void setBankNumber(Integer bankNumber) {
		this.bankNumber = bankNumber;
	}

	public String getBankAgencyNumber() {
		return bankAgencyNumber;
	}

	public void setBankAgencyNumber(String bankAgencyNumber) {
		this.bankAgencyNumber = bankAgencyNumber;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getBankCheckNumber() {
		return bankCheckNumber;
	}

	public void setBankCheckNumber(String bankCheckNumber) {
		this.bankCheckNumber = bankCheckNumber;
	}

	public Integer getCardType() {
		return cardType;
	}

	public void setCardType(Integer cardType) {
		this.cardType = cardType;
	}

	public Integer getCardBrandType() {
		return cardBrandType;
	}

	public void setCardBrandType(Integer cardBrandType) {
		this.cardBrandType = cardBrandType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Integer getCardExpirationDate() {
		if (cardExpirationDate == null) return 0;
		return cardExpirationDate;
	}

	public void setCardExpirationDate(Integer cardExpirationDate) {
		this.cardExpirationDate = cardExpirationDate;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public Integer getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(Integer invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getOtherAccountNumber() {
		return otherAccountNumber;
	}

	public void setOtherAccountNumber(String otherAccountNumber) {
		this.otherAccountNumber = otherAccountNumber;
	}

	public Long getDebitAuthorizationCode() {
		return debitAuthorizationCode;
	}

	public void setDebitAuthorizationCode(Long debitAuthorizationCode) {
		this.debitAuthorizationCode = debitAuthorizationCode;
	}

	public Long getCreditAuthorizationCode() {
		return creditAuthorizationCode;
	}

	public void setCreditAuthorizationCode(Long creditAuthorizationCode) {
		this.creditAuthorizationCode = creditAuthorizationCode;
	}

	public Boolean getPaymentNotified() {
		return paymentNotified;
	}

	public void setPaymentNotified(Boolean paymentNotified) {
		this.paymentNotified = paymentNotified;
	}

	public List<DetailAttribute> getDetailAttributes() {
		return detailAttributes;
	}

	public void setDetailAttributes(List<DetailAttribute> detailAttributes) {
		this.detailAttributes = detailAttributes;
	}

	/**
	 * add to list detailAttributes
	 * 
	 * @param name
	 *            primitive double value
	 * @param value
	 * @return {@link DetailAttribute}
	 * @author leo.costa
	 */
	public DetailAttribute addAttribute(String name, double value) {
		return this.addAttribute(name, "" + value);
	}

	/**
	 * add to list detailAttributes
	 * 
	 * @param name
	 *            primitive int value
	 * @param value
	 * @return {@link DetailAttribute}
	 * @author leo.costa
	 */
	public DetailAttribute addAttribute(String name, int value) {
		return this.addAttribute(name, "" + value);
	}

	/**
	 * add to list detailAttributes
	 * 
	 * @param name
	 * @param value
	 * @return {@link DetailAttribute}
	 * @author leo.costa
	 */
	public DetailAttribute addAttribute(String name, String value) {
		DetailAttribute newAttribute = new DetailAttribute(name, value);
		if (this.getDetailAttributes() == null) {
			this.setDetailAttributes(new ArrayList<DetailAttribute>());
		}
		this.getDetailAttributes().add(newAttribute);
		return newAttribute;
	}
}
