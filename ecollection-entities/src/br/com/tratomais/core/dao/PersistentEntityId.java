/**
 * 
 */
package br.com.tratomais.core.dao;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;

/**
 * @author eduardo.venancio
 *
 */
public abstract class PersistentEntityId implements PersistentEntity {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public final String toString() {
		StringBuilder result = new StringBuilder();
		String separator = "";
		BeanInfo info;
		try {
			info = Introspector.getBeanInfo(this.getClass());
			result.append( info.getBeanDescriptor().getName() + "`");
			for (PropertyDescriptor pd : info.getPropertyDescriptors())
			{
				String propName = pd.getName();
				if (!"class".equals(propName) && !"annotations".equals(propName) && !"hibernateLazyInitializer".equals(propName) && pd.getReadMethod()!=null)
				{
					Object val;
					try {
						val = pd.getReadMethod().invoke(this,(Object[]) null);
						result.append( separator + propName + "='" + val.toString() + "'" );
						separator = ", ";
					} catch ( IllegalArgumentException e ) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch ( IllegalAccessException e ) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch ( InvocationTargetException e ) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} catch ( IntrospectionException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		result.append( "`" );
		return result.toString();
	}		
}