package br.com.tratomais.core.model.collections;

import java.util.Date;

import br.com.tratomais.core.dao.PersistentEntity;

public class Exchange implements PersistentEntity{

	private static final long serialVersionUID = 1L;

	private Integer exchangeID;
	private Integer parentID;
	private Integer exchangeType;
	private Integer chartAccountID;
	private String name;
	private String description;
	private Integer sendSequenceNumber;
	private Integer recieveSequenceNumber;
	private Date lastSendDate;
	private Date lastRecieveDate;
	private Date lastProcessDate;
	private String processClassMethod;
	private Date registred;
	private Date updated;
	private boolean active;
	
	public Integer getExchangeID() {
		return exchangeID;
	}
	
	public void setExchangeID(Integer exchangeID) {
		this.exchangeID = exchangeID;
	}
	
	public Integer getParentID() {
		return parentID;
	}
	
	public void setParentID(Integer parentID) {
		this.parentID = parentID;
	}
	
	public Integer getExchangeType() {
		return exchangeType;
	}
	
	public void setExchangeType(Integer exchangeType) {
		this.exchangeType = exchangeType;
	}
	
	public Integer getChartAccountID() {
		return chartAccountID;
	}
	
	public void setChartAccountID(Integer chartAccountID) {
		this.chartAccountID = chartAccountID;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getSendSequenceNumber() {
		return sendSequenceNumber;
	}
	
	public void setSendSequenceNumber(Integer sendSequenceNumber) {
		this.sendSequenceNumber = sendSequenceNumber;
	}
	
	public Integer getRecieveSequenceNumber() {
		return recieveSequenceNumber;
	}
	
	public void setRecieveSequenceNumber(Integer recieveSequenceNumber) {
		this.recieveSequenceNumber = recieveSequenceNumber;
	}
	
	public Date getLastSendDate() {
		return lastSendDate;
	}
	
	public void setLastSendDate(Date lastSendDate) {
		this.lastSendDate = lastSendDate;
	}
	
	public Date getLastRecieveDate() {
		return lastRecieveDate;
	}
	
	public void setLastRecieveDate(Date lastRecieveDate) {
		this.lastRecieveDate = lastRecieveDate;
	}
	
	public Date getLastProcessDate() {
		return lastProcessDate;
	}
	
	public void setLastProcessDate(Date lastProcessDate) {
		this.lastProcessDate = lastProcessDate;
	}
	
	public String getProcessClassMethod() {
		return processClassMethod;
	}
	
	public void setProcessClassMethod(String processClassMethod) {
		this.processClassMethod = processClassMethod;
	}
	
	public Date getRegistred() {
		return registred;
	}
	
	public void setRegistred(Date registred) {
		this.registred = registred;
	}
	
	public Date getUpdated() {
		return updated;
	}
	
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
	public boolean isActive() {
		return active;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
}
