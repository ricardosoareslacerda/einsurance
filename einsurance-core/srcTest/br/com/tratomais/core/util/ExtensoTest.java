package br.com.tratomais.core.util;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ExtensoTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double Valor = 160.61;
		StringBuilder sbNumeroPorExtenso = new StringBuilder();
		
		if ( formataNumeroPorExtenso( Valor, "real", sbNumeroPorExtenso ) ) {
			System.out.println(sbNumeroPorExtenso.toString());
		}
	}

    /**
	    * Metodo para formatar um n�mero por extenso, de acordo com o tipo passado.
	    * Limita��o do Metodo: O metodo suportar� apenas n�meros com duas casas decimais...
	    * Data de cria��o: (12/06/01 17:07:52)
	    * @return						- boolean		- Se tudo ok, true, sen�o false...
	    * @param	nNumero			- String			- N�mero a ser transformado em extenso
	    * @param	sTipoEscrita	- int				- Real, Inteiro ou Porcento...
	    * @param	sbNomeUnidade	- StringBuffer	- Atributo com o retorno do m�todo, se ok cont�m o n�mero por extenso, sen�o cont�m uma msg de erro...
	    */
	    public static boolean formataNumeroPorExtenso(
	        double nNumero,
	        String sTipoEscrita,
	        StringBuilder sbNumeroPorExtenso)
	    {
	        /************************************** Declara��o **************************************/
	        ArrayList< String > alUnidade;
	        ArrayList< String > alDezena;
	        ArrayList< String > alCentena;
	        ArrayList< String > alGrupo;
	        ArrayList< String > alTexto;
	        boolean bTemE = false;
	        DecimalFormat dfExtensao = new DecimalFormat("000000000000.00");
	        String sNumero = dfExtensao.format(nNumero);
	        String sParte = "";
	        String sAux = "";
	        String sAlimenta = "";
	        int nCont = 0;
	        int nTamanho = 0;
	        int nTeste = 0;
	        int nTestaDecimal = 0;
	        //	StringBuffer sbFinal;
	        /****************************************************************************************/
	        alUnidade = new ArrayList< String >();
	        alDezena = new ArrayList< String >();
	        alCentena = new ArrayList< String >();
	        alGrupo = new ArrayList< String >();
	        alTexto = new ArrayList< String >();
	        //	sbNumeroPorExtenso = new StringBuffer();
	        try
	        {
	            // Unidades...
	            alUnidade.add(0, "UN "); //0
	            alUnidade.add(1, "DOS "); //1
	            alUnidade.add(2, "TRES "); //2
	            alUnidade.add(3, "CUATRO "); //3
	            alUnidade.add(4, "CINCO "); //4
	            alUnidade.add(5, "SEIS "); //5
	            alUnidade.add(6, "SIETE "); //6
	            alUnidade.add(7, "OCHO "); //7
	            alUnidade.add(8, "NUEVE "); //8
	            alUnidade.add(9, "DIEZ "); //9
	            alUnidade.add(10, "ONCE "); //10
	            alUnidade.add(11, "DOCE "); //11
	            alUnidade.add(12, "TRECE "); //12
	            alUnidade.add(13, "CATORCE "); //13
	            alUnidade.add(14, "QUINCE "); //14
	            alUnidade.add(15, "DIECISEIS "); //15
	            alUnidade.add(16, "DIECISIETE "); //16
	            alUnidade.add(17, "DIECIOCHO "); //17
	            alUnidade.add(18, "DIECINUEVE "); //18
	            alUnidade.add(19, "VEINTE "); //18
	            alUnidade.add(20, "VEINTIUNO "); //18
	            alUnidade.add(21, "VEINTID�S "); //18
	            alUnidade.add(22, "VEINTITR�S "); //18
	            alUnidade.add(23, "VEINTICUATRO "); //18
	            alUnidade.add(24, "VEINTICINCO "); //18
	            alUnidade.add(25, "VEINTIS�IS "); //18
	            alUnidade.add(26, "VEINTISIETE "); //18
	            alUnidade.add(27, "VEINTIOCHO "); //18
	            alUnidade.add(28, "VEINTINUEVE "); //18	            
	            // Dezenas...
	            alDezena.add(0, ""); //0
	            alDezena.add(1, ""); //0
	            alDezena.add(2, "TRENTA "); //1
	            alDezena.add(3, "CUARENTA "); //2
	            alDezena.add(4, "CINCUENTA "); //3
	            alDezena.add(5, "SESENTA "); //4
	            alDezena.add(6, "SETENTA "); //5
	            alDezena.add(7, "OCHENTA "); //6
	            alDezena.add(8, "NOVENTA "); //7
	            // Centenas
	            alCentena.add(0, "CIENTO "); //0
	            alCentena.add(1, "DOSCIENTOS "); //1
	            alCentena.add(2, "TRESCIENTOS "); //2
	            alCentena.add(3, "CUATROCIENTOS "); //3
	            alCentena.add(4, "QUINIENTOS "); //4
	            alCentena.add(5, "SEISCIENTOS "); //5
	            alCentena.add(6, "SIETECIENTOS "); //6
	            alCentena.add(7, "OCHOCIENTOS "); //7
	            alCentena.add(8, "NOVECIENTOS "); //8
	            alCentena.add(9, "CIEN "); //9
	            // Dividir o n�mero em grupos...
	            alGrupo.add(0, sNumero.substring(0, 3)); // Primeira parte
	            alGrupo.add(1, sNumero.substring(3, 6)); // Segunda Parte
	            alGrupo.add(2, sNumero.substring(6, 9)); // Terceira Parte
	            alGrupo.add(3, sNumero.substring(9, 12)); // Quarta Parte
	            alGrupo.add(4, sNumero.substring(13)); // Decimal
	            // Loop para calcular os grupos...
	            for (nCont = 0; nCont < 5; nCont++)
	            {
	                // Iniciar o vetor...
	                alTexto.add(nCont, "");
	                // Recuperar o grupos armazenados...
	                sParte = (String) alGrupo.get(nCont);
	                // Recuperar para teste...
	                nTeste = Integer.parseInt(sParte);
	                // Recuperar o tamanho...
	                if (nTeste < 10)
	                {
	                    nTamanho = 1;
	                }
	                else
	                {
	                    if (nTeste < 100)
	                    {
	                        nTamanho = 2;
	                    }
	                    else
	                    {
	                        if (nTeste < 1000)
	                        {
	                            nTamanho = 3;
	                        }
	                    }
	                }
	                // Testar o tamanho...
	                if (nTamanho == 3)
	                {
	                    if (!sParte.substring(1).equals("00"))
	                    {
	                        sAlimenta =
	                            (String) alTexto.get(nCont)
	                                + alCentena.get(
	                                    Integer.parseInt(sParte.substring(0, 1))
	                                        - 1)
	                                + "";
	                        alTexto.set(nCont, sAlimenta);
	                        bTemE = true;
	                        nTamanho = 2;
	                    }
	                    else
	                    {
	                        if (Integer.parseInt(sParte.substring(0, 1)) == 1)
	                        {
	                            sAlimenta =
	                                (String) alTexto.get(nCont) + alCentena.get(9);
	                            alTexto.set(nCont, sAlimenta);
	                        }
	                        else
	                        {
	                            sAlimenta =
	                                (String) alTexto.get(nCont)
	                                    + alCentena.get(
	                                        Integer.parseInt(
	                                            sParte.substring(0, 1))
	                                            - 1);
	                            alTexto.set(nCont, sAlimenta);
	                        }
	                    }
	                }
	                if (nTamanho == 2)
	                {
	                    if (nCont == 4) // Est� no decimal...
	                        nTestaDecimal = Integer.parseInt(sParte);
	                    else
	                        nTestaDecimal = Integer.parseInt(sParte.substring(1));
	                    if (nTestaDecimal < 30)
	                    {
	                    	if(nCont == 4){
		                        sAlimenta =
		                            (String) alTexto.get(nCont)
		                                + alUnidade.get(
		                                    Integer.parseInt(sParte) - 1);
		                        alTexto.set(nCont, sAlimenta);	                    		
	                    	}else{
		                        sAlimenta =
		                            (String) alTexto.get(nCont)
		                                + alUnidade.get(
		                                    Integer.parseInt(sParte.substring(1)) - 1);
		                        alTexto.set(nCont, sAlimenta);	                    		
	                    	}
	                    }
	                    else
	                    {
	                        if (nCont == 4)
	                        {
	                            sAlimenta =
	                                (String) alTexto.get(nCont)
	                                    + alDezena.get(
	                                        Integer.parseInt(
	                                            sParte.substring(0, 1))
	                                            - 1);
	                            alTexto.set(nCont, sAlimenta);
	                        }
	                        else
	                        {
	                            sAlimenta =
	                                (String) alTexto.get(nCont)
	                                    + alDezena.get(
	                                        Integer.parseInt(
	                                            sParte.substring(1, 2))
	                                            - 1);
	                            alTexto.set(nCont, sAlimenta);
	                        }
	                        if (nCont == 4)
	                        {
	                            if (!sParte.substring(2).equals("0"))
	                            {
	                                sAlimenta = (String) alTexto.get(nCont) + "Y ";
	                                alTexto.set(nCont, sAlimenta);
	                                nTamanho = 1;
	                                bTemE = true;
	                            }
	                        }
	                        else
	                        {
	                            if (!sParte.substring(1).equals("0"))
	                            {
	                                sAlimenta = (String) alTexto.get(nCont) + "Y ";
	                                alTexto.set(nCont, sAlimenta);
	                                nTamanho = 1;
	                                bTemE = true;
	                            }
	                        }
	                    }

	                }
	                if (nTamanho == 1)
	                {
	                    if (nTeste > 0)
	                    {
	                        if (nCont == 4)
	                        {
	                        	sAlimenta = "CERO ";
	                            sAlimenta +=
	                                (String) alTexto.get(nCont)
	                                    + alUnidade.get(
	                                        Integer.parseInt(sParte.substring(1))
	                                            - 1);
	                        }
	                        else
	                        {
	                            sAlimenta =
	                                (String) alTexto.get(nCont)
	                                    + alUnidade.get(
	                                        Integer.parseInt(sParte.substring(2))
	                                            - 1);
	                        }
	                        alTexto.set(nCont, sAlimenta);
	                    }
	                }
	            }
	            // Verificar se tem somente centavos...
	            if (Integer
	                .parseInt(
	                    (String) alGrupo.get(0)
	                        + (String) alGrupo.get(1)
	                        + (String) alGrupo.get(2)
	                        + (String) alGrupo.get(3))
	                == 0
	                && Integer.parseInt((String) alGrupo.get(4)) != 0)
	            {
	                // Testar qual a termina��o (real ou percentual)
	                if (sTipoEscrita.equalsIgnoreCase("real"))
	                {
	                    if (((String) alGrupo.get(4)).substring(0).equals("1"))
	                    {
	                        sAux = "CENTIMO ";
	                    }
	                    else
	                    {
	                        sAux = "CENTIMOS ";
	                    }
	                }
	                else
	                {
	                    if (((String) alGrupo.get(4)).substring(0).equals("1"))
	                    {
	                        sAux = "CENT�SIMO ";
	                    }
	                    else
	                    {
	                        sAux = "CENT�SIMOS ";
	                    }
	                }
	                sbNumeroPorExtenso.append(alTexto.get(4) + sAux);
	            }
	            else
	            {
	                // Verificar se coloca o E
	                if (bTemE
	                    || Integer.parseInt(
	                        (String) alGrupo.get(0)
	                            + (String) alGrupo.get(1)
	                            + (String) alGrupo.get(2))
	                        == 0
	                    || Integer.parseInt((String) alGrupo.get(3)) == 0)
	                {
	                    sbNumeroPorExtenso.append("");
	                }
	                else
	                {
	                    sbNumeroPorExtenso.append("Y ");
	                }
	                // Escrever...
	                // Bilh�es
	                if (Integer.parseInt((String) alGrupo.get(0)) != 0)
	                {
	                    sbNumeroPorExtenso.append(alTexto.get(0));
	                    if (Integer.parseInt((String) alGrupo.get(0)) > 1)
	                    {
	                        sbNumeroPorExtenso.append("MIL MILLONES ");
	                    }
	                    else
	                    {
	                        sbNumeroPorExtenso.append("MIL MILL�N ");
	                    }
	                }
	                // Milh�es
	                if (Integer.parseInt((String) alGrupo.get(1)) != 0)
	                {
	                    sbNumeroPorExtenso.append(alTexto.get(1));
	                    if (Integer.parseInt((String) alGrupo.get(1)) > 1)
	                    {
	                        sbNumeroPorExtenso.append("MILLONES ");
	                    }
	                    else
	                    {
	                        sbNumeroPorExtenso.append("MILL�N ");
	                    }
	                }
	                // Milhar
	                if (Integer.parseInt((String) alGrupo.get(2)) != 0)
	                {
	                    sbNumeroPorExtenso.append(alTexto.get(2));
	                    sbNumeroPorExtenso.append("MIL ");
	                }
	                // Unidade/Dezena
	                sbNumeroPorExtenso.append(alTexto.get(3));
	                // Testar qual a termina��o (real Inteiro ou percentual)
	                if (sTipoEscrita.equalsIgnoreCase("real"))
	                {
	                    // Coloca "DE" se necess�rio...
	                    if (Integer
	                        .parseInt(
	                            (String) alGrupo.get(2) + (String) alGrupo.get(3))
	                        == 0)
	                    {
	                        sbNumeroPorExtenso.append("DE ");
	                    }
	                    // Coloca "REAIS" ou "REAL"
	                    if (Integer
	                        .parseInt(
	                            (String) alGrupo.get(0)
	                                + (String) alGrupo.get(1)
	                                + (String) alGrupo.get(2)
	                                + (String) alGrupo.get(3))
	                        == 1)
	                    {
	                        sbNumeroPorExtenso.append("BOL�VAR FUERTE ");
	                    }
	                    else
	                    {
	                        sbNumeroPorExtenso.append("BOL�VARES FUERTES ");
	                    }
	                    // Testa se tem casas decimais...
	                    if (Integer.parseInt((String) alGrupo.get(4)) != 0)
	                    {
	                        // Coloca os valores decimais...
	                        sbNumeroPorExtenso.append(
	                            "CON " + (String) alTexto.get(4));
	                        // Coloca centavos...
	                        if (Integer.parseInt((String) alGrupo.get(4)) == 1)
	                        {
	                            sbNumeroPorExtenso.append("CENTIMO ");
	                        }
	                        else
	                        {
	                            sbNumeroPorExtenso.append("CENTIMOS ");
	                        }
	                    }
	                    // verifica se � do tipo de escrita � inteiro	
	                }
	                else if (sTipoEscrita.equalsIgnoreCase("Inteiro"))
	                {
	                    if (Integer.parseInt((String) alGrupo.get(4)) != 0)
	                    {
	                        sbNumeroPorExtenso.append("Y " + alTexto.get(4));
	                    }
	                    else
	                        sbNumeroPorExtenso.append(alTexto.get(4));

	                }
	                else
	                {
	                    // Coloca "PORCENTO"
	                    sbNumeroPorExtenso.append("PORCENTO ");
	                    // Testa se tem casas decimais...
	                    if (Integer.parseInt((String) alGrupo.get(4)) != 0)
	                    {
	                        // Coloca os valores decimais...
	                        sbNumeroPorExtenso.append("Y " + alTexto.get(4));
	                        // Coloca CENT�SIMOS...
	                        if (Integer.parseInt((String) alGrupo.get(4)) == 1)
	                        {
	                            sbNumeroPorExtenso.append("CENT�SIMO ");
	                        }
	                        else
	                        {
	                            sbNumeroPorExtenso.append("CENT�SIMOS ");
	                        }
	                    }
	                }
	            }
	            // Retornar...
	            //		sbNumeroPorExtenso.append(sbFinal);
	            return true;
	        }
	        catch (Exception eError)
	        {
	            sbNumeroPorExtenso.append(eError.getMessage());
	            return false;
	        }
	    }
	
}
