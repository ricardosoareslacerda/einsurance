package br.com.tratomais.core.util;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

import br.com.tratomais.core.util.DateUtilities.DiferencaData;
public class DateUtilitiesTest {
	private static Date getDate(int day, int month, int year) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, day);
		cal.set(Calendar.MONTH, month - 1);
		cal.set(Calendar.YEAR, year);
		return cal.getTime();
	}
	

	@Test
	public void testActuarialAgeByDate() {
		Date data35, data36, dataComp;
		data35 = getDate(16,1,1975);
		data36 = getDate(15,1,1975);
		dataComp = getDate(16,7,2010);
		assertEquals(36, DateUtilities.actuarialAgeByDate(data36, dataComp));
		assertEquals(35, DateUtilities.actuarialAgeByDate(data35, dataComp));
	}
	
	@Test
	public void testAgeByDate() {
		Date data16, data17, dataComp;
		data16 = getDate(16,7,1992);
		data17 = getDate(17,7,1992);
		dataComp = getDate(16,7,2010);
		assertEquals(18, DateUtilities.ageByDate(data16, dataComp));		
		assertEquals(17, DateUtilities.ageByDate(data17, dataComp));
	}
	
	@Test
	public void testDateDifferences(){
		Date dataInicio = getDate(3,4,1975),
			 dataFinal  = getDate(3,4,1976);
		DiferencaData dateDifference = DateUtilities.getDateDifference(dataInicio, dataFinal);
		assertEquals(0, dateDifference.getDias());
		assertEquals(0, dateDifference.getMeses());
		assertEquals(1, dateDifference.getAnos());
		dataFinal = getDate(4,4,1976);
		dateDifference = DateUtilities.getDateDifference(dataInicio, dataFinal);
		assertEquals(1, dateDifference.getDias());
		assertEquals(0, dateDifference.getMeses());
		assertEquals(1, dateDifference.getAnos());
		dataFinal = getDate(4,5,1976);
		dateDifference = DateUtilities.getDateDifference(dataInicio, dataFinal);
		assertEquals(1, dateDifference.getDias());
		assertEquals(1, dateDifference.getMeses());
		assertEquals(1, dateDifference.getAnos());
		dataFinal = getDate(2,6,1976);
		dateDifference = DateUtilities.getDateDifference(dataInicio, dataFinal);
		assertEquals(29, dateDifference.getDias());
		assertEquals(1, dateDifference.getMeses());
		assertEquals(1, dateDifference.getAnos());
		dataInicio = getDate(26,7,2010);
		dataFinal = getDate(26,8,2010);
		dateDifference = DateUtilities.getDateDifference(dataInicio, dataFinal);
		assertEquals(0, dateDifference.getDias());
		assertEquals(1, dateDifference.getMeses());
		assertEquals(0, dateDifference.getAnos());
		dataInicio = getDate(26,7,2010);
		dataFinal = getDate(26,10,2010);
		dateDifference = DateUtilities.getDateDifference(dataInicio, dataFinal);
		assertEquals(0, dateDifference.getDias());
		assertEquals(3, dateDifference.getMeses());
		assertEquals(0, dateDifference.getAnos());
		dataInicio = getDate(27,7,2010);
		dataFinal = getDate(27,1,2011);
		dateDifference = DateUtilities.getDateDifference(dataInicio, dataFinal);
		assertEquals(0, dateDifference.getDias());
		assertEquals(6, dateDifference.getMeses());
		assertEquals(0, dateDifference.getAnos());
	}

}
