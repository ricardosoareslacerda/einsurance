package br.com.tratomais.core.util;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.com.tratomais.general.utilities.NumberUtilities;

/**
 * Testing the numbers class
 * @author luiz.alberoni
 */
public class NumberUtilitiesTest {
	private double [] valores = {3.0,3.0,3.0,3.0,3.0,3.0};
	
	
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Testes the rounding division routine
	 */
	@Test
	public void testDivideArredonda() {
		//assertEquals(3.33, NumberUtilities.roundingDivision(10.0, 0.3333333333333333), 0.001);
	}

	/**
	 * Tests the division process - proportional to values passed
	 */
	@Test
	public void testDivideMaiorPrimeira() {
		double[] retorno = NumberUtilities.dividesWithBiggestLast(100.0, valores, 2);
		for(int aux=0;aux<retorno.length;aux++)
			assertEquals(aux==0?16.70:16.66, retorno[aux], 0.001);
	}

}
