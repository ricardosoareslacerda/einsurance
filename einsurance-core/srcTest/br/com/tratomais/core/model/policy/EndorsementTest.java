package br.com.tratomais.core.model.policy;

import org.junit.Before;

import junit.framework.TestCase;

public class EndorsementTest extends TestCase {
	private final static int MAX_ITEM = 2;
	private final static int MAX_ITEM_COVERAGE = 3;
		
	private Endorsement endor;
	
	private Item newItem(int i) {
		Item it = new ItemPersonalRisk();
		double valor = 0;
		for(int aux=0;aux<MAX_ITEM_COVERAGE; aux++){;
			ItemCoverage ic = newItemCoverage(i+1, aux+1);
			valor += ic.getNetPremium();
			it.getItemCoverages().add(ic);
		}
		it.setNetPremium(valor);
		return it;
	}

	private ItemCoverage newItemCoverage(int i, int aux) {
		ItemCoverage ic = new ItemCoverage();
		ic.setNetPremium((double)i*aux);
		return ic;
	}
	
	public void testeAlgo() {
		endor.setTariffPremium(100.0);
		System.out.println("-------------- OIA ISSO ------------------------");
		System.out.println(endor.getNetPremium());
		for(Item it : this.endor.getItems()){
			System.out.println("\t-----> ITEM:" + it);
			System.out.println("\t" + it.getNetPremium());
			for(ItemCoverage ic : it.getItemCoverages()){
				System.out.println("\t\t-----> ITEMCOVERAGE:" + ic);
				System.out.println("\t\t" + ic.getNetPremium());
			}
		}
	}
	
	public void testeAlgo2() {
		endor.setNetPremium(1000.00);
		endor.splitTotalValues();
		System.out.println("-------------- OIA ISSO 2------------------------");
		System.out.println(endor.getNetPremium());
		System.out.println(endor.getTariffPremium());
		for(Item it : this.endor.getItems()){
			System.out.println("\t-----> ITEM:" + it);
			System.out.println("\t" + it.getNetPremium());
			System.out.println("\t" + it.getTariffPremium());
			for(ItemCoverage ic : it.getItemCoverages()){
				System.out.println("\t\t-----> ITEMCOVERAGE:" + ic);
				System.out.println("\t\t" + ic.getNetPremium());
				System.out.println("\t\t" + ic.getTariffPremium());
			}
		}
		
	}
	
	protected void setUp() throws Exception {
		super.setUp();
		endor = new Endorsement();
		double somaTaxa=0;
		for(int aux=0;aux<MAX_ITEM;aux++){
			Item it = newItem(aux);
			somaTaxa += it.getNetPremium();
			endor.getItems().add(it);
		}
		endor.setNetPremium(somaTaxa);
	}
}
