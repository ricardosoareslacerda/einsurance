package br.com.tratomais.core.model.policy;

import java.io.File;
import java.util.Date;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class ItemPersonalRiskTest extends TestCase {
	static {
		PropertyConfigurator.configure("C:\\workspace\\prototipo-core-v1\\src\\log4j.xml");
		Configuration conf = new Configuration();
		conf.configure(new File("C:\\workspace\\prototipo-core-v1\\src\\config\\hibernate-config.cfg.xml"));
		conf.setProperty("hibernate.show_sql", "true");
		conf.setProperty("hibernate.format_sql", "true");
		fac = conf.buildSessionFactory();
	}

	private static SessionFactory fac;
	private Session ses;

	public void testItemPersonalRisk() {
		Logger.getLogger(ItemPersonalRiskTest.class).info("HEY");
		System.out.println("Rodando testItemPersonalRisk");
		ses = fac.openSession();
		Endorsement endorsement = getEndorsement();
		Transaction transaction = ses.beginTransaction();
		ItemPersonalRisk risk = new  ItemPersonalRisk();
		risk.setId(new ItemId(4, endorsement.getId().getEndorsementId(),endorsement.getId().getContractId() ));
		risk.setEffectiveDate(new Date());
		risk.setInsuredName("InsuredName 4");
		risk.setCoveragePlanId(1);
		risk.setCommercialPremium(4.0);

		endorsement.getItems().add(risk);
		ses.saveOrUpdate(endorsement);
		transaction.rollback();
		ses.close();
	}

	private Endorsement getEndorsement() {
		Endorsement saida = (Endorsement) ses.load(Endorsement.class, new EndorsementId(1,11));

		System.err.println(saida);
		return  saida;
	}
}
