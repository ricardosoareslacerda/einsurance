package br.com.tratomais.core.model.policy;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.tratomais.core.dao.IDaoSearchDocument;
import br.com.tratomais.core.model.search.SearchDocumentParameters;
import br.com.tratomais.core.model.search.SearchDocumentResult;
@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/config/spring-security-config.xml", "classpath:/config/spring-application-context.xml","classpath:/config/spring-application-daos.xml","classpath:/config/spring-application-services.xml"})
public class CertificateReportTest {

	@Autowired private IDaoSearchDocument daoSearchDocument;
	 
	@Before
	public void setup() throws Exception {
		Logger.getLogger(MasterPolicy.class).info("HEY");
	}
	 
	@After
	public void teardown() throws Exception {
	
	}


	@Test
	@Transactional
	public void testItemPropertyTest() {
		Logger.getLogger(MasterPolicy.class).info("Rodando Master Policy");
		try {
			SearchDocumentParameters searchDocumentParameters = new SearchDocumentParameters();
			searchDocumentParameters.setContractId( 3 );
			searchDocumentParameters.setCertificateNumber( "" );
			searchDocumentParameters.setDocumentNumber( "" );
			searchDocumentParameters.setDocumentType( 0 );
			searchDocumentParameters.setName( "" );
			searchDocumentParameters.setPartnerId( 0 );
			List< SearchDocumentResult > mpList = daoSearchDocument.listSearchDocument( searchDocumentParameters  );

			for ( SearchDocumentResult certificateReportResult : mpList ) {
				Logger.getLogger(MasterPolicy.class).info("CertRep :" + certificateReportResult.getCertificateNumber() 
						+ " prod: " + certificateReportResult.getProductName()  + " contr: " + certificateReportResult.getContractId());
			}
			

			
		} catch ( Exception e ) {
			Logger.getLogger(MasterPolicy.class).info("Erro1" + e.toString());
			Logger.getLogger(MasterPolicy.class).info("Erro2" + e.getMessage());
			e.printStackTrace();
		}
		
	}
}
