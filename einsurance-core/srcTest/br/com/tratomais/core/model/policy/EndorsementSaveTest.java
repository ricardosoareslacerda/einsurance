package br.com.tratomais.core.model.policy;

import java.util.Calendar;
import java.util.Date;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.model.customer.Address;
import br.com.tratomais.core.model.customer.Customer;

@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/config/spring-security-config.xml", "classpath:/config/spring-application-context.xml", "classpath:/config/spring-application-daos.xml",
		"classpath:/config/spring-application-services.xml" })
public class EndorsementSaveTest {

	@Autowired
	private IDaoPolicy daoPolicy;
	@Autowired
//	private IDaoCustomer daoCustomer;

	@Before
	public void setup() throws Exception {
	}

	@After
	public void teardown() throws Exception {

	}

	private static SessionFactory fac;
	private Session ses;

	//@Test
	@Transactional
	public void testListContract(){
		Contract contract = daoPolicy.listContract( 11 );
		Logger.getLogger(Endorsement.class).info("Contract id : " + contract.getContractId());
		
		for ( Endorsement end : contract.getEndorsements() ) {
			Logger.getLogger(Endorsement.class).info("Endorsement id : " + end.getId().getEndorsementId());
		}
		
	}
	
	@Test
	@Transactional
	public void testItemPropertyTest() {
		Logger.getLogger(Customer.class).info("Rodando Customer");
		try {
			//Endorsement endorsement = new Endorsement();

			Contract contract = this.createContract();
			
			Endorsement endorsement = contract.addEndorsement();
			

			Customer customer = this.createCustomer();
			endorsement.setCustomerByInsuredId(customer);

			//EndorsementId endorsementId = new EndorsementId();
			//endorsementId.setContractId(contract.getContractId());
			//endorsement.setUserId(AuthenticationHelper.getLoggedUser().getUserId());
			endorsement.setChannelId(1);
			endorsement.setTermId(1);
			endorsement.setPaymentTermId(1);
			// endorsement.setRiskPlanId();
			endorsement.setHierarchyType(1);
			endorsement.setReferenceDate(new Date(System.currentTimeMillis()));
			endorsement.setEffectiveDate(new Date(System.currentTimeMillis()));
			if (endorsement.getTermId() == 1) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(endorsement.getEffectiveDate());
				calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + 1);
				endorsement.setExpiryDate(calendar.getTime());
			}
			endorsement.setQuotationDate(endorsement.getReferenceDate());
			endorsement.setQuotationNumber(1);
			endorsement.setNetPremium(1.0);
			endorsement.setTotalPremium(10000.00);
			endorsement.setTransmitted(false);
			endorsement.setIssuingStatus(1);

			Endorsement savedEndorsement = daoPolicy.saveEndorsement(endorsement);
			Logger.getLogger(Endorsement.class).info("Endorsement id : " + savedEndorsement.getId().getEndorsementId());
			Assert.assertNotNull(savedEndorsement);
		} catch (Exception e) {
			Logger.getLogger(Customer.class).info("Erro1" + e.toString());
			Logger.getLogger(Customer.class).info("Erro2" + e.getMessage());
			e.printStackTrace();
			Assert.fail();
		}

	}

	/**
	 * 
	 */
	private Contract createContract() {
		Contract contract = new Contract();
//		MasterPolicy masterPolicy = daoPolicy.findMasterPolicyById(1);
//		contract.setMasterPolicy(masterPolicy);
		contract.setInsurerId(1);
		contract.setSubsidiaryId(1);
		contract.setBrokerId(1);
		contract.setPartnerId(1);
		contract.setProductId(1);
		contract.setCurrencyId(1);
		contract.setContractStatus(1);
		contract.setTermId(1);
		contract.setPolicyInformed(false);
		contract.setReferenceDate(new Date(System.currentTimeMillis()));
		contract.setEffectiveDate(new Date(System.currentTimeMillis()));
		if (contract.getTermId() == 1) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(contract.getEffectiveDate());
			calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + 1);
			contract.setExpiryDate(calendar.getTime());
		}
//		contract = daoPolicy.saveContract(contract);
		return contract;
	}

	private Customer createCustomer() {
		Customer customer = new Customer();
		customer.setName("Name 22");
		customer.setLastName("Surname 22");
		customer.setFirstName("Name");
		customer.setTradeName("4");
		customer.setPersonType(4);
		customer.setGender(112);
		customer.setBirthDate(new Date());
		customer.setRegisterDate(new Date());
		customer.setMaritalStatus(116);
		customer.setDocumentType(75);
		customer.setDocumentNumber("1");
		customer.setDocumentIssuer("1");
		customer.setDocumentValidity(new Date());
		customer.setProfessionType(1);
		customer.setProfessionId(1);
		customer.setOccupationId(2);
		customer.setActivityId(1);
		customer.setInflowId(1);
		customer.setEmailAddress("1");
		// customer.setAddressId(null);
		// customer.setPhoneId(null);
		customer.setActive(true);

		Address address = new Address();
		address.setActive(true);

		// address.setCustomerId( customerId );
		address.setAddressType(22);
		address.setAddressStreet("Rua 1");
		address.setDistrictName("bairro");
		// address.setCityId( cityId );
		address.setCityName("Cidade");
		address.setRegionName("Regi�o");
		// address.setStateId( stateId );
		address.setStateName("Estado");
		address.setZipCode("12345");
		// address.setCountryId( countryId );
		address.setCountryName("Brasil");
		address.setActive(true);

		// customer.setAddress( address );
		customer.getAddresses().add(address);
		address.setCustomer(customer);
		// address.getCustomers().add( customer );

		//Customer newCustomer = daoCustomer.save(customer);
		//Logger.getLogger(Customer.class).info("Cliente" + newCustomer.getCustomerId());
		return customer;
	}

}
