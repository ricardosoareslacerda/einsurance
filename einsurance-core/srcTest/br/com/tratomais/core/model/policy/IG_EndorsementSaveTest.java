package br.com.tratomais.core.model.policy;

import java.util.Calendar;
import java.util.Date;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.hibernate.type.Type;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.infrastructure.AuthenticationHelper;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.customer.Address;
import br.com.tratomais.core.model.customer.Customer;
import br.com.tratomais.core.model.customer.Phone;
import br.com.tratomais.core.service.IServiceLogin;

@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/config/spring-security-config.xml", "classpath:/config/spring-application-context.xml", "classpath:/config/spring-application-daos.xml",
		"classpath:/config/spring-application-services.xml" })
public class IG_EndorsementSaveTest {

	@Autowired
	private IDaoPolicy daoPolicy;
	
	@Autowired
	private IServiceLogin serviceLogin;
	
	@Before
	public void setup() throws Exception {
	}

	@After
	public void teardown() throws Exception {

	}

	//@Test
	@Transactional
	public void testListContract(){
		Contract contract = daoPolicy.listContract( 11 );
		Logger.getLogger(Endorsement.class).info("Contract id : " + contract.getContractId());
		
		for ( Endorsement end : contract.getEndorsements() ) {
			Logger.getLogger(Endorsement.class).info("Endorsement id : " + end.getId().getEndorsementId());
		}
		
	}
	
	@Test
	@Transactional
	@Rollback(false)
	public void testItemPropertyTest() {
		Logger.getLogger(Customer.class).info("HEY");
		Logger rootlog = Logger.getRootLogger();
		Logger hiblog = Logger.getLogger( Type.class );
		
		System.out.println(rootlog.getLevel());
		System.out.println(hiblog.getLevel());
		
		System.out.println("Rodando Customer");
		try {
			
			//Login
			User user = serviceLogin.validateLogin( "root@tratomais.com.br", "123123" );
			
			//Endorsement endorsement = new Endorsement();

			Contract contract = this.createContract();
			
			Endorsement endorsement = contract.addEndorsement();
			

			Customer customer = this.createCustomer();
			endorsement.setCustomerByInsuredId(customer);

			//EndorsementId endorsementId = new EndorsementId();
			//endorsementId.setContractId(contract.getContractId());
			endorsement.setUserId(AuthenticationHelper.getLoggedUser().getUserId());
			
			endorsement.setChannelId(1);
			endorsement.setTermId(1);
			endorsement.setPaymentTermId(1);
			endorsement.setRiskPlanId(1);
			endorsement.setHierarchyType(1);
			endorsement.setReferenceDate(new Date(System.currentTimeMillis()));
			endorsement.setEffectiveDate(new Date(System.currentTimeMillis()));
			if (endorsement.getTermId() == 1) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(endorsement.getEffectiveDate());
				calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + 1);
				endorsement.setExpiryDate(calendar.getTime());
			}
			endorsement.setQuotationDate(endorsement.getReferenceDate());
			endorsement.setQuotationNumber(1);
			endorsement.setNetPremium(1.0);
			endorsement.setTotalPremium(10000.00);
			endorsement.setTransmitted(false);
			endorsement.setIssuingStatus(1);
			
			
			//Item
			
			ItemPersonalRisk itemPersonalRisk = ( ItemPersonalRisk ) endorsement.addItem( Item.OBJECT_VIDA );
			
			itemPersonalRisk.setCoveragePlanId( 2 );
			itemPersonalRisk.setRenewalType( 1 );
			itemPersonalRisk.setEffectiveDate( new Date() );
			itemPersonalRisk.setCalculationValid( false );
			itemPersonalRisk.setItemStatus( 1 );
			
			//itemPersonalRisk.set
			itemPersonalRisk.setInsuredName( "Ismael :" + new Date());
			//LastName
			//FirstName
			itemPersonalRisk.setGender( 112 ); //112-Masculino, 113-feminino
			itemPersonalRisk.setBirthDate( new Date(1969 - 1900, 11 - 1, 18) );
			//HireDate
			itemPersonalRisk.setSmoker(true);
			/*AgeActuarial
			MaritalStatus
			ChildrenNumber
			DocumentType
			DocumentNumber
			DocumentIssuer
			DocumentValidity
			ProfessionID
			Inflow
			OccupationID
			OccupationRiskType
*/
	
			ItemCoverage coverage = itemPersonalRisk.addCoverage( 1 );
			
			coverage.setCoverageName( "teste" );
			coverage.setCoverageCode( "XX" );
			coverage.setBasicCoverage( true );
			coverage.setGoodsCode( "YY" );
			coverage.setInsuredValue( 10000.00d );
			
			
			Beneficiary beneficiary = itemPersonalRisk.addBeneficiary();
			beneficiary.setBeneficiaryType( 1 );
			beneficiary.setName( "Benef 001" );
			beneficiary.setKinshipType( 1 );
			beneficiary.setPersonType( 1 );
			beneficiary.setDocumentType( 12 );
			beneficiary.setDocumentNumber( "123123" );
			beneficiary.setParticipationPercentage( 100.0 );


			Endorsement savedEndorsement = daoPolicy.saveEndorsement(endorsement);
			
			Logger.getLogger(Endorsement.class).info("Endorsement id : " + savedEndorsement.getId().getEndorsementId());
			Logger.getLogger(Endorsement.class).info("customer id : " + savedEndorsement.getCustomerByInsuredId().getCustomerId());
			Logger.getLogger(Endorsement.class).info("Address Def id : " + savedEndorsement.getCustomerByInsuredId().getAddress().getAddressId() );
			Logger.getLogger(Endorsement.class).info("Address DefCus id : " + " Cus: " +savedEndorsement.getCustomerByInsuredId().getAddress().getCustomer().getCustomerId());
			int ind=0;
			for ( Address address : savedEndorsement.getCustomerByInsuredId().getAddresses() ) {
				ind++;
				Logger.getLogger(Endorsement.class).info("Address " + ind + "id : " + address.getAddressId() );
				Logger.getLogger(Endorsement.class).info("Address " + ind + "Cus id : " + " Cus: " + address.getCustomer().getCustomerId());
			}
			ind=0;
			for ( Customer customer2 : savedEndorsement.getCustomerByInsuredId().getAddress().getCustomers() ) {
				ind++;
				Logger.getLogger(Endorsement.class).info("Customer " + ind + "Cus id : " + " Cus: " + customer2.getCustomerId());
			}

			
			
			Assert.assertNotNull(savedEndorsement);
		} catch (Exception e) {
			Logger.getLogger(Customer.class).info("Erro1" + e.toString());
			Logger.getLogger(Customer.class).info("Erro2" + e.getMessage());
			e.printStackTrace();
			Assert.fail();
		}

	}

	/**
	 * 
	 */
	private Contract createContract() {
		Contract contract = new Contract();
//		MasterPolicy masterPolicy = daoPolicy.findMasterPolicyById(1);
//		contract.setMasterPolicy(masterPolicy);
		contract.setInsurerId(1);
		contract.setSubsidiaryId(1);
		contract.setBrokerId(5);
		contract.setPartnerId(1);
		contract.setProductId(1);
		contract.setCurrencyId(1);
		contract.setContractStatus(1);
		contract.setTermId(1);
		contract.setPolicyInformed(false);
		contract.setReferenceDate(new Date(System.currentTimeMillis()));
		contract.setEffectiveDate(new Date(System.currentTimeMillis()));
		if (contract.getTermId() == 1) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(contract.getEffectiveDate());
			calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + 1);
			contract.setExpiryDate(calendar.getTime());
		}
//		contract = daoPolicy.saveContract(contract);
		return contract;
	}

	private Customer createCustomer() {
		Customer customer = new Customer();
		customer.setName("Name IG");
		customer.setLastName("Surname IG");
		customer.setFirstName("Name");
		customer.setTradeName("4");
		customer.setPersonType(4);
		customer.setGender(112);
		customer.setBirthDate(new Date());
		customer.setRegisterDate(new Date());
		customer.setMaritalStatus(116);
		customer.setDocumentType(75);
		customer.setDocumentNumber("1");
		customer.setDocumentIssuer("1");
		customer.setDocumentValidity(new Date());
		customer.setProfessionType(1);
		customer.setProfessionId(1);
		customer.setOccupationId(2);
		customer.setActivityId(1);
		customer.setInflowId(1);
		customer.setEmailAddress("1");
		// customer.setAddressId(null);
		// customer.setPhoneId(null);
		customer.setActive(true);

		Address address = new Address();
		address.setActive(true);

		// address.setCustomerId( customerId );
		address.setAddressType(22);
		address.setAddressStreet("Rua IG");
		address.setDistrictName("bairro");
		// address.setCityId( cityId );
		address.setCityName("Cidade");
		address.setRegionName("Regi�o");
		// address.setStateId( stateId );
		address.setStateName("Estado");
		address.setZipCode("12345");
		// address.setCountryId( countryId );
		address.setCountryName("Brasil");
		address.setActive(true);

		customer.setAddress( address );
		customer.getAddresses().add(address);
		address.setCustomer(customer);
		address.getCustomers().add( customer );

		Phone phone = new Phone();
		phone.setActive( true );
		phone.setPhoneType( 1 );
		phone.setPhoneNumber( "234234237" );
		
		//link com Address
		phone.setAddress( address );
		address.getPhones().add( phone );
		
		
		//Customer newCustomer = daoCustomer.save(customer);
		//Logger.getLogger(Customer.class).info("Cliente" + newCustomer.getCustomerId());
		return customer;
	}

}
