package br.com.tratomais.core.model.policy;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.tratomais.core.util.AppContextHelper;

@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/config/spring-security-config.xml", "classpath:/config/spring-application-context.xml","classpath:/config/spring-application-daos.xml","classpath:/config/spring-application-services.xml"})
public class ItemPropertyTest extends TestCase {
	/*
	static {
		PropertyConfigurator.configure("C:\\workspace\\prototipo-core-v1\\src\\log4j.xml");
		Configuration conf = new Configuration();
		conf.configure(new File("C:\\workspace\\prototipo-core-v1\\src\\config\\hibernate-config.cfg.xml"));
		conf.setProperty("hibernate.show_sql", "true");
		conf.setProperty("hibernate.format_sql", "true");
		fac = conf.buildSessionFactory();
	}
	private static SessionFactory fac;
	private Session ses;

	public void testItemPropertyTest() {
		Logger.getLogger(ItemPersonalRiskTest.class).info("HEY");
		System.out.println("Rodando testItemPersonalRisk");
		ses = fac.openSession();
		Endorsement endorsement = getEndorsement();
		Transaction transaction = ses.beginTransaction();
		ItemProperty property = new ItemProperty();
		property.setId(new ItemId(4, endorsement.getId().getEndorsementId(),endorsement.getId().getContractId() ));
//		endorsement.getItems().add(risk);
		ses.saveOrUpdate(property);
		transaction.commit();
		ses.close();
	}

	private Endorsement getEndorsement() {
		Endorsement saida = (Endorsement) ses.load(Endorsement.class, new EndorsementId(1,11));

		System.err.println(saida);
		return  saida;
	}
	*/
	
	@SuppressWarnings("deprecation")
	@Test
	public void testItemProperty() {
		ApplicationContext applicationContext = AppContextHelper.getApplicationContext();
		SessionFactory sf = (SessionFactory)applicationContext.getBean("sessionFactory");
		Session session = sf.openSession();
		Transaction transaction = session.beginTransaction();
		ItemId itemId = new ItemId(1,1,21);

		ItemProperty it = (ItemProperty)session.get(ItemProperty.class, itemId);
		Assert.assertTrue(it.getSingleAddress());
		transaction.commit();
		session.close();
	}
}
