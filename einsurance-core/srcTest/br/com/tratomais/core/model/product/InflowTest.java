package br.com.tratomais.core.model.product;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.model.Inflow;
@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/config/spring-security-config.xml", "classpath:/config/spring-application-context.xml","classpath:/config/spring-application-daos.xml","classpath:/config/spring-application-services.xml"})
public class InflowTest {

	@Autowired private IDaoPolicy daoPolicy;
	 
	@Before
	public void setup() throws Exception {
		Logger.getLogger(ProductVersion.class).info("HEY");
	}
	 
	@After
	public void teardown() throws Exception {
	
	}


	@Test
	@Transactional
	public void testItemPropertyTest() {
		Logger.getLogger(Product.class).info("HEY");
		System.out.println("Rodando Inflow");
		try {
			List< Inflow > inflowList = daoPolicy.listAllInflow("F");
			
			for (Inflow inflow : inflowList) {
				System.out.println(inflow.getDescription()); 
			}
			
		} catch ( Exception e ) {
			Logger.getLogger(Inflow.class).info("Erro1" + e.toString());
			Logger.getLogger(Inflow.class).info("Erro2" + e.getMessage());
			e.printStackTrace();
		}
		
	}

}
