package br.com.tratomais.core.model.product;

import java.util.Date;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.tratomais.core.dao.IDaoProduct;
@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/config/spring-security-config.xml", "classpath:/config/spring-application-context.xml","classpath:/config/spring-application-daos.xml","classpath:/config/spring-application-services.xml"})
public class ProductTest {

	@Autowired private IDaoProduct daoProduct;
	 
	@Before
	public void setup() throws Exception {
		Logger.getLogger(Product.class).info("HEY");
	}
	 
	@After
	public void teardown() throws Exception {
	
	}


	@SuppressWarnings("deprecation")
	@Test
	@Transactional
	public void testItemPropertyTest() {
		Logger.getLogger(Product.class).info("Rodando Master Policy");
		try {
			Product mpList = daoProduct.findProductToBeCopy(1, new Date(2009-1900, 0, 1));

			Logger.getLogger(Product.class).info("ProductId :" + mpList.getProductId()
					+ " NickName: " + mpList.getNickName()  + " ObjectId: " + mpList.getObjectId());
			
			for ( ProductVersion productVersions : mpList.getProductVersions()) {
				Logger.getLogger(ProductVersion.class).info("Vers�o Inicio Vigencia:" + productVersions.getId().getEffectiveDate());
			}
			
			for (ProductPlan productPlan : mpList.getProductPlans()) {
				Logger.getLogger(ProductPlan.class).info("Plano :" + productPlan.getNickName());
				
				for(CoveragePlan coveragePlan : productPlan.getCoveragePlans()){
					Logger.getLogger(CoveragePlan.class).info("Cobertura :" + coveragePlan.getNickName());
				}
			}

		} catch ( Exception e ) {
			Logger.getLogger(Product.class).info("Erro1" + e.toString());
			Logger.getLogger(Product.class).info("Erro2" + e.getMessage());
			e.printStackTrace();
		}
		
	}
}
