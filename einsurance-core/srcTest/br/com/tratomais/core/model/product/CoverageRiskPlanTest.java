package br.com.tratomais.core.model.product;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.tratomais.core.model.User;
import br.com.tratomais.core.service.IServiceLogin;
import br.com.tratomais.core.service.IServiceProduct;
@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/config/spring-security-config.xml", "classpath:/config/spring-application-context.xml","classpath:/config/spring-application-daos.xml","classpath:/config/spring-application-services.xml"})
public class CoverageRiskPlanTest {

	@Autowired private IServiceProduct serviceProduct;
	@Autowired
	private IServiceLogin serviceLogin;
	 
	@Before
	public void setup() throws Exception {
		Logger.getLogger(ProductVersion.class).info("HEY");
	}
	 
	@After
	public void teardown() throws Exception {
	
	}

	@Test
	@Transactional
	public void testItemPropertyTest() {
		Logger.getLogger(Product.class).info("HEY");
		System.out.println("Rodando Product");

		//Login
		User user = serviceLogin.validateLogin( "root@tratomais.com.br", "123123" );
		
		try {
 			List< CoverageOption > dataGridList = serviceProduct.listCoverageOptionByRefDate( 4, 12, new Date() );
			
 			for ( CoverageOption dataGridOption : dataGridList ) {
				Logger.getLogger(ProductVersion.class).info("Cov " + dataGridOption.getNickName());
			}

			
		} catch ( Exception e ) {
			Logger.getLogger(ProductVersion.class).info("Erro1" + e.toString());
			Logger.getLogger(ProductVersion.class).info("Erro2" + e.getMessage());
			e.printStackTrace();
		}
		
	}
}
