package br.com.tratomais.core.model.customer;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.tratomais.core.dao.IDaoSubsidiary;
@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/config/spring-security-config.xml", "classpath:/config/spring-application-context.xml","classpath:/config/spring-application-daos.xml","classpath:/config/spring-application-services.xml"})
public class SubsidiaryTest {

	@Autowired private IDaoSubsidiary daoSubsidiary;
	 
	@Before
	public void setup() throws Exception {
		Logger.getLogger(Subsidiary.class).info("HEY");
	}
	 
	@After
	public void teardown() throws Exception {
	
	}


	@Test
	public void testItemPropertyTest() {
		Logger.getLogger(Subsidiary.class).info("HEY");
		System.out.println("Rodando Product");
		try {
			//List< Subsidiary > customerList = daoProduct.findProductsbyPartner( 0, 0, null );
			List< Subsidiary > customerList = daoSubsidiary.listAll();
			
			for ( Subsidiary subsidiary : customerList ) {
				
				Logger.getLogger(Subsidiary.class).info("Subsidiary: " + subsidiary.getSubsidiaryId() + " - " + subsidiary.getName());
			}

			
		} catch ( Exception e ) {
			Logger.getLogger(Subsidiary.class).info("Erro1" + e.toString());
			Logger.getLogger(Subsidiary.class).info("Erro2" + e.getMessage());
			e.printStackTrace();
		}
		
	}

//	private Endorsement getEndorsement() {
//		Endorsement saida = (Endorsement) ses.load(Endorsement.class, new EndorsementId(1,11));
//
//		System.err.println(saida);
//		return  saida;
//	}
}
