package br.com.tratomais.core.model.customer;

import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:/config/spring-security-config.xml",
		"classpath:/config/spring-application-context.xml",
		"classpath:/config/spring-application-daos.xml",
		"classpath:/config/spring-application-services.xml" })
public class CustomerSaveTest {

	// @Autowired private IDaoCustomer daoCustomer;

	@Before
	public void setup() throws Exception {
		Logger.getLogger(Customer.class).info("HEY");
	}

	@After
	public void teardown() throws Exception {

	}

	// private static SessionFactory fac;
	// private Session ses;
	private Address address;
	private Phone phone;

	@Test
	public void testItemPropertyTest() {
		Logger.getLogger(Customer.class).info("HEY");
		System.out.println("Rodando Customer");

		// List< Customer > customerList = daoCustomer.listAll();

		// for ( Customer customer : customerList ) {
		// Logger.getLogger(Customer.class).info(customer.getFirstName());
		// }

	}

	Address getAddress() {
		if (address == null) {
			address = new Address();
			address.setActive(true);
			address.setAddressStreet("Rua nova");
			address.setAddressType(5);
			address.setCityId(3);
			address.setCountryId(1);
			// address.setCustomerId(1);
			address.setDistrictName("distrito novo");
			address.setRegionName("Regi�o n�o");
			address.setStateId(1);
			address.setZipCode("01511-100");
		}
		return address;
	}

	/**
	 * 
	 * @return
	 */
	Phone getPhone() {
		phone = new Phone();
		return phone;
	}

	@Test
	public void testGetCustomerId() {
		Customer cust = new Customer();
		try {
			Address addr = getAddress();
			cust.setActive(true);
			cust.setActivityId(1);
			cust.getAddresses().add(addr);
			addr.setCustomer(cust);
			/*
			 * Address address2 = getAddress();
			 * cust.getAddresses().add(address2); cust.setAddress(address2);
			 */
			cust.setBirthDate(new Date());
			// cust.setContacts(contacts)
			cust.setDocumentIssuer("doc insur");
			cust.setDocumentNumber("doc num");
			cust.setDocumentType(1);
			cust.setDocumentValidity(new Date());
			cust.setEmailAddress("emailadd");
			cust.setFirstName("teste");
			cust.setGender(1);
			cust.setInflowId(1);
			cust.setLastName("last teste");
			cust.setMaritalStatus(1);
			cust.setName("Teste Last Teste");
			cust.setOccupationId(2);
			cust.setPersonType(1);
			// cust.setPhone(new Phone());
			cust.setProfessionId(1);
			cust.setProfessionType(1);
			cust.setRegisterDate(new Date());
			cust.setTradeName("Tradename56456465");
			// System.err.println("C�digo final gerado: Customer.customerId: " +
			// daoCustomer.save(cust).getCustomerId());
			Assert.assertTrue(true);
		} catch (Exception e) {
			System.err.println("C�digo final gerado: Customer.customerId: "
					+ cust.getCustomerId());
			System.err.println("Erro: " + e.getMessage());
			e.printStackTrace();
			Assert.assertTrue(false);
		}
	}

	@Test
	public void testGetCustomer() {
		Customer cust = new Customer();
		try {
			cust.setActive(true);
			cust.setActivityId(1);
			cust.setBirthDate(new Date());
			// cust.setContacts(contacts)
			cust.setDocumentIssuer("doc insur");
			cust.setDocumentNumber("doc num");
			cust.setDocumentType(1);
			cust.setDocumentValidity(new Date());
			cust.setEmailAddress("emailadd");
			cust.setFirstName("fistname");
			cust.setGender(1);
			cust.setInflowId(1);
			cust.setLastName("lastname");
			cust.setMaritalStatus(1);
			cust.setName("name");
			cust.setOccupationId(2);
			cust.setPersonType(1);
			// cust.setPhone(new Phone());
			cust.setProfessionId(1);
			cust.setProfessionType(1);
			cust.setRegisterDate(new Date());
			cust.setTradeName("Tradename");
			// System.err.println("C�digo final gerado: Customer.customerId: " +
			// ( cust2 = daoCustomer.save(cust)).getCustomerId());
			// cust2.getAddresses().add(getAddress());
			// address.setCustomer(cust2);
			// daoCustomer.save(cust2);
			Assert.assertTrue(true);
		} catch (Exception e) {
			System.err.println("C�digo final gerado: Customer.customerId: "
					+ cust.getCustomerId());
			System.err.println("Erro: " + e.getMessage());
			e.printStackTrace();
			Assert.assertTrue(false);
		}
	}

	@Test
	public void zzz() {
		System.out.println("Valor: "
				+ (System.currentTimeMillis() - ((new GregorianCalendar(2010,
						0, 1)).getTime().getTime()))
				/ (24 * 60 * 60 * 1000 * 365.4));// .getTimeInMillis())));
		System.out.println((new GregorianCalendar(2010, 0, 1)).getTime());
	}

	// private Endorsement getEndorsement() {
	// Endorsement saida = (Endorsement) ses.load(Endorsement.class, new
	// EndorsementId(1,11));
	//
	// System.err.println(saida);
	// return saida;
	// }
}
