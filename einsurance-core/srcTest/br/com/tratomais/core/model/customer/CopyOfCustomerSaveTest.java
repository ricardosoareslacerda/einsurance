package br.com.tratomais.core.model.customer;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/config/spring-security-config.xml", "classpath:/config/spring-application-context.xml","classpath:/config/spring-application-daos.xml","classpath:/config/spring-application-services.xml"})
public class CopyOfCustomerSaveTest {

//	@Autowired private IDaoCustomer daoCustomer;
	 
	@Before
	public void setup() throws Exception {
		Logger.getLogger(Customer.class).info("HEY");
	}
	 
	@After
	public void teardown() throws Exception {
	
	}

	private static SessionFactory fac;
	private Session ses;

	@Test
	public void testItemPropertyTest() {
		Logger.getLogger(Customer.class).info("HEY");
		System.out.println("Rodando Customer");
		try {
//			List< Customer > customerList = daoCustomer.listAll();
			
//			for ( Customer customer : customerList ) {
//				Logger.getLogger(Customer.class).info(customer.getFirstName());
//			}

			Customer customer = new Customer();
			customer.setName("Name 22");
			customer.setLastName("Surname 22");
			customer.setFirstName("Name");
			customer.setTradeName("4");
			customer.setPersonType(4);
			customer.setGender(112);
			customer.setBirthDate(new Date());
			customer.setRegisterDate(new Date());
			customer.setMaritalStatus(116);
			customer.setDocumentType(75);
			customer.setDocumentNumber("1");
			customer.setDocumentIssuer("1");
			customer.setDocumentValidity(new Date());
			customer.setProfessionType(1);
			customer.setProfessionId(1);
			customer.setOccupationId(2);
			customer.setActivityId(1);
			customer.setInflowId(1);
			customer.setEmailAddress("1");
			//customer.setAddressId(null);
			//customer.setPhoneId(null);
			customer.setActive(true);

			Address address = new Address();
			address.setActive( true );
			
			//address.setCustomerId( customerId );
			address.setAddressType( 22 );
			address.setAddressStreet( "Rua 1" );
			address.setDistrictName( "bairro" );
			//address.setCityId( cityId );
			address.setCityName( "Cidade" );
			address.setRegionName( "Regi�o" );
			//address.setStateId( stateId );
			address.setStateName( "Estado" );
			address.setZipCode( "12345" );
			//address.setCountryId( countryId );
			address.setCountryName( "Brasil" );
			address.setActive( true );
			
			//customer.setAddress( address );
			customer.getAddresses().add( address );
			address.setCustomer( customer );
			//address.getCustomers().add( customer );

//			Customer newCustomer = daoCustomer.save( customer );
			
	//		Logger.getLogger(Customer.class).info("Cliente" + newCustomer.getCustomerId());
			
		} catch ( Exception e ) {
			Logger.getLogger(Customer.class).info("Erro1" + e.toString());
			Logger.getLogger(Customer.class).info("Erro2" + e.getMessage());
			e.printStackTrace();
		}
		
	}

//	private Endorsement getEndorsement() {
//		Endorsement saida = (Endorsement) ses.load(Endorsement.class, new EndorsementId(1,11));
//
//		System.err.println(saida);
//		return  saida;
//	}
}
