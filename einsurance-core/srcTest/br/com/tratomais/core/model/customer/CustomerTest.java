package br.com.tratomais.core.model.customer;

import java.util.Date;

import junit.framework.TestCase;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;

public class CustomerTest extends TestCase {

	private SessionFactory sessionFactory;
	private Session openSession;

	public CustomerTest(String name) {
		super(name);
	//	sessionFactory = HibernateUtil.getSessionFactory();
	}

	protected void setUp() throws Exception {
		openSession = sessionFactory.openSession();
	}

	public void testGetCustomerId() {
		Transaction transaction = openSession.beginTransaction();
		try {
			Customer cust = new Customer();
			cust.setActive(true);
			cust.setActivityId(1);
	//		cust.setAddresses(addresses)
			cust.setBirthDate(new Date());
	//		cust.setContacts(contacts)
			cust.setDocumentIssuer("doc insur");
			cust.setDocumentNumber("doc num");
			cust.setDocumentType(1);
			cust.setDocumentValidity(new Date());
			cust.setEmailAddress("emailadd");
			cust.setFirstName("fistname");
			cust.setGender(1);
			cust.setInflowId(1);
			cust.setLastName("lastname");
			cust.setMaritalStatus(1);
			cust.setName("name");
			cust.setOccupationId(1);
			cust.setPersonType(1);
	//		cust.setPhone(new Phone());
			cust.setProfessionId(1);
			cust.setProfessionType(1);
			cust.setRegisterDate(new Date());
			cust.setTradeName("Tradename");
			openSession.save(cust);
			transaction.commit();
		}
		catch(Exception e){
			transaction.rollback();
			e.printStackTrace();
		}
	}

}
