package br.com.tratomais.core.esb;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.jms.JMSException;
import javax.naming.NamingException;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.Installment;
import br.com.tratomais.core.service.IServiceCollection;
import br.com.tratomais.core.service.IServiceLogin;
import br.com.tratomais.core.util.jms.EInsuranceJMSException;

@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/config/spring-security-config.xml", "classpath:/spring-tests-context.xml", "classpath:/config/spring-application-daos.xml",
		"classpath:/config/spring-application-services.xml" })
public class TestCollection {
	@Autowired
	private IDaoPolicy daoPolicy;

	@Autowired IServiceCollection serviceCollection;
	@Autowired IServiceLogin serviceLogin;
	
	
	@Test
	@Transactional
	public void testCollection() throws FileNotFoundException, IOException {

		try {
			
			User login = serviceLogin.validateLogin("root@tratomais.com.br", "123");
			Assert.assertNotNull(login);
			
			Endorsement endorsement = daoPolicy.loadEndorsement(1, 71);
			Assert.assertNotNull(endorsement);
			
			for (Installment installment : endorsement.getInstallments()) {
				installment.setInstallmentStatus(Installment.INSTALLMENT_STATUS_PENDING);
			}
			
			boolean dispachCollection = serviceCollection.dispachCollection(endorsement, false);
			Assert.assertEquals(true, dispachCollection);
		
		} catch (JMSException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EInsuranceJMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
