package br.com.tratomais.core.esb;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import javax.jms.JMSException;
import javax.naming.NamingException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.Installment;
import br.com.tratomais.core.util.StringUtil;
import br.com.tratomais.core.util.jms.EInsuranceJMSConnection;
import br.com.tratomais.core.util.jms.EInsuranceJMSException;
import br.com.tratomais.esb.collections.model.edi.EdiCreditRecord;
import br.com.tratomais.esb.collections.model.edi.EdiDebitFile;
import br.com.tratomais.esb.collections.model.edi.EdiDebitRecord;

@Configurable(autowire = Autowire.BY_NAME) 
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/config/spring-security-config.xml", "classpath:/spring-tests-context.xml", "classpath:/config/spring-application-daos.xml",
		"classpath:/config/spring-application-services.xml" })
public class TestDebitFile {
	@Autowired private IDaoPolicy daoPolicy;
	
	public void setDaoSubsidiary(IDaoPolicy daoPolicy){
		this.daoPolicy = daoPolicy;
	}
	
	@Test
	@Transactional
	public void testEsbDebitFile() throws FileNotFoundException, IOException {
		int[] contractsTest = {14,18};
		//int[] contractsTest = {15,16,17};
		double totalCredit = 0.0;
		
		EdiDebitFile debitFile = new EdiDebitFile();
		
		debitFile.setTipoRegistro("D1");	
		debitFile.setuNBNr( "00000000000001");          
		debitFile.setMessageNr( "013410050001                       ");      
		debitFile.setFechaDebito( new Date());    
		debitFile.setFuncionNegocio( "SUB"); 
		debitFile.setRifOrdenante( "J0000000000340242");   
		debitFile.setNombreOrdenante(StringUtil.fill("ZURICH SEGUROS, S.A.", " ", 35, false) );
		debitFile.setNroEdiEnvia( "ZURICHVE          ");    
		debitFile.setNroEdiRecibe( "7591470000000     ");   
		debitFile.setVersion( "D96A");		

		
		EdiCreditRecord creditRecord = new EdiCreditRecord();
		creditRecord.setTipoRegistro( "D2"); 
		creditRecord.setFechaValor( new Date());   
		creditRecord.setRefCredito( "10050001       ");   
		creditRecord.setMontoCredito( 0.0); 
		creditRecord.setMonedaCredito( "VEB");
		creditRecord.setNroCtaCredito( "01050011111111447130");
		creditRecord.setCodBanco( "UNIOVECA    ");     
				
		debitFile.setCreditRecord( creditRecord );
		
		for (int i = 0; i < contractsTest.length; i++) {
			Endorsement endorsement = daoPolicy.loadEndorsement(1, contractsTest[i]);
			Installment installment = endorsement.getFirtInstallment();
			

//			fechaExpiracionTarjeta = "201405";  
//			refCliente = "19600372139                        ";              
//			refDebito = "37213904";               
//			formaPago = "CB ";               
//			nroCtaTarjeta = "01051111111111152695";           
//			codBanco = "BAMRVECA    ";                
//			rifCliente = "V0000000010111111";          	
//			nombreCliente = "FERRARI CCCCCC, CCCCCC CCCCA       "; 			
			
			if (installment != null) {
				EdiDebitRecord debitRecord = new EdiDebitRecord();
				debitRecord.setTipoRegistro("D3");            

				debitRecord.setMontoDebito(installment.getInstallmentValue());

				debitRecord.setMonedaDebito("VEB");            

				//Sistema
				String refCliente = "EIN";
				//Apolice
				refCliente += StringUtil.fill(installment.getEndorsement().getContract().getPolicyNumber().toString(), "0", 10, true);
				//Canal de Venda
				refCliente += StringUtil.fill(installment.getEndorsement().getChannelId() + "", "0", 3, true);
				//Certificado
				refCliente += StringUtil.fill(installment.getEndorsement().getContract().getCertificateNumber().toString(), "0", 10, true);
				//Contrato
				refCliente += StringUtil.fill(installment.getEndorsement().getContract().getContractId()+"", "0", 9, true);
				
				debitRecord.setRefCliente(refCliente);
				
				//Emissao
				String refDebito = StringUtil.fill(installment.getId().getEndorsementId()+"", "0", 3, true);
				//Parcela
				refDebito += StringUtil.fill(installment.getId().getInstallmentId()+"", "0", 3, true);
				//Filler
				refDebito += "  ";
				
				debitRecord.setRefDebito(refDebito);
				
				if (installment.getBillingMethodId() == 1 || installment.getBillingMethodId() == 2) {
					debitRecord.setFechaExpiracionTarjeta("      ");
					debitRecord.setFormaPago("CB ");
					debitRecord.setNroCtaTarjeta(StringUtil.fill(installment.getBankAccountNumber(), " ", 20, false));
					debitRecord.setCodBanco(StringUtil.fill("UNIOVECA", " ", 12, false));
					debitRecord.setNombreCliente(StringUtil.fill(installment.getEndorsement().getCustomerByPolicyHolderId().getName().toUpperCase(), " ", 35, false));
				}else if(installment.getBillingMethodId() == 4){
					debitRecord.setFechaExpiracionTarjeta(StringUtil.fill(installment.getCardExpirationDate().toString(), "0", 6, true));
					debitRecord.setFormaPago("TA ");
					debitRecord.setNroCtaTarjeta(StringUtil.fill(installment.getCardNumber(), " ", 20, false));
					debitRecord.setCodBanco(StringUtil.fill("UNIOVECA", " ", 12, false));
					debitRecord.setNombreCliente(StringUtil.fill(installment.getCardHolderName().toUpperCase(), " ", 35, false));
				}else if(installment.getBillingMethodId() == 6){
					debitRecord.setFechaExpiracionTarjeta(StringUtil.fill(installment.getCardExpirationDate().toString(), "0", 6, true));
					debitRecord.setFormaPago("TA ");
					debitRecord.setNroCtaTarjeta(StringUtil.fill(installment.getCardNumber(), " ", 20, false));
					debitRecord.setCodBanco(StringUtil.fill("VZLAVECA", " ", 12, false));
					debitRecord.setNombreCliente(StringUtil.fill(installment.getCardHolderName().toUpperCase(), " ", 35, false));
				}
				
				String rifCliente = "";
				if (installment.getEndorsement().getCustomerByPolicyHolderId().getDocumentType().intValue() == 75){
					rifCliente += "V";
				}else if (installment.getEndorsement().getCustomerByPolicyHolderId().getDocumentType().intValue() == 76){
					rifCliente += "J";
				}else if (installment.getEndorsement().getCustomerByPolicyHolderId().getDocumentType().intValue() == 334){
					rifCliente += "E";
				}else if (installment.getEndorsement().getCustomerByPolicyHolderId().getDocumentType().intValue() == 335){
					rifCliente += "P";
				}
				rifCliente += StringUtil.fill(installment.getEndorsement().getCustomerByPolicyHolderId().getDocumentNumber(), "0", 17-rifCliente.length(), true);
				debitRecord.setRifCliente(rifCliente);
				
				
				debitFile.getDebitRecords().add(debitRecord);
				totalCredit += new Double(debitRecord.getMontoDebito()).doubleValue();
			}
			
			creditRecord.setMontoCredito((totalCredit));

		}
		try {
			EInsuranceJMSConnection.newInstance().sendObject(debitFile,"queue/collection_request");
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EInsuranceJMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
    
   

}
