package br.com.tratomais.core.soap;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.policy.Contract;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.Installment;
import br.com.tratomais.core.service.IServiceLogin;

@Transactional(readOnly=true)
@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/testConfig/spring-security-config.xml", 
									"classpath:/testConfig/spring-application-context.xml", 
									"classpath:/testConfig/spring-application-daos.xml",
									"classpath:/testConfig/spring-application-services.xml" })
public class EInsuranceServicesImplTest {
	private static final String INICIO_VIGENCIA_STR = "01/05/2010";
	private static final int CONTRACT_ID = 350;

	private int endorsementId;
	private int installmentId;
	
	private static Date INICIO_VIGENCIA;
	static {
		SimpleDateFormat sdf = (SimpleDateFormat) SimpleDateFormat.getDateInstance();
		try {
			EInsuranceServicesImplTest.INICIO_VIGENCIA = sdf.parse(INICIO_VIGENCIA_STR);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	@Autowired
	private EInsuranceServicesImpl eInsuranceServices;
	@Autowired
	private IServiceLogin serviceLogin;
	@Autowired
	private IDaoPolicy daoPolicy;
	
	@Test
	@Transactional
	public void testGeraEndossoDeAnulacaoPorFaltaDePagamento() {
		//Login
		@SuppressWarnings("unused")
		User user = serviceLogin.validateLogin( "root@tratomais.com.br", "123" );
		//this.eInsuranceServices.geraEndossoDeAnulacaoPorFaltaDePagamento(user.getLogin(), user.getPassword(), daoPolicy.getInstallment(CONTRACT_ID, endorsementId, installmentId));
	}
	
//	@Test
//	@Transactional
//	public void testPolicyReactivation() throws Exception {
//		this.eInsuranceServices.policyReactivation("root@tratomais.com.br", "123", 1);
//	}
	
	
	@Before
	public void beforeMethods() {
		Assert.notNull(EInsuranceServicesImplTest.INICIO_VIGENCIA_STR, "Data n�o ajustada corretamente");
		Contract contract = this.daoPolicy.listContract(CONTRACT_ID);
		Endorsement endors = null;
		for (Endorsement endorsement : contract.getEndorsements()) {
			if (( endors == null) ||(endors.getId().getEndorsementId() < endorsement.getEndorsedId()))
					endors = endorsement;
		}
		Installment inst = null;
		for (Installment installment : endors.getInstallments()) {
			if ( (installment.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_PENDING) && (installment.getExpiryDate().compareTo(INICIO_VIGENCIA)> 0)){
				inst = installment;
				break;
			}
		}
		this.endorsementId = endors.getId().getEndorsementId();
		this.installmentId = inst.getId().getInstallmentId();		
	}
	
	/**
	 * @return the eInsuranceServices
	 */
	public EInsuranceServicesImpl getEInsuranceServices() {
		return this.eInsuranceServices;
	}

	/**
	 * @param insuranceServices the eInsuranceServices to set
	 */
	public void setEInsuranceServices(EInsuranceServicesImpl eInsuranceServices) {
		this.eInsuranceServices = eInsuranceServices;
	}


	/**
	 * @return the daoPolicy
	 */
	public IDaoPolicy getDaoPolicy() {
		return daoPolicy;
	}


	/**
	 * @param daoPolicy the daoPolicy to set
	 */
	public void setDaoPolicy(IDaoPolicy daoPolicy) {
		this.daoPolicy = daoPolicy;
	}

}
