package br.com.tratomais.core.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.tratomais.core.model.Module;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.service.IServiceLogin;
import br.com.tratomais.core.service.IServiceModule;

@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/config/spring-security-config.xml", "classpath:/config/spring-application-context.xml", "classpath:/config/spring-application-daos.xml",
		"classpath:/config/spring-application-services.xml" })
public class ModuleServiceTest {

	@Autowired
	private IServiceLogin serviceLogin;

	@Autowired
	private IServiceModule serviceModule;

	@Test
	@Transactional
	@Rollback(false)
	public void testCreateMenu() throws Exception {

		Logger.getLogger(Module.class).info("Rodando Module");
		User user = serviceLogin.validateLogin("root@tratomais.com.br", "123");
		Logger.getLogger(Module.class).info("user: " + user.getName());

		List<Module> createMenu = serviceModule.listAllowedModulesByUserLogged();

		this.loggerModule(createMenu, "");
	}

	/**
	 * @param createMenu
	 */
	private void loggerModule(List<Module> createMenu, String backspace) {
		for (Module module : createMenu) {
			System.out.println("module : " + module.getName() + backspace);
			if ((module.getModule() == null) && module.isActive()) {
				this.loggerModule(this.getListModules(module.getModules()), "\t");
			}
		}
		System.out.println("********************");
	}

	/**
	 * @param modulesReturn
	 **/
	private List<Module> getListModules(Set<Module> modulesReturn) {
		List<Module> modules = new ArrayList<Module>();
		for (Module module : modulesReturn) {
			modules.add(module);
		}
		return modules;
	}

	public void setServiceLogin(IServiceLogin serviceLogin) {
		this.serviceLogin = serviceLogin;
	}

	public void setServiceModule(IServiceModule serviceModule) {
		this.serviceModule = serviceModule;
	}

}
