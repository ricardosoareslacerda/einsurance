package br.com.tratomais.core.service.impl;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.tratomais.core.service.ICalc;

/**
 * Test Class for BasicCalculationServiceTest
 */
@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/config/spring-security-config.xml", "classpath:/spring-tests-context.xml", "classpath:/config/spring-application-daos.xml",
		"classpath:/config/spring-application-services.xml" })
public class BaixaParcelaTeste {
	@Autowired private ICalc serviceCalc;
	
	
	public void setServiceCalc(ICalc serviceCalc) {
		this.serviceCalc = serviceCalc;
	}
	/*
	@Test 
	@Transactional
	@Rollback(false)
	public void testBaixaParcela() {
		User user = serviceLogin.validateLogin("root@tratomais.com.br", "123");
		List<Installment> lista = getInstallmentToBePaid();
		serviceCalc.baixaParcela(lista);
		Assert.assertTrue(true);
	}
	*/
	
	@Test 
	@Transactional
	@Rollback(false)
	public void testExclusaoPendencia() {
		serviceCalc.deletePendency(55, 1);
		Assert.assertTrue(true);
	}
}
