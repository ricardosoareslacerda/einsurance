package br.com.tratomais.core.service.impl;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.model.product.PaymentOption;
import br.com.tratomais.core.service.IServiceEinsurance;

@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:/config/spring-security-config.xml",
		"classpath:spring-tests-context.xml",
		"classpath:/config/spring-application-daos.xml",
		"classpath:/config/spring-application-services.xml" })
public class ServiceEinsuranceImplTest {
	@Autowired
	private IServiceEinsurance serviceEinsurance;

	public void setServiceEinsurance(IServiceEinsurance serviceEinsurance) {
		this.serviceEinsurance = serviceEinsurance;
	}

	/*
	 * @Test public void testGetPaymentOptionsByBillingMethodId() {
	 * fail("Not yet implemented"); }
	 * 
	 * @Test public void testGetPaymentOptionsDefault() {
	 * fail("Not yet implemented"); }
	 */
	@Test
	public void testGetPaymentOptionsByPaymentType() throws ServiceException {
		Date dtRef = novaData(1, 1, 2010), dtEffec = novaData(1, 1, 2010);
		// expireDate = novaData(3,1,2010);
		List<PaymentOption> list = this.serviceEinsurance
				.getPaymentOptionsByPaymentType(100000d, 1D, 1, 1, dtRef,
						dtEffec, dtEffec);
		assertEquals(1, list.size());
	}

	private Date novaData(int dia, int mes, int ano) {
		Calendar cal = Calendar.getInstance();
		cal.set(ano, mes, dia, 0, 0, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

}
