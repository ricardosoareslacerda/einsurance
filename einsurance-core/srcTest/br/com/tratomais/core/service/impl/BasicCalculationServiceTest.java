package br.com.tratomais.core.service.impl;


import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.model.customer.Address;
import br.com.tratomais.core.model.customer.Customer;
import br.com.tratomais.core.model.policy.Contract;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.EndorsementId;
import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.policy.ItemCoverage;
import br.com.tratomais.core.model.policy.ItemCoverageId;
import br.com.tratomais.core.model.policy.ItemId;
import br.com.tratomais.core.model.policy.ItemPersonalRisk;
import br.com.tratomais.core.service.ICalc;


/**
 * Test Class for BasicCalculationServiceTest
 */
@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/config/spring-security-config.xml", "classpath:/config/spring-application-context.xml", "classpath:/config/spring-application-daos.xml",
		"classpath:/config/spring-application-services.xml" })
public class BasicCalculationServiceTest {
	@Autowired public ICalc serviceBasicCalc;
	@Autowired public IDaoPolicy daoPolicy;
	
	private Endorsement endorsement;
	
	@Before
	public void setUp() throws Exception {
		endorsement=criaEndorsement();
	}
	
	@Test
	@Transactional
	public void testBasicCalculationService() {

//		System.err.println("============================================ INICIANDO TESTS ============================================");
		
		Endorsement simpleCalc = null;
		try {
			simpleCalc = serviceBasicCalc.simpleCalc(endorsement);
		} catch ( ServiceException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(simpleCalc);
		for(Item it : simpleCalc.getItems()){
			System.out.println("id: \n" + it.getId());
			System.out.println(it.getPurePremium());
		}
		Assert.assertTrue(true);
	}
	
	private Logger logger = Logger.getLogger(BasicCalculationServiceTest.class);

	private Endorsement criaEndorsement() {
		Endorsement endorsement = null;
		Contract contract = this.createContract();
		endorsement = contract.addEndorsement();

		Customer customer = this.createCustomer();
		endorsement.setId(new EndorsementId(1,11));
		endorsement.setCustomerByInsuredId(customer);
		endorsement.setUserId(20);
		endorsement.setRiskPlanId(1);

		endorsement.setChannelId(1);
		endorsement.setTermId(1);
		endorsement.setPaymentTermId(1);
		endorsement.setHierarchyType(1);
		endorsement.setReferenceDate(new Date(System.currentTimeMillis()));
		endorsement.setEffectiveDate(new Date(System.currentTimeMillis()));
		if (endorsement.getTermId() == 1) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(endorsement.getEffectiveDate());
			calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + 1);
			endorsement.setExpiryDate(calendar.getTime());
		}
		endorsement.setQuotationDate(endorsement.getReferenceDate());
		endorsement.setQuotationNumber(1);
		endorsement.setNetPremium(1.0);
		endorsement.setTotalPremium(10000.00);
		endorsement.setTransmitted(false);
		endorsement.setIssuingStatus(1);

		endorsement.getItems().add(preencheItem(endorsement.addItem(Item.OBJECT_ACCIDENTES_PERSONALES), endorsement));
		
		return endorsement;// daoPolicy.saveEndorsement(endorsement);
	}

	private Item preencheItem(Item item, Endorsement endorsement) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.set(1975, 4, 3);
		((ItemPersonalRisk)item).setInsuredName("Insured Name");
		item.setItemStatus(1);
		item.setId(new ItemId(1,1,1));
		item.setEffectiveDate(calendar.getTime());
		//item.
//		item.setd
//		item.setRenewalType(0);
		ItemCoverageId ici = new ItemCoverageId(2, 1, 1, 1);
		ItemCoverage ic = new ItemCoverage(ici, item, "covCode", "covName", true, "goodsCode", new Double(100.0));
		ic.setInsuredValue(100.0);
		item.getItemCoverages().add(ic);
		item.setEndorsement(endorsement);
		
		/*
		addItem.set
		new ItemPersonalRisk("Insurade Name", "last name", "first name", 1, calendar.getTime(), calendar.getTime(), false, 40, 1,2, 1, "12",
				"doc insurer", calendar.getTime(), 2, 1.0, 1, 1);
			*/
		return item;
	}

	private Contract createContract() {
		Contract contract = new Contract();
		contract.setInsurerId(1);
		contract.setSubsidiaryId(1);
		contract.setBrokerId(1);
		contract.setPartnerId(1);
		contract.setProductId(1);
		contract.setCurrencyId(1);
		contract.setContractStatus(1);
		contract.setTermId(1);
		contract.setPolicyInformed(false);
		contract.setReferenceDate(new Date(System.currentTimeMillis()));
		contract.setEffectiveDate(new Date(System.currentTimeMillis()));
		if (contract.getTermId() == 1) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(contract.getEffectiveDate());
			calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + 1);
			contract.setExpiryDate(calendar.getTime());
		}
		return contract;
	}

	private Customer createCustomer() {
		Customer customer = new Customer();
		customer.setName("Name 22");
		customer.setLastName("Surname 22");
		customer.setFirstName("Name");
		customer.setTradeName("4");
		customer.setPersonType(4);
		customer.setGender(112);
		customer.setBirthDate(new Date());
		customer.setRegisterDate(new Date());
		customer.setMaritalStatus(116);
		customer.setDocumentType(75);
		customer.setDocumentNumber("1");
		customer.setDocumentIssuer("1");
		customer.setDocumentValidity(new Date());
		customer.setProfessionType(1);
		customer.setProfessionId(1);
		customer.setOccupationId(2);
		customer.setActivityId(1);
		customer.setInflowId(1);
		customer.setEmailAddress("1");
		customer.setActive(true);

		Address address = new Address();
		address.setActive(true);

		address.setAddressType(22);
		address.setAddressStreet("Rua 1");
		address.setDistrictName("bairro");
		address.setCityName("Cidade");
		address.setRegionName("Regi�o");
		address.setStateName("Estado");
		address.setZipCode("12345");
		address.setCountryName("Brasil");
		address.setActive(true);

		customer.getAddresses().add(address);
		address.setCustomer(customer);
		return customer;
	}
	
	@Test
	public void testGetPaymentOptions() {
		/*
		List<PaymentTerm> payments = new ArrayList<PaymentTerm>();
		PaymentTerm pt = new PaymentTerm();
		pt.setName("Anual");
		pt.setQuantityInstallment((short)1);
		payments.add(pt);

		pt = new PaymentTerm();
		pt.setName("Semestral");
		pt.setQuantityInstallment((short)2);
		payments.add(pt);

		pt = new PaymentTerm();
		pt.setName("trimestral");
		pt.setQuantityInstallment((short)4);
		payments.add(pt);
		
		getPaymentOptions(100.0	, List<PaymentTerm> payments, Product product );
		*/
	}
}
