package br.com.tratomais.core.service.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.service.IServiceProduct;

@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/config/spring-security-config.xml", "classpath:spring-tests-context.xml", "classpath:/config/spring-application-daos.xml",
		"classpath:/config/spring-application-services.xml" })
public class ServiceProductImplTest {
//listCoverageHabitatByEndorsement
	
	@Autowired
	private IServiceProduct serviceProduct;
	@Autowired
	private IDaoPolicy daoPolicy;
	
	public void setServiceProduct(IServiceProduct serviceProduct){
		this.serviceProduct = serviceProduct;
	}
	
	public void setDaoPolicy(IDaoPolicy daoPolicy){
		this.daoPolicy = daoPolicy;
	}
	
	
	@Test
	public void testListCoverageHabitatByEndorsement() {
		Endorsement endorsement = daoPolicy.loadEndorsement(1, 1);
		Assert.assertNotNull(endorsement);
	}
}
