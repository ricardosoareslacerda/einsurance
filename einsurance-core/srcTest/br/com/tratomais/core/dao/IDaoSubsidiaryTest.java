package br.com.tratomais.core.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/config/spring-security-config.xml", "classpath:spring-tests-context.xml", "classpath:/config/spring-application-daos.xml",
		"classpath:/config/spring-application-services.xml" })
public class IDaoSubsidiaryTest {
	@Autowired private IDaoSubsidiary daoSubsidiary;
	
	public void setDaoSubsidiary(IDaoSubsidiary daoSubsidiary){
		this.daoSubsidiary = daoSubsidiary;
	}
	
	
	
	@Test
	public void testSubsidiaryExternalCodeExists() {
		assertTrue(daoSubsidiary.subsidiaryExternalCodeExists(1, 2, "1"));
		assertFalse(daoSubsidiary.subsidiaryExternalCodeExists(2, 1, "2"));
	}

}
