package br.com.tratomais.core.dao.impl;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.tratomais.core.dao.IDaoClaimActionLock;
import br.com.tratomais.core.model.claim.ClaimActionLock;
import br.com.tratomais.core.model.claim.ClaimActionLockId;

@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { // N�o colocar a parte de seguran�a !!!!
		"classpath:/testConfig/spring-application-context.xml",
		"classpath:/config/spring-application-daos.xml"}) 
@Transactional
public class DaoClaimActionLockTest {
	@Autowired
	private IDaoClaimActionLock daoClaimActionLock;  
	/** setter */
	public void setDaoClaimActionLock(IDaoClaimActionLock daoClaimActionLock) {
		this.daoClaimActionLock = daoClaimActionLock;
	}
	
	@Test
	public void testeDao() {
		Date data = new Date();
		ClaimActionLockId claimActionLockId = new ClaimActionLockId();
		claimActionLockId.setClaimEffectId(1);
		claimActionLockId.setClaimStatus(1);
		claimActionLockId.setCoverageId(1);
		claimActionLockId.setEffectiveDate(data);
		claimActionLockId.setInsuredMain(true);
		claimActionLockId.setIssuanceType(1);
		ClaimActionLock entity = new ClaimActionLock();
		entity.setClaimActionLockType(1);
		entity.setDescription("Descri��o");
		entity.setExpiryDate(new Date());
		entity.setObjectId(1);
		entity.setRegistred(new Date());
		entity.setUpdated(new Date());
		entity.setClainActionLockId(claimActionLockId);
		
		daoClaimActionLock.save(entity);
		
		ClaimActionLockId claimActionLockId2 = new ClaimActionLockId();
		claimActionLockId2.setClaimEffectId(1);
		claimActionLockId2.setClaimStatus(1);
		claimActionLockId2.setCoverageId(1);
		claimActionLockId2.setEffectiveDate(data);
		claimActionLockId2.setInsuredMain(true);
		claimActionLockId2.setIssuanceType(1);
		
		ClaimActionLock claimActionLock2 = daoClaimActionLock.findById(claimActionLockId2);
		Assert.assertEquals(claimActionLockId.getClaimEffectId(), claimActionLockId2.getClaimEffectId());
		Assert.assertEquals(claimActionLockId.getClaimStatus(), claimActionLockId2.getClaimStatus());
		Assert.assertEquals(claimActionLockId.getCoverageId(), claimActionLockId2.getCoverageId());
		Assert.assertEquals(claimActionLockId.getEffectiveDate(), claimActionLockId2.getEffectiveDate());
		Assert.assertEquals(claimActionLockId.getInsuredMain(), claimActionLockId2.getInsuredMain());
		Assert.assertEquals(claimActionLockId.getIssuanceType(), claimActionLockId2.getIssuanceType());
		Assert.assertEquals(entity.getClaimActionLockType(), claimActionLock2.getClaimActionLockType());
		Assert.assertEquals(entity.getDescription(), claimActionLock2.getDescription());
		Assert.assertEquals(entity.getExpiryDate(), claimActionLock2.getExpiryDate());
		Assert.assertEquals(entity.getObjectId(), claimActionLock2.getObjectId());
		Assert.assertEquals(entity.getRegistred(), claimActionLock2.getRegistred());
		Assert.assertEquals(entity.getUpdated(), claimActionLock2.getUpdated());
		Assert.assertEquals(entity.getClainActionLockId(), claimActionLock2.getClainActionLockId());

	}
}
