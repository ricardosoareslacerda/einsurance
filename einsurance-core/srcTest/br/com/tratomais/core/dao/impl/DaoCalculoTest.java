package br.com.tratomais.core.dao.impl;

import junit.framework.TestCase;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/config/spring-security-config.xml", "classpath:/config/spring-application-context.xml","classpath:/config/spring-application-daos.xml","classpath:/config/spring-application-services.xml"})
public class DaoCalculoTest extends TestCase {
	public DaoCalculoTest() {
		super();
	}
	public DaoCalculoTest(String name) {
		super(name);
	}
}
