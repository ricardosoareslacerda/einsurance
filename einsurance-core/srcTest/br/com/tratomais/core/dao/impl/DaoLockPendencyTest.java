package br.com.tratomais.core.dao.impl;

import java.util.Date;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.pendencies.LockPendency;
import br.com.tratomais.core.model.pendencies.LockPendencyId;
import br.com.tratomais.core.service.ICalc;
import br.com.tratomais.core.service.IServiceLogin;
import br.com.tratomais.core.service.impl.BasicCalculationService;

@Configurable(autowire = Autowire.BY_NAME)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:/config/spring-security-config.xml",
		"classpath:/spring-tests-context.xml",
		"classpath:/config/spring-application-daos.xml",
		"classpath:/config/spring-application-services.xml" })
public class DaoLockPendencyTest {
	private BasicCalculationService serviceBasicCalcX;

	@Autowired
	private DaoLockPendency daoLockPendency;
	@Autowired
	private DaoHistoryPendency daoHistoryPendency;
	@Autowired
	private DaoCalculo daoCalculo;
	@Autowired
	private IDaoPolicy daoPolicy;

	public void setServiceBasicCalc(ICalc serviceBasicCalc) {
		this.serviceBasicCalc = serviceBasicCalc;
	}

	public ICalc getServiceBasicCalc() {
		return this.serviceBasicCalc;
	}

	public void setDaoLockPendency(DaoLockPendency daoLockPendency) {
		this.daoLockPendency = daoLockPendency;
	}

	public void setDaoCalculo(DaoCalculo daoCalculo) {
		this.daoCalculo = daoCalculo;
	}

	public void setDaoPolicy(IDaoPolicy daoPolicy) {
		this.daoPolicy = daoPolicy;
	}

	// @Test
	@Transactional
	@Rollback(false)
	public void testLiberacao() {
		serviceBasicCalcX = new BasicCalculationService();
		serviceBasicCalcX.setDaoLockPendency(daoLockPendency);
		serviceBasicCalcX.setDaoHistoryPendency(daoHistoryPendency);
		serviceBasicCalcX.setDaoCalculo(daoCalculo);
		serviceBasicCalcX.setDaoPolicy(daoPolicy);
		LockPendency l = new LockPendency();
		LockPendencyId lid = new LockPendencyId();
		l.setIssueDate(new Date());
		lid.setContractID(55);
		lid.setEndorsementID(1);
		l.setId(lid);
		l.setPendencyID(1);
		serviceBasicCalcX.freePendency(l);
		Assert.assertTrue(true);
	}

	@Autowired
	private ICalc serviceBasicCalc;
	@Autowired
	private IServiceLogin serviceLogin;

	@Test
	@Transactional
	@Rollback(false)
	public void testDados() {
		Assert.assertNotNull(serviceBasicCalc);
		Assert.assertNotNull(serviceLogin);
		@SuppressWarnings("unused")
		User user = serviceLogin.validateLogin("root@tratomais.com.br", "123");
		LockPendency l = new LockPendency();
		LockPendencyId lid = new LockPendencyId();
		l.setIssueDate(new Date());
		lid.setContractID(54);
		lid.setEndorsementID(1);
		l.setId(lid);
		l.setPendencyID(2);
		serviceBasicCalc.freePendency(l);
		Assert.assertTrue(true);
	}
}
