package br.com.tratomais.test.service;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.AbstractDependencyInjectionSpringContextTests;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.tratomais.core.dao.impl.DaoDomainImpl;
import br.com.tratomais.core.dao.impl.DaoRoleImpl;
import br.com.tratomais.core.dao.impl.DaoUserPartnerImpl;
import br.com.tratomais.core.model.Domain;
import br.com.tratomais.core.model.Role;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.UserPartner;
import br.com.tratomais.core.service.impl.ServiceUserImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:config/spring-application-context.xml" })
public class TestUser extends AbstractDependencyInjectionSpringContextTests {

	@Autowired
	@Qualifier("serviceUser")
	private ServiceUserImpl serviceUser;
//	@Autowired
//	@Qualifier("daoPartner")
//	private DaoPartnerImpl daoPartner;
	@Autowired
	@Qualifier("daoUserPartner")
	private DaoUserPartnerImpl daoUserPartner;
	@Autowired
	@Qualifier("daoDomain")
	private DaoDomainImpl daoDomain;
	@Autowired
	@Qualifier("daoRole")
	private DaoRoleImpl daoRole;

	@Test
	public void initTest() {

		Collection<UserPartner> userPartners = daoUserPartner.listAll();
		assertNotNull("Users Partners vazio", userPartners);

		Collection<Role> roles = daoRole.listAll();
		assertNotNull("Roles vazio", roles);

		Domain domain = this.createDomain();
		assertNotNull("Erro ao cadastrar o domain", domain);

		User user = this.createUser(domain, userPartners, roles);
		assertNotNull("Erro ao gravar o usu�rio", user);
	}

	private User createUser(Domain domain, Collection<UserPartner> userPartners, Collection<Role> roles) {
		User user = new User();
		user.setLogin("testeJunit");
		user.setEmailAddress("testeJunit@junit.com.br");
		user.setName("testeJunit");
//		user.setActive(true);
//		user.setAdministrator(false);
//		user.setDomain(domain);
//		user.setUserPartners(userPartners);
//		user.setRoles(roles);
		return serviceUser.saveObject(user);
	}

	private Domain createDomain() {
		Domain domain = new Domain();
		domain.setName("brasil");
		domain.setAutenticationType(0);
//		domain.setUsers(serviceUser.listAll());
//		domain.setPartners(daoPartner.listAll());
		return daoDomain.save(domain);
	}

	public void setServiceUser(ServiceUserImpl serviceUser) {
		this.serviceUser = serviceUser;
	}

//	public void setDaoPartner(DaoPartnerImpl daoPartner) {
//		this.daoPartner = daoPartner;
//	}

	public void setDaoUserPartner(DaoUserPartnerImpl daoUserPartner) {
		this.daoUserPartner = daoUserPartner;
	}

	public void setDaoDomain(DaoDomainImpl daoDomain) {
		this.daoDomain = daoDomain;
	}

	public void setDaoRole(DaoRoleImpl daoRole) {
		this.daoRole = daoRole;
	}
}
