package br.com.tratomais.test.service;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import br.com.tratomais.core.model.User;
import br.com.tratomais.core.service.IServiceLogin;
import br.com.tratomais.core.util.Utilities;

@ContextConfiguration(locations = { "classpath:config/spring-application-context.xml" })
public class TestLogin {

	private IServiceLogin serviceLogin;

	@Test
	public void testInjecaoSpring() {

		User login = null;
		try {
			login = serviceLogin.validateLogin("leo", Utilities.getHashSha("123"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		Assert.assertNotNull(login);
	}

	public void setServiceLogin(IServiceLogin serviceLogin) {
		this.serviceLogin = serviceLogin;
	}

}