import junit.framework.Test;
import junit.framework.TestSuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;

import br.com.tratomais.core.dao.IDaoSubsidiaryTest;
import br.com.tratomais.core.service.impl.ServiceEinsuranceImplTest;
import br.com.tratomais.core.util.DateUtilitiesTest;


@RunWith(Suite.class)
@SuiteClasses({DateUtilitiesTest.class, IDaoSubsidiaryTest.class,ServiceEinsuranceImplTest.class})
@Configurable(autowire = Autowire.BY_NAME)
@ContextConfiguration(locations = { "classpath:/config/spring-security-config.xml", "classpath:/srcTest/spring-tests-context.xml", "classpath:/config/spring-application-daos.xml",
		"classpath:/config/spring-application-services.xml" })
public class TestAllJUnitSuite {
	public static Test suite() {
		TestSuite suite = new TestSuite(TestAllJUnitSuite.class.getName());
		//$JUnit-BEGIN$

		//$JUnit-END$
		return suite;
	}
}
