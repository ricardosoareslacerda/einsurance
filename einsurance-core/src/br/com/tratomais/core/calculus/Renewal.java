package br.com.tratomais.core.calculus;

import java.util.LinkedList;
import java.util.List;

import br.com.tratomais.core.dao.IDaoMasterPolicy;
import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.model.policy.Contract;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.MasterPolicy;

public class Renewal {
	IDaoPolicy daoPolicy;
	public void setDaoPolicy(IDaoPolicy daoPolicy) {
		this.daoPolicy = daoPolicy;
	}
	
	IDaoMasterPolicy daoMasterPolicy;
	public void setDaoMasterPolicy(IDaoMasterPolicy daoMasterPolicy) {
		this.daoMasterPolicy = daoMasterPolicy;
	}
	
	public List<Endorsement> listContractToRenewal(){
		List<Endorsement> endorsements = new LinkedList<Endorsement>();
		List<MasterPolicy> renewableMasterPolicies = daoMasterPolicy.getRenewableMasterPolicies();
		for(MasterPolicy masterPolicy : renewableMasterPolicies){
			List<Contract> contracts = daoPolicy.listContractsPerMasterPolicy(masterPolicy);
			for(Contract contrato : contracts){
				Endorsement endorsement = daoPolicy.lastValidEndosementForContractNumber(contrato.getContractId());
				if (endorsement != null)
					endorsements.add(endorsement);
			}
		}
		return endorsements;
	}
}