package br.com.tratomais.core.calculus.chron.services;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.model.policy.Contract;

public class RenewalFinalizer implements Runnable {

	private IDaoPolicy daoPolicy;
	public void setDaoPolicy(IDaoPolicy daoPolicy) {
		this.daoPolicy = daoPolicy;
	}
	
	@Override
	@Transactional
	public void run() {
		finalizer(daoPolicy.listEndorsementRenewed());
		finalizer(daoPolicy.listEndorsementNotRenewed());
	}

	private void finalizer(List<Contract> listContracts) {
		for(Contract contract : listContracts) {
			contract.setContractStatus(Contract.CONTRACT_STATUS_FINALIZED);
			daoPolicy.saveContract(contract);
		}
	}
}
