package br.com.tratomais.core.calculus.chron;

public class ParseException extends Exception {
	private static final long serialVersionUID = 1L;

	public ParseException() {
		super();
	}

	public ParseException(String message, Throwable cause) {
		super(message, cause);
	}

	public ParseException(String message) {
		super(message);
	}

	public ParseException(String message, int lineNumber) {
		super(message);
		this.lineNumber = lineNumber;
	}

	public ParseException(Throwable cause) {
		super(cause);
	}
	
	@Override
	public String getMessage() {
		if (lineNumber == null)
			return super.getMessage();
		return super.getMessage() + " Line Number: " + lineNumber;
	}
	
	private Integer lineNumber;
	public Integer getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}
}
