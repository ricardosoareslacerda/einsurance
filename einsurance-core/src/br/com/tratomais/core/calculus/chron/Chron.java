package br.com.tratomais.core.calculus.chron;

import it.sauronsoftware.cron4j.Scheduler;
import it.sauronsoftware.cron4j.Task;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

import org.springframework.context.ApplicationContext;

public class Chron {
	private static Chron chron;

	public final static Chron getSingleton(ApplicationContext applicationContext) {
		if (Chron.chron == null)
			Chron.chron = new Chron(applicationContext);
		return chron;
	}
	
	private Scheduler scheduler = new Scheduler();
	
	private ApplicationContext applicationContext;
	
	public Chron(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
	
	public void loadConfiguration(File configuration) throws IOException, ParseException, ClassNotFoundException{
		FileInputStream fileInputStream = new FileInputStream(configuration);
		try {
			loadConfiguration(fileInputStream);
		}
		finally {
			fileInputStream.close();
		}
	}

	public void loadConfiguration(InputStream configuration) throws IOException, ParseException, ClassNotFoundException{
		InputStreamReader streamReader = new InputStreamReader(configuration);
		try {
			LineNumberReader lnr = new LineNumberReader(streamReader);
			String linha = null;
			while (((linha = lnr.readLine())!= null)){
				try {
					if ((! "".equals(linha = linha.trim()))&&(linha.charAt(0)!='#'))
						parseLine(linha);
				}
				catch(ParseException p){
					p.setLineNumber(lnr.getLineNumber());
					throw p;
				}
			}
			lnr.close();
		}
		finally {
			streamReader.close();
		}
	}

	private void parseLine(String linha) throws ParseException, ClassNotFoundException {
		String[] split = linha.split(" ");
		String aux="";
		Object bean = null;
		
		// Somente a configuração relativa ao Cron
		for(int x=0;x<5;x++)
			aux=aux+split[x]+' ';
		aux = aux.trim();
		
		if ("S".equals(split[5])){
			bean = applicationContext.getBean(split[6].trim());
		} else {
			try {
				bean = Class.forName(split[6].trim()).newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		if (bean instanceof Runnable)
			scheduler.schedule(aux, (Runnable) bean);
		else if (bean instanceof Task)
			scheduler.schedule(aux, (Task) bean);
	}
	
	private boolean iniciado = false;
	
	public void start() {
		if(!iniciado) {
			scheduler.start();
			iniciado = true;
		}
	}

	public void stop() {
		if(iniciado) {
			scheduler.stop();
			iniciado = false;
		}
	}
}
