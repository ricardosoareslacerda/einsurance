package br.com.tratomais.core.calculus.chron.services;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.log4j.Logger;

import br.com.tratomais.core.calculus.Renewal;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.webservices.client.EInsuranceServicesStub;

public class RenewalService implements Runnable {

	private Logger logger = Logger.getLogger(Renewal.class);

	private Renewal renewal;
	public void setRenewal(Renewal renewal) {
		this.renewal = renewal;
	}

	private String configurationFileName;

	private long timeout;
	public void setConfigurationFileName(String configurationFileName) {
		this.configurationFileName = configurationFileName;
	}
	
	@Override
	public void run() {
		List<Endorsement> contractToRenewal = renewal.listContractToRenewal();
		String serverEndPoint = null, password = null, login = null;
		if ( this.configurationFileName != null) {
			Properties properties = new Properties();
			InputStream resourceAsStream = null;
			try {
				resourceAsStream = new FileInputStream(configurationFileName); 
				properties.load(resourceAsStream);
				serverEndPoint = properties.getProperty("einsurance.endpoint", null);
				login = properties.getProperty("einsurance.login", null);
				password = properties.getProperty("einsurance.password", null);
				timeout = Long.parseLong(properties.getProperty("einsurance.timeout", "60"));
			} catch (IOException e) {
				logger.error("No fue posible cargar el archivo de configuración: " + this.configurationFileName);
				e.printStackTrace();
			}
			finally{
				if ( resourceAsStream != null ) {
					try {
						resourceAsStream.close();
					} catch (IOException e) {
						logger.error(e);
						e.printStackTrace();
					}
				}
			}
		}

		EInsuranceServicesStub insuranceServicesStub = null;
		try {
			if (serverEndPoint != null) {
				insuranceServicesStub = new EInsuranceServicesStub(serverEndPoint);
			} else {
				insuranceServicesStub = new EInsuranceServicesStub();
			}
			Options options = insuranceServicesStub._getServiceClient().getOptions();
			options.setTimeOutInMilliSeconds(timeout*1000);
			insuranceServicesStub._getServiceClient().setOptions(options);
			for (Endorsement inputEndorsement : contractToRenewal) {
				EInsuranceServicesStub.Renewal renewalStub = new EInsuranceServicesStub.Renewal();
				renewalStub.setContractId(inputEndorsement.getId().getContractId());
				renewalStub.setEndorsementId(inputEndorsement.getId().getEndorsementId());
				renewalStub.setLogin(login);
				renewalStub.setPassword(password);
				try {
					insuranceServicesStub.renewal(renewalStub);
				} catch (Throwable e) {
					logger.error("Error realizando renovação", e);
					e.printStackTrace();
				}
			}
		}
		catch (AxisFault e1) {
			logger.error("Error realizando renovação", e1);
			e1.printStackTrace();			
		}
	}
}
