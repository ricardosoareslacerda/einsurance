package br.com.tratomais.core.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import br.com.tratomais.core.claim.ReturnClaimStateVerification;
import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.model.City;
import br.com.tratomais.core.model.Country;
import br.com.tratomais.core.model.Domain;
import br.com.tratomais.core.model.Functionality;
import br.com.tratomais.core.model.Inflow;
import br.com.tratomais.core.model.Login;
import br.com.tratomais.core.model.Module;
import br.com.tratomais.core.model.PartnerAndChannel;
import br.com.tratomais.core.model.Role;
import br.com.tratomais.core.model.State;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.auto.AutoBrand;
import br.com.tratomais.core.model.auto.AutoModel;
import br.com.tratomais.core.model.auto.AutoVersion;
import br.com.tratomais.core.model.customer.Activity;
import br.com.tratomais.core.model.customer.Broker;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Customer;
import br.com.tratomais.core.model.customer.Insurer;
import br.com.tratomais.core.model.customer.Occupation;
import br.com.tratomais.core.model.customer.Partner;
import br.com.tratomais.core.model.customer.Profession;
import br.com.tratomais.core.model.customer.Subsidiary;
import br.com.tratomais.core.model.interfaces.FileLot;
import br.com.tratomais.core.model.interfaces.FileLotDetails;
import br.com.tratomais.core.model.interfaces.Lot;
import br.com.tratomais.core.model.paging.Paginacao;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.EndorsementOption;
import br.com.tratomais.core.model.policy.ItemPersonalRisk;
import br.com.tratomais.core.model.policy.MasterPolicy;
import br.com.tratomais.core.model.product.BillingAgency;
import br.com.tratomais.core.model.product.BillingMethod;
import br.com.tratomais.core.model.product.Branch;
import br.com.tratomais.core.model.product.ConversionRate;
import br.com.tratomais.core.model.product.Coverage;
import br.com.tratomais.core.model.product.CoverageOption;
import br.com.tratomais.core.model.product.CoveragePlan;
import br.com.tratomais.core.model.product.CoverageRangeValue;
import br.com.tratomais.core.model.product.Currency;
import br.com.tratomais.core.model.product.DataOption;
import br.com.tratomais.core.model.product.Deductible;
import br.com.tratomais.core.model.product.IdentifiedList;
import br.com.tratomais.core.model.product.InsuredObject;
import br.com.tratomais.core.model.product.PaymentOption;
import br.com.tratomais.core.model.product.PersonalRiskOption;
import br.com.tratomais.core.model.product.Plan;
import br.com.tratomais.core.model.product.PlanKinship;
import br.com.tratomais.core.model.product.PlanRiskType;
import br.com.tratomais.core.model.product.Product;
import br.com.tratomais.core.model.product.ProductOption;
import br.com.tratomais.core.model.product.ProductPaymentTerm;
import br.com.tratomais.core.model.product.ProductPlan;
import br.com.tratomais.core.model.product.ProductTerm;
import br.com.tratomais.core.model.product.ProductVersion;
import br.com.tratomais.core.model.product.Profile;
import br.com.tratomais.core.model.product.ProposalListData;
import br.com.tratomais.core.model.product.QuoteListData;
import br.com.tratomais.core.model.product.ResponseRelationship;
import br.com.tratomais.core.model.product.SimpleCalcResult;
import br.com.tratomais.core.model.report.BillingMethodResult;
import br.com.tratomais.core.model.report.CollectionDetailsMainParameters;
import br.com.tratomais.core.model.report.CollectionDetailsMainResult;
import br.com.tratomais.core.model.report.CollectionMainParameters;
import br.com.tratomais.core.model.report.CollectionMainResult;
import br.com.tratomais.core.model.report.DataViewInterface;
import br.com.tratomais.core.model.report.Report;
import br.com.tratomais.core.model.report.ReportListData;
import br.com.tratomais.core.model.report.StatusLote;
import br.com.tratomais.core.model.search.SearchDocumentParameters;
import br.com.tratomais.core.model.search.SearchDocumentResult;
import br.com.tratomais.core.service.erros.ErrorList;
import br.com.tratomais.validation.bank.BankValidationException;
import br.com.tratomais.validation.bank.IAccountInfo;

public interface IServiceEinsurance {

	public Login saveLoginByUser(User user) throws ServiceException;

	public User validateLogin(String userName, String passwordKey) throws ServiceException;

	/**
	 * Tests login existance
	 * @param login login 
	 * @return true or false
	 * @throws ServiceException
	 */
	public boolean loginExistence(String login) throws ServiceException;

	public Login saveLogin(Login login) throws ServiceException;

	/**
	 * Deletes login based on user name and domain name
	 * @param loginName User name (login name)
	 * @param domain Domain name
	 * @throws ServiceException
	 */
	public void deleteLoginByLoginName(String loginName, String domain) throws ServiceException;

	public Login findLoginByUser(User user) throws ServiceException;

	public Broker saveBroker(Broker broker) throws ServiceException;

	public List<Module> listAllModuleByUser(User user) throws ServiceException;

	public List<Module> listAllModule() throws ServiceException;

	public List<Broker> listAllBroker() throws ServiceException;
	
	public Module findModuleById(int id) throws ServiceException;
	
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId, int endorsementId, int itemId) throws ServiceException;
	
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId, int endorsementId) throws ServiceException;
	
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId) throws ServiceException;
	
	/**
	 * Find a {@link Broker} by broker Id.
	 * 
	 * @param id
	 *            (Integer) to filter a Broker
	 * @return The Object of {@link Broker}
	 */
	public Broker findBrokerById(Integer brokerId) throws ServiceException;

	public List<Channel> listAllChannel() throws ServiceException;

	public List<Channel> findChannelByPartnerId(int partnerId, boolean onlyActive) throws ServiceException;

	public List<Channel> findChannelByNameAndPartner(String name, Partner partner) throws ServiceException;

	/**
	 * Find a {@link Broker} by partner Id.
	 * 
	 * @param id
	 *            (Integer) to filter a Partner
	 * @return The Object of {@link Partner}
	 */
	public Partner findPartnerById(Integer partnerId) throws ServiceException;	

	public List<City> listAllCity() throws ServiceException;

	public List<Country> listAllCountry() throws ServiceException;

	public List<DataOption> findDataOptionByDataGroup(int dataGroupId) throws ServiceException;

	public List<DataOption> findDataOptionByDataGroup(int dataGroupId, boolean bringAll) throws ServiceException;
	
	public List<DataOption> findDataOptionByDataGroup(int dataGroupId, boolean bringAll, String externalCode) throws ServiceException;
	
	public Subsidiary saveSubsidiary(Subsidiary subsidiary) throws ServiceException;

	public List<Subsidiary> listAllSubsidiary() throws ServiceException;

	public List<Subsidiary> listAllActiveSubsidiary() throws ServiceException;
	
	/**
	 * Find a {@link Subsidiary} by subsidiary Id.
	 * 
	 * @param id (Integer) to filter a Subsidiary
	 * @return The Object of {@link Subsidiary}
	 */
	public Subsidiary findSubsidiaryById(Integer subsidiaryId) throws ServiceException;

	public List<Partner> listAllPartner() throws ServiceException;

	public List<Partner> listAllPartner(User user) throws ServiceException;

	public List<Partner> listAllActivePartner() throws ServiceException;

	public List<Partner> listAllActivePartner(User user) throws ServiceException;

	public Partner savePartner(Partner entity) throws ServiceException;

	public Partner savePartnerInsert(Partner entity) throws ServiceException;

	public Collection<Role> listAllRoleByUser(User user) throws ServiceException;

	public Collection<Role> listAllActiveRole() throws ServiceException;

	public Collection<Role> listAllRole() throws ServiceException;

	public List<State> listAllState() throws ServiceException;

	public User saveUser(User user, Login login) throws ServiceException;

	public User saveUserInsert(User user, Login login) throws ServiceException;

	/**
	 * Find a {@link User} by userId.
	 * 
	 * @param id (Integer) to filter a Subsidiary
	 * @return The Object of {@link User}
	 */
	public User findUserById(Integer userId) throws ServiceException;	

	public List<User> listUserAllowed() throws ServiceException;	
	
	public void changePartnerDefaultLoggedUser(int partnerId) throws ServiceException;

	public List<MasterPolicy> listAllMasterPolicy() throws ServiceException;
	
	public MasterPolicy findMasterPolicyById(int id) throws ServiceException;

	public List<Product> listAllProduct() throws ServiceException;

	public List<Profile> findProfileList(int productId, Date referenceDate) throws ServiceException;

	public MasterPolicy saveMasterPolicy(MasterPolicy masterPolicy) throws ServiceException;

	public List<MasterPolicy> listAllWithDescriptions() throws ServiceException;

	/**
	 * Find a {@link ProductVersion} by primary key.
	 * 
	 * @param id
	 *            to filter a product
	 * @return The Object of {@link ProductVersion}.
	 */
	public ProductVersion findProductVersionById(Integer id) throws ServiceException;

	/**
	 * Save a new value into a {@link ProductVersion}.
	 * 
	 * @param productVersion
	 *            the {@link ProductVersion}
	 * @return the {@link ProductVersion} added.
	 */
	public ProductVersion saveProductVersion(ProductVersion productVersion) throws ServiceException;

	/**
	 * Find a {@link Coverage} by primary key.
	 * 
	 * @param id
	 *            (Integer) to filter a Coverage
	 * @return The Object of {@link Coverage}
	 */
	public Coverage findCoverageById(Integer id) throws ServiceException;

	/**
	 * Gets a list of {@link Coverage}
	 * 
	 * @param name
	 *            (String) to filter a product
	 * @return The Object of {@link Coverage}
	 */
	public List<Coverage> findCoverageByName(String name) throws ServiceException;

	/**
	 * Gets a list of all {@link Coverage}
	 * 
	 * @return The list of {@link Coverage}
	 */
	public List<Coverage> listAllCoverage() throws ServiceException;

	/**
	 * Save a value into a {@link Coverage}.
	 * 
	 * @param coverage
	 *            the {@link Coverage}
	 * @return the {@link Coverage} added.
	 */
	public Coverage saveCoverage(Coverage coverage) throws ServiceException;

	/**
	 * Gets a list of all {@link Coverage} attached to a {@link InsuredObject}.
	 * 
	 * @param objectId
	 *            of a {@link InsuredObject}.
	 * @return The list of {@link Coverage} attached to a {@link Coverage}.
	 */
	public List<Coverage> findAllCoverageByObject(Integer objectId) throws ServiceException;

	/**
	 * Find a {@link CoveragePlan} by primary key.
	 * 
	 * @param id
	 *            of a {@link CoveragePlan}.
	 * @return The object of {@link CoveragePlan}
	 */
	public CoveragePlan findCoveragePlanById(Integer id) throws ServiceException;

	/**
	 * Save a value into a {@link CoveragePlan}.
	 * 
	 * @param coveragePlan
	 *            the {@link CoveragePlan}
	 * @return the {@link CoveragePlan} added.
	 */
	public CoveragePlan saveCoveragePlan(CoveragePlan coveragePlan) throws ServiceException;

	/**
	 * Gets a list of all {@link Deductible}
	 * 
	 * @return The list of {@link Deductible}
	 */
	public List<Deductible> listAllDeductible() throws ServiceException;
	
	/**
	 * Save a value into a {@link Plan}.
	 * 
	 * @param plan
	 *            the {@link Plan}
	 * @return the {@link Plan} saved.
	 */
	public Plan savePlan(Plan plan) throws ServiceException;

	/**
	 * Find a {@link Plan} by primary key.
	 * 
	 * @param id
	 *            of a {@link Plan}.
	 * @return The object of {@link Plan}
	 */
	public Plan findPlanById(Integer id) throws ServiceException;

	/**
	 * Gets a list of {@link Plan} by name.
	 * 
	 * @param name
	 *            (String) of a {@link Plan}.
	 * @return The list of {@link Plan}.
	 */
	public List<Plan> findPlanByName(String name) throws ServiceException;

	/**
	 * Gets a list of all {@link Plan}.
	 * 
	 * @return The list of {@link Plan}.
	 */
	public List<Plan> listAllPlan() throws ServiceException;

	/**
	 * Find a {@link InsuredObject} by primary key.
	 * 
	 * @param id
	 *            of a {@link InsuredObject}.
	 * @return The object of {@link InsuredObject}
	 */
	public Object findObjectById(Integer id) throws ServiceException;

	/**
	 * Gets a list of {@link InsuredObject} by name.
	 * 
	 * @param name
	 *            (String) of a {@link InsuredObject}.
	 * @return The list of {@link InsuredObject}.
	 */
	public List<br.com.tratomais.core.model.product.InsuredObject> findObjectByName(String name) throws ServiceException;

	/**
	 * Gets a list of all {@link InsuredObject}.
	 * 
	 * @return The list of {@link InsuredObject}.
	 */
	public List<br.com.tratomais.core.model.product.InsuredObject> listAllObject() throws ServiceException;

	/**
	 * Save a value into a {@link InsuredObject}.
	 * 
	 * @param insuredObject
	 *            the {@link InsuredObject}
	 * @return the {@link InsuredObject} saved.
	 */
	public Object saveObject(br.com.tratomais.core.model.product.InsuredObject insuredObject) throws ServiceException;

	/**
	 * Find a {@link ProductVersion} by partenerId and insurerId and refDate.
	 * 
	 * @param partnerId
	 *            Id of partner
	 * @param insurerId
	 *            Id of insurer
	 * @param refDate
	 *            Date Reference of productVersion
	 * @return The {@link List} of {@link ProductVersion}.
	 */

	public List<ProductOption> findProductsbyPartner(int partnerId, int insurerId, Date refDate) throws ServiceException;

	public List<CoveragePlan> listCoveragePlanByRefDate(int productId, int planId, Date refDate) throws ServiceException;

	public List<ProductPlan> findPlanByProductId(int productId, Date refDate) throws ServiceException;

	public List<ProductPlan> findRiskPlanByProductId(int productId, Date refDate) throws ServiceException;

	public List<ProductPlan> listPersonalRiskPlanByProductId(int productId, Date refDate) throws ServiceException;

	public List<Activity> listAllActivity() throws ServiceException;

	public List<Inflow> listAllInflow(String inflowType) throws ServiceException;

	public List<Profession> listAllProfession() throws ServiceException;

	public List<Occupation> listAllOccupation() throws ServiceException;

	public Role saveRole(Role entity) throws ServiceException;

	public Channel saveChannel(Channel entity) throws ServiceException;

	public List<Domain> listAllDomain() throws ServiceException;

	public List<Domain> listAllDomainActive() throws ServiceException;

	public Endorsement saveEndorsement(Endorsement endorsement) throws ServiceException;

	public Customer saveCustomer(Customer customer) throws ServiceException;

	public SimpleCalcResult simpleCalc(Endorsement endorsement) throws ServiceException;

	public SimpleCalcResult propertyCalc (Endorsement endorsement, List<CoverageOption> coverageOptionList);
	
	public SimpleCalcResult calculate (Endorsement endorsement, int objectId, List<CoverageOption> coverageOptionList);
	
	public ProposalListData loadProposalList() throws ServiceException;

	public ReportListData loadReportList(int partnerId, int issuingStatus, int installmentStatus, int annulmentType, String externalCode, boolean active) throws ServiceException;
	
	public QuoteListData loadQuoteList(int productId, Date referenceDate, int plaId, int objectId) throws ServiceException;

	public ReportListData loadReportListAnnulment(Integer partnerId, Integer productId, Integer brokerId) throws ServiceException;
	
	public List<Partner> listPartnerPerProductOrBroker(Integer partnerId, Integer productId, Integer brokerId) throws ServiceException;
	
	public List<Broker> listBrokerPerProductOrPartner(Integer partnerId, Integer productId, Integer brokerId) throws ServiceException;
	
	public List<Product> listProductPerPartnerOrBroker(Integer partnerId, Integer productId, Integer brokerId) throws ServiceException;
	
	/**
	 * @return Modulos que lista os modulos
	 * @return modulos do user logado @
	 **/
	public List<Module> listAllowedModulesByUserLogged() throws ServiceException;

	/**
	 * Find all Personal risk options to be used in insured view component
	 * 
	 * @param productId
	 * @param riskPlanId
	 * @param referenceDate
	 * @return List of PersonalRiskOption
	 * @see PersonalRiskOption
	 */
	public List<PersonalRiskOption> findPersonalRiskOptions(int productId, int riskPlanId, Date referenceDate) throws ServiceException;

	public List<PartnerAndChannel> listAllPartnerChannel(User user) throws ServiceException;

	/**
	 * Find all Paymnet type options
	 * 
	 * @param productId
	 * @param referenceDate
	 * @return List of {@link BillingMethod}
	 */
	public List<BillingMethod> listPaymentType(int productId, Integer paymentTermType, Date referenceDate) throws ServiceException;

	/**
	 * Find users by partner'
	 * 
	 * @param partnerId
	 * @return {@link List} {@link User}
	 */
	public List<User> listUserByPartnerId(int partnerId) throws ServiceException;

	/**
	 * Fill users of channel
	 * 
	 * @param channel
	 * @return {@link List} {@link User}
	 */
	public Set<User> fillListUserByChannel(Channel channel) throws ServiceException;

	/**
	 * Return the identified list of country
	 * 
	 * @param identifiedList
	 * @return {@link IdentifiedList}
	 */
	public IdentifiedList listCountry(IdentifiedList identifiedList) throws ServiceException;

	/**
	 * Return the identified list of country
	 * 
	 * @param countryId
	 * @return {@link countryId}
	 */
	public List<Country> listCountryId(int countryId) throws ServiceException;	
	/**
	 * Return the identified list of {@link City}
	 * 
	 * @param identifiedList
	 * @return {@link IdentifiedList}
	 */
	public IdentifiedList listIdentifiedListCity(IdentifiedList identifiedList) throws ServiceException;
	
	/**
	 * Return the identified list of {@link State} by {@link Country}
	 * 
	 * @param identifiedList
	 * @param countryId
	 * @return {@link IdentifiedList}
	 */
	public IdentifiedList listStateByCountry(IdentifiedList identifiedList, int countryId) throws ServiceException;

	
	/**
	 * Return the identified list of {@link State} by {@link Country}
	 * 
	 * @param identifiedList
	 * @param countryId
	 * @return {@link IdentifiedList}
	 */
	public List<State> listStateByCountryId(int countryId) throws ServiceException;	

	/**
	 * Return the {@link IdentifiedList} of {@link City} by {@link State}
	 * 
	 * @param identifiedList
	 * @param stateId
	 * @return {@link IdentifiedList}
	 */
	public IdentifiedList listCityByState(IdentifiedList identifiedList, int stateId) throws ServiceException;
	
	/**
	 * Return the {@link IdentifiedList} of {@link City} by {@link State}
	 * 
	 * @param identifiedList
	 * @param stateId
	 * @return {@link IdentifiedList}
	 */
	public List<City> listCityByStateId(int stateId) throws ServiceException;	

	/**
	 * Return a result set for be use in search document
	 * 
	 * @param link{@link SearchDocumentParameters}
	 * @return {@link List} {@link SearchDocumentResult}
	 */
	public List<SearchDocumentResult> listSearchDocument(SearchDocumentParameters searchDocumentParameters) throws ServiceException;

	/**
	 * Return a result set for be use in search document with paging
	 * 
	 * @param link{@link SearchDocumentParameters}
	 * @return {@link Paginacao}
	 */
	public Paginacao listSearchDocumentPaging(SearchDocumentParameters searchDocumentParameters) throws ServiceException;
	
	/**
	 * Return a result set for be use in batch administration main
	 * 
	 * @param link{@link CollectionMainParameters}
	 * @return {@link List} {@link CollectionMainParameters}
	 */
	public List<CollectionDetailsMainResult> listCollectionDetailsMain(CollectionDetailsMainParameters collectionDetailsMainParameters) throws ServiceException;	
	/**
	 * Return a result set for be use in batch administration with paging
	 * 
	 * @param link{@link CollectionMainParameters}
	 * @return {@link Paginacao}
	 */
	public Paginacao listCollectionDetailsMainPaging(CollectionDetailsMainParameters collectionDetailsMainParameters) throws ServiceException;

	/**
	 * Return a result set for be use in combobox of the batch administration main
	 * 
	 * @return {@link List} {@link BillingMethodResult}
	 */	
	public List<BillingMethodResult> listBillingMethod() throws ServiceException;
	
	/**
	 * Return a result set for be use in combobox of the batch administration main
	 * 
	 * @return {@link List} {@link StatusLote}
	 */		
	public List<StatusLote> listStatusLote() throws ServiceException;
	
	/**
	 * Return a result set for be use in datagrid of the batch administration main
	 * @param collectionMainParameters
	 * @return {@link List} {@link BillingMethodResult}
	 */		
	public List<CollectionMainResult> listCollectionMain(CollectionMainParameters collectionMainParameters)throws ServiceException;
	
	/**
	 * Return a result set for be use in f the batch administration main
	 * 
	 * @param link{@link CollectionMainParameters}
	 * @return {@link Paginacao}
	 */
	public Paginacao listCollectionMainPaging(CollectionMainParameters collectionMainParameters) throws ServiceException;
	
	/**
	 * Payment Options
	 * 
	 * @param endorsement
	 * @return List of Payment Options
	 */
	public List<PaymentOption> getPaymentOptionsDefault(Endorsement endorsement) throws ServiceException;

	/**
	 * Payment Options
	 * 
	 * @param totalPremium
	 * @param productId
	 * @param refDate
	 * @return List of Payment Options
	 */
	public List<PaymentOption> getPaymentOptionsDefault(Double totalPremium, Double taxRate, int productId, Date refDate, Date dtEffective, Date dtExpiry) throws ServiceException;

	/**
	 * Payment Options per Endorsement
	 * 
	 * @param endorsement
	 * @param billingMethodId
	 * @return List of Payment Options
	 */
	public List<PaymentOption> getPaymentOptionsByBillingMethodId(Endorsement endorsement, int billingMethodId, Date dtEffective, Date dtExpiry) throws ServiceException;
	
	/**
	 * Payment Option
	 * @param totalPremium
	 * @param productId
	 * @param billingMethodId
	 * @param paymentTermId
	 * @param refDate
	 * @param dtEffective
	 * @param dtExpiry
	 * @return payment Option
	 */
	public PaymentOption getPaymentOptionByPaymentTermId(Double netPremiumAndCosts, double taxRate, int productId, int billingMethodId, int paymentTermId, Date refDate, Date dtEffective, Date dtExpiry) throws ServiceException;
	
	/**
	 * Retrieves payment options according to data 
	 * @param totalPremium Total premium 
	 * @param taxRate Tax rate used
	 * @param productId Product
	 * @param paymentType Payment Type
	 * @param refDate Reference date 
	 * @param effectiveDate Insurance starting date
	 * @param expireDate Insurance finishing date
	 * @return List of Payment types
	 * @throws ServiceException
	 */
	public List<PaymentOption> getPaymentOptionsByPaymentType(Double totalPremium, double taxRate, int productId, int paymentType, Date refDate, Date effectiveDate, Date expireDate) throws ServiceException;

	/**
	 * Get the Product to be copy
	 * 
	 * @param productId
	 * @param referenceDate
	 * @return {@link Product}
	 */
	public Product findProductToBeCopy(int productId, Date referenceDate) throws ServiceException;

	public void logout() throws ServiceException;

 	public Endorsement effectiveProposal(Endorsement endorsement, PaymentOption paymentOption) throws ServiceException;
	
	/**
	 * 
	 * @param endorsement
	 * @param paymentOption
	 * @param issuanceType
	 * @param newEffectiveDate
	 * @return
	 * @throws ServiceException
	 */
	public Endorsement effectiveEndorsement(Endorsement endorsement, PaymentOption paymentOption, int issuanceType, Date newEffectiveDate) throws ServiceException;

	public List<ProductVersion> getProductVersionByProduct(int productId) throws ServiceException;

	public ProductVersion saveProductVersionCopy(ProductVersion inputProductVersion) throws ServiceException;
	
	public List<Product> findProductsPerPartner(int partnerId);
	
	public List<Channel> findByUserAndPartner(User user, int partnerId);
	public List<Channel> findByLoggedUserAndPartner(int partnerId);
	public List<User> listUserPerPartner(int partnerId);
	public DataOption getDataOptionById(int dataOptionId);
	public ProductPlan getPlanById(int productId, int planId, Date refDate);
	public ProductPlan getRiskPlanById(int productId, int planId, Date refDate);
	public List<CoverageOption> listCoverageHabitatByPlanId(Endorsement end, int productId, int coveragePlanId, int riskPlanId, Date refDate) throws ServiceException;
	public List<CoverageOption> listCoverageOptionByRefDate(int productId, int coveragePlanId, Date refDate) throws ServiceException;

	/**
	 * Gets a list of planKingShip
	 * @param productId Product Identificator
	 * @param planId  Plan Identificator
	 * @param renewalType Renewal Type
	 * @param refDate Reference Date
	 * @return PlanKinShip 
	 */
	public List<PlanKinship> listPlanKinshipByRiskPlan(int productId, int riskPlanId, int renewalType, Date refDate);

	public SimpleCalcResult lifeCycleCalc (Endorsement endorsement, List<CoverageOption> coverageOptionList);

	/**
	 * Changes password for user
	 * @param login User login
	 * @param domain Domain name
	 * @param oldPassword Old password
	 * @param newPassword new password
	 * @return if password changed
	 */
	public boolean changeUserPassword( String login, String domain, String oldPassword, String newPassword);
	
	public EndorsementOption loadUnlockableEndorsement (int endorsementId, int contractId);

	public Boolean unlockEndorsement (int endorsementId, int contractId);
	
	public Boolean unlockEndorsement (int endorsementId, int contractId, Date unlockDate) throws ServiceException;
	
	public Endorsement policyChangeRegister(Endorsement endorsement, Date referenceDate ) throws ServiceException;
	
	public Endorsement policyChangeTechnical(Endorsement endorsement, Date referenceDate, boolean isRecalculation, boolean isProposal, PaymentOption paymentOption) throws ServiceException;

	public IdentifiedList findIdentifiedListDataOptionByDataGroup(int dataGroupId, String identifierString, Date refDate);

	public IdentifiedList findIdentifiedListDataOptionByDataGroupParentId(int dataGroupId, int parentId, String identifierString, Date refDate);
	
	public ProductVersion getProductVersionByRefDate(int productId, Date refDate);
	
	public Product getProductById(int productId);
	
	public Boolean cancellationProposal (int endorsementId, int contractId, int reason);
	
	public List<ProductPlan> listCoveragePlanByProductId(int productId, int riskPlanId, Date refDate);
	
	/**
	 * Checks for Subsidiarry's external code existence
	 * @param insurerId 
	 * @param subsidiaryId
	 * @param externalCode
	 * @return true if exists 
	 */
	public boolean subsidiaryExternalCodeExists(int insurerId, int subsidiaryId, String externalCode);	

	/**
	 * Returns the number documents permitted to a document/product
	 * @param productId Product Register 
	 * @param docNumber Document number
	 * @param docType Document type
	 * @param dateIni Starting date
	 * @param dateFin End date
	 * @param contractId Contract identification
	 * @return Number of policies already in the system
	 */
	public int numActivePoliciesPerProductDocument(int productId, String docNumber, int docType, Date dateIni, Date dateFin, int contractId);
	
	public void logAuditAction(String message);	

	/**
	 * Call contract validation info 
	 * @param productId Product Identificator
	 * @param docNumber Document number
	 * @param docType Document type
	 * @paran dateIni Begining date
	 * @param referenceDate reference date
	 * @param contractId Contract identificator to be excluded from query
	 * @return List validation error messages
	 */
	 public List<String> validateContract(int productId, String docNumber, int docType, Date dateIni, Date referenceDate, int contractId);

	 
	 /**
	  * Return the list of @see CoverageRangeValue by reference date.
	  * @param productId
	  * @param coveragePlanId
	  * @param coverageId
	  * @param refDate
	  * @return
	  */
	 public List <CoverageRangeValue> listCoverageRangeValueByRefDate(int productId, int coveragePlanId, int coverageId ,Date refDate);

	 /**
	  * Return the list of @see PlanRiskTypeDescription by reference date.
	  * @param productId
	  * @param planId
	  * @param refDate
	  * @return
	  */
	 public List <PlanRiskType> listPlanRiskTypeDescriptionByRefDate(int productId, int planId, Date refDate);	 

	 /**
	  * Return the list of @see PlanKinship by Personal Risk Type.
	  * @param productId
	  * @param planId
	  * @param personalRiskType
	  * @param refDate
	  * @return
	  */	 
	 public PlanKinship listPlanKinshipByPersonalRiskType(int productId, int riskPlanId, int personalRiskType, int renewalType, Date refDate);

	 /**
	  * 
	  * @param contractId
	  * @return
	  * @throws ServiceException
	  */
	 public ReturnValue policyReactivation(int contractId) throws ServiceException;
	 
	 /**
	  * 
	  * @param contractId
	  * @param endorsementId
	  * @param checkDate
	  * @param issuanceType
	  * @return
	  * @throws ServiceException
	  */
	 public ReturnClaimStateVerification claimStateVerification(int contractId, Date checkDate, int issuanceType) throws ServiceException;
	 
	 /**
	  * 
	  * @param contractId
	  * @param issuanceType
	  * @param cancelDate
	  * @param cancelReason
	  * @return
	  * @throws ServiceException
	  */
	 public ReturnValue policyCancellation(int contractId, int issuanceType, Date cancelDate, int cancelReason) throws ServiceException;
	 
	 public ErrorList listRenewalAlert(int contractId, int endorsementId, boolean resolved) throws ServiceException;
	 
	 public String getApplicationPropertyValue(int applicationId, String propertyName) throws ServiceException;
	 
	 public IdentifiedList getApplicationPropertyValue(IdentifiedList identifiedList, int applicationId, String propertyName) throws ServiceException;
	 
	 public List<AutoBrand> listAutoBrands() throws ServiceException;
	 
	 public List<AutoModel> listAutoModelForBrand(int autoBrandId) throws ServiceException;
	 
	 public List<Integer> listAutoYear(int autoModelId, Date referenceDate) throws ServiceException;
	 
	 public Double getAutoCost(int autoModelId, Integer year, Date referenceDate) throws ServiceException;

	 public AutoVersion findAutoVersion(int autoModelId, Date referenceDate) throws ServiceException;
	 
	 public ProductOption getProduct(int productId, Date referenceDate) throws ServiceException;
	 
	 public ProductVersion getProductVersionByBetweenRefDate(int productId, Date RefDate) throws ServiceException;

	 public List<ResponseRelationship> getResponseRelationship(Integer productId, Integer questionnaireId, Integer questionId, Integer responseId, Date referenceDate);

	 public List<BillingMethodResult> listExchangeCollection();
	
	 
	 /**
	  * Retorna um objeto Lot
	  * @param lotId
	  * @return lot
	  * @throws ServiceException
	  */
	public Lot getLot(Integer lotId) throws ServiceException;
	
	 /**
	  * Retorna um objeto IdentifiedList contendo um objeto Paginacao com uma lista de Lot e parametros de pagina
	  * @param exchangeType
	  * @param lotStatus
	  * @param rangeBefore
	  * @param rangeAfter
	  * @return IdentifiedList
	  * @throws ServiceException
	  */
	public IdentifiedList listLot(IdentifiedList identifiedList, Integer exchangeId, Integer lotStatus, Date rangeBefore, Date rangeAfter) throws ServiceException;
	
	/**
	  * Retorna um objeto Lot
	  * @param lotId
	  * @param parentId
	  * @param exchangeId
	  * @param insurerId
	  * @param partnerId
	  * @return Lot
	  * @throws ServiceException
	  */
	public Lot getLot(Integer lotId, Integer parentId, Integer exchangeId, Integer insurerId, Integer partnerId) throws ServiceException;
	
	 /**
	  * Retorna dados para preencher os combos da tela IntefaceViewMain
	  * @return DataViewInterface
	  * @throws ServiceException
	  */
	public DataViewInterface getDataViewInterface() throws ServiceException;
	
	 /**
	  * Retorna um objeto FileLot
	  * @param fileLotId
	  * @param lotId
	  * @return FileLot
	  * @throws ServiceException
	  */
	public FileLot getFileLot(Integer fileLotId, Integer lotId) throws ServiceException;
	
	 /**
	  * Retorna um objeto IdentifiedList contendo um objeto Paginacao com uma lista de FileLot e parametros de p�gina
	  * @param identifiedList
	  * @param lotId
	  * @return IdentifiedList
	  * @throws ServiceException
	  */
	public IdentifiedList listFileLot(IdentifiedList identifiedList, Integer lotId) throws ServiceException;
	
	 /**
	  * Return um objeto FileLotDetails
	  * @param fileLotDetailsId
	  * @param fileLotId
	  * @return FileLotDetails
	  * @throws ServiceException
	  */
	public FileLotDetails getFileLotDetails(Integer fileLotDetailsId, Integer fileLotId) throws ServiceException;
	
	 /**
	  * Retorna um objeto IdentifiedList contendo um objeto Paginacao com uma lista de FileLotDetails e parametros de pagina
	  * @param identifiedList
	  * @param fileLotId
	  * @return IdentifiedList
	  * @throws ServiceException
	  */
	public IdentifiedList listFileLotDetails(IdentifiedList identifiedList, Integer fileLotId) throws ServiceException;
	
	 /**
	  * Retorna uma lista de objetos do tipo InsuredObject que referencia a tabela Object
	  * @param refDate
	  * @return List de InsuredObject
	  * @throws ServiceException
	  */	
	public List<InsuredObject> listObject(Date refDate) throws ServiceException;
	
	/**
	  * Retorna um objeto do tipo InsuredObject que referencia a tabela Object
	  * @param objectID
	  * @param refDate
	  * @return InsuredObject
	  * @throws ServiceException
	  */
	public InsuredObject getObject(int objectID, Date refDate) throws ServiceException;
	
	/**
	 * Retorna uma lista de produtos 
	 * @param objectID
	 * @param refDate
	 * @return Lista de produtos
	 * @throws ServiceException
	 */
	public List<Product> listProduct(int objectID, Date refDate) throws ServiceException;
	
	/**
	 * Retorna uma lista de ProductVersion
	 * @param productID
	 * @param refDate
	 * @return Lista de ProductVersion
	 * @throws ServiceException
	 */
	public List<ProductVersion> listProductVersion(int productID, Date refDate) throws ServiceException;
	
	/**
	 * Retorna uma lista de Branch
	 * @param refDate
	 * @return Lista de Branch
	 * @throws ServiceException
	 */
	public List<Branch> listBranch(Date refDate) throws ServiceException;
	
	/**
	 * Retorna um branch
	 * @param branchID
	 * @param refDate
	 * @return Retorna um Branch
	 * @throws ServiceException
	 */
	public Branch getBranch(int branchID, Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por objetivo listar moedas e/ou indexadores por seguradora
	 * @param indexer
	 * @param refDate
	 * @return Lista de Currency
	 * @throws ServiceException
	 */
	public List<Currency> listCurrency(boolean indexer, Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por objetivo trazer informacoes da moeda e/ou indexador utilizados pela seguradora
	 * @param currencyID
	 * @param refDate
	 * @return Lista de Currency
	 * @throws ServiceException
	 */
	public Currency getCurrency(int currencyID, Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por objetivo trazer a taxa de conversao da moeda e/ou indexadores por seguradora
	 * @param currencyID
	 * @param refDate
	 * @return Um objeto Currency
	 * @throws ServiceException
	 */
	public ConversionRate findConversionRateByRefDate(int currencyID, Date refDate) throws ServiceException;
	
	/**
	 * Este m�todo tem por objetivo listar os planos de riscos disponiveis para os produtos
	 * @param productID
	 * @param versionDate
	 * @return Lista de ProductPlan
	 * @throws ServiceException
	 */
	public List<ProductPlan> listProductPlan(int productID, Date versionDate) throws ServiceException;
	
	/**
	 * Este m�todo tem por objetivo listar os planos de cobertura disponveis para os produtos
	 * @param productID
	 * @param versionDate
	 * @return Lista de CoveragePlan
	 * @throws ServiceException
	 */
	public List<CoveragePlan> listCoveragePlan(int productID, Date versionDate) throws ServiceException;
	
	/**
	 * Este m�todo tem por objetivo trazer as informacoes do plano de cobertura disponivel para os produtos
	 * @param productID
	 * @param planID
	 * @param versionDate
	 * @return Um objeto ProductPlan
	 * @throws ServiceException
	 */
	public ProductPlan getProductPlanById(int productID, int planID, Date versionDate) throws ServiceException;
	
	/**
	 * Este m�todo tem por objetivo trazer as informa�oes da cobertura disponivel para os produtos.
	 * @param productID
	 * @param planID
	 * @param coverageID
	 * @param versionDate
	 * @return Um objeto CoveragePlan
	 * @throws ServiceException
	 */
	public CoveragePlan getCoveragePlanByRefDate(int productID, int planID, int coverageID, Date versionDate) throws ServiceException;
	
	/**
	 * Este m�todo tem por objetivo trazer as informa�oes de faixa de valor da cobertura disponivel para os produtos.
	 * @param productID
	 * @param planID
	 * @param coverageID
	 * @param rangeValueID
	 * @param versionDate
	 * @return Um objecto CoveragePlan
	 * @throws ServiceException
	 */
	public CoverageRangeValue getCoverageRangeValue(int productID, int planID, int coverageID, int rangeValueID, Date versionDate) throws ServiceException;
	
	/**
	 * Este m�todo tem por objetivo listar os plazos de segurabilidade disponiveis para os produtos
	 * @param productID
	 * @param versionDate
	 * @return Lista de ProductTerm
	 * @throws ServiceException
	 */
	public List <ProductTerm> listProductTermByRefDate(int productID, Date versionDate) throws ServiceException;
	
	/**
	 * Este m�todo tem por objetivo trazer as informa�oes do prazo de segurabilidade disponivel para os produtos
	 * @author jones.silva
	 * @param productID
	 * @param termID
	 * @param versionDate
	 * @return {@link ProductTerm}
	 * @throws ServiceException
	 */
	public ProductTerm getProductTermById(int productID, int termID, Date versionDate) throws ServiceException;
	
	/**
	 * Este m�todo tem por objetivo listar os meios de pagamentos disponiveis para os produtos.
	 * @author jones.silva
	 * @param productID
	 * @param versionDate
	 * @return {@link List} of {@link ProductPaymentTerm}
	 * @throws ServiceException
	 */
	public List<ProductPaymentTerm> listProductPaymentTermByRefDate(int productID, Date versionDate) throws ServiceException;
	
	/**
	 * Este m�todo tem por objetivo trazer as informa�oes do meio de pagamento disponivel para os produtos.
	 * @author jones.silva
	 * @param productID
	 * @param billingMethodID
	 * @param versionDate
	 * @return {@link ProductPaymentTerm}
	 * @throws ServiceException
	 */
	public ProductPaymentTerm getProductPaymentTerm(int productID, int billingMethodID, Date versionDate) throws ServiceException;
	
	/**
	 * Este m�todo tem por objetivo trazer as informa�oes do meio de pagamento disponivel para os produtos.
	 * @author jones.silva
	 * @param productID
	 * @param paymentTermID
	 * @param billingMethodID
	 * @param versionDate
	 * @return {@link ProductPaymentTerm}
	 * @throws ServiceException
	 */
	public ProductPaymentTerm getProductPaymentTerm(int productID, int paymentTermID, int billingMethodID, Date versionDate) throws ServiceException;
	
	/**
	 * Este m�todo tem por objetivo trazer as informacoes do agente cobrador, utilizados pela companhia seguradora
	 * @author jones.silva
	 * @param billingAgencyID
	 * @param refDate
	 * @return {@link BillingAgency}
	 * @throws ServiceException
	 */
	public BillingAgency getBillingAgencyById(int billingAgencyID, Date refDate) throws ServiceException;

	/**
	 * Este metodo tem por finalidade de listar todos os DataOption por DataGroupID e data de referencia.
	 * @author jones.silva
	 * @param dataGroupID
	 * @param refDate
	 * @return {@link List} of {@link DataOption}
	 * @throws ServiceException
	 */
	public List<DataOption> listDataOptionByRefDate(int dataGroupID,Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por finalidade de listar DataOption por fieldValue e data de referencia
	 * @author jones.silva
	 * @param fieldValue
	 * @param refDate
	 * @return {@link List} of {@link DataOption}
	 * @throws ServiceException
	 */
	public List<DataOption> listDataOptionByFieldValue(String fieldValue, Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por finalidade de listar DataOption por fieldValue e data de referencia
	 * @param fieldValue
	 * @param parentID
	 * @param refDate
	 * @return {@link List} of {@link DataOption}
	 * @throws ServiceException
	 */
	public List<DataOption> listDataOptionByFieldValue(String fieldValue, int parentID, Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por finalidade de listar DataOption por parentID e data de referencia
	 * @author jones.silva
	 * @param parentID
	 * @param refDate
	 * @return {@link List} of {@link DataOption}
	 * @throws ServiceException
	 */
	public List<DataOption> listDataOptionByParentID(int parentID, Date refDate) throws ServiceException;
	
	/**
	 * Este m�todo tem por objetivo listar o dominio de profissoes, utilizados pela companhia seguradora
	 * @author jones.silva
	 * @param refDate
	 * @return {@link List} of {@link Profession}
	 * @throws ServiceException
	 */
	public List<Profession> listProfessionByRefDate(Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por objetivo listar o d�minio de ocupa�oes, utilizados pela companhia seguradora
	 * @author jones.silva
	 * @param refDate
	 * @return {@link List} of {@link Occupation}
	 * @throws ServiceException
	 */
	public List<Occupation> listOccupationByRefDate(Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por objetivo listar o d�minio de atividades economicas, utilizados pela companhia seguradora
	 * @author jones.silva
	 * @param refDate
	 * @return {@link List} of {@link Activity}
	 * @throws ServiceException
	 */
	public List<Activity> listActivityByRefDate(Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por objetivo listar o d�minio de capacidades economicas, utilizados pela companhia seguradora
	 * @author jones.silva
	 * @param refDate
	 * @return {@link List} of {@link Inflow}
	 * @throws ServiceException
	 */
	public List<Inflow> listInflowByRefDate(Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por objetivo listar o d�minio de paises, utilizados pela companhia seguradora
	 * @author jones.silva
	 * @param refDate
	 * @return {@link List} of {@link Country}
	 * @throws ServiceException
	 */
	public List<Country> listCountryByRefDate(Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por objetivo listar o d�minio de estados, utilizados pela companhia seguradora
	 * @author jones.silva
	 * @param countryID
	 * @param refDate
	 * @return {@link List} of {@link State}
	 * @throws ServiceException
	 */
	public List<State> listStateByRefDate(int countryID, Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por objetivo listar o d�minio de cidades, utilizados pela companhia seguradora
	 * @author jones.silva
	 * @param countryID
	 * @param stateID
	 * @param refDate
	 * @return {@link List} of {@link City}
	 * @throws ServiceException
	 */
	public List<City> listCityByRefDate(int countryID, int stateID, Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por finalidade trazer um objeto selecionado por ID
	 * @author jones.silva
	 * @param objectID
	 * @return {@link InsuredObject}
	 * @throws ServiceException
	 */
	public InsuredObject getObjectByID(int objectID) throws ServiceException;
	
	/**
	 * Este metodo tem por objetivo listar as seguradoras, utilizadas pela companhia seguradora
	 * @author jones.silva
	 * @param refDate
	 * @return {@link List} of {@link Insurer}
	 * @throws ServiceException
	 */
	public List<Insurer> listInsurerByRefDate(Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por objetivo listar as sucursais, utilizadas pela companhia seguradora.
	 * @author jones.silva
	 * @param insurerID 
	 * @param refDate
	 * @return {@link List} of {@link Subsidiary}
	 * @throws ServiceException
	 */
	public List<Subsidiary> listSubsidiary(int insurerID, Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por objetivo listar os corretores, utilizadas pela companhia seguradora.
	 * @author jones.silva
	 * @param insurerID
	 * @param regulatoryCode
	 * @param refDate
	 * @return {@link List} of {@link Broker}
	 * @throws ServiceException
	 */
	public List<Broker> listBroker(int insurerID, String regulatoryCode, Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por objetivo listar os aliados, utilizadas pela companhia seguradora.
	 * @author jones.silva
	 * @param insurerID
	 * @param refDate
	 * @return {@link List} of {@link Partner}
	 * @throws ServiceException
	 */
	public List<Partner> listPartner(int insurerID, Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por objetivo listar os canais de vendas, utilizadas pela companhia seguradora.
	 * @author jones.silva
	 * @param insurerID
	 * @param partnerID
	 * @param refDate
	 * @return {@link List} of {@link Channel}
	 * @throws ServiceException
	 */
	public List<Channel> listChannel(int insurerID, int partnerID, Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por objetivo listar os usuarios, utilizadas pela companhia seguradora.
	 * @author jones.silva
	 * @param insurerID
	 * @param partnerID
	 * @param channelID
	 * @param refDate
	 * @return {@link List} of {@link User}
	 * @throws ServiceException
	 */
	public List<User> listUser(int insurerID, int partnerID, int channelID, Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por objetivo listar as apolices maes, utilizadas pela companhia seguradora.
	 * @author jones.silva
	 * @param insurerID
	 * @param partnerID
	 * @param refDate
	 * @return {@link List} of {@link MasterPolicy}
	 * @throws ServiceException
	 */
	public List<MasterPolicy> listMasterPolicy(int insurerID, int partnerID, Date refDate) throws ServiceException;
	
	/**
	 * Este metodo tem por objetivo listar os meios de pagamentos disponiveis pelos produtos.
	 * @author jones.silva
	 * @param productID
	 * @param versionDate
	 * @return {@link List} of {@link BillingMethod}
	 * @throws ServiceException
	 */
	public List<BillingMethod> listBillingMethod(int productID, Date versionDate) throws ServiceException;
	
	/**
	 * Este metodo retornara o Endorsement cancelado por erro
	 * @author jones.silva
	 * @param contractId
	 * @param issuanceType
	 * @param cancelDate
	 * @param cancelReason
	 * @return {@link Endorsement}
	 * @throws ServiceException
	 */
	public Endorsement policyCancellationByError(int contractId, Date cancelDate, int cancelReason) throws ServiceException;

	/**
	 * Este metodo retornara o Endorsement cancelado pelo segurado
	 * @author jones.silva
	 * @param contractId
	 * @param issuanceType
	 * @param cancelDate
	 * @param cancelReason
	 * @return {@link Endorsement}
	 * @throws ServiceException
	 */
	public Endorsement policyCancellationByInsured(int contractId, Date cancelDate, int cancelReason) throws ServiceException;
	
	/**
	 * @return Funcionalidades que lista as funcionalidades
	 * @return funcionalidades do user logado @
	 * @throws ServiceException
	 **/
	public List<Functionality> listAllowedFunctionalityByUserLogged() throws ServiceException;
	
	public List<Report> listReportIdByType(Integer contractId, Integer endorsementId) throws ServiceException;
	
	public Integer getReportIdByType(Integer contractId, Integer endorsementId, Integer reportType) throws ServiceException;
	
	public void createAndFreePendency(int contractId, int endorsementId, Date date, int pendencyId, int lockPendencyStatus);
	
	/**
	 * Encontra dados relativos ao CEP
	 * @param cep 
	 * @return Dados relativos ao CEP
	 */
	public IdentifiedList identifiedListFindCep(IdentifiedList identifiedList, String cep);
	
	public List<PaymentOption> getPaymentOptionsByPaymentType(double totalPremium, double netPremium, double policyCost, double fractioningAdditional, double taxValue, double taxRate,	int productId, int paymentType, Date refDate, Date effectiveDate, Date expireDate, Short dueDay, Integer renewalType, Integer renewalInsurerId, Integer issuanceType, Integer paymentTermType, Integer invoiceType, Integer firstPaymentType, Integer qtInstallment, Date proposalDate) throws ServiceException;
	

	/**
	 * Valida as contas banc�rias
	 * @param info Informa��es banc�rias
	 * @throws BankValidationException
	 */
	public void validateBankInfo(IAccountInfo info) throws BankValidationException;
	
}