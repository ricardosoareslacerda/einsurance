package br.com.tratomais.core.service;

import java.util.List;

import br.com.tratomais.core.model.Domain;

public interface IServiceDomain extends IFindEntity<Domain, Integer>, ICrudEntity<Domain, Integer> {

	public Domain findDomainByName(String name);
	
	public List<Domain> listAllActive();
	
}
