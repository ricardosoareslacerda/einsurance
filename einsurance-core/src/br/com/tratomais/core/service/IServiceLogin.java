package br.com.tratomais.core.service;

import br.com.tratomais.core.model.Login;
import br.com.tratomais.core.model.User;

public interface IServiceLogin extends IFindEntity<Login, Integer>, ICrudEntity<Login, Integer> {

	public User validateLogin(String userName, String passwordKey);

	/**
	 * Persist User object
	 * @param user Instance to be persisted
	 * @return Login instance created
	 */
	public Login saveObject(User user);

	/**
	 * Search for existence of determined login
	 * @param login login
	 * @param domain domain
	 * @return exists or not
	 */
	public boolean loginExistence(String login, String domain);

	/**
	 * Search for existence of determined login
	 * @param login Complete login (username@domain)
	 * @return exists or not
	 */
	public boolean loginExistence(String login);

	/**
	 * Searchs for login based on user name
	 * @param login user name
	 * @param domainName domain
	 * @return instance of Login based on information
	 */
	public Login findByLogin(String login, String domainName);

	/**
	 * Seachs for login based on user object 
	 * @param user User
	 * @return Login
	 */
	public Login findByUser(User user);

	/**
	 * Eliminates a login persisted object
	 * @param loginName login name (user name)
	 * @param domainName domain name
	 */
	public void deleteObjectByLoginName(String loginName, String domainName);

	public void logout();

	/**
	 * Log event for audit
	 */
	public void logAuditAction(String message);

	/**
	 * Changes password for user
	 * @param login User login
	 * @param oldPassword Old password
	 * @param newPassword new password
	 * @return if password changed
	 */
	public boolean changeUserPassword(String login, String domain, String oldPassword, String newPassword);
}
