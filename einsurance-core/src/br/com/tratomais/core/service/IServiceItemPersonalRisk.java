package br.com.tratomais.core.service;

import java.util.List;

import br.com.tratomais.core.model.policy.ItemPersonalRisk;

public interface IServiceItemPersonalRisk extends IFindEntity<ItemPersonalRisk, Integer>{
	
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId, int endorsementId, int itemId);
	
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId, int endorsementId);
	
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId);

}
