package br.com.tratomais.core.service;

import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.policy.ItemCoverage;
import br.com.tratomais.core.model.policy.ItemProfile;
import br.com.tratomais.core.model.product.PaymentOption;

public interface IServiceRule {
	
	public void checkRuleClauseAutomatic(Item item);
	
	public boolean checkRuleCoverage(ItemCoverage itemCoverage);
	
	public boolean checkRuleInsuredValue(ItemCoverage itemCoverage);
	
	public boolean checkRuleDeducible(ItemCoverage itemCoverage);
	
	public boolean checkRuleProfile(ItemProfile itemProfile);
	
	public boolean checkRulePaymentTerm(Endorsement endorsement, PaymentOption paymentOption);
	
	public boolean checkRulePaymentType(Endorsement endorsement, int paymentType);
	
	public boolean checkRulesPendency(Item item, int pendencyId);
}
