/**
 * 
 */
package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.ProductPaymentTerm;
import br.com.tratomais.core.model.product.ProductTerm;
import br.com.tratomais.core.model.product.ProductVersion;
import br.com.tratomais.core.model.report.Report;

/**
 * @author eduardo.venancio
 */
public interface IServiceProductVersion extends IFindEntity<ProductVersion, Integer>, ICrudEntity<ProductVersion, Integer> {
	
	public List<ProductVersion> getProductVersionByProduct(int productId);

	public ProductVersion saveProductVersionCopy(ProductVersion inputProductVersion);

	public ProductVersion getProductVersionByRefDate(int productId, Date refDate);
	
	public ProductVersion getProductVersionByBetweenRefDate(int productId, Date RefDate);

	public List<ProductVersion> listProductVersion(int productID, Date refDate);

	public List<ProductTerm> listProductTermByRefDate(int productID, Date versionDate);

	public List<ProductPaymentTerm> listProductPaymentTermByRefDate(int productID, Date versionDate);

	public ProductPaymentTerm getProductPaymentTerm(int productID, int billingMethodID, Date versionDate);

	public ProductPaymentTerm getProductPaymentTerm(int productID, int paymentTermID, int billingMethodID, Date versionDate);
	
	public List<Report> listReportIdByType(Integer contractId, Integer endorsementId);
	
	public Integer getReportIdByType(Integer contractId, Integer endorsementId, Integer reportType);
}
