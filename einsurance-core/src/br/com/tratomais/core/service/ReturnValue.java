package br.com.tratomais.core.service;

import java.io.Serializable;

import br.com.tratomais.core.service.erros.ErrorList;

/**
 * 
 * @author luiz.alberoni
 *
 */
public class ReturnValue implements Serializable {
	
	public ReturnValue() {
		this.success = true;
	}
	
	public ReturnValue(boolean success) {
		super();
		this.success = success;
	}

	public ReturnValue(boolean success, String messages) {
		super();
		this.success = success;
		this.messages = messages;
	}
	
	public ReturnValue(boolean success, Object returnValue, String messages) {
		super();
		this.success = success;
		this.returnValue = returnValue;
		this.messages = messages;
	}
	/* Don't forget to increment this value each time this class changes too */
	private static final long serialVersionUID = 1L;
	/**
	 * Operation success status
	 */
	private boolean success;
	/**
	 * Other Valu
	 */
	private Object returnValue;
	/**
	 * Messages
	 */
	private String messages;
	/**
	 * 
	 */
	private ErrorList listaErros = new ErrorList();
	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the returnValue
	 */
	public Object getReturnValue() {
		return returnValue;
	}

	/**
	 * @param returnValue the returnValue to set
	 */
	public void setReturnValue(Object returnValue) {
		this.returnValue = returnValue;
	}

	/**
	 * @return the messages
	 */
	public String getMessages() {
		return messages;
	}

	/**
	 * @param messages the messages to set
	 */
	public void setMessages(String messages) {
		this.messages = messages;
	}

	/**
	 * @return the listaErros
	 */
	public ErrorList getListaErros() {
		return listaErros;
	}

	/**
	 * @param listaErros the listaErros to set
	 */
	public void setListaErros(ErrorList listaErros) {
		this.listaErros = listaErros;
	}
}
