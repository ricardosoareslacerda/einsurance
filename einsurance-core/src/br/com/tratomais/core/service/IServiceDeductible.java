/**
 * 
 */
package br.com.tratomais.core.service;

import java.util.List;

import br.com.tratomais.core.model.product.Deductible;

/**
 * @author daniel.matuki
 */
public interface IServiceDeductible {

	/**
	 * @return the list of all deductible
	 */
	public List<Deductible> findAllDeductible();
}
