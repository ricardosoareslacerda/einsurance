package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.customer.Insurer;

public interface IServiceInsurer extends IFindEntity< Insurer, Integer >{

	public List<Insurer> listInsurerByRefDate(Date refDate);
	
}
