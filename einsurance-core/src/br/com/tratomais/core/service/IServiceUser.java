package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import br.com.tratomais.core.model.Login;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.util.TransportationClass;

public interface IServiceUser extends IFindEntity<User, Integer>, ICrudEntity<User, Integer> {

	public User loadUserByLogin(String userName);

	public User saveObjectInsert(User user, Login login);

	public User saveObject(User user, Login login);

	public void changePartnerDefaultLoggedUser(int partnerId);

	public List<User> listUserAllowed();

	public List<User> listUserByPartnerId(int partnerId);

	public Set<User> fillListUserByChannel(Channel channel);

	public List<User> listUserPerPartner(int partnerId);
	
	/**
	 * Query to list the users (used in gridview)
	 * 
	 * @param limit: Number of rows to return.  If this is not set all rows will be returned.
	 * @param page: The page number to return (if paginating).
	 * @param columnSorted: Field to sort the output by.
	 * @param typeSorted: Sort order.  Either 'asc' (ascending) or 'desc' (descending).
	 * @param search: True or False (whether or not this is a search). Default is False. 
	 * Also takes strings of 'true' and 'false'.
	 * @param searchField: The field we're searching on.
	 * @param searchString: The string to match for our search.
	 * @return List<User>
	 */	
	public TransportationClass<User> listUserToGrid(int limit, int page, String columnSorted, String typeSorted,
													String search, String searchField, String searchString);
	
	/**
	 * Query to list the users and partners active (used in gridview)
	 * 
	 * @param limit: Number of rows to return.  If this is not set all rows will be returned.
	 * @param page: The page number to return (if paginating).
	 * @param userId: Id User
	 * @return {@link TransportationClass <{@link User}>}
	 */
	public TransportationClass<User> listUserAllActivePartnerToGrid(int limit, int page, int userId);

	public List<User> listUser(int insurerID, int partnerID, int channelID, Date refDate);	
}
