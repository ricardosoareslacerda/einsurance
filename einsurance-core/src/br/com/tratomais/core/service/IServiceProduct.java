package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.ItemCoverage;
import br.com.tratomais.core.model.product.CoverageOption;
import br.com.tratomais.core.model.product.CoveragePlan;
import br.com.tratomais.core.model.product.CoverageRangeValue;
import br.com.tratomais.core.model.product.PlanKinship;
import br.com.tratomais.core.model.product.PlanRiskType;
import br.com.tratomais.core.model.product.Product;
import br.com.tratomais.core.model.product.ProductOption;
import br.com.tratomais.core.model.product.ProductPlan;
import br.com.tratomais.core.model.product.ProductTerm;
import br.com.tratomais.core.model.product.Profile;

public interface IServiceProduct {

	public List<ProductOption> findProductsbyPartner(int partnerId, int insurerId, Date refDate);

	public List<CoveragePlan> listCoveragePlanByRefDate(int productId, int planId, Date refDate);

	public List<ProductPlan> findPlanByProductId(int productId, Date refDate);

	public List<ProductPlan> findRiskPlanByProductId(int productId, Date refDate);

	public List<ProductPlan> listPersonalRiskPlanByProductId(int productId, Date refDate);

	public List< Product > listAllProduct();
	
	public Product findProductToBeCopy(int productId, Date referenceDate);
	
	public List<Product> findProductsPerPartner(int partnerId);

	public ProductPlan getPlanById(int productId, int planId, Date refDate);

	public ProductPlan getRiskPlanById(int productId, int planId, Date refDate);

	public List<PlanKinship> listPlanKinshipByRiskPlan(int productId, int riskPlanId, int renewalType, Date refDate);
		
	public PlanKinship listPlanKinshipByPersonalRiskType(int productId, int riskPlanId, int personalRiskType, int renewalType, Date refDate);

	public List<ProductPlan> listCoveragePlanByProductId(int productId, int riskPlanId, Date refDate);
	
	public List <CoverageRangeValue> listCoverageRangeValueByRefDate(int productId, int coveragePlanId, int coverageId ,Date refDate);	
	
	public List<PlanRiskType> listPlanRiskTypeDescriptionByRefDate(int productId, int planId, Date refDate);

	public List<Product> listProductPerPartnerOrBroker(Integer partnerId, Integer productId, Integer brokerId);
	
	public List<CoverageOption> listCoverageHabitatByEndorsement(Endorsement endorsement, Set< ItemCoverage > itemCoverageList);

	public List<CoverageOption> listCoverageOptionByRefDate(int productId, int coveragePlanId, Date refDate);

	public List<CoverageOption> listCoverageOptionByRefDate(int productId, int coveragePlanId, Date refDate, Set< ItemCoverage > itemCoverageList );
	
	public ProductOption getProduct(int productId, Date referenceDate);	
	
	public Product getProductById(int productId);
	
	public List<Profile> findProfileList(int productId, Date referenceDate);

	public List<Product> listProduct(int objectID, Date refDate);

	public List<ProductPlan> listProductPlan(int productID, Date versionDate);

	public ProductPlan getProductPlanById(int productID, int planID, Date versionDate);

	public CoverageRangeValue getCoverageRangeValue(int productID, int planID, int coverageID, int rangeValueID, Date versionDate);

	public ProductTerm getProductTermById(int productID, int termID, Date versionDate);
}
