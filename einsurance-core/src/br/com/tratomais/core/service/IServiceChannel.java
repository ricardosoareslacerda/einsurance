package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Partner;
import br.com.tratomais.core.util.TransportationClass;

public interface IServiceChannel extends IFindEntity<Channel, Integer>, ICrudEntity<Channel, Integer> {

	public List<Channel> findByNickName(String nickName);

	public List<Channel> findByPartnerId(int partnerId, boolean onlyActive);

	public List<Channel> findByNameAndPartner(String name, Partner partner);

	public List<Channel> listAllActive();

	/**
	 * 
	 * @param user
	 * @param partner
	 * @return
	 */
	public List<Channel> findByUserAndPartner(User user, int partnerId);

	public TransportationClass<Channel> listChannelToGrid(int limit, int page, String columnSorted, String typeSorted, 
			String search, String searchField, String searchString, int partnerID);

	public List<Channel> listChannel(int insurerID, int partnerID, Date refDate);
}
