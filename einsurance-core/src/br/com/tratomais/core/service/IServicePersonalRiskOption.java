package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.PersonalRiskOption;

public interface IServicePersonalRiskOption {
	
	public List<PersonalRiskOption> findPersonalRiskOptions(int productId,
															int riskPlanId, 
															Date referenceDate);

	public List<PersonalRiskOption> findPersonalRiskOptions();

}
