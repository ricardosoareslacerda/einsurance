package br.com.tratomais.core.service;

import br.com.tratomais.core.dao.PersistentEntity;

public interface ICrudEntity<T extends PersistentEntity, ID> {

	public void deleteObject(T entity);

	public T saveObject(T entity);

}