/**
 * 
 */
package br.com.tratomais.core.service;

import java.util.Collection;

import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.UserPartner;

/**
 * @author leo.costa
 */
public interface IServiceUserPartner extends IFindEntity<UserPartner, Integer> {

	public Collection<UserPartner> findByUser(User user);
}
