/**
 * 
 */
package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.CoveragePlan;

/**
 * @author eduardo.venancio
 */
public interface IServiceCoveragePlan extends IFindEntity<CoveragePlan, Integer>, ICrudEntity<CoveragePlan, Integer> {

	public List<CoveragePlan> listCoveragePlan(int productID, Date versionDate);

	public CoveragePlan getCoveragePlanByRefDate(int productID, int planID,	int coverageID, Date versionDate);

}
