package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.auto.AutoBrand;
import br.com.tratomais.core.model.auto.AutoModel;
import br.com.tratomais.core.model.auto.AutoVersion;

public interface IServiceAuto {
	public Double getAutoCost(int autoModelId, Integer year, Date referenceDate);
	public List<AutoBrand> listAutoBrands();
	public List<AutoModel> listAutoModelForBrand(int brandId);
	public List<Integer> listAutoYear(int autoModelId,Date referenceDate);
	public AutoVersion getAutoVersion(int autoModelId, Date effectiveDate);
	public AutoVersion findAutoVersion(int autoModelId, Date referenceDate);
}
