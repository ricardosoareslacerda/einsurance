package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.tratomais.core.dao.IDaoCity;
import br.com.tratomais.core.model.City;
import br.com.tratomais.core.model.product.IdentifiedList;
import br.com.tratomais.core.service.IServiceCity;

public class ServiceCityImpl extends ServiceBean implements IServiceCity{
	private static final Logger logger = Logger.getLogger( ServiceCityImpl.class );
	
	private IDaoCity daoCity;
	
	public void setDaoCity(IDaoCity daoCity) {
		this.daoCity = daoCity;
	}
	
	public City findById(Integer id) {
		return daoCity.findById( id );
	}

	public List< City > findByName(String name) {
		return daoCity.findByName( name );
	}

	public List< City > listAll() {
		return daoCity.listAll();
	}

	public IdentifiedList listIdentifiedListCity(IdentifiedList identifiedList){
		return daoCity.listIdentifiedListCity(identifiedList);
	}
	
	public IdentifiedList listCityByState(IdentifiedList identifiedList, int stateId){
		return daoCity.listCityByState(identifiedList, stateId);
	}
	
	public List<City> listCityByStateId(int stateId){
		return daoCity.listCity(stateId);
	}

	@Override
	public List<City> listCityByRefDate(int countryID, int stateID, Date refDate) {
		return daoCity.listCityByRefDate(countryID, stateID, refDate);
	}	
	
}
