package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.dao.IDaoProductVersion;
import br.com.tratomais.core.dao.IDaoReportJasper;
import br.com.tratomais.core.model.product.ProductPaymentTerm;
import br.com.tratomais.core.model.product.ProductTerm;
import br.com.tratomais.core.model.product.ProductVersion;
import br.com.tratomais.core.model.report.Report;
import br.com.tratomais.core.service.IServiceProductVersion;

public class ServiceProductVersionImpl extends ServiceBean implements IServiceProductVersion{

	private IDaoProductVersion daoProductVersion;
	
	private IDaoReportJasper daoReportJasper;

	public void setDaoProductVersion(IDaoProductVersion daoProductVersion) {
		this.daoProductVersion = daoProductVersion;
	}
	
	public void setDaoReportJasper(IDaoReportJasper daoReportJasper) {
		this.daoReportJasper = daoReportJasper;
	}
	
	@Override
	public ProductVersion findById(Integer id) {
		return daoProductVersion.findById(id);
	}

	@Override
	public List<ProductVersion> findByName(String name) {
		return daoProductVersion.findByName(name);
	}

	@Override
	public List<ProductVersion> listAll() {
		return daoProductVersion.listAll();
	}

	@Override
	public void deleteObject(ProductVersion entity) {
		daoProductVersion.delete(entity);
	}

	@Override
	public ProductVersion saveObject(ProductVersion entity) {
		return daoProductVersion.save(entity);
	}
	
	public List<ProductVersion> getProductVersionByProduct(int productId){
		return daoProductVersion.getProductVersionByProduct(productId);
	}

	@Override
	public ProductVersion saveProductVersionCopy(ProductVersion inputProductVersion) {
		return daoProductVersion.saveProductVersionCopy( inputProductVersion );
	}

	@Override
	public ProductVersion getProductVersionByRefDate(int productId, Date refDate) {
		return daoProductVersion.getProductVersionByRefDate(productId, refDate);
	}

	@Override
	public ProductVersion getProductVersionByBetweenRefDate(int productId, Date RefDate) {
		return daoProductVersion.getProductVersionByBetweenRefDate(productId, RefDate);
	}

	@Override
	public List<ProductVersion> listProductVersion(int productID, Date refDate) {
		return daoProductVersion.listProductVersion(productID, refDate);
	}

	@Override
	public List<ProductTerm> listProductTermByRefDate(int productID, Date versionDate) {
		return daoProductVersion.listProductTermByRefDate(productID, versionDate);
	}

	@Override
	public List<ProductPaymentTerm> listProductPaymentTermByRefDate(int productID, Date versionDate) {
		return daoProductVersion.listProductPaymentTermByRefDate(productID, versionDate);
	}

	@Override
	public ProductPaymentTerm getProductPaymentTerm(int productID, int billingMethodID, Date versionDate) {
		return daoProductVersion.getProductPaymentTerm(productID, billingMethodID, versionDate);
	}

	@Override
	public ProductPaymentTerm getProductPaymentTerm(int productID, int paymentTermID, int billingMethodID, Date versionDate) {
		return daoProductVersion.getProductPaymentTerm(productID, paymentTermID, billingMethodID, versionDate);
	}
	
	@Override
	public List<Report> listReportIdByType(Integer contractId, Integer endorsementId) {
		return daoReportJasper.listReportIdByType(contractId, endorsementId);
	}
	
	@Override
	public Integer getReportIdByType(Integer contractId, Integer endorsementId, Integer reportType) {
		return daoReportJasper.getReportIdByType(contractId, endorsementId, reportType);
	}
}
