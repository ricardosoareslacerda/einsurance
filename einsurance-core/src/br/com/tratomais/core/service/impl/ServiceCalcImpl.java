package br.com.tratomais.core.service.impl;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.jms.JMSException;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.dao.IDaoProduct;
import br.com.tratomais.core.dao.impl.DaoCalculo;
import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.policy.ItemCoverage;
import br.com.tratomais.core.model.policy.ItemPersonalRisk;
import br.com.tratomais.core.model.product.CoverageOption;
import br.com.tratomais.core.model.product.CoveragePersonalRisk;
import br.com.tratomais.core.model.product.CoveragePlan;
import br.com.tratomais.core.model.product.PaymentOption;
import br.com.tratomais.core.model.product.SimpleCalcResult;
import br.com.tratomais.core.model.renewal.RenewalAlert;
import br.com.tratomais.core.service.ICalc;
import br.com.tratomais.core.service.IServiceCollection;
import br.com.tratomais.core.service.IServiceCalc;
import br.com.tratomais.core.service.IServicePolicy;
import br.com.tratomais.core.service.IServiceProduct;
import br.com.tratomais.core.service.erros.Erro;
import br.com.tratomais.core.service.erros.ErrorTypes;
import br.com.tratomais.core.util.jms.EInsuranceJMSException;

/**
 * Service Calculation 
 */
public class ServiceCalcImpl implements IServiceCalc {
	/**
	 * Logger
	 */
	private static final Logger logger = Logger.getLogger( ServiceCalcImpl.class );

	/**
	 * Calculation interface
	 */
	private ICalc calc;
	
	/**
	 * Policy Dao 
	 */
	private IDaoPolicy daoPolicy;
	
	/**
	 * Product Service 
	 */
	private IServiceProduct serviceProduct;
	
	/**
	 * Billing Service 
	 */
	private IServiceCollection serviceCollection;
	
	/**
	 * Policy Service
	 */
	
	private IServicePolicy servicePolicy;	
	
	/**
	 * Dao de Calculo
	 */
	private DaoCalculo daoCalculo;
	
	/** DAO de Produto, injected by spring framework */
	private IDaoProduct daoProduct;	
	
	/**
	 * Setter de Dao de Calculo
	 * @param daoCalculo
	 */
	public void setDaoCalculo(DaoCalculo daoCalculo){
		this.daoCalculo = daoCalculo;
	}

	/**
	 * @param calc
	 */
	public void setCalc(ICalc calc) {
		this.calc = calc;
	}

	/**
	 * @param daoPolicy the daoPolicy to set
	 */
	public void setDaoPolicy(IDaoPolicy daoPolicy) {
		this.daoPolicy = daoPolicy;
	}
	
	/**
	 * @param serviceProduct the serviceProduct to set
	 */
	public void setServiceProduct(IServiceProduct serviceProduct) {
		this.serviceProduct = serviceProduct;
	}

	/**
	 * @param serviceCollection the serviceCollection to set
	 */
	public void setServiceCollection(IServiceCollection serviceCollection) {
		this.serviceCollection = serviceCollection;
	}	
	/**
	 * @param servicePolicy the servicePolicy to set
	 */
	public void setServicePolicy(IServicePolicy servicePolicy) {
		this.servicePolicy = servicePolicy;
	}
	
	public void setDaoProduct(IDaoProduct daoProduct) {
		this.daoProduct = daoProduct;
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceCalc#simpleCalc(br.com.tratomais.core.model.policy.Endorsement)
	 */
	public SimpleCalcResult simpleCalc(Endorsement endorsement) {
/*		SimpleCalcResult result = new SimpleCalcResult();
		try {
			if(endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL)
				endorsement = servicePolicy.policyChangeTechnical(endorsement, endorsement.getEffectiveDate(), false, null);
			else
				endorsement = calc.simpleCalc(endorsement);
			boolean validCalc = true;
			for (Item tmpItem : endorsement.getItems()) {
				if (!tmpItem.isCalculationValid()) {
					validCalc = false;
					break;
				}
			}
			if (validCalc) {
				if(endorsement.getIssuanceType() != Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL)
					endorsement = daoPolicy.saveEndorsement(endorsement);
				
				result.setPaymentOptions(calc.getPaymentOptionsDefault(endorsement));
			} 
			else {
				List<String> lista = new ArrayList<String>();
				for(Erro erro: endorsement.getErrorList().getListaErros())
					lista.add(erro.toString());
				result.setErros(lista);
					
			}
			result.setEndorsement(endorsement);
		} catch (ServiceException e) {
			e.printStackTrace();
			result.addStackTraceToErroList(e.getStackTrace());
		}*/
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceCalc#getPaymentOptionsDefault(br.com.tratomais.core.model.policy.Endorsement)
	 */
	public List<PaymentOption> getPaymentOptionsDefault(Endorsement endorsement) {
		return calc.getPaymentOptionsDefault(endorsement);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceCalc#getPaymentOptionsDefault(java.lang.Double, double, int, java.util.Date)
	 */
	public List<PaymentOption> getPaymentOptionsDefault(Double totalPremium, double taxRate, int productId, Date refDate, Date dtEffective, Date dtExpiry) {
		return calc.getPaymentOptionsDefault(totalPremium, taxRate, productId, refDate, dtEffective, dtExpiry);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceCalc#getPaymentOptionsByPaymentType(br.com.tratomais.core.model.policy.Endorsement, int)
	 */
	public List<PaymentOption> getPaymentOptionsByBillingMethodId(Endorsement endorsement, int billingMethodId, Date dtEffective, Date dtExpiry) {
		return calc.getPaymentOptionsByBillingMethodId(endorsement, billingMethodId, dtEffective, dtExpiry);
	}

	public PaymentOption getPaymentOptionByPaymentTermId(Double totalPremium, double taxRate, int productId, int billingMethodId, int paymentTermId, Date refDate, Date dtEffective, Date dtExpiry) { 
		return calc.getPaymentOptionByPaymentTermId(totalPremium, taxRate, productId, billingMethodId, paymentTermId, refDate, dtEffective, dtExpiry);
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceCalc#getPaymentOptionsByPaymentType(java.lang.Double, double, int, int, java.util.Date)
	 */
	public List<PaymentOption> getPaymentOptionsByPaymentType(Double totalPremium, double taxRate, int productId, int billingMethodId, Date refDate, Date dtEffective, Date dtExpiry) {
		return calc.getPaymentOptionsByBillingMethodId(totalPremium, taxRate, productId, billingMethodId, refDate, dtEffective, dtExpiry);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceCalc#effectiveProposal(br.com.tratomais.core.model.policy.Endorsement, br.com.tratomais.core.model.product.PaymentOption)
	 */
	public Endorsement effectiveProposal(Endorsement endorsement, PaymentOption paymentOption) {
		Endorsement newEndorsement = endorsement;
		try {
			newEndorsement = calc.effectiveProposal(endorsement, paymentOption);
			
			if(newEndorsement!=null && newEndorsement.getIssuingStatus()==Endorsement.ISSUING_STATUS_PROPOSTA){
				daoPolicy.saveEndorsement(endorsement);
				
				// Clean errors for Renewal
				if (endorsement.getContract().isRenewed()) {
					endorsement.setRenewalAlerts(daoPolicy.listRenewalAlert(endorsement.getId(), false));
					for (RenewalAlert renewalAlert : endorsement.getRenewalAlerts() ) {
						renewalAlert.setResolved(true);
						daoPolicy.saveRenewalAlert(renewalAlert);
					}
					endorsement.getRenewalAlerts().clear();
				}
				
				serviceCollection.dispachCollection(endorsement, false);
			}
			
		} catch (ServiceException e) {
			logger.error("effectiveProposal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			newEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		
		// TODO Treating error in sending charges are currently no impact
		} catch (JMSException e) {
			logger.error("effectiveProposal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			newEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (NamingException e) {
			logger.error("effectiveProposal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			newEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (FileNotFoundException e) {
			logger.error("effectiveProposal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			newEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (IOException e) {
			logger.error("effectiveProposal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			newEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (EInsuranceJMSException e) {
			logger.error("effectiveProposal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			newEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		}

		return newEndorsement;
	}
	
	public SimpleCalcResult calculate (Endorsement endorsement, int objectId, List< CoverageOption > coverageDataGrid) {
		SimpleCalcResult result = new SimpleCalcResult();
		try {
			if(coverageDataGrid != null)
			{
				if(objectId != Item.OBJECT_CICLO_VITAL)
					if ( endorsement.getItems() == null || endorsement.getItems().size() != 1 )
						throw new ServiceException("invadid nuber of itens");
				
				Endorsement workEndorsement = new Endorsement();
				BeanUtils.copyProperties( endorsement, workEndorsement );
				Item workItem = endorsement.getMainItem();
				workItem.getItemCoverages().clear();
				for ( CoverageOption coverageOption : coverageDataGrid ) {
					
					if(coverageOption.isContractCoverage()){
						
						if(coverageOption.getCoveragePlan()!= null){
							ItemCoverage workCoverage = workItem.addCoverage( coverageOption.getCoveragePlan().getCoverage().getCoverageId() );
							
							workCoverage.setCoverageCode( coverageOption.getCoveragePlan().getCoverage().getCoverageCode() );
							workCoverage.setBasicCoverage( coverageOption.getCoveragePlan().getCoverage().isBasic() );
							workCoverage.setGoodsCode( coverageOption.getCoveragePlan().getCoverage().getGoodsCode() );
							workCoverage.setBranchId( coverageOption.getCoveragePlan().getCoverage().getBranchId() );
							workCoverage.setCoverageName( coverageOption.getCoveragePlan().getNickName() );
							workCoverage.setDisplayOrder( coverageOption.getCoveragePlan().getDisplayOrder());
							workCoverage.setInsuredValue( coverageOption.getValue());
							workCoverage.setRangeValueId( coverageOption.getRangeValueId());
							
							// Set Risk Value of object Personal Risk
							if( coverageOption.getCoveragePlan().getCoverage().isBasic() )
							{
								switch ( objectId )
								{
									case Item.OBJECT_VIDA:
									case Item.OBJECT_CICLO_VITAL:
									case Item.OBJECT_CREDITO:
									case Item.OBJECT_ACCIDENTES_PERSONALES:
									case Item.OBJECT_PURCHASE_PROTECTED:
										if (coverageOption.isModified()) {
											ItemPersonalRisk workItemPersonalRisk = (ItemPersonalRisk)workItem;
											if (coverageOption.getCoveragePlan().getInsuredValueType() == CoveragePlan.INSURED_VALUE_TYPE_PVR)
												workItemPersonalRisk.setPersonalRiskValue(coverageOption.getCoveragePlan().getFixedInsuredValue());
											else
												workItemPersonalRisk.setPersonalRiskValue(coverageOption.getValue());
										}
										break;
								}
							}
						}
					}
				}
			}
			
			adjustItemAdditional(endorsement);
			
			if(endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL)
			{
				boolean recalc = true;
				if ((endorsement.getAuthorizationType() == null || endorsement.getAuthorizationType() == 0) &&
					((endorsement.getEndorsedId() == null) || 
					 (endorsement.getEndorsedId() != null && endorsement.getEndorsedId() < endorsement.getId().getEndorsementId())))
				{
					endorsement.setEndorsedId(endorsement.getId().getEndorsementId());
					endorsement.setAuthorizationType(1);
					recalc = false;
				}
				endorsement = servicePolicy.policyChangeTechnical(endorsement, endorsement.getEffectiveDate(), recalc, false, null);
			}
			else
				endorsement = calc.simpleCalc(endorsement);
			
			boolean validCalc = true;
			
			// Verifica se veio erros do calculo
			List<String> lista = new ArrayList<String>();
			for(Erro erro: endorsement.getErrorList().getListaErros())
			{
				validCalc = false;
				lista.add(erro.toString());
			}
			result.setErros(lista);
			
			// Verifica se o calculo � invalido
			for (Item tmpItem : endorsement.getItems()) {
				if (!tmpItem.isCalculationValid()) {
					validCalc = false;
					break;
				}
			}
			if (validCalc) {
				if(endorsement.getIssuanceType() != Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL){
					endorsement = daoPolicy.saveEndorsement(endorsement);
					
					// Clean errors for Renewal
					if (endorsement.getContract().isRenewed()) {
						endorsement.setRenewalAlerts(daoPolicy.listRenewalAlert(endorsement.getId(), false));
						for (RenewalAlert renewalAlert : endorsement.getRenewalAlerts() ) {
							renewalAlert.setResolved(true);
							daoPolicy.saveRenewalAlert(renewalAlert);
						}
						endorsement.getRenewalAlerts().clear();
					}
				}
				
				for ( Item retItem : endorsement.getItems() ) {
					retItem.setCoverageOptions(serviceProduct.listCoverageOptionByRefDate( endorsement.getContract().getProductId(), retItem.getCoveragePlanId(), endorsement.getReferenceDate(), retItem.getItemCoverages() ) );
				}
				result.setPaymentOptions(calc.getPaymentOptionsDefault(endorsement));
			}
			result.setEndorsement(endorsement);
			
		} catch (ServiceException e) {
			logger.error("calculate error", e);
			e.printStackTrace();
			result.addStackTraceToErroList(e.getStackTrace());
		}
		return result;
	}

	public void adjustItemAdditional(Endorsement endorsement){
		if(endorsement.getMainItem().getObjectId() == Item.OBJECT_CICLO_VITAL)
		{
			ItemPersonalRisk mainItemPersonalRisk = (ItemPersonalRisk) endorsement.getMainItem();
	
			List<CoveragePersonalRisk> listTypeAdditional = daoProduct.listCoveragePersonalRiskByPersonalRiskType(
					mainItemPersonalRisk.getEndorsement().getContract().getProductId(),
					mainItemPersonalRisk.getCoveragePlanId(),
					ItemPersonalRisk.PERSONAL_RISK_TYPE_ADDITIONAL,
					mainItemPersonalRisk.getEndorsement().getReferenceDate());
			
			List<CoveragePersonalRisk> listTypeNewborn = daoProduct.listCoveragePersonalRiskByPersonalRiskType(
						mainItemPersonalRisk.getEndorsement().getContract().getProductId(),
						mainItemPersonalRisk.getCoveragePlanId(),
						ItemPersonalRisk.PERSONAL_RISK_TYPE_NEWBORN,
						mainItemPersonalRisk.getEndorsement().getReferenceDate());
			
			boolean isRecalculo = (endorsement.getId().getContractId() != 0);
			if(endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_EMISSION && !isRecalculo)
			{
				for(int i = 0; i < mainItemPersonalRisk.getTransientQuantityAdditional(); i++)
				{
					ItemPersonalRisk itemPersonalRisk = (ItemPersonalRisk) mainItemPersonalRisk.getEndorsement().addItem(Item.OBJECT_CICLO_VITAL);
					itemPersonalRisk.setPersonalRiskType(ItemPersonalRisk.PERSONAL_RISK_TYPE_ADDITIONAL);
				}

				for(int i = 0; i < mainItemPersonalRisk.getTransientQuantityNewborn(); i++)
				{
					ItemPersonalRisk itemPersonalRisk = (ItemPersonalRisk) mainItemPersonalRisk.getEndorsement().addItem(Item.OBJECT_CICLO_VITAL);
					itemPersonalRisk.setPersonalRiskType(ItemPersonalRisk.PERSONAL_RISK_TYPE_NEWBORN);
				}
				
				for(ItemPersonalRisk itemPersonalRisk : endorsement.listNotHolder()){
					itemPersonalRisk.setCoveragePlanId(mainItemPersonalRisk.getCoveragePlanId());
					itemPersonalRisk.setRenewalType(mainItemPersonalRisk.getRenewalType());
					itemPersonalRisk.setEffectiveDate(mainItemPersonalRisk.getEffectiveDate());
					itemPersonalRisk.setCalculationValid(mainItemPersonalRisk.isCalculationValid());
					itemPersonalRisk.setItemStatus(mainItemPersonalRisk.getItemStatus());
					itemPersonalRisk.setInsuredName("Ingrese...");
				}
			} else if(endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_EMISSION && isRecalculo) {
				//:: Clone endorsement ::
				Endorsement workEndorsement = new Endorsement();
				
				// Get the main item for endorsement
				Item mainItem = endorsement.getMainItem();
				
				// Data fill others item attributes fix
				for(ItemPersonalRisk itemPersonalRisk : endorsement.listNotHolder()){
					itemPersonalRisk.setCoveragePlanId(mainItemPersonalRisk.getCoveragePlanId());
					itemPersonalRisk.setRenewalType(mainItemPersonalRisk.getRenewalType());
					itemPersonalRisk.setEffectiveDate(mainItemPersonalRisk.getEffectiveDate());
					itemPersonalRisk.setCalculationValid(mainItemPersonalRisk.isCalculationValid());
					itemPersonalRisk.setItemStatus(mainItemPersonalRisk.getItemStatus());
					itemPersonalRisk.setInsuredName(((itemPersonalRisk.getFirstName()!=null?itemPersonalRisk.getFirstName().trim():"Ingrese...") + " " + (itemPersonalRisk.getLastName()!=null?itemPersonalRisk.getLastName().trim():"")).trim());
				}
				
				// Clear other items
				Iterator<Item> iterator = endorsement.getItems().iterator();
				while (iterator.hasNext()) {
					Item workItem = iterator.next();
					if (workItem != mainItem) {
						ItemPersonalRisk workItemPersonalRisk = (ItemPersonalRisk)workEndorsement.addItem(Item.OBJECT_CICLO_VITAL);
						String[] ignorePropertiesItem = { "id", "endorsement", "customer", "itemClauses", "itemCoverages", "itemProfiles", "beneficiaries", "itemPersonalRiskGroups" };
						BeanUtils.copyProperties(workItem, workItemPersonalRisk, ignorePropertiesItem);
						iterator.remove();
					}
				}
				
				// Copy other items
				for (ItemPersonalRisk itemPersonalRisk : workEndorsement.listNotHolder()) {
					ItemPersonalRisk workItemPersonalRisk = (ItemPersonalRisk)endorsement.addItem(Item.OBJECT_CICLO_VITAL);
					String[] ignorePropertiesItem = { "id", "endorsement", "customer", "itemClauses", "itemCoverages", "itemProfiles", "beneficiaries", "itemPersonalRiskGroups" };
					BeanUtils.copyProperties(itemPersonalRisk, workItemPersonalRisk, ignorePropertiesItem);
				}
			}
			
			for(ItemPersonalRisk itemPersonalRisk : endorsement.listNotHolder()){
				List<CoveragePersonalRisk> listTemp = itemPersonalRisk.getPersonalRiskType() == ItemPersonalRisk.PERSONAL_RISK_TYPE_ADDITIONAL? listTypeAdditional : listTypeNewborn;
				itemPersonalRisk.getItemCoverages().clear();
				
				for (ItemCoverage itemCoverage : mainItemPersonalRisk.getItemCoverages()) {
					if (isCorverageAllowed(listTemp, itemCoverage)) {
						ItemCoverage workCoverage = itemPersonalRisk.addCoverage(itemCoverage.getId().getCoverageId());
						String[] ignoreProperties = { "id", "item" };
						BeanUtils.copyProperties(itemCoverage, workCoverage, ignoreProperties);
					}
				}
			}
		}
	}
	
	/**
	 * @param listCoveragePersonalRisk
	 * @param itemCoverage
	 * @return
	 */
	private boolean isCorverageAllowed(List<CoveragePersonalRisk> listCoveragePersonalRisk, ItemCoverage itemCoverage) {
		boolean isCorverageAllowed = false;
		for (CoveragePersonalRisk coveragePersonalRisk : listCoveragePersonalRisk) {
			if (itemCoverage.getId().getCoverageId() == coveragePersonalRisk.getId().getCoverageId()) {
				isCorverageAllowed = true;
			}
		}
		return isCorverageAllowed;
	}
	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceCalc#propertyCalc(br.com.tratomais.core.model.policy.Endorsement, java.util.List)
	 */
	public SimpleCalcResult propertyCalc (Endorsement endorsement, List< CoverageOption > coverageDataGrid) {
/*		SimpleCalcResult result = new SimpleCalcResult();
		try {
			if(coverageDataGrid != null)
			{
				if ( endorsement.getItems() == null || endorsement.getItems().size() != 1 ) {
					throw new ServiceException("invadid nuber of itens");
				}
				Endorsement workEndorsement = new Endorsement();
				BeanUtils.copyProperties( endorsement, workEndorsement );
				Item workItem = endorsement.getMainItem();
				workItem.getItemCoverages().clear();
				for ( CoverageOption coverageOption : coverageDataGrid ) {
	
					if(coverageOption.isContractCoverage()){
						
						if(coverageOption.getCoveragePlan()!= null){
							ItemCoverage workCoverage = workItem.addCoverage( coverageOption.getCoveragePlan().getCoverage().getCoverageId() );
		
							workCoverage.setCoverageCode( coverageOption.getCoveragePlan().getCoverage().getCoverageCode() );
							workCoverage.setBasicCoverage( coverageOption.getCoveragePlan().getCoverage().isBasic() );
							workCoverage.setGoodsCode( coverageOption.getCoveragePlan().getCoverage().getGoodsCode() );
							workCoverage.setBranchId( coverageOption.getCoveragePlan().getCoverage().getBranchId() );
							workCoverage.setCoverageName( coverageOption.getCoveragePlan().getNickName() );
							workCoverage.setDisplayOrder( coverageOption.getCoveragePlan().getDisplayOrder());
							workCoverage.setInsuredValue( coverageOption.getValue());
							workCoverage.setRangeValueId( coverageOption.getRangeValueId());
						}
					}
				}
			}
			if(endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL &&
			   endorsement.getIssuingStatus() == Endorsement.ISSUANCE_TYPE_POLICY_EMISSION)
				//endorsement = servicePolicy.policyChangeTechnical(endorsement, endorsement.getEffectiveDate(), false, null);
			else
				endorsement = calc.simpleCalc(endorsement);
			
			boolean validCalc = true;
			for (Item tmpItem : endorsement.getItems()) {
				if (!tmpItem.isCalculationValid()) {
					validCalc = false;
					break;
				}
			}
			if (validCalc) {
				if(endorsement.getIssuanceType() != Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL &&
				   endorsement.getIssuingStatus() != Endorsement.ISSUANCE_TYPE_POLICY_EMISSION)
					endorsement = daoPolicy.saveEndorsement(endorsement);
				
				for ( Item retItem : endorsement.getItems() ) {
					retItem.setCoverageOptions(serviceProduct.listCoverageOptionByRefDate( endorsement.getContract().getProductId(), retItem.getCoveragePlanId(), endorsement.getReferenceDate(), retItem.getItemCoverages() ) );
				}
				result.setPaymentOptions(calc.getPaymentOptionsDefault(endorsement));
			}
			result.setEndorsement(endorsement);
			
		} catch (ServiceException e) {
			logger.error("calculate error", e);
			e.printStackTrace();
			result.addStackTraceToErroList(e.getStackTrace());
		}*/
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceCalc#lifeCycleCalc(br.com.tratomais.core.model.policy.Endorsement, java.util.List)
	 */
	public SimpleCalcResult lifeCycleCalc (Endorsement endorsement, List< CoverageOption > coverageDataGrid) {
/*		SimpleCalcResult result = new SimpleCalcResult();
		try {
			if ( endorsement.getItems() == null || endorsement.getItems().size() != 1 ) {
				throw new ServiceException("invadid nuber of itens");
			}
			Endorsement workEndorsement = new Endorsement();
			BeanUtils.copyProperties( endorsement, workEndorsement );
			Item workItem = endorsement.getMainItem();
			workItem.getItemCoverages().clear();
			for ( CoverageOption coverageOption : coverageDataGrid ) {

				if(coverageOption.isContractCoverage()){
					
					if(coverageOption.getCoveragePlan()!= null){
						//Se cobertura for range e n�o foi selecionada descartar cobertura
						if ( coverageOption.getCoveragePlan().getInsuredValueType() == CoveragePlan.INSURED_VALUE_TYPE_RSA 
								&& coverageOption.getRangeValueId() == -1 ) {
							continue;
						}
						
						ItemCoverage workCoverage = workItem.addCoverage( coverageOption.getCoveragePlan().getCoverage().getCoverageId() );
	
						// Rever complementa��o de dados para o calculo
						workCoverage.setCoverageCode( coverageOption.getCoveragePlan().getCoverage().getCoverageCode() );
						workCoverage.setBasicCoverage( coverageOption.getCoveragePlan().getCoverage().isBasic() );
						workCoverage.setGoodsCode( coverageOption.getCoveragePlan().getCoverage().getGoodsCode() );
						workCoverage.setBranchId( coverageOption.getCoveragePlan().getCoverage().getBranchId() );
						workCoverage.setCoverageName( coverageOption.getCoveragePlan().getNickName() );
						//workCoverage.setcommissionFactor( commissionFactory );
						workCoverage.setDisplayOrder( coverageOption.getCoveragePlan().getDisplayOrder());
	
						
						workCoverage.setInsuredValue( coverageOption.getValue() );
						workCoverage.setRangeValueId( coverageOption.getRangeValueId() );
					}
				}	
			}
			endorsement = calc.simpleCalc(endorsement);
			boolean validCalc = true;
			for (Item tmpItem : endorsement.getItems()) {
				if (!tmpItem.isCalculationValid()) {
					validCalc = false;
					break;
				}
			}
			if (validCalc) {
				endorsement = daoPolicy.saveEndorsement(endorsement);
				for ( Item retItem : endorsement.getItems() ) {
					retItem.setCoverageOptions(serviceProduct.listCoverageOptionByRefDate( endorsement.getContract().getProductId(), retItem.getCoveragePlanId(), endorsement.getReferenceDate(), retItem.getItemCoverages() ) );
				}
				result.setPaymentOptions(calc.getPaymentOptionsDefault(endorsement));
			}
			result.setEndorsement(endorsement);
		} catch (ServiceException e) {
			logger.error("lifeCycleCalc error", e);
			e.printStackTrace();
			result.addStackTraceToErroList(e.getStackTrace());
		}*/
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceCalc#numActivePoliciesPerProductDocument(int, java.lang.String, int, java.util.Date, java.util.Date, int)
	 */
	public int numActivePoliciesPerProductDocument(int productId, String docNumber, int docType, Date dateIni, Date dateFin, int contractId){
		Integer retorno = daoCalculo.getQuantityOfContracts(productId, docNumber, docType, dateIni, dateFin, contractId);
		return retorno == null?0:retorno;
	}

	/*
	 * (non-Javadoc) 
	 * @see br.com.tratomais.core.service.IServiceCalc#validateContract(int, java.lang.String, int, java.util.Date, int)
	 */
	@Override
	public List<String> validateContract(int productId, String docNumber,
			int docType, Date dateIni, Date referenceDate, int contractId) {
		List<Erro> erros = calc.validateContract(productId, docNumber, docType, dateIni, referenceDate, contractId);
		List<String> retorno = new ArrayList<String>();
		if (erros.size() > 0){
			for (Erro erro : erros) {
				retorno.add(erro.getDescricao());
			}
		}
		return retorno;
	}

	@Override
	public Endorsement effectiveEndorsement(Endorsement endorsement, PaymentOption paymentOption, int issuanceType, Date newEffectiveDate) throws ServiceException {
		Endorsement newEndorsement = endorsement;
		try {
			newEndorsement = calc.effectiveEndorsement(endorsement, paymentOption, issuanceType, newEffectiveDate);
			
			if(newEndorsement!=null && newEndorsement.getIssuingStatus()==Endorsement.ISSUING_STATUS_PROPOSTA){
				daoPolicy.saveEndorsement(endorsement);
				serviceCollection.dispachCollection(endorsement, false);
			}
		
		} catch (ServiceException e) {
			logger.error("effectiveProposal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			newEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		
		// TODO Treating error in sending charges are currently no impact
		} catch (JMSException e) {
			logger.error("effectiveProposal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			newEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (NamingException e) {
			logger.error("effectiveProposal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			newEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (FileNotFoundException e) {
			logger.error("effectiveProposal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			newEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (IOException e) {
			logger.error("effectiveProposal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			newEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (EInsuranceJMSException e) {
			logger.error("effectiveProposal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			newEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		}

		return newEndorsement;
	}

	@Override
	public List<PaymentOption> getPaymentOptionsByPaymentType(double totalPremium, double netPremium, double policyCost, double fractioningAdditional, double taxValue, double taxRate,	int productId, int paymentType, Date refDate, Date effectiveDate, Date expireDate, Short dueDay, Integer renewalType, Integer renewalInsurerId, Integer issuanceType, Integer paymentTermType, Integer invoiceType, Integer firstPaymentType, Integer qtInstallment, Date proposalDate) {
		return calc.getPaymentOptionsByBillingMethodId(totalPremium, netPremium, policyCost, fractioningAdditional, taxValue, taxRate, productId, paymentType, refDate, effectiveDate, expireDate, dueDay, renewalType, renewalInsurerId, issuanceType, paymentTermType, invoiceType, firstPaymentType, qtInstallment, proposalDate);
	}
}