package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.tratomais.core.dao.IDaoCoverage;
import br.com.tratomais.core.dao.IDaoCoverageRule;
import br.com.tratomais.core.info.Calculus;
import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.product.Coverage;
import br.com.tratomais.core.model.product.CoverageRule;
import br.com.tratomais.core.model.product.CoverageRuleValue;
import br.com.tratomais.core.service.IServiceCoverage;
import br.com.tratomais.general.utilities.NumberUtilities;

public class ServiceCoverageImpl extends ServiceBean implements IServiceCoverage {
	private static final Logger logger = Logger.getLogger( ServiceCoverageImpl.class );

	private IDaoCoverage daoCoverage;

	private IDaoCoverageRule daoCoverageRule;
	
	public void setDaoCoverage(IDaoCoverage daoCoverage) {
		this.daoCoverage = daoCoverage;
	}

	public void setDaoCoverageRule(IDaoCoverageRule daoCoverageRule) {
		this.daoCoverageRule = daoCoverageRule;
	}

	@Override
	public Coverage findById(Integer id) {
		return daoCoverage.findById(id);
	}

	@Override
	public List<Coverage> findByName(String name) {
		return daoCoverage.findByName(name);
	}

	@Override
	public List<Coverage> listAll() {
		return daoCoverage.listAll();
	}

	@Override
	public void deleteObject(Coverage entity) {
		daoCoverage.delete(entity);
	}

	@Override
	public Coverage saveObject(Coverage entity) {
		return daoCoverage.save(entity);
	}

	@Override
	public List<Coverage> findByObject(Integer objectId) {
		return daoCoverage.findByObject(objectId);
	}

	@Override
	public double applyCoverageRule(Item item, int ruleType, int productId, int coverageId, Date refDate) {
		double coverageValue = 0.0;
		
		List<CoverageRule> coverageRuleList = daoCoverageRule.getCoverageRuleList(productId, coverageId, ruleType, refDate);
		if (coverageRuleList != null) {
			
			for (CoverageRule coverageRule : coverageRuleList) {
				
				CoverageRuleValue ruleValue = daoCoverageRule.getValueByCoverageRule(coverageRule, item, refDate);
				if (ruleValue != null) {
					
					if (!isRuleFactor(ruleValue) && isRuleFixedValue(ruleValue)) {
						coverageValue = NumberUtilities.roundDecimalPlaces(coverageValue + ruleValue.getRuleValue(), Calculus.STEP_DECIMAL_PLACES);					
						logger.debug(String.format("applyCoverageRule(rule: \"{0}\", item: \"{1}\", refDate: \"{2}\") - value: \"{3}\"", new Object[]{coverageRule, item, refDate, ruleValue.getRuleValue()}));										
					}
					else {
						ruleValue.setRuleFactor(NumberUtilities.roundDecimalPlaces(ruleValue.getRuleFactor(), Calculus.RATE_DECIMAL_PLACES));
						coverageValue = NumberUtilities.roundDecimalPlaces(coverageValue * ruleValue.getRuleFactor(), Calculus.STEP_DECIMAL_PLACES);				
						logger.debug(String.format("applyCoverageRule(rule: \"{0}\", item: \"{1}\", refDate: \"{2}\") - factor: \"{3}\"", new Object[]{coverageRule, item, refDate, ruleValue.getRuleFactor()}));										
					}
				}
			}
		}
		
		return coverageValue;
	}
	
	private boolean isRuleFactor(CoverageRuleValue ruleValue) {
		return ruleValue.getRuleFactor() != null && ruleValue.getRuleFactor() != 0;
	}
	
	private boolean isRuleFixedValue(CoverageRuleValue ruleValue) {
		return ruleValue.getRuleValue() != null && ruleValue.getRuleValue() != 0;
	}
}
