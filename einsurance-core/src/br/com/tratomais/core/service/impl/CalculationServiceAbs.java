package br.com.tratomais.core.service.impl;

import br.com.tratomais.core.exception.CalculusServiceException;
import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.product.PaymentOption;
import br.com.tratomais.core.service.ICalc;

/**
 * Abstract class 
 * @author luiz.alberoni
 */
public abstract class CalculationServiceAbs implements ICalc{
	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.ICalc#simpleEndorsement(br.com.tratomais.core.model.policy.Endorsement)
	 */
	@Override
	public final Endorsement simpleCalc(Endorsement endorsement) throws ServiceException {
		return simpleCalc(endorsement, false);
	}

	public final Endorsement simpleCalc(Endorsement endorsement, boolean isRecalculation) throws ServiceException {
		if (endorsement != null) {
			dataFillCalc(endorsement, isRecalculation);
			validateCalc(endorsement);
			if (endorsement.getErrorList().getErrorCount() == 0 ) {
				calculate(endorsement);
			}
			endorsement.flush();
			validaSaidaCalculo(endorsement);
		}
		return endorsement;
	}

	/**
	 * Validades the output for calculus
	 * @param endorsement
	 * @throws ServiceException
	 */
	abstract protected void validaSaidaCalculo(Endorsement endorsement) throws ServiceException;

	/**
	 * Validate data
	 * @param endorsement Endorsement
	 */
	protected abstract void validateCalc(Endorsement endorsement) throws CalculusServiceException;

	/**
	 * Fills out any data missing
	 * @param endorsement Endorsement
	 */
	protected abstract void dataFillCalc(Endorsement endorsement, boolean isRecalculation) throws CalculusServiceException;

	/**
	 * Calculates the endorsement
	 * @param endorsement Endorsement
	 * @throws ServiceException 
	 */
	protected abstract void calculate(Endorsement endorsement) throws ServiceException;

	@Override
	public final Endorsement effectiveProposal(Endorsement endorsement, PaymentOption paymentOption)throws ServiceException {
		dataFillProposal(endorsement, paymentOption);
		if ( validateProposal(endorsement, paymentOption) ) {
			if ( effectivate(endorsement, paymentOption) )
				if (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_EMISSION){
					verifyPendencies(endorsement);
				}
		}
		return endorsement;
	}
	
	/**
	 * Verify pendencies
	 * @param endorsement Endorsement to have pendencies checked
	 */
	protected abstract void verifyPendencies(Endorsement endorsement);

	protected void saveEffective(Endorsement endorsement) {
	}

	protected boolean effectivate(Endorsement endorsement, PaymentOption paymentOption) {
		return true;
	}

	/**
	 * Validate data
	 * @param endorsement Endorsement
	 */
	protected abstract boolean validateProposal(Endorsement endorsement, PaymentOption paymentOption) throws CalculusServiceException;
	
	/**
	 * Fills out any data missing
	 * @param endorsement Endorsement
	 */
	protected abstract void dataFillProposal(Endorsement endorsement, PaymentOption paymentOption) throws CalculusServiceException;
}
