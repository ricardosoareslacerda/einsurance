package br.com.tratomais.core.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import br.com.tratomais.core.dao.IDaoDomain;
import br.com.tratomais.core.model.Domain;
import br.com.tratomais.core.service.IServiceDomain;

public class ServiceDomainImpl extends ServiceBean implements IServiceDomain {
	private static final Logger logger = Logger.getLogger( ServiceDomainImpl.class );

	private IDaoDomain daoDomain;

	public void setDaoDomain(IDaoDomain daoDomain) {
		this.daoDomain = daoDomain;
	}

	public Domain findById(Integer id) {
		return daoDomain.findById(id);
	}

	public List<Domain> findByName(String name) {
		return daoDomain.findByName(name);
	}

	public Domain findDomainByName(String name) {
		return daoDomain.findDomainByName(name);
	}

	public List<Domain> listAll() {
		return daoDomain.listAll();
	}

	public List<Domain> listAllActive() {
		return daoDomain.listAllActive();
	}

	public void deleteObject(Domain entity) {
		daoDomain.delete(entity);
	}

	public Domain saveObject(Domain entity) {
		return daoDomain.save(entity);
	}
}
