package br.com.tratomais.core.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.tratomais.core.dao.IDaoRule;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.policy.ItemClause;
import br.com.tratomais.core.model.policy.ItemCoverage;
import br.com.tratomais.core.model.policy.ItemProfile;
import br.com.tratomais.core.model.product.Clause;
import br.com.tratomais.core.model.product.CoverageRangeValue;
import br.com.tratomais.core.model.product.PaymentOption;
import br.com.tratomais.core.model.product.Rule;
import br.com.tratomais.core.model.product.RuleFeature;
import br.com.tratomais.core.model.product.RuleTarget;
import br.com.tratomais.core.service.IServiceRule;

public class ServiceRuleImpl extends ServiceBean implements IServiceRule {

	private IDaoRule daoRule;

	/**
	 * @param daoRule the daoRule to set
	 */
	public void setDaoRule(IDaoRule daoRule) {
		this.daoRule = daoRule;
	}
	
	/**
	 * Servi�o se existe regla de inclus�o automatica de "Clausula"
	 * @param item
	 * @void applyClause
	 */
	@Override
	public void checkRuleClauseAutomatic(Item item) {
		Endorsement endorsement = item.getEndorsement();
		final Date refDate = endorsement.getReferenceDate();
		final int productId = endorsement.getContract().getProductId();
		
		// Get Target list for Action
		List<RuleTarget> ruleTargetList = daoRule.listRuleTarget(productId, RuleTarget.RULE_TYPE_CLAUSE_AUTOMATIC, RuleTarget.RULE_ACTION_TYPE_CLAUSE_INCLUDE);		
		for (RuleTarget ruleTarget : ruleTargetList) {
			boolean ruleApply = false;
			
			// Get Rule list for Target
			List<Rule> ruleList = daoRule.listRuleByTargetId(ruleTarget.getId().getRuleTargeId(), refDate);
			for (Rule rule : ruleList) {
				
				// Check if apply rule for condition
				if (!checkRuleApply(item, rule, refDate)) {
					ruleApply = false;
					break;
				} else ruleApply = true;
			}
			
			// Checks if is the Target
			if (ruleApply) {
				// Checks if exists the Clause
				Clause clause = daoRule.getClauseByProductId(productId, ruleTarget.getAttributeValue_1(), refDate);
				if (clause != null) {
					ItemClause itemClause = item.addClause(clause.getId().getClauseId());
					
					itemClause.setClauseCode(clause.getClauseCode());
					itemClause.setClauseDescription(clause.getDescription());
					itemClause.setClauseAutomatically(true);
					itemClause.setEffectiveDate(endorsement.getEffectiveDate());
					itemClause.setExpiryDate(endorsement.getExpiryDate());
				}			 
			}
		}
	}
	
	/**
	 * Servi�o se existe regla de bloqueio associado � "Cobertura"
	 * @param itemCoverage
	 * @return applyBlockade
	 */
	@Override
	public boolean checkRuleCoverage(ItemCoverage itemCoverage) {
		boolean applyBlockade = false;
		
		Item item = itemCoverage.getItem();
		Endorsement endorsement = item.getEndorsement();
		final Date refDate = endorsement.getReferenceDate();
		final int productId = endorsement.getContract().getProductId();
		final int coverageId = itemCoverage.getId().getCoverageId();
		
		// Get Target list for Action
		List<RuleTarget> ruleTargetList = daoRule.listRuleTarget(productId, RuleTarget.RULE_TYPE_COVERAGE, RuleTarget.RULE_ACTION_TYPE_BLOCKADE);		
		for (RuleTarget ruleTarget : ruleTargetList) {
			
			// Checks if is the Target
			if (((ruleTarget.getAttributeValue_1() != 0) && (ruleTarget.getAttributeValue_1() == coverageId)) ){
				boolean ruleApply = false;
				
				// Get Rule list for Target
				List<Rule> ruleList = daoRule.listRuleByTargetId(ruleTarget.getId().getRuleTargeId(), refDate);		
				for (Rule rule : ruleList) {
					
					// Check if apply rule for condition
					if (!checkRuleApply(item, rule, refDate)) {
						ruleApply = false;
						break;
					} else ruleApply = true;
				}
				if (ruleApply) applyBlockade = true;
			}
		}
		
		// If apply all rules, blocked
		return (!applyBlockade);
	}
	
	/**
	 * Servi�o se existe regla de bloqueio associado ao "Valor da Cobertura"
	 * @param itemCoverage
	 * @return applyBlockade
	 */
	@Override
	public boolean checkRuleInsuredValue(ItemCoverage itemCoverage) {
		boolean applyBlockade = false;
		
		Item item = itemCoverage.getItem();
		Endorsement endorsement = item.getEndorsement();
		final Date refDate = endorsement.getReferenceDate();
		final int productId = endorsement.getContract().getProductId();
		final int planId = item.getCoveragePlanId();
		final int coverageId = itemCoverage.getId().getCoverageId();
		final double insuredValue = (itemCoverage.getInsuredValue() != null ? itemCoverage.getInsuredValue() : 0.0);
		
		// Get Target list for Action
		List<RuleTarget> ruleTargetList = daoRule.listRuleTarget(productId, RuleTarget.RULE_TYPE_INSURED_VALUE, RuleTarget.RULE_ACTION_TYPE_BLOCKADE);		
		for (RuleTarget ruleTarget : ruleTargetList) {
			
			// Get the Range the for insuredValue Limit
			CoverageRangeValue coverageRangeValue = daoRule.getCoverageRangeValueByInsuredValueLimit(productId, planId, coverageId, insuredValue, refDate);
			if (coverageRangeValue != null) { 
				// Checks if is the Target
				if (((ruleTarget.getAttributeValue_1() != 0) && (ruleTarget.getAttributeValue_1() == coverageId)) &&
					((ruleTarget.getAttributeValue_2() != 0) && (ruleTarget.getAttributeValue_2() == coverageRangeValue.getId().getRangeId())) ) {
					boolean ruleApply = false;
					
					// Get Rule list for Target
					List<Rule> ruleList = daoRule.listRuleByTargetId(ruleTarget.getId().getRuleTargeId(), refDate);		
					for (Rule rule : ruleList) {
						
						// Check if apply rule for condition
						if (!checkRuleApply(item, rule, refDate)) {
							ruleApply = false;
							break;
						} else ruleApply = true;
					}
					if (ruleApply) applyBlockade = true;
				}
			}
		}
		
		// If apply all rules, blocked
		return (!applyBlockade);
	}
	
	/**
	 * Servi�o se existe regla de bloqueio associado � "Franquia da Cobertura"
	 * @param itemCoverage
	 * @return applyBlockade
	 */
	@Override
	public boolean checkRuleDeducible(ItemCoverage itemCoverage) {
		boolean applyBlockade = false;
		
		Item item = itemCoverage.getItem();
		Endorsement endorsement = item.getEndorsement();
		final Date refDate = endorsement.getReferenceDate();
		final int productId = endorsement.getContract().getProductId();
		final int coverageId = itemCoverage.getId().getCoverageId();
		final int deductibleId = (itemCoverage.getDeductibleId() != null ? itemCoverage.getDeductibleId() : 0);
		
		// Get Target list for Action
		List<RuleTarget> ruleTargetList = daoRule.listRuleTarget(productId, RuleTarget.RULE_TYPE_DEDUCTIBLE, RuleTarget.RULE_ACTION_TYPE_BLOCKADE);		
		for (RuleTarget ruleTarget : ruleTargetList) {
			
			// Checks if is the Target
			if (((ruleTarget.getAttributeValue_1() != 0) && (ruleTarget.getAttributeValue_1() == coverageId)) &&
				((ruleTarget.getAttributeValue_2() != 0) && (ruleTarget.getAttributeValue_2() == deductibleId)) ){
				boolean ruleApply = false;
				
				// Get Rule list for Target
				List<Rule> ruleList = daoRule.listRuleByTargetId(ruleTarget.getId().getRuleTargeId(), refDate);		
				for (Rule rule : ruleList) {
					
					// Check if apply rule for condition
					if (!checkRuleApply(item, rule, refDate)) {
						ruleApply = false;
						break;
					} else ruleApply = true;
				}
				if (ruleApply) applyBlockade = true;
			}
		}
		
		// If apply all rules, blocked
		return (!applyBlockade);
	}
	
	/**
	 * Servi�o se existe regla de bloqueio associado ao "Perfil"
	 * @param itemProfile
	 * @return applyBlockade
	 */
	@Override
	public boolean checkRuleProfile(ItemProfile itemProfile) {
		boolean applyBlockade = false;
		
		Item item = itemProfile.getItem();
		Endorsement endorsement = item.getEndorsement();
		final Date refDate = endorsement.getReferenceDate();
		final int productId = endorsement.getContract().getProductId();
		final int questionnaireId = itemProfile.getId().getQuestionnaireId();
		final int questionId = itemProfile.getId().getQuestionId();
		final int responseId = itemProfile.getId().getResponseId();
		
		// Get Target list for Action
		List<RuleTarget> ruleTargetList = daoRule.listRuleTarget(productId, RuleTarget.RULE_TYPE_PROFILE, RuleTarget.RULE_ACTION_TYPE_BLOCKADE);		
		for (RuleTarget ruleTarget : ruleTargetList) {
			
			// Checks if is the Target
			if (((ruleTarget.getAttributeValue_1() != 0) && (ruleTarget.getAttributeValue_1() == questionnaireId)) &&
				((ruleTarget.getAttributeValue_2() != 0) && (ruleTarget.getAttributeValue_2() == questionId)) &&
				(((ruleTarget.getAttributeValue_3() != 0) && (ruleTarget.getAttributeValue_3() == responseId)) ||
				 ((ruleTarget.getAttributeValue_3() == 0))) ){
				boolean ruleApply = false;
				
				// Get Rule list for Target
				List<Rule> ruleList = daoRule.listRuleByTargetId(ruleTarget.getId().getRuleTargeId(), refDate);		
				for (Rule rule : ruleList) {
					
					// Check if apply rule for condition
					if (!checkRuleApply(item, rule, refDate)) {
						ruleApply = false;
						break;
					} else ruleApply = true;
				}
				if (ruleApply) applyBlockade = true;
			}
		}
		
		// If apply all rules, blocked
		return (!applyBlockade);
	}
	
	/**
	 * Servi�o se existe regla de bloqueio associado � "Forma de Pagamento"
	 * @param endorsement
	 * @param paymentOption
	 * @return applyBlockade
	 */
	@Override
	public boolean checkRulePaymentTerm(Endorsement endorsement, PaymentOption paymentOption) {
		boolean applyBlockade = false;
		
		Item item = endorsement.getMainItem();
		final Date refDate = endorsement.getReferenceDate();
		final int productId = endorsement.getContract().getProductId();
		final int paymentId = paymentOption.getPaymentId();
		final int billingMethodId = paymentOption.getBillingMethodId();
		
		// Get Target list for Action
		List<RuleTarget> ruleTargetList = daoRule.listRuleTarget(productId, RuleTarget.RULE_TYPE_PAYMENT_TERM, RuleTarget.RULE_ACTION_TYPE_BLOCKADE);		
		for (RuleTarget ruleTarget : ruleTargetList) {
			
			// Checks if is the Target
			if (((ruleTarget.getAttributeValue_1() != 0) && (ruleTarget.getAttributeValue_1() == paymentId)) &&
				(((ruleTarget.getAttributeValue_2() != 0) && (ruleTarget.getAttributeValue_2() == billingMethodId)) ||
				 ((ruleTarget.getAttributeValue_2() == 0))) ){
				boolean ruleApply = false;
				
				// Get Rule list for Target
				List<Rule> ruleList = daoRule.listRuleByTargetId(ruleTarget.getId().getRuleTargeId(), refDate);		
				for (Rule rule : ruleList) {
					
					// Check if apply rule for condition
					if (!checkRuleApply(item, rule, refDate)) {
						ruleApply = false;
						break;
					} else ruleApply = true;
				}
				if (ruleApply) applyBlockade = true;
			}
		}
		
		// If apply all rules, blocked
		return (!applyBlockade);
	}
	
	/**
	 * Servi�o se existe regla de bloqueio associado ao "Tipo de Pagamento"
	 * @param endorsement
	 * @param paymentType
	 * @return applyBlockade
	 */
	@Override
	public boolean checkRulePaymentType(Endorsement endorsement, int paymentType) {
		boolean applyBlockade = false;
		
		Item item = endorsement.getMainItem();
		final Date refDate = endorsement.getReferenceDate();
		final int productId = endorsement.getContract().getProductId();
		
		// Get Target list for Action
		List<RuleTarget> ruleTargetList = daoRule.listRuleTarget(productId, RuleTarget.RULE_TYPE_PAYMENT_TYPE, RuleTarget.RULE_ACTION_TYPE_BLOCKADE);		
		for (RuleTarget ruleTarget : ruleTargetList) {
			
			// Checks if is the Target
			if (((ruleTarget.getAttributeValue_1() != 0) && (ruleTarget.getAttributeValue_1() ==  paymentType)) ){
				boolean ruleApply = false;
				
				// Get Rule list for Target
				List<Rule> ruleList = daoRule.listRuleByTargetId(ruleTarget.getId().getRuleTargeId(), refDate);
				for (Rule rule : ruleList) {
					
					// Check if apply rule for condition
					if (!checkRuleApply(item, rule, refDate)) {
						ruleApply = false;
						break;
					} else ruleApply = true;
				}
				if (ruleApply) applyBlockade = true;
			}
		}
		
		// If apply all rules, blocked
		return (!applyBlockade);
	}
	
	/**
	 * Servi�o se existe regla de aplica��o de "Pendencia"
	 * @param endorsement
	 * @param pendencyId
	 * @return applyPendency
	 */
	@Override
	public boolean checkRulesPendency(Item item, int pendencyId) {
		boolean applyPendency = false;
		
		Endorsement endorsement = item.getEndorsement();
		final Date refDate = endorsement.getReferenceDate();
		final int productId = endorsement.getContract().getProductId();
		final int objectId = item.getObjectId();
		
		// Get Target list for Action
		List<RuleTarget> ruleTargetList = daoRule.listRuleTarget(productId, RuleTarget.RULE_TYPE_PENDENCY, RuleTarget.RULE_ACTION_TYPE_PENDENCY_INCLUDE, pendencyId);		
		for (RuleTarget ruleTarget : ruleTargetList) {
			
			// Checks if is the Target
			if ((((ruleTarget.getAttributeValue_2() != 0) && (ruleTarget.getAttributeValue_2() == objectId)) ||
				 ((ruleTarget.getAttributeValue_2() == 0))) ) {
				boolean ruleApply = false;
				
				// Get Rule list for Target
				List<Rule> ruleList = daoRule.listRuleByTargetId(ruleTarget.getId().getRuleTargeId(), refDate);
				for (Rule rule : ruleList) {
					
					// Check if apply rule for condition
					if (!checkRuleApply(item, rule, refDate)) {
						ruleApply = false;
						break;
					} else ruleApply = true;
				}
				if (ruleApply) applyPendency = true;
			}
		}
		
		// If apply all rules, pendency
		return applyPendency;
	}
	
	private boolean checkRuleApply(Item item, Rule rule, Date refDate){
		boolean isAttributesOk = false;
		List<Integer> listAttributeReturnsList = new ArrayList<Integer>();
		listAttributeReturnsList.add(Item.PERGUNTA);
		listAttributeReturnsList.add(Item.RESPOSTA);
		listAttributeReturnsList.add(Item.COBERTURA);
		listAttributeReturnsList.add(Item.TIPO_DE_FRANQUIA);
		listAttributeReturnsList.add(Item.COBERTURA_RANGE);
		listAttributeReturnsList.add(Item.CLAUSE);
		listAttributeReturnsList.add(Item.FORMA_DE_PAGAMENTO);
		
		//Checks Attribute1
		Integer attrNumber1 = rule.getAttributeId_1();
		if(attrNumber1 != null && attrNumber1 > 0){
			
			//Se o valor do Atributo for uma lista de valores
			if(listAttributeReturnsList.indexOf(attrNumber1) > -1){
				List<Integer> listAttributeValues = item.getAttributeListValue(attrNumber1);
				
				for(RuleFeature ruleFeature : daoRule.listRuleFeatureByRuleId(rule.getRuleId(), refDate)){
					if(listAttributeValues.indexOf(ruleFeature.getId().getAttributeValue_1()) > -1){
						isAttributesOk = true;
						break;
					}
				}
			} else { //Se o valor do atributo for um unico valor
				Integer attributeNumber1Value = item.getAttributeValue(attrNumber1);
				
				for(RuleFeature ruleFeature : daoRule.listRuleFeatureByRuleId(rule.getRuleId(), refDate)){
					if(ruleFeature.getId().getAttributeValue_1().equals(attributeNumber1Value)){
						isAttributesOk = true;
						break;
					}
				}	
			}
		}
		
		//Checks Attribute2
		Integer attrNumber2 = rule.getAttributeId_2();
		if(isAttributesOk && attrNumber2 != null && attrNumber2 > 0){
			isAttributesOk = false;
			
			//Se o valor do Atributo for uma lista de valores
			if(listAttributeReturnsList.indexOf(attrNumber2) > -1){
				List<Integer> listAttributeValues = item.getAttributeListValue(attrNumber2);
				
				for(RuleFeature ruleFeature : daoRule.listRuleFeatureByRuleId(rule.getRuleId(), refDate)){
					if(listAttributeValues.indexOf(ruleFeature.getId().getAttributeValue_2()) > -1){
						isAttributesOk = true;
						break;
					}
				}
			} else { //Se o valor do atributo for um unico valor
				Integer attributeNumber2Value = item.getAttributeValue(attrNumber2);
				
				for(RuleFeature ruleFeature : daoRule.listRuleFeatureByRuleId(rule.getRuleId(), refDate)){
					if(ruleFeature.getId().getAttributeValue_2().equals(attributeNumber2Value)){
						isAttributesOk = true;
						break;
					}
				}	
			}
		}
		
		//Checks Attribute3
		Integer attrNumber3 = rule.getAttributeId_3();
		if(isAttributesOk && attrNumber3 != null && attrNumber3 > 0){
			isAttributesOk = false;

			//Se o valor do Atributo for uma lista de valores
			if(listAttributeReturnsList.indexOf(attrNumber3) > -1){
				List<Integer> listAttributeValues = item.getAttributeListValue(attrNumber3);
				
				for(RuleFeature ruleFeature : daoRule.listRuleFeatureByRuleId(rule.getRuleId(), refDate)){
					if(listAttributeValues.indexOf(ruleFeature.getId().getAttributeValue_3()) > -1){
						isAttributesOk = true;
						break;
					}
				}
			} else { //Se o valor do atributo for um unico valor
				Integer attributeNumber3Value = item.getAttributeValue(attrNumber3);
				
				for(RuleFeature ruleFeature : daoRule.listRuleFeatureByRuleId(rule.getRuleId(), refDate)){
					if(ruleFeature.getId().getAttributeValue_3().equals(attributeNumber3Value)){
						isAttributesOk = true;
						break;
					}
				}	
			}
		}
		
		//Checks Attribute4
		Integer attrNumber4 = rule.getAttributeId_4();
		if(isAttributesOk && attrNumber4 != null && attrNumber4 > 0){
			isAttributesOk = false;
			
			//Se o valor do Atributo for uma lista de valores
			if(listAttributeReturnsList.indexOf(attrNumber4) > -1){
				List<Integer> listAttributeValues = item.getAttributeListValue(attrNumber4);
				
				for(RuleFeature ruleFeature : daoRule.listRuleFeatureByRuleId(rule.getRuleId(), refDate)){
					if(listAttributeValues.indexOf(ruleFeature.getId().getAttributeValue_4()) > -1){
						isAttributesOk = true;
						break;
					}
				}
			} else { //Se o valor do atributo for um unico valor
				Integer attributeNumber4Value = item.getAttributeValue(attrNumber4);
				
				for(RuleFeature ruleFeature : daoRule.listRuleFeatureByRuleId(rule.getRuleId(), refDate)){
					if(ruleFeature.getId().getAttributeValue_4().equals(attributeNumber4Value)){
						isAttributesOk = true;
						break;
					}
				}	
			}
		}
		
		//Checks Attribute5
		Integer attrNumber5 = rule.getAttributeId_5();
		if(isAttributesOk && attrNumber5 != null && attrNumber5 > 0){
			isAttributesOk = false;
			
			//Se o valor do Atributo for uma lista de valores
			if(listAttributeReturnsList.indexOf(attrNumber5) > -1){
				List<Integer> listAttributeValues = item.getAttributeListValue(attrNumber5);
				
				for(RuleFeature ruleFeature : daoRule.listRuleFeatureByRuleId(rule.getRuleId(), refDate)){
					if(listAttributeValues.indexOf(ruleFeature.getId().getAttributeValue_5()) > -1){
						isAttributesOk = true;
						break;
					}
				}
			} else { //Se o valor do atributo for um unico valor
				Integer attributeNumber5Value = item.getAttributeValue(attrNumber5);
				
				for(RuleFeature ruleFeature : daoRule.listRuleFeatureByRuleId(rule.getRuleId(), refDate)){
					if(ruleFeature.getId().getAttributeValue_5().equals(attributeNumber5Value)){
						isAttributesOk = true;
						break;
					}
				}	
			}
		}
		
		return isAttributesOk;
	}
}