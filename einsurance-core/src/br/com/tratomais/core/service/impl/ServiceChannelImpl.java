package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.tratomais.core.dao.IDaoChannel;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Partner;
import br.com.tratomais.core.service.IServiceChannel;
import br.com.tratomais.core.util.TransportationClass;

public class ServiceChannelImpl extends ServiceBean implements IServiceChannel {
	private static final Logger logger = Logger.getLogger( ServiceChannelImpl.class );
	
	private IDaoChannel daoChannel;
	
	public void setDaoChannel(IDaoChannel daoChannel) {
		this.daoChannel = daoChannel;
	}
	
	public List<Channel> findByName(String name) {
		return daoChannel.findByName(name);
	}

	public List<Channel> findByNickName(String userName) {
		return daoChannel.findByNickName(userName);
	}

	public Channel findById(Integer id) {
		return daoChannel.findById(id);
	}

	public List<Channel> listAll() {
		return daoChannel.listAll();
	}

	public void deleteObject(Channel entity) {
		daoChannel.delete(entity);
	}

	public Channel saveObject(Channel entity) {
		return daoChannel.save(entity);
	}

	public List< Channel > findByPartnerId(int partnerId, boolean onlyActive) {		
		return daoChannel.findByPartnerId(partnerId, onlyActive);
	}

	public List<Channel> findByNameAndPartner(String name, Partner partner) {
		return daoChannel.findByNameAndPartner(name, partner);
	}

	public List<Channel> listAllActive() {
		return daoChannel.listAllActive();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceChannel#findByUserAndPartner(br.com.tratomais.core.model.User, br.com.tratomais.core.model.customer.Partner)
	 */
	@Override
	public List<Channel> findByUserAndPartner(User user, int partnerId) {
		return daoChannel.findByUserAndPartner(user, partnerId);
	}

	@Override
	public TransportationClass<Channel> listChannelToGrid(int limit, int page,
			String columnSorted, String typeSorted, String search,
			String searchField, String searchString, int partnerID) {
		// TODO Auto-generated method stub
		return daoChannel.listChannelToGrid(limit, page, columnSorted, typeSorted, search, searchField, searchString, partnerID);
	}

	@Override
	public List<Channel> listChannel(int insurerID, int partnerID, Date refDate) {
		return daoChannel.listChannel(insurerID, partnerID, refDate);
	}
}
