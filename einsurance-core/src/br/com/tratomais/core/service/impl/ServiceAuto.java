package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.dao.IDaoAuto;
import br.com.tratomais.core.model.auto.AutoBrand;
import br.com.tratomais.core.model.auto.AutoModel;
import br.com.tratomais.core.model.auto.AutoVersion;
import br.com.tratomais.core.service.IServiceAuto;

public class ServiceAuto implements IServiceAuto {
	private IDaoAuto daoAuto;

	public void setDaoAuto(IDaoAuto daoAuto) {
		this.daoAuto = daoAuto;
	}

	@Override
	public Double getAutoCost(int autoModelId, Integer year, Date referenceDate) {
		return daoAuto.getAutoCost(autoModelId, year, referenceDate);
	}

	@Override
	public List<AutoBrand> listAutoBrands() {
		return daoAuto.listBrands();
	}

	@Override
	public List<AutoModel> listAutoModelForBrand(int brandId) {
		return daoAuto.listModels(brandId);
	}

	@Override
	public List<Integer> listAutoYear(int autoModelId, Date referenceDate) {
		return daoAuto.listModelYear(autoModelId, referenceDate);
	}
	
	@Override
	public AutoVersion getAutoVersion(int autoModelId, Date effectiveDate) {
		return daoAuto.getAutoVersion(autoModelId, effectiveDate);
	}
	
	@Override
	public AutoVersion findAutoVersion(int autoModelId, Date referenceDate) {
		return daoAuto.findAutoVersion(autoModelId, referenceDate);
	}
}
