package br.com.tratomais.core.service.impl;

import java.util.List;

import br.com.tratomais.core.dao.IDaoSearchDocument;
import br.com.tratomais.core.model.paging.Paginacao;
import br.com.tratomais.core.model.search.InstallmentParameters;
import br.com.tratomais.core.model.search.InstallmentResult;
import br.com.tratomais.core.model.search.SearchDocumentParameters;
import br.com.tratomais.core.model.search.SearchDocumentResult;
import br.com.tratomais.core.service.IServiceSearchDocument;

public class ServiceSearchDocumentImpl extends ServiceBean implements IServiceSearchDocument {

	private IDaoSearchDocument daoSearchDocument;
	
	public void setDaoCertificateReport(IDaoSearchDocument daoSearchDocument){
		this.daoSearchDocument = daoSearchDocument;
	}
	
	public List<SearchDocumentResult> listSearchDocument(SearchDocumentParameters searchDocumentParameters){
		return daoSearchDocument.listSearchDocument(searchDocumentParameters);
	}
	
	public Paginacao listSearchDocumentPaging(SearchDocumentParameters searchDocumentParameters){	
		Paginacao paginacao = new Paginacao();
		paginacao.setTotalDados(daoSearchDocument.listSearchDocumentTotalRecords(searchDocumentParameters));
		paginacao.setListaDados(daoSearchDocument.listSearchDocument(searchDocumentParameters));
		return paginacao;
	}

	@Override
	public List<InstallmentResult> listInstallment(InstallmentParameters installmentParameters) {
		return daoSearchDocument.listInstallment(installmentParameters);
	}

	@Override
	public List<SearchDocumentResult> listSearchDocument(SearchDocumentParameters parameters, boolean isAuthentication) {
		return daoSearchDocument.listSearchDocument(parameters, isAuthentication);
	}
}
