package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.dao.IDaoDataOption;
import br.com.tratomais.core.model.product.DataOption;
import br.com.tratomais.core.model.product.IdentifiedList;
import br.com.tratomais.core.service.IServiceDataOption;

public class ServiceDataOptionImpl extends ServiceBean implements IServiceDataOption {

	private IDaoDataOption daoDataOption;
	
	public void setDaoDataOption(IDaoDataOption daoDataOption) {
		this.daoDataOption = daoDataOption;
	}

	public DataOption findById(Integer id) {
		return daoDataOption.findById( id );
	}

	public List< DataOption > findByName(String name) {
		return daoDataOption.findByName( name );
	}

	public List< DataOption > listAll() {
		return daoDataOption.listAll();
	}

	public List< DataOption > listDataOption(int dataGroupId) {
		return daoDataOption.listDataOption(dataGroupId);
	}

	/**
	 * List Data Options by @see DataGroup 
	 * @param dataGroupId
	 * @param bringAll true/false (list all DataOption Active=true and Active=false) 
	 * @return List of @see DataGroup
	 */
	public List< DataOption > listDataOption(int dataGroupId, boolean bringAll) {
		return daoDataOption.listDataOption(dataGroupId, bringAll);
	}
	
	/**
	 * List Data Options by @see DataGroup 
	 * @param dataGroupId
	 * @param bringAll true/false (list all DataOption Active=true and Active=false) 
	 * @param externalCode
	 * @return List of @see DataGroup
	 */
	public List<DataOption> listDataOption(int dataGroupId, boolean bringAll, String externalCode){
		return daoDataOption.listDataOption(dataGroupId, bringAll, externalCode);
	}
	
	/**
	 * to {@link ServicePolicyImpl} listPaymentType method
	 * */
	@Deprecated
	public List<DataOption> listPaymentType(int dataGroupId, int productId, Date referenceDate){
		return daoDataOption.listPaymentType(dataGroupId, productId, referenceDate);
	}

	@Override
	public List<DataOption> listDataOptionByIdAndParent(int dataGroupId, int parentId) {
		return daoDataOption.listDataOptionByIdAndParent(dataGroupId, parentId);
	}

	@Override
	public IdentifiedList findIdentifiedListDataOptionByDataGroup(int dataGroupId, String identifierString, Date refDate){
		return daoDataOption.findIdentifiedListDataOptionByDataGroup(dataGroupId, identifierString, refDate);
	}

	@Override
	public IdentifiedList findIdentifiedListDataOptionByDataGroupParentId(int dataGroupId, int parentId, String identifierString, Date refDate){
		return daoDataOption.findIdentifiedListDataOptionByDataGroupParentId(dataGroupId, parentId, identifierString, refDate);
	}

	@Override
	public List<DataOption> listDataOptionByRefDate(int dataGroupID, Date refDate) {
		return daoDataOption.listDataOptionByRefDate(dataGroupID, refDate);
	}

	@Override
	public List<DataOption> listDataOptionByFieldValue(String fieldValue, Date refDate) {
		return daoDataOption.listDataOptionByFieldValue(fieldValue, refDate);
	}

	@Override
	public List<DataOption> listDataOptionByParentID(int parentID, Date refDate) {
		return daoDataOption.listDataOptionByParentID(parentID, refDate);
	}

	@Override
	public List<DataOption> listDataOptionByParentID(String fieldValue, int parentID, Date refDate) {
		return daoDataOption.listDataOptionByParentID(fieldValue, parentID, refDate);
	}
}
