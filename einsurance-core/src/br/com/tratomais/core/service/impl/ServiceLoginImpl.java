package br.com.tratomais.core.service.impl;

import java.util.List;

import org.hibernate.Hibernate;

import br.com.tratomais.core.dao.IDaoLogin;
import br.com.tratomais.core.infrastructure.AuthenticationHelper;
import br.com.tratomais.core.model.Login;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.UserPartner;
import br.com.tratomais.core.service.IServiceLogin;
import br.com.tratomais.core.util.Utilities;
import br.com.tratomais.core.util.log.AuditLogger;

public class ServiceLoginImpl extends ServiceBean implements IServiceLogin {
	private IDaoLogin daoLogin;

	public void setDaoLogin(IDaoLogin daoLogin) {
		this.daoLogin = daoLogin;
	}

	public User validateLogin(String userName, String passwordKey) {
		User authenticatePrincipal = new AuthenticationHelper().authenticatePrincipal(userName, passwordKey);
		if (authenticatePrincipal != null) {
			int seq = 0;
			for (UserPartner userPartner : authenticatePrincipal.getUserPartners()) {
				Hibernate.initialize(userPartner.getPartner());
				if(seq==0){
					AuthenticationHelper.setSelectedPartnerInLoggedUser(userPartner.getPartner().getPartnerId());
				}
				if (userPartner.isPartnerDefault() || authenticatePrincipal.getUserPartners().size() == 1) {
					AuthenticationHelper.setSelectedPartnerInLoggedUser(userPartner.getPartner().getPartnerId());
				}
				seq++;
			}
		}
		return authenticatePrincipal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceLogin#saveObject(br.com.tratomais.core.model.User)
	 */
	public Login saveObject(User user) {
		if (this.loginExistence(user.getLogin(), user.getDomain().getName()) == true) {
			// TODO thwrow new Login ja existe
			return null;
		}
		Login login = new Login();
		login.setDomain(user.getDomain().getName());
		// login.setExpirePassword(user.isAccountNonExpired());
		login.setLogin(user.getLogin());
		login.setPasswordKey(Utilities.getHashSha("123"));
		return daoLogin.save(login);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceLogin#loginExistence(java.lang.String, java.lang.String)
	 */
	public boolean loginExistence(String login, String domain) {
		Login loginExistence = daoLogin.findByLogin(login, domain);
		if (loginExistence != null) {
			return true;
		}
		return false;
	}

	public List<Login> listAll() {
		return daoLogin.listAll();
	}

	public Login findById(Integer id) {
		return this.daoLogin.findById(id);
	}

	public List<Login> findByName(String domain) {
		return this.daoLogin.findByNameDomain(domain);
	}

	public Login findByLogin(String login, String domain) {
		return this.daoLogin.findByLogin(login, domain);
	}

	public Login saveObject(Login login) {
		login.setPasswordKey(Utilities.getHashSha(login.getPasswordKey()));
		return this.daoLogin.save(login);
	}

	public void deleteObject(Login login) {
		this.daoLogin.delete(login);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceLogin#deleteObjectByLoginName(java.lang.String, java.lang.String)
	 */
	public void deleteObjectByLoginName(String loginName, String domainName) {
		Login findByLogin = this.daoLogin.findByLogin(loginName, domainName);
		if (findByLogin != null)
			this.daoLogin.delete(findByLogin);
	}

	public Login findByUser(User user) {
		return findByLogin(user.getLogin(), user.getDomain().getName());
	}

	public void logout() {
		AuthenticationHelper.logout();
	}
	
	
	public void logAuditAction(String message) {
		AuditLogger auditLogger = new AuditLogger();
		auditLogger.logAuditAction(message);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean changeUserPassword(String login, String domain, String oldPassword,
			String newPassword) {
		Login loginObj = daoLogin.findByLogin(login, domain);
		if ( loginObj.getPasswordKey().equals(Utilities.getHashSha(oldPassword))) {
			loginObj.setPasswordKey(Utilities.getHashSha(newPassword));
			daoLogin.save(loginObj);
			return true;
		} return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean loginExistence(String login) {
		String[] split = login.split("@");
		if (split.length != 2)
			return false;
		return loginExistence(split[0], split[1]);
	}

}
