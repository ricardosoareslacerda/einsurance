package br.com.tratomais.core.service.impl;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;

//import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.tratomais.core.dao.impl.DaoReportJasperImpl;
import br.com.tratomais.core.exception.BusinessException;
import br.com.tratomais.core.exception.DataAccessException;
import br.com.tratomais.core.model.report.Report;
import br.com.tratomais.core.report.EntityFactory;
import br.com.tratomais.core.report.ReportFieldTO;
import br.com.tratomais.core.report.ReportTO;
import br.com.tratomais.core.report.TOFactory;
import br.com.tratomais.core.util.UtilFactory;

@Service
public class ReportService {
	//private static final Logger logger = Logger.getLogger( ReportService.class );

	@Autowired
	public DaoReportJasperImpl dao;
	// @Resource(name="entityFactory")
	public EntityFactory ef;
	public TOFactory tof;
	public UtilFactory uf;

	// public void setDaoReportJasper (IDaoReportJasper dao) {
	// this.dao = dao;
	// }

	public ReportTO getReport(Long id) {
		Report r = dao.getObject(id);
		return (ReportTO) tof.convToReportTO(r);
	}

	/**
	 * Return a connection with data base. <br>
	 * The autocommit status is true.
	 */
	public Connection getConnection() throws DataAccessException {
		return getDestinationConnection("jdbc/einsurancedb");
	}
	
	public Connection getDestinationConnection(String datasource) throws DataAccessException{
		boolean isAutoCommit = true;
		Connection con = null;
		
		if(datasource != null){
	        try {
	            //Obtem o namming context
	     	    Context initCtx = new InitialContext();
	     	    Context envCtx = (Context) initCtx.lookup("java:comp/env");
	
	     	    //Recupera o objeto Datasource
	     	    DataSource ds = (DataSource) envCtx.lookup(datasource);
	
	     	    //Aloca uma conexao no Pool
	     	    con = ds.getConnection();
				con.setAutoCommit(isAutoCommit);
	
	        } catch (SQLException e) {
				throw new DataAccessException(e);
	        } catch (NamingException e) {
	         	throw new DataAccessException(e);
	        }
		}else{
			con = getConnection();
		}
		return con;
	}		

	/**
	 * Close a Statement
	 */
	public void closeStatement(ResultSet rs, PreparedStatement pstmt) {
		try {
			if (rs != null) {
				rs.close();
			}
			if (pstmt != null) {
				pstmt.close();
			}
		} catch (SQLException ec) {
			ec.printStackTrace();
			// LogUtil.log(this, LogUtil.LOG_ERROR, "DB Closing statement error", ec);
		}
	}

	/**
	 * Close a connection
	 */
	public void closeConnection(Connection c) {
		try {
			if (c != null){
				c.close();
				//this.dao.getSessionFactory().close();
			}
		} catch (Exception ec) {
			ec.printStackTrace();
		}
	}

	/**
	 * Perform statement into data base and return an raw object (ResultSet)
	 */
	@SuppressWarnings("unchecked")
	public byte[] performJasperReport(ReportTO rto, String path) throws DataAccessException {
		Connection c = null;
		byte[] response = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		boolean isEmpty = false;

		try {
			c = getDestinationConnection(rto.getReportConnection());
			
			pstmt = this.prepareStatement(rto.getSqlWithoutDomain(), rto, c);
			rs = pstmt.executeQuery();
			
			Map parameters = new HashMap();
			parameters.put(JRParameter.REPORT_CONNECTION, pstmt.getConnection());
			parameters.put("PATH_REPORT", path);
			if (rto.getHandler() != null) {
				Locale loc = rto.getLocale();
				parameters.put(JRParameter.REPORT_LOCALE, loc);
			}
			// 	SELECT ContractID, InsurerID, SubsidiaryID, BrokerID FROM Contract WHERE ContractID = ?#ContractID{ }(3)#
			// 	carrega o relatório com o caminho do arquivo .jasper
			//	JasperReport jasperReport = (JasperReport) JRLoader.loadObject(path + "/" + rto.getReportFileName());
			//	String fullFileName = path + "/" + rto.getReportFileName();
			//	JasperDesign jasperDesign = JRXmlLoader.load(fullFileName);
			//	JasperReport jasperReportOld = JasperCompileManager.compileReport(jasperDesign);

			JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
			JasperPrint jp = JasperFillManager.fillReport(path + "/" + rto.getReportFileName(), parameters, jrRS);
			isEmpty = (jp.getPages().size() == 0);
			if (!isEmpty) {
				if (rto.getExportReportFormat().equals(ReportTO.REPORT_EXPORT_PDF)) {
					response = JasperExportManager.exportReportToPdf(jp);

				}
				else
					if (rto.getExportReportFormat().equals(ReportTO.REPORT_EXPORT_RTF)) {
						JRRtfExporter exporter = new JRRtfExporter();
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						exporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "UTF-8");
						exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
						exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
						exporter.exportReport();
						response = baos.toByteArray();

					}
					else
						if (rto.getExportReportFormat().equals(ReportTO.REPORT_EXPORT_ODT)) {
							JROdtExporter exporter = new JROdtExporter();
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							exporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "UTF-8");
							exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
							exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
							exporter.exportReport();
							response = baos.toByteArray();
						}
					else 
						if(rto.getExportReportFormat().equals(ReportTO.REPORT_EXPORT_EXCEL)){
							JRXlsExporter exporter = new JRXlsExporter();
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jp);
							exporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, baos);
							exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);  
							exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,Boolean.TRUE);  
							exporter.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET,Integer.decode("65000"));  
							exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);  
							exporter.setParameter(JRXlsExporterParameter.IS_IMAGE_BORDER_FIX_ENABLED, Boolean.TRUE);  
							exporter.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);  
							exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);  
							exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);  
							exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
							exporter.exportReport();
							response = baos.toByteArray();
						}
			}

		} catch (Exception e) {
			throw new DataAccessException(e);
		} finally {
			this.closeStatement(rs, pstmt);
			this.closeConnection(c);
		}

		if (isEmpty) {
			// throw new EmptyReportException();
		}

		return response;
	}

	/**
	 * This method parse the query of a report and return a list of filter fields that was extracted from it.
	 */
	@SuppressWarnings("unchecked")
	public List getReportFields(String content) {
		List response = new ArrayList();
		String completeField = "";
		int iniIdx = 0;

		if (content != null) {
			iniIdx = content.indexOf("?#");
			while (iniIdx > 0) {
				int finIdx = content.indexOf("#", iniIdx + 2);
				if (finIdx > 0) {
					int nextIniIdx = content.indexOf("?#", iniIdx + 2);
					if (finIdx - 1 == nextIniIdx) {
						int newFinIdx = content.indexOf("!}(", finIdx + 2);
						if (newFinIdx > 1) {
							completeField = content.substring(iniIdx + 2, newFinIdx + 5);
							response.addAll(getReportFields(completeField));
						}
					}
					else {
						completeField = content.substring(iniIdx + 2, finIdx);
					}

					if (!completeField.equals("")) {
						int separator = completeField.indexOf("{");
						ReportFieldTO field = new ReportFieldTO();
						String value = null;
						if (separator < 0) {
							separator = completeField.length();
						}
						else {
							value = completeField.substring(separator + 1);
						}
						String param = completeField.substring(0, separator);
						field.setId(param.trim());
						if (value != null) {
							String type = null;
							int tsep = value.lastIndexOf("}(");
							if (tsep >= 0) {
								type = value.substring(tsep + 2, value.length() - 1);
								field.setReportFieldType(type);
								field.setLabel(value.substring(0, tsep).trim());
							}
							else {
								field.setLabel("");
							}
						}

						// define visibility of GUI fields
						// (note: the fields with key bellow must not be used into report filter GUI)
						if (("#" + field.getId() + "#").equalsIgnoreCase(ReportTO.PROJECT_ID)) {
							field.setVisible(false);
						}
						if (("#" + field.getId() + "#").equalsIgnoreCase(ReportTO.USER_ID)) {
							field.setVisible(false);
						}
						if (("#" + field.getId() + "#").equalsIgnoreCase(ReportTO.PROJECT_DESCENDANT)) {
							field.setVisible(false);
						}

						response.add(field);
					}

					completeField = "";
					iniIdx = content.indexOf("?#", finIdx + 1);
				}
				else {
					iniIdx = -1;
				}
			}
		}
		return response;
	}

	/**
	 * Perform the sql statement of kpi object and put the result into ReportResult object.
	 */
	private PreparedStatement prepareStatement(String sqlStatement, ReportTO rto, Connection c) throws DataAccessException {
		PreparedStatement pstmt = null;
		// ProjectDelegate pdel = new ProjectDelegate();
		try {
			String previousSql = new String(this.replaceStatement(sqlStatement, rto));
			String sql = this.replaceStatement(sqlStatement, rto);

			// remove the wildcards of sql statement
			sql = sql.replaceAll(ReportTO.PROJECT_ID, "");
			sql = sql.replaceAll(ReportTO.INITIAL_RANGE, "");
			sql = sql.replaceAll(ReportTO.FINAL_RANGE, "");
			sql = sql.replaceAll(ReportTO.USER_ID, "");

			// replace the project descendant key to a lista of projects id's
			/*int pd = sql.indexOf(ReportTO.PROJECT_DESCENDANT);
			if (pd > -1) {
				String inList = pdel.getProjectIn(rto.getProject().getId());
				sql = sql.substring(0, pd-1) + sql.substring(pd);
				sql = sql.replaceAll(ReportTO.PROJECT_DESCENDANT, inList);
			}*/

			// replace the specific report wildcards to empty
			if (rto.getFormFieldsValues() != null) {
				sql = this.removeKeywordsFromReport(sql, rto);
			}

			pstmt = c.prepareStatement(sql);

			// analyse if current PreparedStatememt object must be setted with values
			this.analyseStatement(previousSql, rto, pstmt);

		} catch (Exception e) {
			throw new DataAccessException(e);
		}

		return pstmt;
	}

	private String replaceStatement(String sql, ReportTO rto) throws BusinessException, SQLException {
		int i = 0;
		String token = "";
		String response = sql;

		while (i >= 0) {
			i = sql.indexOf("?", i);
			if (i > 0) {
				int endBlockIdx = sql.indexOf("#", i + 2);
				if (endBlockIdx > 0) {
					token = sql.substring(i + 1, endBlockIdx + 1);
					i = endBlockIdx;
					token = token.replaceAll("#", "");

					ReportFieldTO field = rto.getReportField(token);
					if (field != null && field.getReportFieldType() != null && field.getReportFieldType().equals(ReportFieldTO.TYPE_OBJECT)) {
						response = response.replaceAll("\\?#" + token + "#", field.getValue());
					}
				}
			}
		}

		return response;
	}

	/**
	 * Analyse the sql statement (with wildcards) and set into PreparedStatement the correnct values.
	 */
	private void analyseStatement(String sql, ReportTO rto, PreparedStatement pstmt) throws BusinessException, SQLException {
		// ReportBUS bus = new ReportBUS();
		int i = 0, cont = 1;
		String token = "";

		while (i >= 0) {
			i = sql.indexOf("?", i);
			if (i > 0) {
				int endBlockIdx = sql.indexOf("#", i + 2);
				if (endBlockIdx > 0) {
					token = sql.substring(i + 1, endBlockIdx + 1);
					i = endBlockIdx;

					if (token.equals(ReportTO.PROJECT_ID)) {
						// pstmt.setString(cont, rto.getProject().getId());
					}
					else
						if (token.equals(ReportTO.INITIAL_RANGE)) {
							// pstmt.setTimestamp(cont, bus.getRange(rto, true) );
						}
						else
							if (token.equals(ReportTO.FINAL_RANGE)) {
								// pstmt.setTimestamp(cont, bus.getRange(rto, false));
							}
							else
								if (token.equals(ReportTO.USER_ID)) {
									// pstmt.setString(cont, rto.getHandler().getId());

								}
								else {
									// also, search the token into the fields of report...
									token = token.replaceAll("#", "");

									ReportFieldTO field = rto.getReportField(token);
									if (field != null && field.getReportFieldType() != null && !field.getReportFieldType().equals(ReportFieldTO.TYPE_OBJECT)) {
										if (!rto.setValueIntoPreparedStatement(token, cont, pstmt, rto.getLocale())) {
											throw new BusinessException("The Token [" + token + "] of Report is unknown. It cannot be converted to a value.");
										}
									}
								}

					if (!("#" + token + "#").equals(ReportTO.PROJECT_DESCENDANT)) {
						cont++;
					}
				}
			}
		}
	}

	/**
	 * Remove the wildcards from sql statement.
	 */
	@SuppressWarnings("unchecked")
	private String removeKeywordsFromReport(String sql, ReportTO rto) {
		Iterator i = rto.getFormFieldsValues().iterator();
		while (i.hasNext()) {
			ReportFieldTO field = (ReportFieldTO) i.next();
			sql = sql.replaceAll("#" + field.getId() + "#", "");
		}
		return sql;
	}

}
