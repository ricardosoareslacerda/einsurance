package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

//import org.apache.log4j.Logger;

import br.com.tratomais.core.dao.IDaoMasterPolicy;
import br.com.tratomais.core.model.customer.Insurer;
import br.com.tratomais.core.model.policy.MasterPolicy;
import br.com.tratomais.core.service.IServiceInsurer;
import br.com.tratomais.core.service.IServiceMasterPolicy;
import br.com.tratomais.core.util.Constantes;

public class ServiceMasterPolicyImpl extends ServiceBean implements IServiceMasterPolicy{
//	private static final Logger logger = Logger.getLogger( ServiceMasterPolicyImpl.class );

	private IDaoMasterPolicy daoMasterPolicy;

	private IServiceInsurer serviceInsurer;
	
	public void setServiceInsurer(IServiceInsurer serviceInsurer) {
		this.serviceInsurer = serviceInsurer;
	}
	
	public void setDaoMasterPolicy(IDaoMasterPolicy daoMasterPolicy) {
		this.daoMasterPolicy = daoMasterPolicy;
	}	
	
	public MasterPolicy findById(Integer id) {
		return daoMasterPolicy.findById( id );
	}

	public List< MasterPolicy > findByName(String name) {
		return daoMasterPolicy.findByName(name);
	}

	public List< MasterPolicy > listAll() {
		return daoMasterPolicy.listAll();
	}

	public void deleteObject(MasterPolicy entity) {
		daoMasterPolicy.delete(entity);
	}

	public MasterPolicy saveObject(MasterPolicy masterPolicy) {
		
		if (masterPolicy.getInsurerId() == 0) {
			Insurer insurer = serviceInsurer.findById(Constantes.ID_INSURER_ZURICH);
			masterPolicy.setInsurerId(insurer.getInsurerId());
		}
		
		return daoMasterPolicy.save(masterPolicy);
	}

	public List< MasterPolicy > listAllWithDescriptions() {
		return daoMasterPolicy.listAllWithDescriptions();
	}

	@Override
	public List<MasterPolicy> listMasterPolicy(int insurerID, int partnerID, Date refDate) {
		return daoMasterPolicy.listMasterPolicy(insurerID, partnerID, refDate);
	}
}
