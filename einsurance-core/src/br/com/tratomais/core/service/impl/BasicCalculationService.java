package br.com.tratomais.core.service.impl;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import br.com.tratomais.core.dao.IDaoBroker;
import br.com.tratomais.core.dao.IDaoChannel;
import br.com.tratomais.core.dao.IDaoCoveragePlan;
import br.com.tratomais.core.dao.IDaoCoverageRelationship;
import br.com.tratomais.core.dao.IDaoDataOption;
import br.com.tratomais.core.dao.IDaoInsurer;
import br.com.tratomais.core.dao.IDaoMasterPolicy;
import br.com.tratomais.core.dao.IDaoPartner;
import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.dao.IDaoProduct;
import br.com.tratomais.core.dao.IDaoProductVersion;
import br.com.tratomais.core.dao.IDaoSubsidiary;
import br.com.tratomais.core.dao.impl.DaoCalculo;
import br.com.tratomais.core.dao.impl.DaoHistoryPendency;
import br.com.tratomais.core.dao.impl.DaoLockPendency;
import br.com.tratomais.core.dao.impl.DaoPayment;
import br.com.tratomais.core.exception.CalculusServiceException;
import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.info.Calculus;
import br.com.tratomais.core.infrastructure.AuthenticationHelper;
import br.com.tratomais.core.model.Application;
import br.com.tratomais.core.model.ApplicationProperty;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.auto.AutoRefer;
import br.com.tratomais.core.model.auto.AutoVersion;
import br.com.tratomais.core.model.customer.Address;
import br.com.tratomais.core.model.customer.Broker;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Customer;
import br.com.tratomais.core.model.customer.Insurer;
import br.com.tratomais.core.model.customer.Partner;
import br.com.tratomais.core.model.customer.Phone;
import br.com.tratomais.core.model.customer.Subsidiary;
import br.com.tratomais.core.model.pendencies.LockPendency;
import br.com.tratomais.core.model.pendencies.LockPendencyHistory;
import br.com.tratomais.core.model.pendencies.LockPendencyId;
import br.com.tratomais.core.model.pendencies.Pendency;
import br.com.tratomais.core.model.policy.Beneficiary;
import br.com.tratomais.core.model.policy.CalcStep;
import br.com.tratomais.core.model.policy.Contract;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.Installment;
import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.policy.ItemAuto;
import br.com.tratomais.core.model.policy.ItemCoverage;
import br.com.tratomais.core.model.policy.ItemPersonalRisk;
import br.com.tratomais.core.model.policy.ItemPersonalRiskGroup;
import br.com.tratomais.core.model.policy.ItemProfile;
import br.com.tratomais.core.model.policy.ItemProperty;
import br.com.tratomais.core.model.policy.ItemPurchaseProtected;
import br.com.tratomais.core.model.policy.ItemRiskType;
import br.com.tratomais.core.model.policy.MasterPolicy;
import br.com.tratomais.core.model.policy.Motorist;
import br.com.tratomais.core.model.policy.ValueAndRate;
import br.com.tratomais.core.model.product.BillingMethod;
import br.com.tratomais.core.model.product.ConversionRate;
import br.com.tratomais.core.model.product.Coverage;
import br.com.tratomais.core.model.product.CoveragePlan;
import br.com.tratomais.core.model.product.CoverageRangeValue;
import br.com.tratomais.core.model.product.CoverageRelationship;
import br.com.tratomais.core.model.product.CoverageRule;
import br.com.tratomais.core.model.product.DataGroup;
import br.com.tratomais.core.model.product.DataOption;
import br.com.tratomais.core.model.product.Factor;
import br.com.tratomais.core.model.product.PaymentOption;
import br.com.tratomais.core.model.product.PaymentTerm;
import br.com.tratomais.core.model.product.PlanKinship;
import br.com.tratomais.core.model.product.Product;
import br.com.tratomais.core.model.product.ProductOption;
import br.com.tratomais.core.model.product.ProductPaymentTerm;
import br.com.tratomais.core.model.product.ProductPlan;
import br.com.tratomais.core.model.product.ProductTerm;
import br.com.tratomais.core.model.product.ProductVersion;
import br.com.tratomais.core.model.product.Rate;
import br.com.tratomais.core.model.product.Roadmap;
import br.com.tratomais.core.model.product.Term;
import br.com.tratomais.core.service.IServiceAuto;
import br.com.tratomais.core.service.IServiceCoverage;
import br.com.tratomais.core.service.IServiceRule;
import br.com.tratomais.core.service.erros.Erro;
import br.com.tratomais.core.service.erros.ErrorList;
import br.com.tratomais.core.service.erros.ErrorTypes;
import br.com.tratomais.core.util.DateUtilities;
import br.com.tratomais.core.util.DateUtilities.DiferencaData;
import br.com.tratomais.esb.collections.model.response.CollectionResponse;
import br.com.tratomais.esb.collections.model.response.CollectionResponseItem;
import br.com.tratomais.general.utilities.NumberUtilities;

/*
 * O SpringFramework injeta os MessageSourceAware
 */

/**
 * Basic calculations service
 * 
 * @author luiz.alberoni
 */
public class BasicCalculationService extends CalculationServiceAbs {
	/** Logger instance */
	private static final Logger logger = Logger.getLogger(BasicCalculationService.class);

	/** DAO de Calculo, injected by spring framework */
	private DaoCalculo daoCalculo;
	/** DAO de Produto, injected by spring framework */
	private IDaoProduct daoProduct;
	/** DAO ProductVersion, injected by spring framework */
	private IDaoProductVersion daoProductVersion;
	/** DAO CoveragePlan, injected by spring framework */
	private IDaoCoveragePlan daoCoveragePlan;
	/** DAO Insurer, injected by spring framework */
	private IDaoInsurer daoInsurer;
	/** DAO Master Policy, injected by spring framework */
	private IDaoMasterPolicy daoMasterPolicy;
	/** DAO Subsidiary, injected by spring framework */
	private IDaoSubsidiary daoSubsidiary;
	/** DAO Channel, injected by spring framework */
	private IDaoChannel daoChannel;
	/** DAO Broker, injected by spring framework */
	private IDaoBroker daoBroker;
	/** DAO Partner, injected by spring framework */
	private IDaoPartner daoPartner;	
	/** DAO Coverage Relationship */
	private IDaoCoverageRelationship daoCoverageRelationship;
	/** DAO Lock Pendency */
	private DaoLockPendency daoLockPendency;
	/** DAO History Pendency */
	private DaoHistoryPendency daoHistoryPendency;
	/** DAO Policy */
	private IDaoPolicy daoPolicy;
	private DaoPayment daoPayment;
	private IDaoDataOption daoDataOption;
	
	private IServiceRule serviceRule;
	
	private IServiceCoverage serviceCoverage;
	
	private IServiceAuto serviceAuto;

	// ================================================
	// D a o S e t t e r s
	/**
	 * Sets DAO for calculus
	 * 
	 * @param daoCalculo
	 */
	public void setDaoCalculo(DaoCalculo daoCalculo) {
		this.daoCalculo = daoCalculo;
	}

	public void setDaoProduct(IDaoProduct daoProduct) {
		this.daoProduct = daoProduct;
	}

	public void setDaoProductVersion(IDaoProductVersion daoProductVersion) {
		this.daoProductVersion = daoProductVersion;
	}
	
	/**
	 * Sets DAO for calculus
	 * 
	 * @param daoProductVersion
	 */
	public void setProductVersion(IDaoProductVersion daoProductVersion) {
		this.daoProductVersion = daoProductVersion;
	}

	/**
	 * Sets DAO for calculus
	 * 
	 * @param daoCoveragePlan
	 */
	public void setDaoCoveragePlan(IDaoCoveragePlan daoCoveragePlan) {
		this.daoCoveragePlan = daoCoveragePlan;
	}

	/**
	 * @return Insurer Instance
	 */
	public IDaoInsurer getDaoInsurer() {
		return daoInsurer;
	}
	
	/**
	 * Insurer setter
	 * 
	 * @param daoInsurer
	 *            Insurer
	 */
	public void setDaoInsurer(IDaoInsurer daoInsurer) {
		this.daoInsurer = daoInsurer;
	}	
	
	/**
	 * @return Master Policy Instance
	 */
	public IDaoMasterPolicy getDaoMasterPolicy() {
		return daoMasterPolicy;
	}

	/**
	 * Master Policy setter
	 * 
	 * @param daoMasterPolicy
	 *            Master Policy
	 */
	public void setDaoMasterPolicy(IDaoMasterPolicy daoMasterPolicy) {
		this.daoMasterPolicy = daoMasterPolicy;
	}
	
	/**
	 * @return Subsidiary Instance
	 */
	public IDaoSubsidiary getDaoSubsidiary() {
		return daoSubsidiary;
	}
	
	/**
	 * Subsidiary setter
	 * 
	 * @param daoSubsidiary
	 *            Subsidiary
	 */
	public void setDaoSubsidiary(IDaoSubsidiary daoSubsidiary) {
		this.daoSubsidiary = daoSubsidiary;
	}
	
	/**
	 * @return Channel Instance
	 */
	public IDaoChannel getDaoChannel() {
		return daoChannel;
	}
	
	/**
	 * Channel setter
	 * 
	 * @param daoChannel
	 *            Channel
	 */
	public void setDaoChannel(IDaoChannel daoChannel) {
		this.daoChannel = daoChannel;
	}
	
	/**
	 * @return Broker Instance
	 */
	public IDaoBroker getDaoBroker() {
		return daoBroker;
	}
	
	/**
	 * Broker setter
	 * 
	 * @param daoBroker
	 *            Broker
	 */
	public void setDaoBroker(IDaoBroker daoBroker) {
		this.daoBroker = daoBroker;
	}	
	
	/**
	 * @return Partner Instance
	 */
	public IDaoPartner getDaoPartner() {
		return daoPartner;
	}
	
	/**
	 * Partner setter
	 * 
	 * @param daoPartner
	 *            Partner
	 */
	public void setDaoPartner(IDaoPartner daoPartner) {
		this.daoPartner = daoPartner;
	}
	
	public DaoLockPendency getDaoLockPendency(){
		return this.daoLockPendency;
	}
	
	public void setDaoLockPendency(DaoLockPendency daoLockPendency){
		this.daoLockPendency = daoLockPendency;
	}
	
	public void setDaoHistoryPendency(DaoHistoryPendency daoHistoryPendency) {
		this.daoHistoryPendency = daoHistoryPendency;
	}
	public DaoHistoryPendency getDaoHistoryPendency() {
		return this.daoHistoryPendency;
	}
	
	public IDaoPolicy getDaoPolicy() {
		return this.daoPolicy;
	}
	
	public void setDaoPolicy(IDaoPolicy daoPolicy){
		this.daoPolicy = daoPolicy;
	}

	
	public void setDaoPayment(DaoPayment daoPayment){
		this.daoPayment = daoPayment;
	}
	
	public DaoPayment getDaoPayment() {
		return this.daoPayment;
	}
	
	public void setDaoDataOption( IDaoDataOption daoDataOption ){
		this.daoDataOption = daoDataOption;
	}
	
	/**
	 * @return the serviceRule
	 */
	public IServiceRule getServiceRule() {
		return serviceRule;
	}

	/**
	 * @param serviceRule the serviceRule to set
	 */
	public void setServiceRule(IServiceRule serviceRule) {
		this.serviceRule = serviceRule;
	}

	public void setServiceCoverage(IServiceCoverage serviceCoverage) {
		this.serviceCoverage = serviceCoverage;
	}

	public void setServiceAuto(IServiceAuto serviceAuto) {
		this.serviceAuto = serviceAuto;
	}
	
	public void setDaoCoverageRelationship(IDaoCoverageRelationship daoCoverageRelationship){
		this.daoCoverageRelationship = daoCoverageRelationship;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.tratomais.core.service.impl.CalculationServiceAbs#calculate(br.com.tratomais.core.model.policy.Endorsement)
	 */
	@Override
	@SuppressWarnings("deprecation")
	protected void calculate(Endorsement endorsement) throws ServiceException {
		logger.trace("Starting the calculate process");

		final Contract contract = endorsement.getContract();

		// Reference date of the calculation
		Date refDate = endorsement.getReferenceDate();

		// Product effective for calculating
		ProductVersion productVersion = daoProductVersion.getProductVersionByRefDate(contract.getProductId(), refDate);
		
		// Get value/factor old issuance Endorsement
		ValueAndRate issuanceFactor = this.getIssuanceFactor(endorsement);
		
		// Get application property
		ApplicationProperty propertyReversalCommission = daoPolicy.loadApplicationProperty(1, "ReversalCommission");
		
		// Order the Item
		TreeSet<Item> treeItem = orderItem(endorsement.getItems());
		
		// Calculate process Item
		for (Item item : treeItem) {
			logger.debug("Starting the calculate process item" + item.getId());
			
			// Order the coverage
			TreeSet<ItemCoverage> treeItemCoverage = orderItemCoverage(item.getItemCoverages());
			
			// Set the Transient values
			switch(item.getObjectId()){
				case Item.OBJECT_CAPITAL:
					ItemProperty itemProperty = (ItemProperty) item;
					DataOption dataOption = daoDataOption.getDataOption(itemProperty.getActivityType());
					if(dataOption != null && dataOption.getRelated() != null){
						item.setTTariffType(dataOption.getRelated().getDataOptionId());
					}
					break;
				case Item.OBJECT_VIDA:
				case Item.OBJECT_PURCHASE_PROTECTED:
					if (endorsement.getPaymentTermId() != null) {
						PaymentTerm paymentTerm = daoCalculo.getPaymentTerm(endorsement.getPaymentTermId());
						if (paymentTerm != null) {
							item.setTPaymentTermMultipleType(paymentTerm.getPaymentTermMultipleType());
						}
					}
					break;
			}
			
			// Calculate process Coverage
			for(ItemCoverage itemCoverage : treeItemCoverage) {
				logger.debug("Starting the calculate process coverage" + itemCoverage.getId());
				
				// Coverage Plan effective for calculating
				CoveragePlan coveragePlan = daoCoveragePlan.getCoveragePlanByRefDate(contract.getProductId(), item.getCoveragePlanId(), itemCoverage.getId().getCoverageId(), refDate);
				
				// Clean steps the calc of the roadmap coverage
				itemCoverage.setCalcSteps(new HashSet<CalcStep>());
				
				// Clean Premium of the coverage
				item.limpaPremios();

				// Validate rules values of the coverage: No Value 
				if (coveragePlan.getInsuredValueType() == CoveragePlan.INSURED_VALUE_TYPE_SSA) {
					itemCoverage.setInsuredValue(0.0);
				}
				
				// Set the Transient values
				item.setTCoverageId(itemCoverage.getId().getCoverageId());
				item.setTRangeValueId(itemCoverage.getRangeValueId());
				item.setTDeductibleId(itemCoverage.getDeductibleId() == null? 0 : itemCoverage.getDeductibleId());
				item.setTRiskType(itemCoverage.getGroupCoverageId() == null? 0 : itemCoverage.getGroupCoverageId());
				item.setTRangeFractionId(null);
				
				// Set the Default values
				double workValue = itemCoverage.getInsuredValue();
				double workRate = 0.0;
				double finalRate = 1.0;
				
				logger.debug("Start value: " + workValue);
				ValueAndRate applyFactor = null;
				
				//:: Roadmap: Start Value
				logger.debug("Roadmap the Start Insured Value");
				applyFactor = applyMultipleFactor(Roadmap.ROADMAP_TYPE_START, Factor.FACTOR_TYPE_TECHNICAL, item, itemCoverage, productVersion, coveragePlan, refDate, workValue);
				workRate = applyFactor.getRate();
				workValue = applyFactor.getFinalValue();
				itemCoverage.addCalcStep(Roadmap.ROADMAP_TYPE_START, "INSURED VALUE", itemCoverage.getInsuredValue(), workValue);
				logger.debug(String.format("Insured Value - start: \"{0}\", end: \"{1}\"", new Object[]{itemCoverage.getInsuredValue(), workValue}));
				
				// Set the adjusts Insured Value by...
				switch(coveragePlan.getInsuredValueBy()){
					case CoveragePlan.INSURED_VALUE_BY_PSG:
						// Validate the object items
						switch (item.getObjectId()) {
							case Item.OBJECT_AUTO:
								ItemAuto itemAuto = (ItemAuto)item;
								double passengerNumber = itemAuto.getPassengerNumber().doubleValue();
								
								itemCoverage.addCalcStep(null, Roadmap.ROADMAP_TYPE_START, null, "FixedPoint: by Passenger", workValue, passengerNumber, null, null, (workValue * passengerNumber));
								logger.debug(String.format("Insured Value by Passenger - start: \"{0}\", end: \"{1}\"", new Object[]{workValue, (workValue * passengerNumber)}));
								workValue = (workValue * passengerNumber);
								break;
						}
						break;
				}
				
				//:: Roadmap: Pure Premium
				logger.debug("Roadmap the calculate the Pure Premium");
				finalRate = 1.0;
				
				// Fractionating insured value
				List <CoverageRangeValue> coverageFractionValue = daoProduct.listCoverageFractionValueByInsuredValue(contract.getProductId(), item.getCoveragePlanId(), item.getTCoverageId(), workValue, refDate);
				if (coverageFractionValue != null && coverageFractionValue.size() > 0) {
					double fractionValue = workValue;
					double rangeValue = 0.0;
					workValue = 0.0;
					for(CoverageRangeValue coverageRangeValue : coverageFractionValue) {
						// Set the Transient values
						item.setTRangeFractionId(coverageRangeValue.getId().getRangeId());
						// Check if is for fractionate
						rangeValue = fractionValue;
						if ((coverageRangeValue.getRangeValue() != 0) && (coverageRangeValue.getRangeValue() < fractionValue)) {
							fractionValue = (fractionValue - coverageRangeValue.getRangeValue());
							rangeValue = coverageRangeValue.getRangeValue();
						}
						applyFactor = applyMultipleFactor(Roadmap.ROADMAP_TYPE_PURE_PREMIUM, Factor.FACTOR_TYPE_TECHNICAL, item, itemCoverage, productVersion, coveragePlan, refDate, rangeValue);
						workValue = (workValue + applyFactor.getFinalValue());
					}
					item.setTRangeFractionId(null);
				}
				else {
					// Apply factors default
					applyFactor = applyMultipleFactor(Roadmap.ROADMAP_TYPE_PURE_PREMIUM, Factor.FACTOR_TYPE_TECHNICAL, item, itemCoverage, productVersion, coveragePlan, refDate, workValue);
					workRate = applyFactor.getRate();
					workValue = applyFactor.getFinalValue();
					finalRate = NumberUtilities.roundDecimalPlaces(finalRate * workRate, Calculus.RATE_DECIMAL_PLACES);
					logger.debug(String.format("Pure Premium (Factor Technical) - rate: \"{0}\", value: \"{1}\"", new Object[]{workRate, workValue}));
				}
				
				// Fixed factor: Prazo de vigencia the contract (pro-rata)
				applyFactor = applyIssuanceTermFactor(Roadmap.ROADMAP_TYPE_PURE_PREMIUM, item, itemCoverage, workValue, issuanceFactor);
				workRate = applyFactor.getRate();
				workValue = applyFactor.getFinalValue();
				logger.debug(String.format("Pure Premium (Factor Term) - rate: \"{0}\", value: \"{1}\"", new Object[]{workRate, workValue}));
				
				itemCoverage.setPureRate(NumberUtilities.roundDecimalPlaces(finalRate, Calculus.RATE_DECIMAL_PLACES));
				itemCoverage.setPurePremium(NumberUtilities.roundDecimalPlaces(workValue, Calculus.RESULT_DECIMAL_PLACES));
				
				itemCoverage.addCalcStep(Roadmap.ROADMAP_TYPE_PURE_PREMIUM, "PURE PREMIUM", 0.0, itemCoverage.getPurePremium());
				logger.debug(String.format("PURE PREMIUM - rate: \"{0}\", value: \"{1}\"", new Object[]{finalRate, workValue}));
				
				//:: Roadmap: Tariff Premium
				logger.debug("Roadmap the calculate the Tariff Premium");
				finalRate = 1.0;
				applyFactor = applyMultipleFactor(Roadmap.ROADMAP_TYPE_TARIFF_PREMIUM, Factor.FACTOR_TYPE_TECHNICAL, item, itemCoverage, productVersion, coveragePlan, refDate, workValue);
				workRate = applyFactor.getRate();
				workValue = applyFactor.getFinalValue();
				finalRate = NumberUtilities.roundDecimalPlaces(finalRate * workRate, Calculus.RATE_DECIMAL_PLACES);
				logger.debug(String.format("Tariff Premium (Factor Technical) - rate: \"{0}\", value: \"{1}\"", new Object[]{workRate, workValue}));

				itemCoverage.setTariffRate(NumberUtilities.roundDecimalPlaces(finalRate, Calculus.RATE_DECIMAL_PLACES));
				itemCoverage.setTariffPremium(NumberUtilities.roundDecimalPlaces(workValue, Calculus.RESULT_DECIMAL_PLACES));
				
				itemCoverage.addCalcStep(Roadmap.ROADMAP_TYPE_TARIFF_PREMIUM, "TARIFF PREMIUM", itemCoverage.getPurePremium(), itemCoverage.getTariffPremium());
				logger.debug(String.format("TARIFF PREMIUM - rate: \"{0}\", value: \"{1}\"", new Object[]{finalRate, workValue}));
		
				//:: Roadmap: Commercial Premium
				logger.debug("Roadmap the calculate the Commercial Premium");
				finalRate = 1.0;
				applyFactor = applyMultipleFactor(Roadmap.ROADMAP_TYPE_COMMERCIAL_PREMIUM, Factor.FACTOR_TYPE_TECHNICAL, item, itemCoverage, productVersion, coveragePlan, refDate, workValue);
				workRate = applyFactor.getRate();
				workValue = applyFactor.getFinalValue();
				finalRate = NumberUtilities.roundDecimalPlaces(finalRate * workRate, Calculus.RATE_DECIMAL_PLACES);
				logger.debug(String.format("Commercial Premium (Factor Technical) - rate: \"{0}\", value: \"{1}\"", new Object[]{workRate, workValue}));
				
				applyFactor = applyMultipleFactor(Roadmap.ROADMAP_TYPE_COMMERCIAL_PREMIUM, Factor.FACTOR_TYPE_COMMERCIAL, item, itemCoverage, productVersion, coveragePlan, refDate, workValue);
				workRate = applyFactor.getRate();
				workValue = applyFactor.getFinalValue();
				finalRate = NumberUtilities.roundDecimalPlaces(finalRate * workRate, Calculus.RATE_DECIMAL_PLACES);
				logger.debug(String.format("Commercial Premium (Factor Commercial) - rate: \"{0}\", value: \"{1}\"", new Object[]{workRate, workValue}));
				
				applyFactor = applyMultipleFactor(Roadmap.ROADMAP_TYPE_COMMERCIAL_PREMIUM, Factor.FACTOR_TYPE_SPENDING, item, itemCoverage, productVersion, coveragePlan, refDate, workValue);
				workRate = applyFactor.getRate();
				workValue = (applyFactor.getFinalValue() / (1 - workRate)); //Rule:(TC = TP/(1-G))
				logger.debug(String.format("Commercial Premium (Factor Spending) - rate: \"{0}\", value: \"{1}\"", new Object[]{workRate, workValue}));
				
				// Fixed point: Spending Rate
				itemCoverage.setSpendingRate(NumberUtilities.roundDecimalPlaces(workRate, Calculus.RATE_DECIMAL_PLACES));
				itemCoverage.addCalcStep(null, Roadmap.ROADMAP_TYPE_COMMERCIAL_PREMIUM, Factor.FACTOR_TYPE_SPENDING, "(TG): Total Spending", applyFactor.getInputValue(), applyFactor.getRate(), applyFactor.getFixedValue(), null, workValue);
				
				itemCoverage.setCommercialRate(NumberUtilities.roundDecimalPlaces(finalRate, Calculus.RATE_DECIMAL_PLACES));
				itemCoverage.setCommercialPremium(NumberUtilities.roundDecimalPlaces(workValue, Calculus.RESULT_DECIMAL_PLACES));

				itemCoverage.addCalcStep(Roadmap.ROADMAP_TYPE_COMMERCIAL_PREMIUM, "COMMERCIAL PREMIUM", itemCoverage.getTariffPremium(), itemCoverage.getCommercialPremium());
				logger.debug(String.format("COMMERCIAL PREMIUM - rate: \"{0}\", value: \"{1}\"", new Object[]{finalRate, workValue}));
				
				//:: Roadmap: Profit Premium
				logger.debug("Roadmap the calculate the Profit Premium");
				finalRate = 1.0;
				applyFactor = applyMultipleFactor(Roadmap.ROADMAP_TYPE_PROFIT_PREMIUM, Factor.FACTOR_TYPE_TECHNICAL, item, itemCoverage, productVersion, coveragePlan, refDate, workValue);
				workRate = applyFactor.getRate();
				workValue = applyFactor.getFinalValue();
				finalRate = NumberUtilities.roundDecimalPlaces(finalRate * workRate, Calculus.RATE_DECIMAL_PLACES);
				logger.debug(String.format("Profit Premium (Factor Technical) - rate: \"{0}\", value: \"{1}\"", new Object[]{workRate, workValue}));
				
				applyFactor = applyMultipleFactor(Roadmap.ROADMAP_TYPE_PROFIT_PREMIUM, Factor.FACTOR_TYPE_COMMERCIAL, item, itemCoverage, productVersion, coveragePlan, refDate, workValue);
				workRate = applyFactor.getRate();
				workValue = applyFactor.getFinalValue();
				finalRate = NumberUtilities.roundDecimalPlaces(finalRate * workRate, Calculus.RATE_DECIMAL_PLACES);
				logger.debug(String.format("Profit Premium (Factor Commercial) - rate: \"{0}\", value: \"{1}\"", new Object[]{workRate, workValue}));				
				
				itemCoverage.setProfitRate(NumberUtilities.roundDecimalPlaces(finalRate, Calculus.RATE_DECIMAL_PLACES));
				itemCoverage.setProfitPremium(NumberUtilities.roundDecimalPlaces(workValue, Calculus.RESULT_DECIMAL_PLACES));
				
				itemCoverage.addCalcStep(Roadmap.ROADMAP_TYPE_PROFIT_PREMIUM, "PROFIT PREMIUM", itemCoverage.getCommercialPremium(), itemCoverage.getProfitPremium());
				logger.debug(String.format("PROFIT PREMIUM - rate: \"{0}\", value: \"{1}\"", new Object[]{finalRate, workValue}));
				
				//:: Roadmap: Net Premium
				logger.debug("Roadmap the calculate the Net Premium");
				finalRate = 1.0;
				applyFactor = applyMultipleFactor(Roadmap.ROADMAP_TYPE_NET_PREMIUM, Factor.FACTOR_TYPE_TECHNICAL, item, itemCoverage, productVersion, coveragePlan, refDate, workValue);
				workRate = applyFactor.getRate();
				workValue = applyFactor.getFinalValue();
				finalRate = NumberUtilities.roundDecimalPlaces(finalRate * workRate, Calculus.RATE_DECIMAL_PLACES);
				logger.debug(String.format("Net Premium (Factor Technical) - rate: \"{0}\", value: \"{1}\"", new Object[]{workRate, workValue}));
				
				applyFactor = applyMultipleFactor(Roadmap.ROADMAP_TYPE_NET_PREMIUM, Factor.FACTOR_TYPE_COMMERCIAL, item, itemCoverage, productVersion, coveragePlan, refDate, workValue);
				workRate = applyFactor.getRate();
				workValue = applyFactor.getFinalValue();
				finalRate = NumberUtilities.roundDecimalPlaces(finalRate * workRate, Calculus.RATE_DECIMAL_PLACES);
				logger.debug(String.format("Net Premium (Factor Commercial) - rate: \"{0}\", value: \"{1}\"", new Object[]{workRate, workValue}));
				
				// Fixed point: Technical Premium
				//itemCoverage.setTechnicalRate(NumberUtilities.roundDecimalPlaces(finalRate, Calculus.RATE_DECIMAL_PLACES));
				itemCoverage.setTechnicalPremium(NumberUtilities.roundDecimalPlaces(workValue, Calculus.RESULT_DECIMAL_PLACES));
				
				itemCoverage.addCalcStep(Roadmap.ROADMAP_TYPE_NET_PREMIUM, "TECHNICAL PREMIUM", itemCoverage.getProfitPremium(), itemCoverage.getTechnicalPremium());
				logger.debug(String.format("TECHNICAL PREMIUM - rate: \"{0}\", value: \"{1}\"", new Object[]{finalRate, workValue}));
				
				// Fixed point: Factor Endorsement
				applyFactor = applyIssuanceFactor(item, itemCoverage, productVersion, coveragePlan, refDate, workValue, issuanceFactor);
				workRate = applyFactor.getRate();
				workValue = applyFactor.getFinalValue();
				finalRate = NumberUtilities.roundDecimalPlaces(finalRate * workRate, Calculus.RATE_DECIMAL_PLACES);
				logger.debug(String.format("Net Premium (Factor Endorsement) - rate: \"{0}\", value: \"{1}\"", new Object[]{workRate, workValue}));
				
				// Fixed point: Net Premium
				itemCoverage.setNetRate(NumberUtilities.roundDecimalPlaces(finalRate, Calculus.RESULT_DECIMAL_PLACES));
				itemCoverage.setNetPremium(NumberUtilities.roundDecimalPlaces(workValue, Calculus.RESULT_DECIMAL_PLACES));
				
				itemCoverage.addCalcStep(Roadmap.ROADMAP_TYPE_NET_PREMIUM, "NET PREMIUM", itemCoverage.getProfitPremium(), itemCoverage.getNetPremium());
				logger.debug(String.format("NET PREMIUM - rate: \"{0}\", value: \"{1}\"", new Object[]{finalRate, workValue}));
				
				// Fixed point: Retained Premium
				this.applyRetainedValue(item, itemCoverage, workValue, issuanceFactor);
				
				//:: Roadmap: Calculate Commission
				// Fixed point: Commission
				itemCoverage.setCommission(NumberUtilities.roundDecimalPlaces((itemCoverage.getNetPremium() * itemCoverage.getCommissionFactor()), Calculus.RESULT_DECIMAL_PLACES));
				itemCoverage.addCalcStep(null, Roadmap.ROADMAP_TYPE_NET_PREMIUM, Factor.FACTOR_TYPE_FIX, "FixedPoint: Commission", itemCoverage.getNetPremium(), itemCoverage.getCommissionFactor(), null, null, itemCoverage.getCommission());
				
				// Fixed point: Labour
				itemCoverage.setLabour(NumberUtilities.roundDecimalPlaces((itemCoverage.getNetPremium() * itemCoverage.getLabourFactor()), Calculus.RESULT_DECIMAL_PLACES));
				itemCoverage.addCalcStep(null, Roadmap.ROADMAP_TYPE_NET_PREMIUM, Factor.FACTOR_TYPE_FIX, "FixedPoint: Labour", itemCoverage.getNetPremium(), itemCoverage.getLabourFactor(), null, null, itemCoverage.getLabour());
				
				// Fixed point: Agency
				itemCoverage.setAgency(NumberUtilities.roundDecimalPlaces((itemCoverage.getNetPremium() * itemCoverage.getAgencyFactor()), Calculus.RESULT_DECIMAL_PLACES));
				itemCoverage.addCalcStep(null, Roadmap.ROADMAP_TYPE_NET_PREMIUM, Factor.FACTOR_TYPE_FIX, "FixedPoint: Agency", itemCoverage.getNetPremium(), itemCoverage.getAgencyFactor(), null, null, itemCoverage.getAgency());
				
				// Fixed point: Check if Reversal Commission when negative
				if (workValue < 0) {
					if ((endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL) ||
						(endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_INSURED)) {	
						if (propertyReversalCommission != null && Boolean.parseBoolean(propertyReversalCommission.getValue()) && !itemCoverage.getCommission().equals(0.0)) {
							workValue = NumberUtilities.roundDecimalPlaces((workValue + (itemCoverage.getCommission() * -1)), Calculus.RESULT_DECIMAL_PLACES);
							
							//Set Retained Premium if Reversal Commission
							endorsement.setRetainedPremium(NumberUtilities.roundDecimalPlaces(endorsement.getRetainedPremium() + (itemCoverage.getCommission() * -1), Calculus.RESULT_DECIMAL_PLACES));
							endorsement.setUnearnedPremium(NumberUtilities.roundDecimalPlaces(endorsement.getUnearnedPremium() + (itemCoverage.getCommission() * -1), Calculus.RESULT_DECIMAL_PLACES));
							
							// Fixed point: Reversal Commission
							itemCoverage.addCalcStep(null, Roadmap.ROADMAP_TYPE_NET_PREMIUM, Factor.FACTOR_TYPE_FIX, "FixedPoint: Reversal Commission", itemCoverage.getNetPremium(), null, (itemCoverage.getCommission() * -1), null, workValue);
							itemCoverage.setCommission(0.0);
							
							// Fixed point: Net Premium
							itemCoverage.setNetPremium(NumberUtilities.roundDecimalPlaces(workValue, Calculus.RESULT_DECIMAL_PLACES));
							itemCoverage.addCalcStep(Roadmap.ROADMAP_TYPE_NET_PREMIUM, "NET PREMIUM", itemCoverage.getProfitPremium(), itemCoverage.getNetPremium());
						}
					}
				}
				
				//:: Roadmap: Total Premium
				logger.debug("Roadmap the calculate the Total Premium");
				finalRate = 1.0;
				applyFactor = applyMultipleFactor(Roadmap.ROADMAP_TYPE_TOTAL_PREMIUM, Factor.FACTOR_TYPE_TECHNICAL, item, itemCoverage, productVersion, coveragePlan, refDate, workValue);
				workRate = applyFactor.getRate();
				workValue = applyFactor.getFinalValue();
				finalRate = NumberUtilities.roundDecimalPlaces(finalRate * workRate, Calculus.RATE_DECIMAL_PLACES);
				logger.debug(String.format("Total Premium (Factor Technical) - rate: \"{0}\", value: \"{1}\"", new Object[]{workRate, workValue}));
				
				applyFactor = applyMultipleFactor(Roadmap.ROADMAP_TYPE_TOTAL_PREMIUM, Factor.FACTOR_TYPE_COMMERCIAL, item, itemCoverage, productVersion, coveragePlan, refDate, workValue);
				workRate = applyFactor.getRate();
				workValue = applyFactor.getFinalValue();
				finalRate = NumberUtilities.roundDecimalPlaces(finalRate * workRate, Calculus.RATE_DECIMAL_PLACES);
				logger.debug(String.format("Total Premium (Factor Commercial) - rate: \"{0}\", value: \"{1}\"", new Object[]{workRate, workValue}));
				
				// Fixed point: Financing
				applyFactor = applyMultipleFactor(Roadmap.ROADMAP_TYPE_TOTAL_PREMIUM, Factor.FACTOR_TYPE_FINANCING, item, itemCoverage, productVersion, coveragePlan, refDate, workValue);
				workRate = applyFactor.getRate();
				workValue = applyFactor.getFinalValue();
				finalRate = NumberUtilities.roundDecimalPlaces(finalRate * workRate, Calculus.RATE_DECIMAL_PLACES);
				itemCoverage.setFractioningAdditional(NumberUtilities.roundDecimalPlaces((applyFactor.getFinalValue() - applyFactor.getInputValue()), Calculus.RESULT_DECIMAL_PLACES));
				itemCoverage.addCalcStep(Roadmap.ROADMAP_TYPE_TOTAL_PREMIUM, "FixedPoint: Financing Value", applyFactor.getInputValue(), itemCoverage.getFractioningAdditional());
				logger.debug(String.format("Total Premium (Factor Financing) - rate: \"{0}\", value: \"{1}\"", new Object[]{workRate, itemCoverage.getFractioningAdditional()}));
				
				// Fixed point: Tribute
				applyFactor = applyMultipleFactor(Roadmap.ROADMAP_TYPE_TOTAL_PREMIUM, Factor.FACTOR_TYPE_TRIBUTE, item, itemCoverage, productVersion, coveragePlan, refDate, workValue);
				workRate = applyFactor.getRate();
				workValue = applyFactor.getFinalValue();
				itemCoverage.setTaxIncidence(workRate != 1.0);
				finalRate = NumberUtilities.roundDecimalPlaces(finalRate * workRate, Calculus.RATE_DECIMAL_PLACES);
				itemCoverage.setTaxValue(NumberUtilities.roundDecimalPlaces((applyFactor.getFinalValue() - applyFactor.getInputValue()), Calculus.RESULT_DECIMAL_PLACES));
				itemCoverage.addCalcStep(Roadmap.ROADMAP_TYPE_TOTAL_PREMIUM, "FixedPoint: Tribute Value", applyFactor.getInputValue(), itemCoverage.getTaxValue());
				logger.debug(String.format("Total Premium (Factor Tribute) - rate: \"{0}\", value: \"{1}\"", new Object[]{workRate, itemCoverage.getTaxValue()}));
				
				// Fixed point: Tax Rate
				if (itemCoverage.getTaxIncidence())
					endorsement.setTaxRate(NumberUtilities.roundDecimalPlaces(workRate, Calculus.RATE_DECIMAL_PLACES));
				
				// Fixed point: Total Premium
				//itemCoverage.setTotalRate(NumberUtilities.roundDecimalPlaces(finalRate, Calculus.RESULT_DECIMAL_PLACES));
				itemCoverage.setTotalPremium(NumberUtilities.roundDecimalPlaces(workValue, Calculus.RESULT_DECIMAL_PLACES));
				
				itemCoverage.addCalcStep(Roadmap.ROADMAP_TYPE_TOTAL_PREMIUM, "TOTAL PREMIUM", itemCoverage.getNetPremium(), itemCoverage.getTotalPremium());
				logger.debug(String.format("TOTAL PREMIUM - rate: \"{0}\", value: \"{1}\"", new Object[]{finalRate, workValue}));
				
				logger.debug(String.format("End value: \"{0}\"", new Object[]{workValue}));
				
				logger.debug(String.format("Ending the calculate process coverage: \"{0}\"", new Object[]{itemCoverage.getId()}));
			}
			
			// Fixed point: Analysis Status
			item.setCalculationValid(true);
			if ((endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_EMISSION) ||
				(endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_REACTIVATION)){
				item.setItemStatus(Item.ITEM_STATUS_ACTIVE);
			}
			
			logger.debug(String.format("Ending the calculate process item: \"{0}\"", new Object[]{item.getId()}));
		}
		
		//:: Roadmap: Totalize
		endorsement.flush();
		endorsement.splitTotalValues();
		
		endorsement.setTaxRate((endorsement.getTaxRate() != null ? endorsement.getTaxRate() : 0.0));
		endorsement.setQuotationDate(new Date());
		endorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_COTACAO);
		
		//:: Roadmap: Readjustment
		if ( (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_REACTIVATION) ||
			((endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CLAIM) && (endorsement.getEndorsementType() == Endorsement.ENDORSEMENT_TYPE_COLLECTION)) ) {
			double differencePremium = ((issuanceFactor.getOldPremium() - issuanceFactor.getPaidPremium()) - endorsement.getTotalPremium());
			if (differencePremium != 0) {
				endorsement.setNetPremium(NumberUtilities.roundDecimalPlaces((endorsement.getNetPremium() + differencePremium), Calculus.RESULT_DECIMAL_PLACES));
				endorsement.setTotalPremium(NumberUtilities.roundDecimalPlaces((endorsement.getTotalPremium() + differencePremium), Calculus.RESULT_DECIMAL_PLACES));
				endorsement.splitInItem();
				endorsement.flush();
			}
		} else if (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL) {
			double differencePremium = NumberUtilities.roundDecimalPlaces(issuanceFactor.getResidualValue(), Calculus.RESULT_DECIMAL_PLACES);
			if (differencePremium != 0) {
				endorsement.setNetPremium(NumberUtilities.roundDecimalPlaces((endorsement.getNetPremium() + differencePremium), Calculus.RESULT_DECIMAL_PLACES));
				endorsement.setTotalPremium(NumberUtilities.roundDecimalPlaces((endorsement.getTotalPremium() + differencePremium), Calculus.RESULT_DECIMAL_PLACES));
				endorsement.splitInItem();
				endorsement.flush();
			}
		} else if ((endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_INSURED) ||
				   (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR)) {
			double differencePremium = (endorsement.getUnearnedPremium() - endorsement.getTotalPremium());
			if ((differencePremium != 0) && (endorsement.getUnearnedPremium() < 0)) {
				endorsement.setNetPremium(NumberUtilities.roundDecimalPlaces((endorsement.getNetPremium() + differencePremium), Calculus.RESULT_DECIMAL_PLACES));
				endorsement.setTotalPremium(NumberUtilities.roundDecimalPlaces((endorsement.getTotalPremium() + differencePremium), Calculus.RESULT_DECIMAL_PLACES));
				endorsement.splitInItem();
				endorsement.flush();
			}
		}
		
		//:: Roadmap: Totalize
		endorsement.splitTotalValues();
		
		// Fixed point: check Premium
		if (endorsement.getTotalPremium() < 0) {
			endorsement.setEndorsementType(Endorsement.ENDORSEMENT_TYPE_REFUND);
		} else if (endorsement.getTotalPremium() > 0) {
			endorsement.setEndorsementType(Endorsement.ENDORSEMENT_TYPE_COLLECTION);
		} else {
			endorsement.setEndorsementType(Endorsement.ENDORSEMENT_TYPE_NOT);
		}
		
		if (((endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_NO_PAYMENT_CANCEL) ||
			 (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_INSURED) ||
			 (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR)) &&
			 (endorsement.getUnearnedPremium() >= 0)) {
			endorsement.setEndorsementType(Endorsement.ENDORSEMENT_TYPE_NOT);
		}
		
		// Fixed point: check Policy
		if (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_EMISSION) {
			endorsement.getContract().setContractStatus(Contract.CONTRACT_STATUS_ACTIVE);
		}
		
		printKeys(endorsement);
		
		logger.trace("Ending the calculate process");
	}
	
	private ValueAndRate applyIssuanceTermFactor(int roadmapType, Item item, ItemCoverage itemCoverage, double inputValue, ValueAndRate issuanceFactor) throws ServiceException {
		logger.debug("Apply the factor issuance type");
		
		final Endorsement endorsement = item.getEndorsement();
		final Contract contract = endorsement.getContract();
		
		// Set default values
		int termId = contract.getTermId();
		Date effectiveDate = contract.getEffectiveDate();
		Date expiryDate = contract.getExpiryDate();
		
		switch (endorsement.getIssuanceType()) {
			case Endorsement.ISSUANCE_TYPE_NO_PAYMENT_CANCEL:
			case Endorsement.ISSUANCE_TYPE_POLICY_REACTIVATION:
			case Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR:
			case Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_INSURED:
			case Endorsement.ISSUANCE_TYPE_POLICY_CLAIM:
				termId = issuanceFactor.getEndorsedTermId();
				effectiveDate = issuanceFactor.getEndorsedEffectiveDate();
				expiryDate = issuanceFactor.getEndorsedExpiryDate();
				break;
				
			case Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_REGISTER:
			case Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL:
				termId = endorsement.getTermId();
				effectiveDate = endorsement.getEffectiveDate();
				expiryDate = endorsement.getExpiryDate();
				break;
		}
		
		return applyTermFactor(roadmapType, termId, effectiveDate, expiryDate, itemCoverage, inputValue);
	}
	
	private ValueAndRate applyTermFactor(int roadmapType, int termId, Date effectiveDate, Date expiryDate, ItemCoverage itemCoverage, double inputValue) throws ServiceException {
		logger.debug("Apply the fixed factor");
		
		double coefPeriodo = 1.0;
		double newValue = inputValue;
		
		// Get Term for calculation
		Term term = daoCalculo.getTermById(termId);
		switch (term.getMultipleType()) {
			case Term.MULTIPLE_TYPE_YEAR:
				coefPeriodo = (1.0 * term.getMultipleQuantity());
				break;
			case Term.MULTIPLE_TYPE_MONTH:
				coefPeriodo = (1.0 / 12 * term.getMultipleQuantity());
				break;
			case Term.MULTIPLE_TYPE_DAY:
				coefPeriodo = (DateUtilities.diffDateInDays(effectiveDate, expiryDate) * 1.0 / term.getMultipleQuantity());
				break;
			default:
				coefPeriodo = 1.0;
				break;
		}
		
		coefPeriodo = NumberUtilities.roundDecimalPlaces(coefPeriodo, Calculus.RATE_DECIMAL_PLACES);
		newValue = NumberUtilities.roundDecimalPlaces(newValue * coefPeriodo, Calculus.STEP_DECIMAL_PLACES);
		itemCoverage.addCalcStep(null, roadmapType, Factor.FACTOR_TYPE_FIX, "FixedPoint: (Term) - " + term.getName(), inputValue, coefPeriodo, 0.0, 0.0, newValue);
		
		return new ValueAndRate(inputValue, coefPeriodo, 0.0, newValue);
	}
	
	private ValueAndRate applyMultipleFactor(int roadmapType, int factorType, Item item, ItemCoverage itemCoverage, ProductVersion productVersion, CoveragePlan coveragePlan, Date refDate, double inputValue) throws ServiceException {
		logger.debug("Apply the factor type");
		
		final Endorsement endorsement = item.getEndorsement();
		double oldValue = inputValue;
		double newValue = inputValue;
		double minimumValue = 0.0;
		double finalFixedValue = 0.0;
		double finalRate = ((factorType == Factor.FACTOR_TYPE_SPENDING) ? 0.0 : 1.0);
		
		// Check issuance Type
		switch (endorsement.getIssuanceType()) {
			case Endorsement.ISSUANCE_TYPE_NO_PAYMENT_CANCEL:
			case Endorsement.ISSUANCE_TYPE_POLICY_REACTIVATION:
			case Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR:
			case Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_INSURED:
			case Endorsement.ISSUANCE_TYPE_POLICY_CLAIM:
			case Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_REGISTER:
				if (roadmapType == Roadmap.ROADMAP_TYPE_START) newValue = 0.0;
				return new ValueAndRate(inputValue, 0.0, 0.0, 0.0, newValue);
		}
		
		// Get factor list for roadmap
		List<Factor> factorList = daoCalculo.getFactorList(productVersion.getId().getProductId(), coveragePlan.getId().getCoverageId(), roadmapType, factorType, refDate);
		
		// Get rate or value for factor type
		for (Factor factor : factorList) {
			List<Rate> rateList = daoCalculo.getRateListByFactor(factor, item, refDate);
			if (rateList != null && rateList.size() > 0) {
				for (Rate rate : rateList) {
					if (rate != null) {
						oldValue = newValue;
						if (rate.getRateFactor() == 0 && (rate.getFixedValue() != null) && (rate.getFixedValue() != 0)) {
							newValue = NumberUtilities.roundDecimalPlaces(this.ApplyMathOperator(factor, oldValue, rate.getFixedValue()), Calculus.STEP_DECIMAL_PLACES);
							finalFixedValue = NumberUtilities.roundDecimalPlaces(rate.getFixedValue(), Calculus.STEP_DECIMAL_PLACES);
							itemCoverage.addCalcStep(factor.getFactorId(), roadmapType, factorType, factor.getDescription(), oldValue, 0.0, rate.getFixedValue(), 0.0, newValue);
							logger.debug(String.format("applyMultipleFactor(factor: \"{0}\", item: \"{1}\", refDate: \"{2}\") - value: \"{3}\"", new Object[]{factor, item, refDate, rate.getFixedValue()}));
						}
						else {
							if (factorType == Factor.FACTOR_TYPE_SPENDING) {
								rate.setRateFactor(NumberUtilities.roundDecimalPlaces(rate.getRateFactor(), Calculus.RATE_DECIMAL_PLACES));
								newValue = NumberUtilities.roundDecimalPlaces(oldValue + (rate.getFixedValue() != null ? rate.getFixedValue() : 0.0), Calculus.STEP_DECIMAL_PLACES);
								finalRate = NumberUtilities.roundDecimalPlaces(finalRate + rate.getRateFactor(), Calculus.RATE_DECIMAL_PLACES);
								itemCoverage.addCalcStep(factor.getFactorId(), roadmapType, factorType, factor.getDescription(), oldValue, rate.getRateFactor(), 0.0, 0.0, newValue);
								logger.debug(String.format("applyMultipleFactor(factor: \"{0}\", item: \"{1}\", refDate: \"{2}\") - factor: \"{3}\"", new Object[]{factor, item, refDate, rate.getRateFactor()}));
							}
							else {
								rate.setRateFactor(NumberUtilities.roundDecimalPlaces(rate.getRateFactor(), Calculus.RATE_DECIMAL_PLACES));
								newValue = NumberUtilities.roundDecimalPlaces(this.ApplyMathOperator(factor, oldValue, rate.getRateFactor()), Calculus.STEP_DECIMAL_PLACES);
								finalRate = NumberUtilities.roundDecimalPlaces(this.ApplyMathOperator(factor, finalRate, rate.getRateFactor()), Calculus.RATE_DECIMAL_PLACES);
								itemCoverage.addCalcStep(factor.getFactorId(), roadmapType, factorType, factor.getDescription(), oldValue, rate.getRateFactor(), 0.0, 0.0, newValue);
								logger.debug(String.format("applyMultipleFactor(factor: \"{0}\", item: \"{1}\", refDate: \"{2}\") - factor: \"{3}\"", new Object[]{factor, item, refDate, rate.getRateFactor()}));
							}
						}
						// Check minimum premium
						if ((rate.getMinimumValue() != null) && (rate.getMinimumValue() != 0)) {
							if (newValue < rate.getMinimumValue().doubleValue()) {
								newValue = NumberUtilities.roundDecimalPlaces(rate.getMinimumValue(), Calculus.STEP_DECIMAL_PLACES);
								minimumValue = NumberUtilities.roundDecimalPlaces(rate.getMinimumValue(), Calculus.STEP_DECIMAL_PLACES);
								itemCoverage.addCalcStep(factor.getFactorId(), roadmapType, factorType, factor.getDescription(), oldValue, 0.0, 0.0, rate.getMinimumValue(), newValue);
								logger.debug(String.format("applyMultipleFactor(factor: \"{0}\", item: \"{1}\", refDate: \"{2}\") - minimum: \"{3}\"", new Object[]{factor, item, refDate, rate.getMinimumValue()}));
							}
						}
					}
					else if (factor.isCompulsory()) { 
						logger.debug(String.format("Rate compulsory not found! (factor: \"{0}\", item: \"{1}\", refDate: \"{2}\")", new Object[]{factor, item, refDate}));
						endorsement.getErrorList().add(ErrorTypes.PROCESSING_RATE_NOT_FOUND, new Object[]{factor.getDescription(), itemCoverage.getCoverageName()}, BasicCalculationService.class, "", item.getId().getItemId());
						//throw new CalculusServiceException(String.format("Rate compulsory not found! (factor: \"{0}\", item: \"{1}\", refDate: \"{2}\")", new Object[]{factor, item, refDate}));
					}
				}
			}
			else if (factor.isCompulsory()) { 
				logger.debug(String.format("Rate compulsory not found! (factor: \"{0}\", item: \"{1}\", refDate: \"{2}\")", new Object[]{factor, item, refDate}));
				endorsement.getErrorList().add(ErrorTypes.PROCESSING_RATE_NOT_FOUND, new Object[]{factor.getDescription(), itemCoverage.getCoverageName()}, BasicCalculationService.class, "", item.getId().getItemId());
				//throw new CalculusServiceException(String.format("Rate compulsory not found! (factor: \"{0}\", item: \"{1}\", refDate: \"{2}\")", new Object[]{factor, item, refDate}));
			}
		}
		return new ValueAndRate(inputValue, finalRate, finalFixedValue, minimumValue, newValue);
	}

	private double ApplyMathOperator(Factor factor, double value1, double value2) {
		double retorno = 0d;
		
		switch (factor.getOperatorType()) {
			case Factor.OPERATOR_TYPE_MULTIPLICATION:
				retorno = (value1 * value2);
				break;
			case Factor.OPERATOR_TYPE_DIVISION:
				retorno = (value1 / value2);
				break;
			case Factor.OPERATOR_TYPE_ADDITION:
				retorno = (value1 + value2);
				break;
			case Factor.OPERATOR_TYPE_SUBTRACTION:
				retorno = (value1 - value2);
				break;
		}
		
		return retorno;
	}

	private ValueAndRate applyIssuanceFactor(Item item, ItemCoverage itemCoverage, ProductVersion productVersion, CoveragePlan coveragePlan, Date refDate, double inputValue, ValueAndRate issuanceFactor) throws ServiceException {
		logger.debug("Apply the factor issuance type");
		
		final Endorsement endorsement = item.getEndorsement();
		double oldValue = inputValue;
		double newValue = inputValue;
		double minimumValue = 0;
		double finalFixedValue = 0;
		double finalRate = 1;
		
		// Set old values
		double fixRate = 0.0;
		double fixValue = 0.0;
		
		// Consulting old coverage
		ItemCoverage oldItemCoverage = null;
		if (endorsement.getEndorsedId() != null && endorsement.getEndorsedId() != 0) {
			oldItemCoverage = daoCalculo.getItemCoverageById(itemCoverage.getId().getContractId(), issuanceFactor.getEndorsedId(), itemCoverage.getId().getItemId(), itemCoverage.getId().getCoverageId());
		}
		
		switch (endorsement.getIssuanceType()) {
			case Endorsement.ISSUANCE_TYPE_NO_PAYMENT_CANCEL:
				finalRate = NumberUtilities.roundDecimalPlaces(0.0, Calculus.RATE_DECIMAL_PLACES);
				newValue = NumberUtilities.roundDecimalPlaces(0.0, Calculus.STEP_DECIMAL_PLACES);
				itemCoverage.addCalcStep(null, Roadmap.ROADMAP_TYPE_NET_PREMIUM, Factor.FACTOR_TYPE_FIX, "FixedPoint: Factor of Cancellation Non Payment", oldValue, finalRate, 0.0, 0.0, newValue);
				break;
				
			case Endorsement.ISSUANCE_TYPE_POLICY_REACTIVATION:
				// Check old issuance values
				if (oldItemCoverage != null) {
					// Check refund or unpaid value
					fixRate = issuanceFactor.getUnEarnedFactor();
					
					// Apply fix factor/value
					fixValue = NumberUtilities.roundDecimalPlaces((oldItemCoverage.getTotalPremium() * fixRate), Calculus.RESULT_DECIMAL_PLACES);
					newValue = NumberUtilities.roundDecimalPlaces(fixValue, Calculus.STEP_DECIMAL_PLACES);
					itemCoverage.addCalcStep(null, Roadmap.ROADMAP_TYPE_NET_PREMIUM, Factor.FACTOR_TYPE_FIX, "FixedPoint: Factor of Reactivation", oldValue, 0.0, fixValue, 0.0, newValue);
				}				
				break;
				
			case Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR:
				// Check old issuance values
				if (oldItemCoverage != null) {
					// Check refund or unpaid value
					fixRate = (issuanceFactor.getPaidFactor() * -1.0);
					
					// Apply fix factor/value
					fixValue = NumberUtilities.roundDecimalPlaces((oldItemCoverage.getTotalPremium() * fixRate), Calculus.RESULT_DECIMAL_PLACES);
					newValue = NumberUtilities.roundDecimalPlaces(fixValue, Calculus.STEP_DECIMAL_PLACES);
					itemCoverage.addCalcStep(null, Roadmap.ROADMAP_TYPE_NET_PREMIUM, Factor.FACTOR_TYPE_FIX, "FixedPoint: Factor of Emission Error", oldValue, 0.0, fixValue, 0.0, newValue);
				}
				break;
				
			case Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_INSURED:
				// Check old issuance values
				if (oldItemCoverage != null) {
					// Check refund or unpaid value
					if (issuanceFactor.getUnPaidFactor() != 0)
						fixRate = 0.0;
					else if (issuanceFactor.getRefundFactor() != 0)
						fixRate = issuanceFactor.getRefundFactor();
					
					// Apply fix factor/value
					fixValue = NumberUtilities.roundDecimalPlaces((oldItemCoverage.getTotalPremium() * fixRate), Calculus.RESULT_DECIMAL_PLACES);
					newValue = NumberUtilities.roundDecimalPlaces(fixValue, Calculus.STEP_DECIMAL_PLACES);
					itemCoverage.addCalcStep(null, Roadmap.ROADMAP_TYPE_NET_PREMIUM, Factor.FACTOR_TYPE_FIX, "FixedPoint: Factor of Cancellation Insured", oldValue, 0.0, fixValue, 0.0, newValue);
				}
				break;
				
			case Endorsement.ISSUANCE_TYPE_POLICY_CLAIM:
				// Check old issuance values
				if (oldItemCoverage != null) {
					// Check refund or unpaid value
					if (endorsement.getEndorsementType() == Endorsement.ENDORSEMENT_TYPE_REFUND)
						fixRate = (issuanceFactor.getPaidFactor() * -1.0);
					else
						fixRate = issuanceFactor.getUnEarnedFactor();
					
					// Apply fix factor/value
					fixValue = NumberUtilities.roundDecimalPlaces((oldItemCoverage.getTotalPremium() * fixRate), Calculus.RESULT_DECIMAL_PLACES);
					newValue = NumberUtilities.roundDecimalPlaces(fixValue, Calculus.STEP_DECIMAL_PLACES);
					itemCoverage.addCalcStep(null, Roadmap.ROADMAP_TYPE_NET_PREMIUM, Factor.FACTOR_TYPE_FIX, "FixedPoint: Factor of Claim", oldValue, 0.0, fixValue, 0.0, newValue);
				}
				break;
				
			case Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_REGISTER:
				finalRate = NumberUtilities.roundDecimalPlaces(0.0, Calculus.RATE_DECIMAL_PLACES);
				newValue = NumberUtilities.roundDecimalPlaces(0.0, Calculus.STEP_DECIMAL_PLACES);
				itemCoverage.addCalcStep(null, Roadmap.ROADMAP_TYPE_NET_PREMIUM, Factor.FACTOR_TYPE_FIX, "FixedPoint: Factor of Change Register", oldValue, finalRate, 0.0, 0.0, newValue);
				break;
				
			case Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL:
				// Check old issuance values
				if (oldItemCoverage != null) {
					// Check refund or unpaid value
					if (issuanceFactor.getUnPaidFactor() != 0)
						fixRate = issuanceFactor.getUnPaidFactor();
					else if (issuanceFactor.getRefundFactor() != 0)
						fixRate = issuanceFactor.getRefundFactor();
					
					// Apply fix factor/value
					fixValue = NumberUtilities.roundDecimalPlaces((oldItemCoverage.getTotalPremium() * fixRate), Calculus.RESULT_DECIMAL_PLACES);
					newValue = NumberUtilities.roundDecimalPlaces((newValue + fixValue), Calculus.STEP_DECIMAL_PLACES);
					itemCoverage.addCalcStep(null, Roadmap.ROADMAP_TYPE_NET_PREMIUM, Factor.FACTOR_TYPE_FIX, "FixedPoint: Factor of Change Technical", oldValue, 0.0, fixValue, 0.0, newValue);
					
					// Adjust Residual value
					issuanceFactor.setResidualValue(issuanceFactor.getResidualValue() - fixValue);
				}
				break;
		}
		return new ValueAndRate(inputValue, finalRate, finalFixedValue, minimumValue, newValue);
	}
	
	private ValueAndRate getIssuanceFactor(Endorsement endorsement) {
		int endorsedId = 0;
		int endorsedTermId = 0;
		Date endorsedEffectiveDate = null;
		Date endorsedExpiryDate = null;
		double oldPremium = 0.0;
		double paidPremium = 0.0;
		double paidFactor = 0.0;
		double unPaidFactor = 0.0;
		double refundFactor = 0.0;
		double unEarnedFactor = 0.0;
		double residualValue = 0.0;
		int daysRetained = 0;
		int daysEarned = 0;
		int daysUnearned = 0;
		int oldEndorsementType= 0;

		if (endorsement.getEndorsedId() != null && endorsement.getEndorsedId() != 0) {
			boolean nextRecord = true;
			Endorsement workEndorsement = endorsement;
			
			while (nextRecord) {
				nextRecord = false;
				workEndorsement = daoPolicy.loadEndorsement(workEndorsement.getEndorsedId(), workEndorsement.getId().getContractId());
				if ((workEndorsement.getEndorsedId() != null && workEndorsement.getEndorsedId() != 0) &&
				   ((workEndorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_NO_PAYMENT_CANCEL) ||
					(workEndorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_REGISTER))) {
						nextRecord = true;
				}
				else {
					List<Installment> workInstallments = daoPolicy.getInstallmentForEndorsementId(workEndorsement.getId().getContractId(), workEndorsement.getId().getEndorsementId());
					
					endorsedTermId = workEndorsement.getTermId();
					endorsedEffectiveDate = workEndorsement.getEffectiveDate();
					endorsedExpiryDate = workEndorsement.getExpiryDate();
					
					daysRetained = DateUtilities.diffDateInDays(workEndorsement.getEffectiveDate(), workEndorsement.getExpiryDate());
					daysEarned = (DateUtilities.diffDateInDays(workEndorsement.getEffectiveDate(), endorsement.getEffectiveDate()));
					endorsedId = workEndorsement.getId().getEndorsementId();
					
					for (Installment workInstallment : workInstallments) {
						oldPremium = NumberUtilities.roundDecimalPlaces(oldPremium + workInstallment.getInstallmentValue(), Calculus.RESULT_DECIMAL_PLACES);
						if (workInstallment.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_PAID) {
							paidPremium = NumberUtilities.roundDecimalPlaces(paidPremium + workInstallment.getInstallmentValue(), Calculus.RESULT_DECIMAL_PLACES);
						}
					}
				}
				
				// Check is Refund Premium
				oldEndorsementType = workEndorsement.getEndorsementType();
				if (oldEndorsementType == Endorsement.ENDORSEMENT_TYPE_REFUND) {
					daysEarned = daysRetained;
					paidPremium = 0.0;
					oldPremium = (oldPremium *-1);
				}
				
				//Set old Retained Premium
				endorsement.setRetainedPremium((workEndorsement.getRetainedPremium()!= null?workEndorsement.getRetainedPremium():0.0));
				endorsement.setUnearnedPremium((workEndorsement.getUnearnedPremium()!= null?workEndorsement.getUnearnedPremium():0.0));
				endorsement.setEarnedPremium((workEndorsement.getEarnedPremium()!= null?workEndorsement.getEarnedPremium():0.0));
			}
			
			//Set old Residual Value
			residualValue = endorsement.getUnearnedPremium();
			
			//Set new Retained Premium
			endorsement.setRetainedPremium(endorsement.getRetainedPremium() + NumberUtilities.roundDecimalPlaces(((oldPremium / daysRetained) * daysEarned), Calculus.RESULT_DECIMAL_PLACES));
			endorsement.setEarnedPremium(endorsement.getEarnedPremium() + NumberUtilities.roundDecimalPlaces(paidPremium, Calculus.RESULT_DECIMAL_PLACES));
			endorsement.setUnearnedPremium(NumberUtilities.roundDecimalPlaces((endorsement.getRetainedPremium() - endorsement.getEarnedPremium()), Calculus.RESULT_DECIMAL_PLACES));
			
			// Check is Refund Premium
			if (oldEndorsementType == Endorsement.ENDORSEMENT_TYPE_REFUND) {
				oldPremium = (oldPremium *-1);
				paidPremium = oldPremium;
			}
			
			//Set new Paid value
			if (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR)
				paidPremium = endorsement.getEarnedPremium();
			
			if ((endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CLAIM) &&
				(endorsement.getEndorsementType() == Endorsement.ENDORSEMENT_TYPE_REFUND))
				paidPremium = endorsement.getEarnedPremium();
			
			if (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_INSURED)
				paidPremium = NumberUtilities.roundDecimalPlaces((paidPremium + (residualValue * -1)), Calculus.RESULT_DECIMAL_PLACES); //endorsement.getUnearnedPremium(); //
			
			//Set new Residual Value
			residualValue = endorsement.getUnearnedPremium();
			
			//Calculate factors
			paidFactor = (paidPremium / oldPremium);
			unPaidFactor = (residualValue>0?(residualValue / oldPremium):0);
			refundFactor = (residualValue<0?(residualValue / oldPremium):0);
			unEarnedFactor = ((oldPremium - paidPremium) / oldPremium);
		}
		return new ValueAndRate(endorsedId, endorsedTermId, endorsedEffectiveDate, endorsedExpiryDate, oldPremium, paidPremium, paidFactor, unPaidFactor, refundFactor, unEarnedFactor, residualValue, daysRetained, daysEarned, daysUnearned);
	}
	
	private void applyRetainedValue(Item item, ItemCoverage itemCoverage, double inputValue, ValueAndRate issuanceFactor) throws ServiceException {
		logger.debug("Apply the retained value");
		
		// Set final values
		itemCoverage.setRetainedPremium(0.0);
		itemCoverage.setEarnedPremium(0.0);
		itemCoverage.setUnearnedPremium(0.0);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.tratomais.core.service.impl.CalculationServiceAbs#calculate(br.com.tratomais.core.model.policy.Endorsement)
	 */
	@Override
	protected void validateCalc(Endorsement endorsement) {
		endorsement.setErrorList(new ErrorList());
		ErrorList errorList = endorsement.getErrorList();

		// Validate this contract data
		if (!validateCalcContract(endorsement)) {
			errorList.add(ErrorTypes.VALIDATION_CONTRACT_INVALID, null, null, "Calculation Service");
		}
		else {
			// Validate this endorsement data
			if (!validateCalcEndorsement(endorsement)) {
				errorList.add(ErrorTypes.VALIDATION_ENDORSEMENT_INVALID, null, null, "Calculation Service");
			}
			else {
				// Order the Item
				TreeSet<Item> treeItem = orderItem(endorsement.getItems());
				
				// Validate this items data
				for (Item item : treeItem) {
					if (!validateCalcItem(item)) {
						item.setCalculationValid(false);
						errorList.add(ErrorTypes.VALIDATION_ITEM_INVALID, null, null, "Calculation Service", item.getId().getItemId());
					}
					else {
						// Order the profile
						TreeSet<ItemProfile> treeItemProfile = orderItemProfile(item.getItemProfiles());
						
						// Validate this profile data
						for(ItemProfile itemProfile : treeItemProfile) {
							if (!validateCalcProfile(itemProfile)) {
								item.setCalculationValid(false);
								errorList.add(ErrorTypes.VALIDATION_ITEM_PROFILE_INVALID, null, null, "Calculation Service", item.getId().getItemId());
							}
						}
						
						// Order the coverage
						TreeSet<ItemCoverage> treeItemCoverage = orderItemCoverage(item.getItemCoverages());
						
						// Validate this coverage data
						for(ItemCoverage itemCoverage : treeItemCoverage) {
							if (!validateCalcCoverage(itemCoverage)) {
								item.setCalculationValid(false);
								errorList.add(ErrorTypes.VALIDATION_ITEM_COVERAGE_INVALID, null, null, "Calculation Service", item.getId().getItemId(), 0, itemCoverage.getId().getCoverageId());
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Validate contract
	 * @param endorsement
	 * @return
	 */
	private boolean validateCalcContract(Endorsement endorsement) {
		final Contract contract = endorsement.getContract();
		ErrorList errorList = endorsement.getErrorList();
		
		if (contract != null) {
			// Validate the insurer ID
			Insurer insurer = daoInsurer.findById(contract.getInsurerId());
			if (insurer == null) {
				errorList.add(ErrorTypes.VALIDATION_INSURER_ID_INVALID, null, BasicCalculationService.class, "Calculation Service", 0);
				return false;
			}
			
			// Validate the product ID
			Product product = daoProduct.findById(contract.getProductId());
			if (product == null) {
				errorList.add(ErrorTypes.VALIDATION_PRODUCT_ID_INVALID, null, BasicCalculationService.class, "Calculation Service", 0);
				return false;
			}
			
			// Validate the reference date
			if (contract.getReferenceDate() == null) {
				errorList.add(ErrorTypes.VALIDATION_REFERENCE_DATE_INVALID, null, BasicCalculationService.class, "Calculation Service", 0);
				return false;
			}
			
			// Validate the effective date
			if (contract.getEffectiveDate() == null) {
				errorList.add(ErrorTypes.VALIDATION_EFFECTIVE_DATE_INVALID, null, BasicCalculationService.class, "Calculation Service", 0);
				return false;
			}
			else if (contract.getExpiryDate() == null) {
				errorList.add(ErrorTypes.VALIDATION_EXPIRY_DATE_INVALID, null, BasicCalculationService.class, "Calculation Service", 0);
				return false;
			}
			else if (contract.getEffectiveDate().compareTo(contract.getExpiryDate()) > 0) {
				errorList.add(ErrorTypes.VALIDATION_EXPIRY_DATE_INVALID, null, BasicCalculationService.class, "Calculation Service", 0);
				return false;
			}
			
			// Validate the term ID
			Term term = daoCalculo.getTermById(contract.getTermId());
			if (term == null) {
				errorList.add(ErrorTypes.VALIDATION_TERM_ID_INVALID, null, BasicCalculationService.class, "Calculation Service", 0);
				return false;
			}
			
			// Get the master policy
			MasterPolicy masterPolicy = daoMasterPolicy.findById(contract.getMasterPolicyId());
			
			// Validate the product version
			ProductVersion productVersion = daoProductVersion.getProductVersionByRefDate(contract.getProductId(), endorsement.getReferenceDate());
			if (productVersion == null) {
				errorList.add(ErrorTypes.VALIDATION_PRODUCT_VERSION_INVALID, null, null, "Calculation Service", 0);
				return false;
			}
			else {
				// Validate the master policy
				if (product.isMasterPolicyRequired()) {
					if (masterPolicy == null) {
						errorList.add(ErrorTypes.VALIDATION_MASTER_POLICY_ID_INVALID, null, BasicCalculationService.class, "Calculation Service", 0);
						return false;
					}
					else if (contract.getInsurerId() != masterPolicy.getInsurerId()) {
						errorList.add(ErrorTypes.VALIDATION_INSURER_ID_DISPARITY, null, BasicCalculationService.class, "Calculation Service", 0);
						return false;
					}
				}
			}
			
			// Validate the subsidiary ID
			Subsidiary subsidiary = daoSubsidiary.findById(contract.getSubsidiaryId());
			if (subsidiary == null) {
				errorList.add(ErrorTypes.VALIDATION_SUBSIDIARY_ID_INVALID, null, BasicCalculationService.class, "Calculation Service", 0);
				return false;
			}
			
			// Validate the broker ID
			Broker broker = daoBroker.findById(contract.getBrokerId());
			if (broker == null) {
				errorList.add(ErrorTypes.VALIDATION_BROKER_ID_INVALID, null, BasicCalculationService.class, "Calculation Service", 0);
				return false;
			}
			else if (product.isMasterPolicyRequired()) {
				if (!this.parseNull(contract.getBrokerId()).equals(masterPolicy.getBrokerId())) {
					errorList.add(ErrorTypes.VALIDATION_BROKER_ID_DISPARITY, null, BasicCalculationService.class, "Calculation Service", 0);
					return false;
				}
			}
			
			// Validate the partner ID
		 	Partner partner = daoPartner.findById(contract.getPartnerId());
			if ((partner == null) && product.isMasterPolicyRequired()) {
				errorList.add(ErrorTypes.VALIDATION_PARTNER_ID_INVALID, null, BasicCalculationService.class, "Calculation Service", 0);
				return false;
			}
			else if (product.isMasterPolicyRequired()) {
				if (contract.getPartnerId() != masterPolicy.getPartnerId()) {
					errorList.add(ErrorTypes.VALIDATION_PARTNER_ID_DISPARITY, null, BasicCalculationService.class, "Calculation Service", 0);
					return false;
				}
			}
			
			// Define the policy type
			if (product.isMasterPolicyRequired()) {
				contract.setPolicyType(Endorsement.POLICY_TYPE_COLETIVA);
			}
			else {
				contract.setPolicyType(Endorsement.POLICY_TYPE_INDIVIDUAL);
			}
			
			// Validate/Define the policy number
			if (contract.getPolicyNumber() == null) {
				if (product.isMasterPolicyRequired()) {
					contract.setPolicyNumber(masterPolicy.getPolicyNumber());
				}
			}
			else if (product.isMasterPolicyRequired()) { 
				if (!this.parseNull(contract.getPolicyNumber()).equals(masterPolicy.getPolicyNumber())) {
					errorList.add(ErrorTypes.VALIDATION_POLICY_NUMBER_DISPARITY, null, BasicCalculationService.class, "Calculation Service", 0);
					return false;
				}
			}
			
			// Validate the minimum commission factor
			if (!this.parseNull(productVersion.getMinimumCommissionFactor()).equals(0.0)) {
				if (this.parseNull(endorsement.getCommissionFactor()) < productVersion.getMinimumCommissionFactor()) {
					errorList.add(
						ErrorTypes.VALIDATION_COMMISSION_FACTOR_LESS_THAN_MINIMUM,
						new Object[] { NumberUtilities.formatCurrency(productVersion.getMinimumCommissionFactor()) },
						null, "Calculation Service", 0);
					return false;
				}
			}
			
			// Validate the maximum commission factor
			if (!this.parseNull(productVersion.getMaximumCommissionFactor()).equals(0.0)) {
				if (this.parseNull(endorsement.getCommissionFactor()) > productVersion.getMaximumCommissionFactor()) {
					errorList.add(
						ErrorTypes.VALIDATION_COMMISSION_FACTOR_GRATER_THAN_MAXIMUM,
						new Object[] { NumberUtilities.formatCurrency(productVersion.getMaximumCommissionFactor()) },
						null, "Calculation Service", 0);
					return false;
				}
			}
		}
		return (contract != null);
	}
	
	/**
	 * Validate endorsement
	 * @param Endorsement
	 * @return
	 */
	private boolean validateCalcEndorsement(Endorsement endorsement) {
		final Contract contract = endorsement.getContract();
		ErrorList errorList = endorsement.getErrorList();
		
		// Validate the reference date
		if (endorsement.getReferenceDate() == null) {
			errorList.add(ErrorTypes.VALIDATION_REFERENCE_DATE_INVALID, null, BasicCalculationService.class, "Calculation Service", 0);
			return false;
		}
		
		// Validate the effective date
		if (endorsement.getEffectiveDate() == null) {
			errorList.add(ErrorTypes.VALIDATION_EFFECTIVE_DATE_INVALID, null, BasicCalculationService.class, "Calculation Service", 0);
			return false;
		}
		
		// Validate the subsidiary ID
		Channel channel = daoChannel.findById(endorsement.getChannelId());
		if (channel == null) {
			errorList.add(ErrorTypes.VALIDATION_CHANNEL_ID_INVALID, null, BasicCalculationService.class, "Calculation Service", 0);
			return false;
		}
		else if (contract.getSubsidiaryId() != channel.getSubsidiaryId()) {
			errorList.add(ErrorTypes.VALIDATION_SUBSIDIARY_DISPARITY, null, BasicCalculationService.class, "Calculation Service", 0);
			return false;
		}
		
		// Validate the term ID
		Term term = daoCalculo.getTermById(endorsement.getTermId());
		if (term == null) {
			errorList.add(ErrorTypes.VALIDATION_TERM_ID_INVALID, null, BasicCalculationService.class, "Calculation Service", 0);
			return false;
		}
		else {
			// Validate the term effective
			ProductTerm productTerm = daoProduct.getProductTermById(contract.getProductId(), endorsement.getTermId(), endorsement.getReferenceDate());
			if (productTerm == null) {
				errorList.add(ErrorTypes.VALIDATION_TERM_ID_DISPARITY, null, BasicCalculationService.class, "Calculation Service", 0);
				return false;
			}
		}
		
		// Validate that insered items
		if ((endorsement.getItems() == null) || (endorsement.getItems().isEmpty())) {
			errorList.add(ErrorTypes.VALIDATION_ITEM_ABSENT, null, BasicCalculationService.class, "Calculation Service", 0);
			return false;
		}
		
		return true;
	}
	
	/**
	 * Validate item
	 * @param Item
	 * @return
	 */
	private boolean validateCalcItem(Item item) {
		Double riskValue = 0d;
		NumberFormat nf = new DecimalFormat("#,###.00");
		
		Endorsement endorsement = item.getEndorsement();
		Contract contract = endorsement.getContract();
		Customer customer = endorsement.getCustomerByInsuredId();
		ErrorList errorList = endorsement.getErrorList();
		ItemPersonalRisk itemPersonalRisk = null;
		ItemProperty itemProperty = null;
		PlanKinship planKinship = null;
		DataOption personalRiskType = null;
		DataOption kinshipType = null;
		final Date refDate = endorsement.getReferenceDate();
		final int itemId = item.getId().getItemId();
		
		// Get the product version
		ProductVersion productVersion = daoProductVersion.getProductVersionByRefDate(contract.getProductId(), refDate);
		if (productVersion != null) {
			item.setObjectId(productVersion.getProduct().getObjectId());
		}
		
		// Validate the object items
		switch (item.getObjectId()) {
			case Item.OBJECT_VIDA:
			case Item.OBJECT_ACCIDENTES_PERSONALES:
			case Item.OBJECT_PURCHASE_PROTECTED:
				itemPersonalRisk = (ItemPersonalRisk)item;
				
				// Validate the birth date
				if (itemPersonalRisk.getBirthDate() == null) {
					errorList.add(ErrorTypes.VALIDATION_BIRTH_DATE_INVALID, null, BasicCalculationService.class, "Calculation Service", itemId);
					return false;
				}
				else {
					// Validate the age actuarial
					itemPersonalRisk.setAgeActuarial(DateUtilities.actuarialAgeByDate(itemPersonalRisk.getBirthDate(), contract.getEffectiveDate()));
				}
				
				// Validate the personal risk type
				if (this.parseNull(itemPersonalRisk.getPersonalRiskType()).equals(0)) {
					itemPersonalRisk.setPersonalRiskType(ItemPersonalRisk.PERSONAL_RISK_TYPE_TITULAR);
				}
				personalRiskType = daoDataOption.getDataOption(itemPersonalRisk.getPersonalRiskType());
				if (personalRiskType == null || !itemPersonalRisk.getPersonalRiskType().equals(ItemPersonalRisk.PERSONAL_RISK_TYPE_TITULAR)) {
					errorList.add(ErrorTypes.VALIDATION_PERSONAL_RISK_TYPE_INVALID, null, BasicCalculationService.class, "Calculation Service", itemId);
					return false;
				}
				
				// Validate the kinship type
				if (this.parseNull(itemPersonalRisk.getKinshipType()).equals(0)) {
					itemPersonalRisk.setKinshipType(ItemPersonalRisk.PERSONAL_KINSHIP_TYPE_PROPRIO);
				}
				kinshipType = daoDataOption.getDataOption(itemPersonalRisk.getKinshipType());
				if (kinshipType == null || !itemPersonalRisk.getKinshipType().equals(ItemPersonalRisk.PERSONAL_KINSHIP_TYPE_PROPRIO)) {
					errorList.add(ErrorTypes.VALIDATION_PERSONAL_KINSHIP_TYPE_INVALID, null, BasicCalculationService.class, "Calculation Service", itemId);
					return false;
				}
				
				// Validate the plan kinship
				planKinship = daoCalculo.getPlanKinshipByRefDate(contract.getProductId(), endorsement.getRiskPlanId(), itemPersonalRisk.getPersonalRiskType(), itemPersonalRisk.getKinshipType(), itemPersonalRisk.getRenewalType(), refDate);
				if (planKinship != null) {
					// Validate the age insured
					double age = DateUtilities.agePartByDate(itemPersonalRisk.getBirthDate(), item.getEffectiveDate());
					if (((planKinship.getMinimumAge() != 0) && (age < planKinship.getMinimumAge())) 
					 || ((planKinship.getMaximumAge() != 0) && (age > planKinship.getMaximumAge()))) {
							errorList.add(
								ErrorTypes.VALIDATION_INSURED_AGE_INVALID,
								personalRiskType.getFieldDescription(),
								itemPersonalRisk.getInsuredName(),
								planKinship.getMinimumAge(),
								planKinship.getMaximumAge() == 0 ? 99 : planKinship.getMaximumAge(), null,
								"Calculation Service", itemId, 0);
							return false;
					}
				}
				
				// Get the risk value from object
				riskValue = this.parseNull(itemPersonalRisk.getPersonalRiskValue());
				break;

			case Item.OBJECT_CREDITO:
				itemPersonalRisk = (ItemPersonalRisk)item;
				
				// Get the risk value from object
				riskValue = this.parseNull(itemPersonalRisk.getPersonalRiskValue());
				
				// Validate the minimum insured value
				if ((productVersion.getMinimumInsuredValue() > 0) &&
					(riskValue < productVersion.getMinimumInsuredValue())) {
						errorList.add(
							ErrorTypes.VALIDATION_ITEM_CREDIT_VALUE_LESS_THAN_MINIMUM,
							new Object[] { NumberUtilities.formatCurrency(productVersion.getMinimumInsuredValue()) },
							null, "Calculation Service", itemId);
						return false;
				}
				
				// Validate the maximum insured value
				if ((productVersion.getMaximumInsuredValue() > 0) && 
					(riskValue > productVersion.getMaximumInsuredValue())) {
						errorList.add(
							ErrorTypes.VALIDATION_ITEM_CREDIT_VALUE_GRATER_THAN_MAXIMUM,
							new Object[] { NumberUtilities.formatCurrency(productVersion.getMaximumInsuredValue()) },
							null, "Calculation Service", itemId);
						return false;
				}
				break;
				
			case Item.OBJECT_CICLO_VITAL:
				itemPersonalRisk = (ItemPersonalRisk)item;
				
				// Validate the birth date
				if (itemPersonalRisk.getBirthDate() != null) {
					
					// Validate the personal risk type
					personalRiskType = daoDataOption.getDataOption(itemPersonalRisk.getPersonalRiskType());
					if (personalRiskType == null || 
							(!itemPersonalRisk.getPersonalRiskType().equals(ItemPersonalRisk.PERSONAL_RISK_TYPE_TITULAR) &&
							 !itemPersonalRisk.getPersonalRiskType().equals(ItemPersonalRisk.PERSONAL_RISK_TYPE_FAMILY_GROUP) &&
							 !itemPersonalRisk.getPersonalRiskType().equals(ItemPersonalRisk.PERSONAL_RISK_TYPE_ADDITIONAL) &&
							 !itemPersonalRisk.getPersonalRiskType().equals(ItemPersonalRisk.PERSONAL_RISK_TYPE_NEWBORN))) {
						errorList.add(ErrorTypes.VALIDATION_PERSONAL_RISK_TYPE_INVALID, null, BasicCalculationService.class, "Calculation Service", itemId);
						return false;
					}
					
					// Validate the kinship type
					kinshipType = daoDataOption.getDataOption(DataGroup.GROUP_KINSHIP_TYPE, itemPersonalRisk.getKinshipType());
					if (kinshipType == null) {
						errorList.add(ErrorTypes.VALIDATION_PERSONAL_KINSHIP_TYPE_INVALID, null, BasicCalculationService.class, "Calculation Service", itemId);
						return false;
					}
					
					// Validate the plan kinship
					planKinship = daoCalculo.getPlanKinshipByRefDate(contract.getProductId(), endorsement.getRiskPlanId(), itemPersonalRisk.getPersonalRiskType(), itemPersonalRisk.getKinshipType(), itemPersonalRisk.getRenewalType(), refDate);
					if (planKinship != null) {
						// Validate the age insured
						double age = DateUtilities.agePartByDate(itemPersonalRisk.getBirthDate(), item.getEffectiveDate());
						if (((planKinship.getMinimumAge() != 0) && (age < planKinship.getMinimumAge())) 
						 || ((planKinship.getMaximumAge() != 0) && (age > planKinship.getMaximumAge()))) {
								errorList.add(
									ErrorTypes.VALIDATION_INSURED_AGE_INVALID,
									daoDataOption.getDataOption(planKinship.getId().getPersonalRiskType()).getFieldDescription(),
									itemPersonalRisk.getInsuredName(),
									planKinship.getMinimumAge(),
									planKinship.getMaximumAge() == 0 ? 99 : planKinship.getMaximumAge(), null,
									"Calculation Service", itemId, 0);
								return false;
						}
					}
				}
				break;
				
			case Item.OBJECT_HABITAT:
				itemProperty = (ItemProperty)item;
				
				// Get the risk value from object
				switch (itemProperty.getPropertyRiskType()) {
					case Coverage.RISKTYPE_CONTENT:
						riskValue = this.parseNull(itemProperty.getContentValue());
						break;
					case Coverage.RISKTYPE_STRUCTURE:
						riskValue = this.parseNull(itemProperty.getStructureValue());
						break;
					case Coverage.RISKTYPE_BOTH:
						riskValue = this.parseNull(itemProperty.getContentValue());
						riskValue += this.parseNull(itemProperty.getStructureValue());
						break;
					default:
						errorList.add(ErrorTypes.VALIDATION_PROPERTY_RISK_TYPE_INVALID, null, BasicCalculationService.class, "Calculation Service", itemId);
						return false;
				}
				break;
				
			case Item.OBJECT_CAPITAL:
				itemProperty = (ItemProperty)item;
				
				// Get the risk value from object
				ItemRiskType itemRiskType = item.findRiskType(item.getBasicCoverage().getGroupCoverageId());
				if (itemRiskType != null) {
					riskValue = this.parseNull(itemRiskType.getRiskTypeValue());
				}
				break;
				
			case Item.OBJECT_AUTO:
				ItemAuto itemAuto = (ItemAuto)item;
				riskValue = this.parseNull(itemAuto.getValueRefer());
				
				// Validate the personal risk type
				DataOption motoristRiskType = daoDataOption.getDataOption(ItemAuto.PERSONAL_RISK_TYPE_MOTORIST);
				
				// Validate the Motorist data
				for (Motorist motorist : itemAuto.getMotorists()) {
					// Validate the birth date
					if (motorist.getBirthDate() != null) {
						// Set default data
						if (this.parseNull(motorist.getKinshipType()).equals(0)) {
							motorist.setKinshipType(ItemAuto.PERSONAL_KINSHIP_TYPE_OWN);
						}
						// Validate the kinship type
						kinshipType = daoDataOption.getDataOption(DataGroup.GROUP_KINSHIP_TYPE, motorist.getKinshipType());
						if (kinshipType == null) {
							errorList.add(ErrorTypes.VALIDATION_PERSONAL_KINSHIP_TYPE_INVALID, null, BasicCalculationService.class, "Calculation Service", itemId);
							return false;
						}
						
						// Validate the plan kinship: Motorist
						planKinship = daoCalculo.getPlanKinshipByRefDate(contract.getProductId(), endorsement.getRiskPlanId(), ItemAuto.PERSONAL_RISK_TYPE_MOTORIST, motorist.getKinshipType(), itemAuto.getRenewalType(), refDate);
						if (planKinship != null) {
							// Validate the age insured
							double age = DateUtilities.agePartByDate(motorist.getBirthDate(), item.getEffectiveDate());
							if (((planKinship.getMinimumAge() != 0) && (age < planKinship.getMinimumAge())) 
							 || ((planKinship.getMaximumAge() != 0) && (age > planKinship.getMaximumAge()))) {
									errorList.add(
										ErrorTypes.VALIDATION_INSURED_AGE_INVALID,
										motoristRiskType.getFieldDescription(),
										(motorist.getName() != null ? motorist.getName() : 
																	 (customer != null && motorist.getMotoristType() == Motorist.MOTORIST_TYPE_MAIN ? customer.getName() : "")),
										planKinship.getMinimumAge(),
										planKinship.getMaximumAge() == 0 ? 99 : planKinship.getMaximumAge(), null,
										"Calculation Service", itemId, 0);
									return false;
							}
						}
					}
				}
				break;
		}
		
		// Validate that coverage of items
		if ((item.getItemCoverages() == null) || (item.getItemCoverages().isEmpty())) {
			errorList.add(ErrorTypes.VALIDATION_ITEM_COVERAGE_ABSENT, null, BasicCalculationService.class, "Calculation Service", itemId);
			return false;
		}
		else if (item.getBasicCoverage() == null) {
			errorList.add(ErrorTypes.VALIDATION_ITEM_COVERAGE_BASIC_ABSENT, null, BasicCalculationService.class, "Calculation Service", itemId);
			return false;
		}
		else {
			riskValue = (riskValue != 0d ? riskValue : item.getBasicCoverageValue(null));
		}
		
		// Validate the minimum insured value
		if ((productVersion.getMinimumInsuredValue() > 0) && 
			(riskValue < productVersion.getMinimumInsuredValue())) {
				errorList.add(
					ErrorTypes.VALIDATION_RISK_VALUE_LESS_THAN_MINIMUM,
					new String[] {
							nf.format(productVersion.getMinimumInsuredValue()),
							nf.format(riskValue),
							Integer.toString(itemId) },
					BasicCalculationService.class, "Item Validation", itemId);
				return false;
		}
		
		// Validate the maximum insured value
		if ((productVersion.getMaximumInsuredValue() > 0) && 
			(riskValue > productVersion.getMaximumInsuredValue())) {
				errorList.add(
					ErrorTypes.VALIDATION_RISK_VALUE_GRATER_THAN_MAXIMUM,
					new String[] {
							nf.format(productVersion.getMaximumInsuredValue()),
							nf.format(riskValue),
							Integer.toString(itemId) },
					BasicCalculationService.class, "Item Validation", itemId);
				return false;
		}
		
		return true;
	}
	
	/**
	 * Validate Profile Item
	 * 
	 * @param itemProfile
	 *            itemProfile
	 * @return
	 */
	private boolean validateCalcProfile(ItemProfile itemProfile) {
		Item item = itemProfile.getItem();
		Endorsement endorsement = item.getEndorsement();
		ErrorList errorList = endorsement.getErrorList();
		final int itemId = item.getId().getItemId();
		
		// Checks the rules for Profile
		if (!serviceRule.checkRuleProfile(itemProfile)) {
			errorList.add(
					ErrorTypes.VALIDATION_RULE_TYPE_PROFILE_BLOCKED,
					new String[] {
							itemProfile.getQuestionName(),
							itemProfile.getResponseName(),
							Integer.toString(itemId) },
					BasicCalculationService.class, "Profile Validation", itemId);
			return false;
		}
		
		return true;
	}
	
	/**
	 * Validate Coverage Item
	 * 
	 * @param itemCoverage
	 *            CoverageItem
	 * @return
	 */
	private boolean validateCalcCoverage(ItemCoverage itemCoverage) {
		NumberFormat nf = new DecimalFormat("#,##0.00");
		Double riskValue = 0d;
		Integer riskType = null;
		String additionalName = "";
		
		Item item = itemCoverage.getItem();
		Endorsement endorsement = item.getEndorsement();
		Contract contract = endorsement.getContract();
		ErrorList errorList = endorsement.getErrorList();
		ItemProperty itemProperty = null;
		final Date refDate = endorsement.getReferenceDate();
		final int itemId = item.getId().getItemId();
		final int coverageId = itemCoverage.getId().getCoverageId();
		
		// Set the Transient values
		item.setTCoverageId(itemCoverage.getId().getCoverageId());
		item.setTRangeValueId(itemCoverage.getRangeValueId());
		item.setTDeductibleId(this.parseNull(itemCoverage.getDeductibleId()));
		
		// Get application property
		ApplicationProperty propertyUTCurrencyId = daoPolicy.loadApplicationProperty(1, "UTCurrencyId");
		
		// Get the coverage plan
		CoveragePlan coveragePlan = daoCoveragePlan.getCoveragePlanByRefDate(contract.getProductId(), item.getCoveragePlanId(), coverageId, refDate);
		if (coveragePlan == null) {
			errorList.add(ErrorTypes.VALIDATION_PRODUTCT_PLAN_COVERAGE_INVALID, null, BasicCalculationService.class, "Calculation Service", itemId, 0, coverageId);
			return false;
		}
		else {
			// Get the coverage
			Coverage coverage = coveragePlan.getCoverage();
			if (coverage !=  null) {
				additionalName = "(" + coverage.getCoverageCode() + ":" + coverage.getGoodsCode() + ")";
			}
			
			// Validate the coverage relationship
			if (!validateCalcCoverageRelationship(coverage, itemCoverage)){
				return false;
			}
			
			// Validate the insured value not service or not value
			if (!(coverage.isService() || (coveragePlan.getInsuredValueType() == CoveragePlan.INSURED_VALUE_TYPE_SSA))) {
				if ((itemCoverage.getInsuredValue() == null) || (itemCoverage.getInsuredValue().equals(0.0))) {
					errorList.add(ErrorTypes.VALIDATION_COVERAGE_VALUE_INVALID, new String[] { coveragePlan.getNickName(), additionalName }, BasicCalculationService.class, "Calculation Service", itemId, 0, coverageId);
					return false;
				}
			}
			
			// Validate the insured value from range
			if (coveragePlan.getInsuredValueType() == CoveragePlan.INSURED_VALUE_TYPE_RSA) {
				CoverageRangeValue coverageRangeValue = daoProduct.getCoverageRangeValueById(contract.getProductId(), item.getCoveragePlanId(), coverageId, itemCoverage.getRangeValueId(), refDate);
				if (coverageRangeValue == null) {
					errorList.add(ErrorTypes.VALIDATION_COVERAGE_VALUE_RANGE_ID_INVALID, new String[] { coveragePlan.getNickName(), additionalName }, BasicCalculationService.class, "Calculation Service", itemId, 0, coverageId);
					return false;
				}
			}
			
			// Get the risk value from object
			switch (item.getObjectId()) {
				case Item.OBJECT_VIDA:
				case Item.OBJECT_CREDITO:
				case Item.OBJECT_CICLO_VITAL:
				case Item.OBJECT_ACCIDENTES_PERSONALES:
				case Item.OBJECT_PURCHASE_PROTECTED:
					ItemPersonalRisk itemPersonalRisk = (ItemPersonalRisk)item;
					riskValue = this.parseNull(itemPersonalRisk.getPersonalRiskValue());
					break;
				case Item.OBJECT_HABITAT:
					itemProperty = (ItemProperty)item;
					switch (coverage.getRiskType()) {
						case Coverage.RISKTYPE_CONTENT:
							riskValue = this.parseNull(itemProperty.getContentValue());
							riskType = coverage.getRiskType();
							break;
						case Coverage.RISKTYPE_STRUCTURE:
							riskValue = this.parseNull(itemProperty.getStructureValue());
							riskType = coverage.getRiskType();
							break;
						case Coverage.RISKTYPE_BOTH:
							riskValue = this.parseNull(itemProperty.getContentValue());
							riskValue += this.parseNull(itemProperty.getStructureValue());
							break;
					}
					break;
				case Item.OBJECT_CAPITAL:
					itemProperty = (ItemProperty)item;
					riskType = itemCoverage.getGroupCoverageId();
					ItemRiskType itemRiskType = item.findRiskType(riskType);
					if (itemRiskType != null) {
						riskValue = this.parseNull(itemRiskType.getRiskTypeValue());
					}
					break;
				case Item.OBJECT_AUTO:
					ItemAuto itemAuto = (ItemAuto)item;
					riskValue = this.parseNull(itemAuto.getValueRefer());
					break;
			}
			
			// Review the risk value
			riskValue = (riskValue != 0d ? riskValue : item.getBasicCoverageValue(riskType));
			
			// Validate the insured value limit
			if (!this.parseNull(coveragePlan.getInsuredValueLimitType()).equals(0)) {
				// Validate the object items
				switch (coveragePlan.getInsuredValueLimitType()) {
					case CoveragePlan.LIMIT_TYPE_FIXED_VALUE:
						// Validate the minimum insured value
						if (!this.parseNull(coveragePlan.getMinimumInsuredValue()).equals(0.0)) {
							if (NumberUtilities.roundDecimalPlaces(itemCoverage.getInsuredValue(), 2) < NumberUtilities.roundDecimalPlaces(coveragePlan.getMinimumInsuredValue(), 2)) {							
								errorList.add(
									ErrorTypes.VALIDATION_COVERAGE_VALUE_LESS_THAN_MINIMUM,
										new String[] {
											nf.format(coveragePlan.getMinimumInsuredValue()),
											nf.format(itemCoverage.getInsuredValue()),
											coveragePlan.getNickName(),
											additionalName },
									BasicCalculationService.class, "Calculation Service", itemId, 0, coverageId);
								return false;
							}
						}
						
						// Validate the maximum insured value
						if (!this.parseNull(coveragePlan.getMaximumInsuredValue()).equals(0.0)) {
							if (NumberUtilities.roundDecimalPlaces(itemCoverage.getInsuredValue(), 2) > NumberUtilities.roundDecimalPlaces(coveragePlan.getMaximumInsuredValue(), 2)) {
								errorList.add(
									ErrorTypes.VALIDATION_COVERAGE_VALUE_GRATER_THAN_MAXIMUM,
									new String[] {
										nf.format(coveragePlan.getMaximumInsuredValue()),
										nf.format(itemCoverage.getInsuredValue()),
										coveragePlan.getNickName(), 
										additionalName },
									BasicCalculationService.class, "Calculation Service", itemId, 0, coverageId);
								return false;
							}
						}
						break;
						
					case CoveragePlan.LIMIT_TYPE_UT:
						// Get the UT currency value
						Double utValue = 0d;
						if (propertyUTCurrencyId != null) {
							ConversionRate conversionRate = daoPolicy.findConversionRateByRefDate(Integer.parseInt(propertyUTCurrencyId.getValue()), endorsement.getEffectiveDate());
							if (conversionRate != null) {
								utValue = conversionRate.getMultipleRateBy();
							}
						}
						
						// Validate the minimum insured value
						if ((!this.parseNull(coveragePlan.getMinimumInsuredValue()).equals(0.0)) && (!utValue.equals(0.0))) {
							if (NumberUtilities.roundDecimalPlaces(itemCoverage.getInsuredValue(), 2) < NumberUtilities.roundDecimalPlaces(coveragePlan.getMinimumInsuredValue() * utValue, 2)) {							
								errorList.add(
									ErrorTypes.VALIDATION_COVERAGE_VALUE_LESS_THAN_MINIMUM,
										new String[] {
											nf.format(coveragePlan.getMinimumInsuredValue() * utValue),
											nf.format(itemCoverage.getInsuredValue()),
											coveragePlan.getNickName(),
											additionalName },
									BasicCalculationService.class, "Calculation Service", itemId, 0, coverageId);
								return false;
							}
						}
						
						// Validate the maximum insured value
						if ((!this.parseNull(coveragePlan.getMaximumInsuredValue()).equals(0.0)) && (!utValue.equals(0.0))) {
							if (NumberUtilities.roundDecimalPlaces(itemCoverage.getInsuredValue(), 2) > NumberUtilities.roundDecimalPlaces(coveragePlan.getMaximumInsuredValue() * utValue, 2)) {
								errorList.add(
									ErrorTypes.VALIDATION_COVERAGE_VALUE_GRATER_THAN_MAXIMUM,
									new String[] {
										nf.format(coveragePlan.getMaximumInsuredValue() * utValue),
										nf.format(itemCoverage.getInsuredValue()),
										coveragePlan.getNickName(), 
										additionalName },
									BasicCalculationService.class, "Calculation Service", itemId, 0, coverageId);
								return false;
							}
						}						
						break;
						
					case CoveragePlan.LIMIT_TYPE_RISK_VALUE:
						// Validate the minimum insured value
						if (!this.parseNull(coveragePlan.getMinimumInsuredValue()).equals(0.0)) {
							if (NumberUtilities.roundDecimalPlaces(itemCoverage.getInsuredValue(), 2) < NumberUtilities.roundDecimalPlaces(coveragePlan.getMinimumInsuredValue() * riskValue, 2)) {							
								errorList.add(
									ErrorTypes.VALIDATION_COVERAGE_VALUE_LESS_THAN_MINIMUM,
										new String[] {
											nf.format(coveragePlan.getMinimumInsuredValue() * riskValue),
											nf.format(itemCoverage.getInsuredValue()),
											coveragePlan.getNickName(),
											additionalName },
									BasicCalculationService.class, "Calculation Service", itemId, 0, coverageId);
								return false;
							}
						}
						
						// Validate the maximum insured value
						if (!this.parseNull(coveragePlan.getMaximumInsuredValue()).equals(0.0)) {
							if (NumberUtilities.roundDecimalPlaces(itemCoverage.getInsuredValue(), 2) > NumberUtilities.roundDecimalPlaces(coveragePlan.getMaximumInsuredValue() * riskValue, 2)) {
								errorList.add(
									ErrorTypes.VALIDATION_COVERAGE_VALUE_GRATER_THAN_MAXIMUM,
									new String[] {
										nf.format(coveragePlan.getMaximumInsuredValue() * riskValue),
										nf.format(itemCoverage.getInsuredValue()),
										coveragePlan.getNickName(), 
										additionalName },
									BasicCalculationService.class, "Calculation Service", itemId, 0, coverageId);
								return false;
							}
						}
						break;
						
					case CoveragePlan.LIMIT_TYPE_MULTIPLES_RISK_VALUE:
						// Get the Multiples coverage values
						Double mrValue = 0d;
						Double insuredValueRelationship = this.getInsuredValueCoverageRelationship(itemCoverage);
						if (insuredValueRelationship != null) {
							mrValue = insuredValueRelationship;
						}
						
						// Validate the minimum insured value
						if ((!this.parseNull(coveragePlan.getMinimumInsuredValue()).equals(0.0)) && (!mrValue.equals(0.0))) {
							if (NumberUtilities.roundDecimalPlaces(itemCoverage.getInsuredValue(), 2) < NumberUtilities.roundDecimalPlaces(coveragePlan.getMinimumInsuredValue() * mrValue, 2)) {							
								errorList.add(
									ErrorTypes.VALIDATION_COVERAGE_VALUE_LESS_THAN_MINIMUM,
										new String[] {
											nf.format(coveragePlan.getMinimumInsuredValue() * mrValue),
											nf.format(itemCoverage.getInsuredValue()),
											coveragePlan.getNickName(),
											additionalName },
									BasicCalculationService.class, "Calculation Service", itemId, 0, coverageId);
								return false;
							}
						}
						
						// Validate the maximum insured value
						if ((!this.parseNull(coveragePlan.getMaximumInsuredValue()).equals(0.0)) && (!mrValue.equals(0.0))) {
							if (NumberUtilities.roundDecimalPlaces(itemCoverage.getInsuredValue(), 2) > NumberUtilities.roundDecimalPlaces(coveragePlan.getMaximumInsuredValue() * mrValue, 2)) {
								errorList.add(
									ErrorTypes.VALIDATION_COVERAGE_VALUE_GRATER_THAN_MAXIMUM,
									new String[] {
										nf.format(coveragePlan.getMaximumInsuredValue() * mrValue),
										nf.format(itemCoverage.getInsuredValue()),
										coveragePlan.getNickName(), 
										additionalName },
									BasicCalculationService.class, "Calculation Service", itemId, 0, coverageId);
								return false;
							}
						}
						break;
				}
			}
			
			// Checks the rules for Coverage
			if (!serviceRule.checkRuleCoverage(itemCoverage)) {
				errorList.add(
						ErrorTypes.VALIDATION_RULE_TYPE_COVERAGE_BLOCKED,
						new String[] {
							coveragePlan.getNickName(), 
							additionalName },
						BasicCalculationService.class, "Rule Service", itemId, 0, coverageId);				
				return false;
			}
			
			// Checks the rules for InsuredValue
			if (!serviceRule.checkRuleInsuredValue(itemCoverage)) {
				errorList.add(
						ErrorTypes.VALIDATION_RULE_TYPE_INSURED_VALUE_BLOCKED,
						new String[] {
							nf.format(itemCoverage.getInsuredValue()),
							coveragePlan.getNickName(), 
							additionalName },
						BasicCalculationService.class, "Rule Service", itemId, 0, coverageId);
				return false;
			}
			
			// Checks the rules for Deducible
			if (!serviceRule.checkRuleDeducible(itemCoverage)) {
				errorList.add(
						ErrorTypes.VALIDATION_RULE_TYPE_DEDUCTIBLE_BLOCKED,
						new String[] {
							itemCoverage.getDeductibleDescription(),
							coveragePlan.getNickName(), 
							additionalName },
						BasicCalculationService.class, "Rule Service", itemId, 0, coverageId);
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Validate Coverage Relationship
	 * 
	 * @param itemCoverage
	 *            CoverageItem
	 * @return
	 */
	private boolean validateCalcCoverageRelationship(Coverage coverage, ItemCoverage itemCoverage){
		boolean retorno = true;
		boolean achou = false;
		String mensagem = "";
		String coverageName = "";
		String parentName = "";
		
		Item item = itemCoverage.getItem();
		Endorsement endorsement = item.getEndorsement();
		Contract contract = endorsement.getContract();
		ErrorList errorList = endorsement.getErrorList();
		List<CoverageRelationship> listCoverageRelationship = null;
		final Date refDate = endorsement.getReferenceDate();
		final int itemId = item.getId().getItemId();
		final int productId = contract.getProductId();
		final int planId = item.getCoveragePlanId();
		final int coverageId = itemCoverage.getId().getCoverageId();
		
		// Get the coverage
		if (coverage !=  null) {
			coverageName = "(" + coverage.getCoverageCode() + ":" + coverage.getGoodsCode()+ ") " + itemCoverage.getCoverageName();
		}
		
		// Checks y validate the relationship mutually exclusive
		listCoverageRelationship = daoCoverageRelationship.listCoverageRelationshipByProductId(productId, planId, coverageId, CoverageRelationship.RELATIONSHIP_TYPE_MUTUALLY_EXCLUSIVE, refDate);
		for (CoverageRelationship coverageRelationship : listCoverageRelationship) {
			for (ItemCoverage workItemCoverage : item.getItemCoverages()) {
				if (workItemCoverage.getId().getCoverageId() == coverageRelationship.getId().getParentId()) {
					// Get the parent
					Coverage parent = coverageRelationship.getCoverageByParentId();
					if (parent !=  null) {
						parentName = "(" + parent.getCoverageCode() + ":" + parent.getGoodsCode()+ ") " + parent.getNickName();
					}
					errorList.add(
							ErrorTypes.VALIDATION_COVERAGE_MUTUALLY_EXCLUSIVE,
							new String[] {
								coverageName,
								parentName },
							BasicCalculationService.class, "Calculation Service", itemId, 0, coverageId);
					retorno = false;
				}
			}
		}
		
		// Checks y validate the relationship dependency "OR"
		achou = false;
		listCoverageRelationship = daoCoverageRelationship.listCoverageRelationshipByProductId(productId, planId, coverageId, CoverageRelationship.RELATIONSHIP_TYPE_DEPENDENCE_OR, refDate);
		for (CoverageRelationship coverageRelationship : listCoverageRelationship) {
			// Get the parent
			Coverage parent = coverageRelationship.getCoverageByParentId();
			if (parent !=  null) {
				parentName = "(" + parent.getCoverageCode() + ":" + parent.getGoodsCode()+ ") " + parent.getNickName();
			}
			mensagem += (mensagem.length() == 0? "\"" : ", \"") + parentName + "\"";
			for (ItemCoverage workItemCoverage : item.getItemCoverages()) {
				if (workItemCoverage.getId().getCoverageId() == coverageRelationship.getId().getParentId()) {
					achou = true;
					break;
				}
			}
		}
		// Check if apply error the relationship dependency "OR"
		if (!achou && listCoverageRelationship != null && listCoverageRelationship.size() > 0) {
			errorList.add(
					ErrorTypes.VALIDATION_COVERAGE_DEPENDENCE_OR,
					new String[] {
						coverageName,
						mensagem },
					BasicCalculationService.class, "Calculation Service", itemId, 0, coverageId);
			retorno = false;
			
		}
		
		// Checks y validate the relationship dependency "AND"
		listCoverageRelationship = daoCoverageRelationship.listCoverageRelationshipByProductId(productId, planId, coverageId, CoverageRelationship.RELATIONSHIP_TYPE_DEPENDENCE_AND, refDate);
		for (CoverageRelationship coverageRelationship : listCoverageRelationship) {
			achou = false;
			for (ItemCoverage workItemCoverage : item.getItemCoverages()) {
				if (workItemCoverage.getId().getCoverageId() == coverageRelationship.getId().getParentId()) {
					achou = true;
					break;
				}
			}
			// Check if apply error the relationship dependency "AND"
			if (!achou) {
				// Get the parent
				Coverage parent = coverageRelationship.getCoverageByParentId();
				if (parent !=  null) {
					parentName = "(" + parent.getCoverageCode() + ":" + parent.getGoodsCode()+ ") " + parent.getNickName();
				}
				errorList.add(
						ErrorTypes.VALIDATION_COVERAGE_DEPENDENCE_AND,
						new String[] {
							coverageName,
							parentName },
						BasicCalculationService.class, "Calculation Service", itemId, 0, coverageId);
				retorno = false;
			}
		}
		return retorno;
	}
	
	/**
	 * Get Insured Value Coverage Relationship
	 * 
	 * @param itemCoverage
	 *            CoverageItem
	 * @return
	 */
	private Double getInsuredValueCoverageRelationship(ItemCoverage itemCoverage){
		Double retorno = 0.0;
		
		Item item = itemCoverage.getItem();
		Endorsement endorsement = item.getEndorsement();
		Contract contract = endorsement.getContract();
		final Date refDate = endorsement.getReferenceDate();
		final int productId = contract.getProductId();
		final int planId = item.getCoveragePlanId();
		final int coverageId = itemCoverage.getId().getCoverageId();
		
		// Get values the relationship Insured Limit
		List<CoverageRelationship> listCoverageRelationship = daoCoverageRelationship.listCoverageRelationshipByProductId(productId, planId, coverageId, CoverageRelationship.RELATIONSHIP_TYPE_INSURED_LIMIT, refDate);
		for (CoverageRelationship coverageRelationship : listCoverageRelationship) {
			for (ItemCoverage workItemCoverage : item.getItemCoverages()) {
				if (workItemCoverage.getId().getCoverageId() == coverageRelationship.getId().getParentId()) {
					if ((coverageRelationship.getRelationshipFactor() != null) && (!coverageRelationship.getRelationshipFactor().equals(0.0))) {
						retorno = (retorno + NumberUtilities.roundDecimalPlaces(workItemCoverage.getInsuredValue() * coverageRelationship.getRelationshipFactor().doubleValue(), 2));
					} else {
						retorno = (retorno + NumberUtilities.roundDecimalPlaces(workItemCoverage.getInsuredValue(), 2));
					}
				}
			}
		}
		return retorno;
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.tratomais.core.service.impl.CalculationServiceAbs#calculate(br.com.tratomais.core.model.policy.Endorsement)
	 */
	@Override
	protected void dataFillCalc(Endorsement endorsement, boolean isRecalculation) {
		// Data fill the contract
		this.dataFillCalcContract(endorsement, isRecalculation);

		// Data fill the endorsement
		this.dataFillCalcEndorsement(endorsement, isRecalculation);
		
		// Order the Item
		TreeSet<Item> treeItem = orderItem(endorsement.getItems());
		
		// Data fill the items
		for (Item item : treeItem) {
			this.dataFillCalcItem(item, isRecalculation);
			
			// Data fill the beneficiaries
			this.dataFillBeneficiary(item);
			
			// Order the coverage
			TreeSet<ItemCoverage> treeItemCoverage = orderItemCoverage(item.getItemCoverages());
			
			// Data fill the coverage
			for(ItemCoverage itemCoverage : treeItemCoverage) {
				this.dataFillCalcCoverage(itemCoverage, isRecalculation);
			}
			
			// Data fill the clause
			this.dataFillCalcClause(item, isRecalculation);
		}		 
	}
	
	/**
	 * dataFill endorsement
	 * @param endorsement
	 * @param isRecalculation
	 * @return
	 */
	private void dataFillCalcContract(Endorsement endorsement, boolean isRecalculation) {
		Contract contract = endorsement.getContract();
		
		if (contract != null) {
			// Check if reference date by contract
			if (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_EMISSION) {
				if (contract.getReferenceDate() == null) {
					// Get version to effective date 
					ProductVersion productVersion = daoProductVersion.getProductVersionByBetweenRefDate(contract.getProductId(), contract.getEffectiveDate());
					if (productVersion != null) {
						// Set the reference date
						contract.setReferenceDate(productVersion.getReferenceDate());
					}
				}
			}
			
			// Check the term ID
			Term term = daoCalculo.getTermById(contract.getTermId());
			if (term != null) {
				// Set the expiry date
				if (contract.getEffectiveDate() != null) {
					contract.setExpiryDate(dataFillExpiryDate(term, contract.getEffectiveDate(), contract.getExpiryDate()));
				}
				
				// Set the principal dates
				if (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_EMISSION) {
					endorsement.setTermId(contract.getTermId());
					endorsement.setEffectiveDate(contract.getEffectiveDate());
				}
				endorsement.setExpiryDate(contract.getExpiryDate());
			}
		}
	}
	
	/**
	 * dataFill endorsement
	 * @param endorsement
	 * @param isRecalculation
	 * @return
	 */
	private void dataFillCalcEndorsement(Endorsement endorsement, boolean isRecalculation) {
		Contract contract = endorsement.getContract();
		
		if (contract != null) {
			Date refDate = endorsement.getReferenceDate();
			
			// Data fill the insured
			this.dataFillCustomer(endorsement.getCustomerByInsuredId());
			
			// Data fill the policy holder
			this.dataFillCustomer(endorsement.getCustomerByPolicyHolderId());
			
			// Clear installments
			endorsement.getInstallments().clear();
			
			// Update the keys to cover the complete listing error
			printKeys(endorsement);
			for (Item item : endorsement.getItems()) {
				item.updateKey(endorsement);
			}
			printKeys(endorsement);
			
			// Check if reference date by contract
			if (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_EMISSION) {
				refDate = contract.getReferenceDate();
			}
			
			// Product effective for calculating
			ProductVersion productVersion = daoProductVersion.getProductVersionByRefDate(contract.getProductId(), refDate);
			if (productVersion != null) {
				final Product product = productVersion.getProduct();
				
				// Set the reference date
				endorsement.setReferenceDate(productVersion.getReferenceDate());
				
				// Check the default commission factor
				if (this.parseNull(endorsement.getCommissionFactor()).equals(0.0)) {
					endorsement.setCommissionFactor(productVersion.getDefaultCommissionFactor());
				}
				
				// Get master policy effective
				MasterPolicy masterPolicy = daoMasterPolicy.findById(contract.getMasterPolicyId());
				
				// Set the commission factor
				if (product.isMasterPolicyRequired() && masterPolicy != null) {
					endorsement.setCommissionFactor(masterPolicy.getCommissionFactor());
					endorsement.setLabourFactor(masterPolicy.getLabourFactor());
					endorsement.setAgencyFactor(masterPolicy.getAgencyFactor());
				}
				
				// Set additional data
				contract.setPolicyInformed((masterPolicy != null && masterPolicy.isPolicyInformed()));
				contract.setCurrencyId(product.getCurrencyId());
				
				// Set conversion rate
				ConversionRate conversionRate = daoPolicy.findConversionRateByRefDate(contract.getCurrencyId(), endorsement.getEffectiveDate());
				if (conversionRate != null) {
					endorsement.setCurrencyDate(conversionRate.getCurrencyDate());
					endorsement.setConversionRate(conversionRate.getMultipleRateBy());
				} else {
					endorsement.setConversionRate(1.0);
				}
			}
			
			/*
			// Check if recalculation
			if (!isRecalculation) {
				// Get the main item for endorsement
				Item mainItem = endorsement.getMainItem();
				if (mainItem.getObjectId() == Item.OBJECT_CICLO_VITAL && endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_EMISSION) {
					// Clear other items
 					Iterator<Item> iterator = endorsement.getItems().iterator();
					while (iterator.hasNext()) {
						Item workItem = iterator.next();
						if (workItem != mainItem) {
							iterator.remove();
						}
					}
					
					// Get the main item for personal risk
					ItemPersonalRisk mainItemPersonalRisk = (ItemPersonalRisk) mainItem;
					
					// Create the item additional
					if (mainItemPersonalRisk.getTransientQuantityAdditional() > 0) {
						List<CoveragePersonalRisk> listCoveragePersonalRisk = 
							daoProduct.listCoveragePersonalRiskByPersonalRiskType(
									mainItemPersonalRisk.getEndorsement().getContract().getProductId(),
									mainItemPersonalRisk.getCoveragePlanId(),
									ItemPersonalRisk.PERSONAL_RISK_TYPE_ADDITIONAL,
									mainItemPersonalRisk.getEndorsement().getReferenceDate());
						
						for (int i = 0; i < mainItemPersonalRisk.getTransientQuantityAdditional(); i++) {
							createAdditionalPersonalRisk(
									mainItemPersonalRisk,
									ItemPersonalRisk.PERSONAL_RISK_TYPE_ADDITIONAL,
									listCoveragePersonalRisk);
						}
					}
					
					// Create the item newborn
					if (mainItemPersonalRisk.getTransientQuantityNewborn() > 0) {
						List<CoveragePersonalRisk> listCoveragePersonalRisk = 
							daoProduct.listCoveragePersonalRiskByPersonalRiskType(
									mainItemPersonalRisk.getEndorsement().getContract().getProductId(),
									mainItemPersonalRisk.getCoveragePlanId(),
									ItemPersonalRisk.PERSONAL_RISK_TYPE_NEWBORN,
									mainItemPersonalRisk.getEndorsement().getReferenceDate());
						
						for (int i = 0; i < mainItemPersonalRisk.getTransientQuantityNewborn(); i++) {
							createAdditionalPersonalRisk(
									mainItemPersonalRisk,
									ItemPersonalRisk.PERSONAL_RISK_TYPE_NEWBORN,
									listCoveragePersonalRisk);
						}
					}					
				}
			}
			*/
		}
	}
	
	/**
	 * dataFill item
	 * @param item
	 * @param isRecalculation
	 * @return
	 */
	private void dataFillCalcItem(Item item, boolean isRecalculation) {
		Endorsement endorsement = item.getEndorsement();
		Contract contract = endorsement.getContract();
		final Date refDate = endorsement.getReferenceDate();
		
		// Set the commission factor
		item.setCommissionFactor(endorsement.getCommissionFactor());
		item.setLabourFactor(endorsement.getLabourFactor());
		item.setAgencyFactor(endorsement.getAgencyFactor());
		
		// Get the list coverage plan
		ProductPlan productPlan = daoProduct.getProductPlanById(contract.getProductId(), item.getCoveragePlanId(), refDate);
		
		// Get the coverage plan Standard
		if (productPlan == null) {
			productPlan = daoProduct.getProductPlanByStandard(contract.getProductId(), endorsement.getRiskPlanId(), refDate);
			if (productPlan != null) {
				// Set the New coverage plan
				item.setCoveragePlanId(productPlan.getId().getPlanId());
			}
		}
		
		if (productPlan != null) {
			// Get the risk value from object
			switch (item.getObjectId()) {
				case Item.OBJECT_VIDA:
				case Item.OBJECT_CREDITO:
				case Item.OBJECT_CICLO_VITAL:
				case Item.OBJECT_ACCIDENTES_PERSONALES:
				case Item.OBJECT_PURCHASE_PROTECTED:
					ItemPersonalRisk itemPersonalRisk = (ItemPersonalRisk)item;
					// Check the fixed insured value
					if (!this.parseNull(productPlan.getFixedInsuredValue()).equals(0.0)) {
						// Set the fixed risk value
						itemPersonalRisk.setPersonalRiskValue(productPlan.getFixedInsuredValue());
					} else {
						// Set the risk value from basic coverage
						itemPersonalRisk.setPersonalRiskValue(item.getBasicCoverageValue(null));
					}
					
					// Data fill the main item 
					this.dataFillPersonalRisk(endorsement.getCustomerByInsuredId(), itemPersonalRisk);
					
					// Data fill the item risk group
					this.dataFillPersonalRiskGroup(itemPersonalRisk);
					
					break;
					
				case Item.OBJECT_AUTO:
					ItemAuto itemAuto = (ItemAuto)item;
					
					// Set the auto version data
					AutoVersion autoVersion = serviceAuto.findAutoVersion(itemAuto.getAutoModelId(), refDate);
					if (autoVersion != null) {
						itemAuto.setAutoTableType(AutoRefer.AUTO_TABLE_TYPE_MAIN);
						itemAuto.setAutoClassType(autoVersion.getAutoClassType());
						itemAuto.setAutoTariffType(autoVersion.getAutoTariffType());
						
						if (autoVersion.getAutoCategoryType() != null)
							itemAuto.setAutoCategoryType(autoVersion.getAutoCategoryType());

						if (autoVersion.getAutoGroupType() != null)
							itemAuto.setAutoGroupType(autoVersion.getAutoGroupType());
						
						//itemAuto.setAutoUseType(autoVersion.getAutoUseType());
						
						//Get y set auto value
						Double valueRefer = serviceAuto.getAutoCost(itemAuto.getAutoModelId(), itemAuto.getYearModel(), refDate);
						if (valueRefer != null)
							itemAuto.setValueRefer(NumberUtilities.roundDecimalPlaces(valueRefer, 0));
					}
					
					// Validate the Motorist data
					this.dataFillMotorist(itemAuto);
					
					break;
			}
		}
		
		// Get the list coverage plan
		List <CoveragePlan> listCoveragePlan = daoProduct.listCoveragePlanByRefDate(contract.getProductId(), item.getCoveragePlanId(), refDate);
		if (listCoveragePlan != null && listCoveragePlan.size() > 0) {
			// Insert coverages
			for (CoveragePlan coveragePlan : listCoveragePlan) {
				// Data fill the coverage
				boolean findCoverage = false;
				if (coveragePlan.isCompulsory()) {
					for(ItemCoverage itemCoverage : item.getItemCoverages()) {
						if (itemCoverage.getId().getCoverageId() == coveragePlan.getId().getCoverageId()) {
							findCoverage = true;
							break;
						}
					}
					
					// Insert compulsory coverage 
					ItemCoverage itemCoverage = null;
					if (!findCoverage) {
						if (!this.parseNull(coveragePlan.getCoverage().getRiskType()).equals(0)) {
							boolean findRiskType = false;
							for (ItemRiskType itemRiskType : item.getItemRiskTypes()) {
								if (itemRiskType.getId().getRiskType() == coveragePlan.getCoverage().getRiskType()) {
									findRiskType = true;
								}
							}
							if (findRiskType) {
								itemCoverage = item.addCoverage(coveragePlan.getId().getCoverageId());
								itemCoverage.setDisplayOrder(coveragePlan.getDisplayOrder());
							}
						}
						else {
							itemCoverage = item.addCoverage(coveragePlan.getId().getCoverageId());
							itemCoverage.setDisplayOrder(coveragePlan.getDisplayOrder());
						}
					}
				}
			}
			
			// Delete coverage not exists in plan
			Iterator<ItemCoverage> iterator = item.getItemCoverages().iterator();
			while (iterator.hasNext()) {
				ItemCoverage itemCoverage = iterator.next();
				// Data fill the coverage
				boolean findCoverage = false;
				for (CoveragePlan coveragePlan : listCoveragePlan) {
					if (itemCoverage.getId().getCoverageId() == coveragePlan.getId().getCoverageId()) {
						findCoverage = true;
						break;
					}
				}
				if (!findCoverage)
					iterator.remove();
			}
		}
	}

	/**
	 * dataFill clause
	 * @param item
	 * @param isRecalculation
	 * @return
	 */
	private void dataFillCalcClause(Item item, boolean isRecalculation) {
		// Data fill the clauses
		item.removeAllClauseAutomatic();
		
		// Checks y apply automatic Clause
		serviceRule.checkRuleClauseAutomatic(item);
	}
	
	/**
	 * dataFill coverage
	 * @param itemCoverage
	 * @param isRecalculation
	 * @return
	 */
	private void dataFillCalcCoverage(ItemCoverage itemCoverage, boolean isRecalculation) {
		Item item = itemCoverage.getItem();
		Endorsement endorsement = item.getEndorsement();
		Contract contract = endorsement.getContract();
		ItemPersonalRisk itemPersonalRisk = null;
		ItemProperty itemProperty = null;
		CoverageRangeValue coverageRangeValue = null;
		final Date refDate = endorsement.getReferenceDate();
		final int coverageId = itemCoverage.getId().getCoverageId();
		Double riskValue = 0d;
		Integer riskType = null;
		
		// Set the Transient values
		item.setTCoverageId(itemCoverage.getId().getCoverageId());
		item.setTRangeValueId(itemCoverage.getRangeValueId());
		item.setTDeductibleId(this.parseNull(itemCoverage.getDeductibleId()));
		
		// Clean coverage properties
		itemCoverage.cleanValueAndFactor();
		itemCoverage.limpaPremios();
		
		// Get the coverage plan
		CoveragePlan coveragePlan = daoCoveragePlan.getCoveragePlanByRefDate(contract.getProductId(), item.getCoveragePlanId(), coverageId, refDate);
		if (coveragePlan != null) {
			// Get the coverage data
			Coverage coverage = coveragePlan.getCoverage();
			
			// Set then complementary data
			itemCoverage.setCoverageCode(coverage.getCoverageCode());
			itemCoverage.setCoverageName(coveragePlan.getNickName());
			itemCoverage.setBasicCoverage(coverage.isBasic());
			itemCoverage.setGroupCoverageId(coverage.getRiskType());
			itemCoverage.setGoodsCode(coverage.getGoodsCode());
			itemCoverage.setBranchId(coveragePlan.getSubBranchId());
			itemCoverage.setDisplayOrder(coveragePlan.getDisplayOrder());
			
			// Set the commission factor from rules/object
			switch (coveragePlan.getCommissionIncidenceType()) {
				case CoveragePlan.COMMISSION_INCIDENCE_TYPE_NOT_APPLY:
					itemCoverage.setCommissionFactor(0.0);
					itemCoverage.setLabourFactor(0.0);
					itemCoverage.setAgencyFactor(0.0);
					break;
					
				case CoveragePlan.COMMISSION_INCIDENCE_TYPE_NET_PREMIUM:
				case CoveragePlan.COMMISSION_INCIDENCE_TYPE_WEIGHTED:
					itemCoverage.setCommissionFactor(item.getCommissionFactor());
					itemCoverage.setLabourFactor(item.getLabourFactor());
					itemCoverage.setAgencyFactor(item.getAgencyFactor());
					break;
			}
			
			// Set the insured value from rules/object
			switch (coveragePlan.getInsuredValueType()) {
				case CoveragePlan.INSURED_VALUE_TYPE_CSA:
					itemCoverage.setRangeValueId(-1);
					break;
					
				case CoveragePlan.INSURED_VALUE_TYPE_CSR:
					itemCoverage.setRangeValueId(-1);
					itemCoverage.setInsuredValue(serviceCoverage.applyCoverageRule(item, CoverageRule.RULE_TYPE_SA, contract.getProductId(), coverageId, refDate));
					break;
					
				case CoveragePlan.INSURED_VALUE_TYPE_CSP:
					itemCoverage.setRangeValueId(-1);
					itemCoverage.setInsuredValue(0.0);
					
					// Get the object
					switch (item.getObjectId()) {
						case Item.OBJECT_AUTO:
							ItemAuto itemAuto = (ItemAuto)item;
							int passengerNumber = this.parseNull(itemAuto.getPassengerNumber());
							itemCoverage.setInsuredValue(NumberUtilities.roundDecimalPlaces((coveragePlan.getFixedInsuredValue() * passengerNumber), 2));
							break;
					}
					break;
					
				case CoveragePlan.INSURED_VALUE_TYPE_SSA:
					itemCoverage.setRangeValueId(-1);
					itemCoverage.setInsuredValue(0.0);
					break;
					
				case CoveragePlan.INSURED_VALUE_TYPE_SAF:
					itemCoverage.setRangeValueId(-1);
					itemCoverage.setInsuredValue(NumberUtilities.roundDecimalPlaces(coveragePlan.getFixedInsuredValue(), 2));
					
					// Check if basic coverage
					if (itemCoverage.isBasicCoverage()) {
						// Get the risk value from object
						switch (item.getObjectId()) {
							case Item.OBJECT_VIDA:
							case Item.OBJECT_CREDITO:
							case Item.OBJECT_CICLO_VITAL:
							case Item.OBJECT_ACCIDENTES_PERSONALES:
							case Item.OBJECT_PURCHASE_PROTECTED:
								itemPersonalRisk = (ItemPersonalRisk)item;
								// Set the risk value from basic coverage
								itemPersonalRisk.setPersonalRiskValue(itemCoverage.getInsuredValue());
								break;
						}
					}
					break;
					
				case CoveragePlan.INSURED_VALUE_TYPE_RSA:
					coverageRangeValue = daoProduct.getCoverageRangeValueById(contract.getProductId(), item.getCoveragePlanId(), coverageId, itemCoverage.getRangeValueId(), refDate);
					if (coverageRangeValue != null) {
						itemCoverage.setInsuredValue(NumberUtilities.roundDecimalPlaces(coverageRangeValue.getRangeValue(), 2));
					}
					break;
					
				case CoveragePlan.INSURED_VALUE_TYPE_PVR:
					// Get the risk value from object
					switch (item.getObjectId()) {
						case Item.OBJECT_VIDA:
						case Item.OBJECT_CREDITO:
						case Item.OBJECT_CICLO_VITAL:
						case Item.OBJECT_ACCIDENTES_PERSONALES:
						case Item.OBJECT_PURCHASE_PROTECTED:
							itemPersonalRisk = (ItemPersonalRisk)item;
							riskValue = this.parseNull(itemPersonalRisk.getPersonalRiskValue());
							break;
							
						case Item.OBJECT_HABITAT:
							itemProperty = (ItemProperty)item;
							switch (coverage.getRiskType()) {
								case Coverage.RISKTYPE_CONTENT:
									riskValue = this.parseNull(itemProperty.getContentValue());
									riskType = coverage.getRiskType();
									break;
								case Coverage.RISKTYPE_STRUCTURE:
									riskValue = this.parseNull(itemProperty.getStructureValue());
									riskType = coverage.getRiskType();
									break;
								case Coverage.RISKTYPE_BOTH:
									riskValue =  this.parseNull(itemProperty.getContentValue());
									riskValue += this.parseNull(itemProperty.getStructureValue());
									break;
							}
							break;
							
						case Item.OBJECT_CAPITAL:
							itemProperty = (ItemProperty)item;
							riskType = itemCoverage.getGroupCoverageId();
							ItemRiskType itemRiskType = item.findRiskType(riskType);
							if (itemRiskType != null) {
								riskValue = this.parseNull(itemRiskType.getRiskTypeValue());
							}
							break;
							
						case Item.OBJECT_AUTO:
							ItemAuto itemAuto = (ItemAuto)item;
							riskValue = this.parseNull(itemAuto.getValueRefer());
							break;
					}
					// Review the risk value
					riskValue = (riskValue != 0d ? riskValue : item.getBasicCoverageValue(riskType));
					
					itemCoverage.setRangeValueId(-1);
					if (!this.parseNull(coveragePlan.getFactorRiskValue()).equals(0.0)) {
						itemCoverage.setInsuredValue(NumberUtilities.roundDecimalPlaces(coveragePlan.getFactorRiskValue() * riskValue, 2));
					}
					
					// check the insured value limit
					switch (this.parseNull(coveragePlan.getInsuredValueLimitType())) {
						case CoveragePlan.LIMIT_TYPE_FIXED_VALUE:
							// fill the minimum insured value
							if (!this.parseNull(coveragePlan.getMinimumInsuredValue()).equals(0d)) {
								if (NumberUtilities.roundDecimalPlaces(itemCoverage.getInsuredValue(), Calculus.RESULT_DECIMAL_PLACES) < NumberUtilities.roundDecimalPlaces(coveragePlan.getMinimumInsuredValue(), Calculus.RESULT_DECIMAL_PLACES)) {							
									itemCoverage.setInsuredValue(coveragePlan.getMinimumInsuredValue());
								}
							}
							
							// fill the maximum insured value
							if (!this.parseNull(coveragePlan.getMaximumInsuredValue()).equals(0d)) {
								if (NumberUtilities.roundDecimalPlaces(itemCoverage.getInsuredValue(), Calculus.RESULT_DECIMAL_PLACES) > NumberUtilities.roundDecimalPlaces(coveragePlan.getMaximumInsuredValue(), Calculus.RESULT_DECIMAL_PLACES)) {
									itemCoverage.setInsuredValue(coveragePlan.getMaximumInsuredValue());
								}
							}
							break;
							
						case CoveragePlan.LIMIT_TYPE_RULE_LMI:
							// fill the maximum insured value
							Double maximumInsuredRule = serviceCoverage.applyCoverageRule(item, CoverageRule.RULE_TYPE_SA, contract.getProductId(), coverageId, refDate);
							if (!this.parseNull(maximumInsuredRule).equals(0d)) {
								if (NumberUtilities.roundDecimalPlaces(itemCoverage.getInsuredValue(), Calculus.RESULT_DECIMAL_PLACES) > NumberUtilities.roundDecimalPlaces(maximumInsuredRule, Calculus.RESULT_DECIMAL_PLACES)) {
									itemCoverage.setInsuredValue(maximumInsuredRule);
								}
							}
							break;
					}
					break;
					
				case CoveragePlan.INSURED_VALUE_TYPE_PMR:
					Double insuredValueRelationship = this.getInsuredValueCoverageRelationship(itemCoverage);
					riskValue = this.parseNull(insuredValueRelationship);
					
					itemCoverage.setRangeValueId(-1);
					if ((coveragePlan.getFactorRiskValue() != null) && (!coveragePlan.getFactorRiskValue().equals(0.0))) {
						itemCoverage.setInsuredValue(NumberUtilities.roundDecimalPlaces(coveragePlan.getFactorRiskValue() * riskValue, 2));
					}
					break;
					
				case CoveragePlan.INSURED_VALUE_TYPE_RSI:
					coverageRangeValue = daoProduct.getCoverageRangeValueByInsuredValue(contract.getProductId(), item.getCoveragePlanId(), coverageId, itemCoverage.getInsuredValue(), refDate);
					if (coverageRangeValue != null) {
						itemCoverage.setRangeValueId(coverageRangeValue.getId().getRangeId());
					}
					break;
			}
			
			// Review the range value
			if ((itemCoverage.getRangeValueId() == -1) && 
				(coveragePlan.getInsuredValueType() != CoveragePlan.INSURED_VALUE_TYPE_SSA)) {
				coverageRangeValue = daoProduct.getCoverageRangeValueByInsuredValue(contract.getProductId(), item.getCoveragePlanId(), coverageId, itemCoverage.getInsuredValue(), refDate);
				if (coverageRangeValue != null) {
					itemCoverage.setRangeValueId(coverageRangeValue.getId().getRangeId());
				}
			}
		}
	}
	
	/**
	 * dataFill expiryDate
	 * @param term
	 * @param effectiveDate
	 * @param expiryDate
	 * @return expiryDate
	 */	
	private Date dataFillExpiryDate(Term term, Date effectiveDate, Date expiryDate) {
		switch (term.getMultipleType()) {
			case Term.MULTIPLE_TYPE_YEAR:
				expiryDate = DateUtilities.addYears(effectiveDate, term.getMultipleQuantity());
				break;
			case Term.MULTIPLE_TYPE_MONTH:
				expiryDate = DateUtilities.addMonths(effectiveDate, term.getMultipleQuantity());
				break;
			case Term.MULTIPLE_TYPE_DAY:
				if (!term.getModifyPermit()) {
					expiryDate = DateUtilities.addDays(effectiveDate, term.getMultipleQuantity());
				}
				break;
		}
		return expiryDate;
	}
	
	/**
	 * Used to do the comparator y order item
	 */
	private Comparator<Item> comparatorItem = new Comparator<Item>() {
		/** {@inheritDoc} */
		@Override
		public int compare(Item arg0, Item arg1) {
			/* Regra b�sica: retorna -1 se menor, 0 se igual, 1 se maior */
			int result = arg0.getId().getItemId() - arg1.getId().getItemId();
			if (result != 0)
				return result;
			return result;
		}
	};
	
	/**
	 * Used to do the order item
	 */
	private TreeSet<Item> orderItem(Set<Item> itens) {
		TreeSet<Item> treeSet = null;
		treeSet = new TreeSet<Item>(comparatorItem);
		treeSet.addAll(itens);
		return treeSet;
	}
	
	/**
	 * Used to do the comparator y order item risk group
	 */
	private Comparator<ItemPersonalRiskGroup> comparatorItemRiskGroup = new Comparator<ItemPersonalRiskGroup>() {
		/** {@inheritDoc} */
		@Override
		public int compare(ItemPersonalRiskGroup arg0, ItemPersonalRiskGroup arg1) {
			/* Regra b�sica: retorna -1 se menor, 0 se igual, 1 se maior */
			int result = arg0.getId().getRiskGroupId() - arg1.getId().getRiskGroupId();
			if (result != 0)
				return result;
			return result;
		}
	};
	
	/**
	 * Used to do the order item risk group
	 */
	private TreeSet<ItemPersonalRiskGroup> orderItemRiskGroup(Set<ItemPersonalRiskGroup> itemRiskGroups) {
		TreeSet<ItemPersonalRiskGroup> treeSet = null;
		treeSet = new TreeSet<ItemPersonalRiskGroup>(comparatorItemRiskGroup);
		treeSet.addAll(itemRiskGroups);
		return treeSet;
	}
	
	/**
	 * Used to do the comparator y order coverage
	 */
	private Comparator<ItemCoverage> comparatorItemCoverage = new Comparator<ItemCoverage>() {
		/** {@inheritDoc} */
		@Override
		public int compare(ItemCoverage arg0, ItemCoverage arg1) {
			/* Regra b�sica: retorna -1 se menor, 0 se igual, 1 se maior */
			int result = arg0.getDisplayOrder() - arg1.getDisplayOrder();
			if (result != 0)
				return result;
			result = arg0.getId().getCoverageId() - arg1.getId().getCoverageId();
			if (result != 0)
				return result;
			return result;
		}
	};
	
	/**
	 * Used to do the order coverage
	 */
	private TreeSet<ItemCoverage> orderItemCoverage(Set<ItemCoverage> itemCoverages) {
		TreeSet<ItemCoverage> treeSet = null;
		treeSet = new TreeSet<ItemCoverage>(comparatorItemCoverage);
		treeSet.addAll(itemCoverages);
		return treeSet;
	}
	
	/**
	 * Used to do the comparator y order profile
	 */
	private Comparator<ItemProfile> comparatorItemProfile = new Comparator<ItemProfile>() {
		/** {@inheritDoc} */
		@Override
		public int compare(ItemProfile arg0, ItemProfile arg1) {
			/* Regra b�sica: retorna -1 se menor, 0 se igual, 1 se maior */
			int result = arg0.getDisplayOrder() - arg1.getDisplayOrder();
			if (result != 0)
				return result;
			result = arg0.getId().getQuestionnaireId() - arg1.getId().getQuestionnaireId();
			if (result != 0)
				return result;
			result = arg0.getId().getQuestionId() - arg1.getId().getQuestionId();
			if (result != 0)
				return result;
			result = arg0.getId().getResponseId() - arg1.getId().getResponseId();
			if (result != 0)
				return result;
			return result;
		}
	};
	
	/**
	 * Used to do the order coverage
	 */
	private TreeSet<ItemProfile> orderItemProfile(Set<ItemProfile> itemProfiles) {
		TreeSet<ItemProfile> treeSet = null;
		treeSet = new TreeSet<ItemProfile>(comparatorItemProfile);
		treeSet.addAll(itemProfiles);
		return treeSet;
	}
	
//	/**
//	 * Creates an invalid product error message
//	 */
//	private void invalidProductVersion(Endorsement endorsement) {
//		endorsement.getErrorList().add(ErrorTypes.VALIDATION_PRODUCT_VERSION_INVALID, null, null, "Calculation Service", 0);
//	}
//	
//	/**
//	 * @param mainPersonalRisk
//	 * @param workPersonalRisk
//	 */
//	private void createAdditionalPersonalRisk(ItemPersonalRisk mainPersonalRisk, int personalRiskType, List<CoveragePersonalRisk> listCoveragePersonalRisk) {
//		ItemPersonalRisk itemPersonalRisk = (ItemPersonalRisk) mainPersonalRisk.getEndorsement().addItem(Item.OBJECT_CICLO_VITAL);
//		
//		itemPersonalRisk.setPersonalRiskType(personalRiskType);
//		itemPersonalRisk.setCoveragePlanId(mainPersonalRisk.getCoveragePlanId());
//		itemPersonalRisk.setRenewalType(mainPersonalRisk.getRenewalType());
//		itemPersonalRisk.setEffectiveDate(mainPersonalRisk.getEffectiveDate());
//		itemPersonalRisk.setCalculationValid(mainPersonalRisk.isCalculationValid());
//		itemPersonalRisk.setItemStatus(mainPersonalRisk.getItemStatus());
//		itemPersonalRisk.setInsuredName("llenar");
//		
//		for (ItemCoverage itemCoverage : mainPersonalRisk.getItemCoverages()) {
//			if (isCorverageAllowed(listCoveragePersonalRisk, itemCoverage)) {
//				ItemCoverage workCoverage = itemPersonalRisk.addCoverage(itemCoverage.getId().getCoverageId());
//				String[] ignoreProperties = { "id", "item" };
//				BeanUtils.copyProperties(itemCoverage, workCoverage, ignoreProperties);
//			}
//		}
//	}
//
//	/**
//	 * @param listCoveragePersonalRisk
//	 * @param itemCoverage
//	 * @return
//	 */
//	private boolean isCorverageAllowed(List<CoveragePersonalRisk> listCoveragePersonalRisk, ItemCoverage itemCoverage) {
//		boolean isCorverageAllowed = false;
//		for (CoveragePersonalRisk coveragePersonalRisk : listCoveragePersonalRisk) {
//			if (itemCoverage.getId().getCoverageId() == coveragePersonalRisk.getId().getCoverageId()) {
//				isCorverageAllowed = true;
//			}
//		}
//		return isCorverageAllowed;
//	}	

	/**
	 * Return payment options for list of payments
	 * 
	 * @param netPremiumAndCosts
	 * @param payments
	 * @param dtExpiry 
	 * @param dtEffective 
	 * @return
	 */
	List<PaymentOption> getPaymentOptions(double totalPremium, double taxRate, List<ProductPaymentTerm> payments, Date dtEffective, Date dtExpiry, Date refDate) {
		List<PaymentOption> retorno = new ArrayList<PaymentOption>();
		
		// Set unique Installment when Refund
		if (totalPremium == 0) {
			return retorno;
		}
		else if (totalPremium < 0) {
			PaymentOption paymentOption = new PaymentOption();
			paymentOption.setQuantityInstallment(1);
			paymentOption.setFirstInstallmentValue(totalPremium);
			paymentOption.setNextInstallmentValue(0.0);
			paymentOption.setPaymentId(Endorsement.PAYMENT_TERM_UNIQUE);
			paymentOption.setBillingMethodId(Endorsement.BILLING_METHOD_ZURICH);
			paymentOption.setPaymentTermName("�nica");
			paymentOption.setFractioningAdditional(0.0);
			paymentOption.setTotalPremium(totalPremium);
			paymentOption.setTaxRate(0.0);
			paymentOption.setDefault(true);
			retorno.add(paymentOption);
			return retorno;
		}
		
		double total, taxValue, netPremium, fractioningAditional;
		DiferencaData dateDifference = DateUtilities.getDateDifference(dtEffective, dtExpiry);
		for (ProductPaymentTerm prodPay : payments) {
			// Consulting Product version
			ProductVersion productVersion = daoProductVersion.getProductVersionByRefDate(prodPay.getId().getProductId(), refDate);			
			PaymentTerm pay = prodPay.getPaymentTerm();
			
			if (taxRate == 0.0) { taxRate = 1d; }
			fractioningAditional = 0d;
			total = NumberUtilities.roundDecimalPlaces(totalPremium, Calculus.RESULT_DECIMAL_PLACES);
			netPremium = NumberUtilities.roundDecimalPlaces(totalPremium / taxRate, Calculus.RESULT_DECIMAL_PLACES);
			taxValue = NumberUtilities.roundDecimalPlaces(totalPremium - netPremium, Calculus.RESULT_DECIMAL_PLACES);

			//----------INI
			int totalDays = DateUtilities.diffDateInDays(dtEffective, dtExpiry);
			int diffPeriod  = ((pay.getQuantityInstallment() == 1?1:pay.getNextInstallment()) * (dateDifference.getMeses() / (pay.getQuantityInstallment() == 1?1:pay.getNextInstallment())));
			Date nextPaymentDate = (diffPeriod==0?dtEffective:DateUtilities.dateInFuture(dtExpiry, DateUtilities.MENSAL, (0 - diffPeriod)));
			int diffDays = DateUtilities.diffDateInDays(dtEffective, nextPaymentDate);
			int qtdeParcelas = ((pay.getQuantityInstallment() == 1) ? 1 : (dateDifference.getMeses() / pay.getNextInstallment()));
			ApplicationProperty applicationProperty = daoPolicy.loadApplicationProperty(1, "DaysToleranceInstallment");
			int daysToleranceInstallment = (applicationProperty!=null?Integer.parseInt(applicationProperty.getValue()):15);
			boolean diffNextParcela = (diffDays >= daysToleranceInstallment);
			if (pay.getQuantityInstallment() > 1 && diffNextParcela) {
				qtdeParcelas = (qtdeParcelas + 1);
			} else nextPaymentDate = (pay.getQuantityInstallment() == 1)?null:DateUtilities.dateInFuture(nextPaymentDate, pay.getNextMultipleType(), pay.getNextInstallment());
			//----------FIM
			
			if ((pay.getQuantityInstallment() == 1) ||
				(pay.getQuantityInstallment() > 1 && qtdeParcelas > 1)) {
				
				PaymentOption paymentOption = new PaymentOption();
				paymentOption.setQuantityInstallment(pay.getQuantityInstallment());
				paymentOption.setNextInstallment(pay.getNextInstallment());
				paymentOption.setBillingMethodId(prodPay.getId().getBillingMethodId());

				if (pay.getQuantityInstallment() > 1) {
					double demais = 0.0;
					double primeira = 0.0;

					paymentOption.setQuantityInstallment(qtdeParcelas);
					paymentOption.setNextPaymentDate(nextPaymentDate);
					
					if (diffDays != 0) {
						demais = NumberUtilities.roundDecimalPlaces((total - (diffDays * (total / totalDays))) / (qtdeParcelas -(diffNextParcela?1:0)), Calculus.RESULT_DECIMAL_PLACES);
						primeira = NumberUtilities.roundDecimalPlaces(total
								- NumberUtilities.roundDecimalPlaces(demais * (qtdeParcelas -1),
									Calculus.RESULT_DECIMAL_PLACES),
									Calculus.RESULT_DECIMAL_PLACES);
					}
					else {
						demais = NumberUtilities.roundDecimalPlaces(total / qtdeParcelas, Calculus.RESULT_DECIMAL_PLACES);
						primeira = NumberUtilities.roundDecimalPlaces(total
										- NumberUtilities.roundDecimalPlaces(demais * (qtdeParcelas - 1),
											Calculus.RESULT_DECIMAL_PLACES),
											Calculus.RESULT_DECIMAL_PLACES);
					}
					
					// Check value Minimum Installment
					if ((productVersion.getMinimumInstallment() != null) && (!productVersion.getMinimumInstallment().equals(0.0)))
						if (demais < productVersion.getMinimumInstallment())
							continue;
					
					paymentOption.setFirstInstallmentValue(primeira);
					paymentOption.setNextInstallmentValue(demais);
				} else {
					paymentOption.setFirstInstallmentValue(total);
					paymentOption.setNextInstallmentValue(0.0);
				}
				paymentOption.setPaymentTermName(pay.getName());
				paymentOption.setDefault(prodPay.isPaymentTermDefault());
				paymentOption.setPaymentId(pay.getPaymentTermId());
				paymentOption.setFractioningAdditional(fractioningAditional);
				paymentOption.setTotalPremium(total);
				paymentOption.setNetPremium(netPremium);
				paymentOption.setTaxRate(taxRate);
				paymentOption.setTaxValue(taxValue);
				paymentOption.setSelected(false);
				retorno.add(paymentOption);
			} 
		}
		return retorno;
	}

	/**
	 * Payment Options per Endorsement
	 * 
	 * @param endorsement
	 * @return List of Payment Options
	 */
	public List<PaymentOption> getPaymentOptionsDefault(Endorsement endorsement) {
		endorsement.flush();
		return this.getPaymentOptionsDefault(
				endorsement.getTotalPremium(),
				endorsement.getTaxRate(), 
				endorsement.getContract().getProductId(), 
				endorsement.getReferenceDate(), endorsement.getEffectiveDate(), endorsement.getExpiryDate());
	}

	/**
	 * Payment Options
	 * 
	 * @param netPremiumAndCosts
	 *            Net premium and other costs (policy and inspection)
	 * @param productId
	 * @param refDate
	 * @param dtExpiry 
	 * @param DtEffective 
	 * @return List of Payment Options
	 */
	public List<PaymentOption> getPaymentOptionsDefault(Double totalPremium, double taxRate, int productId, Date refDate, Date DtEffective, Date dtExpiry) {
		return this.getPaymentOptions(totalPremium, taxRate, daoCalculo.getPaymentsTermsDefault(productId, refDate), DtEffective, dtExpiry, refDate);
	}

	/**
	 * Payment Options per Endorsement
	 * 
	 * @param endorsement
	 * @param billingMethodId
	 * @return List of Payment Options
	 */
	public List<PaymentOption> getPaymentOptionsByBillingMethodId(Endorsement endorsement, int billingMethodId, Date dtEffective, Date dtExpiry) {
		endorsement.flush();
		return this.getPaymentOptionsByBillingMethodId(endorsement.getTotalPremium(), endorsement.getTaxRate(), endorsement.getContract().getProductId(), billingMethodId, endorsement.getReferenceDate(), dtEffective, dtExpiry);
	}

	/**
	 * Payment Options
	 * 
	 * @param totalPremium
	 *            Net premium and other costs (policy and inspection)
	 * @param productId
	 * @param billingMethodId
	 * @param refDate
	 * @return List of Payment Options
	 */
	public List<PaymentOption> getPaymentOptionsByBillingMethodId(Double totalPremium, double taxRate, int productId, int billingMethodId, Date refDate, Date dtEffective, Date dtExpiry) {
		List<PaymentOption> paymentOptions = this.getPaymentOptions(totalPremium, taxRate, daoCalculo.getPaymentsTermsByBillingMethodId(productId, refDate, billingMethodId),dtEffective, dtExpiry, refDate);
		BillingMethod billingMethod = daoCalculo.getBillingMethod(billingMethodId);
		if (paymentOptions != null) {
			for (PaymentOption po : paymentOptions) {
				po.setBillingMethod(billingMethod);
			}
		}
		return paymentOptions;
	}

	/**
	 * Payment Option
	 * 
	 * @param totalPremium
	 *            Net premium and other costs (policy and inspection)
	 * @param productId
	 * @param billingMethodId
	 * @param paymentTermId
	 * @param refDate
	 * @param dtEffective
	 * @param dtExpiry
	 * @return Payment Option
	 */
	public PaymentOption getPaymentOptionByPaymentTermId(Double totalPremium, double taxRate, int productId, int billingMethodId, int paymentTermId, Date refDate, Date dtEffective, Date dtExpiry) {
		List<ProductPaymentTerm> productPaymentTerms = new ArrayList<ProductPaymentTerm>();
		productPaymentTerms.add(daoCalculo.getProductPaymentTerm(productId, refDate, billingMethodId, paymentTermId));
		if (productPaymentTerms.get(0) == null) {
			return null;
		}
		BillingMethod billingMethod = daoCalculo.getBillingMethod(billingMethodId);
		List<PaymentOption> paymentOptions = this.getPaymentOptions(totalPremium, taxRate, productPaymentTerms, dtEffective, dtExpiry, refDate);
		if (paymentOptions != null) {
			for (PaymentOption po : paymentOptions) {
				po.setBillingMethod(billingMethod);
			}
		}
		return paymentOptions.get(0);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.tratomais.core.service.ICalc#listInstallments(br.com.tratomais
	 * .core.model.policy.Endorsement)
	 */
	@Override
	public List<Installment> listInstallments(Endorsement endorsement) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private void dataFillCustomer(Customer customer) {
		// Data fill the customer
		if (customer != null) {
			if (customer.getPersonType() != null && customer.getPersonType() == Customer.PERSON_TYPE_NATURAL) {
				customer.setName((this.parseNull(customer.getFirstName()) + " " + this.parseNull(customer.getLastName())).trim());
			}
			
			// Data fill the Address for customer
			for (Address address : customer.getAddresses()) {
				Iterator<Phone> iterator = address.getPhones().iterator();
				while (iterator.hasNext()) {
					Phone phone = iterator.next();
					if ("".equals(this.parseNull(phone.getPhoneNumber())))
						iterator.remove();
				}
			}
		}
	}
	
	private void dataFillBeneficiary(Item item) {
		// Data fill the beneficiaries
		if (item.getBeneficiaries() != null) {
			Iterator<Beneficiary> iterator = item.getBeneficiaries().iterator();
			while (iterator.hasNext()) {
				Beneficiary workBeneficiary = iterator.next();
				if (workBeneficiary.getBeneficiaryType() == 0 || workBeneficiary.getFirstName() == null)
					iterator.remove();
				else {
					workBeneficiary.setName((this.parseNull(workBeneficiary.getFirstName()) + " " + this.parseNull(workBeneficiary.getLastName())).trim());
				}
			}
		}
	}
	
	private void dataFillPersonalRisk(Customer insured, ItemPersonalRisk itemPersonalRisk) {
		// Data fill the main item 
		if (itemPersonalRisk.getId().getItemId() == 1) {
			if (insured != null) {
				itemPersonalRisk.setFirstName(insured.getFirstName());
				itemPersonalRisk.setLastName(insured.getLastName());
				itemPersonalRisk.setBirthDate(insured.getBirthDate());
				itemPersonalRisk.setMaritalStatus(insured.getMaritalStatus());
				itemPersonalRisk.setGender(insured.getGender());
				itemPersonalRisk.setDocumentIssuer(insured.getDocumentIssuer());
				itemPersonalRisk.setDocumentNumber(insured.getDocumentNumber());
				itemPersonalRisk.setDocumentType(insured.getDocumentType());
				itemPersonalRisk.setDocumentValidity(insured.getDocumentValidity());
				itemPersonalRisk.setProfessionId(insured.getProfessionId());
				itemPersonalRisk.setOccupationId(insured.getOccupationId());
				itemPersonalRisk.setKinshipType(ItemPersonalRisk.PERSONAL_KINSHIP_TYPE_PROPRIO);
			}
		}
		itemPersonalRisk.setInsuredName((this.parseNull(itemPersonalRisk.getFirstName()) + " " + this.parseNull(itemPersonalRisk.getLastName())).trim());
	}
	
	private void dataFillPersonalRiskGroup(ItemPersonalRisk itemPersonalRisk) {
		// Data fill the item risk group
		for(ItemPersonalRiskGroup itemRiskGroup : itemPersonalRisk.getItemPersonalRiskGroups()) {
			itemRiskGroup.setInsuredName((this.parseNull(itemRiskGroup.getFirstName()) + " " + this.parseNull(itemRiskGroup.getLastName())).trim());
		}
	}
	
	private void dataFillMotorist(ItemAuto itemAuto) {
		// Validate the Motorist data
		for (Motorist motorist : itemAuto.getMotorists()) {
			motorist.setName((this.parseNull(motorist.getFirstName()) + " " + this.parseNull(motorist.getLastName())).trim());
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.tratomais.core.service.impl.CalculationServiceAbs#validateProposal(br.com.tratomais.core.model.policy.Endorsement)
	 */
	@Override
	protected void dataFillProposal(Endorsement endorsement, PaymentOption paymentOption) throws CalculusServiceException {
		if (endorsement != null) {
			// Data fill the insured
			Customer insured = endorsement.getCustomerByInsuredId();
			this.dataFillCustomer(insured);
			
			// Data fill the policy holder
			this.dataFillCustomer(endorsement.getCustomerByPolicyHolderId());
			
			//Data fill the payment option
			if (!this.parseNull(endorsement.getEndorsementType()).equals(Endorsement.ENDORSEMENT_TYPE_NOT)) {
				endorsement.setPaymentTermId(((paymentOption != null && paymentOption.getPaymentId() != 0) ? paymentOption.getPaymentId() : null));
			}
			else {
				paymentOption = null;
				endorsement.setPaymentTermId(null);
			}
			
			// Data fill the itens
			for (Item item : endorsement.getItems()) {
				
				// Data fill the beneficiaries
				this.dataFillBeneficiary(item);
				
				// Data fill the item
				switch (item.getObjectId()) {
					case Item.OBJECT_VIDA:
					case Item.OBJECT_CREDITO:
					case Item.OBJECT_CICLO_VITAL:
					case Item.OBJECT_ACCIDENTES_PERSONALES:
					case Item.OBJECT_PURCHASE_PROTECTED:
						ItemPersonalRisk  itemPersonalRisk  = (ItemPersonalRisk)item;
						
						// Data fill the main item 
						this.dataFillPersonalRisk(insured, itemPersonalRisk);
						
						// Data fill the item risk group
						this.dataFillPersonalRiskGroup(itemPersonalRisk);
						
						break;
						
					case Item.OBJECT_AUTO:
						ItemAuto itemAuto = (ItemAuto)item;
						
						// Validate the Motorist data
						this.dataFillMotorist(itemAuto);
						
						break;
				}
			}
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.tratomais.core.service.impl.CalculationServiceAbs#validateProposal(br.com.tratomais.core.model.policy.Endorsement)
	 */
	@Override
	protected boolean validateProposal(Endorsement endorsement, PaymentOption paymentOption) throws CalculusServiceException {
		boolean retorno = true;
		
		// Validate this contract data
		if (!validateProposalContract(endorsement)) {
			endorsement.getErrorList().add(ErrorTypes.VALIDATION_CONTRACT_INVALID, null, null, "Proposal Service");
			retorno = false;
		}
		
		// Validate this endorsement data
		if (!validateProposalEndorsement(endorsement)) {
			endorsement.getErrorList().add(ErrorTypes.VALIDATION_ENDORSEMENT_INVALID, null, null, "Proposal Service");
			retorno = false;
		}
		
		// Validate this paymentOption data
		if (!validateProposalPaymentOption(endorsement, paymentOption)) {
			endorsement.getErrorList().add(ErrorTypes.VALIDATION_PAYMENT_OPTION_INVALID, null, null, "Proposal Service");
			retorno = false;
		}
		
		// Order the Item
		TreeSet<Item> treeItem = orderItem(endorsement.getItems());
		
		// Data fill the items
		for (Item item : treeItem) {
			if (!validateProposalItem(item)) {
				endorsement.getErrorList().add(ErrorTypes.VALIDATION_ITEM_INVALID, null, null, "Proposal Service", item.getId().getItemId());
				retorno = false;
			}
		}
		return retorno;
	}

	/**
	 * Validate contract
	 * @param endorsement
	 * @return
	 */
	private boolean validateProposalContract(Endorsement endorsement) {
		boolean retorno = true;
		final Contract contract = endorsement.getContract();
		
		if (contract != null) {
			Customer customer = endorsement.getCustomerByInsuredId();
			List<Erro> erros = validateContract(contract.getProductId(), customer.getDocumentNumber(), customer.getDocumentType(), endorsement.getEffectiveDate(), endorsement.getReferenceDate(), contract.getContractId());
			if (erros.size() > 0) {
				retorno = false;
				for (Erro erro : erros) {
					endorsement.getErrorList().add(erro);
				}
			}
		}
		
		return ((contract != null) && retorno);
	}

	/**
	 * Validate endorsement
	 * @param endorsement
	 * @return
	 */
	private boolean validateProposalEndorsement(Endorsement endorsement) {
		boolean retorno = true;
		
		if (endorsement != null) {
			final Contract contract = endorsement.getContract();
			final Customer customer = endorsement.getCustomerByInsuredId();
			final Customer policyHolder = endorsement.getCustomerByPolicyHolderId();
			final ErrorList errorList = endorsement.getErrorList();
			
			// Validate the customer kinship
			if (this.parseNull(customer.getPersonType()).equals(Customer.PERSON_TYPE_NATURAL)) {
				PlanKinship planKinship = daoCalculo.getPlanKinshipByRefDate(contract.getProductId(), endorsement.getRiskPlanId(), ItemPersonalRisk.PERSONAL_RISK_TYPE_TITULAR, ItemPersonalRisk.PERSONAL_KINSHIP_TYPE_PROPRIO, (contract.getParentId() == null ? Item.ITEM_RENEW_TYPE_EMISSION : Item.ITEM_RENEW_TYPE_RENEWAL), endorsement.getReferenceDate());
				if (planKinship != null) {
					// Validate the age customer
					double age = DateUtilities.agePartByDate(customer.getBirthDate(), endorsement.getEffectiveDate());		
					if (((planKinship.getMinimumAge() != 0) && (age < planKinship.getMinimumAge())) 
					 || ((planKinship.getMaximumAge() != 0) && (age > planKinship.getMaximumAge()))) {
						errorList.add(
								ErrorTypes.VALIDATION_INSURED_AGE_INVALID,
								daoDataOption.getDataOption(planKinship.getId().getPersonalRiskType()).getFieldDescription(),
								customer.getName(),
								planKinship.getMinimumAge(),
								planKinship.getMaximumAge() == 0 ? 99 : planKinship.getMaximumAge(), null,
								"Proposal validation", 0, 0);
							retorno = false;
					}
				}				
			}
			
			// Validate the Address for insured
			if (customer.getAddresses() != null && customer.getAddresses().size() == 0) {
				errorList.add(ErrorTypes.VALIDATION_INSURED_ADDRESS_ABSENT, null, BasicCalculationService.class, "Proposal validation", 0);
				retorno = false;
			}
			
			// Validate the Address for policyHolder
			if (policyHolder != customer) {
				if (customer.getAddresses() != null && policyHolder.getAddresses().size() == 0) {
					errorList.add(ErrorTypes.VALIDATION_POLICY_HOLDER_ADDRESS_ABSENT, null, BasicCalculationService.class, "Proposal validation", 0);
					retorno = false;
				}
			}
		}
		
		return ((endorsement != null) && retorno);
	}
	
	/**
	 * Validate paymentOption
	 * @param endorsement
	 * @param paymentOption
	 * @return
	 */
	private boolean validateProposalPaymentOption(Endorsement endorsement, PaymentOption paymentOption) {
		boolean retorno = true;
		
		if (endorsement != null && (endorsement.getEndorsementType() != Endorsement.ENDORSEMENT_TYPE_NOT)) {
			final Contract contract = endorsement.getContract();
			final ErrorList errorList = endorsement.getErrorList();
			final Date refDate = endorsement.getReferenceDate();
			
			// Checks if payment option is informed
			if ((paymentOption == null) || (endorsement.getPaymentTermId() == null) ||(paymentOption != null && paymentOption.getPaymentId() == 0)) {
				errorList.add(ErrorTypes.VALIDATION_PAYMENT_OPTION_ABSENT, null, BasicCalculationService.class, "Proposal validation", 0);
				retorno = false;
			} 
			else {
				// Validate if the payment term exists
				PaymentTerm paymentTerm = daoCalculo.getPaymentTerm(endorsement.getPaymentTermId());
				if (paymentTerm == null) {
					errorList.add(ErrorTypes.VALIDATION_PAYMENT_TERM_ID_INVALID, null, BasicCalculationService.class, "Proposal validation", 0);
					retorno = false;
				}
				else {
					// Validate if the payment term exists for the product
					ProductPaymentTerm productPaymentTerm = daoCalculo.getProductPaymentTerm(contract.getProductId(), refDate, paymentOption.getBillingMethodId(), endorsement.getPaymentTermId());
					if (productPaymentTerm == null) {
						errorList.add(ErrorTypes.VALIDATION_PAYMENT_TERM_ID_DISPARITY, null, BasicCalculationService.class, "Proposal validation", 0);
						retorno = false;
					} 
					else {
						
						// Checks the rules for PaymentTerm
						if (!serviceRule.checkRulePaymentTerm(endorsement, paymentOption)) {
							errorList.add(
									ErrorTypes.VALIDATION_RULE_TYPE_PAYMENT_TERM_BLOCKED,
									new String[] { paymentTerm.getName() },
									BasicCalculationService.class, "Proposal validation", 0);
							retorno = false;
						}
						
						// Checks the rules for PaymentType
						if (!serviceRule.checkRulePaymentType(endorsement, paymentTerm.getFirstPaymentType())) {
							DataOption paymentType = daoDataOption.getDataOption(paymentTerm.getFirstPaymentType());
							errorList.add(
									ErrorTypes.VALIDATION_RULE_TYPE_PAYMENT_TYPE_BLOCKED,
									new String[] { (paymentType != null ? paymentType.getFieldDescription() : paymentTerm.getName()) },
									BasicCalculationService.class, "Proposal validation", 0);
							retorno = false;
						}
					}
				}
			}
		}
		
		return retorno;
	}
	
	/**
	 * Validate item
	 * @param item
	 * @return
	 */
	private boolean validateProposalItem(Item item) {
		boolean retorno = true;
		
		final Endorsement endorsement = item.getEndorsement();
		final Contract contract = endorsement.getContract();
		final ErrorList errorList = endorsement.getErrorList();
		final Date refDate = endorsement.getReferenceDate();
		final int itemId = item.getId().getItemId();

		ItemPersonalRisk itemPersonalRisk = null;
		ItemProperty itemProperty = null;
		
		// Validate the object items of life
		switch (item.getObjectId()) {
			case Item.OBJECT_VIDA:
			case Item.OBJECT_CREDITO:
			case Item.OBJECT_CICLO_VITAL:
			case Item.OBJECT_ACCIDENTES_PERSONALES:
			case Item.OBJECT_PURCHASE_PROTECTED:
				itemPersonalRisk = (ItemPersonalRisk)item;
				
				// Check the main item
				if (itemId != 1) {
					// Validate the birth date
					if (itemPersonalRisk.getBirthDate() == null) {
						errorList.add(ErrorTypes.VALIDATION_BIRTH_DATE_INVALID, null, BasicCalculationService.class, "Proposal validation", itemId);
						retorno = false;
					}
					else {
						// Validate the plan kinship
						PlanKinship planKinship = daoCalculo.getPlanKinshipByRefDate(contract.getProductId(), endorsement.getRiskPlanId(), itemPersonalRisk.getPersonalRiskType(), itemPersonalRisk.getKinshipType(), itemPersonalRisk.getRenewalType(), refDate);
						if (planKinship != null) {
							// Validate the age insured
							double age = DateUtilities.agePartByDate(itemPersonalRisk.getBirthDate(), item.getEffectiveDate());		
							if (((planKinship.getMinimumAge() != 0) && (age < planKinship.getMinimumAge())) 
							 || ((planKinship.getMaximumAge() != 0) && (age > planKinship.getMaximumAge()))) {
									errorList.add(
										ErrorTypes.VALIDATION_INSURED_AGE_INVALID,
										daoDataOption.getDataOption(planKinship.getId().getPersonalRiskType()).getFieldDescription(),
										itemPersonalRisk.getInsuredName(),
										planKinship.getMinimumAge(),
										planKinship.getMaximumAge() == 0 ? 99 : planKinship.getMaximumAge(), null,
										"Proposal validation", itemId, 0);
									retorno = false;
							}
						}
					}
				}
				
				// Validate that the card is already insured
				if (item.getObjectId() == Item.OBJECT_PURCHASE_PROTECTED) {
					ItemPurchaseProtected itemPurchaseProtected = (ItemPurchaseProtected)item;
					
					Integer qtdeRegistro = daoCalculo.getQuantityOfContracts(item.getObjectId(), itemPurchaseProtected.getCardNumber(), endorsement.getEffectiveDate(), contract.getContractId());
					if ((qtdeRegistro == null ? 0 : qtdeRegistro) >= 1) {
						errorList.add(
							ErrorTypes.VALIDATION_POLICY_NUMBER_OBJECT_RISK_FOUND, null,
							BasicCalculationService.class, "Proposal validation", itemId);
						retorno = false;
					}
				}
				
				// Order the item Risk Group
				TreeSet<ItemPersonalRiskGroup> treeItemRiskGroup = orderItemRiskGroup(itemPersonalRisk.getItemPersonalRiskGroups());
				
				// Validate the item risk group kinship
				for (ItemPersonalRiskGroup itemRiskGroup : treeItemRiskGroup) {
					if (!validateProposalRiskGroup(itemRiskGroup, false)) {
						retorno = false;
					}
				}
				break;
				
			case Item.OBJECT_HABITAT:
			case Item.OBJECT_CAPITAL:
				itemProperty = (ItemProperty)item;
				
				// Validate the Address of Risk
//				if ((this.parseNull(itemProperty.getStateId()).equals(0)) &&
//				    (this.parseNull(itemProperty.getStateName()).isEmpty())) {
//					errorList.add(ErrorTypes.VALIDATION_ITEM_PROPERTY_STATE_ABSENT, null, BasicCalculationService.class, "Proposal validation", itemId);
//					retorno = false;
//				}
//				if ((this.parseNull(itemProperty.getCityId()).equals(0)) &&
//					(this.parseNull(itemProperty.getCityName()).isEmpty())) {
//					errorList.add(ErrorTypes.VALIDATION_ITEM_PROPERTY_CITY_ABSENT, null, BasicCalculationService.class, "Proposal validation", itemId);
//					retorno = false;
//				}
//				if (this.parseNull(itemProperty.getDistrictName()).isEmpty()) {
//					errorList.add(ErrorTypes.VALIDATION_ITEM_PROPERTY_DISTRICT_ABSENT, null, BasicCalculationService.class, "Proposal validation", itemId);
//					retorno = false;
//				}
				if (this.parseNull(itemProperty.getAddressStreet()).isEmpty()) {
					errorList.add(ErrorTypes.VALIDATION_ITEM_PROPERTY_STREET_ABSENT, null, BasicCalculationService.class, "Proposal validation", itemId);
					retorno = false;
				}
				break;
				
			case Item.OBJECT_AUTO:
				ItemAuto itemAuto = (ItemAuto)item;
				
				// Validate that the license/serial is already insured
				Integer qtdeRegistro = daoCalculo.getQuantityOfContracts(item.getObjectId(), itemAuto.getLicensePlate(), itemAuto.getChassisSerial(), itemAuto.getEngineSerial(), endorsement.getEffectiveDate(), contract.getContractId());
				if ((qtdeRegistro == null ? 0 : qtdeRegistro) >= 1) {
					errorList.add(
						ErrorTypes.VALIDATION_POLICY_NUMBER_OBJECT_RISK_FOUND, null,
						BasicCalculationService.class, "Proposal validation", itemId);
					retorno = false;
				}
				
				// Validate the personal risk type
				DataOption motoristRiskType = daoDataOption.getDataOption(ItemAuto.PERSONAL_RISK_TYPE_MOTORIST);
				
				// Validate the Motorist data
				for (Motorist motorist : itemAuto.getMotorists()) {
					// Validate the birth date
					if (motorist.getBirthDate() == null) {
						errorList.add(ErrorTypes.VALIDATION_BIRTH_DATE_INVALID, null, BasicCalculationService.class, "Proposal validation", itemId);
						retorno = false;
					}
					else {
						// Validate the plan kinship: Motorist
						PlanKinship planKinship = daoCalculo.getPlanKinshipByRefDate(contract.getProductId(), endorsement.getRiskPlanId(), ItemAuto.PERSONAL_RISK_TYPE_MOTORIST, this.parseNull(motorist.getKinshipType()), itemAuto.getRenewalType(), refDate);
						if (planKinship != null) {
							// Validate the age insured
							double age = DateUtilities.agePartByDate(motorist.getBirthDate(), item.getEffectiveDate());
							if (((planKinship.getMinimumAge() != 0) && (age < planKinship.getMinimumAge())) 
							 || ((planKinship.getMaximumAge() != 0) && (age > planKinship.getMaximumAge()))) {
									errorList.add(
										ErrorTypes.VALIDATION_INSURED_AGE_INVALID,
										motoristRiskType.getFieldDescription(),
										motorist.getName(),
										planKinship.getMinimumAge(),
										planKinship.getMaximumAge() == 0 ? 99 : planKinship.getMaximumAge(), null,
										"Proposal validation", itemId, 0);
									retorno = false;
							}
						}
					}
				}
				break;
		}
		
		return retorno;
	}
	
	private boolean validateProposalRiskGroup(ItemPersonalRiskGroup itemRiskGroup, boolean isCalculate) {
		boolean retorno = true;
		
		final Endorsement endorsement = itemRiskGroup.getItemPersonalRisk().getEndorsement();
		final Contract contract = endorsement.getContract();
		final ErrorList errorList = endorsement.getErrorList();
		final Date refDate = endorsement.getReferenceDate();
		final int itemId = itemRiskGroup.getItemPersonalRisk().getId().getItemId();
		final int riskGroupId = itemRiskGroup.getId().getRiskGroupId();
		
		if (itemRiskGroup != null) {
			// Validate the birth date
			if (itemRiskGroup.getBirthDate() == null) {
				if (!isCalculate) {
					errorList.add(ErrorTypes.VALIDATION_BIRTH_DATE_INVALID, null, BasicCalculationService.class, "Proposal validation", itemId);
					retorno = false;
				}
			}
			else {
				// Validate the plan kinship
				PlanKinship planKinship = daoCalculo.getPlanKinshipByRefDate(contract.getProductId(), endorsement.getRiskPlanId(), itemRiskGroup.getPersonalRiskType(), itemRiskGroup.getKinshipType(), itemRiskGroup.getItemPersonalRisk().getRenewalType(), refDate);
				if (planKinship != null) {
					// Validate the age insured
					double age = DateUtilities.agePartByDate(itemRiskGroup.getBirthDate(), itemRiskGroup.getEffectiveDate());		
					if (((planKinship.getMinimumAge() != 0) && (age < planKinship.getMinimumAge())) 
					 || ((planKinship.getMaximumAge() != 0) && (age > planKinship.getMaximumAge()))) {
							errorList.add(
								ErrorTypes.VALIDATION_INSURED_AGE_INVALID,
								daoDataOption.getDataOption(planKinship.getId().getPersonalRiskType()).getFieldDescription(),
								itemRiskGroup.getInsuredName(),
								planKinship.getMinimumAge(),
								planKinship.getMaximumAge() == 0 ? 99 : planKinship.getMaximumAge(), null,
								(isCalculate?"Calculation Service":"Proposal validation"), itemId, riskGroupId);
							retorno = false;
					}
				}
			}
		}
		
		return retorno;
	}

	/**
	 * Generates Installments
	 * 
	 * @param endorsement
	 * @param paymentOption
	 * @return
	 * @throws ServiceException
	 */
	public List<Installment> generateInstallment(Endorsement endorsement, PaymentOption paymentOption) throws ServiceException {
		List<Installment> lista = new LinkedList<Installment>();
		
		PaymentTerm paymentTerm = daoCalculo.getPaymentTerm(paymentOption.getPaymentId());
		if (paymentTerm == null || paymentOption == null) {
			endorsement.getErrorList().add(ErrorTypes.VALIDATION_PAYMENT_TERM_ID_INVALID, null, BasicCalculationService.class, "Proposal validation", 0);
			return null;
		}
		
		double totalPremium = 0d;
		double taxValue = 0d;
		double netPremium = 0d;
		Date issueDate = null, dueDate = null, effectiveDate = null, expiryDate = null;
		short qtdeParcelas = (short)paymentOption.getQuantityInstallment();

		for (int pagto = 0; pagto < qtdeParcelas; pagto++) {
			if (pagto == 0) {
				issueDate = DateUtilities.dateInFuture(endorsement.getEffectiveDate(), paymentTerm.getFirstMultipleType(), paymentTerm.getFirstInstallment());
				dueDate = DateUtilities.dateInFuture(issueDate, DateUtilities.DIARIO, paymentTerm.getCancelFirstInstallment());
				effectiveDate = endorsement.getEffectiveDate();
				expiryDate = (qtdeParcelas == 1 ? endorsement.getExpiryDate() : paymentOption.getNextPaymentDate());
				totalPremium = paymentOption.getFirstInstallmentValue();
			} else if (pagto == 1) {
				issueDate = paymentOption.getNextPaymentDate();
				dueDate = DateUtilities.dateInFuture(issueDate, DateUtilities.DIARIO, paymentTerm.getCancelNextInstallment());
				effectiveDate = expiryDate;
				expiryDate = DateUtilities.dateInFuture(effectiveDate, paymentTerm.getNextMultipleType(), paymentTerm.getNextInstallment());
				totalPremium = paymentOption.getNextInstallmentValue();
			} else {
				issueDate = DateUtilities.dateInFuture(issueDate, paymentTerm.getNextMultipleType(), paymentTerm.getNextInstallment());
				dueDate = DateUtilities.dateInFuture(issueDate, DateUtilities.DIARIO, paymentTerm.getCancelNextInstallment());
				effectiveDate = expiryDate;
				expiryDate = DateUtilities.dateInFuture(effectiveDate, paymentTerm.getNextMultipleType(), paymentTerm.getNextInstallment());
				totalPremium = paymentOption.getNextInstallmentValue();
			}
			
			taxValue = NumberUtilities.roundDecimalPlaces(paymentOption.getTaxValue() / qtdeParcelas, Calculus.RESULT_DECIMAL_PLACES);
			netPremium = NumberUtilities.roundDecimalPlaces(totalPremium - taxValue, Calculus.RESULT_DECIMAL_PLACES);

			//Check if date less than today6
			if (dueDate.before(DateUtilities.truncDate(new Date())))
				dueDate = DateUtilities.dateInFuture(DateUtilities.truncDate(new Date()), DateUtilities.DIARIO, paymentTerm.getCancelFirstInstallment());
			
			//Set date the in expiry date
			if (dueDate.after(endorsement.getExpiryDate())) 
				dueDate = endorsement.getExpiryDate();
				
			Installment installment = endorsement.addInstallment(pagto + 1);
			installment.setBillingMethodId(paymentOption.getBillingMethodId());
			installment.setInstallmentType(totalPremium < 0 ? Installment.INSTALLMENT_TYPE_DEBIT : Installment.INSTALLMENT_TYPE_CREDIT);
			installment.setPaymentType(pagto == 0 ? paymentTerm.getFirstPaymentType() : paymentTerm.getNextPaymentType());
			installment.setEffectiveDate(effectiveDate);
			installment.setExpiryDate(expiryDate);
			installment.setIssueDate(issueDate);
			installment.setDueDate(dueDate);
			installment.setNetPremium(netPremium);
			installment.setPolicyCost(0.0);
			installment.setInspectionCost(0.0);
			installment.setTaxValue(taxValue);
			installment.setFractioningAdditional(0.0);
			installment.setInstallmentValue(totalPremium);
			installment.setCurrencyId(endorsement.getContract().getCurrencyId());
			if ((paymentTerm.getFirstPaymentType() == BillingMethod.PAYMENT_TYPE_CREDIT_CARD) || 
				(paymentTerm.getFirstPaymentType() == BillingMethod.PAYMENT_TYPE_DEBIT_CARD)) {				
				installment.setCardType(paymentOption.getCardType());
				installment.setCardBrandType(paymentOption.getCardBrandType());
				installment.setCardNumber(paymentOption.getCardNumber());
				installment.setCardExpirationDate(paymentOption.getCardExpirationDate());
				installment.setCardHolderName(paymentOption.getCardHolderName());
			} else if ((paymentTerm.getFirstPaymentType() == BillingMethod.PAYMENT_TYPE_DEBIT_ACCOUNT) || 
					   (paymentTerm.getFirstPaymentType() == BillingMethod.PAYMENT_TYPE_ACCOUNT_SAVINGS)) {
				installment.setBankNumber(paymentOption.getBankNumber());
				installment.setBankAgencyNumber(paymentOption.getBankAgencyNumber());
				installment.setBankAccountNumber(paymentOption.getBankAccountNumber());
			} else if (paymentTerm.getFirstPaymentType() == BillingMethod.PAYMENT_TYPE_CHECK) {
				installment.setBankNumber(paymentOption.getBankNumber());
				installment.setBankAgencyNumber(paymentOption.getBankAgencyNumber());
				installment.setBankAccountNumber(paymentOption.getBankAccountNumber());
				installment.setBankCheckNumber(paymentOption.getBankCheckNumber());
			} else { // BillingMethod.PAYMENT_TYPE_OTHERS
				installment.setOtherAccountNumber(paymentOption.getOtherAccountNumber());
			}
			installment.setTransmitted(false);
			installment.setInstallmentStatus(Installment.INSTALLMENT_STATUS_PENDING);
			lista.add(installment);
		}
		return lista;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.tratomais.core.service.impl.CalculationServiceAbs#saveEffective
	 * (br.com.tratomais.core.model.policy.Endorsement,
	 */
	@Override
	protected void saveEffective(Endorsement endorsement) {
		//WBB daoPolicy.saveEndorsement(endorsement);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.tratomais.core.service.impl.CalculationServiceAbs#processing(br
	 * .com.tratomais.core.model.policy.Endorsement,
	 * br.com.tratomais.core.model.product.PaymentOption)
	 */
	@Override
	protected boolean effectivate(Endorsement endorsement, PaymentOption paymentOption) {
		boolean retorno = true;
		try {
			endorsement.getInstallments().clear();
			// Check if the endorsement is without movement prize  
			if (!this.parseNull(endorsement.getEndorsementType()).equals(Endorsement.ENDORSEMENT_TYPE_NOT)) {
				List<Installment> lista = generateInstallment(endorsement, paymentOption);
				for (Installment ins : lista) {
					endorsement.attachInstallment(ins);
				}
			}
			endorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_PROPOSTA);
			endorsement.setProposalDate(new Date());
			if (this.parseNull(endorsement.getContract().getCertificateNumber()).equals(0L)) {
				endorsement.getContract().setCertificateNumber(getCeritifateNumber(endorsement));
			}
			endorsement.setProposalNumber(daoCalculo.getNextProposalNumber());
			endorsement.flush();
			endorsement.splitTotalValues();
		} catch (Exception e) {
			endorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR.getErrorCode(), e.getMessage(), null, 0, "Proposal Service");
			logger.fatal(e);
			e.printStackTrace();
			retorno = false;
		}
		return retorno;
	}

	private Long getCeritifateNumber(Endorsement endorsement) {
		return daoCalculo.getCertificateNumber(endorsement.getContract().getMasterPolicyId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.tratomais.core.service.impl.CalculationServiceAbs#validaSaidaCalculo
	 * (br.com.tratomais.core.model.policy.Endorsement)
	 */
	@Override
	protected void validaSaidaCalculo(Endorsement endorsement) {
		Contract contract = endorsement.getContract();
		if (contract.getCurrencyId() == 0) {
			endorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR1, null, BasicCalculationService.class, "Calculation Service", 0);
		}
		if (contract.getExpiryDate() == null) {
			endorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR1, null, BasicCalculationService.class, "Calculation Service", 0);
		}
		if (contract.getContractStatus() == 0) {
			endorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR1, null, BasicCalculationService.class, "Calculation Service", 0);
		}
	}

	public void validaCamposEfetivacao(Endorsement endorsement) {
		Contract contract = endorsement.getContract();
		if (contract.getCertificateNumber() == null) {
			endorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR1, null, null, "Calculation Service", 0);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.tratomais.core.service.ICalc#validateContract(int,
	 * java.lang.String, int, java.util.Date, java.util.Date, int)
	 */
	@Override
	public List<Erro> validateContract(int productId, String docNumber, int docType, Date dateIni, Date referenceDate, int contractId) {
		ArrayList<Erro> retorno = new ArrayList<Erro>();
		
		// Valida quantidade de registros
		ProductOption productOption = daoProduct.getProduct(productId, referenceDate);
		if (productOption == null)
			retorno.add(new Erro(ErrorTypes.VALIDATION_PRODUCT_VERSION_INVALID.getErrorCode(),
								 ErrorTypes.VALIDATION_PRODUCT_VERSION_INVALID.getMessage(null), 
								 BasicCalculationService.class,
								 ErrorTypes.VALIDATION_PRODUCT_VERSION_INVALID.getErrorType(), "Validation Type", (retorno.size()+1)));
		else {
			Integer qtdeRegistro = daoCalculo.getQuantityOfContracts(productId, docNumber, docType, dateIni, dateIni, contractId);
			if ((productOption.getMaxOccurrences() != null) && 
				(productOption.getMaxOccurrences() <= (qtdeRegistro == null ? 0 : qtdeRegistro))) {
				retorno.add(new Erro(ErrorTypes.VALIDATION_POLICY_NUMBER_GREATER_THAN_MAXIMUM.getErrorCode(),
								ErrorTypes.VALIDATION_POLICY_NUMBER_GREATER_THAN_MAXIMUM.getMessage(new Object[] { productOption.getMaxOccurrences(), productOption.getNickName() }),
								BasicCalculationService.class,
								ErrorTypes.VALIDATION_POLICY_NUMBER_GREATER_THAN_MAXIMUM.getErrorType(), "Validation Type", (retorno.size()+1)));
			}
			Double valueSumOfContracts = daoCalculo.getValueSumOfContracts(productId, docNumber, docType, dateIni, contractId);
			ProductVersion productVersion = daoProductVersion.getProductVersionByRefDate(productId, referenceDate);
			if (productVersion == null)
				retorno.add(new Erro(ErrorTypes.VALIDATION_PRODUCT_VERSION_INVALID.getErrorCode(),
									 ErrorTypes.VALIDATION_PRODUCT_VERSION_INVALID.getMessage(null),
									 BasicCalculationService.class,
									 ErrorTypes.VALIDATION_PRODUCT_VERSION_INVALID.getErrorType(), "Validation Type", (retorno.size()+1)));
			else if ((productVersion.getMaximumInsuredValue() != 0) && 
					 (productVersion.getMaximumInsuredValue() < ((valueSumOfContracts == null) ? 0 : valueSumOfContracts.doubleValue()))) {
				retorno.add(new Erro(ErrorTypes.VALIDATION_CONTRACT_POLICY_SUM_GREATER_THAN_MAXIMUM.getErrorCode(),
								ErrorTypes.VALIDATION_CONTRACT_POLICY_SUM_GREATER_THAN_MAXIMUM.getMessage(new Object[] { productVersion.getMaximumInsuredValue(), productOption.getNickName() }),
								BasicCalculationService.class,
								ErrorTypes.VALIDATION_CONTRACT_POLICY_SUM_GREATER_THAN_MAXIMUM.getErrorType(), "Validation Type", (retorno.size()+1)));
			}
		}
		return retorno;
	}

	/**
	 * Print data to log (finger-print)
	 * 
	 * @param endorsement
	 */
	private void printKeys(Endorsement endorsement) {
		String tabKey = "\t";
		int level = 0;
		logger.info("###PrintKeys-Start---------------###");
		logger.info("Endorsement: " + endorsement.getId().toString());
		level++;
		for (Item item : endorsement.getItems()) {
			logger.info(tabKey + "Item: " + item.getId().toString());
			level++;
			for (ItemCoverage coverage : item.getItemCoverages()) {
				logger.info(tabKey + tabKey + "Coverage: "
						+ coverage.getId().toString());
			}
			level--;
		}
		level--;
		logger.info("###PrintKeys-End-----------------###");
	}

	/**
	 * {@inheritDoc}
	 */
	protected void verifyPendencies(Endorsement endorsement) {
		Item mainItem = endorsement.getMainItem();
		
		// Get pendency list
		List<Pendency> listPendency = daoLockPendency.listPendencyList(mainItem.getObjectId());
		List<LockPendency> lockPendency2Save = new LinkedList<LockPendency>();
		
		// Check if apply pendency for the endorsement and/or item
		for (Pendency pendency : listPendency) {
			//Check if pendency is the endorsement
			if (pendency.getPendencyLevel() == Pendency.PENDENCY_LEVEL_ENDORSEMENT) {
				//Check if apply pendency for the endorsement
				if (serviceRule.checkRulesPendency(mainItem, pendency.getId())) {
					// Set id pendency
					LockPendencyId lockPendencyId = new LockPendencyId();
					lockPendencyId.setContractID(endorsement.getId().getContractId());
					lockPendencyId.setEndorsementID(endorsement.getId().getEndorsementId());
					
					// Set detalle pendency
					LockPendency lockPendency = new LockPendency();
					lockPendency.setId(lockPendencyId);
					lockPendency.setItemID(null);
					lockPendency.setPendencyID(pendency.getId());
					lockPendency.setIssueDate(new Date());
					lockPendency2Save.add(lockPendency);
				}
			}
			else if (pendency.getPendencyLevel() == Pendency.PENDENCY_LEVEL_ITEM) {
				for (Item item : endorsement.getItems()) {
					//Check if apply pendency for the item
					if (serviceRule.checkRulesPendency(item, pendency.getId())) {
						// Set id pendency
						LockPendencyId lockPendencyId = new LockPendencyId();
						lockPendencyId.setContractID(endorsement.getId().getContractId());
						lockPendencyId.setEndorsementID(endorsement.getId().getEndorsementId());
						
						// Set detalle pendency
						LockPendency lockPendency = new LockPendency();
						lockPendency.setId(lockPendencyId);
						lockPendency.setItemID(item.getId().getItemId());
						lockPendency.setPendencyID(pendency.getId());
						lockPendency.setIssueDate(new Date());
						lockPendency2Save.add(lockPendency);
					}
				}
			}
			else if (pendency.getPendencyLevel() == Pendency.PENDENCY_LEVEL_AUTOMATIC) {
				// Set id pendency
				LockPendencyId lockPendencyId = new LockPendencyId();
				lockPendencyId.setContractID(endorsement.getId().getContractId());
				lockPendencyId.setEndorsementID(endorsement.getId().getEndorsementId());
				
				// Set detalle pendency
				LockPendency lockPendency = new LockPendency();
				lockPendency.setId(lockPendencyId);
				lockPendency.setItemID(null);
				lockPendency.setPendencyID(pendency.getId());
				lockPendency.setIssueDate(new Date());
				lockPendency2Save.add(lockPendency);
			}
		}
		
		// Register dependency on endorsement
		if (lockPendency2Save.size() > 0){
			registerPendencies(lockPendency2Save);	
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void registerPendencies(List<LockPendency> lockPendencyList) {
		// Checks if exists pendency for register
		if (lockPendencyList != null && lockPendencyList.size() > 0) {
			LockPendency lockPendency = lockPendencyList.get(0);
			int contractId = lockPendency.getId().getContractID();
			int endorsementId = lockPendency.getId().getEndorsementID();
			
			// Verify if exists y register pendencies
			for (LockPendency lckPendency : lockPendencyList) {
				List<LockPendency> lockPendenciesPersisted = daoLockPendency.findLockPendencies(endorsementId, contractId, lockPendency.getItemID(), lckPendency.getPendencyID());
				if ((lockPendenciesPersisted == null) || (lockPendenciesPersisted.isEmpty())) {
					lckPendency.setLockPendencyStatus(LockPendency.LOCK_PENDENCY_STATUS_PENDENT);
					lckPendency = daoLockPendency.save(lckPendency);
					historico(lckPendency, "Creaci�n", LockPendencyHistory.HISTORY_TYPE_STATUS_CHANGE, null, null, LockPendency.LOCK_PENDENCY_STATUS_PENDENT);
				}
			}
		}
	}
	
	private void historico(LockPendency lockPendency, String descricao, int historyStatus, Date issueDate, Integer userID, Integer lockPendencyStatus){
		LockPendencyHistory historyPendency = new LockPendencyHistory();
		historyPendency.setContractID(lockPendency.getId().getContractID());
		historyPendency.setEndorsementID(lockPendency.getId().getEndorsementID());
		historyPendency.setLockID(lockPendency.getId().getLockID());
		historyPendency.setDescription(descricao);
		historyPendency.setHistoryType(historyStatus);
		historyPendency.setIssueDate(issueDate==null?new Date():issueDate);
		historyPendency.setUserID(userID==null?1:userID);
		historyPendency.setLockPendencyStatus(lockPendencyStatus);
		daoHistoryPendency.save(historyPendency);
	}

	public void freePendency(LockPendency pendencyInput) {
		LockPendency pendency = daoLockPendency.findPendency(pendencyInput.getId().getContractID(), pendencyInput.getId().getEndorsementID(), pendencyInput.getItemID(), pendencyInput.getPendencyID());
		if (pendency.getLockPendencyStatus() == LockPendency.LOCK_PENDENCY_STATUS_PENDENT) {
			pendency.setUserID(pendencyInput.getUserID());
			pendency.setIssueDate(pendencyInput.getIssueDate());
			pendency.setLockPendencyStatus(pendencyInput.getLockPendencyStatus());
			historico(pendency, "Cambio de estatus: " + daoDataOption.getDataOption(pendency.getLockPendencyStatus()).getFieldDescription(), LockPendencyHistory.HISTORY_TYPE_STATUS_CHANGE, pendencyInput.getIssueDate(), pendencyInput.getUserID(), pendencyInput.getLockPendencyStatus());
			daoLockPendency.save(pendency);
			emission(pendency.getId().getContractID(), pendency.getId().getEndorsementID());
		}
	}
	
	public void emission(int contractId, int endorsementId) {
		Endorsement endorsement = daoPolicy.loadEndorsement(endorsementId, contractId);
		if (endorsement.getIssuingStatus() != Endorsement.ISSUING_STATUS_PROPOSTA)
			return;
		List<LockPendency> lockPendencies = daoLockPendency.findLockPendencies(endorsementId, contractId);
		int status = LockPendency.LOCK_PENDENCY_STATUS_APROVED; 
		loop:for(LockPendency lockPendency:lockPendencies){
			switch (lockPendency.getLockPendencyStatus()){
			case LockPendency.LOCK_PENDENCY_STATUS_REJECTED:
				status = LockPendency.LOCK_PENDENCY_STATUS_REJECTED;
				break loop;
			case LockPendency.LOCK_PENDENCY_STATUS_APROVED:
			case LockPendency.LOCK_PENDENCY_STATUS_BLOCKED:
				break;
			default:
				status = LockPendency.LOCK_PENDENCY_STATUS_PENDENT;
			}
		}
		if ((status == LockPendency.LOCK_PENDENCY_STATUS_APROVED)||(status == LockPendency.LOCK_PENDENCY_STATUS_REJECTED)){
			if (status == LockPendency.LOCK_PENDENCY_STATUS_APROVED)
				endorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_EMITIDA);
			else if (status == LockPendency.LOCK_PENDENCY_STATUS_REJECTED)
				endorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_ANULADA);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void baixaParcela(CollectionResponseItem collectionResponse ){
		Installment installment = daoPayment.findInstallment(collectionResponse.getProposalNumber(), collectionResponse.getInstallmentId());
		if (installment != null ){
			if (((installment.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_PENDING)||(installment.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_ANNULMENT))&& ( collectionResponse.getType() == CollectionResponseItem.TYPE_PAYMENT) ) {
				installment.setPaymentDate(collectionResponse.getPaymentDate());
				installment.setPaidValue(collectionResponse.getPaidValue());
				installment.setCurrencyDate(collectionResponse.getCurrencyDate());
				installment.setConversionRate(collectionResponse.getConversionRate());
				installment.setBankCheckNumber((collectionResponse.getBankCheckNumber() != null ? collectionResponse.getBankCheckNumber().toString() : null));
				installment.setInvoiceNumber(collectionResponse.getInvoiceNumber());
				installment.setDebitAuthorizationCode(collectionResponse.getDebitAuthorizationCode());
				installment.setCreditAuthorizationCode(collectionResponse.getCreditAuthorizationCode());
				installment.setInstallmentStatus(Installment.INSTALLMENT_STATUS_PAID);
				LockPendencyId lckId = new LockPendencyId();
				lckId.setContractID(installment.getId().getContractId());
				lckId.setEndorsementID(installment.getId().getEndorsementId());
				LockPendency lck = new LockPendency();
				lck.setId(lckId);
				lck.setPendencyID(2);
				lck.setIssueDate(new Date());
				lck.setLockPendencyStatus(LockPendency.LOCK_PENDENCY_STATUS_APROVED);
				freePendency(lck);
			}
			else if ( (installment.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_PENDING) && ( collectionResponse.getType() == CollectionResponseItem.TYPE_CANCEL) ) {
				installment.setInstallmentStatus(Installment.INSTALLMENT_STATUS_ANNULMENT);
				installment.setCancelDate(new Date());
				LockPendencyId lckId = new LockPendencyId();
				lckId.setContractID(installment.getId().getContractId());
				lckId.setEndorsementID(installment.getId().getEndorsementId());
				LockPendency lck = new LockPendency();
				lck.setId(lckId);
				lck.setPendencyID(2);
				lck.setIssueDate(new Date());
				lck.setLockPendencyStatus(LockPendency.LOCK_PENDENCY_STATUS_BLOCKED);
				freePendency(lck);
			}
			else {
				logger.error("Alteraci�n de estatus inv�lida");
			}
		}
		else logger.error("installment not found");
				
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void baixaParcela(CollectionResponse collectionResponse){
		for (CollectionResponseItem collectionResponseItem : collectionResponse.getLista()) {
			baixaParcela(collectionResponseItem);
		}
	}
	
	public void deletePendency(int contractId, int endorsementId) {
		daoLockPendency.deleteUntouchedPendencies(contractId, endorsementId);
	}

	@Override
	public Endorsement effectiveEndorsement(Endorsement endorsement, PaymentOption paymentOption, int issuanceType, Date newEffectiveDate) throws ServiceException {
		dataFillProposal(endorsement, paymentOption);
		if (validateProposal(endorsement, paymentOption)) {
			if (effectivate(endorsement, paymentOption)) {
				verifyPendencies(endorsement);
			}
		}
		return endorsement;
	}
	
	private String parseNull(String inputValue) {
		if (inputValue != null)
			return inputValue.trim();
		return "";
	}
	
	private Double parseNull(Double inputValue) {
		if (inputValue != null)
			return inputValue;
		return 0.0;
	}
	
	private Integer parseNull(Integer inputValue) {
		if (inputValue != null)
			return inputValue;
		return 0;
	}
	
	private BigInteger parseNull(BigInteger inputValue) {
		if (inputValue != null)
			return inputValue;
		return BigInteger.valueOf(0);
	}
	
	private Long parseNull(Long inputValue) {
		if (inputValue != null)
			return inputValue;
		return 0L;
	}

	@Override
	public List<PaymentOption> getPaymentOptionsByBillingMethodId(double totalPremium, double netPremium, double policyCost, double fractioningAdditional, double taxValue, double taxRate,	int productId, int billingMethodId, Date refDate, Date effectiveDate, Date expireDate, Short dueDay, Integer renewalType, Integer renewalInsurerId, Integer issuanceType, Integer paymentTermType, Integer invoiceType, Integer firstPaymentType, Integer qtInstallment, Date proposalDate) { 
		List<PaymentOption> paymentOptions = this.getPaymentOptionsByBillingMethod( totalPremium, netPremium, policyCost, fractioningAdditional,
				taxValue, taxRate, productId, billingMethodId, refDate,
				effectiveDate, expireDate, dueDay, renewalType,
				renewalInsurerId, issuanceType, paymentTermType, invoiceType,
				firstPaymentType, qtInstallment, proposalDate);
		return paymentOptions;
	}

	private List<PaymentOption> getPaymentOptionsByBillingMethod(double totalPremium, double netPremium, double policyCost, double fractioningAdditional, double taxValue, double taxRate, int productId, int billingMethodId, Date refDate, Date effectiveDate, Date expireDate, Short dueDay, Integer renewalType, Integer renewalInsurerId, Integer issuanceType, Integer paymentTermType, Integer invoiceType, Integer firstPaymentType, Integer qtInstallment, Date proposalDate) {
		List<PaymentOption> paymentOptions = this.getPaymentOptions(totalPremium, netPremium, policyCost, fractioningAdditional, taxValue, taxRate, daoCalculo.getPaymentsTermsByBillingMethodId(productId, refDate, billingMethodId, paymentTermType), effectiveDate, expireDate, refDate, dueDay, renewalType, renewalInsurerId, issuanceType, invoiceType, firstPaymentType, qtInstallment, proposalDate);
		BillingMethod billingMethod = daoCalculo.getBillingMethod(billingMethodId);
		if (paymentOptions != null) {
			for (PaymentOption po : paymentOptions) {
				po.setBillingMethod(billingMethod);
			}
		}
		return paymentOptions;
	}
	/**
	 * Return payment options for list of payments
	 * 
	 * @param totalPremium
	 * @param netPremium
	 * @param policyCost
	 * @param fractioningAdditional
	 * @param taxValue
	 * @param taxRate
	 * @param payments
	 * @param dtEffective 
	 * @param dtExpiry 
	 * @param refDate
	 * @param firstPaymentType 
	 * @param qtInstallment 
	 * @param proposalDate 
	 * @return List<PaymentOption>
	 */
	List<PaymentOption> getPaymentOptions(double totalPremium, double netPremium, double policyCost, double fractioningAdditional, double taxValue, double taxRate, List<ProductPaymentTerm> payments, Date dtEffective, Date dtExpiry, Date refDate, Short dueDay, Integer renewalType, Integer renewalInsurerId, Integer issuanceType, Integer invoiceType, Integer firstPaymentType, Integer qtInstallment, Date proposalDate) {
		List<PaymentOption> retorno = new ArrayList<PaymentOption>();
		ApplicationProperty applicationProperty = null;
		
		Date issueDate = (proposalDate != null ? proposalDate : new Date());
		dtExpiry = DateUtilities.addDays(dtExpiry, 1);
		
		totalPremium = NumberUtilities.roundDecimalPlaces(totalPremium, Calculus.STEP_DECIMAL_PLACES);
		
		// Set unique Installment when Refund
		if (totalPremium == 0) {
			return retorno;
		}
		else if (totalPremium < 0) {
			applicationProperty = daoPolicy.loadApplicationProperty(Application.DEFAULT, ApplicationProperty.PAYMENT_TERM_ID_FOR_TREASURY);
			Integer paymentTermIdForTreasury = (applicationProperty!=null && applicationProperty.getValue()!=null?Integer.parseInt(applicationProperty.getValue()):0);
			
			applicationProperty = daoPolicy.loadApplicationProperty(Application.DEFAULT, ApplicationProperty.BILLING_METHOD_ID_FOR_TREASURY);
			Integer billingMethodIdTreasury = (applicationProperty!=null && applicationProperty.getValue()!=null?Integer.parseInt(applicationProperty.getValue()):0);
			
			PaymentOption paymentOption = new PaymentOption();
			paymentOption.setQuantityInstallment(1);
			paymentOption.setFirstInstallmentValue(totalPremium);
			paymentOption.setNextInstallmentValue(0d);
			paymentOption.setBillingMethodId(billingMethodIdTreasury.intValue());
			paymentOption.setPaymentId(paymentTermIdForTreasury.intValue());
			paymentOption.setPaymentType(BillingMethod.PAYMENT_TYPE_OTHERS);
			paymentOption.setPaymentTermName("�nica");
			paymentOption.setNetPremium(netPremium);
			paymentOption.setPolicyCost(policyCost);
			paymentOption.setFractioningAdditional(fractioningAdditional);
			paymentOption.setTaxRate(taxRate);
			paymentOption.setTaxValue(taxValue);
			paymentOption.setTotalPremium(totalPremium);
			paymentOption.setDefault(true);
			retorno.add(paymentOption);
			return retorno;
		}

		applicationProperty = daoPolicy.loadApplicationProperty(Application.DEFAULT, ApplicationProperty.DAYS_TOLERANCE_INSTALLMENT);
		int daysToleranceInstallment = (applicationProperty!=null?Integer.parseInt(applicationProperty.getValue()):15);

		applicationProperty = daoPolicy.loadApplicationProperty(Application.DEFAULT, ApplicationProperty.DAYS_TO_BILLET_LOOSE);
		int daysToBilletLoose = (applicationProperty!=null?Integer.parseInt(applicationProperty.getValue()):15);
		
		for (ProductPaymentTerm prodPay : payments) {
			// Consulting Product version
			PaymentTerm pay = prodPay.getPaymentTerm();

			// Sera usado para calcular o tempo de diferenca somando as datas
			DiferencaData dateDifference = null;
			DiferencaData firstDateDifference = null;
			DiferencaData nextDateDifference = null;

			// Sera usado pra somar o tempo que sera cobrado a primeira parcela e as demais parcelas
			Date firstInstallmentDate = null;
			Date nextInstallmentDate = null;
			
			if (firstPaymentType == null || firstPaymentType.equals(0)) {
				// Verifica de somente ir� apresentar as formas de pgto com "Boleto Avulso", ou vice-versa
				if (issuanceType != null && issuanceType == Endorsement.ISSUANCE_TYPE_INVOICE) {
					if (pay.getFirstPaymentType() == Installment.PAYMENT_TYPE_BILLET_LOOSE) {
						if (!NumberUtilities.parseNull(invoiceType).equals(Term.INVOICE_TYPE_FLEXIBLE)) {
							continue;
						}
					}
				}
				else if (prodPay.getPaymentTermType() == ProductPaymentTerm.PAYMENT_TERM_TYPE_ALL ||
						 prodPay.getPaymentTermType() == ProductPaymentTerm.PAYMENT_TERM_TYPE_POLICY) {
					DiferencaData differenceDate = DateUtilities.fillDateDifference(DateUtilities.toDateTime(issueDate), DateUtilities.toDateTime(dtEffective));
					int diffDays = differenceDate.getDias();
	
					// Verifica se � uma renova��o/transferencia da propria Seguradora
					User user = AuthenticationHelper.getLoggedUser();
					if (user != null && (user.getAuthorizationType() != User.AUTHORIZATION_TYPE_BROKER )) {
						if (pay.getFirstPaymentType() == Installment.PAYMENT_TYPE_BILLET_LOOSE) {
							continue;
						}
					}
					else {
						if (pay.getFirstPaymentType() == Installment.PAYMENT_TYPE_BILLET_LOOSE) {
							if (diffDays > daysToBilletLoose)
								continue;
						}
						else {
							if (diffDays < daysToBilletLoose)
								continue;
						}
					}
				}
				else if (prodPay.getPaymentTermType() == ProductPaymentTerm.PAYMENT_TERM_TYPE_INVOICE) {
					if (pay.getFirstPaymentType() == Installment.PAYMENT_TYPE_BILLET_LOOSE) {
						if (!NumberUtilities.parseNull(invoiceType).equals(Term.INVOICE_TYPE_FLEXIBLE)) {
							continue;
						}
					}
				}
			} else {
				if (firstPaymentType.equals(Installment.PAYMENT_TYPE_BILLET_LOOSE)) {
					if (pay.getFirstPaymentType() != Installment.PAYMENT_TYPE_BILLET_LOOSE) {
							continue;
					}
				} else {
					if (pay.getFirstPaymentType() == Installment.PAYMENT_TYPE_BILLET_LOOSE) {
						continue;
					}
				}
			}
			int qtdeParcelas = 0;
			int diffDays = 0;

			Date effectiveDate = dtEffective;
			if (pay.getQuantityInstallment() != 0 &&
					effectiveDate.before(issueDate)) {
				effectiveDate = issueDate;
			}

			firstInstallmentDate = effectiveDate;
			if (daysToleranceInstallment != 0)
				firstInstallmentDate = DateUtilities.dateInFuture(effectiveDate, pay.getFirstMultipleType(), pay.getFirstInstallment());
			
			if (firstInstallmentDate.compareTo(dtExpiry) >= 0 &&
					pay.getQuantityInstallment() != 1)
				continue;

			firstDateDifference = DateUtilities.getDateDifference(firstInstallmentDate, dtExpiry);
			qtdeParcelas = pay.getQuantityInstallment();

			// Calcula a primeira data da proxima parcela se houver
			if(pay.getNextMultipleType() != null) {
				nextInstallmentDate = DateUtilities.dateInFuture(firstInstallmentDate, pay.getNextMultipleType(), pay.getNextInstallment());

				// Verifica se a proxima cobranca esta dentro do limite de dias comparado ao da primeira parcela
				diffDays = DateUtilities.getDateDifference(effectiveDate, nextInstallmentDate).getDias();
				if(nextInstallmentDate.compareTo(dtExpiry) >= 0 || diffDays <= daysToleranceInstallment)
					continue;

				nextDateDifference = DateUtilities.getDateDifference(nextInstallmentDate, dtExpiry);

				// Retirando a primeira parcela
				if(qtdeParcelas > 0)
					qtdeParcelas--;
			}

			dateDifference = nextDateDifference == null ? firstDateDifference : nextDateDifference;
			int frequency = PaymentTerm.getFrequencyByMultipleType(pay.getPaymentTermMultipleType());

			switch(pay.getPaymentTermMultipleType()) {
				case PaymentTerm.MULTIPLE_TYPE_ANNUAL:
					qtdeParcelas = qtdeParcelas == 0 ? (dateDifference.getMeses() / frequency) : qtdeParcelas;
					if(qtdeParcelas != 1 || nextDateDifference != null)
						continue;
					break;
				case PaymentTerm.MULTIPLE_TYPE_SIX_MONTHLY:
					qtdeParcelas = qtdeParcelas == 0 ? (dateDifference.getMeses() / frequency) : qtdeParcelas;
					if(qtdeParcelas < 1 || qtdeParcelas > 2 || dateDifference.getMeses() < (qtdeParcelas * frequency))
						continue;
					break;
				case PaymentTerm.MULTIPLE_TYPE_FOUR_MONTHLY:
					qtdeParcelas = qtdeParcelas == 0 ? (dateDifference.getMeses() / frequency) : qtdeParcelas;
					if(qtdeParcelas < 1 || qtdeParcelas > 3 || dateDifference.getMeses() < (qtdeParcelas * frequency))
						continue;
					break;
				case PaymentTerm.MULTIPLE_TYPE_THREE_MONTHLY:
					qtdeParcelas = qtdeParcelas == 0 ? (dateDifference.getMeses() / frequency) : qtdeParcelas;
					if(qtdeParcelas < 1 || qtdeParcelas > 4 || dateDifference.getMeses() < (qtdeParcelas * frequency))
						continue;
					break;
				case PaymentTerm.MULTIPLE_TYPE_TWO_MONTHLY:
					qtdeParcelas = qtdeParcelas == 0 ? (dateDifference.getMeses() / frequency) : qtdeParcelas;
					if(qtdeParcelas < 1 || qtdeParcelas > 6 || dateDifference.getMeses() < (qtdeParcelas * frequency))
						continue;
					break;
				case PaymentTerm.MULTIPLE_TYPE_MONTHLY:
					qtdeParcelas = qtdeParcelas == 0 ? dateDifference.getMeses() : qtdeParcelas;
					if(qtdeParcelas < 1 || qtdeParcelas > 12 || dateDifference.getMeses() < qtdeParcelas)
						continue;
					break;
			}

			if(nextDateDifference != null)
				qtdeParcelas++;

			ProductVersion productVersion = daoProductVersion.getProductVersionByRefDate(prodPay.getId().getProductId(), refDate);
			
			double premio = NumberUtilities.roundDecimalPlaces((netPremium + policyCost), Calculus.RESULT_DECIMAL_PLACES);
			fractioningAdditional = 0d;

			if (prodPay.isApplyFractioningAdditional() &&
					pay.getFractioningAdditionalFactor() != null && pay.getFractioningAdditionalFactor() > 0d) {
				fractioningAdditional =  NumberUtilities.roundDecimalPlaces(((premio * pay.getFractioningAdditionalFactor()) - premio), Calculus.RESULT_DECIMAL_PLACES);
			}

			if (fractioningAdditional != 0d) {
				if (qtdeParcelas == 1) {
					taxValue = NumberUtilities.roundDecimalPlaces((totalPremium - netPremium - policyCost - fractioningAdditional), Calculus.RESULT_DECIMAL_PLACES);
				}
				else {
					taxValue = NumberUtilities.roundDecimalPlaces(((premio + fractioningAdditional) * taxRate), Calculus.RESULT_DECIMAL_PLACES);
					totalPremium = NumberUtilities.roundDecimalPlaces((netPremium  + policyCost + fractioningAdditional + taxValue), Calculus.RESULT_DECIMAL_PLACES);
				}
			}

			PaymentOption paymentOption = new PaymentOption();
			paymentOption.setQuantityInstallment(pay.getQuantityInstallment());
			paymentOption.setNextInstallment(pay.getNextInstallment());
			paymentOption.setBillingMethodId(prodPay.getId().getBillingMethodId());
			paymentOption.setPaymentId(pay.getPaymentTermId());
			paymentOption.setPaymentType(this.mainPaymentType(pay));
			paymentOption.setPaymentTermName(pay.getName());
			paymentOption.setPaymentDescription(daoDataOption.getDataOption(pay.getFirstPaymentType()).getFieldDescription());
			paymentOption.setDueDay(dueDay);

			if (qtdeParcelas > 1) {
				double demais = 0d;
				double primeira = 0d;
				
				paymentOption.setQuantityInstallment(qtdeParcelas);
				paymentOption.setNextPaymentDate(nextInstallmentDate);

				demais = NumberUtilities.roundDecimalPlaces(totalPremium  / qtdeParcelas, Calculus.RESULT_DECIMAL_PLACES);
				primeira = NumberUtilities.roundDecimalPlaces(demais + (totalPremium - (demais * qtdeParcelas)), Calculus.RESULT_DECIMAL_PLACES);

				// Check value Minimum Installment
				if ((productVersion.getMinimumInstallment() != null) && (!productVersion.getMinimumInstallment().equals(0d)))
					if (demais < productVersion.getMinimumInstallment())
						continue;
				
				paymentOption.setFirstInstallmentValue(primeira);
				paymentOption.setNextInstallmentValue(demais);
				if ((pay.getNextPaymentType() != null) &&
						(pay.getFirstPaymentType() != pay.getNextPaymentType().intValue()))
					paymentOption.setPaymentDescription(paymentOption.getPaymentDescription() + "/" + daoDataOption.getDataOption(pay.getNextPaymentType()).getFieldDescription());
			}
			else {
				paymentOption.setFirstInstallmentValue(totalPremium);
				paymentOption.setNextInstallmentValue(0d);
			}
			
			if (qtInstallment != null && !qtInstallment.equals(0) && !qtInstallment.equals(paymentOption.getQuantityInstallment())) {
				continue;
			}
			
			paymentOption.setNetPremium(netPremium);
			paymentOption.setPolicyCost(policyCost);
			paymentOption.setFractioningAdditional(fractioningAdditional);
			paymentOption.setTaxRate(taxRate);
			paymentOption.setTaxValue(taxValue);
			paymentOption.setTotalPremium(totalPremium);
			paymentOption.setDefault(prodPay.isPaymentTermDefault());
			paymentOption.setSelected(false);
			paymentOption.setDisplayOrder(pay.getDisplayOrder());
			retorno.add(paymentOption);
		}
		orderPaymentOptions(retorno);
		return retorno;
	}

	private int mainPaymentType(PaymentTerm pay) {
		int paymentType = pay.getFirstPaymentType();
		if (pay.getNextPaymentType() != null) {
			paymentType = pay.getNextPaymentType();
		}
		return paymentType;
	}

	/**
	 * Used to do the order installment
	 */
	public void orderPaymentOptions(List<PaymentOption> paymentOptions) {
		Collections.sort(paymentOptions, new Comparator<PaymentOption>() {
			@Override
			public int compare(PaymentOption paymentOption1, PaymentOption  paymentOption2)
			{
				return  paymentOption1.getDisplayOrder().compareTo(paymentOption2.getDisplayOrder());
			}
		});
	}
}