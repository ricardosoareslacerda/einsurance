package br.com.tratomais.core.service.impl;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.hibernate.proxy.HibernateProxy;

import br.com.tratomais.core.dao.IDaoBean;
import br.com.tratomais.core.service.IServiceBean;

@SuppressWarnings("unchecked")
public abstract class ServiceBean implements IServiceBean {
	private static final Logger logger = Logger.getLogger( ServiceBean.class );

	protected IDaoBean daoBean;

	public void setDaoBean(IDaoBean daoBean) {
		this.daoBean = daoBean;
	}

	public Object load(Class clazz, Serializable id) {
		Object entity = daoBean.load(clazz, id);

		Boolean isLazyProxy = entity instanceof HibernateProxy && (((HibernateProxy) entity).getHibernateLazyInitializer().isUninitialized());

		if (isLazyProxy) {
			((HibernateProxy) entity).getHibernateLazyInitializer().initialize();
		}

		Object returnEntity = entity;
		// try {
		// returnEntity = Class.forName(clazz.getName()).cast(entity);
		// } catch ( ClassNotFoundException e ) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		return returnEntity;
	}
}
