package br.com.tratomais.core.service.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import br.com.tratomais.core.dao.IDaoRole;
import br.com.tratomais.core.infrastructure.AuthenticationHelper;
import br.com.tratomais.core.model.Functionality;
import br.com.tratomais.core.model.Module;
import br.com.tratomais.core.model.Role;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.service.IServiceRole;

public class ServiceRoleImpl extends ServiceBean implements IServiceRole {
	private static final Logger logger = Logger.getLogger( ServiceRoleImpl.class );

	private IDaoRole daoRole;

	public void setDaoRole(IDaoRole daoRole) {
		this.daoRole = daoRole;
	}

	public Role findById(Integer id) {
		return daoRole.findById(id);
	}

	public List<Role> findByName(String name) {
		return null;
	}

	public List<Role> listAll() {
		return daoRole.listAll();
	}

	public List<Role> listAllActiveRole() {
		return daoRole.listAllActive();
	}	
	
	public Collection<Role> listAll(User user) {
		Collection<Role> rolesOfUser = user.getRoles();
		if(AuthenticationHelper.getLoggedUser().isAdministrator() == false){
			for (Role roleUser : rolesOfUser) {
				roleUser.setTransientSelected(true);
			}
			return rolesOfUser;
		}
		Collection<Role> roles = this.listAllActiveRole();
		for (Role role : roles) {
			role.setTransientSelected(false);
			for (Role roleUser : rolesOfUser) {
				if (role.getRoleId() == roleUser.getRoleId()) {
					role.setTransientSelected(true);
				}
			}
		}
		return roles;
	}

	//
	public void deleteObject(Role entity) {

	}

	// TODO SALVAR TODAS AS FUNCIONALIDADES DE TODOS OS ROLES EM ROLES.FUNCIONALIDADES
	public Role saveObject(Role entity) {
		Set<Module> modulesSelected = new HashSet<Module>();
		Set<Functionality> functionalitiesSelected = new HashSet<Functionality>();

		this.setModulesAndFunctionalitiesSelected(entity.getModules(), modulesSelected , functionalitiesSelected);
		this.setFunctionalitys(entity.getFunctionalities(), functionalitiesSelected);

		entity.setModules(modulesSelected);
		entity.setFunctionalities(functionalitiesSelected);
		
		Role newRole = daoRole.save(entity);

		return newRole;
	}
	
	private void setFunctionalitys(Set<Functionality> functionalities, Set<Functionality> functionalitySelectted){
		int unchecked = 0;
		
		volta: for(Functionality workFunctionality : functionalities){

			if(workFunctionality.getChecked() == unchecked){
				continue volta;
			}
			
			if(functionalitySelectted.contains(workFunctionality)){
				continue volta;
			}

			functionalitySelectted.add(workFunctionality);

			if(workFunctionality.getFunctionalities() != null && workFunctionality.getFunctionalities().size() > 0){
				setFunctionalitys(workFunctionality.getFunctionalities(), functionalitySelectted);
			}

		}
	}

	private void setModulesAndFunctionalitiesSelected(Set<Module> modules, Set<Module> modulesSelected,  Set<Functionality> functionalities) {
		int unchecked = 0;

		for (Module module : modules) {
			if (module.getChecked() != unchecked) {
				modulesSelected.add(module);
				if ( module.getFunctionality() != null ) {
					functionalities.add(module.getFunctionality());	
				}
				
				if (module.getModules() != null) {
					this.setModulesAndFunctionalitiesSelected(module.getModules(), modulesSelected, functionalities);
				}
			}
		}
	}	
}
