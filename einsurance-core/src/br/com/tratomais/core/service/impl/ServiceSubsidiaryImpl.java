package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.tratomais.core.dao.IDaoSubsidiary;
import br.com.tratomais.core.model.customer.Insurer;
import br.com.tratomais.core.model.customer.Subsidiary;
import br.com.tratomais.core.service.IServiceInsurer;
import br.com.tratomais.core.service.IServiceSubsidiary;
import br.com.tratomais.core.util.Constantes;
import br.com.tratomais.core.util.TransportationClass;

public class ServiceSubsidiaryImpl extends ServiceBean implements IServiceSubsidiary {
	private static final Logger logger = Logger.getLogger( ServiceSubsidiaryImpl.class );

	private IDaoSubsidiary daoSubsidiary;

	private IServiceInsurer serviceInsurer;

	public void setServiceInsurer(IServiceInsurer serviceInsurer) {
		this.serviceInsurer = serviceInsurer;
	}

	public void setDaoSubsidiary(IDaoSubsidiary daoSubsidiary) {
		this.daoSubsidiary = daoSubsidiary;
	}

	public List<Subsidiary> findByName(String name) {
		return daoSubsidiary.findByName(name);
	}

	public List<Subsidiary> findByNickName(String userName) {
		return daoSubsidiary.findByNickName(userName);
	}

	public Subsidiary findById(Integer id) {
		return daoSubsidiary.findById(id);
	}

	public List<Subsidiary> listAll() {
		return daoSubsidiary.listAll();
	}

	public List<Subsidiary> listAllActiveSubsidiary() {
		return daoSubsidiary.listAllActive();
	}	
	
	public void deleteObject(Subsidiary entity) {
		daoSubsidiary.delete(entity);

	}

	public Subsidiary saveObject(Subsidiary subsidiary) {
		if (subsidiary.getInsurerId() == 0) {
			Insurer insurer = serviceInsurer.findById(Constantes.ID_INSURER_ZURICH);
			subsidiary.setInsurerId( insurer.getInsurerId());
		}
		return daoSubsidiary.save(subsidiary);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceSubsidiary#subsidiaryExternalCodeExists(int, int, java.lang.String)
	 */
	public boolean subsidiaryExternalCodeExists(int insurerId, int subsidiaryId, String externalCode){
		return daoSubsidiary.subsidiaryExternalCodeExists(insurerId, subsidiaryId, externalCode);
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceSubsidiary#listSubsidiaryToGrid(int, int, String, String, String, String, String)
	 */	
	public TransportationClass<Subsidiary> listSubsidiaryToGrid(int limit, int page, String columnSorted, String typeSorted,
																String search, String searchField, String searchString){	
		return daoSubsidiary.listSubsidiaryToGrid(limit, page, columnSorted, typeSorted, search, searchField, searchString);
	}

	@Override
	public List<Subsidiary> listSubsidiary(int insurerID, Date refDate) {
		return daoSubsidiary.listSubsidiary(insurerID, refDate);
	}
}
