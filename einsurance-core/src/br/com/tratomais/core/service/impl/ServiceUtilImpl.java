package br.com.tratomais.core.service.impl;

import java.util.List;

import br.com.tratomais.core.model.User;
import br.com.tratomais.core.service.IServiceUser;
import br.com.tratomais.core.util.Utilities;

public class ServiceUtilImpl extends ServiceBean {
	private IServiceUser serviceUserImpl;

	public void setServiceUser(IServiceUser serviceUser) {
		serviceUserImpl = serviceUser;
	}

	public String getStringHash(String hash) {
		return Utilities.getHashSha(hash);
	}

	public User getLoggedUser() {
		List<User> listAll = serviceUserImpl.listAll();
		for (User user : listAll) {
			return user;
		}
		return null;
	}
	
}
