package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.tratomais.core.dao.IDaoObject;
import br.com.tratomais.core.model.product.InsuredObject;
import br.com.tratomais.core.service.IServiceObject;

public class ServiceObjectImpl extends ServiceBean implements IServiceObject {
	private static final Logger logger = Logger.getLogger( ServiceObjectImpl.class );

	private IDaoObject daoObject;

	public void setDaoObject(IDaoObject daoObject) {
		this.daoObject = daoObject;
	}

	@Override
	public InsuredObject findById(Integer id) {
		return daoObject.findById(id);
	}

	@Override
	public List<InsuredObject> findByName(String name) {
		return daoObject.findByName(name);
	}

	@Override
	public List<InsuredObject> listAll() {
		return daoObject.listAll();
	}

	@Override
	public void deleteObject(InsuredObject entity) {
		daoObject.delete(entity);
	}

	@Override
	public InsuredObject saveObject(InsuredObject entity) {
		return daoObject.save(entity);
	}

	@Override
	public List<InsuredObject> listObject(Date refDate) {
		return daoObject.listObject(refDate);
	}

	@Override
	public InsuredObject getObject(int objectID, Date refDate) {
		return daoObject.getObject(objectID, refDate);
	}

	@Override
	public InsuredObject getObjectByID(int objectID) {
		return daoObject.getObjectByID(objectID);
	}

}
