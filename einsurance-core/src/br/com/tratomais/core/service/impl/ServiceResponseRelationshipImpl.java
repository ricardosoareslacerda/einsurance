package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.dao.IDaoResponseRelationship;
import br.com.tratomais.core.model.product.ResponseRelationship;
import br.com.tratomais.core.service.IServiceResponseRelationship;

public class ServiceResponseRelationshipImpl extends ServiceBean implements IServiceResponseRelationship {

	private IDaoResponseRelationship daoResponseRelationship;

	public void setDaoResponseRelationship(
			IDaoResponseRelationship daoResponseRelationship) {
		this.daoResponseRelationship = daoResponseRelationship;
	}
	
	@Override
	public List<ResponseRelationship> getResponseRelationship(Integer productId, Integer questionnaireId, Integer questionId, Integer responseId, Date referenceDate) {
		return daoResponseRelationship.getResponseRelationship(productId, questionnaireId, questionId, responseId, referenceDate);
	}

}