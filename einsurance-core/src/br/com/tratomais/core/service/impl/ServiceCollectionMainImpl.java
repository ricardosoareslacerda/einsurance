package br.com.tratomais.core.service.impl;

import java.util.List;

import br.com.tratomais.core.dao.IDaoCollectionMain;
import br.com.tratomais.core.model.paging.Paginacao;
import br.com.tratomais.core.model.report.CollectionDetailsMainParameters;
import br.com.tratomais.core.model.report.CollectionMainParameters;
import br.com.tratomais.core.model.report.CollectionDetailsMainResult;
import br.com.tratomais.core.model.report.BillingMethodResult;
import br.com.tratomais.core.model.report.CollectionMainResult;
import br.com.tratomais.core.model.report.StatusLote;
import br.com.tratomais.core.service.IServiceCollectionMain;

public class ServiceCollectionMainImpl extends ServiceBean implements IServiceCollectionMain {

	private IDaoCollectionMain daoCollectionMain;
	
	public void setDaoCollectionMain(IDaoCollectionMain daoCollectionMain){
		this.daoCollectionMain = daoCollectionMain;
	}
	@Override
	public Paginacao listCollectionDetailsMainPaging(CollectionDetailsMainParameters collectionDetailsMainParameters) {
		Paginacao paginacao = new Paginacao();
		paginacao.setTotalDados(daoCollectionMain.listCollectionDetailsMainTotalRecords(collectionDetailsMainParameters));
		paginacao.setListaDados(daoCollectionMain.listCollectionDetailsMain(collectionDetailsMainParameters));
		return paginacao;
	}
	
	@Override
	public List<CollectionDetailsMainResult> listCollectionDetailsMain(CollectionDetailsMainParameters collectionDetailsMainParameters) {
		return daoCollectionMain.listCollectionDetailsMain(collectionDetailsMainParameters);
	}
	@Override
	public List<BillingMethodResult> listBillingMethod() {
		return daoCollectionMain.listBillingMethod();
	}
	@Override
	public List<CollectionMainResult> listCollectionMain(CollectionMainParameters collectionMainParameters) {
		return daoCollectionMain.listCollectionMain(collectionMainParameters);
	}
	@Override
	public Paginacao listCollectionMainPaging(CollectionMainParameters collectionMainParameters) {
		Paginacao paginacao = new Paginacao();
		paginacao.setListaDados(daoCollectionMain.listCollectionMain(collectionMainParameters));
		paginacao.setTotalDados(daoCollectionMain.listCollectionMainTotalRecords(collectionMainParameters));
		return paginacao;
	}
	@Override
	public List<StatusLote> listStatusLote() {
		// TODO Auto-generated method stub
		return daoCollectionMain.listStatusLote();
	}
	@Override
	public List<BillingMethodResult> listExchangeCollection() {
		return daoCollectionMain.listExchangeCollection();
	}
}
