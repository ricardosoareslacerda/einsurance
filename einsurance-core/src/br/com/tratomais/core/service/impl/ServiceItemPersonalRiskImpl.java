package br.com.tratomais.core.service.impl;

import java.util.List;

import br.com.tratomais.core.dao.IDaoItemPersonalRisk;
import br.com.tratomais.core.model.policy.ItemPersonalRisk;
import br.com.tratomais.core.service.IServiceItemPersonalRisk;

public class ServiceItemPersonalRiskImpl extends ServiceBean implements IServiceItemPersonalRisk{

	private IDaoItemPersonalRisk daoItemPersonalRisk;
	
	public void setDaoItemPersonalRisk(IDaoItemPersonalRisk daoItemPersonalRisk){
		this.daoItemPersonalRisk = daoItemPersonalRisk;
	}
	
	@Override
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId, int endorsementId, int itemId) {
		return daoItemPersonalRisk.listItemPersonalRisk(contractId, endorsementId, itemId);
	}

	@Override
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId, int endorsementId) {
		return daoItemPersonalRisk.listItemPersonalRisk(contractId, endorsementId);
	}

	@Override
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId) {
		return daoItemPersonalRisk.listItemPersonalRisk(contractId);
	}

	@Override
	public ItemPersonalRisk findById(Integer id) {
		return daoItemPersonalRisk.findById(id);
	}

	@Override
	public List<ItemPersonalRisk> findByName(String name) {
		return daoItemPersonalRisk.findByName(name);
	}

	@Override
	public List<ItemPersonalRisk> listAll() {
		return daoItemPersonalRisk.listAll();
	}

}
