package br.com.tratomais.core.service.impl;

import java.util.Collection;
import java.util.List;

import br.com.tratomais.core.dao.IDaoUserPartner;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.UserPartner;
import br.com.tratomais.core.service.IServiceUserPartner;

public class ServiceUserPartnerImpl extends ServiceBean implements IServiceUserPartner {
	private IDaoUserPartner daoUserPartner;

	public void setDaoUserPartner(IDaoUserPartner daoUserPartner) {
		this.daoUserPartner = daoUserPartner;
	}

	public Collection<UserPartner> findByUser(User user) {
		return daoUserPartner.findByUser(user);
	}

	// TODO implementar
	public UserPartner findById(Integer id) {
		return null;
	}

	// TODO Auto-generated method stub
	public List<UserPartner> findByName(String name) {
		return null;
	}

	// TODO Auto-generated method stub
	public List<UserPartner> listAll() {
		return null;
	}

}
