package br.com.tratomais.core.service.impl;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import br.com.tratomais.core.claim.ReturnClaimStateVerification;
import br.com.tratomais.core.dao.IDaoBean;
import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.infrastructure.AuthenticationHelper;
import br.com.tratomais.core.model.City;
import br.com.tratomais.core.model.Country;
import br.com.tratomais.core.model.Domain;
import br.com.tratomais.core.model.Functionality;
import br.com.tratomais.core.model.Inflow;
import br.com.tratomais.core.model.Login;
import br.com.tratomais.core.model.Module;
import br.com.tratomais.core.model.PartnerAndChannel;
import br.com.tratomais.core.model.Role;
import br.com.tratomais.core.model.State;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.auto.AutoBrand;
import br.com.tratomais.core.model.auto.AutoModel;
import br.com.tratomais.core.model.auto.AutoVersion;
import br.com.tratomais.core.model.customer.Activity;
import br.com.tratomais.core.model.customer.Broker;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Customer;
import br.com.tratomais.core.model.customer.Insurer;
import br.com.tratomais.core.model.customer.Occupation;
import br.com.tratomais.core.model.customer.Partner;
import br.com.tratomais.core.model.customer.Profession;
import br.com.tratomais.core.model.customer.Subsidiary;
import br.com.tratomais.core.model.interfaces.FileLot;
import br.com.tratomais.core.model.interfaces.FileLotDetails;
import br.com.tratomais.core.model.interfaces.Lot;
import br.com.tratomais.core.model.paging.Paginacao;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.EndorsementOption;
import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.policy.ItemPersonalRisk;
import br.com.tratomais.core.model.policy.MasterPolicy;
import br.com.tratomais.core.model.product.BillingAgency;
import br.com.tratomais.core.model.product.BillingMethod;
import br.com.tratomais.core.model.product.Branch;
import br.com.tratomais.core.model.product.ConversionRate;
import br.com.tratomais.core.model.product.Coverage;
import br.com.tratomais.core.model.product.CoverageOption;
import br.com.tratomais.core.model.product.CoveragePlan;
import br.com.tratomais.core.model.product.CoverageRangeValue;
import br.com.tratomais.core.model.product.Currency;
import br.com.tratomais.core.model.product.DataOption;
import br.com.tratomais.core.model.product.Deductible;
import br.com.tratomais.core.model.product.IdentifiedList;
import br.com.tratomais.core.model.product.InsuredObject;
import br.com.tratomais.core.model.product.PaymentOption;
import br.com.tratomais.core.model.product.PersonalRiskOption;
import br.com.tratomais.core.model.product.Plan;
import br.com.tratomais.core.model.product.PlanKinship;
import br.com.tratomais.core.model.product.PlanRiskType;
import br.com.tratomais.core.model.product.Product;
import br.com.tratomais.core.model.product.ProductOption;
import br.com.tratomais.core.model.product.ProductPaymentTerm;
import br.com.tratomais.core.model.product.ProductPlan;
import br.com.tratomais.core.model.product.ProductTerm;
import br.com.tratomais.core.model.product.ProductVersion;
import br.com.tratomais.core.model.product.Profile;
import br.com.tratomais.core.model.product.ProposalListData;
import br.com.tratomais.core.model.product.QuoteListData;
import br.com.tratomais.core.model.product.ResponseRelationship;
import br.com.tratomais.core.model.product.SimpleCalcResult;
import br.com.tratomais.core.model.report.BillingMethodResult;
import br.com.tratomais.core.model.report.CollectionDetailsMainParameters;
import br.com.tratomais.core.model.report.CollectionDetailsMainResult;
import br.com.tratomais.core.model.report.CollectionMainParameters;
import br.com.tratomais.core.model.report.CollectionMainResult;
import br.com.tratomais.core.model.report.DataViewInterface;
import br.com.tratomais.core.model.report.Report;
import br.com.tratomais.core.model.report.ReportListData;
import br.com.tratomais.core.model.report.StatusLote;
import br.com.tratomais.core.model.search.SearchDocumentParameters;
import br.com.tratomais.core.model.search.SearchDocumentResult;
import br.com.tratomais.core.service.IServiceAuto;
import br.com.tratomais.core.service.IServiceBillingMethod;
import br.com.tratomais.core.service.IServiceBranch;
import br.com.tratomais.core.service.IServiceBroker;
import br.com.tratomais.core.service.IServiceCalc;
import br.com.tratomais.core.service.IServiceChannel;
import br.com.tratomais.core.service.IServiceCity;
import br.com.tratomais.core.service.IServiceCollectionMain;
import br.com.tratomais.core.service.IServiceConversionRate;
import br.com.tratomais.core.service.IServiceCountry;
import br.com.tratomais.core.service.IServiceCoverage;
import br.com.tratomais.core.service.IServiceCoveragePlan;
import br.com.tratomais.core.service.IServiceCurrency;
import br.com.tratomais.core.service.IServiceDataOption;
import br.com.tratomais.core.service.IServiceDeductible;
import br.com.tratomais.core.service.IServiceDomain;
import br.com.tratomais.core.service.IServiceEinsurance;
import br.com.tratomais.core.service.IServiceFunctionality;
import br.com.tratomais.core.service.IServiceInsurer;
import br.com.tratomais.core.service.IServiceInterface;
import br.com.tratomais.core.service.IServiceItemPersonalRisk;
import br.com.tratomais.core.service.IServiceLogin;
import br.com.tratomais.core.service.IServiceMasterPolicy;
import br.com.tratomais.core.service.IServiceModule;
import br.com.tratomais.core.service.IServiceObject;
import br.com.tratomais.core.service.IServicePartner;
import br.com.tratomais.core.service.IServicePersonalRiskOption;
import br.com.tratomais.core.service.IServicePlan;
import br.com.tratomais.core.service.IServicePolicy;
import br.com.tratomais.core.service.IServiceProduct;
import br.com.tratomais.core.service.IServiceProductVersion;
import br.com.tratomais.core.service.IServiceResponseRelationship;
import br.com.tratomais.core.service.IServiceRole;
import br.com.tratomais.core.service.IServiceSearchDocument;
import br.com.tratomais.core.service.IServiceState;
import br.com.tratomais.core.service.IServiceSubsidiary;
import br.com.tratomais.core.service.IServiceUser;
import br.com.tratomais.core.service.ReturnValue;
import br.com.tratomais.core.service.erros.ErrorList;
import br.com.tratomais.core.util.Constantes;
import br.com.tratomais.core.util.TransportationClass;
import br.com.tratomais.core.util.findcep.IFindCEP;
import br.com.tratomais.validation.bank.BankValidationException;
import br.com.tratomais.validation.bank.IAccountInfo;
import br.com.tratomais.validation.bank.IValidateBankInfo;

public class ServiceEinsuranceImpl extends ServiceBean implements IServiceEinsurance{
//	private static final Logger logger = Logger.getLogger( ServiceEinsuranceImpl.class );
	
	private transient IServiceBroker serviceBroker;
	private transient IServiceChannel serviceChannel;
	private transient IServiceCity serviceCity;
	private transient IServiceCountry serviceCountry;
	private transient IServiceDataOption serviceDataOption;
	private transient IServiceSubsidiary serviceSubsidiary;
	private transient IServiceLogin serviceLogin;
	private transient IServiceModule serviceModule;
	private transient IServicePartner servicePartner;
	private transient IServiceProduct serviceProduct;
	private transient IServiceRole serviceRole;
	private transient IServiceState serviceState;
	private transient IServiceUser serviceUser;
	private transient IServicePolicy servicePolicy;
	private transient IServiceDomain serviceDomain;
	private transient IServiceProductVersion serviceProductVersion;
	private transient IServiceCoverage serviceCoverage;
	private transient IServiceCoveragePlan serviceCoveragePlan;
	private transient IServicePlan servicePlan;
	private transient IServiceObject serviceObject;
	private transient IServiceMasterPolicy serviceMasterPolicy;
	private transient IServicePersonalRiskOption servicePersonalRiskOption;
	private transient IServiceCalc serviceCalc;
	private transient IServiceSearchDocument serviceSearchDocument;
	private transient IServiceCollectionMain serviceCollectionMain;
	private transient IServiceItemPersonalRisk serviceItemPersonalRisk;
	private transient IServiceAuto serviceAuto;
	private transient IServiceResponseRelationship serviceResponseRelationship;
	private transient IServiceDeductible serviceDeductible;
	private transient IServiceInterface serviceInterface;
	private transient IServiceBranch serviceBranch;
	private transient IServiceCurrency serviceCurrency;
	private transient IServiceConversionRate serviceConversionRate;
	private transient IServiceInsurer serviceInsurer;
	private transient IServiceBillingMethod serviceBillingMethod;
	private transient IServiceFunctionality serviceFunctionality;
	private transient IFindCEP findCEP;
	
	
	public void setServiceDeductible(IServiceDeductible serviceDeductible) {
		this.serviceDeductible = serviceDeductible;
	}
		
	public void setServiceAuto(IServiceAuto serviceAuto) {
		this.serviceAuto = serviceAuto;
	}	

	public void setServiceBroker(IServiceBroker serviceBroker) {
		this.serviceBroker = serviceBroker;
	}

	public void setServiceChannel(IServiceChannel serviceChannel) {
		this.serviceChannel = serviceChannel;
	}

	public void setServiceCity(IServiceCity serviceCity) {
		this.serviceCity = serviceCity;
	}

	public void setServiceCountry(IServiceCountry serviceCountry) {
		this.serviceCountry = serviceCountry;
	}

	public void setServiceDataOption(IServiceDataOption serviceDataOption) {
		this.serviceDataOption = serviceDataOption;
	}

	public void setServiceSubsidiary(IServiceSubsidiary serviceSubsidiary) {
		this.serviceSubsidiary = serviceSubsidiary;
	}

	public void setServiceLogin(IServiceLogin serviceLogin) {
		this.serviceLogin = serviceLogin;
	}

	public void setServiceModule(IServiceModule serviceModule) {
		this.serviceModule = serviceModule;
	}

	public void setServicePartner(IServicePartner servicePartner) {
		this.servicePartner = servicePartner;
	}

	public void setServiceProduct(IServiceProduct serviceProduct) {
		this.serviceProduct = serviceProduct;
	}

	public void setServiceRole(IServiceRole serviceRole) {
		this.serviceRole = serviceRole;
	}

	public void setServiceState(IServiceState serviceState) {
		this.serviceState = serviceState;
	}

	public void setServiceUser(IServiceUser serviceUser) {
		this.serviceUser = serviceUser;
	}
	
	public void setServiceBillingMethod(IServiceBillingMethod serviceBillingMethod) {
		this.serviceBillingMethod = serviceBillingMethod;
	}

	public void setDaoBean(IDaoBean daoBean) {
		this.daoBean = daoBean;
	}
	
	public void setServiceCollectionMainMain(IServiceCollectionMain serviceCollectionMain){
		this.serviceCollectionMain = serviceCollectionMain;
	}

	/**
	 * Represents the service of Coverage.
	 */
	public void setServiceCoverage(IServiceCoverage serviceCoverage) {
		this.serviceCoverage = serviceCoverage;
	}

	/**
	 * Represents the service of Coverage Plan.
	 */
	public void setServiceCoveragePlan(IServiceCoveragePlan serviceCoveragePlan) {
		this.serviceCoveragePlan = serviceCoveragePlan;
	}

	/**
	 * Represents the service of InsuredObject.
	 */
	public void setServiceObject(IServiceObject serviceObject) {
		this.serviceObject = serviceObject;
	}

	/**
	 * Represents the service of Plan.
	 */
	public void setServicePlan(IServicePlan servicePlan) {
		this.servicePlan = servicePlan;
	}

	/**
	 * Represents the service of product version.
	 */
	public void setServiceProductVersion(IServiceProductVersion serviceProductVersion) {
		this.serviceProductVersion = serviceProductVersion;
	}

	/**
	 * Represents the service of Activity version.
	 */
	public void setServicePolicy(IServicePolicy servicePolicy) {
		this.servicePolicy = servicePolicy;

	}

	/**
	 * Represents the service of Domain version.
	 */
	public void setServiceDomain(IServiceDomain serviceDomain) {
		this.serviceDomain = serviceDomain;
	}

	/**
	 * Represents the service of Calc version.
	 */
	public void setServiceCalc(IServiceCalc serviceCalc) {
		this.serviceCalc = serviceCalc;
	}

	public void setServiceMasterPolicy(IServiceMasterPolicy serviceMasterPolicy) {
		this.serviceMasterPolicy = serviceMasterPolicy;
	}

	public void setServicePersonalRiskOption(IServicePersonalRiskOption servicePersonalRiskOption) {
		this.servicePersonalRiskOption = servicePersonalRiskOption;
	}

	public void setServiceSearchDocument(IServiceSearchDocument serviceSearchDocument) {
		this.serviceSearchDocument = serviceSearchDocument;
	}
	
	public void setServiceResponseRelationship(IServiceResponseRelationship serviceResponseRelationship) {
		this.serviceResponseRelationship = serviceResponseRelationship;
	}
	
	public void setServiceItemPersonalRisk(IServiceItemPersonalRisk serviceItemPersonalRisk) {
		this.serviceItemPersonalRisk = serviceItemPersonalRisk;
	}
	
	public void setServiceInterface(IServiceInterface serviceInterface){
		this.serviceInterface = serviceInterface;
	}
	
	public void setServiceBranch(IServiceBranch serviceBranch){
		this.serviceBranch = serviceBranch;
	}
	
	public void setServiceCurrecy(IServiceCurrency serviceCurrency){
		this.serviceCurrency = serviceCurrency;
	}
	
	public void setServiceConversionRate(IServiceConversionRate serviceConversionRate){
		this.serviceConversionRate = serviceConversionRate;
	}
	
	public void setServiceInsurer(IServiceInsurer serviceInsurer){
		this.serviceInsurer = serviceInsurer;
	}

	public void setServiceFunctionality(IServiceFunctionality serviceFunctionality){
		this.serviceFunctionality = serviceFunctionality;
	}
	
	public void setFindCEP(IFindCEP findCEP) {
		this.findCEP = findCEP;
	}
	

	private transient IValidateBankInfo validateBankInfo;
	public void setValidateBankInfo(IValidateBankInfo validateBankInfo) {
		this.validateBankInfo = validateBankInfo;
	}
	
	/**
	 * {@inheritDoc}
	 * @throws Exception 
	 */
	public User validateLogin(String userName, String passwordKey) throws ServiceException{
		return this.serviceLogin.validateLogin(userName, passwordKey);
	}

	/**
	 * {@inheritDoc}
	 */
	public void deleteLoginByLoginName(String loginName, String domain) throws ServiceException{
		this.serviceLogin.deleteObjectByLoginName(loginName, domain);
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean loginExistence(String login) throws ServiceException { 
		return this.serviceLogin.loginExistence(login);
	}

	/**
	 * {@inheritDoc}
	 */
	public Login saveLogin(Login login) throws ServiceException { 
		return this.serviceLogin.saveObject(login);
	}

	/**
	 * {@inheritDoc}
	 */
	public Login saveLoginByUser(User user) throws ServiceException { 
		return this.serviceLogin.saveObject(user);
	}

	/**
	 * {@inheritDoc}
	 */
	public Login findLoginByUser(User user) throws ServiceException { 
		return this.serviceLogin.findByUser(user);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Module> listAllModuleByUser(User user) throws ServiceException { 
		return this.serviceModule.listAllByUser(user);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Module> listAllModule() throws ServiceException { 
		return this.serviceModule.listAll();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Broker> listAllBroker() throws ServiceException { 
		return this.serviceBroker.listAll();
	}

	/**
	 * {@inheritDoc}
	 */
	public Broker saveBroker(Broker broker) throws ServiceException { 
		return this.serviceBroker.saveObject(broker);
	}

	/**
	 * {@inheritDoc}
	 */
	public Channel saveChannel(Channel entity) throws ServiceException { 
		return this.serviceChannel.saveObject(entity);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Channel> listAllChannel() throws ServiceException { 
		return this.serviceChannel.listAll();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Channel> findChannelByNameAndPartner(String name, Partner partner) throws ServiceException { 
		return this.serviceChannel.findByNameAndPartner(name, partner);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Channel> findChannelByPartnerId(int partnerId, boolean onlyActive) throws ServiceException { 
		return this.serviceChannel.findByPartnerId(partnerId, onlyActive);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<City> listAllCity() throws ServiceException { 
		return this.serviceCity.listAll();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Country> listAllCountry() throws ServiceException { 
		return this.serviceCountry.listAll();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<DataOption> listDataOptionById(int dataGroupId) throws ServiceException { 
		return this.serviceDataOption.listDataOption(dataGroupId);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<DataOption> listDataOptionByIdAndParent(int dataGroupId, int parentId) throws ServiceException { 
		return this.serviceDataOption.listDataOptionByIdAndParent(dataGroupId, parentId);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Subsidiary> listAllSubsidiary() throws ServiceException { 
		return this.serviceSubsidiary.listAll();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Subsidiary> listAllActiveSubsidiary() throws ServiceException { 
		return this.serviceSubsidiary.listAllActiveSubsidiary();
	}

	/**
	 * {@inheritDoc}
	 */
	public Subsidiary saveSubsidiary(Subsidiary filial) throws ServiceException { 
		return this.serviceSubsidiary.saveObject(filial);
	}

	/**
	 * {@inheritDoc}
	 */
	public Subsidiary findSubsidiaryById(Integer subsidiaryId) throws ServiceException { 
		return this.serviceSubsidiary.findById(subsidiaryId);
	}	
	
	/**
	 * {@inheritDoc}
	 */
	public TransportationClass<Subsidiary> listSubsidiaryToGrid(int limit, int page, String columnSorted, String typeSorted,
																String search, String searchField, String searchString) throws ServiceException{
		return this.serviceSubsidiary.listSubsidiaryToGrid(limit, page, columnSorted, typeSorted, search, searchField, searchString);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<Partner> listAllPartner() throws ServiceException { 
		return this.servicePartner.listAll();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Partner> listAllPartner(User user) throws ServiceException { 
		return this.servicePartner.listAll(user);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Partner> listAllActivePartner() throws ServiceException { 
		return this.servicePartner.listAllActivePartner();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Partner> listAllActivePartner(User user) throws ServiceException { 
		return this.servicePartner.listAllActivePartner(user);
	}

	/**
	 * {@inheritDoc}
	 */
	public Partner savePartner(Partner entity) throws ServiceException { 
		return this.servicePartner.saveObject(entity);
	}

	/**
	 * {@inheritDoc}
	 */
	public Partner savePartnerInsert(Partner entity) throws ServiceException { 
		return this.servicePartner.saveObjectInsert(entity);
	}

	/**
	 * {@inheritDoc}
	 */
	public Role saveRole(Role entity) throws ServiceException { 
		return this.serviceRole.saveObject(entity);
	}

	/**
	 * {@inheritDoc}
	 */
	public Collection<Role> listAllRole() throws ServiceException { 
		return this.serviceRole.listAll();
	}

	/**
	 * {@inheritDoc}
	 */
	public Collection<Role> listAllRoleByUser(User user) throws ServiceException { 
		return this.serviceRole.listAll(user);
	}

	public Collection<Role> listAllActiveRole() throws ServiceException { 
		return this.serviceRole.listAllActiveRole();
	}

	/**
	 * {@inheritDoc}
	 */
	public void changePartnerDefaultLoggedUser(int partnerId) throws ServiceException { 
		this.serviceUser.changePartnerDefaultLoggedUser(partnerId);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<State> listAllState() throws ServiceException { 
		return this.serviceState.listAll();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<User> listUserAllowed() throws ServiceException { 
		return this.serviceUser.listUserAllowed();
	}

	/**
	 * {@inheritDoc}
	 */
	public User saveUserInsert(User user, Login login) throws ServiceException { 
		return this.serviceUser.saveObjectInsert(user, login);
	}

	/**
	 * {@inheritDoc}
	 */
	public User saveUser(User user, Login login) throws ServiceException { 
		return this.serviceUser.saveObject(user, login);
	}

	/**
	 * {@inheritDoc}
	 */
	public User findUserById(Integer userId) throws ServiceException { 
		return this.serviceUser.findById(userId);
	}	
	
	/**
	 * {@inheritDoc}
	 */
	public TransportationClass<User> listUserToGrid(int limit, int page, String columnSorted, String typeSorted, String search, 
													String searchField, String searchString) throws ServiceException{
		return this.serviceUser.listUserToGrid(limit, page, columnSorted, typeSorted, search, searchField, searchString);
	}	
	
	/**
	 * {@inheritDoc}
	 */
	public TransportationClass<User> listUserAllActivePartnerToGrid(int limit, int page, int userId){
		return this.serviceUser.listUserAllActivePartnerToGrid(limit, page, userId);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<DataOption> findDataOptionByDataGroup(int dataGroupId) throws ServiceException { 
		return this.serviceDataOption.listDataOption(dataGroupId);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<DataOption> findDataOptionByDataGroup(int dataGroupId, boolean bringAll) throws ServiceException { 
		return this.serviceDataOption.listDataOption(dataGroupId, bringAll);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<DataOption> findDataOptionByDataGroup(int dataGroupId, boolean bringAll, String externalCode) throws ServiceException { 
		return this.serviceDataOption.listDataOption(dataGroupId, bringAll, externalCode);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public ProductVersion findProductVersionById(Integer id) throws ServiceException { 
		return this.serviceProductVersion.findById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<ProductVersion> findProductVersionByName(String name) throws ServiceException { 
		return this.serviceProductVersion.findByName(name);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<ProductVersion> listAllProductVersion() throws ServiceException { 
		return this.serviceProductVersion.listAll();
	}

	/**
	 * {@inheritDoc}
	 */
	public ProductVersion saveProductVersion(ProductVersion entity) throws ServiceException { 
		return this.serviceProductVersion.saveObject(entity);
	}

	/**
	 * {@inheritDoc}
	 */
	public Coverage findCoverageById(Integer id) throws ServiceException { 
		return this.serviceCoverage.findById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Coverage> findCoverageByName(String name) throws ServiceException { 
		return this.serviceCoverage.findByName(name);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Coverage> listAllCoverage() throws ServiceException { 
		return this.serviceCoverage.listAll();
	}

	/**
	 * {@inheritDoc}
	 */
	public Coverage saveCoverage(Coverage entity) throws ServiceException { 
		return this.serviceCoverage.saveObject(entity);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Coverage> findAllCoverageByObject(Integer objectId) throws ServiceException { 
		return this.serviceCoverage.findByObject(objectId);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Plan savePlan(Plan entity) throws ServiceException { 
		return this.servicePlan.saveObject(entity);
	}

	/**
	 * {@inheritDoc}
	 */
	public Plan findPlanById(Integer id) throws ServiceException { 
		return this.servicePlan.findById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Plan> findPlanByName(String name) throws ServiceException { 
		return this.servicePlan.findByName(name);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Plan> listAllPlan() throws ServiceException { 
		return this.servicePlan.listAll();
	}

	/**
	 * {@inheritDoc}
	 */
	public CoveragePlan findCoveragePlanById(Integer id) throws ServiceException { 
		return this.serviceCoveragePlan.findById(id);
	}

	
	/**
	 * {@inheritDoc}
	 */
	public CoveragePlan saveCoveragePlan(CoveragePlan coveragePlan) throws ServiceException { 
		return this.serviceCoveragePlan.saveObject(coveragePlan);
	}

	/**
	 * {@inheritDoc}
	 */
	public InsuredObject findObjectById(Integer id) throws ServiceException { 
		return this.serviceObject.findById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<InsuredObject> findObjectByName(String name) throws ServiceException { 
		return this.serviceObject.findByName(name);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<InsuredObject> listAllObject() throws ServiceException { 
		return this.serviceObject.listAll();
	}

	/**
	 * {@inheritDoc}
	 */
	public InsuredObject saveObject(InsuredObject insuredObject) throws ServiceException { 
		return this.serviceObject.saveObject(insuredObject);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<CoveragePlan> listCoveragePlanByRefDate(int productId, int planId, Date refDate) throws ServiceException { 
		return this.serviceProduct.listCoveragePlanByRefDate(productId, planId, refDate);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<ProductPlan> findPlanByProductId(int productId, Date refDate) throws ServiceException { 
		return this.serviceProduct.findPlanByProductId(productId, refDate);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<ProductPlan> findRiskPlanByProductId(int productId, Date refDate) throws ServiceException { 
		return this.serviceProduct.findRiskPlanByProductId(productId, refDate);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<ProductPlan> listPersonalRiskPlanByProductId(int productId, Date refDate) throws ServiceException { 
		return this.serviceProduct.listPersonalRiskPlanByProductId(productId, refDate);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<ProductOption> findProductsbyPartner(int partnerId, int insurerId, Date refDate) throws ServiceException { 
		return this.serviceProduct.findProductsbyPartner(partnerId, insurerId, refDate);
	}

	/**
	 * {@inheritDoc}
	 */

	public List<Activity> listAllActivity() throws ServiceException { 
		return this.servicePolicy.listAllActivity();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Occupation> listAllOccupation() throws ServiceException { 
		return this.servicePolicy.listAllOccupation();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Profession> listAllProfession() throws ServiceException { 
		return this.servicePolicy.listAllProfession();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Inflow> listAllInflow(String inflowType) throws ServiceException { 
		return this.servicePolicy.listAllInflow(inflowType);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Domain> listAllDomain() throws ServiceException { 
		return serviceDomain.listAll();
	}

	public Endorsement saveEndorsement(Endorsement endorsement) throws ServiceException { 
		return servicePolicy.saveEndorsement(endorsement);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<Domain> listAllDomainActive() throws ServiceException { 
		return serviceDomain.listAllActive();
	}

	/**
	 * {@inheritDoc}
	 */
	public Customer saveCustomer(Customer customer) throws ServiceException { 
		return servicePolicy.saveCustomer(customer);
	}

	/**
	 * {@inheritDoc}
	 */
	public SimpleCalcResult simpleCalc(Endorsement endorsement) throws ServiceException { 
		return this.serviceCalc.simpleCalc(endorsement);
	}

	public SimpleCalcResult propertyCalc (Endorsement endorsement, List<CoverageOption> coverageOptionList){
		return this.serviceCalc.propertyCalc(endorsement, coverageOptionList);
	}

	public SimpleCalcResult calculate (Endorsement endorsement, int objectId, List<CoverageOption> coverageOptionList){
		SimpleCalcResult result = this.serviceCalc.calculate(endorsement, objectId, coverageOptionList);
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<MasterPolicy> listAllMasterPolicy() throws ServiceException { 
		return this.serviceMasterPolicy.listAll();
	}

	public MasterPolicy findMasterPolicyById(int id) throws ServiceException{
		return this.serviceMasterPolicy.findById(id);
	}	
	
	/**
	 * {@inheritDoc}
	 */
	public List<Product> listAllProduct() throws ServiceException { 
		return this.serviceProduct.listAllProduct();
	}
	
	public List<Profile> findProfileList(int productId, Date referenceDate) throws ServiceException {
		return this.serviceProduct.findProfileList(productId, referenceDate);
	}

	/**
	 * {@inheritDoc}
	 */
	public MasterPolicy saveMasterPolicy(MasterPolicy masterPolicy) throws ServiceException { 
		return this.serviceMasterPolicy.saveObject(masterPolicy);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<MasterPolicy> listAllWithDescriptions() throws ServiceException { 
		return this.serviceMasterPolicy.listAllWithDescriptions();
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public ProposalListData loadProposalList() throws ServiceException { 

		ProposalListData proposalListData = new ProposalListData();

		proposalListData.setListAddressType(listDataOptionById(Constantes.ADDRESS_TYPE));
		proposalListData.setListPhoneType(listDataOptionById(Constantes.PHONE_TYPE));
		//proposalListData.setListActivity(listAllActivity());
		proposalListData.setListGender(listDataOptionById(Constantes.GENDER_TYPE));
		proposalListData.setListKinshipType(listDataOptionById(Constantes.KINSHIP_TYPE));
		proposalListData.setListMaritalStatus(listDataOptionById(Constantes.MARITAL_STATUS));
		//proposalListData.setListProfessionType(listDataOptionById(Constantes.PROFESSION_TYPE));
		//proposalListData.setListPersonType(servicePersonalRiskOption.findPersonalRiskOptions());
		//proposalListData.setListOccupation(listAllOccupation());
		//proposalListData.setListProfession(listAllProfession());
		
		proposalListData.setListDocumentTypeLegally(listDataOptionByIdAndParent(Constantes.DOCUMENT_TYPE, Constantes.LEGALLY_PERSON));
		proposalListData.setListDocumentTypeNatural(listDataOptionByIdAndParent(Constantes.DOCUMENT_TYPE, Constantes.NATURAL_PERSON));

		return proposalListData;
	}

	/**
	 * {@inheritDoc}
	 */
	public QuoteListData loadQuoteList(int productId, Date referenceDate, int planId, int objectId) throws ServiceException { 
		QuoteListData quoteListData = new QuoteListData();
		
		quoteListData.setListGender(listDataOptionById(Constantes.GENDER_TYPE));
		
		if(objectId == Item.OBJECT_CICLO_VITAL)quoteListData.setListPersonalRiskPlan(listPersonalRiskPlanByProductId(productId, referenceDate));
		if(objectId != Item.OBJECT_HABITAT)quoteListData.setListProductPlan(findPlanByProductId(productId, referenceDate));
		
		if(objectId == Item.OBJECT_ACCIDENTES_PERSONALES){
			quoteListData.setListOccupation(listAllOccupation());
			quoteListData.setListPlanRiskType(listPlanRiskTypeDescriptionByRefDate(productId, Constantes.ID_PLAN_RISK_DEFAULT, referenceDate));
		}
		
		if(objectId == Item.OBJECT_HABITAT){
			quoteListData.setListPlanRisk(findRiskPlanByProductId(productId, referenceDate));
			quoteListData.setListHousing(findDataOptionByDataGroup(Constantes.ID_DATA_GROUP_HOUSING));
			quoteListData.setProductVersion(getProductVersionByRefDate(productId, referenceDate));
		}
		
		if(planId > 0){			
			quoteListData.setProductPlan(getPlanById(productId, planId, referenceDate));
			if(objectId == Item.OBJECT_HABITAT){
				PlanRiskType planRisk = quoteListData.getProductPlan().getPlanRiskTypeDefault();
				switch ((planRisk != null ? planRisk.getId().getRiskType() : 0)) {
				case Coverage.RISKTYPE_STRUCTURE:
					quoteListData.setListCoverageStructure(listCoverageRangeValueByRefDate(productId, Constantes.ID_PLAN_RISK_CONTENT, Constantes.COVERAGE_STRUCTURE, referenceDate));
					break;
				case Coverage.RISKTYPE_CONTENT:
					quoteListData.setListCoverageContent(listCoverageRangeValueByRefDate(productId, Constantes.ID_PLAN_RISK_STRUCTURE, Constantes.COVERAGE_CONTENT, referenceDate));
					break;
				case Coverage.RISKTYPE_BOTH:
					quoteListData.setListCoverageContent(listCoverageRangeValueByRefDate(productId, Constantes.ID_PLAN_RISK_ALL, Constantes.COVERAGE_CONTENT, referenceDate));
					quoteListData.setListCoverageStructure(listCoverageRangeValueByRefDate(productId, Constantes.ID_PLAN_RISK_ALL, Constantes.COVERAGE_STRUCTURE, referenceDate));				
					break;
				default:
					quoteListData.setListCoverageContent(listCoverageRangeValueByRefDate(productId, Constantes.ID_PLAN_RISK_ALL, Constantes.COVERAGE_CONTENT, referenceDate));
					quoteListData.setListCoverageStructure(listCoverageRangeValueByRefDate(productId, Constantes.ID_PLAN_RISK_ALL, Constantes.COVERAGE_STRUCTURE, referenceDate));
					break;
				}
			}
		}

		return quoteListData;
	}
	
	public ReportListData loadReportList(int partnerId, int issuingStatus, int installmentStatus, int annulmentType, String externalCode, boolean active) throws ServiceException { 
		ReportListData reportListData = new ReportListData();

		reportListData.setListIssuingStatus(findDataOptionByDataGroup(issuingStatus, active));	
		reportListData.setListInstallmentStatus(findDataOptionByDataGroup(installmentStatus, active));
		reportListData.setListAnnulmentType(findDataOptionByDataGroup(annulmentType, active, externalCode));
		reportListData.setListChannel(findChannelByPartnerId(partnerId, active));
		reportListData.setListProductsPerPartner(findProductsPerPartner(partnerId));
		reportListData.setListUserPerPartner(listUserPerPartner(partnerId));
		reportListData.setListPartnerPerProductOrBroker(listPartnerPerProductOrBroker(0, 0, 0));
		reportListData.setListProductPerPartnerOrBroker(listProductPerPartnerOrBroker(0, 0, 0));
		reportListData.setListBrokerPerProductOrPartner(listBrokerPerProductOrPartner(0, 0, 0));
		
		return reportListData;
	}
	
	public ReportListData loadReportListAnnulment(Integer partnerId, Integer productId, Integer brokerId){
		ReportListData reportListData = new ReportListData();
		
		reportListData.setListPartnerPerProductOrBroker(listPartnerPerProductOrBroker(partnerId, productId, brokerId));
		reportListData.setListProductPerPartnerOrBroker(listProductPerPartnerOrBroker(partnerId, productId, brokerId));
		reportListData.setListBrokerPerProductOrPartner(listBrokerPerProductOrPartner(partnerId, productId, brokerId));
		
		return reportListData;
	}
	
	public List<Partner> listPartnerPerProductOrBroker(Integer partnerId, Integer productId, Integer brokerId){
		return servicePartner.listPartnerPerProductOrBroker(partnerId, productId, brokerId);
	}
	
	public List<Broker> listBrokerPerProductOrPartner(Integer partnerId, Integer productId, Integer brokerId){
		return serviceBroker.listBrokerPerProductOrPartner(partnerId, productId, brokerId);
	}
	
	public List<Product> listProductPerPartnerOrBroker(Integer partnerId, Integer productId, Integer brokerId){
		return serviceProduct.listProductPerPartnerOrBroker(partnerId, productId, brokerId);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<Module> listAllowedModulesByUserLogged() throws ServiceException { 
		return serviceModule.listAllowedModulesByUserLogged();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<PersonalRiskOption> findPersonalRiskOptions(int productId, int riskPlanId, Date referenceDate) throws ServiceException { 

		List<PersonalRiskOption> result = servicePersonalRiskOption.findPersonalRiskOptions(productId, riskPlanId, referenceDate);
		return result;
	}
	
	public List<PartnerAndChannel> listAllPartnerChannel(User user) throws ServiceException { 
		return servicePartner.listAllPartnerChannel(user);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<BillingMethod> listPaymentType(int productId,  Integer paymentTermType, Date referenceDate){
		 List<BillingMethod> listPaymentType = servicePolicy.listPaymentType(productId, paymentTermType, referenceDate);
		 return listPaymentType;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<User> listUserByPartnerId(int partnerId) throws ServiceException { 
		return serviceUser.listUserByPartnerId(partnerId);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Set<User> fillListUserByChannel(Channel channel) throws ServiceException { 
		return serviceUser.fillListUserByChannel(channel);
	}
	
	public IdentifiedList listCountry(IdentifiedList identifiedList){
		return serviceCountry.listCountry(identifiedList);
	}

	public IdentifiedList listIdentifiedListCity(IdentifiedList identifiedList){
		return serviceCity.listIdentifiedListCity(identifiedList);
	}
	
	public IdentifiedList listStateByCountry(IdentifiedList identifiedList, int countryId){
		return serviceState.listStateByCountry(identifiedList, countryId);
	}

	public IdentifiedList listCityByState(IdentifiedList identifiedList, int stateId){
		return serviceCity.listCityByState(identifiedList, stateId);
	}
	
	public List<State> listStateByCountryId(int countryId){
		return serviceState.listStateByCountryId(countryId);
	}
	
	public List<City> listCityByStateId(int stateId){
		return serviceCity.listCityByStateId(stateId);
	}	
	
	public List<SearchDocumentResult> listSearchDocument(SearchDocumentParameters searchDocumentParameters){
		return serviceSearchDocument.listSearchDocument(searchDocumentParameters);
	}

	public Paginacao listSearchDocumentPaging(SearchDocumentParameters searchDocumentParameters){
		return serviceSearchDocument.listSearchDocumentPaging(searchDocumentParameters);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<PaymentOption> getPaymentOptionsByBillingMethodId(Endorsement endorsement, int billingMethodIdId, Date dtEffective, Date dtExpiry) throws ServiceException { 
		return serviceCalc.getPaymentOptionsByBillingMethodId(endorsement, billingMethodIdId, dtEffective, dtExpiry);
	}
	
	public PaymentOption getPaymentOptionByPaymentTermId(Double totalPremium, double taxRate, int productId, int billingMethodId, int paymentTermId, Date refDate, Date dtEffective, Date dtExpiry) throws ServiceException { 
		return serviceCalc.getPaymentOptionByPaymentTermId(totalPremium, taxRate, productId, billingMethodId, paymentTermId, refDate, dtEffective, dtExpiry);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<PaymentOption> getPaymentOptionsDefault(Double totalPremium, Double taxRate, int productId, Date refDate, Date dtEffective, Date dtExpiry) throws ServiceException { 
		return serviceCalc.getPaymentOptionsDefault(totalPremium, taxRate, productId, refDate, dtEffective, dtExpiry);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<PaymentOption> getPaymentOptionsByPaymentType(Double totalPremium, double taxRate, int productId, int paymentType, Date refDate, Date effectiveDate, Date expireDate) throws ServiceException {
		return serviceCalc.getPaymentOptionsByPaymentType(totalPremium, taxRate, productId, paymentType, refDate, effectiveDate, expireDate);
	}

	@Override
	public List<PaymentOption> getPaymentOptionsByPaymentType(double totalPremium, double netPremium, double policyCost, double fractioningAdditional, double taxValue, double taxRate,	int productId, int paymentType, Date refDate, Date effectiveDate, Date expireDate, Short dueDay, Integer renewalType, Integer renewalInsurerId, Integer issuanceType, Integer paymentTermType, Integer invoiceType, Integer firstPaymentType, Integer qtInstallment, Date proposalDate) throws ServiceException {
		return serviceCalc.getPaymentOptionsByPaymentType(totalPremium, netPremium, policyCost, fractioningAdditional, taxValue, taxRate, productId, paymentType, refDate, effectiveDate, expireDate, dueDay, renewalType, renewalInsurerId, issuanceType, paymentTermType, invoiceType, firstPaymentType, qtInstallment, proposalDate);
	}
	
	/**
	 * {@inheritDoc}
	 */	
	public Product findProductToBeCopy(int productId, Date referenceDate){
		return serviceProduct.findProductToBeCopy(productId, referenceDate);
	}
	
	public void logout(){
		serviceLogin.logout();
	}

	
	public Endorsement effectiveProposal(Endorsement endorsement, PaymentOption paymentOption) throws ServiceException { 
		endorsement =  serviceCalc.effectiveProposal(endorsement, paymentOption);
		return endorsement;
	}
	
	public Endorsement effectiveEndorsement(Endorsement endorsement, PaymentOption paymentOption, int issuanceType, Date newEffectiveDate) throws ServiceException { 
		endorsement =  serviceCalc.effectiveEndorsement(endorsement, paymentOption, issuanceType, newEffectiveDate);
		return endorsement;
	}

	public List<ProductVersion> getProductVersionByProduct(int productId) throws ServiceException { 
		return serviceProductVersion.getProductVersionByProduct(productId);
	}
	
	@Override
	public ProductVersion saveProductVersionCopy(ProductVersion inputProductVersion) throws ServiceException {
		return serviceProductVersion.saveProductVersionCopy( inputProductVersion );
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceEinsurance#findByUserAndPartner(br.com.tratomais.core.model.User, br.com.tratomais.core.model.customer.Partner)
	 */
	public List<Channel> findByUserAndPartner(User user, int partnerId) {
		return serviceChannel.findByUserAndPartner(user, partnerId);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceEinsurance#findProductsPerPartner(int)
	 */
	public List<Product> findProductsPerPartner(int partnerId) {
		return serviceProduct.findProductsPerPartner(partnerId);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceEinsurance#findByLoggedUserAndPartner(int)
	 */
	public List<Channel> findByLoggedUserAndPartner(int partnerId){
		return serviceChannel.findByUserAndPartner(AuthenticationHelper.getLoggedUser(), partnerId);
	}
	
	public List<User> listUserPerPartner(int partnerId){
		return serviceUser.listUserPerPartner(partnerId);
	}
	
	public DataOption getDataOptionById(int dataOptionId){
		return serviceDataOption.findById(dataOptionId);
	}
	
	public ProductPlan getPlanById(int productId, int planId, Date refDate){
		return serviceProduct.getPlanById(productId, planId, refDate);
	}
	
	public ProductPlan getRiskPlanById(int productId, int planId, Date refDate){
		return serviceProduct.getRiskPlanById(productId, planId, refDate);
	}

	public List<CoverageOption> listCoverageHabitatByPlanId(Endorsement end, int productId, int coveragePlanId, int riskPlanId, Date refDate) throws ServiceException {
		if ( end == null )
			return serviceProduct.listCoverageOptionByRefDate(productId, coveragePlanId, refDate);
		return serviceProduct.listCoverageHabitatByEndorsement(end, null);
	}
	
	public List <CoverageRangeValue> listCoverageRangeValueByRefDate(int productId, int coveragePlanId, int coverageId ,Date refDate){
		return serviceProduct.listCoverageRangeValueByRefDate(productId, coveragePlanId, coverageId, refDate);
	}	

	/**
	 * Gets a list of PlanRiskTypeDescription
	 * @param productId Product Identificator
	 * @param planId  Plan Identificator
	 * @param refDate Reference Date
	 * @return PlanRiskType 
	 */	
	public List <PlanRiskType> listPlanRiskTypeDescriptionByRefDate(int productId, int planId, Date refDate){
		return serviceProduct.listPlanRiskTypeDescriptionByRefDate(productId, planId, refDate);
	}	
	
	/**
	 * Gets a list of planKingShip
	 * @param productId Product Identificator
	 * @param planId  Plan Identificator
	 * @param personalRiskType personal Risk Type
	 * @param refDate Reference Date
	 * @return PlanKinShip 
	 */	
	public PlanKinship listPlanKinshipByPersonalRiskType(int productId, int riskPlanId, int personalRiskType, int renewalType, Date refDate){
		return serviceProduct.listPlanKinshipByPersonalRiskType(productId, riskPlanId, personalRiskType, renewalType, refDate);
	}	
	
	/**
	 * Gets a list of planKingShip
	 * @param productId Product Identificator
	 * @param planId  Plan Identificator
	 * @param renewalType Renewal Type
	 * @param refDate Reference Date
	 * @return PlanKinShip 
	 */
	public List<PlanKinship> listPlanKinshipByRiskPlan(int productId, int riskPlanId, int renewalType, Date refDate){
		return serviceProduct.listPlanKinshipByRiskPlan(productId, riskPlanId, renewalType, refDate);
	}

	public SimpleCalcResult lifeCycleCalc (Endorsement endorsement, List<CoverageOption> coverageOptionList) {
		return this.serviceCalc.lifeCycleCalc(endorsement, coverageOptionList);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceEinsurance#changeUserPassword(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean changeUserPassword( String login, String domain, String oldPassword, String newPassword) {
		return serviceLogin.changeUserPassword(login, domain, oldPassword, newPassword);
	}
	
	public EndorsementOption loadUnlockableEndorsement (int endorsementId, int contractId){
		return servicePolicy.loadUnlockableEndorsement(endorsementId, contractId);
	}
	
	public Boolean unlockEndorsement(int endorsementId, int contractId){
		return servicePolicy.unlockEndorsement(endorsementId, contractId);
	}
	
	public Endorsement policyChangeRegister(Endorsement endorsement, Date referenceDate) throws ServiceException {
		Endorsement retorno;
		try {
			retorno = servicePolicy.policyChangeRegister(endorsement, referenceDate);
		}
		catch(Throwable e) {
			retorno = endorsement;
			retorno.getErrorList().add(999, e.getMessage(), null, 0, null, 0);
		}	
		return retorno;
	}
	
	public Endorsement policyChangeTechnical(Endorsement endorsement, Date referenceDate, boolean isRecalculation, boolean isProposal, PaymentOption paymentOption) throws ServiceException {
		Endorsement retorno;
		try {
			retorno = servicePolicy.policyChangeTechnical(endorsement, referenceDate, isRecalculation, isProposal, paymentOption);
		}
		catch(Throwable e) {
			retorno = endorsement;
			retorno.getErrorList().add(999, e.getMessage(), null, 0, null, 0);
		}	
		return retorno;
	}
	
	public IdentifiedList findIdentifiedListDataOptionByDataGroup(int dataGroupId, String identifierString, Date refDate){
		return serviceDataOption.findIdentifiedListDataOptionByDataGroup(dataGroupId, identifierString, refDate);
	}
	
	public IdentifiedList findIdentifiedListDataOptionByDataGroupParentId(int dataGroupId, int parentId, String identifierString, Date refDate){
		return serviceDataOption.findIdentifiedListDataOptionByDataGroupParentId(dataGroupId, parentId, identifierString, refDate);
	}
	
	@Override
	public ProductVersion getProductVersionByRefDate(int productId, Date refDate) {
		return serviceProductVersion.getProductVersionByRefDate(productId, refDate);
	}
	
	@Override
	public Product getProductById(int productId) {
		return serviceProduct.getProductById(productId);
	}
	
	@Override
	public Boolean cancellationProposal(int endorsementId, int contractId, int reason) {
		return servicePolicy.cancellationProposal(endorsementId, contractId, reason);
	}
	
	public List<ProductPlan> listCoveragePlanByProductId(int productId, int riskPlanId, Date refDate) {
		return serviceProduct.listCoveragePlanByProductId( productId, riskPlanId, refDate );
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean subsidiaryExternalCodeExists(int insurerId, int subsidiaryId, String externalCode) {
		return serviceSubsidiary.subsidiaryExternalCodeExists(insurerId, subsidiaryId, externalCode);
	}

	/**
	 * {@inheritDoc}
	 */
	public int numActivePoliciesPerProductDocument(int productId, String docNumber, int docType, Date dateIni, Date dateFin, int contractId){
		return serviceCalc.numActivePoliciesPerProductDocument(productId, docNumber, docType, dateIni, dateFin, contractId);
	}
	public void logAuditAction(String message) {
		serviceLogin.logAuditAction(message);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<String> validateContract(int productId, String docNumber, int docType, Date dateIni, Date referenceDate, int contractId){
		return serviceCalc.validateContract(productId, docNumber, docType, dateIni, referenceDate, contractId);
	}

	@Override
	public Broker findBrokerById(Integer brokerId) throws ServiceException {
		return serviceBroker.findById(brokerId);
	}

	@Override
	public Partner findPartnerById(Integer partnerId) throws ServiceException {
		return servicePartner.findById(partnerId);
	}

	@Override
	public Module findModuleById(int id) throws ServiceException {
		return this.serviceModule.findById(id);
	}
	
	@Override
	public ReturnValue policyReactivation(int contractId) throws ServiceException{
		try {
			servicePolicy.policyReactivation(contractId);
			return new ReturnValue();
		}
		catch(Throwable e){
			return new ReturnValue(false, e.getMessage());
		}
	}

	/** {@inheritDoc} */
	@Override
	public ReturnClaimStateVerification claimStateVerification(int contractId, Date checkDate, int issuanceType) throws ServiceException {
		return servicePolicy.claimStateVerification(contractId, checkDate, issuanceType);
	}
	
	@Override
	// 666
	public ReturnValue policyCancellation(int contractId, int issuanceType, Date cancelDate, int cancelReason) throws ServiceException{
		try {
			servicePolicy.policyCancellation(contractId, issuanceType, cancelDate, cancelReason);
			return new ReturnValue();
		}
		catch(Throwable e){
			return new ReturnValue(false, e.getMessage());
		}
	}

	@Override
	public Paginacao listCollectionDetailsMainPaging(CollectionDetailsMainParameters collectionDetailsMainParameters)	throws ServiceException {
		return serviceCollectionMain.listCollectionDetailsMainPaging(collectionDetailsMainParameters);
	}

	@Override
	public List<CollectionDetailsMainResult> listCollectionDetailsMain(CollectionDetailsMainParameters collectionDetailsMainParameters)	throws ServiceException {
		return serviceCollectionMain.listCollectionDetailsMain(collectionDetailsMainParameters);
	}

	@Override
	public List<BillingMethodResult> listBillingMethod() throws ServiceException {
		return serviceCollectionMain.listBillingMethod();
	}

	@Override
	public List<CollectionMainResult> listCollectionMain(CollectionMainParameters collectionMainParameters) throws ServiceException {
		return serviceCollectionMain.listCollectionMain(collectionMainParameters);
	}

	@Override
	public Paginacao listCollectionMainPaging(CollectionMainParameters collectionMainParameters) throws ServiceException {
		return serviceCollectionMain.listCollectionMainPaging(collectionMainParameters);
	}

	@Override
	public List<StatusLote> listStatusLote() throws ServiceException {
		return serviceCollectionMain.listStatusLote();
	}
	
	@Override
	public ErrorList listRenewalAlert(int contractId, int endorsementId, boolean resolved) throws ServiceException { 
		return servicePolicy.listRenewalAlert(contractId, endorsementId, resolved);
	}
	
	@Override	
	public String getApplicationPropertyValue(int applicationId, String propertyName) throws ServiceException {
		return servicePolicy.getApplicationPropertyValue(applicationId, propertyName);
	}
	
	@Override
	public IdentifiedList getApplicationPropertyValue(IdentifiedList identifiedList, int applicationId, String propertyName) throws ServiceException {
		return servicePolicy.getApplicationPropertyValue(identifiedList, applicationId, propertyName);
	}
	
	@Override
	public List<PaymentOption> getPaymentOptionsDefault(Endorsement endorsement) throws ServiceException {
		return serviceCalc.getPaymentOptionsDefault(endorsement);
	}

	@Override
	public List<CoverageOption> listCoverageOptionByRefDate(int productId, int coveragePlanId, Date refDate) throws ServiceException {
		return serviceProduct.listCoverageOptionByRefDate(productId, coveragePlanId, refDate);
	}

	@Override
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId, int endorsementId, int itemId) throws ServiceException{
		return serviceItemPersonalRisk.listItemPersonalRisk(contractId, endorsementId, itemId);
	}

	@Override
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId, int endorsementId) throws ServiceException{
		return serviceItemPersonalRisk.listItemPersonalRisk(contractId, endorsementId);
	}

	@Override
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId) throws ServiceException{
		return serviceItemPersonalRisk.listItemPersonalRisk(contractId);
	}
	
	@Override
	public Double getAutoCost(int autoModelId, Integer year, Date referenceDate) throws ServiceException {
		return serviceAuto.getAutoCost(autoModelId, year, referenceDate);
	}

	@Override
	public List<AutoBrand> listAutoBrands() throws ServiceException {
		return serviceAuto.listAutoBrands();
	}

	@Override
	public List<AutoModel> listAutoModelForBrand(int autoBrandId) throws ServiceException {
		return serviceAuto.listAutoModelForBrand(autoBrandId);
	}

	@Override
	public List<Integer> listAutoYear(int autoModelId, Date referenceDate) throws ServiceException {
		return serviceAuto.listAutoYear(autoModelId, referenceDate);
	}
	
	@Override
	public List<ResponseRelationship> getResponseRelationship(Integer productId,Integer questionnaireId, Integer questionId, Integer responseId, Date referenceDate) {
		return serviceResponseRelationship.getResponseRelationship(productId, questionnaireId, questionId, responseId, referenceDate); 
	}

	@Override
	public AutoVersion findAutoVersion(int autoModelId, Date referenceDate) throws ServiceException {
		return serviceAuto.findAutoVersion(autoModelId, referenceDate);
	}
	
	@Override
	public ProductOption getProduct(int productId, Date referenceDate) throws ServiceException {
		return serviceProduct.getProduct(productId, referenceDate);
	}

	@Override
	public ProductVersion getProductVersionByBetweenRefDate(int productId, Date RefDate) throws ServiceException {
		return serviceProductVersion.getProductVersionByBetweenRefDate(productId, RefDate);
	}

	@Override
	public List<Country> listCountryId(int countryId) throws ServiceException {
		return serviceCountry.listCountryId(countryId);
	}

	@Override
	public List<Deductible> listAllDeductible() throws ServiceException {
		return serviceDeductible.findAllDeductible();
	}

	@Override
	public List<BillingMethodResult> listExchangeCollection() {
		return serviceCollectionMain.listExchangeCollection();
	}

	@Override
	public Lot getLot(Integer lotId) throws ServiceException {
		return serviceInterface.getLot(lotId);
	}

	@Override
	public Lot getLot(Integer lotId, Integer parentId, Integer exchangeId, Integer insurerId, Integer partnerId) throws ServiceException {
		return serviceInterface.getLot(lotId, parentId, exchangeId, insurerId, partnerId);
	}

	@Override
	public DataViewInterface getDataViewInterface() throws ServiceException {
		return serviceInterface.getDataViewInterface();
	}

	@Override
	public IdentifiedList listLot(IdentifiedList identifiedList, Integer exchangeId, Integer lotStatus, Date rangeBefore, Date rangeAfter) throws ServiceException {
		return serviceInterface.listLot(identifiedList, exchangeId, lotStatus, rangeBefore, rangeAfter);
	}

	@Override
	public FileLot getFileLot(Integer fileLotId, Integer lotId) throws ServiceException {
		return serviceInterface.getFileLot(fileLotId, lotId);
	}

	@Override
	public FileLotDetails getFileLotDetails(Integer fileLotDetailsId, Integer fileLotId) throws ServiceException {
		return serviceInterface.getFileLotDetails(fileLotDetailsId, fileLotId);
	}

	@Override
	public IdentifiedList listFileLot(IdentifiedList identifiedList, Integer lotId) throws ServiceException {
		return serviceInterface.listFileLot(identifiedList, lotId);
	}

	@Override
	public IdentifiedList listFileLotDetails(IdentifiedList identifiedList,	Integer fileLotId) throws ServiceException {
		return serviceInterface.listFileLotDetails(identifiedList, fileLotId);
	}

	@Override
	public List<InsuredObject> listObject(Date refDate) throws ServiceException {
		return serviceObject.listObject(refDate);
	}

	@Override
	public InsuredObject getObject(int objectID, Date refDate) throws ServiceException {
		return serviceObject.getObject(objectID, refDate);
	}

	@Override
	public List<Product> listProduct(int objectID, Date refDate) throws ServiceException {
		return serviceProduct.listProduct(objectID,refDate);
	}

	@Override
	public List<ProductVersion> listProductVersion(int productID, Date refDate) throws ServiceException {
		return serviceProductVersion.listProductVersion(productID,refDate);
	}

	@Override
	public List<Branch> listBranch(Date refDate) throws ServiceException {
		return serviceBranch.listBranch(refDate);
	}

	@Override
	public Branch getBranch(int branchID, Date refDate) throws ServiceException {
		return serviceBranch.getBranch(branchID, refDate);
	}

	@Override
	public List<Currency> listCurrency(boolean indexer, Date refDate) {
		return serviceCurrency.listCurrency(indexer, refDate);
	}

	@Override
	public Currency getCurrency(int currencyID, Date refDate) throws ServiceException {
		return serviceCurrency.getCurrency(currencyID, refDate);
	}

	@Override
	public ConversionRate findConversionRateByRefDate(int currencyID, Date refDate) throws ServiceException {
		return serviceConversionRate.findConversionRateByRefDate(currencyID, refDate);
	}

	@Override
	public List<ProductPlan> listProductPlan(int productID, Date versionDate) throws ServiceException {
		return serviceProduct.listProductPlan(productID, versionDate);
	}

	@Override
	public List<CoveragePlan> listCoveragePlan(int productID, Date versionDate) throws ServiceException {
		return serviceCoveragePlan.listCoveragePlan(productID, versionDate);
	}

	@Override
	public ProductPlan getProductPlanById(int productID, int planID, Date versionDate) throws ServiceException {
		return serviceProduct.getProductPlanById(productID, planID, versionDate);
	}

	@Override
	public CoveragePlan getCoveragePlanByRefDate(int productID, int planID,	int coverageID, Date versionDate) throws ServiceException {
		return serviceCoveragePlan.getCoveragePlanByRefDate(productID, planID, coverageID, versionDate);
	}

	@Override
	public CoverageRangeValue getCoverageRangeValue(int productID, int planID,int coverageID, int rangeValueID, Date versionDate) throws ServiceException {
		return serviceProduct.getCoverageRangeValue(productID, planID, coverageID, rangeValueID, versionDate);
	}

	@Override
	public List<ProductTerm> listProductTermByRefDate(int productID, Date versionDate) throws ServiceException {
		return serviceProductVersion.listProductTermByRefDate(productID, versionDate);
	}

	@Override
	public ProductTerm getProductTermById(int productID, int termID, Date versionDate) throws ServiceException {
		return serviceProduct.getProductTermById(productID, termID, versionDate);
	}

	@Override
	public List<ProductPaymentTerm> listProductPaymentTermByRefDate(int productID, Date versionDate) throws ServiceException {
		return serviceProductVersion.listProductPaymentTermByRefDate(productID, versionDate);
	}

	@Override
	public ProductPaymentTerm getProductPaymentTerm(int productID, int billingMethodID, Date versionDate) throws ServiceException {
		return serviceProductVersion.getProductPaymentTerm(productID, billingMethodID, versionDate);
	}

	@Override
	public ProductPaymentTerm getProductPaymentTerm(int productID, int paymentTermID, int billingMethodID, Date versionDate) throws ServiceException {
		return serviceProductVersion.getProductPaymentTerm(productID, paymentTermID, billingMethodID, versionDate);
	}
	
	@Override
	public BillingAgency getBillingAgencyById(int billingAgencyID, Date refDate) throws ServiceException {
		return serviceBillingMethod.getBillingAgencyById(billingAgencyID, refDate);
	}

	@Override
	public List<DataOption> listDataOptionByRefDate(int dataGroupID, Date refDate) throws ServiceException {
		return serviceDataOption.listDataOptionByRefDate(dataGroupID, refDate);
	}

	@Override
	public List<DataOption> listDataOptionByFieldValue(String fieldValue, Date refDate) throws ServiceException {
		return serviceDataOption.listDataOptionByFieldValue(fieldValue, refDate);
	}

	@Override
	public List<Profession> listProfessionByRefDate(Date refDate) throws ServiceException {
		return servicePolicy.listProfessionByRefDate(refDate);
	}

	@Override
	public List<Occupation> listOccupationByRefDate(Date refDate) throws ServiceException {
		return servicePolicy.listOccupationByRefDate(refDate);
	}

	@Override
	public List<Activity> listActivityByRefDate(Date refDate) throws ServiceException {
		return servicePolicy.listActivityByRefDate(refDate);
	}

	@Override
	public List<Inflow> listInflowByRefDate(Date refDate) throws ServiceException {
		return servicePolicy.listInflowByRefDate(refDate);
	}

	@Override
	public List<Country> listCountryByRefDate(Date refDate)	throws ServiceException {
		return serviceCountry.listCountryByRefDate(refDate);
	}

	@Override
	public List<State> listStateByRefDate(int countryID, Date refDate) throws ServiceException {
		return serviceState.listStateByRefDate(countryID, refDate);
	}

	@Override
	public List<City> listCityByRefDate(int countryID, int stateID, Date refDate) throws ServiceException {
		return serviceCity.listCityByRefDate(countryID, stateID, refDate);
	}

	@Override
	public List<DataOption> listDataOptionByParentID(int parentID, Date refDate) throws ServiceException {
		return serviceDataOption.listDataOptionByParentID(parentID, refDate);
	}

	@Override
	public InsuredObject getObjectByID(int objectID) throws ServiceException {
		return serviceObject.getObjectByID(objectID);
	}

	@Override
	public List<DataOption> listDataOptionByFieldValue(String fieldValue, int parentID, Date refDate) throws ServiceException {
		return serviceDataOption.listDataOptionByParentID(fieldValue, parentID, refDate);
	}

	@Override
	public List<Insurer> listInsurerByRefDate(Date refDate)	throws ServiceException {
		return serviceInsurer.listInsurerByRefDate(refDate);
	}

	@Override
	public List<Subsidiary> listSubsidiary(int insurerID, Date refDate) throws ServiceException {
		return serviceSubsidiary.listSubsidiary(insurerID, refDate);
	}

	@Override
	public List<Broker> listBroker(int insurerID, String regulatoryCode, Date refDate) throws ServiceException {
		return serviceBroker.listBroker(insurerID, regulatoryCode, refDate);
	}

	@Override
	public List<Channel> listChannel(int insurerID, int partnerID, Date refDate) throws ServiceException {
		return serviceChannel.listChannel(insurerID, partnerID, refDate);
	}

	@Override
	public List<MasterPolicy> listMasterPolicy(int insurerID, int partnerID, Date refDate) throws ServiceException {
		return serviceMasterPolicy.listMasterPolicy(insurerID, partnerID, refDate);
	}

	@Override
	public List<Partner> listPartner(int insurerID, Date refDate) throws ServiceException {
		return servicePartner.listPartner(insurerID, refDate);
	}

	@Override
	public List<User> listUser(int insurerID, int partnerID, int channelID, Date refDate) throws ServiceException {
		return serviceUser.listUser(insurerID, partnerID, channelID, refDate);
	}

	@Override
	public List<BillingMethod> listBillingMethod(int productID, Date versionDate) throws ServiceException {
		return serviceBillingMethod.listBillingMethod(productID, versionDate);
	}

	@Override
	public Endorsement policyCancellationByError(int contractId, Date cancelDate, int cancelReason) throws ServiceException {
		return servicePolicy.policyCancellationByError(contractId, cancelDate, cancelReason);
	}

	@Override
	public Boolean unlockEndorsement(int endorsementId, int contractId,	Date unlockDate) throws ServiceException {
		return servicePolicy.unlockEndorsement(endorsementId, contractId, unlockDate);
	}

	@Override
	public Endorsement policyCancellationByInsured(int contractId, Date cancelDate, int cancelReason) throws ServiceException {
		return servicePolicy.policyCancellationByInsured(contractId, cancelDate, cancelReason);
	}

	@Override
	public List<Functionality> listAllowedFunctionalityByUserLogged() throws ServiceException {
		return serviceFunctionality.listAllowedFunctionalityByUserLogged();
	}
	
	@Override
	public List<Report> listReportIdByType(Integer contractId, Integer endorsementId) throws ServiceException {
		return serviceProductVersion.listReportIdByType(contractId, endorsementId);
	}
	
	@Override
	public Integer getReportIdByType(Integer contractId, Integer endorsementId, Integer reportType) throws ServiceException {
		return serviceProductVersion.getReportIdByType(contractId, endorsementId, reportType);
	}

	@Override
	public void createAndFreePendency(int contractId, int endorsementId, Date date, int pendencyId, int lockPendencyStatus) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DAY_OF_MONTH, -1);
		servicePolicy.createAndFreePendency(contractId, endorsementId, c.getTime(), pendencyId, lockPendencyStatus);
	}
	
	@Override
	public IdentifiedList identifiedListFindCep(IdentifiedList identifiedList, String cep) {
		identifiedList.setValue(findCEP.search(cep));
		return identifiedList;
	}

	@Override
	public void validateBankInfo(IAccountInfo info)	throws BankValidationException {
		validateBankInfo.validateInfo(info);
	}
	
}