package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.dao.IDaoConversionRate;
import br.com.tratomais.core.model.product.ConversionRate;
import br.com.tratomais.core.service.IServiceConversionRate;

public class ServiceConversionRateImpl extends ServiceBean implements IServiceConversionRate{ 

	private IDaoConversionRate daoConversionRate;

	public void setDaoConversionRate(IDaoConversionRate daoConversionRate){
		this.daoConversionRate = daoConversionRate;
	}
	
	@Override
	public ConversionRate findConversionRateByRefDate(int currencyID, Date refDate) {
		return daoConversionRate.findConversionRateByRefDate(currencyID, refDate);
	}

	@Override
	public void delete(ConversionRate entity) {
		daoConversionRate.delete(entity);
	}

	@Override
	public ConversionRate findById(Integer id) {
		return daoConversionRate.findById(id);
	}

	@Override
	public List<ConversionRate> listAll() {
		return daoConversionRate.listAll();
	}

	@Override
	public List<ConversionRate> listAllActive() {
		return daoConversionRate.listAllActive();
	}

	@Override
	public ConversionRate save(ConversionRate entity) {
		return daoConversionRate.save(entity);
	}
}