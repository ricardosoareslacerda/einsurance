package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.tratomais.core.dao.IDaoState;
import br.com.tratomais.core.model.State;
import br.com.tratomais.core.model.product.IdentifiedList;
import br.com.tratomais.core.service.IServiceState;

public class ServiceStateImpl extends ServiceBean implements IServiceState{
	private static final Logger logger = Logger.getLogger( ServiceStateImpl.class );
	
	private IDaoState daoState;
	
	public void setDaoState(IDaoState daoState) {
		this.daoState = daoState;
	}

	public State findById(Integer id) {
		return daoState.findById( id );
	}

	public List< State > findByName(String name) {
		return daoState.findByName( name );
	}

	public List< State > listAll() {
		return daoState.listAll();
	}
	
	public List< State > findByCountryId(Integer id){
		return daoState.findByCountryId( id );
	}

	public IdentifiedList listStateByCountry(IdentifiedList identifiedList, int countryId){
		return daoState.listStateByCountry(identifiedList, countryId);
	}

	public List< State > listStateByCountryId(int countryId){
		return daoState.findByCountryId( countryId );
	}

	@Override
	public List<State> listStateByRefDate(int countryID, Date refDate) {
		return daoState.listStateByRefDate(countryID, refDate);
	}	
	
}
