package br.com.tratomais.core.service.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.jms.JMSException;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import br.com.tratomais.core.claim.CheckStateClaim;
import br.com.tratomais.core.claim.ReturnClaimStateVerification;
import br.com.tratomais.core.dao.IDaoBillingMethod;
import br.com.tratomais.core.dao.IDaoChannel;
import br.com.tratomais.core.dao.IDaoDataOption;
import br.com.tratomais.core.dao.IDaoInstallment;
import br.com.tratomais.core.dao.IDaoPartner;
import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.dao.IDaoProduct;
import br.com.tratomais.core.dao.IDaoPolicy.AdjustmentsToEndorsement;
import br.com.tratomais.core.dao.impl.DaoCalculo;
import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.info.Calculus;
import br.com.tratomais.core.infrastructure.AuthenticationHelper;
import br.com.tratomais.core.model.ApplicationProperty;
import br.com.tratomais.core.model.Inflow;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.claim.ClaimAction;
import br.com.tratomais.core.model.customer.Activity;
import br.com.tratomais.core.model.customer.Address;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Customer;
import br.com.tratomais.core.model.customer.Occupation;
import br.com.tratomais.core.model.customer.Partner;
import br.com.tratomais.core.model.customer.Phone;
import br.com.tratomais.core.model.customer.Profession;
import br.com.tratomais.core.model.pendencies.LockPendency;
import br.com.tratomais.core.model.pendencies.LockPendencyHistory;
import br.com.tratomais.core.model.pendencies.LockPendencyId;
import br.com.tratomais.core.model.policy.Beneficiary;
import br.com.tratomais.core.model.policy.CalcStep;
import br.com.tratomais.core.model.policy.Contract;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.EndorsementId;
import br.com.tratomais.core.model.policy.EndorsementOption;
import br.com.tratomais.core.model.policy.Installment;
import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.policy.ItemCoverage;
import br.com.tratomais.core.model.policy.ItemPersonalRisk;
import br.com.tratomais.core.model.policy.ItemPersonalRiskGroup;
import br.com.tratomais.core.model.policy.ItemProperty;
import br.com.tratomais.core.model.policy.ItemRiskType;
import br.com.tratomais.core.model.product.BillingMethod;
import br.com.tratomais.core.model.product.DataOption;
import br.com.tratomais.core.model.product.Factor;
import br.com.tratomais.core.model.product.IdentifiedList;
import br.com.tratomais.core.model.product.PaymentOption;
import br.com.tratomais.core.model.product.PlanRiskType;
import br.com.tratomais.core.model.product.Product;
import br.com.tratomais.core.model.product.Rate;
import br.com.tratomais.core.model.product.Roadmap;
import br.com.tratomais.core.model.renewal.RenewalAlert;
import br.com.tratomais.core.service.ICalc;
import br.com.tratomais.core.service.IServiceCollection;
import br.com.tratomais.core.service.IServicePolicy;
import br.com.tratomais.core.service.IServiceProduct;
import br.com.tratomais.core.service.erros.Erro;
import br.com.tratomais.core.service.erros.ErrorList;
import br.com.tratomais.core.service.erros.ErrorTypes;
import br.com.tratomais.core.soap.to.PolicyCertificateNumber;
import br.com.tratomais.core.soap.validation.ErrorValidationClaim;
import br.com.tratomais.core.soap.validation.ValidationInterfaces;
import br.com.tratomais.core.util.DateUtilities;
import br.com.tratomais.core.util.StringUtil;
import br.com.tratomais.core.util.datatrans.ECollectionJMSTransmitter;
import br.com.tratomais.core.util.jms.EInsuranceJMSConnection;
import br.com.tratomais.core.util.jms.EInsuranceJMSException;
import br.com.tratomais.esb.collections.model.request.CollectionDetail;
import br.com.tratomais.esb.collections.model.request.CollectionEnvelope;
import br.com.tratomais.esb.collections.model.request.DetailAttribute;
import br.com.tratomais.esb.collections.model.response.CollectionResponseItem;
import br.com.tratomais.esb.interfaces.model.request.ClaimRequest;
import br.com.tratomais.esb.interfaces.model.response.ClaimResponse;
import br.com.tratomais.general.utilities.NumberUtilities;

public class ServicePolicyImpl extends ServiceBean implements IServicePolicy, ApplicationContextAware {
	private Logger logger = Logger.getLogger(ServicePolicyImpl.class);
	
	private IDaoPolicy daoPolicy;
	private IDaoBillingMethod daoBillingMethod;
	private IDaoProduct daoProduct;
	private IDaoChannel daoChannel;
	private IDaoPartner daoPartner;
	private IServiceProduct serviceProduct;
	private IServiceCollection serviceCollection;
	private ICalc serviceBasicCalc;
	private DaoCalculo daoCalculo;
	private ValidationInterfaces validationInterfaces;
	private ErrorValidationClaim errorValidationClaim;
	private IDaoDataOption daoDataOption;
	private IDaoInstallment daoInstallment;
	
	public void setValidationInterfaces(ValidationInterfaces validationInterfaces) {
		this.validationInterfaces = validationInterfaces;
	}

	public void setErrorValidationClaim(ErrorValidationClaim errorValidationClaim) {
		this.errorValidationClaim = errorValidationClaim;
	}
	
	public DaoCalculo getDaoCalculo() {
		return daoCalculo;
	}

	public void setDaoCalculo(DaoCalculo daoCalculo) {
		this.daoCalculo = daoCalculo;
	}
	
	/**
	 * @return the serviceBasicCalc by dependecy injection 
	 */
	public ICalc getServiceBasicCalc() {
		return serviceBasicCalc;
	}
	
	/**
	 * @param serviceBasicCalc
	 */
	public void setServiceBasicCalc(ICalc serviceBasicCalc) {
		this.serviceBasicCalc = serviceBasicCalc;
	}
	
	public void setDaoPolicy(IDaoPolicy daoPolicy) {
		this.daoPolicy = daoPolicy;
	}

	public void setDaoProduct(IDaoProduct daoProduct) {
		this.daoProduct = daoProduct;
	}

	public void setDaoChannel(IDaoChannel daoChannel) {
		this.daoChannel = daoChannel;
	}

	public void setDaoPartner(IDaoPartner daoPartner) {
		this.daoPartner = daoPartner;
	}

	/**
	 * @param daoDataOption the daoDataOption to set
	 */
	public void setDaoDataOption(IDaoDataOption daoDataOption) {
		this.daoDataOption = daoDataOption;
	}	
	
	/**
	 * Ajusta o DAO de installment (p/ IOC)
	 * @param daoInstallment Dao de Installment 
	 */
	public void setDaoInstallment(IDaoInstallment daoInstallment) {
		this.daoInstallment = daoInstallment;
	}	
	
	public void setDaoBillingMethod(IDaoBillingMethod daoBillingMethod) {
		this.daoBillingMethod = daoBillingMethod;
	}
	
	public void setServiceProduct(IServiceProduct serviceProduct) {
		this.serviceProduct = serviceProduct;
	}
	
	public void setServiceCollection(IServiceCollection serviceCollection) {
		this.serviceCollection = serviceCollection;
	}
	
	public List<Activity> listAllActivity() {
		return daoPolicy.listAllActivity();
	}

	public List<Occupation> listAllOccupation() {
		return daoPolicy.listAllOccupation();
	}

	public List<Profession> listAllProfession() {
		return daoPolicy.listAllProfession();
	}

	public List<Inflow> listAllInflow(String inflowType) {
		return daoPolicy.listAllInflow(inflowType);
	}

	public Endorsement saveEndorsement(Endorsement endorsement) {
		Endorsement endorsementSaved = daoPolicy.saveEndorsement(endorsement);
		return endorsementSaved;
	}

	public Customer saveCustomer(Customer customer) {
		return daoPolicy.saveCustomer(customer);
	}

	/**
	 * @author leo.costa
	 * @param endorsementId
	 * @param contractId
	 * @return {@link EndorsementOption} filled
	 * @return <code>null</code> not found
	 **/
	public EndorsementOption loadUnlockableEndorsement(int endorsementId, int contractId) {
		Endorsement endorsement = daoPolicy.loadEndorsement(endorsementId, contractId);
		if (endorsement != null) {
			EndorsementOption endorsementOption = new EndorsementOption();
			endorsementOption.setProductOption(daoProduct.getProduct(endorsement.getContract().getProductId(), endorsement.getReferenceDate()));
			for (Item retItem : endorsement.getItems()) {
				if (retItem.getObjectId() == Item.OBJECT_CICLO_VITAL || retItem.getObjectId() == Item.OBJECT_HABITAT || retItem.getObjectId() == Item.OBJECT_CAPITAL) {
					retItem.setCoverageOptions(serviceProduct.listCoverageOptionByRefDate(endorsement.getContract().getProductId(), retItem.getCoveragePlanId(), endorsement.getReferenceDate(), retItem.getItemCoverages()));
				}
			}
			
			endorsementOption.setEndorsement(endorsement);
			endorsementOption.setPartnerName(daoPartner.findById(endorsement.getContract().getPartnerId()).getNickName());
			endorsementOption.setChannelName(daoChannel.findById(endorsement.getChannelId()).getNickName());
			endorsementOption.setPlanPersonalRisk(daoProduct.findPlanPersonalRisk(endorsement.getReferenceDate(), 
																				  63, 
																				  endorsement.getRiskPlanId(), 
																				  endorsement.getContract().getProductId()));
			return endorsementOption;
		}
		return null;
	}
	
	/**
	 * Method that proposal unlock
	 * 
	 * @param endorsementId 
	 * @param contractID 
	 * @return {@link Boolean}
	 * **/
	public Boolean unlockEndorsement(int endorsementId, int contractId) {
		// TODO falta gravar log com as informa��es do usu�rio que est� desbloqueando
		Endorsement endorsement = daoPolicy.loadEndorsement(endorsementId, contractId);

		try {
			if(endorsement != null) {

				serviceCollection.dispachCollection(endorsement, true);
	
				endorsement.setProposalDate(null);
				endorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_COTACAO);
				if(endorsement.getInstallments() != null) {
					for (Installment installment : endorsement.getInstallments()) {
						installment.setInstallmentStatus(Installment.INSTALLMENT_STATUS_LOCKED);
					}
				}
				// TODO update com sql
				daoPolicy.saveEndorsement(endorsement);
				return true;
			}
		// Communication error	
		} catch (JMSException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EInsuranceJMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	

	@Override
	public Boolean unlockEndorsement(int endorsementId, int contractId,	Date unlockDate) {
		return unlockEndorsement(endorsementId, contractId);
	}

	/**
	 * 
	 * Method that cancellation the proposal
	 * 
	 * @param endorsementId will be canceled
	 * @param contractId will be canceled
	 * @param reason identifier of reason for cancellation
	 *
	 * TODO Alterar m�todo, pois a mensagem j� ter� sido enviada mesmo que ocorra um erro na grava��o dos dados, ocasionando disparidade entre os dados dos sistemas
	 * **/
	public Boolean cancellationProposal(int endorsementId, int contractId, int reason) {

		boolean returnValue = false;
		try {
			serviceCollection.dispachCollection(endorsementId, contractId, true);

			int cancellationContract = daoPolicy.cancellationContract(contractId, Contract.CONTRACT_STATUS_ABROGATED, new Date());
			int cancellationEndorsement = daoPolicy.cancellationEndorsement(endorsementId, contractId, reason, Endorsement.ISSUING_STATUS_ANULADA, new Date());
			int cancellationItem = daoPolicy.cancellationItem(endorsementId, contractId, Item.ITEM_STATUS_INACTIVE, new Date());
			
			daoPolicy.cancellationInstallment(endorsementId, contractId, Installment.INSTALLMENT_STATUS_ANNULMENT, new Date());

			returnValue = (cancellationContract > 0 && cancellationEndorsement > 0 && cancellationItem > 0);

		} catch (JMSException e) {
			e.printStackTrace();
			returnValue = false;
		} catch (NamingException e) {
			e.printStackTrace();
			returnValue = false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			returnValue = false;
		} catch (EInsuranceJMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnValue;
	}

	/**
	 * @author leo.costa
	 * @param productId
	 * @param referenceDate
	 * 
	 * @return {@link ArrayList} of {@link BillingMethod}
	 * **/
	public List<BillingMethod> listPaymentType(int productId, Integer paymentTermType, Date referenceDate) {
		return daoBillingMethod.listPaymentType(productId, paymentTermType, referenceDate);
	}
	
	private Endorsement createEndosementByIssuanceType(Endorsement inputEndorsement, int issuanceType, AdjustmentsToEndorsement adjustments) {
		Endorsement endorsementToSave = inputEndorsement;
		
		// Check if is Change Technical and Not Proposal
		if ((issuanceType == Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL && !adjustments.isProposal()) ||
			(issuanceType != Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL)) {
			endorsementToSave = daoPolicy.adjustEndorsementToCalc(inputEndorsement, issuanceType, adjustments);
			try {
				serviceBasicCalc.simpleCalc(endorsementToSave, true);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		
		// Check if is Change Technical and is Proposal
		if ((issuanceType == Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL && adjustments.isProposal()) ||
			(issuanceType != Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL)) {
			this.adjustPaymentOption(endorsementToSave, adjustments);
			daoPolicy.adjustEndorsementToEffectivate(endorsementToSave, adjustments);
			try {
				serviceBasicCalc.effectiveProposal(endorsementToSave, adjustments.getPaymentOption());
				//WBB daoPolicy.save(endorsementToSave);
			} catch (ServiceException e) {
				e.printStackTrace();
			}
			
			// Emitting Endorsement
			if (endorsementToSave.getIssuingStatus() == Endorsement.ISSUING_STATUS_PROPOSTA) {
				endorsementToSave.setAuthorizationType(0);
				endorsementToSave.setIssuingStatus(Endorsement.ISSUING_STATUS_EMITIDA);
				
				Calendar c = Calendar.getInstance();
				c.setTime(new Date());
				c.add(Calendar.DAY_OF_MONTH, -1);
				endorsementToSave.setIssuanceDate(c.getTime());
				
				endorsementToSave.setEndorsementNumber(daoCalculo.getNextEndorsementNumber());
				endorsementToSave.setEndorsementDate(new Date());
				daoPolicy.save(endorsementToSave);
			}
		}
		
		return endorsementToSave;
	}

	public Endorsement lastValidEndosementForContractNumber(Integer numContrato) {
		Endorsement returnEndosement = daoPolicy.lastValidEndosementForContractNumber(numContrato);
		return returnEndosement;
	}
	
	public Endorsement lastValidEndorsementByEvict(Integer contractId) {
		Endorsement returnEndorsement = daoPolicy.lastValidEndorsementByEvict(contractId);
		return returnEndorsement;
	}

	public Endorsement lastValidEndosementForContractNumber(Integer numContrato, Date effectiveDate){
		Endorsement returnEndosement = daoPolicy.lastValidEndosementForContractNumber(numContrato, effectiveDate);
		return returnEndosement;
	}

	public void policyCancellationNoPayment(int contractId, final Date effectiveDate, final Date cancelDate) throws ServiceException {
		Endorsement endorsement = this.lastValidEndosementForContractNumber(contractId);

		if ((endorsement != null) &&
			(this.validatePolicyNoPayment(endorsement))) {

			//Cancela todos as parcelas de cobranza pendentes
			this.cancelAllInstallmentForCollection(endorsement, cancelDate, cancelDate);

			//Gera o endoso de anula��o por Falta de Pagamento
			endorsement = this.createEndosementByIssuanceType(endorsement, Endorsement.ISSUANCE_TYPE_NO_PAYMENT_CANCEL, new IDaoPolicy.AdjustmentsToEndorsement(effectiveDate, cancelDate));

			//Cancela propuesta o emission renovada
			Integer renewalId = endorsement.getContract().getRenewalId();
			if (renewalId != null) {
				if (daoPolicy.lastValidEndosementForContractNumber(renewalId) == null)
					this.cancellationProposal(1, renewalId, Endorsement.CANCEL_REASON_OTHERS);
				else
					this.policyCancellation(renewalId, Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR, new Date(), Endorsement.CANCEL_REASON_OTHERS);
			}
		}
		else{
			throw new ServiceException(ErrorTypes.VALIDATION_POLICY_CANCEL_NOT_ALLOWED.getMessage(null));
		}
	}

	public void policyReactivation(int contractId) throws ServiceException {
		Endorsement endorsement = this.lastValidEndosementForContractNumber(contractId);
		
		if ((endorsement != null) && (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_NO_PAYMENT_CANCEL)) {
			this.createEndosementByIssuanceType(endorsement, Endorsement.ISSUANCE_TYPE_POLICY_REACTIVATION, new IDaoPolicy.AdjustmentsToEndorsement(endorsement.getEffectiveDate(), null));
		}
		else{
			throw new ServiceException(ErrorTypes.VALIDATION_POLICY_REACTIVATION_NOT_ALLOWED.getMessage(null));
		}
	}

	public void policyCancellation(int contractId, final int issuanceType, final Date cancelDate, final int cancelReason) throws ServiceException {
		Endorsement endorsement = this.lastValidEndosementForContractNumber(contractId);
		if ((endorsement != null) && 
				(this.validatePolicyCancellation(endorsement, issuanceType, cancelDate))) {

			//Cancela todos as parcelas de cobranza pendentes
			this.cancelAllInstallmentForCollection(endorsement, cancelDate, new Date());

			//Gera o endoso de anula��o por Solicita��o ou por Erro de emiss�o
			endorsement = this.createEndosementByIssuanceType(endorsement, issuanceType, new IDaoPolicy.AdjustmentsToEndorsement(endorsement.getEffectiveDate(), cancelDate, cancelReason, Endorsement.ENDORSEMENT_TYPE_REFUND));

			//Cancela propuesta o emission renovada
			Integer renewalId = endorsement.getContract().getRenewalId();
			if (renewalId != null) {
				if (daoPolicy.lastValidEndosementForContractNumber(renewalId) == null)
					this.cancellationProposal(1, renewalId, Endorsement.CANCEL_REASON_OTHERS);
				else
					this.policyCancellation(renewalId, Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR, new Date(), Endorsement.CANCEL_REASON_OTHERS);
			}
		}
		else {
			throw new ServiceException(ErrorTypes.VALIDATION_POLICY_CANCEL_NOT_ALLOWED.getMessage(null));
		}
	}
	
	public Endorsement policyCancellationByError(int contractId, final Date cancelDate, final int cancelReason) throws ServiceException {
		return verifyCancellation(contractId, Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR, cancelDate, cancelReason);
	}

	public Endorsement policyCancellationByInsured(int contractId, final Date cancelDate, final int cancelReason) throws ServiceException {
		return verifyCancellation(contractId, Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_INSURED, cancelDate, cancelReason);
	}
	
	private Endorsement verifyCancellation(int contractId, final int issuanceType, final Date cancelDate, final int cancelReason) throws ServiceException{
		Endorsement endorsement = this.lastValidEndosementForContractNumber(contractId);
		if ((endorsement != null) && 
				(this.validatePolicyCancellation(endorsement, issuanceType, cancelDate))) {

				//Cancela todos as parcelas de cobranza pendentes
				this.cancelAllInstallmentForCollection(endorsement, cancelDate, new Date());

				//Gera o endoso de anula��o por Solicita��o ou por Erro de emiss�o
				endorsement = this.createEndosementByIssuanceType(endorsement, issuanceType, new IDaoPolicy.AdjustmentsToEndorsement(endorsement.getEffectiveDate(), cancelDate, cancelReason, Endorsement.ENDORSEMENT_TYPE_REFUND));

				//Cancela propuesta o emission renovada
				Integer renewalId = endorsement.getContract().getRenewalId();
				if (renewalId != null) {
					if (daoPolicy.lastValidEndosementForContractNumber(renewalId) == null)
						this.cancellationProposal(1, renewalId, Endorsement.CANCEL_REASON_OTHERS);
					else
						this.policyCancellation(renewalId, Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR, new Date(), Endorsement.CANCEL_REASON_OTHERS);
				}
		}
		else {
			throw new ServiceException(ErrorTypes.VALIDATION_POLICY_CANCEL_NOT_ALLOWED.getMessage(null));
		}
		
		return endorsement;
	}
	
	/**
	 * Valida se o endorsement pode ser cancelado por falta de pagamento
	 * @param endorsement
	 * @return
	 */
	private boolean validatePolicyNoPayment(Endorsement endorsement) {
		boolean validate = false;

		if ((endorsement.getContract().getContractStatus() == Contract.CONTRACT_STATUS_ACTIVE) || 
			(endorsement.getContract().getContractStatus() == Contract.CONTRACT_STATUS_FINALIZED)) {
			if ((endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_EMISSION) || 
			    (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_REACTIVATION) ||
			    (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_REGISTER) ||
			    (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL)) {
				
				validate = true;
			}
		}

		return validate;
	}

	private boolean validatePolicyCancellation(Endorsement endorsement, final int issuanceType, final Date cancelDate) throws ServiceException {
		boolean validate = false;

		if (endorsement.getContract().getContractStatus() == Contract.CONTRACT_STATUS_ACTIVE) {
			if ((issuanceType == Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR) ||
				(issuanceType == Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_INSURED)) {

				if ((issuanceType == Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR) && 
					(endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_EMISSION)) {

					validate = true;
				}
				else if ((issuanceType == Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_INSURED) && 
						 ((endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_EMISSION) || 
						  (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_REACTIVATION) ||
						  (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_REGISTER) ||
						  (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL))) {

					validate = true;
				}
			}
		}

		if (validate) {
			ReturnClaimStateVerification resultClaim = claimStateVerification(endorsement.getContract().getContractId(), cancelDate, issuanceType);
			if (! resultClaim.isResult() ) {
				validate = false;
				if (resultClaim.getErros() > 0) {
					throw new ServiceException(resultClaim.getErrorMessages().get(0).trim());
				}
				else {
					throw new ServiceException(ErrorTypes.VALIDATION_POLICY_CANCEL_CLAIM_ERROR.getMessage(null));
				}
			}
		}
		
		return validate;
	}

	private boolean validatePolicyChangeRegister(Endorsement endorsement, Endorsement inputEndorsement, Date changeDate) throws ServiceException {
		boolean validate = false;

		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		
		if (! df.format(endorsement.getIssuanceDate()).equals(df.format(new Date()))) {
			if (endorsement.getContract().getContractStatus() == Contract.CONTRACT_STATUS_ACTIVE) {
				if ((endorsement.getId().getContractId() == inputEndorsement.getId().getContractId()) && 
					(endorsement.getId().getEndorsementId() == inputEndorsement.getId().getEndorsementId())) {
					validate = true;
				}
			}
		}
		
		if (validate) {
			ReturnClaimStateVerification resultClaim = claimStateVerification(endorsement.getContract().getContractId(), changeDate, Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_REGISTER);
			if (! resultClaim.isResult() ) {
				validate = false;
				if (resultClaim.getErros() > 0) {
					throw new ServiceException(resultClaim.getErrorMessages().get(0).trim());
				}
				else {
					endorsement.getErrorList().add(ErrorTypes.VALIDATION_POLICY_CHANGE_CLAIM_ERROR, null, null, null, 0);
				}
			}			
		}

		return validate;
	}

	private boolean validatePolicyChangeTechnical(Endorsement endorsement, Endorsement inputEndorsement, Date changeDate) throws ServiceException {
		boolean validate = false;

		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		
		if (! df.format(endorsement.getIssuanceDate()).equals(df.format(new Date()))) {
			if (endorsement.getContract().getContractStatus() == Contract.CONTRACT_STATUS_ACTIVE) {
				if ((endorsement.getId().getContractId() == inputEndorsement.getId().getContractId()) && 
					(endorsement.getId().getEndorsementId() == inputEndorsement.getEndorsedId())) {
					validate = true;
				}
			}
		}
		
		if (validate) {
			ReturnClaimStateVerification resultClaim = claimStateVerification(endorsement.getContract().getContractId(), changeDate, Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL);
			if (! resultClaim.isResult() ) {
				validate = false;
				if (resultClaim.getErros() > 0) {
					throw new ServiceException(resultClaim.getErrorMessages().get(0).trim());
				}
				else {
					endorsement.getErrorList().add(ErrorTypes.VALIDATION_POLICY_CHANGE_CLAIM_ERROR, null, null, null, 0);
					//throw new ServiceException(ErrorTypes.VALIDATION_POLICY_CHANGE_CLAIM_ERROR.getMessage(null));
				}
			}			
		}

		return validate;
	}
	
	/**
	 * Cancela todas as parcelas pend�ntes de um endorsement
	 * 
	 * @param endorsement
	 * @param cancelDate
	 * @param transmiterDate
	 */
	private void cancelAllInstallmentForCollection(Endorsement endorsement, final Date cancelDate, final Date transmiterDate) {
		cancelAllInstallmentForCollection(endorsement, cancelDate, transmiterDate, true);
	}

	private void cancelAllInstallmentForCollection(Endorsement endorsement, final Date cancelDate, final Date transmiterDate, boolean inclusive) {
		Contract workContract = daoPolicy.listContract(endorsement.getContract().getContractId());
		if (workContract != null) {
			for (int endorsementId = 1; endorsementId <= endorsement.getId().getEndorsementId(); endorsementId++) {
				Endorsement workEndorsement = daoPolicy.loadEndorsement(endorsementId, endorsement.getContract().getContractId());
				if (workEndorsement != null) {
					if (workEndorsement.getEndorsementType() == Endorsement.ENDORSEMENT_TYPE_COLLECTION) {
						// Check if cancel actual Installment
						if ((workEndorsement.getId().getEndorsementId() != endorsement.getId().getEndorsementId()) ||
							(workEndorsement.getId().getEndorsementId() == endorsement.getId().getEndorsementId() && inclusive)) {
								this.cancelaParcelasPendentesAnteriores(workEndorsement, cancelDate, transmiterDate);
						}
					}
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AdjustmentsToEndorsement getPremiumPending(final Endorsement endorsement, final Date claimDate){
		Double installmentPending = 0.0;
		Date expiryDate = null;
		Date effectiveDate = null;

		boolean controle = true;
		Endorsement workEndorsement = endorsement;
		
		while (controle) {
			controle = false;
			if (workEndorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_REGISTER) {
				workEndorsement = daoPolicy.loadEndorsement(workEndorsement.getEndorsedId(), workEndorsement.getId().getContractId());
				controle = true;
			} else {
				List<Installment> installments = daoPolicy.getInstallmentForEndorsementId(workEndorsement.getId().getContractId(), workEndorsement.getId().getEndorsementId());
				for (Installment installment : installments) {
					if ((installment.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_PENDING) && (installment.getIssueDate().before(claimDate))) {
						installmentPending = installmentPending + installment.getInstallmentValue();
						expiryDate = installment.getExpiryDate();
						if (effectiveDate == null) effectiveDate = installment.getEffectiveDate();
					}
				}
			}
		}

		return new IDaoPolicy.AdjustmentsToEndorsement(installmentPending, expiryDate, effectiveDate);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ClaimAction getClaimAction(ClaimResponse claimResponse){
		ClaimAction claimAction = new ClaimAction();

		claimAction = daoPolicy.getClaimAction(claimResponse);
		
		return claimAction;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Contract getContractNumber(BigInteger policyNumber, Long certificateNumber, Date effectiveDate) {
		Contract contract = daoPolicy.getContractNumber(policyNumber, certificateNumber, effectiveDate);
		return contract;
	}

	public int cancellationClaimItem(int endorsementId, int contractId, int itemId, int itemStatus, Date cancelDate, String claimNumber, Date claimDate, Date claimNotification){
		return daoPolicy.cancellationClaimItem(endorsementId, contractId, itemId, itemStatus, cancelDate, claimNumber, claimDate, claimNotification);
	}

	public int cancellationClaimItemRiskGroup(int endorsementId, int contractId, int itemId, int riskGroupId, Date cancelDate, String claimNumber, Date claimDate, Date claimNotification){
		return daoPolicy.cancellationClaimItemRiskGroup(endorsementId, contractId, itemId, riskGroupId, cancelDate, claimNumber, claimDate, claimNotification);
	}
	
	/**
	 * Recupera a lista de parcelas de um contrato e endosso.
	 * @param contractId
	 * @param endorsementId
	 * @return lista de Parcelas
	 */
	public List<Installment> getInstallmentForEndorsementId(int contractId,	int endorsementId){
		return daoPolicy.getInstallmentForEndorsementId(contractId, endorsementId);
	}

	private void claimAffectedItem(ClaimResponse claimResponse, final Endorsement endorsement){
		if (endorsement != null) {
			if ((claimResponse.getRiskGroupId() != null) && (claimResponse.getRiskGroupId() != 0)){
				cancellationClaimItemRiskGroup(endorsement.getId().getEndorsementId(), endorsement.getContract().getContractId(), claimResponse.getItemId(), claimResponse.getRiskGroupId(), claimResponse.getClaimDate(), claimResponse.getClaimNumber().toString(), claimResponse.getClaimDate(), claimResponse.getClaimNotification() );				
			}else{
				cancellationClaimItem(endorsement.getId().getEndorsementId(), endorsement.getContract().getContractId(), claimResponse.getItemId(), Item.ITEM_STATUS_INACTIVE, claimResponse.getClaimDate(), claimResponse.getClaimNumber().toString(), claimResponse.getClaimDate(), claimResponse.getClaimNotification() );
			}
		}
	}

	@Override
	public void policyClaim(ClaimRequest[] claimRequests) throws ServiceException {
		// Copia a estrutura para o treemap, juntamente com a resposta
		Map<PolicyCertificateNumber, TreeSet<ClaimResponse>> data = organizeData(claimRequests);
		List<ClaimResponse> claimResponses = null;

		if (validatePolicyClaim(data)){
			for (PolicyCertificateNumber polNumber : data.keySet()) {
				Endorsement endorsement = null;
				boolean endorsementReactivate = false;
				boolean isTotalLoss = false;
				boolean isFalseDeclaration = false;

				TreeSet<ClaimResponse> treeSet = data.get(polNumber);
				Date lastClaimDate = treeSet.last().getClaimDate();
				
				AdjustmentsToEndorsement adjustmentsToEndorsement = null;
				
				for (ClaimResponse claimResponse : data.get(polNumber)) {
					// objeto com a acao a ser tomada
					ClaimAction claimAction = getClaimAction(claimResponse);
					if (claimAction != null){
						// recupera o endosso referente ao periodo do sinistro
						Contract contract = getContractNumber(claimResponse.getPolicyNumber(), claimResponse.getCertificateNumber(), claimResponse.getClaimDate());
						endorsement = this.lastValidEndosementForContractNumber(contract.getContractId());

						// valor de premio pendente caso o endosso referente ao periodo de sinistro esteja ativo
						adjustmentsToEndorsement = getPremiumPending(endorsement, claimResponse.getClaimDate());
						claimResponse.setPremiumPending(adjustmentsToEndorsement.getPremiumPending());

						// reativar endosso case esteja cancelado por falta de pagamento.
						if ((! endorsementReactivate) && (claimResponse.getPremiumPending() == 0) && (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_NO_PAYMENT_CANCEL)) {
							endorsement = new Endorsement();
							
							policyReactivation(contract.getContractId());
							endorsement = this.lastValidEndosementForContractNumber(contract.getContractId());

							adjustmentsToEndorsement = getPremiumPending(endorsement, claimResponse.getClaimDate());
							claimResponse.setPremiumPending(adjustmentsToEndorsement.getPremiumPending());
							endorsementReactivate = true;
						}
						
						// quando por Perda Total: cobrar todas as parcelas
						if ((claimAction.getClaimActionType() == ClaimAction.ACTION_TOTAL_LOSS) || (claimAction.getClaimActionType() == ClaimAction.ACTION_FALSE_DECLARATION)){
							adjustmentsToEndorsement = getPremiumPending(endorsement, endorsement.getExpiryDate());
							claimResponse.setPremiumPending(adjustmentsToEndorsement.getPremiumPending());

							// quando por Falsa Declara��o: n�o cobra premio pendente
							if (claimAction.getClaimActionType() == ClaimAction.ACTION_FALSE_DECLARATION){
								claimResponse.setPremiumPending(0.0);
							}
							
							// datos de identificacao do sinistro
							adjustmentsToEndorsement = new IDaoPolicy.AdjustmentsToEndorsement(claimResponse.getPremiumPending(), adjustmentsToEndorsement.getEffectiveDate(), claimResponse.getClaimNumber().toString(), claimResponse.getClaimDate(), claimResponse.getClaimNotification());
						} else {
							// datos de identificacao do sinistro
							adjustmentsToEndorsement = new IDaoPolicy.AdjustmentsToEndorsement(claimResponse.getPremiumPending(), adjustmentsToEndorsement.getExpiryDate(), claimResponse.getClaimNumber().toString(), claimResponse.getClaimDate(), claimResponse.getClaimNotification());
						}
						
						switch (claimAction.getClaimActionType()) {
							case ClaimAction.ACTION_TOTAL_LOSS:
								isTotalLoss = true;
								break;
							case ClaimAction.ACTION_PARTIAL_LOSS:
								break;
							case ClaimAction.ACTION_AFFECTED_ITEM:
								this.claimAffectedItem(claimResponse, endorsement);
								break;
							case ClaimAction.ACTION_FALSE_DECLARATION:
								isFalseDeclaration = true;
								break;
						}

						claimResponse.setResponseCode(claimAction.getClaimActionType()+"");
						claimResponse.setResponseDescription(claimAction.getDescription());
					} else {
						claimResponse.setErrorID(ErrorValidationClaim.CLAIM_ACTION_TYPE_ERROR.getError());
						claimResponse.setErrorMessage(ErrorValidationClaim.CLAIM_ACTION_TYPE_ERROR.toString());
					}
					
					if (claimResponses == null)
						claimResponses = new LinkedList<ClaimResponse>();					
					claimResponses.add(claimResponse);
				}
				
				if (isTotalLoss) {
					if (adjustmentsToEndorsement.getExpiryDate() != null)
						this.cancelAllInstallmentForCollection(endorsement, lastClaimDate, adjustmentsToEndorsement.getExpiryDate());
					endorsement = this.createEndosementByIssuanceType(endorsement, Endorsement.ISSUANCE_TYPE_POLICY_CLAIM, new IDaoPolicy.AdjustmentsToEndorsement(lastClaimDate, lastClaimDate, Endorsement.ENDORSEMENT_TYPE_COLLECTION, adjustmentsToEndorsement.getClaimNumber(), adjustmentsToEndorsement.getClaimDate(), adjustmentsToEndorsement.getClaimNotification() ));
				} else if (isFalseDeclaration) {
					if (adjustmentsToEndorsement.getExpiryDate() != null)
						this.cancelAllInstallmentForCollection(endorsement, lastClaimDate, adjustmentsToEndorsement.getExpiryDate());
					endorsement = this.createEndosementByIssuanceType(endorsement, Endorsement.ISSUANCE_TYPE_POLICY_CLAIM, new IDaoPolicy.AdjustmentsToEndorsement(lastClaimDate, lastClaimDate, Endorsement.ENDORSEMENT_TYPE_REFUND, adjustmentsToEndorsement.getClaimNumber(), adjustmentsToEndorsement.getClaimDate(), adjustmentsToEndorsement.getClaimNotification() ));
				} else if (endorsementReactivate) {
					if (adjustmentsToEndorsement.getExpiryDate() != null)
						this.cancelAllInstallmentForCollection(endorsement, lastClaimDate, adjustmentsToEndorsement.getExpiryDate());
					endorsement = this.createEndosementByIssuanceType(endorsement, Endorsement.ISSUANCE_TYPE_NO_PAYMENT_CANCEL, new IDaoPolicy.AdjustmentsToEndorsement(endorsement.getEffectiveDate(), lastClaimDate));
				}
				
				//Cancela propuesta o emission renovada
				if (isTotalLoss || isFalseDeclaration) {
					Integer renewalId = endorsement.getContract().getRenewalId();
					if (renewalId != null) {
						if (daoPolicy.lastValidEndosementForContractNumber(renewalId) == null)
							this.cancellationProposal(1, renewalId, Endorsement.CANCEL_REASON_OTHERS);
						else
							this.policyCancellation(renewalId, Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR, new Date(), Endorsement.CANCEL_REASON_OTHERS);
					}
				}
			}
		} else {
			for (PolicyCertificateNumber polNumber : data.keySet()) {
				for (ClaimResponse claimResponse : data.get(polNumber)) {
					if (claimResponses == null)
						claimResponses = new LinkedList<ClaimResponse>();					
					claimResponses.add(claimResponse);
				}
			}
		}
		
		// retorno do JMS do claimResponses
		if (claimResponses != null)
			this.enviarClaimResponse(claimResponses);
	}

	private void enviarClaimResponse(List<ClaimResponse> claimResponses){

		try {
			// Envia ao eInterfaces
			dispachClaimResponse(claimResponses);

		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EInsuranceJMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Envia o envelope de sinistros.
	 * 
	 * @param claimResponses
	 * @return true for sending false for not sending
	 * @throws JMSException Error connecting to server
	 * @throws NamingException  Error connecting to the queue
	 * @throws IOException 
	 * @throws IOException 
	 * @throws EInsuranceJMSException 
	 **/
	private boolean dispachClaimResponse(List<ClaimResponse> claimResponses) throws JMSException, NamingException, IOException, EInsuranceJMSException {

		//EInterfacesJMSTransmitter.getInstance().addClaimResponse(claimResponses);
		EInsuranceJMSConnection.newInstance().sendObject(claimResponses, "queue/zurClaim_response_gw");

		return true;
	}

	private boolean validatePolicyClaim(Map<PolicyCertificateNumber, TreeSet<ClaimResponse>> data) {
		boolean validate = false;
		boolean error = false;
		
		for (PolicyCertificateNumber polNumber : data.keySet()) {
			
			for (ClaimResponse claimResponse : data.get(polNumber)) {

				// validacao que verifica se o endosso nao esta com status de anulacao por sinistro
				if (validationInterfaces.validateEndorsementToClaimRequest(claimResponse)) {
					
					// validacao das demais propriedades do objeto
					if (! validationInterfaces.validateFieldEnvelope(claimResponse)) {
						
						errorValidationClaim = validationInterfaces.getLastError();
						claimResponse.setErrorID(errorValidationClaim.getError());
						claimResponse.setErrorMessage(errorValidationClaim.toString());
					}
					
					else 
						if (!error) validate = true;
					
				} else {
					claimResponse.setErrorID(ErrorValidationClaim.POLICY_ANNULLMENT_CLAIM.getError());
					claimResponse.setErrorMessage(ErrorValidationClaim.POLICY_ANNULLMENT_CLAIM.toString());
					validate = false;
					error = true;
				}
				
			}
		}
		return validate;
	}

	/** Usado para realizar a ordena��o */
	private Comparator<ClaimResponse> comparatorClaimRequest = new Comparator<ClaimResponse>(){
		/** {@inheritDoc} */
		@Override
		public int compare(ClaimResponse arg0, ClaimResponse arg1) {
			/* Regra b�sica: retorna -1 se menor, 0 se igual, 1 se maior */
			int result = (arg0.getClaimDate().compareTo(arg1.getClaimDate()));
			if (result != 0)
				return result;
			result = arg1.getItemId() - arg0.getItemId();
			if (result != 0)
				return result;
			result = (arg1.getRiskGroupId()==null?0:arg1.getRiskGroupId()) - (arg0.getRiskGroupId()==null?0:arg0.getRiskGroupId()) ;
			if (result != 0)
				return result;
			result = arg1.getCoverageId() - arg0.getCoverageId();
			return result;
		}
	};

	/** ApplicationConext to be used to search for beans */ 
	private ApplicationContext applicationContext;

	/**
	 * Organiza os dados
	 * @param claimRequests
	 * @return
	 */
	private Map<PolicyCertificateNumber, TreeSet<ClaimResponse>> organizeData(ClaimRequest[] claimRequests) {
		Map<PolicyCertificateNumber, TreeSet<ClaimResponse>> reqResp = new HashMap<PolicyCertificateNumber, TreeSet<ClaimResponse>> ();

		for (ClaimRequest claimRequest:claimRequests) {
			PolicyCertificateNumber certificateNumber = new PolicyCertificateNumber(claimRequest.getPolicyNumber(), claimRequest.getCertificateNumber());
			TreeSet<ClaimResponse> treeSet = null; 

			if (! reqResp.keySet().contains(certificateNumber)) {
				treeSet = new TreeSet<ClaimResponse>(comparatorClaimRequest);
				reqResp.put(certificateNumber, treeSet);
			} else {
				treeSet = reqResp.get(certificateNumber);
			}
			
			treeSet.add(new ClaimResponse(claimRequest));
		}
		return reqResp;
	}

	/**.
	 * Cancela as parcelas pendentes anteriores, enviando um aviso para o sistema de cobran�a (JMS)
	 * @param endorsement Endorsement a ser atualizado
	 * @param cancelDate Data de Cancelamento
	 * @param transmiterDate Hora de transmiss�o
	 */
	private void cancelaParcelasPendentesAnteriores(Endorsement endorsement, Date cancelDate, Date transmiterDate) {

		boolean hasInstallments = anulaParcelasPendentes(endorsement, cancelDate, transmiterDate);

		try {
			// Envia ao eCollection
			if (hasInstallments)
				dispachCollection(endorsement, true);

		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EInsuranceJMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Anula parcelas pendentes
	 * @param endorsement
	 * @param dataAnulacao
	 * @param transmiterDate
	 * @return true if has installments pending
	 */
	private boolean anulaParcelasPendentes(Endorsement endorsement, Date cancelDate, Date transmiterDate) {
		boolean retorno = false;

		for (Installment installment :	endorsement.getInstallments()){
			if (installment.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_PENDING){
				retorno = true;

				installment.setInstallmentStatus(Installment.INSTALLMENT_STATUS_ANNULMENT);
				installment.setCancelDate(cancelDate);
				installment.setTransmitted(false);

				if ( endorsement.getIssuingStatus() == Endorsement.ISSUING_STATUS_ANULADA){
					installment.setTransmitted(true);
					installment.setShippingCollection(false);
				}else{
					//Transmitir somente parcelas anteriores a Data de Cancelamento/Anula��o
					Calendar calendarTransmiterDate = Calendar.getInstance();
					Calendar calendarEffectiveDate = Calendar.getInstance();
					
					calendarTransmiterDate.setTime(transmiterDate);
					calendarEffectiveDate.setTime(installment.getEffectiveDate());

					if (calendarEffectiveDate.after(calendarTransmiterDate)){
						installment.setTransmitted(true);
					}
				}
				daoInstallment.mergeAndFlush(installment);
			}
		}
		return retorno;
	}

	public boolean dispachCollectionPay(Installment installment) throws EInsuranceJMSException {
		if (installment != null) {
			List<Installment> installments = new ArrayList<Installment>();
			installments.add(installment);
			
			// fill Detail
			CollectionEnvelope collectionEnvelope = this.fillCollectionEnvelope(installment.getEndorsement(), installments, (installment.isPaymentNotified()?CollectionEnvelope.ACTION_NOTIFY:CollectionEnvelope.ACTION_PAY));
			
			if (collectionEnvelope.getCollectionDetails().size() > 0) {
				try {
					EInsuranceJMSConnection.newInstance().sendObject(collectionEnvelope, "queue/collection_request");
					return true;
				}
				catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JMSException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return false;
	}

	/**
	 * Envia o envelope de cobran�a.
	 * 
	 * @param endorsement
	 * @return true for sending false for not sending
	 * @throws JMSException Error connecting to server
	 * @throws NamingException Error connecting to the queue
	 * @author leo.costa
	 * @throws IOException
	 * @throws IOException
	 * @throws EInsuranceJMSException
	 **/
	private boolean dispachCollection(Endorsement endorsement, boolean cancellation) throws JMSException, NamingException, IOException, EInsuranceJMSException {
		if (endorsement == null || endorsement.getInstallments() == null || endorsement.getCustomerByPolicyHolderId() == null)
			throw new IllegalArgumentException("endorsement and installments and customerByPolicyHolderId can not be null");

		List<Installment> workInstallmentSent = new ArrayList<Installment>();
		for (Installment installment : endorsement.getInstallments()) {
			if (installment.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_PENDING) {
				workInstallmentSent.add(installment);
			} else if (installment.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_ANNULMENT && cancellation) {
				workInstallmentSent.add(installment);
			}
		}

		if ((workInstallmentSent.size() == 0)) {
			return false;
		}

		CollectionEnvelope collectionEnvelope = this.fillCollectionEnvelope(endorsement, workInstallmentSent, (cancellation ? CollectionEnvelope.ACTION_CANCELLATION : CollectionEnvelope.ACTION_EFFECTIVE));

		if (collectionEnvelope.getCollectionDetails().size() > 0)
			ECollectionJMSTransmitter.getInstance().addCollectionEnvelope(collectionEnvelope);

		return true;
	}
	
	/**
	 * Envia o envelope de resposta da cobran�a.
	 * 
	 * @param clientReference
	 * @param collectionReference
	 * @throws JMSException Error connecting to server
	 * @throws NamingException Error connecting to the queue
	 * @author wellington.barbosa
	 * @throws EInsuranceJMSException
	 **/
	private void responseCollection(String clientReference, String billingReference) {
		try {
			if (clientReference != null && billingReference != null) {
				if (!clientReference.equalsIgnoreCase("") && !billingReference.equalsIgnoreCase("")) {
					CollectionEnvelope collectionEnvelope = this.fillCollectionResponse(clientReference, billingReference);
					
					if (collectionEnvelope.getCollectionDetails().size() > 0)
						ECollectionJMSTransmitter.getInstance().addCollectionEnvelope(collectionEnvelope);
				}
			}
		} catch (EInsuranceJMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Popula os objetos de cobran�a de acordo com o endorsement
	 * 
	 * @param endorsement
	 * @return {@link CollectionEnvelope} objeto populado
	 * @author leo.costa
	 **/
	private CollectionEnvelope fillCollectionEnvelope(final Endorsement endorsement, final List<Installment> installments, final int action) {
		final int applicationId = 1;
		final String applicationDescricao = "I";  //eInsurance
		final Contract contract = endorsement.getContract();
		
		Customer policyHolder = endorsement.getCustomerByPolicyHolderId();
		Phone phone = policyHolder.getFirstAddress().getFirstPhone();
		Partner partner = daoPartner.findById(contract.getPartnerId());
		DataOption personType = daoDataOption.findById(policyHolder.getPersonType());
		DataOption phoneType = daoDataOption.findById(phone.getPhoneType());
		Channel channel = daoChannel.findById(endorsement.getChannelId());
		Product product = daoProduct.findById(contract.getProductId());
		
		List<DetailAttribute> basicAttributes = new ArrayList<DetailAttribute>();
		basicAttributes.add(new DetailAttribute(DetailAttribute.CERTIFICATE_NUMBER, contract.getCertificateNumber().toString()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.POLICY_NUMBER, contract.getPolicyNumber().toString()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.PARTNER_ID, contract.getPartnerId()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.ENDORSEMENT_ID, endorsement.getId().getEndorsementId()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.PROPOSAL_NUMBER, endorsement.getProposalNumber().toString()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.CONTRACT_ID, endorsement.getId().getContractId()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.CHANNEL_CODE, channel.getExternalCode())); 
		basicAttributes.add(new DetailAttribute(DetailAttribute.PHONE_TYPE, phoneType.getFieldDescription()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.PHONE_NUMBER, phone.getPhoneNumber()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.PARTNER_NAME, partner.getNickName()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.POLICY_HOLDER_PERSON_TYPE, personType.getFieldValue()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.PRODUCT_CODE, product.getProductCode()));
		if (contract.getExternalKey() != null && !"".equalsIgnoreCase(contract.getExternalKey().trim()))
			basicAttributes.add(new DetailAttribute(DetailAttribute.EXTERNAL_KEY, contract.getExternalKey().trim()));
		
		CollectionEnvelope collectionEnvelope = new CollectionEnvelope(action);
		collectionEnvelope.setApplicationId(applicationId);
		collectionEnvelope.setLotCode(endorsement.getProposalNumber().toString());
		
		for (Installment readingInstallment : installments) {
			CollectionDetail collectionDetail = collectionEnvelope.addCollectionDetail();
			StringBuilder clientReference = new StringBuilder();
			StringBuilder billingReference = new StringBuilder();
			List<DetailAttribute> attributesSpecified = new ArrayList<DetailAttribute>();
			
			attributesSpecified.addAll(basicAttributes);
			attributesSpecified.add(new DetailAttribute(DetailAttribute.INSTALLMENT_ID, readingInstallment.getId().getInstallmentId()));
			if (readingInstallment.getCommission() != null && !readingInstallment.getCommission().equals(0.0))
				attributesSpecified.add(new DetailAttribute(DetailAttribute.COMMISSION_VALUE, readingInstallment.getCommission().toString()));
			if (readingInstallment.getLabour() != null && !readingInstallment.getLabour().equals(0.0))
				attributesSpecified.add(new DetailAttribute(DetailAttribute.LABOUR_VALUE, readingInstallment.getLabour().toString()));
			collectionDetail.setDetailAttributes(attributesSpecified);
			
			collectionDetail.setBillingMethodId(readingInstallment.getBillingMethodId());
			collectionDetail.setInstallmentType(readingInstallment.getInstallmentType());
			collectionDetail.setPaymentType(readingInstallment.getPaymentType());
			
			// Client Reference
			clientReference.append(applicationDescricao);
			clientReference.append(StringUtil.fill(contract.getPolicyNumber().toString(), "0", 13, true));
			clientReference.append(StringUtil.fill(contract.getCertificateNumber().toString(), "0", 6, true));
			collectionDetail.setClientReference(clientReference.toString());
			
			// Billing Reference
			billingReference.append(StringUtil.fill(endorsement.getProposalNumber().toString() + "", "0", 6, true));
			billingReference.append(StringUtil.fill(readingInstallment.getId().getInstallmentId() + "", "0", 2, true));
			collectionDetail.setBillingReference(billingReference.toString());
			
			collectionDetail.setDueDate(readingInstallment.getIssueDate());
			collectionDetail.setLimitDate(readingInstallment.getDueDate());
			collectionDetail.setCancelDate(readingInstallment.getCancelDate());
			if (action == CollectionEnvelope.ACTION_CANCELLATION && collectionDetail.getCancelDate() == null)
				collectionDetail.setCancelDate(new Date());
			
			collectionDetail.setInstallmentValue(readingInstallment.getInstallmentValue());
			collectionDetail.setCurrencyId(contract.getCurrencyId());
			collectionDetail.setCurrencyDate(readingInstallment.getEndorsement().getCurrencyDate());
			collectionDetail.setConversionRate(readingInstallment.getEndorsement().getConversionRate());
			
			if (action == CollectionEnvelope.ACTION_PAY) {
				collectionDetail.setPaymentDate(readingInstallment.getPaymentDate());
				collectionDetail.setPaidValue(readingInstallment.getPaidValue());
				collectionDetail.setPaidCurrencyDate(readingInstallment.getCurrencyDate());
				collectionDetail.setPaidConversionRate(readingInstallment.getConversionRate());
				collectionDetail.setCreditAuthorizationCode(readingInstallment.getCreditAuthorizationCode());
			}
			
			collectionDetail.setHolderName(policyHolder.getName());
			collectionDetail.setHolderDocumentType(policyHolder.getDocumentType());
			collectionDetail.setHolderDocumentNumber(policyHolder.getDocumentNumber());
			
			collectionDetail.setBankNumber(readingInstallment.getBankNumber());
			collectionDetail.setBankAgencyNumber(readingInstallment.getBankAgencyNumber());
			collectionDetail.setBankAccountNumber(readingInstallment.getBankAccountNumber());
			collectionDetail.setBankCheckNumber(readingInstallment.getBankCheckNumber());
			
			collectionDetail.setCardType(readingInstallment.getCardType());
			collectionDetail.setCardBrandType(readingInstallment.getCardBrandType());
			collectionDetail.setCardNumber(readingInstallment.getCardNumber());
			collectionDetail.setCardExpirationDate(readingInstallment.getCardExpirationDate());
			collectionDetail.setCardHolderName(readingInstallment.getCardHolderName());
			
			collectionDetail.setOtherAccountNumber(readingInstallment.getOtherAccountNumber());
			collectionDetail.setInvoiceNumber(readingInstallment.getInvoiceNumber());
			
			collectionDetail.setPaymentNotified(readingInstallment.isPaymentNotified());
		}
		
		return collectionEnvelope;
	}

	/**
	 * Popula os objetos de resposta cobran�a
	 * 
	 * @param clientReference
	 * @param collectionReference
	 * @return {@link CollectionEnvelope} objeto populado
	 * @author wellington.barbosa
	 **/
	private CollectionEnvelope fillCollectionResponse(String clientReference, String billingReference) {
		final int applicationEinsuranceId = 1;
		
		// Envelope
		CollectionEnvelope collectionEnvelope = new CollectionEnvelope(CollectionEnvelope.ACTION_RESPONSE);
		collectionEnvelope.setApplicationId(applicationEinsuranceId);
		collectionEnvelope.setLotDate(new Date());
		
		// Parcela
		CollectionDetail collectionDetail = collectionEnvelope.addCollectionDetail();
		collectionDetail.setClientReference(clientReference);
		collectionDetail.setBillingReference(billingReference);
		
		return collectionEnvelope;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ReturnClaimStateVerification claimStateVerification(int contractId, Date checkDate, int issuanceType) {
		Endorsement endorsement = daoPolicy.getEndosement(contractId, 1);
		Contract contract = endorsement.getContract();
		ReturnClaimStateVerification retorno = new ReturnClaimStateVerification();

		String beanName = getBeanNameByContract(contract);
		if (this.applicationContext.containsBean(beanName)){ // Verifica se cont�m o bean que far� a checagem
			CheckStateClaim stateClaim = (CheckStateClaim) this.applicationContext.getBean(beanName);
			Map<String, Object> check = stateClaim.check(createCheckParameters(endorsement, checkDate, issuanceType));
			// Transforma o retorno no tipo ReturnClaimStateVerification
			retorno.setResult((Boolean) check.get(CheckStateClaim.RESULT_VAR));
			Object object = check.get(CheckStateClaim.RESULT_ERRORS_MESSAGES);
			if (object != null) {
				retorno.setErrorMessages((List<String>) object);
				retorno.setErros(retorno.getErrorMessages().size());
			}
		} else {
			logger.error("Bean not found: " + beanName);
		}
		return retorno;
	}

	/**
	 * Cria mapa com dados para 
	 * @param endorsement
	 * @param date
	 * @return
	 */
	private Map<String, Object> createCheckParameters(Endorsement endorsement, Date date, int issuanceType) {
		HashMap<String, Object> map = new HashMap<String, Object>();

		// Dados usado no AS400
		map.put(CheckStateClaim.PARAM_POLICE, endorsement.getContract().getPolicyNumber());
		map.put(CheckStateClaim.PARAM_CERTIFICATE, endorsement.getContract().getCertificateNumber());
		map.put(CheckStateClaim.PARAM_AGENCY, daoChannel.findById(endorsement.getChannelId()).getExternalCode());
		map.put(CheckStateClaim.PARAM_QUERY_DATE, date);
		map.put(CheckStateClaim.PARAM_ISSUANCE_TYPE, issuanceType);
		return map;
	}

	/**
	 * Para cada tipo de contrato, poder� ser usado um beans diferente
	 * 
	 * @param contract
	 * @return Nome do bean relacionado
	 */
	private String getBeanNameByContract(Contract contract) {
		return "zurichClaim";
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}

	public void processInstallment(CollectionResponseItem[] collectionResponse) throws ServiceException {
		for (CollectionResponseItem collectionResponseItem : collectionResponse) {
			processInstallmentItem(collectionResponseItem);
		}
	}

	private void processInstallmentItem(CollectionResponseItem collectionResponse) {
		Installment installment = daoPolicy.findInstallment(collectionResponse.getProposalNumber(), collectionResponse.getInstallmentId());
		if (installment != null) {
			if (((installment.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_PENDING) || 
					(installment.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_LOCKED) || 
					(installment.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_ANNULMENT)) && 
					(collectionResponse.getType() == CollectionResponseItem.TYPE_PAYMENT)) {
				installment.setPaymentDate(collectionResponse.getPaymentDate());
				installment.setPaidValue(collectionResponse.getPaidValue());
				installment.setCurrencyDate(collectionResponse.getCurrencyDate());
				installment.setConversionRate(collectionResponse.getConversionRate());
				installment.setBankCheckNumber((collectionResponse.getBankCheckNumber() != null ? collectionResponse.getBankCheckNumber().toString() : null));
				installment.setInvoiceNumber(collectionResponse.getInvoiceNumber());
				installment.setDebitAuthorizationCode(collectionResponse.getDebitAuthorizationCode());
				installment.setCreditAuthorizationCode(collectionResponse.getCreditAuthorizationCode());
				installment.setInstallmentStatus(Installment.INSTALLMENT_STATUS_PAID);
				installment.setPaymentNotified(false);
				installment.setTransmitted(false);
				daoPolicy.save(installment);
				
				if (installment.getEndorsement().getIssuingStatus() == Endorsement.ISSUING_STATUS_PROPOSTA)
					createAndFreePendency(installment, new Date(), 2, LockPendency.LOCK_PENDENCY_STATUS_APROVED);
				
				this.responseCollection(collectionResponse.getClientReference(), collectionResponse.getBillingReference());
			}
			else if ((installment.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_PAID) && (collectionResponse.getType() == CollectionResponseItem.TYPE_PENDENT)) {
				installment.setPaymentDate(null);
				installment.setPaidValue(null);
				installment.setCurrencyDate(null);
				installment.setConversionRate(null);
				installment.setBankCheckNumber(null);
				installment.setInvoiceNumber(null);
				installment.setDebitAuthorizationCode(null);
				installment.setCreditAuthorizationCode(null);
				installment.setInstallmentStatus(Installment.INSTALLMENT_STATUS_PENDING);
				installment.setPaymentNotified(false);
				installment.setTransmitted(false);
				daoPolicy.save(installment);
				
				this.responseCollection(collectionResponse.getClientReference(), collectionResponse.getBillingReference());
			}
			else if ((installment.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_PAID) && (collectionResponse.getType() == CollectionResponseItem.TYPE_PAYMENT)) {
				this.responseCollection(collectionResponse.getClientReference(), collectionResponse.getBillingReference());
			}
			else if ((installment.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_PENDING) && ((collectionResponse.getType() == CollectionResponseItem.TYPE_CANCEL) || ((collectionResponse.getType() == CollectionResponseItem.TYPE_PENDENT)))) {
				// verifica se a parcela passou do limite de pagto
				if (installment.getDueDate().compareTo(new Date()) < 0) {
					installment.setInstallmentStatus(Installment.INSTALLMENT_STATUS_ANNULMENT);
					installment.setCancelDate(new Date());
					
					if (installment.getEndorsement().getIssuingStatus() == Endorsement.ISSUING_STATUS_PROPOSTA){
						createAndFreePendency(installment, new Date(), 2, LockPendency.LOCK_PENDENCY_STATUS_REJECTED);
					}
					else {
						//Defini��o da data de anulacao pela Zurich
						installment.setCancelDate(installment.getExpiryDate());
						try {
							this.policyCancellationNoPayment(installment.getId().getContractId(), installment.getEffectiveDate(), installment.getCancelDate());
						} catch (ServiceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					daoPolicy.save(installment);
				}
			}
			else if ((installment.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_ANNULMENT) && ((collectionResponse.getType() == CollectionResponseItem.TYPE_CANCEL) || ((collectionResponse.getType() == CollectionResponseItem.TYPE_PENDENT)))) {
				try {
					// Envia ao eCollection
					dispachCollection(installment.getEndorsement(), true);

				} catch (JMSException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (EInsuranceJMSException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				logger.error("Alteraci�n de estatus inv�lida");
			}
		}
		else logger.error("installment not found");
	}
	
	/**
	 * @param installment
	 * @param date
	 * @param pendencyId
	 * @param lockPendencyStatus
	 */
	public void createAndFreePendency(Installment installment, Date date, int pendencyId, int lockPendencyStatus) {
		LockPendency lck = createPendency(installment.getId().getContractId(), installment.getId().getEndorsementId(), date, pendencyId, lockPendencyStatus);
		
		// free pendency
		User user = AuthenticationHelper.getLoggedUser();
		lck.setUnlockDate(((lockPendencyStatus == LockPendency.LOCK_PENDENCY_STATUS_APROVED || lockPendencyStatus == LockPendency.LOCK_PENDENCY_STATUS_REJECTED) ? date : null));
		lck.setUserID((user != null ? user.getUserId() : null));
		
		freePendency(lck);
	}
	
	@Override
	public void createAndFreePendency(int contractId, int endorsementId, Date date, int pendencyId, int lockPendencyStatus) {
		LockPendency lck = createPendency(contractId, endorsementId, date, pendencyId, lockPendencyStatus);
		
		// free pendency
		User user = AuthenticationHelper.getLoggedUser();
		lck.setUnlockDate(((lockPendencyStatus == LockPendency.LOCK_PENDENCY_STATUS_APROVED || lockPendencyStatus == LockPendency.LOCK_PENDENCY_STATUS_REJECTED) ? date : null));
		lck.setUserID((user != null ? user.getUserId() : null));
		
		freePendency(lck);
	}
	
	@Override
	public LockPendency createPendency(int contractId, int endorsementId, Date date, int pendencyId, int lockPendencyStatus){
		LockPendencyId lckId = new LockPendencyId();
		lckId.setContractID(contractId);
		lckId.setEndorsementID(endorsementId);
		LockPendency lck = new LockPendency();
		lck.setId(lckId);
		lck.setPendencyID(pendencyId);
		lck.setIssueDate(date);
		lck.setLockPendencyStatus(lockPendencyStatus);
		return lck;
	}

	private void freePendency(LockPendency pendenciaEntrada) {
		LockPendency pendency = daoPolicy.findPendency(pendenciaEntrada.getId().getContractID(), pendenciaEntrada.getId().getEndorsementID(), pendenciaEntrada.getItemID(), pendenciaEntrada.getPendencyID());
		if (pendency != null) {
			if(pendency.getLockPendencyStatus() == LockPendency.LOCK_PENDENCY_STATUS_PENDENT){
				pendency.setUserID(pendenciaEntrada.getUserID());
				//pendency.setIssueDate(pendenciaEntrada.getIssueDate());
				pendency.setUnlockDate(pendenciaEntrada.getUnlockDate());
				pendency.setLockPendencyStatus(pendenciaEntrada.getLockPendencyStatus());
				daoPolicy.historico(pendency, "Cambio de estatus: " + daoPolicy.getDataOption(pendency.getLockPendencyStatus()).getFieldDescription(), LockPendencyHistory.HISTORY_TYPE_STATUS_CHANGE, pendenciaEntrada.getIssueDate(), pendenciaEntrada.getUserID(), pendenciaEntrada.getLockPendencyStatus());
				daoPolicy.save(pendency);
			}
		}
		else {
			logger.fatal("Pendency no encontrada: Contract: [" + pendenciaEntrada.getId().getContractID() + "], Endorsement: [" + pendenciaEntrada.getId().getEndorsementID() + "], Item[" + ( pendenciaEntrada.getItemID()==null?"":pendenciaEntrada.getItemID() ) + "], pendencyID: [" + pendenciaEntrada.getPendencyID() + "]");
		}
		if ( pendenciaEntrada.getLockPendencyStatus() == LockPendency.LOCK_PENDENCY_STATUS_REJECTED){
			Endorsement endorsement = daoPolicy.loadEndorsement(pendenciaEntrada.getId().getEndorsementID(), pendenciaEntrada.getId().getContractID());
			anulaProposta(endorsement, pendenciaEntrada.getIssueDate());
			System.out.println("Anulou a pendencia");
		}
		else{
			try {
				policyEmision(pendenciaEntrada.getId().getContractID(), pendenciaEntrada.getId().getEndorsementID());
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Emitiu a pendencia");
		}
	}
	
	/**
	 * Anulates endorsement
	 * @param endorsement
	 */
	private void anulaProposta(Endorsement endorsement, Date cancelDate) {
		// Contract Cancelation
		if (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_EMISSION){
			endorsement.getContract().setContractStatus(Contract.CONTRACT_STATUS_ABROGATED);
			endorsement.getContract().setCancelDate(cancelDate);
		}

		// Endorsement Cancelation
		endorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_ANULADA);
		endorsement.setCancelReason(Endorsement.CANCEL_REASON_OTHERS);
		endorsement.setCancelDate(cancelDate);
		endorsement.setTransmitted(true);

		// Item Cancelation
		for (Item item : endorsement.getItems()){
			item.setItemStatus(Item.ITEM_STATUS_INACTIVE);
			item.setCancelDate(cancelDate);
		}

		cancelaParcelasPendentesAnteriores(endorsement, cancelDate, cancelDate);

		daoPolicy.save(endorsement.getContract());
		daoPolicy.save(endorsement);
	}

	public void policyEmision(int contractId, int endorsementId) throws ServiceException {
		Endorsement endorsement = daoPolicy.loadEndorsement(endorsementId, contractId);
		if (endorsement.getIssuingStatus() != Endorsement.ISSUING_STATUS_PROPOSTA)
			return;
		List<LockPendency> lockPendencies = daoPolicy.findLockPendencies(endorsementId, contractId);
		int status = LockPendency.LOCK_PENDENCY_STATUS_APROVED; 
		loop:for(LockPendency lockPendency:lockPendencies){
			switch (lockPendency.getLockPendencyStatus()){
			case LockPendency.LOCK_PENDENCY_STATUS_REJECTED:
				status = LockPendency.LOCK_PENDENCY_STATUS_REJECTED;
				break loop;
			case LockPendency.LOCK_PENDENCY_STATUS_APROVED:
			case LockPendency.LOCK_PENDENCY_STATUS_BLOCKED:
				break;
			default:
				status = LockPendency.LOCK_PENDENCY_STATUS_PENDENT;
			}
		}
		
		System.out.println("\n\n    -> status" + status);
		if ((status == LockPendency.LOCK_PENDENCY_STATUS_APROVED)||(status == LockPendency.LOCK_PENDENCY_STATUS_REJECTED)){
			if (status == LockPendency.LOCK_PENDENCY_STATUS_APROVED){
				endorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_EMITIDA);
				Calendar c = Calendar.getInstance();
				c.setTime(new Date());
				c.add(Calendar.DAY_OF_MONTH, -1);
				endorsement.setIssuanceDate(c.getTime());
			}
			else if (status == LockPendency.LOCK_PENDENCY_STATUS_REJECTED){
				System.out.println("Anulando o endorsement");
				anulaProposta(endorsement, new Date());
			}
		}
		daoPolicy.save(endorsement);
	}

	public void receiveAdvice(CollectionResponseItem[] collectionResponse) throws ServiceException {
		for (CollectionResponseItem collectionResponseItem:collectionResponse) {
			Installment installment = daoPolicy.findInstallment(collectionResponseItem.getProposalNumber(), collectionResponseItem.getInstallmentId());
			if (installment != null ) {
				installment.setShippingCollection(true);
				daoPolicy.save(installment);
			}
		}
	}

	public Endorsement policyChangeRegister(Endorsement endorsement, Date referenceDate) throws ServiceException {
		Endorsement returnEndosement = endorsement;
		Endorsement validateEndorsement = this.lastValidEndosementForContractNumber(endorsement.getId().getContractId());

		if ((endorsement != null) &&
			(validateEndorsement != null) &&
			(this.validatePolicyChangeRegister(validateEndorsement, endorsement, referenceDate))) {

			//Gera o endoso de anula��o por Solicita��o ou por Erro de emiss�o
			returnEndosement = this.createEndosementByIssuanceType(endorsement, Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_REGISTER, new IDaoPolicy.AdjustmentsToEndorsement(referenceDate));
		} else {
			returnEndosement.getErrorList().add(ErrorTypes.VALIDATION_POLICY_CHANGE_REGISTER_ERROR, null, null, null, 0);
		}		

		return returnEndosement;
	}
	
	public Endorsement policyChangeTechnical(Endorsement endorsement, Date referenceDate, boolean isRecalculation, boolean isProposal, PaymentOption paymentOption) throws ServiceException {
		Endorsement returnEndosement = endorsement;
		Endorsement validateEndorsement = this.lastValidEndosementForContractNumber(endorsement.getId().getContractId());

		if ((endorsement != null) &&
			(validateEndorsement != null) &&
			(this.validatePolicyChangeTechnical(validateEndorsement, endorsement, referenceDate))) {

			//Gera o endoso de anula��o por Solicita��o ou por Erro de emiss�o
			returnEndosement = this.createEndosementByIssuanceType(endorsement, Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL, new IDaoPolicy.AdjustmentsToEndorsement(referenceDate, isRecalculation, isProposal, paymentOption));

			//Verifica si finalizou a emiss�o
			if (isProposal && returnEndosement.getIssuingStatus() == Endorsement.ISSUING_STATUS_EMITIDA) {
				
				//Cancela todos as parcelas de cobranza pendentes, da emiss�o anterior
				this.cancelAllInstallmentForCollection(returnEndosement, referenceDate, new Date(), false);
				
				// Envia ao eCollection as novas parcelas
				if (returnEndosement.getEndorsementType() == Endorsement.ENDORSEMENT_TYPE_COLLECTION) {
					try {
						
						serviceCollection.dispachCollection(returnEndosement, false);
						
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JMSException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (NamingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (EInsuranceJMSException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} else {
			returnEndosement.getErrorList().add(ErrorTypes.VALIDATION_POLICY_CHANGE_TECHNICAL_ERROR, null, null, null, 0);
			//throw new ServiceException(ErrorTypes.VALIDATION_POLICY_CHANGE_TECHNICAL_ERROR.getMessage(null));
		}

		return returnEndosement;
	}
	
	public Endorsement policyRenewal(int contractId, int endorsementId) throws ServiceException {
		Endorsement inputEndosement = daoPolicy.getEndosement(contractId, endorsementId);
		Endorsement returnEndosement = this.adjustEndorsementToRenewal(inputEndosement);
		if (returnEndosement != null) {
			try {
				this.calculateRenewal(returnEndosement, inputEndosement);
				if (!(returnEndosement.getErrorList() != null && returnEndosement.getErrorList().getListaErros().size() > 0)) {
					PaymentOption paymentOption = this.fillPaymentOption(returnEndosement, contractId);
					this.proposalRenewal(returnEndosement, paymentOption);
				}
				logger.debug("Renovando contrato: " + returnEndosement.getContract().getContractId());
			} catch (ServiceException e) {
				logger.error("Erro durante renovaci�n del contrato", e);
			}
		}
		return returnEndosement;
	}

	public Endorsement policyRenewal(Endorsement endorsement, PaymentOption paymentOption) throws ServiceException {
		if (endorsement != null && paymentOption != null) {
			try {
				if (this.validatePolicyRenewal(endorsement)) {
					this.adjustToPolicyRenewal(endorsement);
					this.calculateRenewal(endorsement, null);
					PaymentOption newPaymentOption = this.fillPaymentOption(endorsement, paymentOption);
					this.proposalRenewal(endorsement, newPaymentOption);
				}
				logger.debug("Renovando contrato: " + endorsement.getContract().getContractId());
			} catch (ServiceException e) {
				logger.error("Error durante la renovaci�n del contrato externo", e);
			}
		}
		return endorsement;
	}
	
	private void adjustToPolicyRenewal(Endorsement endorsement) {
		// Set contract properties
		Contract contract = endorsement.getContract();
		contract.setRenewed(true);
		contract.setContractStatus(Contract.CONTRACT_STATUS_ACTIVE);
		
		// Set endorsement properties
		endorsement.setIssuanceType(Endorsement.ISSUANCE_TYPE_POLICY_EMISSION);
		endorsement.setTransmitted(false);
		endorsement.setQuotationDate(new Date());
		endorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_COTACAO);
		
		// Set item properties
		for (Item item : endorsement.getItems()) {
			item.setRenewalType(Item.ITEM_RENEW_TYPE_RENEWAL);
			item.setRenewalInsurerId(contract.getInsurerId());
			item.setRenewalPolicyNumber(contract.getPolicyNumber().toString());
			item.setBonusType(null);
			item.setEffectiveDate(endorsement.getEffectiveDate());
			item.setCancelDate(null);
			item.setCalculationValid(false);
			item.setItemStatus(Item.ITEM_STATUS_ACTIVE);
		}
	}

	private void adjustToPolicyIssuance(Endorsement endorsement) {
		// Set contract properties
		Contract contract = endorsement.getContract();
		contract.setRenewed(false);
		contract.setContractStatus(Contract.CONTRACT_STATUS_ACTIVE);
		
		// Set endorsement properties
		endorsement.setIssuanceType(Endorsement.ISSUANCE_TYPE_POLICY_EMISSION);
		endorsement.setTransmitted(false);
		endorsement.setQuotationDate(new Date());
		endorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_COTACAO);
		
		// Set item properties
		for (Item item : endorsement.getItems()) {
			item.setRenewalType(Item.ITEM_RENEW_TYPE_EMISSION);
			item.setBonusType(null);
			item.setEffectiveDate(endorsement.getEffectiveDate());
			item.setCancelDate(null);
			item.setCalculationValid(false);
			item.setItemStatus(Item.ITEM_STATUS_ACTIVE);
		}
	}
	
	private void adjustToPolicyChangeRegister(Endorsement lastEndorsement, Endorsement inputEndorsement) {
		// adjust Customer by Insured
		this.adjustToPolicyCustomer(lastEndorsement.getCustomerByInsuredId(), inputEndorsement.getCustomerByInsuredId(), lastEndorsement.getMainItem().getObjectId());
		
		// adjust Customer by Holder
		this.adjustToPolicyCustomer(lastEndorsement.getCustomerByPolicyHolderId(), inputEndorsement.getCustomerByPolicyHolderId(), lastEndorsement.getMainItem().getObjectId());
		
		// adjust Item
		this.adjustToPolicyItem(lastEndorsement.getMainItem(), inputEndorsement.getMainItem());
	}

	private void adjustToPolicyItem(Item lastItem, Item inputItem) {
		// clean beneficiary Last item
		Iterator<Beneficiary> iterator = lastItem.getBeneficiaries().iterator();
		while (iterator.hasNext()) {
			iterator.next();
			iterator.remove();
		}
		// adjust beneficiary Last with Input
		for (Beneficiary inputBeneficiary: inputItem.getBeneficiaries() ) {
			this.adjustToPolicyBeneficiary(lastItem.addBeneficiary(), inputBeneficiary);
		}
	}
	
	private void adjustToPolicyBeneficiary(Beneficiary lastBeneficiary, Beneficiary inputBeneficiary) {
		// adjust beneficiary Last with Input
		lastBeneficiary.setBeneficiaryType(inputBeneficiary.getBeneficiaryType());
		lastBeneficiary.setName(inputBeneficiary.getName());
		lastBeneficiary.setLastName(inputBeneficiary.getLastName());
		lastBeneficiary.setFirstName(inputBeneficiary.getFirstName());
		lastBeneficiary.setKinshipType(inputBeneficiary.getKinshipType());
		lastBeneficiary.setPersonType(inputBeneficiary.getPersonType());
		lastBeneficiary.setGender(inputBeneficiary.getGender());
		lastBeneficiary.setBirthDate(inputBeneficiary.getBirthDate());
		lastBeneficiary.setMaritalStatus(inputBeneficiary.getMaritalStatus());
		lastBeneficiary.setUndocumented(inputBeneficiary.isUndocumented());
		lastBeneficiary.setDocumentType(inputBeneficiary.getDocumentType());
		lastBeneficiary.setDocumentNumber(inputBeneficiary.getDocumentNumber());
		lastBeneficiary.setDocumentIssuer(inputBeneficiary.getDocumentIssuer());
		lastBeneficiary.setDocumentValidity(inputBeneficiary.getDocumentValidity());
		lastBeneficiary.setParticipationPercentage(inputBeneficiary.getParticipationPercentage());
		lastBeneficiary.setDisplayOrder((short)lastBeneficiary.getId().getBeneficiaryId());
		lastBeneficiary.setName(lastBeneficiary.getFirstName().trim() + " " + lastBeneficiary.getLastName().trim());
	}
	
	private void adjustToPolicyCustomer(Customer lastCustomer, Customer inputCustomer, int objectId) {
		// adjust customer Last with Input
		if (lastCustomer != null && inputCustomer != null) {
			lastCustomer.setName(inputCustomer.getName());
			lastCustomer.setLastName(inputCustomer.getLastName());
			lastCustomer.setFirstName(inputCustomer.getFirstName());
			lastCustomer.setTradeName(inputCustomer.getTradeName());
			lastCustomer.setPersonType(inputCustomer.getPersonType());
			//lastCustomer.setGender(inputCustomer.getGender());
			//lastCustomer.setBirthDate(inputCustomer.getBirthDate());
			lastCustomer.setRegisterDate(inputCustomer.getRegisterDate());
			lastCustomer.setMaritalStatus(inputCustomer.getMaritalStatus());
			lastCustomer.setDocumentType(inputCustomer.getDocumentType());
			lastCustomer.setDocumentNumber(inputCustomer.getDocumentNumber());
			lastCustomer.setDocumentIssuer(inputCustomer.getDocumentIssuer());
			lastCustomer.setDocumentValidity(inputCustomer.getDocumentValidity());
			lastCustomer.setProfessionType(inputCustomer.getProfessionType());
			lastCustomer.setProfessionId(inputCustomer.getProfessionId());
			if (objectId != Item.OBJECT_ACCIDENTES_PERSONALES)
				lastCustomer.setOccupationId(inputCustomer.getOccupationId());
			lastCustomer.setActivityId(inputCustomer.getActivityId());
			lastCustomer.setInflowId(inputCustomer.getInflowId());
			lastCustomer.setEmailAddress(inputCustomer.getEmailAddress());
			
			// adjust addres Last with Input
			this.adjustToPolicyAddress(lastCustomer.getFirstAddress(), inputCustomer.getFirstAddress());
		}
	}

	private void adjustToPolicyAddress(Address lastAddress, Address inputAddress) {
		// adjust addres Last with Input
		if (lastAddress != null && inputAddress != null) {
			lastAddress.setAddressType(inputAddress.getAddressType());
			lastAddress.setAddressStreet(inputAddress.getAddressStreet());
			lastAddress.setDistrictName(inputAddress.getDistrictName());
			lastAddress.setCityId(inputAddress.getCityId());
			lastAddress.setCityName(inputAddress.getCityName());
			lastAddress.setRegionName(inputAddress.getRegionName());
			lastAddress.setStateId(inputAddress.getStateId());
			lastAddress.setStateName(inputAddress.getStateName());
			lastAddress.setCountryId(inputAddress.getCountryId());
			lastAddress.setCountryName(inputAddress.getCountryName());
			lastAddress.setZipCode(inputAddress.getZipCode());
			
			// clean phone Last address
			Iterator<Phone> iterator = lastAddress.getPhones().iterator();
			while (iterator.hasNext()) {
				iterator.next();
				iterator.remove();
			}
			// adjust phone Last with Input
			for (Phone inputPhone : inputAddress.getPhones() ) {
				this.adjustToPolicyPhone(lastAddress.addPhone(), inputPhone);
			}
		}
	}

	private void adjustToPolicyPhone(Phone lastPhone, Phone inputPhone) {
		lastPhone.setPhoneId(0);
		lastPhone.setPhoneType(inputPhone.getPhoneType());
		lastPhone.setPhoneNumber(inputPhone.getPhoneNumber());
		lastPhone.setActive(true);
	}

	private boolean validatePolicyRenewal(Endorsement endorsement) {
		Contract contract = endorsement.getContract();
		List<Contract> listContract = daoPolicy.listContractRenewed(contract.getPolicyNumber(), contract.getCertificateNumber());
		if (listContract != null && listContract.size() > 0) {
			endorsement.getErrorList().add(ErrorTypes.VALIDATION_POLICY_RENEWAL_INVALID, new Object[]{contract.getPolicyNumber().toString(), contract.getCertificateNumber().toString()}, BasicCalculationService.class, "", 0);
			return false;
		}
		return true;
	}

	public Endorsement policyIssuance(Endorsement endorsement, PaymentOption paymentOption) throws ServiceException {
		Contract contract = null;
		if (endorsement != null) {
			try {
				//check the issuance Type
				switch (endorsement.getIssuanceType()) {
					case Endorsement.ISSUANCE_TYPE_POLICY_EMISSION:
						// adjuste data
						this.adjustToPolicyIssuance(endorsement);
						// execute calculate
						this.calculateIssuance(endorsement, null);
						if (!(endorsement.getErrorList() != null && endorsement.getErrorList().getListaErros().size() > 0)) {
							PaymentOption newPaymentOption = this.fillPaymentOption(endorsement, paymentOption);
							// execute proposal
							this.proposalIssuance(endorsement, newPaymentOption);
						}
						break;
						
					case Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_REGISTER:
						// check if exists the contract
						contract = daoPolicy.getContractNumber(endorsement.getContract().getPolicyNumber(), endorsement.getContract().getCertificateNumber(), endorsement.getEffectiveDate());
						if (contract != null) {
							// check if exists the endorsement
							Endorsement lastEndorsement = this.lastValidEndorsementByEvict(contract.getContractId());
							if (lastEndorsement != null) {
								// adjuste data
								this.adjustToPolicyChangeRegister(lastEndorsement, endorsement);
								// execute change policy
								endorsement = this.policyChangeRegister(lastEndorsement, endorsement.getEffectiveDate());
							} else 
								throw new ServiceException(ErrorTypes.VALIDATION_POLICY_CHANGE_REGISTER_ERROR.getMessage(null));
						} else 
							throw new ServiceException(ErrorTypes.VALIDATION_POLICY_CHANGE_REGISTER_ERROR.getMessage(null));
						break;
						
					case Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_INSURED:
						// check if exists the contract
						contract = daoPolicy.getContractNumber(endorsement.getContract().getPolicyNumber(), endorsement.getContract().getCertificateNumber(), endorsement.getEffectiveDate());
						if (contract != null) {
							endorsement = this.policyCancellationByInsured(contract.getContractId(), endorsement.getCancelDate(), Endorsement.CANCEL_REASON_OTHERS);
						} else 
							throw new ServiceException(ErrorTypes.VALIDATION_POLICY_CANCEL_NOT_ALLOWED.getMessage(null));
						break;
						
					case Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR:
						// check if exists the contract
						contract = daoPolicy.getContractNumber(endorsement.getContract().getPolicyNumber(), endorsement.getContract().getCertificateNumber(), endorsement.getEffectiveDate());
						if (contract != null) {
							endorsement = this.policyCancellationByError(contract.getContractId(), endorsement.getCancelDate(), Endorsement.CANCEL_REASON_OTHERS);
						} else 
							throw new ServiceException(ErrorTypes.VALIDATION_POLICY_CANCEL_NOT_ALLOWED.getMessage(null));
						break;
				}
				logger.debug("Emisi�n del contrato: " + endorsement.getContract().getContractId());
			}
			catch (ServiceException e) {
				logger.error("Error durante la emisi�n del contrato externo", e);
			}
		}
		return endorsement;
	}

	private Endorsement adjustEndorsementToRenewal(Endorsement inputEndorsement) {
		if (inputEndorsement == null)
			return null;
		
		daoPolicy.evict(inputEndorsement);
		Contract inputContract = inputEndorsement.getContract();
		
		//:: Clone contract ::
		Contract workContract = new Contract();
		String[] ignorePropertiesContract = { "contractId", "parentId", "parent", "renewalId", "renewal", "renewed", 
											  "cancelDate", "effectiveDate", "expiryDate", "referenceDate", 
											  "endorsements" };
		BeanUtils.copyProperties(inputEndorsement.getContract(), workContract, ignorePropertiesContract);
		
		// Set contract key
		workContract.setContractId(0);
		
		// Set contract properties
		if (inputContract.getExpiryDate().compareTo(new Date()) <= 0)
			inputContract.setContractStatus(Contract.CONTRACT_STATUS_FINALIZED);
		
		workContract.setParentId(inputContract.getContractId());
		workContract.setRenewed(true);
		workContract.setEffectiveDate(DateUtilities.addDays(inputContract.getExpiryDate(), 1));
		workContract.setContractStatus(Contract.CONTRACT_STATUS_ACTIVE);
		
		//:: Clone endorsement ::
		Endorsement workEndorsement = new Endorsement();
		String[] ignorePropertiesEndorsement = { "endorsedId", "issuanceDate", "issuanceType", "termId", "paymentTermId",
												 "referenceDate", "effectiveDate", "expiryDate", "refusalDate", "cancelDate",
												 "quotationNumber", "quotationDate", "proposalNumber", "proposalDate", 
												 "endorsedNumber", "endorsementType", "endorsementNumber", "endorsementDate",  
												 "taxExemptionType", "transmitted", "issuingStatus", 
												 "changeLocked", "cancelReason", "claimNumber", "claimDate", "claimNotification", 
												 "items", "installments", "errorList", "renewalAlerts" };
		BeanUtils.copyProperties(inputEndorsement, workEndorsement, ignorePropertiesEndorsement);
		
		// Update endorsement key
		try {
			workEndorsement.setId((EndorsementId) inputEndorsement.getId().clone());
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		workEndorsement.getId().setEndorsementId(1);
		workEndorsement.updateKey(workContract);
		
		// Attach endorsement in contract
		workContract.getEndorsements().clear();
		workContract.getEndorsements().add(workEndorsement);
		
		// Set endorsement properties
		workEndorsement.setIssuanceType(Endorsement.ISSUANCE_TYPE_POLICY_EMISSION);
		workEndorsement.setTermId(inputContract.getTermId());
		workEndorsement.setEffectiveDate(workContract.getEffectiveDate());
		workEndorsement.setEndorsementType(Endorsement.ENDORSEMENT_TYPE_COLLECTION);
		workEndorsement.setTransmitted(false);
		workEndorsement.setQuotationDate(new Date());
		workEndorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_COTACAO);
		workEndorsement.cleanValueAndFactor();
		
		// Order the Item
		TreeSet<Item> treeItem = orderItem(inputEndorsement.getItems());
		
		//:: Clone item ::
		int itemId = 0;
		for (Item inputItem : treeItem) {
			if (inputItem.canBeRenewed()) {
				Item workItem = (Item)inputItem.clone();
				
				// Update item key
				workItem.getId().setItemId(++itemId);
				workEndorsement.attachItem(workItem);
				
				// Set item properties
				workItem.setRenewalType(Item.ITEM_RENEW_TYPE_RENEWAL);
				workItem.setRenewalInsurerId(workContract.getInsurerId());
				workItem.setRenewalPolicyNumber(workContract.getPolicyNumber().toString());
				workItem.setBonusType(null);
				workItem.setEffectiveDate(workEndorsement.getEffectiveDate());
				workItem.setCancelDate(null);
				workItem.setCalculationValid(false);
				workItem.setClaimNumber(null);
				workItem.setClaimDate(null);
				workItem.setClaimNotification(null);
				workItem.cleanValueAndFactor();
				
				// Set item properties object
				switch (workItem.getObjectId()) {
					case Item.OBJECT_VIDA:
					case Item.OBJECT_CREDITO:
					case Item.OBJECT_CICLO_VITAL:
					case Item.OBJECT_ACCIDENTES_PERSONALES:
					case Item.OBJECT_PURCHASE_PROTECTED:
						ItemPersonalRisk workItemPersonalRisk = (ItemPersonalRisk)workItem;
						workItemPersonalRisk.setAgeActuarial(null);
						
						// Restatementd the Insured risk value
						workItemPersonalRisk.setPersonalRiskValue(applyRestatement(Roadmap.ROADMAP_TYPE_RENEWAL, 
																				   Factor.FACTOR_TYPE_FIX, 
																				   workItem, 0, 
																				   workContract.getProductId(), 
																				   workEndorsement.getEffectiveDate(), 
																				   (workItemPersonalRisk.getPersonalRiskValue() != null ? workItemPersonalRisk.getPersonalRiskValue() : 0)));
						
						//:: Clear item risk group cannot be Renewed ::
						Iterator<ItemPersonalRiskGroup> iterator = workItemPersonalRisk.getItemPersonalRiskGroups().iterator();
						while (iterator.hasNext()) {
							ItemPersonalRiskGroup workItemRiskGroup = iterator.next();
							if (!workItemRiskGroup.canBeRenewed()) {
								iterator.remove();
							}
						}
						break;
						
					case Item.OBJECT_HABITAT:
					case Item.OBJECT_CAPITAL:
						ItemProperty workItemProperty = (ItemProperty)workItem;
						
						// Restatementd the Insured risk value
						if (workItem.getObjectId() == Item.OBJECT_HABITAT) {
							workItemProperty.setStructureValue(applyRestatement(Roadmap.ROADMAP_TYPE_RENEWAL, 
																				Factor.FACTOR_TYPE_FIX, 
																				workItem, 0, 
																				workContract.getProductId(), 
																				workEndorsement.getEffectiveDate(), 
																				(workItemProperty.getStructureValue() != null ? workItemProperty.getStructureValue() : 0)));
							
						}
						else {
							// Update risk values of types indicate
							for(ItemRiskType workItemRiskType : workItem.getItemRiskTypes()) {
								PlanRiskType planRiskType = daoProduct.getPlanRiskTypeById(workContract.getProductId(),
																						   workEndorsement.getRiskPlanId(),
																						   workItemRiskType.getId().getRiskType(),
																						   workEndorsement.getEffectiveDate());
								if (planRiskType != null && planRiskType.isRiskValueRestatement()) {
									workItemRiskType.setRiskTypeValue(applyRestatement(Roadmap.ROADMAP_TYPE_RENEWAL, 
																						Factor.FACTOR_TYPE_FIX, 
																						workItem, 0, 
																						workContract.getProductId(), 
																						workEndorsement.getEffectiveDate(), 
																						(workItemRiskType.getRiskTypeValue() != null ? workItemRiskType.getRiskTypeValue() : 0)));
								}
							}
						}
						break;
				}
				
				//:: Clone coverage ::
				for(ItemCoverage workItemCoverage : workItem.getItemCoverages()) {
					// Set coverage properties
					workItemCoverage.cleanValueAndFactor();
					workItemCoverage.limpaPremios();
					
					// Clean steps
					workItemCoverage.setCalcSteps(new HashSet<CalcStep>(0));
					
					// Restatementd the Insured Value
					if (workItemCoverage.getInsuredValue() != null) {
						workItemCoverage.setInsuredValue(applyRestatement(Roadmap.ROADMAP_TYPE_RENEWAL, 
																		   Factor.FACTOR_TYPE_FIX, 
																		   workItem, workItemCoverage.getId().getCoverageId(), 
																		   workContract.getProductId(), 
																		   workEndorsement.getEffectiveDate(), 
																		   workItemCoverage.getInsuredValue()));
					}
				}
			}
		}
		workEndorsement.flush();
		return workEndorsement;
	}
	
	/**
	 * Used to do the comparator y order item
	 */
	private Comparator<Item> comparatorItem = new Comparator<Item>() {
		/** {@inheritDoc} */
		@Override
		public int compare(Item arg0, Item arg1) {
			/* Regra b�sica: retorna -1 se menor, 0 se igual, 1 se maior */
			int result = arg0.getId().getItemId() - arg1.getId().getItemId();
			if (result != 0)
				return result;
			return result;
		}
	};
	
	/**
	 * Used to do the order item
	 */
	private TreeSet<Item> orderItem(Set<Item> itens) {
		TreeSet<Item> treeSet = null;
		treeSet = new TreeSet<Item>(comparatorItem);
		treeSet.addAll(itens);
		return treeSet;
	}	
	
	private boolean calculateRenewal(Endorsement endorsement, Endorsement parentEndorsement) throws ServiceException {
		try {
			Endorsement returnEndorsement = serviceBasicCalc.simpleCalc(endorsement, true);
			
			// Set renew alerts
			for (Erro erro: endorsement.getErrorList().getListaErros()) {
				endorsement.addRenewalAlert(erro);
			}
			
			// Check insured equals policy holder
			if (endorsement.getCustomerByInsuredId() == endorsement.getCustomerByPolicyHolderId()) {
				endorsement.setCustomerByInsuredId(daoPolicy.saveCustomer(endorsement.getCustomerByInsuredId()));
				endorsement.setCustomerByPolicyHolderId(endorsement.getCustomerByInsuredId());
			} else {
				endorsement.setCustomerByInsuredId(daoPolicy.saveCustomer(endorsement.getCustomerByInsuredId()));
				endorsement.setCustomerByPolicyHolderId(daoPolicy.saveCustomer(endorsement.getCustomerByPolicyHolderId()));
			}
			
			daoPolicy.saveEndorsement(endorsement);
			daoPolicy.saveRenewalAlert(endorsement.getRenewalAlerts());
			
			// Update contractId after save
			endorsement.getContract().setContractId(endorsement.getId().getContractId());
			daoPolicy.updateContract(endorsement.getContract());
			
			// Update renewed contractId after save
			if (parentEndorsement != null) {
				parentEndorsement.getContract().setRenewalId(endorsement.getId().getContractId());
				daoPolicy.updateContract(parentEndorsement.getContract());
			}
			
			for (Item returnItem : returnEndorsement.getItems()) {
				if (!returnItem.isCalculationValid()) {
					return false;
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private boolean calculateIssuance(Endorsement endorsement, Endorsement parentEndorsement) throws ServiceException {
		try {
			endorsement.setErrorList(new ErrorList());
			Endorsement returnEndorsement = serviceBasicCalc.simpleCalc(endorsement, true);
			
			if (!(endorsement.getErrorList() != null && endorsement.getErrorList().getListaErros().size() > 0)) {
				// Check insured equals policy holder
				if (endorsement.getCustomerByInsuredId() == endorsement.getCustomerByPolicyHolderId()) {
					endorsement.setCustomerByInsuredId(daoPolicy.saveCustomer(endorsement.getCustomerByInsuredId()));
					endorsement.setCustomerByPolicyHolderId(endorsement.getCustomerByInsuredId());
				} else {
					endorsement.setCustomerByInsuredId(daoPolicy.saveCustomer(endorsement.getCustomerByInsuredId()));
					endorsement.setCustomerByPolicyHolderId(daoPolicy.saveCustomer(endorsement.getCustomerByPolicyHolderId()));
				}
				
				daoPolicy.saveEndorsement(endorsement);
				
				// Update contractId after save
				endorsement.getContract().setContractId(endorsement.getId().getContractId());
				daoPolicy.updateContract(endorsement.getContract());
			}
			
			for (Item returnItem : returnEndorsement.getItems()) {
				if (!returnItem.isCalculationValid()) {
					return false;
				}
			}
		} catch (ServiceException e) {
			logger.error("calculateIssuance error",e);
			e.printStackTrace();
			return false;
		}
		return true;
	}	

	private Endorsement proposalRenewal(Endorsement endorsement, PaymentOption paymentOption) throws ServiceException {
		Endorsement returnEndorsement = endorsement;
		try {
			// Guard if error Renewal alerts
			boolean calcRenewalAlerts = (endorsement.getRenewalAlerts().size() > 0);
			endorsement.getRenewalAlerts().clear();
			endorsement.setErrorList(new ErrorList());
			
			returnEndorsement = serviceBasicCalc.effectiveProposal(endorsement, paymentOption);
			
			// Set renew alerts
			for (Erro erro: endorsement.getErrorList().getListaErros()) {
				endorsement.addRenewalAlert(erro);
			}
			
			if ((returnEndorsement != null) && (calcRenewalAlerts || endorsement.getRenewalAlerts().size() > 0 || returnEndorsement.getErrorList().getErrorCount() > 0)) {
				endorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_COTACAO);
			}
			
			daoPolicy.saveEndorsement(endorsement);
			daoPolicy.saveRenewalAlert(endorsement.getRenewalAlerts());
			
			if ((returnEndorsement != null) && (endorsement.getIssuingStatus() == Endorsement.ISSUING_STATUS_PROPOSTA)) {
				serviceCollection.dispachCollection(endorsement, false);
			}
		} catch (ServiceException e) {
			logger.error("proposalRenewal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			returnEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (JMSException e) {
			logger.error("proposalRenewal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			returnEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (NamingException e) {
			logger.error("proposalRenewal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			returnEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (FileNotFoundException e) {
			logger.error("proposalRenewal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			returnEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (IOException e) {
			logger.error("proposalRenewal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			returnEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (EInsuranceJMSException e) {
			logger.error("proposalRenewal error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			returnEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		}
		return returnEndorsement;
	}

	private Endorsement proposalIssuance(Endorsement endorsement, PaymentOption paymentOption) throws ServiceException {
		Endorsement returnEndorsement = endorsement;
		try {
			endorsement.setErrorList(new ErrorList());
			returnEndorsement = serviceBasicCalc.effectiveProposal(endorsement, paymentOption);
			
			if ((returnEndorsement != null) && (returnEndorsement.getErrorList().getErrorCount() > 0)) {
				endorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_COTACAO);
			}
			else {
				daoPolicy.saveEndorsement(endorsement);
				
				if ((returnEndorsement != null) && (endorsement.getIssuingStatus() == Endorsement.ISSUING_STATUS_PROPOSTA)) {
					serviceCollection.dispachCollection(endorsement, false);
				}
			}
		} catch (ServiceException e) {
			logger.error("proposalIssuance error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			returnEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (JMSException e) {
			logger.error("proposalIssuance error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			returnEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (NamingException e) {
			logger.error("proposalIssuance error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			returnEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (FileNotFoundException e) {
			logger.error("proposalIssuance error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			returnEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (IOException e) {
			logger.error("proposalIssuance error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			returnEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		} catch (EInsuranceJMSException e) {
			logger.error("proposalIssuance error",e);
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			for (StackTraceElement stackTraceElement : e.getStackTrace()) {
				sb.append(stackTraceElement.toString() + "\n");
			}
			returnEndorsement.getErrorList().add(ErrorTypes.PROCESSING_ERROR, null, e.getStackTrace()[0].getClass(), sb.toString(), 0);
		}
		return returnEndorsement;
	}

	private PaymentOption fillPaymentOption(Endorsement endorsement, int contractId) {
		Endorsement lastvalidEndorsement = daoPolicy.getLastValidEndosementWithPaymentOptions(contractId);
		Iterator<Installment> iterator = lastvalidEndorsement.getInstallments().iterator();
		Installment installment = iterator.next();
		
		PaymentOption paymentOption = serviceBasicCalc.getPaymentOptionByPaymentTermId(endorsement.getTotalPremium(), endorsement.getTaxRate(), endorsement.getContract().getProductId(), installment.getBillingMethodId(), lastvalidEndorsement.getPaymentTermId(), endorsement.getReferenceDate(), endorsement.getEffectiveDate(), endorsement.getExpiryDate());
		if (paymentOption != null) {
			paymentOption.setBankAccountNumber(installment.getBankAccountNumber());
			paymentOption.setBankAgencyNumber(installment.getBankAgencyNumber());
			paymentOption.setBankCheckNumber(installment.getBankCheckNumber());
			paymentOption.setBankNumber(installment.getBankNumber());
			paymentOption.setCardExpirationDate(installment.getCardExpirationDate());
			paymentOption.setCardHolderName(installment.getCardHolderName());
			paymentOption.setCardNumber(installment.getCardNumber());
			paymentOption.setInvoiceNumber(installment.getInvoiceNumber());
			paymentOption.setOtherAccountNumber(installment.getOtherAccountNumber());
		}
		return paymentOption;
	}
	
	private PaymentOption fillPaymentOption(Endorsement endorsement, PaymentOption inputPaymentOption) {
		PaymentOption paymentOption = serviceBasicCalc.getPaymentOptionByPaymentTermId(endorsement.getTotalPremium(), endorsement.getTaxRate(), endorsement.getContract().getProductId(), inputPaymentOption.getBillingMethodId(), inputPaymentOption.getPaymentId(), endorsement.getReferenceDate(), endorsement.getEffectiveDate(), endorsement.getExpiryDate());
		if (paymentOption != null) {
			paymentOption.setBankAccountNumber(inputPaymentOption.getBankAccountNumber());
			paymentOption.setBankAgencyNumber(inputPaymentOption.getBankAgencyNumber());
			paymentOption.setBankCheckNumber(inputPaymentOption.getBankCheckNumber());
			paymentOption.setBankNumber(inputPaymentOption.getBankNumber());
			paymentOption.setCardExpirationDate(inputPaymentOption.getCardExpirationDate());
			paymentOption.setCardHolderName(inputPaymentOption.getCardHolderName());
			paymentOption.setCardNumber(inputPaymentOption.getCardNumber());
			paymentOption.setInvoiceNumber(inputPaymentOption.getInvoiceNumber());
			paymentOption.setOtherAccountNumber(inputPaymentOption.getOtherAccountNumber());
		}
		return paymentOption;
	}
	
	public ErrorList listRenewalAlert(int contractId, int endorsementId, boolean resolved) {
		ErrorList errorList = new ErrorList();
		List<RenewalAlert> renewalAlerts = daoPolicy.listRenewalAlert(contractId, endorsementId, resolved); 
		if (renewalAlerts != null && renewalAlerts.size() > 0)
			for(RenewalAlert renewalAlert: renewalAlerts) {
				errorList.add(renewalAlert.getAlertCode(), 
							  renewalAlert.getDescription(), null, renewalAlert.getAlertType(), 
							  null, 
							  (renewalAlert.getItemId() != null ? renewalAlert.getItemId() : 0), 
							  (renewalAlert.getRiskGroupId() != null ? renewalAlert.getRiskGroupId() : 0), 
							  (renewalAlert.getCoverageId() != null ? renewalAlert.getCoverageId() : 0));
			}
		return errorList;
	}
	
	private double applyRestatement(int roadmapType, int factorType, Item item, int coverageId, int productId, Date refDate, double inputValue) {
		double oldValue = inputValue;
		double newValue = inputValue;
		
		// Get factor list for roadmap
		List<Factor> factorList = daoCalculo.getFactorList(productId, coverageId, roadmapType, factorType, refDate);
		
		// Get rate or value for factor type
		for (Factor factor : factorList) {
			Rate rate = daoCalculo.getRateByFator(factor, item, refDate);
			if (rate != null) {
				oldValue = newValue;
				if (rate.getRateFactor() != 0) {
					rate.setRateFactor(NumberUtilities.roundDecimalPlaces(rate.getRateFactor(), Calculus.RATE_DECIMAL_PLACES));
					newValue = NumberUtilities.roundDecimalPlaces(this.ApplyMathOperator(factor, oldValue, rate.getRateFactor()), 0);
				}
			}
		}
		return newValue;
	}

	private double ApplyMathOperator(Factor factor, double value1, double value2) {
		double retorno = 0d;
		
		switch (factor.getOperatorType()) {
			case Factor.OPERATOR_TYPE_MULTIPLICATION:
				retorno = (value1 * value2);
				break;
			case Factor.OPERATOR_TYPE_DIVISION:
				retorno = (value1 / value2);
				break;
			case Factor.OPERATOR_TYPE_ADDITION:
				retorno = (value1 + value2);
				break;
			case Factor.OPERATOR_TYPE_SUBTRACTION:
				retorno = (value1 - value2);
				break;
		}
		
		return retorno;
	}

	private void adjustPaymentOption(Endorsement endorsement, AdjustmentsToEndorsement adjustments) {
		if (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_REACTIVATION) {
			PaymentOption paymentOption = serviceBasicCalc.getPaymentOptionByPaymentTermId(endorsement.getTotalPremium(), endorsement.getTaxRate(), endorsement.getContract().getProductId(), adjustments.getPaymentOption().getBillingMethodId(), adjustments.getPaymentOption().getPaymentId(), endorsement.getReferenceDate(), endorsement.getEffectiveDate(), endorsement.getExpiryDate());
			if (paymentOption != null) {
				adjustments.getPaymentOption().setQuantityInstallment(paymentOption.getQuantityInstallment());
				adjustments.getPaymentOption().setFirstInstallmentValue(paymentOption.getFirstInstallmentValue());
				adjustments.getPaymentOption().setNextInstallmentValue(paymentOption.getNextInstallmentValue());
				adjustments.getPaymentOption().setNextPaymentDate(paymentOption.getNextPaymentDate());
				adjustments.getPaymentOption().setPaymentTermName(paymentOption.getPaymentTermName());
				adjustments.getPaymentOption().setFractioningAdditional(paymentOption.getFractioningAdditional());
				adjustments.getPaymentOption().setTotalPremium(paymentOption.getTotalPremium());
				adjustments.getPaymentOption().setTaxRate(paymentOption.getTaxRate());
				endorsement.setPaymentTermId(adjustments.getPaymentOption().getPaymentId());
			}
		}
	}
	
	public String getApplicationPropertyValue(int applicationId, String propertyName) {
		ApplicationProperty applicationProperty = daoPolicy.loadApplicationProperty(applicationId, propertyName);
		if (applicationProperty != null)
			return applicationProperty.getValue();
		else return null;
	}
	
	public IdentifiedList getApplicationPropertyValue(IdentifiedList identifiedList, int applicationId, String propertyName) {
		identifiedList.setValue(daoPolicy.loadApplicationProperty(applicationId, propertyName));
		return identifiedList;
	}

	@Override
	public List<Profession> listProfessionByRefDate(Date refDate) {
		return daoPolicy.listProfessionByRefDate(refDate);
	}

	@Override
	public List<Occupation> listOccupationByRefDate(Date refDate) {
		return daoPolicy.listOccupationByRefDate(refDate);
	}

	@Override
	public List<Activity> listActivityByRefDate(Date refDate) {
		return daoPolicy.listActivityByRefDate(refDate);
	}

	@Override
	public List<Inflow> listInflowByRefDate(Date refDate) {
		return daoPolicy.listInflowByRefDate(refDate);
	}
}