package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.dao.IDaoBroker;
import br.com.tratomais.core.infrastructure.AuthenticationHelper;
import br.com.tratomais.core.model.customer.Broker;
import br.com.tratomais.core.model.customer.Insurer;
import br.com.tratomais.core.service.IServiceBroker;
import br.com.tratomais.core.service.IServiceInsurer;
import br.com.tratomais.core.util.Constantes;

public class ServiceBrokerImpl extends ServiceBean implements IServiceBroker {

	private IDaoBroker daoBroker;

	private IServiceInsurer serviceInsurer;

	public void setServiceInsurer(IServiceInsurer serviceInsurer) {
		this.serviceInsurer = serviceInsurer;
	}

	public void setDaoBroker(IDaoBroker daoBroker) {
		this.daoBroker = daoBroker;
	}

	public List<Broker> findByName(String name) {
		return daoBroker.findByName(name);
	}

	public List<Broker> findByNickName(String userName) {
		return daoBroker.findByNickName(userName);
	}

	public Broker findById(Integer id) {
		return daoBroker.findById(id);
	}

	public List<Broker> listAll() {
		return daoBroker.listAll();
	}
	
	public List<Broker> listAllActiveBroker() {
		return daoBroker.listAllActive();
	}

	public void deleteObject(Broker entity) {
		daoBroker.delete(entity);

	}

	public Broker saveObject(Broker broker) {
		if (broker.getInsurerId() == 0) {
			Insurer insurer = serviceInsurer.findById(Constantes.ID_INSURER_ZURICH);
			broker.setInsurerId( insurer.getInsurerId());
		}
		return daoBroker.save(broker);
	}

	public boolean brokerExternalCodeExists(int insurerId, int brokerId, String externalCode) {
		return daoBroker.brokerExternalCodeExists(insurerId, brokerId, externalCode);
	}

	/**
     * list all Broker from the user
     * @param partnerId
     * @param productId
     * @param brokerId
     * @return List<Broker>
     */
	public List<Broker> listBrokerPerProductOrPartner(Integer partnerId, Integer productId, Integer brokerId){
		if(partnerId == 0 || partnerId == 99999)partnerId = null;
		if(productId == 0 || productId == 99999)productId = null;
		if(brokerId == 0 || brokerId == 99999)brokerId = null;
		
		return daoBroker.listBrokerPerProductOrPartner(partnerId, productId, brokerId,  AuthenticationHelper.getLoggedUser().getUserId());
	}

	@Override
	public List<Broker> listBroker(int insurerID, String regulatoryCode, Date refDate) {
		return daoBroker.listBroker(insurerID, regulatoryCode, refDate);
	}
}
