package br.com.tratomais.core.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import br.com.tratomais.core.dao.IDaoPlan;
import br.com.tratomais.core.model.product.Plan;
import br.com.tratomais.core.service.IServicePlan;

public class ServicePlanImpl extends ServiceBean implements IServicePlan {
	private static final Logger logger = Logger.getLogger( ServicePlanImpl.class );

	public IDaoPlan daoPlan;

	public void setDaoPlan(IDaoPlan daoPlan) {
		this.daoPlan = daoPlan;
	}

	@Override
	public void deleteObject(Plan entity) {
		daoPlan.delete(entity);
	}

	@Override
	public Plan saveObject(Plan entity) {
		return daoPlan.save(entity);
	}

	@Override
	public Plan findById(Integer id) {
		return daoPlan.findById(id);
	}

	@Override
	public List<Plan> findByName(String name) {
		return daoPlan.findByName(name);
	}

	@Override
	public List<Plan> listAll() {
		return daoPlan.listAll();
	}

}
