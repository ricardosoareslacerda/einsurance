package br.com.tratomais.core.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.BeanUtils;

import br.com.tratomais.core.dao.IDaoModule;
import br.com.tratomais.core.infrastructure.AuthenticationHelper;
import br.com.tratomais.core.model.Module;
import br.com.tratomais.core.model.Role;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.service.IServiceModule;

public class ServiceModuleImpl extends ServiceBean implements IServiceModule {

	private IDaoModule daoModule;

	public void setDaoModule(IDaoModule daoModule) {
		this.daoModule = daoModule;
	}

	public List<Module> findByName(String name) {
		return daoModule.findByName(name);
	}

	public List<Module> listAll() {
		return daoModule.listAll();
	}

	public Module findById(Integer id) {
		return this.daoModule.findById(id);
	}

	public List<Module> listAllByUser(User user) {
		return this.listAll();
	}

	/**
	 * List modules allowed
	 * 
	 * @return Modulos que lista os modulos
	 * @return modulos do user logado
	 * @throws Exception 
	 **/
	public List<Module> listAllowedModulesByUserLogged(){
		User loggedUser = AuthenticationHelper.getLoggedUser();
		Set<Module> allowedModules = new HashSet<Module>();
		Set<Module> modulesReturn = new HashSet<Module>();
		
		for (Role role : loggedUser.getRoles()) {
			allowedModules.addAll( role.getModules() );
		}

		for (Module module : allowedModules) {
			if ( module.getModule() == null) {
				Module workModule = new Module();
				BeanUtils.copyProperties( module, workModule );
				excludeNotAllowedModules( module, allowedModules);
				modulesReturn.add( module );
			}
		}
		
		
//		for (Role role : loggedUser.getRoles()) {
//			this.populateModulesList(role.getModules(), modulesReturn);
//		}

		List<Module> modulesListReturn = this.getListModules(modulesReturn);
		this.sortModules(modulesListReturn);
		return modulesListReturn;
	}

	private void excludeNotAllowedModules(Module module, Set< Module > allowedModules) {
		List<Module> workList = new ArrayList< Module >();
		workList.addAll( module.getModules());
		for ( Module workModule : workList ) {
			if ( !allowedModules.contains( workModule )) {
				module.getModules().remove( workModule );
			}else{
				excludeNotAllowedModules(workModule, allowedModules);
			}				
		}
	}

	/**
	 * @param modulesReturn
	 **/
	private List<Module> getListModules(Set<Module> modulesReturn) {
		List<Module> modules = new ArrayList<Module>();
		for (Module module : modulesReturn) {
			modules.add(module);
		}
		return modules;
	}

	/**
	 * @param modulesReturn
	 **/
	private void sortModules(List<Module> modulesReturn) {
		Collections.sort(modulesReturn);
		for (Module module : modulesReturn) {
			if (module.getModules() != null && module.getModules().size() > 0) {
				this.sortModules(this.getListModules(module.getModules()));
			}
		}
	}

	/**
	 * @author leo.costa Metodo responsavel por distinguir os modulos de roles diferentes
	 * @param modules
	 *            Modulos que serao verificados se j� foram inseridos na lista de retorno por outros role
	 * @param modulesReturn
	 *            todos os modulos distintos que formarao o menu
	 */
	@SuppressWarnings("unused")
	private void populateModulesList(Set<Module> modules, Set<Module> modulesReturn) {
		for (Module module : modules) {
			if (module.isActive()) {
				if (module.getModule() == null) {
					modulesReturn.add(module);
//					boolean add = 
//					if (add == false) {
//						modulesReturn.add(module);
//						this.populateModulesList(modules, modulesReturn);
//					}
				}
			}
		}
	}
}
