package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.dao.IDaoBranch;
import br.com.tratomais.core.model.product.Branch;
import br.com.tratomais.core.service.IServiceBranch;

public class ServiceBranchImpl extends ServiceBean implements IServiceBranch{
	
	private IDaoBranch daoBranch;
	
	public void setDaoBranch(IDaoBranch daoBranch){
		this.daoBranch = daoBranch;
	}

	@Override
	public void delete(Branch entity) {
		daoBranch.delete(entity);
	}

	@Override
	public Branch findById(Integer id) {
		return daoBranch.findById(id);
	}

	@Override
	public List<Branch> listAll() {
		return daoBranch.listAll();
	}

	@Override
	public List<Branch> listAllActive() {
		return daoBranch.listAllActive();
	}

	@Override
	public Branch save(Branch entity) {
		return daoBranch.save(entity);
	}

	@Override
	public List<Branch> listBranch(Date refDate) {
		return daoBranch.listBranch(refDate);
	}

	@Override
	public Branch getBranch(int branchID, Date refDate) {
		return daoBranch.getBranch(branchID, refDate);
	}
}
