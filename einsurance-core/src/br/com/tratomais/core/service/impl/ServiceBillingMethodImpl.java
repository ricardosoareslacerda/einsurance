package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.dao.IDaoBillingMethod;
import br.com.tratomais.core.model.product.BillingAgency;
import br.com.tratomais.core.model.product.BillingMethod;
import br.com.tratomais.core.service.IServiceBillingMethod;

public class ServiceBillingMethodImpl extends ServiceBean implements IServiceBillingMethod{
	
	private IDaoBillingMethod daoBillingMethod;
	
	public void setDaoBillingMethod(IDaoBillingMethod daoBillingMethod){
		this.daoBillingMethod = daoBillingMethod;
	}

	@Override
	public List<BillingMethod> listBillingMethod(int productID, Date versionDate) {
		return daoBillingMethod.listBillingMethod(productID, versionDate);
	}

	@Override
	public void delete(BillingMethod entity) {
		daoBillingMethod.delete(entity);
	}

	@Override
	public BillingMethod findById(Integer id) {
		return daoBillingMethod.findById(id);
	}

	@Override
	public List<BillingMethod> listAll() {
		return daoBillingMethod.listAll();
	}

	@Override
	public List<BillingMethod> listAllActive() {
		return daoBillingMethod.listAllActive();
	}

	@Override
	public BillingMethod save(BillingMethod entity) {
		return daoBillingMethod.save(entity);
	}

	@Override
	public BillingAgency getBillingAgencyById(int billingAgencyID, Date refDate) {
		return daoBillingMethod.getBillingAgencyById(billingAgencyID, refDate);
	}
}
