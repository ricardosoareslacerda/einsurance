package br.com.tratomais.core.service.impl;

import java.util.ArrayList;
import java.util.Collections;
//import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
//import java.util.TreeSet;

import org.springframework.beans.BeanUtils;

import br.com.tratomais.core.dao.IDaoFunctionality;
import br.com.tratomais.core.infrastructure.AuthenticationHelper;
import br.com.tratomais.core.model.Functionality;
import br.com.tratomais.core.model.Module;
import br.com.tratomais.core.model.Role;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.service.IServiceFunctionality;

public class ServiceFunctionalityImpl  extends ServiceBean implements IServiceFunctionality {

	private IDaoFunctionality daoFunctionality;
	
	public void setDaoFunctionality(IDaoFunctionality daoFunctionality){
		this.daoFunctionality = daoFunctionality;
	}
	
	@Override
	public Functionality findById(Integer id) {
		return daoFunctionality.findById(id);
	}

	@Override
	public List<Functionality> findByName(String name) {
		return daoFunctionality.findByName(name);
	}

	@Override
	public List<Functionality> listAll() {
		return daoFunctionality.listAll();
	}

	@Override
	public List<Functionality> listAllowedFunctionalityByUserLogged() {
		User loggedUser = AuthenticationHelper.getLoggedUser();
		Set<Functionality> allowedFunctionality = new HashSet<Functionality>();
		Set<Functionality> functionalitysReturn = new HashSet<Functionality>();
		Set<Module> allowedModule = new HashSet<Module>();
		
		for (Role role : loggedUser.getRoles()) {
			allowedModule.addAll(role.getModules());
			allowedFunctionality.addAll( role.getFunctionalities() );
		}
		
		for (Module module : allowedModule) {
			if(allowedFunctionality.contains(module.getFunctionality())){
				allowedFunctionality.remove(module.getFunctionality());
			}
		}
		
		for (Functionality functionality : allowedFunctionality) {
			if ( functionality.getFunctionality() == null) {
				Functionality workFunctionality = new Functionality();
				BeanUtils.copyProperties( functionality, workFunctionality );
				excludeNotAllowedFunctionalitys( functionality, allowedFunctionality);
				functionalitysReturn.add( functionality );
			}
		}
		
		List<Functionality> functionalitysListReturn = this.getListFunctionalitys(functionalitysReturn);
		this.sortFunctionalitys(functionalitysListReturn);
		return functionalitysListReturn;
	}

	private void excludeNotAllowedFunctionalitys(Functionality functionality, Set< Functionality > allowedFunctionalitys) {
		List<Functionality> workList = new ArrayList< Functionality >();
		workList.addAll( functionality.getFunctionalities());
		for ( Functionality workFunctionality : workList ) {
			if ( !allowedFunctionalitys.contains( workFunctionality )) {
				functionality.getFunctionalities().remove( workFunctionality );
			}else{
				excludeNotAllowedFunctionalitys(workFunctionality, allowedFunctionalitys);
			}				
		}
	}

	/**
	 * @param FunctionalitysReturn
	 **/
	private List<Functionality> getListFunctionalitys(Set<Functionality> functionalitysReturn) {
		List<Functionality> functionalitys = new ArrayList<Functionality>();
		for (Functionality functionality : functionalitysReturn) {
			functionalitys.add(functionality);
		}
		return functionalitys;
	}

	/**
	 * @param modulesReturn
	 **/
	private void sortFunctionalitys(List<Functionality> functionalitysReturn) {
		Collections.sort(functionalitysReturn);
		for (Functionality functionality : functionalitysReturn) {
			if (functionality.getFunctionalities() != null && functionality.getFunctionalities().size() > 0) {
				this.sortFunctionalitys(this.getListFunctionalitys(functionality.getFunctionalities()));
			}
		}
	}
}
