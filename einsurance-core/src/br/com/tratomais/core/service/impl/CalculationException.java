package br.com.tratomais.core.service.impl;

public class CalculationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8216921288985994428L;

	public CalculationException() {
		super();
	}

	public CalculationException(String message, Throwable cause) {
		super(message, cause);
	}

	public CalculationException(String message) {
		super(message);
	}

	public CalculationException(Throwable cause) {
		super(cause);
	}

}
