package br.com.tratomais.core.service.impl;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.security.userdetails.UserDetails;
import org.springframework.security.userdetails.UserDetailsService;
import org.springframework.security.userdetails.UsernameNotFoundException;

import br.com.tratomais.core.dao.IDaoDomain;
import br.com.tratomais.core.dao.IDaoLogin;
import br.com.tratomais.core.dao.IDaoUser;
import br.com.tratomais.core.model.Domain;
import br.com.tratomais.core.model.Login;
import br.com.tratomais.core.model.User;

public class ServiceProfileImpl extends ServiceBean implements UserDetailsService {
	private static final Logger logger = Logger.getLogger( ServiceProfileImpl.class );

	private IDaoUser daoUser;
	private IDaoLogin daoLogin;
	private IDaoDomain daoDomain;

	public void setDaoUser(IDaoUser daoUser) {
		this.daoUser = daoUser;
	}

	public void setDaoLogin(IDaoLogin daoLogin) {
		this.daoLogin = daoLogin;
	}

	public void setDaoDomain(IDaoDomain daoDomain) {
		this.daoDomain = daoDomain;
	}

	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException, DataAccessException {
		try {
			String domainName = Arrays.asList(userName.split("@")).get(1).toString();
			String loginName = Arrays.asList(userName.split("@")).get(0).toString();
			Domain domain = daoDomain.findDomainByName(domainName);
			Login login = daoLogin.findByLogin(loginName, domain.getName());
			User user = daoUser.findByLogin(loginName, domain);
			user.setPassword(login.getPasswordKey());
			return user;
		} catch (Exception e) {
			throw new UsernameNotFoundException("User : " + userName + " not found");
		}
	}

	
}
