/**
 * 
 */
package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.tratomais.core.dao.IDaoCoveragePlan;
import br.com.tratomais.core.model.product.CoveragePlan;
import br.com.tratomais.core.service.IServiceCoveragePlan;

/**
 * @author eduardo.venancio
 */
public class ServiceCoveragePlanImpl extends ServiceBean implements IServiceCoveragePlan {
	private static final Logger logger = Logger.getLogger( ServiceCoveragePlanImpl.class );

	public IDaoCoveragePlan daoCoveragePlan;

	/* (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceCoveragePlan#setDaoCoveragePlan(br.com.tratomais.core.dao.IDaoCoveragePlan)
	 */
	public void setDaoCoveragePlan(IDaoCoveragePlan daoCoveragePlan) {
		this.daoCoveragePlan = daoCoveragePlan;
	}

	/* (non-Javadoc)
	 * @see br.com.tratomais.core.service.IFindEntity#findById(java.lang.Object)
	 */
	@Override
	public CoveragePlan findById(Integer id) {
		return daoCoveragePlan.findById(id);
	}

	/* (non-Javadoc)
	 * @see br.com.tratomais.core.service.IFindEntity#findByName(java.lang.String)
	 */
	@Override
	public List<CoveragePlan> findByName(String name) {
		return daoCoveragePlan.findByName(name);
	}

	/* (non-Javadoc)
	 * @see br.com.tratomais.core.service.IFindEntity#listAll()
	 */
	@Override
	public List<CoveragePlan> listAll() {
		return daoCoveragePlan.listAll();
	}

	/* (non-Javadoc)
	 * @see br.com.tratomais.core.service.ICrudEntity#deleteObject(br.com.tratomais.core.dao.PersistentEntity)
	 */
	@Override
	public void deleteObject(CoveragePlan entity) {
		daoCoveragePlan.delete(entity);
	}

	/* (non-Javadoc)
	 * @see br.com.tratomais.core.service.ICrudEntity#saveObject(br.com.tratomais.core.dao.PersistentEntity)
	 */
	@Override
	public CoveragePlan saveObject(CoveragePlan entity) {
		return daoCoveragePlan.save(entity);
	}

	@Override
	public List<CoveragePlan> listCoveragePlan(int productID, Date versionDate) {
		return daoCoveragePlan.listCoveragePlan(productID, versionDate);
	}

	@Override
	public CoveragePlan getCoveragePlanByRefDate(int productID, int planID,	int coverageID, Date versionDate) {
		return daoCoveragePlan.getCoveragePlanByRefDate(productID, planID, coverageID, versionDate);
	}

}
