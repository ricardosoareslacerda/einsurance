package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

//import org.apache.log4j.Logger;

import br.com.tratomais.core.dao.IDaoInsurer;
import br.com.tratomais.core.model.customer.Insurer;
import br.com.tratomais.core.service.IServiceInsurer;

public class ServiceInsurerImpl extends ServiceBean implements IServiceInsurer{
	//private static final Logger logger = Logger.getLogger( ServiceInsurerImpl.class );

	private IDaoInsurer daoInsurer;

	public void setDaoInsurer(IDaoInsurer daoInsurer) {
		this.daoInsurer = daoInsurer;
	}
	
	public Insurer findById(Integer id) {
		return daoInsurer.findById( id );
	}

	public List< Insurer > findByName(String name) {		
		return daoInsurer.findByName( name );
	}

	public List< Insurer > listAll() {
		return daoInsurer.listAll() ;
	}

	@Override
	public List<Insurer> listInsurerByRefDate(Date refDate) {
		return daoInsurer.listInsurerByRefDate(refDate);
	}

}
