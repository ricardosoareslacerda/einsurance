package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

//import org.apache.log4j.Logger;

import br.com.tratomais.core.dao.IDaoPersonalRiskOption;
import br.com.tratomais.core.model.product.PersonalRiskOption;
import br.com.tratomais.core.service.IServicePersonalRiskOption;

public class ServicePersonalRiskOption extends ServiceBean implements IServicePersonalRiskOption{
	//private static final Logger logger = Logger.getLogger( ServicePersonalRiskOption.class );

	private IDaoPersonalRiskOption daoPersonalRiskOption;

	public void setServicePersonalRiskOption(IDaoPersonalRiskOption daoPersonalRiskOption) {
		this.daoPersonalRiskOption = daoPersonalRiskOption;
	}	
	
	public List<PersonalRiskOption> findPersonalRiskOptions(int productId, int riskPlanId, Date referenceDate) {
		return daoPersonalRiskOption.findPersonalRiskOptions(productId, riskPlanId, referenceDate);
	}
	
	public List<PersonalRiskOption> findPersonalRiskOptions() {
		return daoPersonalRiskOption.findPersonalRiskOptions();
	}
}
