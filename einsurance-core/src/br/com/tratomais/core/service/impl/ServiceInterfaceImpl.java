package br.com.tratomais.core.service.impl;

import java.util.Date;

import br.com.tratomais.core.dao.IDaoDataOption;
import br.com.tratomais.core.dao.IDaoExchange;
import br.com.tratomais.core.dao.IDaoFileLot;
import br.com.tratomais.core.dao.IDaoFileLotDetails;
import br.com.tratomais.core.dao.IDaoLot;
import br.com.tratomais.core.model.interfaces.FileLot;
import br.com.tratomais.core.model.interfaces.FileLotDetails;
import br.com.tratomais.core.model.interfaces.Lot;
import br.com.tratomais.core.model.product.DataGroup;
import br.com.tratomais.core.model.product.IdentifiedList;
import br.com.tratomais.core.model.report.DataViewInterface;
import br.com.tratomais.core.service.IServiceInterface;

public class ServiceInterfaceImpl extends ServiceBean implements IServiceInterface{
	
	private IDaoLot daoLot;
	private IDaoFileLot daoFileLot;
	private IDaoFileLotDetails daoFileLotDetails;
	private IDaoDataOption daoDataOption;
	private IDaoExchange daoExchange;
	
	public void setDaoLot(IDaoLot daoLot){
		this.daoLot = daoLot;
	}
	
	public void setDaoFileLot(IDaoFileLot daoFileLot){
		this.daoFileLot = daoFileLot;
	}
	
	public void setDaoFileLotDetails(IDaoFileLotDetails daoFileLotDetails){
		this.daoFileLotDetails = daoFileLotDetails;
	}
	
	public void setDaoDataOption(IDaoDataOption daoDataOption){
		this.daoDataOption = daoDataOption;
	}
	
	public void setDaoExchange(IDaoExchange daoExchange){
		this.daoExchange = daoExchange;
	}

	@Override
	public Lot getLot(Integer lotId) {
		return daoLot.getLot(lotId);
	}

	@Override
	public Lot getLot(Integer lotId, Integer parentId, Integer exchangeId, Integer insurerId, Integer partnerId) {
		return daoLot.getLot(lotId, parentId, exchangeId, insurerId, partnerId);
	}


	@Override
	public IdentifiedList listLot(IdentifiedList identifiedList, Integer exchangeId, Integer lotStatus, Date rangeBefore, Date rangeAfter) {
		return daoLot.listLot(identifiedList, exchangeId, lotStatus, rangeBefore, rangeAfter);
	}

	@Override
	public FileLot getFileLot(Integer fileLotId, Integer lotId) {
		return daoFileLot.getFileLot(fileLotId, lotId);
	}

	@Override
	public FileLotDetails getFileLotDetails(Integer fileLotDetailsId, Integer fileLotId) {
		return daoFileLotDetails.getFileLotDetails(fileLotDetailsId, fileLotId);
	}

	@Override
	public IdentifiedList listFileLot(IdentifiedList identifiedList, Integer lotId) {
		return daoFileLot.listFileLot(identifiedList, lotId);
	}

	@Override
	public IdentifiedList listFileLotDetails(IdentifiedList identifiedList,	Integer fileLotId) {
		return daoFileLotDetails.listFileLotDetails(identifiedList, fileLotId);
	}

	@Override
	public DataViewInterface getDataViewInterface() {
		DataViewInterface dvi = new DataViewInterface();
		
		dvi.setListExchangeType(daoDataOption.listDataOption(DataGroup.GROUP_EXCHANGE_TYPE));
		dvi.setListExchange(daoExchange.listExchange());
		dvi.setListLotStatus(daoDataOption.listDataOption(DataGroup.GROUP_LOT_STATUS));
		
		return dvi;
	}
}
