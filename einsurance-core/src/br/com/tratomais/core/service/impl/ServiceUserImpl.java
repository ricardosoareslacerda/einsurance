package br.com.tratomais.core.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import br.com.tratomais.core.dao.IDaoUser;
import br.com.tratomais.core.infrastructure.AuthenticationHelper;
import br.com.tratomais.core.model.Login;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.UserPartner;
import br.com.tratomais.core.model.UserPartnerId;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Partner;
import br.com.tratomais.core.service.IServiceLogin;
import br.com.tratomais.core.service.IServiceUser;
import br.com.tratomais.core.util.TransportationClass;

public final class ServiceUserImpl extends ServiceBean implements IServiceUser {
	private static final Logger logger = Logger.getLogger( ServiceUserImpl.class );

	private IDaoUser daoUser;
	private IServiceLogin serviceLogin;

	public void setDaoUser(IDaoUser daoUser) {
		this.daoUser = daoUser;
	}

	public void setServiceLogin(IServiceLogin serviceLogin) {
		this.serviceLogin = serviceLogin;
	}

	public User loadUserByLogin(String userName) {
		return daoUser.findByLogin(userName);
	}

	public User saveObject(User user, Login login) {
		if (login != null) {
			user.setLogin(login.getLogin());
			login.setDomain(user.getDomain().getName());
			serviceLogin.saveObject(login);
		}

		this.setUserPartner(user, user.getUserPartners());
		return daoUser.save(user);
	}

	public User saveObjectInsert(User user, Login login) {
		Login loginSaved = serviceLogin.saveObject(login);
		if (loginSaved != null) {
			user.setLogin(login.getLogin());

			Set<UserPartner> userPartnerNew = user.getUserPartners();
			user.setUserPartners(new HashSet<UserPartner>());
			User newUser = daoUser.save(user);
			this.setUserPartner(newUser, userPartnerNew);
			return daoUser.save(newUser);
		}
		return null;
	}

	public void changePartnerDefaultLoggedUser(int partnerId) {
		AuthenticationHelper.setSelectedPartnerInLoggedUser(partnerId);
	}

	public List<User> listUserAllowed() {
		List<Integer> partnersId = new ArrayList<Integer>();
		List<User> listUserAllowed = new ArrayList<User>();
		User loggedUser = AuthenticationHelper.getLoggedUser();
		for (UserPartner userPartner : loggedUser.getUserPartners()) {
			if (userPartner.isAdministrator()) {
				partnersId.add(userPartner.getId().getPartnerId());
			}
		}
		if (partnersId.isEmpty()) {
			listUserAllowed.add(loggedUser);
		} else {
			listUserAllowed = daoUser.listUserAllowed(partnersId, loggedUser.isAdministrator());
		}
		return listUserAllowed;
	}

	public List<User> listUserByPartnerId(int partnerId) {
		User loggedUser = AuthenticationHelper.getLoggedUser();
		List<Integer> partnersId = new ArrayList<Integer>();
		List<User> listUserAllowed = new ArrayList<User>();
		partnersId.add(partnerId);
		if (partnersId.isEmpty()) {
			listUserAllowed.add(loggedUser);
		} else {
			listUserAllowed = daoUser.listUserAllowed(partnersId, loggedUser.isAdministrator());
		}
		return listUserAllowed;
	}

	public Set<User> fillListUserByChannel(Channel channel) {
		Set<User> usersChannel = new HashSet<User>();
		List<User> usersPartner = this.listUserByPartnerId(channel.getPartnerId());
		for (User user : usersPartner) {
			for (User userChannel : channel.getUsers()) {
				if (user.getUserId() == userChannel.getUserId()) {
					user.setTransientSelected(true);
				}
			}
			usersChannel.add(user);
		}
		return usersChannel;
	}

	public User findById(Integer id) {
		return daoUser.findById(id);
	}

	public List<User> findByName(String name) {
		return daoUser.findByName(name);
	}

	public void deleteObject(User user) {
		user.setActive(false);
		daoUser.delete(user);
		serviceLogin.deleteObjectByLoginName(user.getLogin(), user.getDomain().getName());
	}

	private void setUserPartner(User user, Set<UserPartner> userPartners) {
		for (UserPartner userPartner : userPartners) {
			Partner partner = userPartner.getPartner();
			userPartner.setAdministrator(partner.isTransientAdministrator());
			userPartner.setAllChannel(partner.isTransientAllChannel());
			userPartner.setPartnerDefault(partner.isTransientIsDefault());
			UserPartnerId userPartnerId = new UserPartnerId();
			userPartnerId.setPartnerId(partner.getPartnerId());
			userPartnerId.setUserId(user.getUserId());
			userPartner.setId(userPartnerId);
			userPartner.setUser(user);
			userPartner.setPartner(partner);
		}
		user.setUserPartners(userPartners);
	}

	@Deprecated
	public List<User> listAll() {
		return daoUser.listAll();
	}

	@Deprecated
	public User saveObject(User entity) {
		return daoUser.save(entity);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceUser#listUserPerPartner(int)
	 */
	public List<User> listUserPerPartner(int partnerId){
		return daoUser.listUserPerPartner(partnerId);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceUser#listUserToGrid(int, int, String, String, String, String, String)
	 */
	public TransportationClass<User> listUserToGrid(int limit, int page, String columnSorted, String typeSorted,
													String search, String searchField, String searchString){
		List<Integer> partnersId = new ArrayList<Integer>();
		TransportationClass<User> listUserAllowed = new TransportationClass<User>();
		User loggedUser = AuthenticationHelper.getLoggedUser();
		
		for (UserPartner userPartner : loggedUser.getUserPartners()) {
			if (userPartner.isAdministrator()) {
				partnersId.add(userPartner.getId().getPartnerId());
			}
		}
		
		if (partnersId.isEmpty()) {
			listUserAllowed = null;
		} else {
			listUserAllowed = daoUser.listUserToGrid(limit, page, columnSorted, typeSorted, search, searchField, searchString, partnersId, loggedUser.isAdministrator());
		}
		return listUserAllowed;		
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.service.IServiceUser#listUserAllActivePartnerToGrid(int, int, int)
	 */
	public TransportationClass<User> listUserAllActivePartnerToGrid(int limit, int page, int userId){
		return daoUser.listUserAllActivePartnerToGrid(limit, page, userId);
	}

	@Override
	public List<User> listUser(int insurerID, int partnerID, int channelID,	Date refDate) {
		return daoUser.listUser(insurerID, partnerID, channelID, refDate);
	}
}