package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.dao.IDaoCurrency;
import br.com.tratomais.core.model.product.Currency;
import br.com.tratomais.core.service.IServiceCurrency;

public class ServiceCurrencyImpl extends ServiceBean implements IServiceCurrency{
	
	private IDaoCurrency daoCurrency;
	
	public void setDaoCurrency(IDaoCurrency daoCurrency){
		this.daoCurrency = daoCurrency;
	}

	@Override
	public void delete(Currency entity) {
		daoCurrency.delete(entity);
	}

	@Override
	public Currency findById(Integer id) {
		return daoCurrency.findById(id);
	}

	@Override
	public List<Currency> listAll() {
		return daoCurrency.listAll();
	}

	@Override
	public List<Currency> listAllActive() {
		return daoCurrency.listAllActive();
	}

	@Override
	public Currency save(Currency entity) {
		return daoCurrency.save(entity);
	}
	
	@Override
	public List<Currency> listCurrency(boolean indexer, Date refDate) {
		return daoCurrency.listCurrency(indexer, refDate);
	}

	@Override
	public Currency getCurrency(int currencyID, Date refDate) {
		return daoCurrency.getCurrency(currencyID, refDate);
	}
}
