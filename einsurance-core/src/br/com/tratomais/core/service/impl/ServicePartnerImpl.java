package br.com.tratomais.core.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import br.com.tratomais.core.dao.IDaoPartner;
import br.com.tratomais.core.dao.IDaoUser;
import br.com.tratomais.core.dao.IDaoUserPartner;
import br.com.tratomais.core.infrastructure.AuthenticationHelper;
import br.com.tratomais.core.model.Domain;
import br.com.tratomais.core.model.PartnerAndChannel;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.UserPartner;
import br.com.tratomais.core.model.UserPartnerId;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Partner;
import br.com.tratomais.core.service.IServiceDomain;
import br.com.tratomais.core.service.IServicePartner;

public class ServicePartnerImpl extends ServiceBean implements IServicePartner {

	private IServiceDomain serviceDomain;
	private IDaoUserPartner daoUserPartner;
	private IDaoPartner daoPartner;
	private IDaoUser daoUser;

	public void setServiceDomain(IServiceDomain serviceDomain) {
		this.serviceDomain = serviceDomain;
	}

	public void setDaoUserPartner(IDaoUserPartner daoUserPartner) {
		this.daoUserPartner = daoUserPartner;
	}

	public void setDaoUser(IDaoUser daoUser) {
		this.daoUser = daoUser;
	}

	public void setDaoPartner(IDaoPartner daoPartner) {
		this.daoPartner = daoPartner;
	}

	public List<Partner> findByName(String name) {
		return this.daoPartner.findByName(name);
	}

	public Partner findById(Integer id) {
		return this.daoPartner.findById(id);
	}

	public List<Partner> listAll()  {
		return this.listAll(AuthenticationHelper.getLoggedUser());
	}

	public List<Partner> listAll(User user) {
		if (user.isAdministrator()) {
			return (List<Partner>) daoPartner.listAll();
		}
		return daoPartner.listAllAllowedByUser(user);
	}

	public List<Partner> listAllActivePartner()  {
		return this.listAllActivePartner(AuthenticationHelper.getLoggedUser());
	}

	public List<PartnerAndChannel> listAllPartnerChannel(User user)  {
		List<Partner> listAllActivePartner = this.listAllActivePartner(user);
		List<Integer> partnersIdList = new ArrayList<Integer>();
		for (Partner partner : listAllActivePartner) {
			partnersIdList.add(partner.getPartnerId());
		}
		List<PartnerAndChannel> listPartnersAndChannel = daoPartner.listPartnersAndChannel(partnersIdList);
		List<PartnerAndChannel> listPartnersAndChannelByUser = new ArrayList<PartnerAndChannel>();
		for (Channel channel : user.getChannels()) {
			for (PartnerAndChannel partnerAndChannel : listPartnersAndChannel) {
				if (channel.getChannelId() == partnerAndChannel.getChannelId()) {
					listPartnersAndChannelByUser.add(partnerAndChannel);
				}
			}
		}
		return listPartnersAndChannelByUser;
	}

	public List<Partner> listAllActivePartner(User userEdit)  {
		User loggedUser = AuthenticationHelper.getLoggedUser();
		List<Partner> partnersOfUserLogged = (List<Partner>) daoPartner.listAllByUser(loggedUser);
		if (userEdit == null) {
			for (Partner partnerUserLogged : partnersOfUserLogged) {
				partnerUserLogged.setTransientSelected(false);
				partnerUserLogged.setTransientAdministrator(false);
				partnerUserLogged.setTransientAllChannel(false);
				partnerUserLogged.setTransientIsDefault(false);
			}
		} else if (userEdit.isAdministrator()) {
			for (Partner partnerUserLogged : partnersOfUserLogged) {
				partnerUserLogged.setTransientSelected(true);
				partnerUserLogged.setTransientAdministrator(true);
				partnerUserLogged.setTransientAllChannel(true);
				partnerUserLogged.setTransientIsDefault(false);
			}
		} else {
			List<Partner> partnersOfUser = daoPartner.listAllByUser(userEdit);
			for (Partner partnerUserLogged : partnersOfUserLogged) {
				partnerUserLogged.setTransientSelected(false);
				partnerUserLogged.setTransientAdministrator(false);
				partnerUserLogged.setTransientAllChannel(false);
				partnerUserLogged.setTransientIsDefault(false);
				for (Partner partnerUserEdit : partnersOfUser) {
					if (partnerUserEdit.getPartnerId() == partnerUserLogged.getPartnerId()) {
						partnerUserLogged.setTransientSelected(partnerUserEdit.isTransientSelected());
						partnerUserLogged.setTransientAdministrator(partnerUserEdit.isTransientAdministrator());
						partnerUserLogged.setTransientAllChannel(partnerUserEdit.isTransientAllChannel());
						partnerUserLogged.setTransientIsDefault(partnerUserEdit.isTransientIsDefault());
					}
				}
			}
		}
		return partnersOfUserLogged;
	}

	public void deleteObject(Partner entity) {
		daoPartner.delete(entity);
	}

	public Partner saveObjectInsert(Partner entity){
		Partner partner = daoPartner.save(entity);
		List<User> users = daoUser.listAllAdmin();

		for (User user : users) {
			UserPartner userPartnerTmp = null;
			if (partner.getUserPartners() == null) {
				partner.setUserPartners(new HashSet<UserPartner>());
			}
			for (UserPartner userPartner : partner.getUserPartners()) {
				if (userPartner.getUser().getUserId() == user.getUserId()) {
					userPartnerTmp = userPartner;
				}
			}

			if (userPartnerTmp == null) {
				UserPartnerId userPartnerId = new UserPartnerId(user.getUserId(), partner.getPartnerId());
				userPartnerTmp = new UserPartner();
				userPartnerTmp.setId(userPartnerId);
				userPartnerTmp.setPartner(partner);
				userPartnerTmp.setUser(user);
			}
			userPartnerTmp.setAdministrator(true);
			daoUserPartner.save(userPartnerTmp);
		}
		
		return daoPartner.findById(partner.getPartnerId());
	}

	/**
	 * Para cada parceiro e necessario um dominio diferente
	 **/
	public Partner saveObject(Partner entity, String domainOld) {
		if(entity.getDomain() != null){
			entity.setDomain(serviceDomain.findById(entity.getDomain().getDomainId()));
		}
		Partner par = daoPartner.save(entity);
		return par;
	}

	public Partner saveObject(Partner entity) {
		entity.getUserPartners().clear();
		return this.saveObject(entity, null);
	}

	@SuppressWarnings("unused")
	private Domain managedDomainPartner(String nameNewDomain, String nameOldDomain) {
		if (nameOldDomain != null) {
			Domain oldDomain = serviceDomain.findDomainByName(nameOldDomain);
			serviceDomain.deleteObject(oldDomain);
		}
		Domain newDomain = new Domain();
		newDomain.setName(nameNewDomain);
		newDomain.setAutenticationType(1);
		return serviceDomain.saveObject(newDomain);
	}

	/**
	 * list all partner from the user
	 * @param partnerId
	 * @param productId
	 * @param brokerId
	 * @return List<Partner>
	 */
	public List<Partner> listPartnerPerProductOrBroker(Integer partnerId, Integer productId, Integer brokerId){
		if(partnerId == 0 || partnerId == 99999)partnerId = null;
		if(productId == 0 || productId == 99999)productId = null;
		if(brokerId == 0 || brokerId == 99999)brokerId = null;
		
		return daoPartner.listPartnerPerProductOrBroker(partnerId, productId, brokerId, AuthenticationHelper.getLoggedUser().getUserId());
	}
	
	/**
	 * Query to list partners active by user
	 * 
	 * @param userId: Id User
	 * @return {List<{@link Partner}>}
	 */
	public List<Partner> listAllActivePartnerByUser(int userId){
		return daoPartner.listAllActivePartnerByUser(userId);
	}

	@Override
	public List<Partner> listPartner(int insurerID, Date refDate) {
		return daoPartner.listPartner(insurerID, refDate);
	}
}
