package br.com.tratomais.core.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.com.tratomais.core.dao.IDaoDeductible;
import br.com.tratomais.core.dao.IDaoProduct;
import br.com.tratomais.core.dao.IDaoProfile;
import br.com.tratomais.core.infrastructure.AuthenticationHelper;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.policy.ItemCoverage;
import br.com.tratomais.core.model.product.CoverageOption;
import br.com.tratomais.core.model.product.CoveragePlan;
import br.com.tratomais.core.model.product.CoverageRangeValue;
import br.com.tratomais.core.model.product.PlanKinship;
import br.com.tratomais.core.model.product.PlanRiskType;
import br.com.tratomais.core.model.product.Product;
import br.com.tratomais.core.model.product.ProductOption;
import br.com.tratomais.core.model.product.ProductPlan;
import br.com.tratomais.core.model.product.ProductTerm;
import br.com.tratomais.core.model.product.Profile;
import br.com.tratomais.core.service.IServiceProduct;
import br.com.tratomais.core.util.Constantes;

public class ServiceProductImpl extends ServiceBean implements IServiceProduct {

	private IDaoProduct daoProduct;

	private IDaoProfile daoProfile;
	
	private IDaoDeductible daoDeductible;
	
	public void setDaoProduct(IDaoProduct daoProduct) {
		this.daoProduct = daoProduct;  
	}

	public void setDaoProfile(IDaoProfile daoProfile) {
		this.daoProfile = daoProfile;
	}

	public List<ProductOption> findProductsbyPartner(int partnerId, int insurerId, Date refDate) {

		if (refDate == null) {
			refDate = new Date(System.currentTimeMillis());
		}
		if (insurerId == 0) {
			insurerId = Constantes.ID_INSURER_ZURICH;
		}

		List<ProductOption> products = daoProduct.findProductsbyPartner(partnerId, insurerId, refDate);
		return products;
	}

	public List<CoveragePlan> listCoveragePlanByRefDate(int productId, int planId, Date refDate) {
		if (refDate == null) {
			refDate = new Date(System.currentTimeMillis());
		}
		return daoProduct.listCoveragePlanByRefDate(productId, planId, refDate);
	}

	@Override
	public List<CoverageOption> listCoverageOptionByRefDate(int productId, int coveragePlanId, Date refDate) {
		return listCoverageOptionByRefDate( productId, coveragePlanId, refDate, null );
	}

	public List<CoverageOption> listCoverageOptionByRefDate(int productId, int coveragePlanId, Date refDate, Set< ItemCoverage > itemCoverageList) {
		List<CoveragePlan> coveragePlanList = daoProduct.listCoveragePlanByRefDate(productId, coveragePlanId, refDate);
		List<CoverageOption> coverageOptionList = new ArrayList<CoverageOption>();
		
		ItemCoverage workItemCoverage = null;
		
		for(CoveragePlan workCoveragePlan : coveragePlanList) {
			
			//Testa Lista de Coberturas do Item
			//Se foi passada a lista de coberturas filtra somente coberturas contratadas
			CoverageOption workCoverageOption = new CoverageOption();
			
			workCoverageOption.setCoveragePlan(workCoveragePlan);
			
			if ( workCoveragePlan.isCompulsory() ) {
				workCoverageOption.setContractCoverage( true );
			}
			
			setCoverageOptions(workCoverageOption);
			if ( itemCoverageList != null && !itemCoverageList.isEmpty() ) {
				workItemCoverage = getSelectedCoverage(workCoveragePlan, itemCoverageList);
				if ( workItemCoverage ==null ) {
					continue;
				}
			}

			List<CoverageRangeValue> listRangeValue = new ArrayList< CoverageRangeValue >();

			if ( workCoveragePlan.getInsuredValueType() == 207 ) {
				listRangeValue = daoProduct.listCoverageRangeValueByRefDate( workCoveragePlan.getId().getProductId(), workCoveragePlan.getId().getPlanId(), workCoveragePlan.getId().getCoverageId(), refDate );
			}
			
			workCoverageOption.setRangeValueList( listRangeValue );	
			workCoverageOption.setDeductibleList(daoDeductible.findByCoverageIdReferenceDate(workCoveragePlan.getId().getCoverageId(), refDate));

			if ( itemCoverageList != null && !itemCoverageList.isEmpty() ) {
				workCoverageOption.setTotalPremium(workItemCoverage.getTotalPremium());
				workCoverageOption.setValue( workItemCoverage.getInsuredValue() );
				workCoverageOption.setCalcStepList(workItemCoverage.getCalcSteps());
				if(workItemCoverage.getRangeValueId() != null){
					workCoverageOption.setRangeValueId( workItemCoverage.getRangeValueId() );
				}
			}else{
				workCoverageOption.setRangeValueId( -1 );

				switch ( workCoveragePlan.getInsuredValueType() ) {
				case CoveragePlan.INSURED_VALUE_TYPE_SAF:
				case CoveragePlan.INSURED_VALUE_TYPE_CSA:
					workCoverageOption.setValue( workCoveragePlan.getFixedInsuredValue() );
					break;
				default:
					workCoverageOption.setValue( 0.0 );
					break;
				} 
			}

			coverageOptionList.add(workCoverageOption);

		}
		
		Collections.sort( coverageOptionList, new Comparator< CoverageOption >(){

			@Override
			public int compare(CoverageOption o1, CoverageOption o2) {
				return o1.getCoveragePlan().getDisplayOrder().compareTo( o2.getCoveragePlan().getDisplayOrder() );
			}
		});
		
		return coverageOptionList;
	}	
	
	public List<PlanRiskType> listPlanRiskTypeDescriptionByRefDate(int productId, int planId, Date refDate) {
		if (refDate == null) {
			refDate = new Date(System.currentTimeMillis());
		}
		return daoProduct.listPlanRiskTypeDescriptionByRefDate(productId, planId, refDate);
	}	
		
	public PlanKinship listPlanKinshipByPersonalRiskType(int productId, int riskPlanId, int personalRiskType, int renewalType, Date refDate){
		if (refDate == null) {
			refDate = new Date(System.currentTimeMillis());
		}
		return daoProduct.listPlanKinshipByPersonalRiskType(productId, riskPlanId, personalRiskType, renewalType, refDate);
	}	

	public List<ProductPlan> findPlanByProductId(int productId, Date refDate) {
		if (refDate == null) {
			refDate = new Date(System.currentTimeMillis());
		}
		return daoProduct.findPlanByProductId(productId, refDate);
	}

	public List<ProductPlan> listCoveragePlanByProductId(int productId, int riskPlanId, Date refDate) {
		if ( riskPlanId == 0 ) {
			riskPlanId = 1;
		}
		if (refDate == null) {
			refDate = new Date(System.currentTimeMillis());
		}
		return daoProduct.listCoveragePlanByProductId(productId, riskPlanId, refDate);
	}
	
	public List<ProductPlan> findRiskPlanByProductId(int productId, Date refDate) {
		if (refDate == null) {
			refDate = new Date(System.currentTimeMillis());
		}
		return daoProduct.findRiskPlanByProductId(productId, refDate);
	}

	public List<ProductPlan> listPersonalRiskPlanByProductId(int productId, Date refDate) {
		if (refDate == null) {
			refDate = new Date(System.currentTimeMillis());
		}
		return daoProduct.listPersonalRiskPlanByProductId(productId, refDate);
	}

	public List<Product> listAllProduct() {
		return daoProduct.listAll();
	}

	public Product findProductToBeCopy(int productId, Date referenceDate) {
		return daoProduct.findProductToBeCopy(productId, referenceDate);
	}

	@Override
	public List<Product> findProductsPerPartner(int partnerId) {
		return daoProduct.findProductsPerPartner(partnerId);
	}

	public ProductPlan getPlanById(int productId, int planId, Date refDate) {
		return daoProduct.getPlanById(productId, planId, refDate);
	}

	public ProductPlan getRiskPlanById(int productId, int planId, Date refDate) {
		return daoProduct.getRiskPlanById(productId, planId, refDate);
	}
	
	public List <CoverageRangeValue> listCoverageRangeValueByRefDate(int productId, int coveragePlanId, int coverageId ,Date refDate){
		return daoProduct.listCoverageRangeValueByRefDate(productId, coveragePlanId, coverageId, refDate);
	}

	public void setCoverageOptions(CoverageOption coverageOption){
		
		if( coverageOption.getCoveragePlan().isCompulsory() )
		{
			coverageOption.setContractCoverage(true);
			coverageOption.setContractPlan(true);
			coverageOption.setEnableSelectionCoverage(false);
			coverageOption.setEnableSelectionPlan(false);

		}
//		else if( coverageOption.getCoveragePlan().getCoverage().isCompulsory() )
//		{
//			coverageOption.setContractCoverage(true);
//			coverageOption.setContractPlan(false);
//			coverageOption.setEnableSelectionCoverage(false);
//			coverageOption.setEnableSelectionPlan(true);
//		}
		else
		{
			coverageOption.setContractCoverage(false);
			coverageOption.setContractPlan(false);
			coverageOption.setEnableSelectionCoverage(true);
			coverageOption.setEnableSelectionPlan(true);
		}	
		
	}
	
	private ItemCoverage getSelectedCoverage(CoveragePlan workCoveragePlan, Set< ItemCoverage > itemCoverageList) {
		for ( ItemCoverage itemCoverage : itemCoverageList ) {
			if ( workCoveragePlan.getCoverage().getCoverageId() == itemCoverage.getId().getCoverageId() ) {
				return itemCoverage;
			}
		}
		return null;
	}

	/**
	 * Gets a list of planKingShip
	 * @param productId Product Identificator
	 * @param planId  Plan Identificator
	 * @param renewalType Renewal Type
	 * @param refDate Reference Date
	 * @return PlanKinShip 
	 */
	public List<PlanKinship> listPlanKinshipByRiskPlan(int productId, int riskPlanId, int renewalType, Date refDate){
		return this.daoProduct.listPlanKinshipByRiskPlan( productId, riskPlanId, renewalType, refDate );
	}

	/**
	 * @param productId Lista de produtos
	 * @param riskPlanId Lista de Id de planos (planId)
	 * @param refDate Data de Refer�ncia
	 * @return Lista de Risk Types 
	 */
	private List<Integer>getRiskTypes(int productId, int riskPlanId, Date refDate){
		List< Integer > returnVals = new ArrayList< Integer >();		
		List< PlanRiskType > riskTypeList = daoProduct.listPlanRiskTypeByRefDate( productId, riskPlanId, refDate);	
		for ( PlanRiskType planRiskType : riskTypeList ) {
			returnVals.add( new Integer(planRiskType.getId().getRiskType()) );
		}
		return returnVals;
	}
	
	@Override
	public List<CoverageOption> listCoverageHabitatByEndorsement(
			Endorsement endorsement, Set< ItemCoverage > itemCoverageLista) {
		Set< ItemCoverage > itemCoverageList = new HashSet<ItemCoverage>(itemCoverageLista);
		List<CoverageOption> listReturn = new ArrayList<CoverageOption>();
		HashMap< String, CoverageOption > map = new HashMap< String, CoverageOption >();
		Item itemAnalisado = (Item) endorsement.getItems().toArray()[0]; 
		int productId, coveragePlanId, riskPlanId;		
		productId = endorsement.getContract().getProductId();
		coveragePlanId = itemAnalisado.getCoveragePlanId();
		riskPlanId = endorsement.getRiskPlanId();
		
		Date refDate = endorsement.getIssuanceDate();
		
		for (Item item : endorsement.getItems()){
			itemCoverageList.addAll(item.getItemCoverages());
		}
			
		// Lista de Risk Types 
		ItemCoverage workItemCoverage = null;
		List< Integer > workRiskTyp = getRiskTypes(productId, riskPlanId, refDate);	
		List<CoveragePlan> coveragePlanList = daoProduct.listCoveragePlanByRefDate(productId, coveragePlanId, refDate);
		for (CoveragePlan workCoveragePlan : coveragePlanList) {
			
			//Testa tipo de risco
			//Se n�o estiver na lista descarta a cobertura
			if ( !workRiskTyp.contains( new Integer(workCoveragePlan.getCoverage().getRiskType()) ) ) {
				continue;
			}
			
			//Testa Lista de Coberturas do Item
			//Se foi passada a lista de coberturas filtra somente coberturas contratadas
			if ( itemCoverageList != null && !itemCoverageList.isEmpty() ) {
				workItemCoverage = getSelectedCoverage(workCoveragePlan, itemCoverageList);
				if ( workItemCoverage ==null ) {
					continue;
				}
			}
				

			CoverageOption workGridOption = map.get( workCoveragePlan.getCoverage().getCoverageCode() );
			
			if ( workGridOption == null ) {
				workGridOption = new CoverageOption();

				workGridOption.getCoveragePlan().getCoverage().setCoverageCode( workCoveragePlan.getCoverage().getCoverageCode() );
				workGridOption.getCoveragePlan().getCoverage().setNickName( workCoveragePlan.getNickName() );
				workGridOption.getCoveragePlan().getCoverage().setDisplayOrder( workCoveragePlan.getDisplayOrder() );
				workGridOption.getCoveragePlan().getCoverage().setCompulsory( workCoveragePlan.isCompulsory() );
				
				map.put( workGridOption.getCoveragePlan().getCoverage().getCoverageCode(), workGridOption );
				
			}
			
			if ( workCoveragePlan.isCompulsory() ) {
				workGridOption.setContractCoverage( true );
			}
			
			List<CoverageRangeValue> listRangeValue = new ArrayList< CoverageRangeValue >();
			//se for range
			if ( workCoveragePlan.getInsuredValueType() == 207 ) {
				listRangeValue = daoProduct.listCoverageRangeValueByRefDate( workCoveragePlan.getId().getProductId(), workCoveragePlan.getId().getPlanId(), workCoveragePlan.getId().getCoverageId(), refDate );
			}
			
			workGridOption.getCoveragePlan().getCoverage().setRiskType( workCoveragePlan.getCoverage().getRiskType() );
			workGridOption.getCoveragePlan().setInsuredValueType( workCoveragePlan.getInsuredValueType() );
			workGridOption.getCoveragePlan().setInsuredValueBy( workCoveragePlan.getInsuredValueBy() );
			workGridOption.setRangeValueList( listRangeValue );	
			workGridOption.setCoveragePlan( workCoveragePlan );
			if ( itemCoverageList != null && !itemCoverageList.isEmpty() ) {
				workGridOption.setValue( workItemCoverage.getInsuredValue() );
				if(workItemCoverage.getRangeValueId() != null){
					workGridOption.setRangeValueId( workItemCoverage.getRangeValueId() );
				}
			}else{
				workGridOption.setRangeValueId( -1 );
				switch ( workGridOption.getCoveragePlan().getInsuredValueType() ) {
				case CoveragePlan.INSURED_VALUE_TYPE_SAF:
				case CoveragePlan.INSURED_VALUE_TYPE_CSA:
					workGridOption.setValue( workCoveragePlan.getFixedInsuredValue() );
					break;
				default:
					workGridOption.setValue( 0.0 );
					break;
				} 
			}
			
			/*switch ( workCoveragePlan.getCoverage().getRiskType() ) {
			case Coverage.RISKTYPE_STRUCTURE:  // 297 Estructura

				
				break;
			case Coverage.RISKTYPE_CONTENT: // 298 Contenido
				workGridOption.setRiskType2( workCoveragePlan.getCoverage().getRiskType() );
				workGridOption.setInsuredValueType2( workCoveragePlan.getInsuredValueType() );
				workGridOption.setRangeValueList2( listRangeValue );	
				workGridOption.setCoveragePlan2( workCoveragePlan );
				if ( itemCoverageList != null && !itemCoverageList.isEmpty() ) {
					workGridOption.setInsuredValue2( workItemCoverage.getInsuredValue() );
					if(workItemCoverage.getRangeValueId() != null){
						workGridOption.setRangeValueId2( workItemCoverage.getRangeValueId() );
					}
				}else{
					workGridOption.setRangeValueId2( -1 );
					switch ( workGridOption.getInsuredValueType2() ) {
					case CoveragePlan.INSURED_VALUE_TYPE_SAF:
					case CoveragePlan.INSURED_VALUE_TYPE_CSA:
						workGridOption.setInsuredValue2( workCoveragePlan.getFixedInsuredValue() );
						break;
					default:
						workGridOption.setInsuredValue2( 0.0 );
						break;
					} 
				}
				
				
				break;
			case Coverage.RISKTYPE_BOTH: // 299 Ambos
				workGridOption.setRiskType3( workCoveragePlan.getCoverage().getRiskType() );
				workGridOption.setInsuredValueType3( workCoveragePlan.getInsuredValueType() );
				workGridOption.setRangeValueList3( listRangeValue );	
				workGridOption.setCoveragePlan3( workCoveragePlan );
				if ( itemCoverageList != null && !itemCoverageList.isEmpty() ) {
					workGridOption.setInsuredValue3( workItemCoverage.getInsuredValue() );
					if(workItemCoverage.getRangeValueId() != null){
						workGridOption.setRangeValueId3( workItemCoverage.getRangeValueId() );
					}
				}else{
					workGridOption.setRangeValueId3( -1 );
					switch ( workGridOption.getInsuredValueType3() ) {
					case CoveragePlan.INSURED_VALUE_TYPE_SAF:
					case CoveragePlan.INSURED_VALUE_TYPE_CSA:
						workGridOption.setInsuredValue3( workCoveragePlan.getFixedInsuredValue() );
						break;
					default:
						workGridOption.setInsuredValue3( 0.0 );
						break;
					} 
				}
				
				
				break;

			default:
				break;
			}*/

		}

		listReturn.addAll(  map.values() );
		
//		// percorre a lista que est� vindo 
//		for (CoverageOption dataGridOption : listReturn) {
//			for (ItemCoverage ic : itemAnalisado.getItemCoverages()) {
//				dataGridOption.get
//				ic.get
//			}
//			
//			itemAnalisado.getItemCoverages().contains(dataGridOption);
//		}
		
		Collections.sort( listReturn, new Comparator< CoverageOption >(){

			@Override
			public int compare(CoverageOption o1, CoverageOption o2) {
				return o1.getCoveragePlan().getDisplayOrder().compareTo( o2.getCoveragePlan().getDisplayOrder() );
			}
		});
		
		
		// Neste ponto temos todas os dados default
		
		
		
		return listReturn;
	}

    /**
     * list all Product from the user
     * @param partnerId
     * @param productId
     * @param brokerId
     * @return List<Product>
     */
	public List<Product> listProductPerPartnerOrBroker(Integer partnerId, Integer productId, Integer brokerId){
		if(partnerId == 0 || partnerId == 99999)partnerId = null;
		if(productId == 0 || productId == 99999)productId = null;
		if(brokerId == 0 || brokerId == 99999)brokerId = null;
		
		return daoProduct.listProductPerPartnerOrBroker(partnerId, productId, brokerId, AuthenticationHelper.getLoggedUser().getUserId());
	}

	@Override
	public ProductOption getProduct(int productId, Date referenceDate) {
		return daoProduct.getProduct(productId, referenceDate);
	}

	@Override
	public Product getProductById(int productId) {
		return daoProduct.getProductById(productId);
	}

	@Override
	public List<Profile> findProfileList(int productId, Date referenceDate) {
		return daoProfile.findByProductIdReferenceDate(productId, referenceDate);
	}

	public void setDaoDeductible(IDaoDeductible daoDeductible) {
		this.daoDeductible = daoDeductible;
	}

	@Override
	public List<Product> listProduct(int objectID, Date refDate) {
		return daoProduct.listProduct(objectID, refDate);
	}

	@Override
	public List<ProductPlan> listProductPlan(int productID, Date versionDate) {
		return daoProduct.listProductPlan(productID, versionDate);
	}

	@Override
	public ProductPlan getProductPlanById(int productID, int planID, Date versionDate) {
		return daoProduct.getProductPlanById(productID, planID, versionDate);
	}

	@Override
	public CoverageRangeValue getCoverageRangeValue(int productID, int planID, int coverageID, int rangeValueID, Date versionDate) {
		return daoProduct.getCoverageRangeValue(productID, planID, coverageID, rangeValueID, versionDate);
	}

	@Override
	public ProductTerm getProductTermById(int productID, int termID, Date versionDate) {
		return daoProduct.getProductTermById(productID, termID, versionDate);
	}
}
