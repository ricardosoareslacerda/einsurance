package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.dao.IDaoCountry;
import br.com.tratomais.core.model.Country;
import br.com.tratomais.core.model.product.IdentifiedList;
import br.com.tratomais.core.service.IServiceCountry;

public class ServiceCountryImpl extends ServiceBean implements IServiceCountry {

	private IDaoCountry daoCountry;
	
	public void setDaoCountry(IDaoCountry daoCountry) {
		this.daoCountry = daoCountry;
	}

	public Country findById(Integer id) {
		return daoCountry.findById(id);
	}

	public List<Country> findByName(String name) {
		return daoCountry.findByName(name);
	}

	public List<Country> listAll() {
		return daoCountry.listAll();
	}

	public IdentifiedList listCountry(IdentifiedList identifiedList) {
		return daoCountry.listCountry(identifiedList);
	}

	@Override
	public List<Country> listCountryId(int countryId) {
		return daoCountry.findByName(countryId);
	}

	@Override
	public List<Country> listCountryByRefDate(Date refDate) {
		return daoCountry.listCountryByRefDate(refDate);
	}
}
