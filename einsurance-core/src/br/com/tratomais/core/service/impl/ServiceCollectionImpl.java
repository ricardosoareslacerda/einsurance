package br.com.tratomais.core.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jms.JMSException;
import javax.naming.NamingException;

import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.dao.IDaoProduct;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Customer;
import br.com.tratomais.core.model.customer.Partner;
import br.com.tratomais.core.model.customer.Phone;
import br.com.tratomais.core.model.policy.Contract;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.Installment;
import br.com.tratomais.core.model.product.DataOption;
import br.com.tratomais.core.model.product.Product;
import br.com.tratomais.core.service.IServiceChannel;
import br.com.tratomais.core.service.IServiceCollection;
import br.com.tratomais.core.service.IServiceDataOption;
import br.com.tratomais.core.service.IServicePartner;
import br.com.tratomais.core.util.StringUtil;
import br.com.tratomais.core.util.datatrans.ECollectionJMSTransmitter;
import br.com.tratomais.core.util.jms.EInsuranceJMSException;
import br.com.tratomais.esb.collections.model.request.CollectionDetail;
import br.com.tratomais.esb.collections.model.request.CollectionEnvelope;
import br.com.tratomais.esb.collections.model.request.DetailAttribute;

/**
 * @author leo.costa
 */
public class ServiceCollectionImpl implements IServiceCollection {

	private IDaoPolicy daoPolicy;
	private IDaoProduct daoProduct;
	private IServicePartner servicePartner;
	private IServiceDataOption serviceDataOption;
	private IServiceChannel serviceChannel;

	/**
	 * 
	 */
	public ServiceCollectionImpl() {
	}
	
	public void setDaoPolicy(IDaoPolicy daoPolicy) {
		this.daoPolicy = daoPolicy;
	}
	
	public void setDaoProduct(IDaoProduct daoProduct) {
		this.daoProduct = daoProduct;
	}

	public void setServicePartner(IServicePartner servicePartner) {
		this.servicePartner = servicePartner;
	}

	public void setServiceDataOption(IServiceDataOption serviceDataOption) {
		this.serviceDataOption = serviceDataOption;
	}
	
	public void setServiceChannel(IServiceChannel serviceChannel) {
		this.serviceChannel = serviceChannel;
	}

	/**
	 * @param endorsementId
	 * @param contractId
	 * @throws JMSException Error connecting to server
	 * @throws NamingException  Error connecting to the queue
	 * @return
	 * @throws IOException 
	 * @throws EInsuranceJMSException 
	 */
	public boolean dispachCollection(int endorsementId, int contractId, boolean cancellation) throws JMSException, NamingException, IOException, EInsuranceJMSException {
		return this.dispachCollection(daoPolicy.loadEndorsement(endorsementId, contractId), cancellation);
	}

	/**
	 * Envia o envelope de cobran�a.
	 * 
	 * @param endorsement
	 * @return true for sending false for not sending
	 * @throws JMSException Error connecting to server
	 * @throws NamingException  Error connecting to the queue
	 * @author leo.costa
	 * @throws IOException 
	 * @throws IOException 
	 * @throws EInsuranceJMSException 
	 **/
	public boolean dispachCollection(Endorsement endorsement, boolean cancellation) throws JMSException, NamingException, IOException, EInsuranceJMSException {
		if (endorsement == null || endorsement.getInstallments() == null || endorsement.getCustomerByPolicyHolderId() == null)
			throw new IllegalArgumentException("endorsement and installments and customerByPolicyHolderId can not be null");

		List<Installment> workInstallmentSent = new ArrayList<Installment>();
		for (Installment installment : endorsement.getInstallments()) {
			if (installment.getInstallmentStatus() == Installment.INSTALLMENT_STATUS_PENDING)
				workInstallmentSent.add(installment); 
		}

		if ((workInstallmentSent.size() == 0)) {
			return false;
		}

		CollectionEnvelope collectionEnvelope = this.fillCollectionEnvelope(endorsement, workInstallmentSent, (cancellation ? CollectionEnvelope.ACTION_CANCELLATION : CollectionEnvelope.ACTION_EFFECTIVE));
		ECollectionJMSTransmitter.getInstance().addCollectionEnvelope(collectionEnvelope);

		return true;
	}

	/**
	 * Popula os objetos de cobran�a de acordo com o endorsement
	 * 
	 * @param endorsement
	 * @return {@link CollectionEnvelope} objeto populado
	 * @author leo.costa
	 **/
	private CollectionEnvelope fillCollectionEnvelope(final Endorsement endorsement, final List<Installment> installments, final int action) {
		final int applicationId = 1;
		final String applicationDescricao = "I";  //eInsurance
		final Contract contract = endorsement.getContract();
		
		Customer policyHolder = endorsement.getCustomerByPolicyHolderId();
		Phone phone = policyHolder.getFirstAddress().getFirstPhone();
		Partner partner = servicePartner.findById(contract.getPartnerId());
		DataOption personType = serviceDataOption.findById(policyHolder.getPersonType());
		DataOption phoneType = serviceDataOption.findById(phone.getPhoneType());
		Channel channel = serviceChannel.findById(endorsement.getChannelId());
		Product product = daoProduct.findById(contract.getProductId());
		
		List<DetailAttribute> basicAttributes = new ArrayList<DetailAttribute>();
		basicAttributes.add(new DetailAttribute(DetailAttribute.CERTIFICATE_NUMBER, contract.getCertificateNumber().toString()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.POLICY_NUMBER, contract.getPolicyNumber().toString()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.PARTNER_ID, contract.getPartnerId()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.ENDORSEMENT_ID, endorsement.getId().getEndorsementId()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.PROPOSAL_NUMBER, endorsement.getProposalNumber().toString()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.CONTRACT_ID, endorsement.getId().getContractId()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.CHANNEL_CODE, channel.getExternalCode())); 
		basicAttributes.add(new DetailAttribute(DetailAttribute.PHONE_TYPE, phoneType.getFieldDescription()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.PHONE_NUMBER, phone.getPhoneNumber()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.PARTNER_NAME, partner.getNickName()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.POLICY_HOLDER_PERSON_TYPE, personType.getFieldValue()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.PRODUCT_CODE, product.getProductCode()));
		if (contract.getExternalKey() != null && !"".equalsIgnoreCase(contract.getExternalKey().trim()))
			basicAttributes.add(new DetailAttribute(DetailAttribute.EXTERNAL_KEY, contract.getExternalKey().trim()));
		
		CollectionEnvelope collectionEnvelope = new CollectionEnvelope(action);
		collectionEnvelope.setApplicationId(applicationId);
		collectionEnvelope.setLotCode(endorsement.getProposalNumber().toString());
		
		for (Installment readingInstallment : installments) {
			CollectionDetail collectionDetail = collectionEnvelope.addCollectionDetail();
			StringBuilder clientReference = new StringBuilder();
			StringBuilder billingReference = new StringBuilder();
			List<DetailAttribute> attributesSpecified = new ArrayList<DetailAttribute>();
			
			attributesSpecified.addAll(basicAttributes);
			attributesSpecified.add(new DetailAttribute(DetailAttribute.INSTALLMENT_ID, readingInstallment.getId().getInstallmentId()));
			if (readingInstallment.getCommission() != null && !readingInstallment.getCommission().equals(0.0))
				attributesSpecified.add(new DetailAttribute(DetailAttribute.COMMISSION_VALUE, readingInstallment.getCommission().toString()));
			if (readingInstallment.getLabour() != null && !readingInstallment.getLabour().equals(0.0))
				attributesSpecified.add(new DetailAttribute(DetailAttribute.LABOUR_VALUE, readingInstallment.getLabour().toString()));
			collectionDetail.setDetailAttributes(attributesSpecified);
			
			collectionDetail.setBillingMethodId(readingInstallment.getBillingMethodId());
			collectionDetail.setInstallmentType(readingInstallment.getInstallmentType());
			collectionDetail.setPaymentType(readingInstallment.getPaymentType());
			
			// Client Reference
			clientReference.append(applicationDescricao);
			clientReference.append(StringUtil.fill(contract.getPolicyNumber().toString(), "0", 13, true));
			clientReference.append(StringUtil.fill(contract.getCertificateNumber().toString(), "0", 6, true));
			collectionDetail.setClientReference(clientReference.toString());
			
			// Billing Reference
			billingReference.append(StringUtil.fill(endorsement.getProposalNumber().toString() + "", "0", 6, true));
			billingReference.append(StringUtil.fill(readingInstallment.getId().getInstallmentId() + "", "0", 2, true));
			collectionDetail.setBillingReference(billingReference.toString());
			
			collectionDetail.setDueDate(readingInstallment.getIssueDate());
			collectionDetail.setLimitDate(readingInstallment.getDueDate());
			collectionDetail.setCancelDate(readingInstallment.getCancelDate());
			if (action == CollectionEnvelope.ACTION_CANCELLATION && collectionDetail.getCancelDate() == null)
				collectionDetail.setCancelDate(new Date());
			
			collectionDetail.setInstallmentValue(readingInstallment.getInstallmentValue());
			collectionDetail.setCurrencyId(contract.getCurrencyId());
			collectionDetail.setCurrencyDate(readingInstallment.getEndorsement().getCurrencyDate());
			collectionDetail.setConversionRate(readingInstallment.getEndorsement().getConversionRate());
			
			collectionDetail.setHolderName(policyHolder.getName());
			collectionDetail.setHolderDocumentType(policyHolder.getDocumentType());
			collectionDetail.setHolderDocumentNumber(policyHolder.getDocumentNumber());
			
			collectionDetail.setBankNumber(readingInstallment.getBankNumber());
			collectionDetail.setBankAgencyNumber(readingInstallment.getBankAgencyNumber());
			collectionDetail.setBankAccountNumber(readingInstallment.getBankAccountNumber());
			collectionDetail.setBankCheckNumber(readingInstallment.getBankCheckNumber());
			
			collectionDetail.setCardType(readingInstallment.getCardType());
			collectionDetail.setCardBrandType(readingInstallment.getCardBrandType());
			collectionDetail.setCardNumber(readingInstallment.getCardNumber());
			collectionDetail.setCardExpirationDate(readingInstallment.getCardExpirationDate());
			collectionDetail.setCardHolderName(readingInstallment.getCardHolderName());
			
			collectionDetail.setOtherAccountNumber(readingInstallment.getOtherAccountNumber());
			collectionDetail.setInvoiceNumber(readingInstallment.getInvoiceNumber());
			
			collectionDetail.setPaymentNotified(readingInstallment.isPaymentNotified());
		}
		
		return collectionEnvelope;
	}
}