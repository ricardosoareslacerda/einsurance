package br.com.tratomais.core.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import br.com.tratomais.core.dao.IDaoDeductible;
import br.com.tratomais.core.model.product.Deductible;
import br.com.tratomais.core.service.IServiceDeductible;

public class ServiceDeductibleImpl extends ServiceBean implements IServiceDeductible {
	private static final Logger logger = Logger.getLogger( ServiceDeductibleImpl.class );

	private IDaoDeductible daoDeductible;

	public void setDaoDeductible(IDaoDeductible daoDeductible) {
		this.daoDeductible = daoDeductible;
	}
	
	public List<Deductible> findAllDeductible(){
		return daoDeductible.listAll();
	}

}
