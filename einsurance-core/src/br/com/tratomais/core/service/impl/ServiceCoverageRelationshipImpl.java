package br.com.tratomais.core.service.impl;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.dao.IDaoCoverageRelationship;
import br.com.tratomais.core.model.product.CoverageRelationship;
import br.com.tratomais.core.service.IServiceCoverageRelationship;

public class ServiceCoverageRelationshipImpl extends ServiceBean implements IServiceCoverageRelationship{

	private IDaoCoverageRelationship daoCoverageRelationship;
	
	public void setDaoCoverageRelationship(IDaoCoverageRelationship daoCoverageRelationship){
		this.daoCoverageRelationship = daoCoverageRelationship;
	}
	
	@Override
	public List<CoverageRelationship> listCoverageRelationship(int coverageId, int parentId, int coverageRelationshipType, Date refDate) {
		return daoCoverageRelationship.listCoverageRelationship(coverageId, parentId, coverageRelationshipType, refDate);
	}

	@Override
	public List<CoverageRelationship> listCoverageRelationship(int coverageId, int coverageRelationshipType, Date refDate) {
		return daoCoverageRelationship.listCoverageRelationship(coverageId, coverageRelationshipType, refDate);
	}

	@Override
	public List<CoverageRelationship> listCoverageRelationship(int coverageId, Date refDate) {
		return daoCoverageRelationship.listCoverageRelationship(coverageId, refDate);
	}

	@Override
	public CoverageRelationship findById(Integer id) {
		return daoCoverageRelationship.findById(id);
	}

	@Override
	public List<CoverageRelationship> findByName(String name) {
		return daoCoverageRelationship.findByName(name);
	}

	@Override
	public List<CoverageRelationship> listAll() {
		return daoCoverageRelationship.listAll();
	}

	@Override
	public void deleteObject(CoverageRelationship entity) {
		daoCoverageRelationship.delete(entity);
	}

	@Override
	public CoverageRelationship saveObject(CoverageRelationship entity) {
		return daoCoverageRelationship.save(entity);
	}

	@Override
	public List<CoverageRelationship> listCoverageRelationshipByProductId(int productId, int planId, int coverageId, int coverageRelationshipType, Date refDate) {
		return daoCoverageRelationship.listCoverageRelationshipByProductId(productId, planId, coverageId, coverageRelationshipType, refDate);
	}

}
