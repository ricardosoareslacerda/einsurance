package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.ResponseRelationship;

public interface IServiceResponseRelationship {

	public List<ResponseRelationship> getResponseRelationship( Integer productId, Integer questionnaireId, Integer questionId, Integer responseId, Date referenceDate );
	
}
