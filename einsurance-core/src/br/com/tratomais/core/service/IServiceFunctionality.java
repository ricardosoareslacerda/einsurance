package br.com.tratomais.core.service;

import java.util.List;

import br.com.tratomais.core.model.Functionality;

public interface IServiceFunctionality extends IFindEntity<Functionality, Integer>{

	public List<Functionality> listAllowedFunctionalityByUserLogged();

}
