/**
 * 
 */
package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.InsuredObject;

/**
 * @author eduardo.venancio
 */
public interface IServiceObject extends IFindEntity<InsuredObject, Integer>, ICrudEntity<InsuredObject, Integer> {

	public List<InsuredObject> listObject(Date refDate);

	public InsuredObject getObject(int objectID, Date refDate);

	public InsuredObject getObjectByID(int objectID);

}
