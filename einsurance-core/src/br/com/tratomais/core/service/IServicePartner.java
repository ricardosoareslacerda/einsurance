package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.PartnerAndChannel;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.customer.Partner;

public interface IServicePartner extends IFindEntity<Partner, Integer>, ICrudEntity<Partner, Integer> {

	public List<Partner> listAll(User user);

	public Partner saveObjectInsert(Partner entity);

	public List< Partner > listAllActivePartner(User user);

	public List< Partner > listAllActivePartner();
	
	public List<PartnerAndChannel> listAllPartnerChannel(User user);
	
	public List<Partner> listPartnerPerProductOrBroker(Integer partnerId, Integer productId, Integer brokerId);
	
	/**
	 * Query to list partners active by user
	 * 
	 * @param userId: Id User
	 * @return {List<{@link Partner}>}
	 */
	public List<Partner> listAllActivePartnerByUser(int userId);

	public List<Partner> listPartner(int insurerID, Date refDate);
}
