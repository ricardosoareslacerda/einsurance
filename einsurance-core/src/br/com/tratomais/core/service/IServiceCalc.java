package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.product.CoverageOption;
import br.com.tratomais.core.model.product.PaymentOption;
import br.com.tratomais.core.model.product.SimpleCalcResult;

/**
 * Calculation Service Interface 
 */
public interface IServiceCalc {
	/**
	 * 
	 * @param endorsement
	 * @return
	 */
	public SimpleCalcResult simpleCalc (Endorsement endorsement);

	/**
	 * 
	 * @param endorsement
	 * @param objectId
	 * @param List of coverageOption
	 * @return
	 */
	public SimpleCalcResult calculate (Endorsement endorsement, int objectId, List<CoverageOption> coverageOptionList);
	
	/**
	 * 
	 * @param endorsement
	 * @param List of coverageOption
	 * @return
	 */
	public SimpleCalcResult propertyCalc (Endorsement endorsement, List<CoverageOption> coverageOptionList);

	/**
	 * 
	 * @param endorsement
	 * @param List of coverageOption
	 * @return
	 */
	public SimpleCalcResult lifeCycleCalc (Endorsement endorsement, List<CoverageOption> coverageOptionList);

	/**
	 * Payment Options per Endorsement
	 * @param endorsement
	 * @return List of Payment Options
	 */ 
	public List<PaymentOption> getPaymentOptionsDefault(Endorsement endorsement);

	/**
	 * Payment Options 
	 * @param totalPremium
	 * @param productId 
	 * @param refDate
	 * @return List of Payment Options
	 */ 
	public List<PaymentOption> getPaymentOptionsDefault(Double totalPremium, double taxRate, int productId, Date refDate, Date dtEffective, Date dtExpiry);

	/**
	 * Payment Options per Endorsement
	 * @param endorsement
	 * @param billingMethodId
	 * @return List of Payment Options
	 */ 
	public List<PaymentOption> getPaymentOptionsByBillingMethodId(Endorsement endorsement, int billingMethodId, Date dtEffective, Date dtExpiry) ;
	
	/**
	 * Payment Option
	 * @param totalPremium
	 * @param productId
	 * @param billingMethodId
	 * @param paymentTermId
	 * @param refDate
	 * @param dtEffective
	 * @param dtExpiry
	 * @return payment Option
	 */
	public PaymentOption getPaymentOptionByPaymentTermId(Double totalPremium, double taxRate, int productId, int billingMethodId, int paymentTermId, Date refDate, Date dtEffective, Date dtExpiry);
	
	/**
	 * Payment Options 
	 * @param totalPremium
	 * @param productId 
	 * @param paymentType 
	 * @param refDate
	 * @return List of Payment Options
	 */ 
	public List<PaymentOption> getPaymentOptionsByPaymentType(Double totalPremium, double taxRate, int productId, int paymentType, Date refDate, Date dtEffective, Date dtExpiry) ;

	/**
	 * @param endorsement
	 * @param paymentOption
	 * @return Same instance with valid values
	 */
	public Endorsement effectiveProposal(Endorsement endorsement, PaymentOption paymentOption);

	/**
	 * @param endorsement
	 * @param paymentOption
	 * @param issuanceType
	 * @param newEffectiveDate
	 * @return
	 * @throws ServiceException
	 */
	public Endorsement effectiveEndorsement(Endorsement endorsement, PaymentOption paymentOption, int issuanceType, Date newEffectiveDate) throws ServiceException; 

	/**
	 * Returns the number documents permitted to a document/product
	 * @param productId Product Register 
	 * @param docNumber Document number
	 * @param docType Document type
	 * @param dateIni Starting date
	 * @param dateFin End date
	 * @param contractId Contract identification
	 * @return Number of policies already in the system
	 */
	public int numActivePoliciesPerProductDocument(int productId, String docNumber, int docType, Date dateIni, Date dateFin, int contractId);
	
	/**
	 * Call contract validation info 
	 * @param productId Product Identificator
	 * @param docNumber Document number
	 * @param docType Document type
	 * @paran dateIni Beggining date
	 * @param referenceDate Reference date to calculus 
	 * @param contractId Contract identificator to be excluded from query
	 * @return List validation error messages
	 */
	 public List<String> validateContract(int productId, String docNumber, int docType, Date dateIni, Date referenceDate, int contractId);

	 public List<PaymentOption> getPaymentOptionsByPaymentType(double totalPremium, double netPremium, double policyCost, double fractioningAdditional, double taxValue, double taxRate, int productId, int paymentType, Date refDate, Date effectiveDate, Date expireDate, Short dueDay, Integer renewalType, Integer renewalInsurerId, Integer issuanceType, Integer paymentTermType, Integer invoiceType, Integer firstPaymentType, Integer qtInstallment, Date proposalDate) throws ServiceException;
}
