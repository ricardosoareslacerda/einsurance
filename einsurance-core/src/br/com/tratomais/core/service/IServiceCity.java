package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.City;
import br.com.tratomais.core.model.product.IdentifiedList;

public interface IServiceCity extends IFindEntity< City, Integer >{

	public IdentifiedList listCityByState(IdentifiedList identifiedList, int stateId);
	
	public List<City> listCityByStateId(int stateId);

	public IdentifiedList listIdentifiedListCity(IdentifiedList identifiedList);

	public List<City> listCityByRefDate(int countryID, int stateID, Date refDate);
	
}
