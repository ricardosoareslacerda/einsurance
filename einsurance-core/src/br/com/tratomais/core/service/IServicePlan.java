/**
 * 
 */
package br.com.tratomais.core.service;

import br.com.tratomais.core.model.product.Plan;

/**
 * @author eduardo.venancio
 */
public interface IServicePlan extends ICrudEntity<Plan, Integer>, IFindEntity<Plan, Integer> {

}
