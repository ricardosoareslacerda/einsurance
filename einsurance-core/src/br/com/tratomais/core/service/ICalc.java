package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.model.pendencies.LockPendency;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.Installment;
import br.com.tratomais.core.model.product.PaymentOption;
import br.com.tratomais.core.service.erros.Erro;
import br.com.tratomais.esb.collections.model.response.CollectionResponse;
import br.com.tratomais.esb.collections.model.response.CollectionResponseItem;

/**
 * Defines insurance calculation methods
 * @author luiz.alberoni
 */
public interface ICalc {
	/**
	 * Calculates the endorsement
	 * @param endorsement Data to be calculated
	 * @return Same instance with calculation values
	 * @throws ServiceException 
	 */
	Endorsement simpleCalc(Endorsement endorsement) throws ServiceException;
	
	Endorsement simpleCalc(Endorsement endorsement, boolean isRecalculation) throws ServiceException;
	
	/**
	 * List of installments for the endorsement
	 * @return List of Installments
	 */
	List<Installment> listInstallments(Endorsement endorsement);

	/**
	 * 
	 * @return Same instance with valid values
	 */
	Endorsement effectiveProposal(Endorsement endorsement, PaymentOption paymentOption) throws ServiceException;

	/**
	 * 
	 * @param endorsement
	 * @param paymentOption
	 * @param issuanceType
	 * @param newEffectiveDate
	 * @return
	 * @throws ServiceException
	 */
	public Endorsement effectiveEndorsement(Endorsement endorsement, PaymentOption paymentOption, int issuanceType, Date newEffectiveDate) throws ServiceException;

	/**
	 * Payment Options per Endorsement
	 * @param endorsement
	 * @return List of Payment Options
	 */ 
	public List<PaymentOption> getPaymentOptionsDefault(Endorsement endorsement);

	/**
	 * Payment Options 
	 * @param totalPremium
	 * @param productId 
	 * @param refDate
	 * @return List of Payment Options
	 */ 
	public List<PaymentOption> getPaymentOptionsDefault(Double totalPremium, double taxRate, int productId, Date refDate, Date dtEffective, Date dtExpiry);

	/**
	 * Payment Options per Endorsement
	 * @param endorsement
	 * @param billingMethodId
	 * @return List of Payment Options
	 */ 
	public List<PaymentOption> getPaymentOptionsByBillingMethodId(Endorsement endorsement, int billingMethodId, Date dtEffective, Date dtExpiry) ;

	/**
	 * Payment Options 
	 * @param totalPremium
	 * @param productId 
	 * @param paymentType 
	 * @param refDate
	 * @return List of Payment Options
	 */ 
	public List<PaymentOption> getPaymentOptionsByBillingMethodId(Double totalPremium, double taxRate, int productId, int billingMethodId, Date refDate, Date dtEffective, Date dtExpiry) ;

	/**
	 * Payment Option
	 * @param totalPremium
	 * @param productId
	 * @param billingMethodId
	 * @param paymentTermId
	 * @param refDate
	 * @param dtEffective
	 * @param dtExpiry
	 * @return payment Option
	 */
	public PaymentOption getPaymentOptionByPaymentTermId(Double totalPremium, double taxRate, int productId, int billingMethodId, int paymentTermId, Date refDate, Date dtEffective, Date dtExpiry);
	
	/**
	 * Call contract validation info 
	 * @param productId Product Identificator
	 * @param docNumber Document number
	 * @param docType Document type
	 * @paran dateIni Beggining date
	 * @param referenceDate Reference date to calculus 
	 * @param contractId Contract identificator to be excluded from query
	 * @return List validation error messages
	 */
	 public List<Erro> validateContract(int productId, String docNumber, int docType, Date dateIni, Date referenceDate, int contractId);

	 /**
	  * @param lockPendencies2Save  Lock pendencies to be saved    
	  */	 
	 public void registerPendencies(List<LockPendency> lockPendencies2Save );
	 public void freePendency(LockPendency pendenciaEntrada);
	 public void baixaParcela(CollectionResponse collectionResponse );
  	 public void baixaParcela(CollectionResponseItem collectionResponse);
	 public void deletePendency(int contractId, int endorsementId);
	 
	 public List<PaymentOption> getPaymentOptionsByBillingMethodId(double totalPremium, double netPremium, double policyCost, double fractioningAdditional, double taxValue, double taxRate, int productId, int paymentType, Date refDate, Date effectiveDate, Date expireDate, Short dueDay, Integer renewalType, Integer renewalInsurerId, Integer issuanceType, Integer paymentTermType, Integer invoiceType, Integer firstPaymentType, Integer qtInstallment, Date proposalDate);
}
