package br.com.tratomais.core.service;

import java.util.List;

import br.com.tratomais.core.model.paging.Paginacao;
import br.com.tratomais.core.model.report.CollectionDetailsMainParameters;
import br.com.tratomais.core.model.report.CollectionMainParameters;
import br.com.tratomais.core.model.report.CollectionDetailsMainResult;
import br.com.tratomais.core.model.report.BillingMethodResult;
import br.com.tratomais.core.model.report.CollectionMainResult;
import br.com.tratomais.core.model.report.StatusLote;

public interface IServiceCollectionMain {
	
	public Paginacao listCollectionDetailsMainPaging(CollectionDetailsMainParameters collectionDetailsMainParameters);

	public List<CollectionDetailsMainResult> listCollectionDetailsMain(CollectionDetailsMainParameters collectionDetailsMainParameters);

	public List<BillingMethodResult> listBillingMethod();

	public List<CollectionMainResult> listCollectionMain(CollectionMainParameters collectionMainParameters);

	public Paginacao listCollectionMainPaging(CollectionMainParameters collectionMainParameters);

	public List<StatusLote> listStatusLote();

	public List<BillingMethodResult> listExchangeCollection();
}
