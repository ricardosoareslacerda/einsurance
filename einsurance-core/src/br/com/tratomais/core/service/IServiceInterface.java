package br.com.tratomais.core.service;

import java.util.Date;

import br.com.tratomais.core.model.interfaces.FileLot;
import br.com.tratomais.core.model.interfaces.FileLotDetails;
import br.com.tratomais.core.model.interfaces.Lot;
import br.com.tratomais.core.model.product.IdentifiedList;
import br.com.tratomais.core.model.report.DataViewInterface;

public interface IServiceInterface {
	
	public Lot getLot(Integer lotId);
	
	public Lot getLot(Integer lotId, Integer parentId, Integer exchangeId, Integer insurerId, Integer partnerId);
	
	public DataViewInterface getDataViewInterface();

	public IdentifiedList listLot(IdentifiedList identifiedList, Integer exchangeId, Integer lotStatus, Date rangeBefore, Date rangeAfter);

	public FileLot getFileLot(Integer fileLotId, Integer lotId);

	public FileLotDetails getFileLotDetails(Integer fileLotDetailsId, Integer fileLotId);

	public IdentifiedList listFileLot(IdentifiedList identifiedList, Integer lotId);

	public IdentifiedList listFileLotDetails(IdentifiedList identifiedList, Integer fileLotId);
}
