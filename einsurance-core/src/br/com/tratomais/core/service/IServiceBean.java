package br.com.tratomais.core.service;

import java.io.Serializable;

@SuppressWarnings("unchecked")
public interface IServiceBean {

	public Object load(Class clazz, Serializable id);

}
