package br.com.tratomais.core.service;

import java.util.List;

import br.com.tratomais.core.model.paging.Paginacao;
import br.com.tratomais.core.model.search.InstallmentParameters;
import br.com.tratomais.core.model.search.InstallmentResult;
import br.com.tratomais.core.model.search.SearchDocumentParameters;
import br.com.tratomais.core.model.search.SearchDocumentResult;

public interface IServiceSearchDocument {

	public List<SearchDocumentResult> listSearchDocument(SearchDocumentParameters searchDocumentParameters);
	
	public Paginacao listSearchDocumentPaging(SearchDocumentParameters searchDocumentParameters);

	public List<InstallmentResult> listInstallment(InstallmentParameters installmentParameters);

	public List<SearchDocumentResult> listSearchDocument(SearchDocumentParameters parameters, boolean isAuthentication);
}
