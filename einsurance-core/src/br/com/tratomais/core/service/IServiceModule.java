package br.com.tratomais.core.service;

import java.util.List;

import br.com.tratomais.core.model.Module;
import br.com.tratomais.core.model.User;

public interface IServiceModule extends IFindEntity<Module, Integer> {

	public List<Module> listAllByUser(User user);

	/**
	 * @return Modulos que lista os modulos
	 * @return modulos do user logado
	 **/
	public List<Module> listAllowedModulesByUserLogged();

}
