package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.DataOption;
import br.com.tratomais.core.model.product.IdentifiedList;

public interface IServiceDataOption extends IFindEntity< DataOption, Integer >{

	
	public List<DataOption> listDataOption(int dataGroupId);
	
	/**
	 * List Data Options by @see DataGroup 
	 * @param dataGroupId
	 * @param bringAll true/false (list all DataOption Active=true and Active=false) 
	 * @return List<DataGroup> of @see DataGroup
	 */	
	public List<DataOption> listDataOption(int dataGroupId, boolean bringAll);

	/**
	 * List Data Options by @see DataGroup 
	 * @param dataGroupId
	 * @param bringAll true/false (list all DataOption Active=true and Active=false) 
	 * @param externalCode
	 * @return List of @see DataGroup
	 */
	public List<DataOption> listDataOption(int dataGroupId, boolean bringAll, String externalCode);
	
	@Deprecated
	public List<DataOption> listPaymentType(int dataGroupId, int productId, Date referenceDate);
	public List<DataOption> listDataOptionByIdAndParent(int dataGroupId, int parentId);
	public IdentifiedList findIdentifiedListDataOptionByDataGroup(int dataGroupId, String identifierString, Date refDate);
	public IdentifiedList findIdentifiedListDataOptionByDataGroupParentId(int dataGroupId, int parentId, String identifierString, Date refDate);

	public List<DataOption> listDataOptionByRefDate(int dataGroupID, Date refDate);

	public List<DataOption> listDataOptionByFieldValue(String fieldValue, Date refDate);

	public List<DataOption> listDataOptionByParentID(int parentID, Date refDate);

	public List<DataOption> listDataOptionByParentID(String fieldValue, int parentID, Date refDate);
}
