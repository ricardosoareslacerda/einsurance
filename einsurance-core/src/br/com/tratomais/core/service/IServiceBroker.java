package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.customer.Broker;

public interface IServiceBroker extends IFindEntity<Broker, Integer>, ICrudEntity<Broker, Integer> {

	public List<Broker> findByNickName(String nickName);
	
	public List<Broker> listAllActiveBroker();

	/**
	 * Checks for Broker's external code existence
	 * @param insurerId 
	 * @param subsidiaryId
	 * @param externalCode
	 * @return true if exists 
	 */
	public boolean brokerExternalCodeExists(int insurerId, int brokerId, String externalCode);
	
	public List<Broker> listBrokerPerProductOrPartner(Integer partnerId, Integer productId, Integer brokerId);

	public List<Broker> listBroker(int insurerID, String regulatoryCode, Date refDate);

}
