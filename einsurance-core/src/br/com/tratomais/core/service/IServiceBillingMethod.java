package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.dao.IDaoGeneric;
import br.com.tratomais.core.model.product.BillingAgency;
import br.com.tratomais.core.model.product.BillingMethod;

public interface IServiceBillingMethod extends IDaoGeneric<BillingMethod, Integer> {

	public List<BillingMethod> listBillingMethod(int productID, Date versionDate);

	public BillingAgency getBillingAgencyById(int billingAgencyID, Date refDate);
}
