package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.policy.MasterPolicy;

public interface IServiceMasterPolicy extends IFindEntity< MasterPolicy, Integer >, ICrudEntity< MasterPolicy, Integer > {

	public List< MasterPolicy > listAllWithDescriptions();

	public List<MasterPolicy> listMasterPolicy(int insurerID, int partnerID, Date refDate);
}
