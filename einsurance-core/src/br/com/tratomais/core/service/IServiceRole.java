package br.com.tratomais.core.service;

import java.util.Collection;

import br.com.tratomais.core.model.Role;
import br.com.tratomais.core.model.User;

public interface IServiceRole extends IFindEntity<Role, Integer>, ICrudEntity<Role, Integer> {

	public Collection<Role> listAll(User user);

	public Collection<Role> listAllActiveRole();
}
