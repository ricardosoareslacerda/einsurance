package br.com.tratomais.core.service;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.jms.JMSException;
import javax.naming.NamingException;

import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.util.jms.EInsuranceJMSException;

/**
 * @author leo.costa
 *
 */
public interface IServiceCollection {

	/**
	 * @param endorsementId
	 * @param cancellation
	 * @param contractId
	 * @throws JMSException Error connecting to server
	 * @throws NamingException  Error connecting to the queue
	 * @return
	 * @throws IOException 
	 * @throws EInsuranceJMSException 
	 */
	public abstract boolean dispachCollection(int endorsementId, int contractId, boolean cancellation) throws JMSException, NamingException, IOException, EInsuranceJMSException ;

	/**
	 * Envia o envelope de cobran�a.
	 * 
	 * @param endorsement
	 * @param cancellation
	 * @return true for sending false for not sending
	 * @throws JMSException Error connecting to server
	 * @throws NamingException  Error connecting to the queue
	 * @author leo.costa
	 * @throws FileNotFoundException 
	 * @throws IOException 
	 * @throws EInsuranceJMSException 
	 **/
	public abstract boolean dispachCollection(Endorsement endorsement, boolean cancellation) throws JMSException, NamingException, FileNotFoundException, IOException, EInsuranceJMSException ;

}