package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.State;
import br.com.tratomais.core.model.product.IdentifiedList;

public interface IServiceState extends IFindEntity<State, Integer> {

	public IdentifiedList listStateByCountry(IdentifiedList identifiedList, int countryId);
	
	public List<State> listStateByCountryId(int countryId);

	public List<State> listStateByRefDate(int countryID, Date refDate);
	
}
