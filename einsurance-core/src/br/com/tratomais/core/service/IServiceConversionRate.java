package br.com.tratomais.core.service;

import java.util.Date;

import br.com.tratomais.core.dao.IDaoGeneric;
import br.com.tratomais.core.model.product.ConversionRate;

public interface IServiceConversionRate extends IDaoGeneric<ConversionRate, Integer> {

	public ConversionRate findConversionRateByRefDate(int currencyID, Date refDate);

}
