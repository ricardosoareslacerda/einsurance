package br.com.tratomais.core.service;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import br.com.tratomais.core.claim.ReturnClaimStateVerification;
import br.com.tratomais.core.dao.IDaoPolicy.AdjustmentsToEndorsement;
import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.model.Inflow;
import br.com.tratomais.core.model.claim.ClaimAction;
import br.com.tratomais.core.model.customer.Activity;
import br.com.tratomais.core.model.customer.Customer;
import br.com.tratomais.core.model.customer.Occupation;
import br.com.tratomais.core.model.customer.Profession;
import br.com.tratomais.core.model.pendencies.LockPendency;
import br.com.tratomais.core.model.policy.Contract;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.EndorsementOption;
import br.com.tratomais.core.model.policy.Installment;
import br.com.tratomais.core.model.product.BillingMethod;
import br.com.tratomais.core.model.product.IdentifiedList;
import br.com.tratomais.core.model.product.PaymentOption;
import br.com.tratomais.core.service.erros.ErrorList;
import br.com.tratomais.core.util.jms.EInsuranceJMSException;
import br.com.tratomais.esb.collections.model.response.CollectionResponseItem;
import br.com.tratomais.esb.interfaces.model.request.ClaimRequest;
import br.com.tratomais.esb.interfaces.model.response.ClaimResponse;

public interface IServicePolicy {

	public List<Occupation> listAllOccupation();

	public List<Profession> listAllProfession();

	public List<Activity> listAllActivity();

	public List<Inflow> listAllInflow(String inflowType);

	public Endorsement saveEndorsement(Endorsement endorsement);

	public Customer saveCustomer(Customer customer);

	public EndorsementOption loadUnlockableEndorsement(int endorsementId, int contractId);

	public Boolean unlockEndorsement(int endorsementId, int contractId);

	public Boolean cancellationProposal(int endorsementId, int contractId, int reason);

	public List<BillingMethod> listPaymentType(int productId, Integer paymentTermType, Date referenceDate);
		
	public Endorsement lastValidEndosementForContractNumber(Integer numContrato);
	
	public Endorsement lastValidEndosementForContractNumber(Integer numContrato, Date effectiveDate);
	
	/**
	 * method to return the premium pending from the last endorsement active
	 * 
	 * @param {@link Endorsement}
	 * @param {@link Date} Claim Date
	 * @return {@link AdjustmentsToEndorsement} installment pending and Expiry Date
	 */
	public AdjustmentsToEndorsement getPremiumPending(final Endorsement endorsement, final Date claimDate);
	
	/**
	 * Return action to the claim
	 * 
	 * @param {@link ClaimRequest}
	 * @return {@link ClaimAction}
	 */
	public ClaimAction getClaimAction(ClaimResponse claimResponse);
	
	/**
	 * Return the contract object
	 * 
	 * @param {@link BidInteger} policyNumber
	 * @param {@link Long} certificateNumber
	 * @return {@link Contract}
	 */
	public Contract getContractNumber(BigInteger policyNumber, Long certificateNumber, Date effectiveDate);

	public int cancellationClaimItem(int endorsementId, int contractId, int itemId, int itemStatus, Date cancelDate, String claimNumber, Date claimDate, Date claimNotification);

	public int cancellationClaimItemRiskGroup(int endorsementId, int contractId, int itemId, int riskGroupId, Date cancelDate, String claimNumber, Date claimDate, Date claimNotification);
	
	/**
	 * Recupera a lista de parcelas de um contrato e endosso.
	 * @param contractId
	 * @param endorsementId
	 * @return lista de Parcelas
	 */
	public List<Installment> getInstallmentForEndorsementId(int contractId,	int endorsementId);

	 /**
	  * @param collectionResponse
	  * @throws ServiceException
	  */	
	public void processInstallment(CollectionResponseItem[] collectionResponse) throws ServiceException;

	 /**
	  * @param collectionResponse
	  * @throws ServiceException
	  */
	public void receiveAdvice(CollectionResponseItem[] collectionResponse) throws ServiceException;

	 /**
	  * @param contractId
	  * @param effectiveDate
	  * @param cancelDate
	  * @throws ServiceException
	  */
	public void policyCancellationNoPayment(int contractId, Date effectiveDate, Date cancelDate) throws ServiceException;

	/**
	 * 
	 * @param endorsement
	 * @param checkDate
	 * @param issuanceType
	 * @return
	 */
	public ReturnClaimStateVerification claimStateVerification(int contractId, Date checkDate,int issuanceType); 

	 /**
	  * @param contractId
	  * @throws ServiceException
	  */
	public void policyReactivation(int contractId) throws ServiceException;

	 /**
	  * 
	  * @param claimRequests
	  * @throws ServiceException
	  */
	public void policyClaim(ClaimRequest[] claimRequests) throws ServiceException;

	 /**
	  * @param contractId
	  * @param issuanceType
	  * @param cancelDate
	  * @param cancelReason
	  * @throws ServiceException
	  */
	public void policyCancellation(int contractId, int issuanceType, Date cancelDate, int cancelReason) throws ServiceException;
	
	 /**
	  * 
	  * @param endorsement
	  * @return endorsement
	  * @throws ServiceException
	  */
	public Endorsement policyChangeRegister(Endorsement endorsement, Date referenceDate) throws ServiceException;
	
	 /**
	  * 
	  * @param endorsement
	  * @param referenceDate
	  * @param isRecalculation
	  * @param isProposal
	  * @param paymentOption 
	  * @return endorsement
	  * @throws ServiceException
	  */
	public Endorsement policyChangeTechnical(Endorsement endorsement, Date referenceDate, boolean isRecalculation, boolean isProposal, PaymentOption paymentOption) throws ServiceException;
	
	 /**
	  * @param contractId
	  * @param endorsementId
	  * @return endorsement
	  * @throws ServiceException
	  */
	public Endorsement policyRenewal(int contractId, int endorsementId) throws ServiceException;

	/**
	  * @param endorsement
	  * @param paymentOption
	  * @return endorsement
	  * @throws ServiceException
	  */
	public Endorsement policyRenewal(Endorsement endorsement, PaymentOption paymentOption) throws ServiceException;

	public Endorsement policyIssuance(Endorsement endorsement, PaymentOption paymentOption) throws ServiceException;

	public ErrorList listRenewalAlert(int contractId, int endorsementId, boolean resolved);

	public String getApplicationPropertyValue(int applicationId, String propertyName);

	public IdentifiedList getApplicationPropertyValue(IdentifiedList identifiedList, int applicationId,	String propertyName);

	public List<Profession> listProfessionByRefDate(Date refDate);

	public List<Occupation> listOccupationByRefDate(Date refDate);

	public List<Activity> listActivityByRefDate(Date refDate);

	public List<Inflow> listInflowByRefDate(Date refDate);

	public Endorsement policyCancellationByError(int contractId, Date cancelDate, int cancelReason) throws ServiceException;

	public Boolean unlockEndorsement(int endorsementId, int contractId,	Date unlockDate);

	public Endorsement policyCancellationByInsured(int contractId, Date cancelDate, int cancelReason) throws ServiceException;

	public boolean dispachCollectionPay(Installment installment) throws EInsuranceJMSException;
	
	public LockPendency createPendency(int contractId, int endorsementId, Date date, int pendencyId, int lockPendencyStatus);
	
	public void createAndFreePendency(int contractId, int endorsementId, Date date, int pendencyId, int lockPendencyStatus);
}
