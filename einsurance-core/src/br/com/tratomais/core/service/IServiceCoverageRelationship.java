package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.CoverageRelationship;

/**
 * @author jones.silva
 */
public interface IServiceCoverageRelationship extends IFindEntity<CoverageRelationship, Integer>, ICrudEntity<CoverageRelationship, Integer>{

	public List <CoverageRelationship> listCoverageRelationship(int coverageId, int parentId, int coverageRelationshipType, Date refDate);
	
	public List <CoverageRelationship> listCoverageRelationship(int coverageId, int coverageRelationshipType, Date refDate);
	
	public List <CoverageRelationship> listCoverageRelationship(int coverageId, Date refDate);
	
	public List<CoverageRelationship> listCoverageRelationshipByProductId(int productId, int planId, int coverageId, int coverageRelationshipType, Date refDate);
}
