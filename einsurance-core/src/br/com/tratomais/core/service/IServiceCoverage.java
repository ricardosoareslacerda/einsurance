/**
 * 
 */
package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.product.Coverage;

/**
 * @author eduardo.venancio
 */
public interface IServiceCoverage extends IFindEntity<Coverage, Integer>, ICrudEntity<Coverage, Integer> {

	public List<Coverage> findByObject(Integer oobjectId);
	
	public double applyCoverageRule(Item item, int ruleType, int productId, int coverageId, Date refDate);

}
