package br.com.tratomais.core.service;

import java.util.List;

import br.com.tratomais.core.dao.PersistentEntity;

public interface IFindEntity<T extends PersistentEntity, ID> {

	public List<T> findByName(String name);

	public T findById(ID id);

	public List<T> listAll();

}