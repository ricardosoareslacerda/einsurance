package br.com.tratomais.core.service;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.Country;
import br.com.tratomais.core.model.product.IdentifiedList;

public interface IServiceCountry extends IFindEntity< Country, Integer > {

	public IdentifiedList listCountry(IdentifiedList identifiedList);

	public List<Country> listCountryId(int countryId);

	public List<Country> listCountryByRefDate(Date refDate);
	
}
