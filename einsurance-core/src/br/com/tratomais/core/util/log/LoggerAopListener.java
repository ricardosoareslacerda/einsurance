package br.com.tratomais.core.util.log;

import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggerAopListener {

	// @Before("todosOsMetodosTratoMais()")
	// public void logaNoFinal(JoinPoint joinPoint) {
	// Logger logger = Logger.getLogger(joinPoint.getTarget().getClass());
	// String methodName = joinPoint.getSignature().getName();
	// String typeName = joinPoint.getSignature().getDeclaringTypeName();
	// logger.info("Executando: " + typeName + "." + methodName);
	// System.out.println("88888888888888888888888888");
	// }
	// @Pointcut("within(br.com.tratomais.core..*)")
	// public void todosOsMetodosTratoMais() {
	// }
}
