package br.com.tratomais.core.util.log;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

public class LoggerEventListener implements ApplicationListener {

	private static Log LOG = LogFactory.getLog(LoggerEventListener.class);

	public void onApplicationEvent(ApplicationEvent event) {
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("EVENTO: " + event.toString());
		}
	}
}