package br.com.tratomais.core.util.log;

import org.apache.log4j.Logger;

public class AuditLogger {
	
	private static final Logger logger = Logger.getLogger( AuditLogger.class );
	
	public void logAuditAction(String message){
		
		String[] msgSplit = message.split(":");
		
		StringBuilder strBuilder = new StringBuilder("");
		strBuilder.append("User: " + msgSplit[0]);
		strBuilder.append(" - Domain: " + msgSplit[1]);
		strBuilder.append(" - Module: " + msgSplit[2]);
		strBuilder.append(" - Action: " + msgSplit[3]);
		if (msgSplit.length > 4)
			strBuilder.append(" - Contract: " + msgSplit[4]);
		
		logger.info(strBuilder.toString());
		
	}
}
