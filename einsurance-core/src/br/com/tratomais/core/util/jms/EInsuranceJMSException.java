package br.com.tratomais.core.util.jms;

public class EInsuranceJMSException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 246967662967743924L;

	public EInsuranceJMSException() {
		super();
	}

	public EInsuranceJMSException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public EInsuranceJMSException(String arg0) {
		super(arg0);
	}

	public EInsuranceJMSException(Throwable arg0) {
		super(arg0);
	}

}
