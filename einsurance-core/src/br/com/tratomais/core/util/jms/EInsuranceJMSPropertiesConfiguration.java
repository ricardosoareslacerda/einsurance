package br.com.tratomais.core.util.jms;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;

import javax.jms.JMSException;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;


public class EInsuranceJMSPropertiesConfiguration extends EInsuranceJMSConnection {
	private static Logger logger = Logger.getLogger(EInsuranceJMSPropertiesConfiguration.class);

	private boolean hasFile;
	protected EInsuranceJMSPropertiesConfiguration(String filename) throws NamingException {
		super();
		File file = new File(filename);
		if (file.exists()){
			hasFile = true;
			Properties properties1 = new Properties();
			try {
				properties1.load(new FileInputStream(file));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				logger.error("JNDI configuration File not found",e);
			} catch (IOException e) {
				e.printStackTrace();
				logger.error("JNDI configuration File reading error",e);
			}
			InitialContext iniCtx = new InitialContext(properties1);
			setInitialContext(iniCtx);
		}
		else {
			logger.error("File configuration not found for JNDI transmitions");
			hasFile = false;
		}
	}

	/* (non-Javadoc)
	 * @see br.com.tratomais.core.util.jms.EInsuranceJMSConnection#finalize()
	 */
	@Override
	protected void finalize() throws JMSException {
		if (hasFile)
			super.finalize();
	}

	/* (non-Javadoc)
	 * @see br.com.tratomais.core.util.jms.EInsuranceJMSConnection#initialize(java.lang.String)
	 */
	@Override
	protected void initialize(String queue) throws NamingException, JMSException  {
		if (hasFile)
			super.initialize(queue);
	}

	/* (non-Javadoc)
	 * @see br.com.tratomais.core.util.jms.EInsuranceJMSConnection#sendMessage(java.io.Serializable)
	 */
	@Override
	protected void sendMessage(Serializable msg) throws JMSException {
		if (hasFile)
			super.sendMessage(msg);
	}
}
