package br.com.tratomais.core.util.jms;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

public class EInsuranceJMSConnection {
	
	public static EInsuranceJMSConnection newInstance() throws EInsuranceJMSException {
		try {
			return new EInsuranceJMSPropertiesConfiguration("C:/eInsurance/jndi.properties");
		} catch (NamingException e) {
			e.printStackTrace();
			throw new EInsuranceJMSException(e);
		}
	}
	/** 
	 * Logger for this class
	 */
	private static Logger logger = Logger.getLogger(EInsuranceJMSConnection.class);
	
	/**
	 * Can't be instancied from constructor
	 */
	protected EInsuranceJMSConnection() {}
	
	/**
	 * Initial context used by this class
	 */
	private InitialContext initialContext;
	
	/**
	 * 
	 */
	private QueueConnection conn;
	
	/**
	 * 
	 */
	private QueueSession session;
	
	/**
	 * 
	 */
	private Queue que;

	/**
	 * 
	 * @param initialContext
	 */
	public void setInitialContext(InitialContext initialContext){
		this.initialContext = initialContext;
	}

	/**
	 * 
	 * @param queue
	 * @throws NamingException
	 * @throws JMSException
	 */
	protected void initialize(String queue) throws NamingException, JMSException  {
		Object tmp = initialContext.lookup("ConnectionFactory");
		QueueConnectionFactory qcf = (QueueConnectionFactory) tmp;
		conn = qcf.createQueueConnection();
		que = (Queue) initialContext.lookup(queue);
		session = conn.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);
		conn.start();
	}

	/**
	 * @throws JMSException
	 */
	protected void finalize() throws JMSException {
		conn.stop();
		session.close();
		conn.close();
	}

	/**
	 * @param msg
	 * @throws JMSException
	 */
	protected void sendMessage(Serializable msg) throws JMSException {
		QueueSender send = session.createSender(que);
		ObjectMessage tm = session.createObjectMessage(msg);

		send.send(tm);
		send.close();
	}
	
	/**
	 * @param object
	 * @throws JMSException
	 * @throws NamingException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public final void sendObject(Object object, String queueName) throws JMSException, NamingException, FileNotFoundException, IOException {
		initialize(queueName);
		sendMessage((Serializable) object);
		finalize();
	}
	
	public static void main(String[] args) throws FileNotFoundException, JMSException, NamingException, IOException, EInsuranceJMSException {
		EInsuranceJMSConnection.newInstance().sendObject("WELLINGTON, OLHA O LOG", "queue/collection_request");
	}
}
