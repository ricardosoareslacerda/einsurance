package br.com.tratomais.core.util;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class NumberExtensive {

	/**
	 * @param args
	 */
	
	public static final String SYMBOL_R$ = "R$";
	public static final String SYMBOL_$ = "$";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double Valor = 1550.00;
		StringBuilder sbExtensiveNumber = new StringBuilder();
		
		if ( formatExtensiveNumber( Valor, "real", sbExtensiveNumber, SYMBOL_R$ ) ) {
			System.out.println(sbExtensiveNumber.toString());
		}
	}

	public static String formatExtensiveNumberToCertificate(double nNumber){
		StringBuilder sbExtensiveNumber = new StringBuilder();
		if ( formatExtensiveNumber( nNumber, "real", sbExtensiveNumber, SYMBOL_R$ ) ) {
			return sbExtensiveNumber.toString();
		}
		return "";
	}

	public static String formatExtensiveNumberToCertificate(double nNumber, String nTipoMoneda){
		StringBuilder sbExtensiveNumber = new StringBuilder();
		if ( formatExtensiveNumber( nNumber, "real", sbExtensiveNumber, nTipoMoneda.trim() ) ) {
			return sbExtensiveNumber.toString();
		}
		return "";
	}
    /**
	    * Metodo para formatar um n�mero por extenso, de acordo com o tipo passado.
	    * Limita��o do Metodo: O metodo suportar� apenas n�meros com duas casas decimais...
	    * Data de cria��o: (12/06/01 17:07:52)
	    * @return					- boolean		- Se tudo ok, true, sen�o false...
	    * @param	nNumero			- Double		- N�mero a ser transformado em extenso
	    * @param	sTipoEscrita	- int			- Real, Inteiro ou Porcento...
	    * @param	sbNomeUnidade	- StringBuffer	- Atributo com o retorno do m�todo, se ok cont�m o n�mero por extenso, sen�o cont�m uma msg de erro...
	    */
	    public static boolean formatExtensiveNumber(
	        double nNumero,
	        String sTipoEscrita,
	        StringBuilder sbNumeroPorExtenso,
	        String sTipoMoneda)
	    {
	        /************************************** Declara��o **************************************/
	        ArrayList< String > alUnidade;
	        ArrayList< String > alDezena;
	        ArrayList< String > alCentena;
	        ArrayList< String > alGrupo;
	        ArrayList< String > alTexto;
	        boolean bTemE = false;
	        DecimalFormat dfExtensao = new DecimalFormat("000000000000.00");
	        String sNumero = dfExtensao.format(nNumero);
	        String sParte = "";
	        String sAux = "";
	        String sAlimenta = "";
	        int nCont = 0;
	        int nTamanho = 0;
	        int nTeste = 0;
	        int nTestaDecimal = 0;
	        //	StringBuffer sbFinal;
	        /****************************************************************************************/
	        alUnidade = new ArrayList< String >();
	        alDezena = new ArrayList< String >();
	        alCentena = new ArrayList< String >();
	        alGrupo = new ArrayList< String >();
	        alTexto = new ArrayList< String >();
	        //	sbNumeroPorExtenso = new StringBuffer();
	        try
	        {
	            // Unidades...
	            alUnidade.add(0, "UM "); //0
	            alUnidade.add(1, "DOIS "); //1
	            alUnidade.add(2, "TRES "); //2
	            alUnidade.add(3, "QUATRO "); //3
	            alUnidade.add(4, "CINCO "); //4
	            alUnidade.add(5, "SEIS "); //5
	            alUnidade.add(6, "SETE "); //6
	            alUnidade.add(7, "OITO "); //7
	            alUnidade.add(8, "NOVE "); //8
	            alUnidade.add(9, "DEZ "); //9
	            alUnidade.add(10, "ONZE "); //10
	            alUnidade.add(11, "DOZE "); //11
	            alUnidade.add(12, "TREZE "); //12
	            alUnidade.add(13, "QUATORZE "); //13
	            alUnidade.add(14, "QUINZE "); //14
	            alUnidade.add(15, "DEZESSEIS "); //15
	            alUnidade.add(16, "DEZESSETE "); //16
	            alUnidade.add(17, "DEZOITO "); //17
	            alUnidade.add(18, "DEZENOVE "); //18
	            alUnidade.add(19, "VINTE "); //18
	            alUnidade.add(20, "VINTE E UM "); //18
	            alUnidade.add(21, "VINTE E DOIS "); //18
	            alUnidade.add(22, "VINTE E TR�S "); //18
	            alUnidade.add(23, "VINTE E QUATRO "); //18
	            alUnidade.add(24, "VINTE E CINCO "); //18
	            alUnidade.add(25, "VINTE E SEIS "); //18
	            alUnidade.add(26, "VINTE E SETE "); //18
	            alUnidade.add(27, "VINTE E OITO "); //18
	            alUnidade.add(28, "VINTE E NOVE "); //18	            
	            // Dezenas...
	            alDezena.add(0, ""); //0
	            alDezena.add(1, ""); //0
	            alDezena.add(2, "TRINTA "); //1
	            alDezena.add(3, "QUARENTA "); //2
	            alDezena.add(4, "CINQUENTA "); //3
	            alDezena.add(5, "SESSENTA "); //4
	            alDezena.add(6, "SETENTA "); //5
	            alDezena.add(7, "OITENTA "); //6
	            alDezena.add(8, "NOVENTA "); //7
	            // Centenas
	            alCentena.add(0, "CENTO "); //0
	            alCentena.add(1, "DOZENTOS "); //1
	            alCentena.add(2, "TREZENTOS "); //2
	            alCentena.add(3, "QUATROCENTOS "); //3
	            alCentena.add(4, "QUINHENTOS "); //4
	            alCentena.add(5, "SEISCENTOS "); //5
	            alCentena.add(6, "SETECENTOS "); //6
	            alCentena.add(7, "OITOCENTOS "); //7
	            alCentena.add(8, "NOVECENTOS "); //8
	            alCentena.add(9, "CEM "); //9
	            // Dividir o n�mero em grupos...
	            alGrupo.add(0, sNumero.substring(0, 3)); // Primeira parte
	            alGrupo.add(1, sNumero.substring(3, 6)); // Segunda Parte
	            alGrupo.add(2, sNumero.substring(6, 9)); // Terceira Parte
	            alGrupo.add(3, sNumero.substring(9, 12)); // Quarta Parte
	            alGrupo.add(4, sNumero.substring(13)); // Decimal
	            // Loop para calcular os grupos...
	            for (nCont = 0; nCont < 5; nCont++)
	            {
	                // Iniciar o vetor...
	                alTexto.add(nCont, "");
	                // Recuperar o grupos armazenados...
	                sParte = (String) alGrupo.get(nCont);
	                // Recuperar para teste...
	                nTeste = Integer.parseInt(sParte);
	                // Recuperar o tamanho...
	                if (nTeste < 10)
	                {
	                    nTamanho = 1;
	                }
	                else
	                {
	                    if (nTeste < 100)
	                    {
	                        nTamanho = 2;
	                    }
	                    else
	                    {
	                        if (nTeste < 1000)
	                        {
	                            nTamanho = 3;
	                        }
	                    }
	                }

	                //Testar o tamanho...
	                if (nTamanho == 3)
	                {
	                    if (!sParte.substring(1).equals("00"))
	                    {
	                        sAlimenta =
	                            (String) alTexto.get(nCont)
	                                + alCentena.get(
	                                    Integer.parseInt(sParte.substring(0, 1))
	                                        - 1)
	                                + "";
	                        alTexto.set(nCont, sAlimenta);
	                        bTemE = true;
	                        nTamanho = 2;
	                    }
	                    else
	                    {
	                        if (Integer.parseInt(sParte.substring(0, 1)) == 1)
	                        {
	                            sAlimenta =
	                                (String) alTexto.get(nCont) + alCentena.get(9);
	                            alTexto.set(nCont, sAlimenta);
	                        }
		                    else if (sParte.substring(1).equals("00"))
		                    {
		                        sAlimenta =
		                            (String) alTexto.get(nCont)
		                                + alCentena.get(
		                                    Integer.parseInt(sParte.substring(0, 1))
		                                        - 1);
		                        alTexto.set(nCont, sAlimenta);
		                        bTemE = true;
		                    }	                        
	                        else
	                        {
	                            sAlimenta =
	                                (String) alTexto.get(nCont)
	                                    + alCentena.get(
	                                        Integer.parseInt(
	                                            sParte.substring(0, 1))
	                                            - 1);
	                            alTexto.set(nCont, sAlimenta);
	                        }
	                    }
	                }
	                
	                if (nTamanho == 2)
	                {
	                    if (nCont == 4) // Est� no decimal...
	                        nTestaDecimal = Integer.parseInt(sParte);
	                    else
	                        nTestaDecimal = Integer.parseInt(sParte.substring(1));
	                    
	                    if (nTestaDecimal < 30)
	                    {
	                    	if(nCont == 4){
		                        sAlimenta =
		                            (String) alTexto.get(nCont)
		                                + alUnidade.get(
		                                    Integer.parseInt(sParte) - 1);
		                        alTexto.set(nCont, sAlimenta);	                    		
	                    	}else{
		                        sAlimenta =
		                            (String) alTexto.get(nCont)
		                                + alUnidade.get(
		                                    Integer.parseInt(sParte.substring(1)) - 1);
		                        alTexto.set(nCont, sAlimenta);	                    		
	                    	}
	                    }
	                    else
	                    {
	                        if (nCont == 4)
	                        {
	                            sAlimenta =
	                                (String) alTexto.get(nCont)
	                                    + alDezena.get(
	                                        Integer.parseInt(
	                                            sParte.substring(0, 1))
	                                            - 1);
	                            alTexto.set(nCont, sAlimenta);
	                        }
	                        else
	                        {
	                            sAlimenta =
	                                (String) alTexto.get(nCont)
	                                    + alDezena.get(
	                                        Integer.parseInt(
	                                            sParte.substring(1, 2))
	                                            - 1);
	                            alTexto.set(nCont, sAlimenta);
	                        }
	                    
	                        if (nCont == 4)
	                        {
	                            if (!sParte.substring(1).equals("0"))
	                            {
	                                sAlimenta = (String) alTexto.get(nCont) + "E ";
	                                alTexto.set(nCont, sAlimenta);
	                                nTamanho = 1;
	                                bTemE = true;
	                            }
	                        }
	                        else
	                        {
	                            if (!sParte.substring(2).equals("0"))
	                            {
	                                sAlimenta = (String) alTexto.get(nCont) + "E ";
	                                alTexto.set(nCont, sAlimenta);
	                                nTamanho = 1;
	                                bTemE = true;
	                            }
	                        }
	                    }
	                }
	                
	                if (nTamanho == 1)
	                {
	                    if (nTeste > 0)
	                    {                 	
	                        if (nCont == 4)
	                        {	            
	                            if (sParte.substring(0,1).equals("0"))
	                            {
	                                sAlimenta = "ZERO "; 
		                            sAlimenta +=
		                                (String) alTexto.get(nCont)
		                                    + alUnidade.get(
		                                        Integer.parseInt(sParte.substring(1))
		                                            - 1);	                                
	                            }else{
		                            sAlimenta =
		                                (String) alTexto.get(nCont)
		                                    + alUnidade.get(
		                                        Integer.parseInt(sParte.substring(1))
		                                            - 1);	                            	
	                            }
	                        }
	                        else
	                        {
	                            sAlimenta =
	                                (String) alTexto.get(nCont)
	                                    + alUnidade.get(
	                                        Integer.parseInt(sParte.substring(2))
	                                            - 1);
	                        }
	                        alTexto.set(nCont, sAlimenta);
	                    }
	                }
	            }
	            
	            // Verificar se tem somente centavos...
	            if (Integer
	                .parseInt(
	                    (String) alGrupo.get(0)
	                        + (String) alGrupo.get(1)
	                        + (String) alGrupo.get(2)
	                        + (String) alGrupo.get(3))
	                == 0
	                && Integer.parseInt((String) alGrupo.get(4)) != 0)
	            {
	                // Testar qual a termina��o (real ou percentual)
	                if (sTipoEscrita.equalsIgnoreCase("real"))
	                {
	                    if (((String) alGrupo.get(4)).substring(0).equals("1"))
	                    {
	                        sAux = "CENTAVO ";
	                    }
	                    else
	                    {
	                        sAux = "CENTAVOS ";
	                    }
	                }
	                else
	                {
	                    if (((String) alGrupo.get(4)).substring(0).equals("1"))
	                    {
	                        sAux = "CENT�SIMO ";
	                    }
	                    else
	                    {
	                        sAux = "CENT�SIMOS ";
	                    }
	                }
	                sbNumeroPorExtenso.append(alTexto.get(4) + sAux);
	            }
	            else
	            {
	                // Verificar se coloca o E
	                if (bTemE
	                    || Integer.parseInt(
	                        (String) alGrupo.get(0)
	                            + (String) alGrupo.get(1)
	                            + (String) alGrupo.get(2))
	                        == 0
	                    || Integer.parseInt((String) alGrupo.get(3)) == 0)
	                {
	                    sbNumeroPorExtenso.append("");
	                }
	                else
	                {
	                    sbNumeroPorExtenso.append("Y ");
	                }
	                
	                // Escrever...
	                // Bilh�es
	                if (Integer.parseInt((String) alGrupo.get(0)) != 0)
	                {
	                    sbNumeroPorExtenso.append(alTexto.get(0));
	                    if (Integer.parseInt((String) alGrupo.get(0)) > 1)
	                    {
	                        sbNumeroPorExtenso.append("BILH�ES ");
	                    }
	                    else
	                    {
	                        sbNumeroPorExtenso.append("BILH�O ");
	                    }
	                }
	                
	                // Milh�es
	                if (Integer.parseInt((String) alGrupo.get(1)) != 0)
	                {
	                    sbNumeroPorExtenso.append(alTexto.get(1));
	                    if (Integer.parseInt((String) alGrupo.get(1)) > 1)
	                    {
	                        sbNumeroPorExtenso.append("MILH�ES ");
	                    }
	                    else
	                    {
	                        sbNumeroPorExtenso.append("MILH�O ");
	                    }
	                }
	                
	                // Milhar
	                if (Integer.parseInt((String) alGrupo.get(2)) != 0)
	                {
	                    sbNumeroPorExtenso.append(alTexto.get(2));
	                    sbNumeroPorExtenso.append("MIL ");
	                }
	                
	                // Unidade/Dezena
	                sbNumeroPorExtenso.append(alTexto.get(3));
	                // Testar qual a termina��o (real Inteiro ou percentual)
	                if (sTipoEscrita.equalsIgnoreCase("real"))
	                {
	                    // Coloca "DE" se necess�rio...
	                    if (Integer
	                        .parseInt(
	                            (String) alGrupo.get(2) + (String) alGrupo.get(3))
	                        == 0)
	                    {
	                        sbNumeroPorExtenso.append("DE ");
	                    }
	                    // Coloca "REAIS" ou "REAL"
	                    if (Integer
	                        .parseInt(
	                            (String) alGrupo.get(0)
	                                + (String) alGrupo.get(1)
	                                + (String) alGrupo.get(2)
	                                + (String) alGrupo.get(3))
	                        == 1)
	                    {
	                        sbNumeroPorExtenso.append(consultarMoeda(sTipoMoneda, false));
	                    }
	                    else
	                    {
	                        sbNumeroPorExtenso.append(consultarMoeda(sTipoMoneda, true));
	                    }
	                    
	                    // Testa se tem casas decimais...
	                    if (Integer.parseInt((String) alGrupo.get(4)) != 0)
	                    {
	                        // Coloca os valores decimais...
	                        sbNumeroPorExtenso.append(
	                            "E " + (String) alTexto.get(4));
	                        // Coloca centavos...
	                        if (Integer.parseInt((String) alGrupo.get(4)) == 1)
	                        {
	                            sbNumeroPorExtenso.append("CENTAVO ");
	                        }
	                        else
	                        {
	                            sbNumeroPorExtenso.append("CENTAVOS ");
	                        }
	                    }
	                    // verifica se � do tipo de escrita � inteiro	
	                }
	                else if (sTipoEscrita.equalsIgnoreCase("Inteiro"))
	                {
	                    if (Integer.parseInt((String) alGrupo.get(4)) != 0)
	                    {
	                        sbNumeroPorExtenso.append("Y " + alTexto.get(4));
	                    }
	                    else
	                        sbNumeroPorExtenso.append(alTexto.get(4));

	                }
	                else
	                {
	                    // Coloca "PORCENTO"
	                    sbNumeroPorExtenso.append("PORCENTO ");
	                    // Testa se tem casas decimais...
	                    if (Integer.parseInt((String) alGrupo.get(4)) != 0)
	                    {
	                        // Coloca os valores decimais...
	                        sbNumeroPorExtenso.append("Y " + alTexto.get(4));
	                        // Coloca CENT�SIMOS...
	                        if (Integer.parseInt((String) alGrupo.get(4)) == 1)
	                        {
	                            sbNumeroPorExtenso.append("CENT�SIMO ");
	                        }
	                        else
	                        {
	                            sbNumeroPorExtenso.append("CENT�SIMOS ");
	                        }
	                    }
	                }
	            }
	            // Retornar...
	            //		sbNumeroPorExtenso.append(sbFinal);
	            return true;
	        }
	        catch (Exception eError)
	        {
	            sbNumeroPorExtenso.append(eError.getMessage());
	            return false;
	        }
	    }
	    
	    public static String consultarMoeda(String tipo, boolean isPlural){
	    	String moeda = "";
	    	
	    	if(tipo.equalsIgnoreCase(SYMBOL_R$)){
	    		moeda = isPlural? "REAIS " : "REAL ";	    		
	    	} else{
	    		moeda = isPlural? "DOLARES " : "DOLAR ";		    		
	    	}

	    	return moeda;
	    }
	
}
