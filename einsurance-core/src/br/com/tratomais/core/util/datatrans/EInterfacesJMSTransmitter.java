package br.com.tratomais.core.util.datatrans;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.tratomais.core.util.jms.EInsuranceJMSConnection;
import br.com.tratomais.core.util.jms.EInsuranceJMSException;
import br.com.tratomais.esb.interfaces.model.response.ClaimResponse;

public class EInterfacesJMSTransmitter implements Runnable {
	private static final long TIMEOUT = 1000 * 60; // milisegundos

	/** 4 logging */
	private Logger logger = Logger.getLogger(EInterfacesJMSTransmitter.class);
	
	/** Lista sincronizada de Collections a serem enviados */
	private List<List<ClaimResponse>> lista = Collections.synchronizedList(new LinkedList<List<ClaimResponse>>());
	
	/** Indica se est� rodando ou n�o */
	private boolean running;
	
	/** Conex�o JMS para o eInterfaces */
	private EInsuranceJMSConnection jmsConnection;
	
	/**
	 * @return the running
	 */
	public boolean isRunning() {
		return running;
	}

	/**
	 * Adiciona um novo envelope para ser transmitido
	 * @param envelope
	 */
	public EInterfacesJMSTransmitter addClaimResponse(List<ClaimResponse> envelope) {
		synchronized (lista) {
			lista.add(envelope);
			lista.notifyAll();
		}
		synchronized (this) {			
			this.notifyAll();
		}
		return this;
	}
	
	/**
	 * Preenche um novo objeto claimResponses e coloca na fila para transmiss�o
	 * @param claimResponses
	 * @return same instance
	 * @see br.com.tratomais.esb.interfaces.model.response.ClaimResponse
	 */
	public EInterfacesJMSTransmitter fillAndAdd(final List<ClaimResponse> claimResponses) {
		this.addClaimResponse(claimResponses);
		return this;
	}
	
	/**
	 * @return same instance
	 */
	private List<ClaimResponse> getNext(){
		List<ClaimResponse> envelope = null;
		synchronized (this.lista) {
			if (lista.size() > 0){
				envelope = lista.get(0);
				lista.remove(envelope);
			}
			lista.notifyAll();
		}
		synchronized (this) {			
			this.notifyAll();
		}
		return envelope;
	}

	public EInterfacesJMSTransmitter() throws EInsuranceJMSException {
		jmsConnection = EInsuranceJMSConnection.newInstance();
	}
	
	/**
	 * Usado para todar na thread
	 */
	@Override
	public void run() {
		while(running){
			List<ClaimResponse> claimResponses = getNext();
			if (claimResponses == null){
				try {
					synchronized (this) {
						this.wait();	
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			else {
				try {
					jmsConnection.sendObject(claimResponses, "queue/zurClaim_response_gw");						
				} catch (Throwable e) {
					logger.error("Error adding collection", e);
					this.addClaimResponse(claimResponses);
					try {
						this.wait(TIMEOUT);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				}
			}
		}		
	}

	/**
	 * Limpa a fila de objetos
	 * @return same instance
	 */
	public EInterfacesJMSTransmitter clearQueue(){
		synchronized (this.lista) {
			lista.clear();
		}
		lista.notifyAll();
		return this;
	}
	
	/**
	 * Inicia o processo
	 * @return same instance
	 */
	public EInterfacesJMSTransmitter start() {
		if (! running){
			running = true;
			new Thread(this).start();
		}
		return this;
	}
	
	/**
	 * Para o processo
	 * @return same instance
	 */
	public EInterfacesJMSTransmitter stop() {
		running = false;
		clearQueue();
		return this;
	}

	/**
	 * Inst�ncia
	 */
	private static EInterfacesJMSTransmitter instance;

	/**
	 * @return Inst�ncia de eInterfacesJMSTransmitter n�o rodando
	 * @throws EInsuranceJMSException Caso ocorra erro de conex�o JMS
	 */
	public synchronized static EInterfacesJMSTransmitter getInstance() throws EInsuranceJMSException {
		if(instance == null){
			instance = new EInterfacesJMSTransmitter();
		}
		return instance;
	}
}
