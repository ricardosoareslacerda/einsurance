package br.com.tratomais.core.util.datatrans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Customer;
import br.com.tratomais.core.model.customer.Partner;
import br.com.tratomais.core.model.customer.Phone;
import br.com.tratomais.core.model.policy.Contract;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.Installment;
import br.com.tratomais.core.model.product.DataOption;
import br.com.tratomais.core.model.product.Product;
import br.com.tratomais.core.util.StringUtil;
import br.com.tratomais.core.util.jms.EInsuranceJMSConnection;
import br.com.tratomais.core.util.jms.EInsuranceJMSException;
import br.com.tratomais.esb.collections.model.request.CollectionDetail;
import br.com.tratomais.esb.collections.model.request.CollectionEnvelope;
import br.com.tratomais.esb.collections.model.request.DetailAttribute;

public class ECollectionJMSTransmitter implements Runnable {

	public ECollectionJMSTransmitter() throws EInsuranceJMSException {
		jmsConnection = EInsuranceJMSConnection.newInstance();
	}

	private static final long TIMEOUT = 1000 * 60; // milisegundos

	/** 4 logging */
	private Logger logger = Logger.getLogger(ECollectionJMSTransmitter.class);
	
	/** Lista sincronizada de Collections a serem enviados */
	private List<CollectionEnvelope>  lista = Collections.synchronizedList(new LinkedList<CollectionEnvelope>());
	
	/** Indica se est� rodando ou n�o */
	private boolean running;
	
	/** Conex�o JMS para o ECollection */
	private EInsuranceJMSConnection jmsConnection;
	
	/**
	 * @return the running
	 */
	public boolean isRunning() {
		return running;
	}
	
	/**
	 * Adiciona um novo envelope para ser transmitido
	 * @param envelope
	 */
	public ECollectionJMSTransmitter addCollectionEnvelope(CollectionEnvelope envelope) {
		synchronized (lista) {
			lista.add(envelope);
			lista.notifyAll();
		}
		synchronized (this) {			
			this.notifyAll();
		}
		return this;
	}
	
	/**
	 * Preenche um novo objeto CollectionEnvelope e coloca na fila para transmiss�o
	 * @param installments
	 * @param partner
	 * @param personType
	 * @param phoneType
	 * @param channel
	 * @param isCancellation
	 * @return same instance
	 * @see br.com.tratomais.esb.collections.model.request.CollectionEnvelope
	 */
	public ECollectionJMSTransmitter fillAndAdd(final List<Installment> installments, Partner partner, DataOption personType, DataOption phoneType, Channel channel, Product product, final boolean isCancellation) {
		final int applicationId = 1;
		final String applicationDescricao = "I"; //eInsurance
		final Endorsement endorsement = installments.get(0).getEndorsement();
		final Contract contract = endorsement.getContract();

		Customer policyHolder = endorsement.getCustomerByPolicyHolderId();
		Phone phone = policyHolder.getFirstAddress().getFirstPhone();
		
		List<DetailAttribute> basicAttributes = new ArrayList<DetailAttribute>();
		basicAttributes.add(new DetailAttribute(DetailAttribute.CERTIFICATE_NUMBER, contract.getCertificateNumber().toString()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.POLICY_NUMBER, contract.getPolicyNumber().toString()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.PARTNER_ID, contract.getPartnerId()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.ENDORSEMENT_ID, endorsement.getId().getEndorsementId()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.PROPOSAL_NUMBER, endorsement.getProposalNumber().toString()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.CONTRACT_ID, endorsement.getId().getContractId()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.CHANNEL_CODE, channel.getExternalCode())); 
		basicAttributes.add(new DetailAttribute(DetailAttribute.PHONE_TYPE, phoneType.getFieldDescription()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.PHONE_NUMBER, phone.getPhoneNumber()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.PARTNER_NAME, partner.getNickName()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.POLICY_HOLDER_PERSON_TYPE, personType.getFieldValue()));
		basicAttributes.add(new DetailAttribute(DetailAttribute.PRODUCT_CODE, product.getProductCode()));
		if (contract.getExternalKey() != null && !"".equalsIgnoreCase(contract.getExternalKey().trim()))
			basicAttributes.add(new DetailAttribute(DetailAttribute.EXTERNAL_KEY, contract.getExternalKey().trim()));
		
		CollectionEnvelope collectionEnvelope = new CollectionEnvelope((isCancellation ? CollectionEnvelope.ACTION_CANCELLATION : CollectionEnvelope.ACTION_EFFECTIVE));
		collectionEnvelope.setApplicationId(applicationId);
		collectionEnvelope.setLotCode(endorsement.getProposalNumber().toString());
		
		for (Installment readingInstallment : installments) {
			CollectionDetail collectionDetail = collectionEnvelope.addCollectionDetail();
			StringBuilder clientReference = new StringBuilder();
			StringBuilder billingReference = new StringBuilder();
			List<DetailAttribute> attributesSpecified = new ArrayList<DetailAttribute>();
			
			attributesSpecified.addAll(basicAttributes);
			attributesSpecified.add(new DetailAttribute(DetailAttribute.INSTALLMENT_ID, readingInstallment.getId().getInstallmentId()));
			if (readingInstallment.getCommission() != null && !readingInstallment.getCommission().equals(0.0))
				attributesSpecified.add(new DetailAttribute(DetailAttribute.COMMISSION_VALUE, readingInstallment.getCommission().toString()));
			if (readingInstallment.getLabour() != null && !readingInstallment.getLabour().equals(0.0))
				attributesSpecified.add(new DetailAttribute(DetailAttribute.LABOUR_VALUE, readingInstallment.getLabour().toString()));
			collectionDetail.setDetailAttributes(attributesSpecified);
			
			collectionDetail.setBillingMethodId(readingInstallment.getBillingMethodId());
			collectionDetail.setInstallmentType(readingInstallment.getInstallmentType());
			collectionDetail.setPaymentType(readingInstallment.getPaymentType());
			
			// Client Reference
			clientReference.append(applicationDescricao);
			clientReference.append(StringUtil.fill(contract.getPolicyNumber().toString(), "0", 13, true));
			clientReference.append(StringUtil.fill(contract.getCertificateNumber().toString(), "0", 6, true));
			collectionDetail.setClientReference(clientReference.toString());
			
			// Billing Reference
			billingReference.append(StringUtil.fill(endorsement.getProposalNumber().toString() + "", "0", 6, true));
			billingReference.append(StringUtil.fill(readingInstallment.getId().getInstallmentId() + "", "0", 2, true));
			collectionDetail.setBillingReference(billingReference.toString());
			
			collectionDetail.setDueDate(readingInstallment.getIssueDate());
			collectionDetail.setLimitDate(readingInstallment.getDueDate());
			collectionDetail.setCancelDate(readingInstallment.getCancelDate());
			if (isCancellation && collectionDetail.getCancelDate() == null)
				collectionDetail.setCancelDate(new Date());
			
			collectionDetail.setInstallmentValue(readingInstallment.getInstallmentValue());
			collectionDetail.setCurrencyId(contract.getCurrencyId());
			collectionDetail.setCurrencyDate(readingInstallment.getEndorsement().getCurrencyDate());
			collectionDetail.setConversionRate(readingInstallment.getEndorsement().getConversionRate());
			
			collectionDetail.setHolderName(policyHolder.getName());
			collectionDetail.setHolderDocumentNumber(policyHolder.getDocumentNumber());
			collectionDetail.setHolderDocumentType(policyHolder.getDocumentType());
			
			collectionDetail.setBankNumber(readingInstallment.getBankNumber());
			collectionDetail.setBankAgencyNumber(readingInstallment.getBankAgencyNumber());
			collectionDetail.setBankAccountNumber(readingInstallment.getBankAccountNumber());
			collectionDetail.setBankCheckNumber(readingInstallment.getBankCheckNumber());
			
			collectionDetail.setCardType(readingInstallment.getCardType());
			collectionDetail.setCardBrandType(readingInstallment.getCardBrandType());
			collectionDetail.setCardNumber(readingInstallment.getCardNumber());
			collectionDetail.setCardExpirationDate(readingInstallment.getCardExpirationDate());
			collectionDetail.setCardHolderName(readingInstallment.getCardHolderName());
			
			collectionDetail.setOtherAccountNumber(readingInstallment.getOtherAccountNumber());
			collectionDetail.setInvoiceNumber(readingInstallment.getInvoiceNumber());
			
			collectionDetail.setPaymentNotified(readingInstallment.isPaymentNotified());
		}
		
		this.addCollectionEnvelope(collectionEnvelope);
		return this;
	}
	
	/**
	 * @return same instance
	 */
	private CollectionEnvelope getNext(){
		CollectionEnvelope envelope = null;
		synchronized (this.lista) {
			if (lista.size() > 0){
				envelope = lista.get(0);
				lista.remove(envelope);
			}
			lista.notifyAll();
		}
		synchronized (this) {			
			this.notifyAll();
		}
		return envelope;
	}

	/**
	 * Usado para todar na thread
	 */
	@Override
	public void run() {
		while(running){
			CollectionEnvelope collectionEnvelope = getNext();
			if (collectionEnvelope == null){
				try {
					synchronized (this) {
						this.wait();	
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			else {
				try {
					jmsConnection.sendObject(collectionEnvelope, "queue/collection_request");						
				} catch (Throwable e) {
					logger.error("Error adding collection", e);
					this.addCollectionEnvelope(collectionEnvelope);
					try {
						this.wait(TIMEOUT);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				}
			}
		}		
	}

	/**
	 * Limpa a fila de objetos
	 * @return same instance
	 */
	public ECollectionJMSTransmitter clearQueue(){
		synchronized (this.lista) {
			lista.clear();
		}
		lista.notifyAll();
		return this;
	}
	
	/**
	 * Inicia o processo
	 * @return same instance
	 */
	public ECollectionJMSTransmitter start() {
		if (! running){
			running = true;
			new Thread(this).start();
		}
		return this;
	}
	
	/**
	 * Para o processo
	 * @return same instance
	 */
	public ECollectionJMSTransmitter stop() {
		running = false;
		clearQueue();
		return this;
	}

	/**
	 * Inst�ncia
	 */
	private static ECollectionJMSTransmitter instance;

	/**
	 * @return Inst�ncia de ECollectionJMSTransmitter n�o rodando
	 * @throws EInsuranceJMSException Caso ocorra erro de conex�o JMS
	 */
	public synchronized static ECollectionJMSTransmitter getInstance() throws EInsuranceJMSException {
		if(instance == null){
			instance = new ECollectionJMSTransmitter();
		}
		return instance;
	}
}
