package br.com.tratomais.core.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import br.com.tratomais.general.utilities.DateUtilities;

public class Utilities {

	public static void main(String[] args) throws Exception {
		final String userPass = "";
		System.out.println("Hahs do " + userPass + "-> " + Utilities.getHashSha(userPass));
	}
	
	public static String getHashSha(String msg)  {

		MessageDigest md;
		StringBuffer msgRet = new StringBuffer() ;
		try {
	        md = MessageDigest.getInstance("sha");
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalArgumentException();
			//throw new SelogException("Erro ao buscar o algoritmo de criptografia MD5 \n" + e.getMessage());			// TODO: handle exception
		}

        md.update(msg.getBytes());

        byte[] msgHash = md.digest();
        
        for (int i = 0; i < msgHash.length; i++) {
			msgRet.append( new String("0" + Integer.toHexString(msgHash[i])).substring(Integer.toHexString(msgHash[i]).length()-1) );
		}
        
		return  msgRet.toString() ;
	}

    public static Object cloneObject(Object o) 
    {
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        ObjectOutputStream out;
        ObjectInputStream in = null;
		try {
			out = new ObjectOutputStream(bOut);
	        out.writeObject(o);
	        ByteArrayInputStream bIn = new ByteArrayInputStream(bOut.toByteArray());
	        in = new ObjectInputStream(bIn);
		} catch ( IOException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			return (in.readObject());
		} catch ( IOException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch ( ClassNotFoundException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    }

    /**
     * Recebe dois objetos diferentes, mas que contenham campos de getter e setter em comum, copiando os valores do obj1 para obj2
     * @param obj1
     * @param obj2
     * @throws SecurityException
     * @throws NoSuchMethodException
     * @throws ClassNotFoundException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    
	public static void copyToObjectDifferent(Object obj1,  Object obj2){
		copyToObjectDifferent(obj1, obj2, null);
	}
	
	@SuppressWarnings("unchecked")
	public static void copyToObjectDifferent(Object obj1,  Object obj2, String [] ignoreProperties) {
		Class clazz = obj2.getClass();
		
		Method [] listMethods = clazz.getMethods();
		Method getter = null;
		
		for(Method method : listMethods){
			String getName = "";
			if(method.getName().startsWith("set")){
				if (ignoreProperties != null)
					if (ignorar(ignoreProperties, method.getName()))
						continue;
				
				getter = null;
				getName = method.getName().replaceFirst("set", "get");
				if(getName.endsWith("ID")) 
					getName = getName.substring(0, getName.length()-1)+ "d";
				else if(getName.endsWith("Id")) 
					getName = getName.substring(0, getName.length()-1)+ "D";
				
				try {
					getter = obj1.getClass().getMethod(getName);
				} catch (NoSuchMethodException ex){
					System.out.println(getName);
					try{
						getter = obj1.getClass().getMethod(getName.replaceFirst("get", "is"));
					} catch (NoSuchMethodException ex1){
						try {
							if(getName.toUpperCase().endsWith("ID"))
								getter = obj1.getClass().getMethod(getName.substring(0, getName.length()-2));
							else 
								getter = obj1.getClass().getMethod(getName + "ID");
						} catch (NoSuchMethodException ex2){
							try{
								getter = obj1.getClass().getMethod(method.getName().replaceFirst("set", "get"));
							}catch(Exception ex3){}
						}
					}
				}
				try{
					if(method.getParameterTypes()[0].getName().equals(getter.getReturnType().getName())){
						method.invoke(obj2, getter.invoke(obj1));
					} else {
						Object valueObj1 = getter.invoke(obj1);

						getName = method.getName().replaceFirst("set", "get");
						Object valueObj2 = null;
						try{
							valueObj2 = obj2.getClass().getMethod(getName).invoke(obj2);
						} catch (NoSuchMethodException ex){
							try{
								valueObj2 = obj2.getClass().getMethod(getName.replaceFirst("get", "is")).invoke(obj2);
							} catch (Exception e){}
						}

						if(valueObj2 == null){
							valueObj2 = populateNullValue(obj2.getClass().getMethod(getName).getReturnType());
						}

						valueObj2 = convertValue(valueObj1, valueObj2);
						method.invoke(obj2, valueObj2);
					}
				} catch (Exception ex){}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Object populateNullValue(Class clazz) throws ClassNotFoundException{
		Object obj = null;
		
		try {
			obj = clazz.getConstructor(String.class).newInstance("0");
			if(obj instanceof String){
				obj = "";
			}
		} catch (Exception e) {	
			try {
				obj = DatatypeFactory.newInstance().newXMLGregorianCalendar();
			} catch (Exception e1) {}
		}
		
		return obj;
	}
	
	public static Object convertValue(Object obj1, Object obj2){
		if(obj2 instanceof BigDecimal){
			obj2 = new BigDecimal(obj1.toString());
		} else if(obj2 instanceof BigInteger){
			obj2 = new BigInteger(obj1.toString());
		} else if(obj2 instanceof Double){
			obj2 = Double.valueOf(obj1.toString());
		} else if(obj2 instanceof Long){
			obj2 = Long.valueOf(obj1.toString());
		} else if(obj2 instanceof Integer){
			obj2 = Integer.valueOf(obj1.toString());
		} else if(obj2 instanceof Short){
			obj2 = Short.valueOf(obj1.toString());
		} else if(obj2 instanceof String){
			obj2 = obj1.toString();
		} else if(obj2 instanceof XMLGregorianCalendar){
			try {
				obj2 = DateUtilities.dateToXMLGregorianCalendar((Date)obj1);
			} catch (DatatypeConfigurationException e) { }
		} else if(obj2 instanceof Boolean){
			obj2 = Boolean.valueOf(obj1.toString().trim().matches("(true|1).*")? true:false);
		}
		
		return obj2;
	}
	
	public static Object convertNullValue(Object value, Object valueDefault){
		return value != null? value : valueDefault;
	}
	
	private static boolean ignorar(String[] ignoreProperties, String fieldName) {
		
		String parteNomeCampo = fieldName.startsWith("is")?fieldName.substring(2):fieldName.substring(3); 
		char[] charArray = parteNomeCampo.toCharArray();
		charArray[0] = Character.toLowerCase(charArray[0]);
		String nomeCampo = new String(charArray);
		
		if (ignoreProperties != null)
			for(String nome: ignoreProperties)
				if(nomeCampo.equals(nome))
					return true;
		return false;
	}
	
	public static boolean validateBankAccountVE(final Integer bankNumber, final String accountNumber) {
		if (accountNumber == null || accountNumber.length() != 20) {
			return false;
		}
		
		String codBco = accountNumber.substring(0,4).toString();
		String codAgencia = accountNumber.substring(4,8).toString();
		String codDigito = accountNumber.substring(8,10).toString();
		String codConta = accountNumber.substring(10,accountNumber.length()).toString();
		
		if (bankNumber == null || !bankNumber.equals(new Integer(codBco))) {
			return false;
		}
		
		String primerDigit = calculoModulo11("000000" + codBco + codAgencia);
		String secondDigit = calculoModulo11(codAgencia + codConta);
		
		if (primerDigit != null && secondDigit != null) {
			if ((primerDigit.toString() + secondDigit.toString()).equalsIgnoreCase(codDigito)) {
				return true;
			}
		}
		
		return false;
	}

	private static String calculoModulo11(String accountNumber) {
		if (accountNumber == null || accountNumber.length() != 14) {
			return null;
		}
		
		String peso14 = "23456723456723";
	    int progressivo = 0;
	    int divisor = 0;
	    String digito;
	    
	    for (int regressivo = peso14.length() - 1; regressivo >= 0; regressivo = regressivo - 1) {
	    	String valueProgressivo = ""+accountNumber.charAt(regressivo);
	    	String valueRegressivo = ""+peso14.charAt(progressivo);
			divisor = divisor + ((new Integer(valueRegressivo)) * (new Integer(valueProgressivo)));
			progressivo = progressivo + 1;
		}
	    
	    int resto = (divisor % 11);
	    int c = (11 - resto);
	    if (c > 9) {
	    	digito = (new Integer(c - 10)).toString();
	    }
	    else {
	    	digito = (new Integer(c)).toString();
	    }
		return digito;
	}

	public static Date parseDate(String yyyyMMdd) {
		try {
			SimpleDateFormat out = new SimpleDateFormat("yyyyMMdd");
	    	Date result = out.parse(yyyyMMdd);
	    	return result;
		} catch (Exception e) {
			return null;
		}
	}
}
