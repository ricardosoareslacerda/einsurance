package br.com.tratomais.core.util;

import java.math.BigDecimal;
import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.Months;
import org.joda.time.Years;

/**
 * Date utilities class
 * @author luiz.alberoni
 */
public final class DateUtilities {
    /**
     * All minutes have this many milliseconds except the last minute of the day on a day defined with
     * a leap second.
     */
    public static final long MILLISECS_PER_MINUTE = 60*1000;
    
    /**
     * Number of milliseconds per hour, except when a leap second is inserted.
     */
    public static final long MILLISECS_PER_HOUR   = 60*MILLISECS_PER_MINUTE;
    
    /**
     * Number of leap seconds per day expect on 
     * <BR/>1. days when a leap second has been inserted, e.g. 1999 JAN  1.
     * <BR/>2. Daylight-savings "spring forward" or "fall back" days.
     */
    protected static final long MILLISECS_PER_DAY = 24*MILLISECS_PER_HOUR;
	/**
	 * Represents the number of miliseconds in a day
	 */
	public static final int DAY_MILI = 1000*24*60*60;
	public static final double YEAR_MILI = DAY_MILI * 365.25;
	
	/**
	 * Diference between two dates, in days
	 * @param data1 First date
	 * @param data2 Second date
	 * @return Diference in days
	 */
//	public static long diffDateInDays( Date data1, Date data2){
//		if ( data1.compareTo(data2) > 0)
//			return ( data1.getTime() - data2.getTime() ) / DAY_MILI;
//		else
//			return ( data2.getTime() - data1.getTime() ) / DAY_MILI;
//	}

	/**
	 * Diference between two dates, in years
	 * @param dataNascimento First date
	 * @param dataReferencia Second date
	 * @return Diference in years
	 */
	public static int actuarialAgeByDate(Date dataNascimento, Date dataReferencia){
		int idade = getYear(dataReferencia) - getYear(dataNascimento);
		Date dataAtual = (new GregorianCalendar(getYear(dataNascimento) + idade, getMes(dataNascimento)-1, getDia(dataNascimento))).getTime();
		long diff = (dataReferencia.getTime() - dataAtual.getTime())/DAY_MILI;
		if (diff<0) // O anivers�rio ainda vai ser
			idade--;
		// Modifica��o Actuarial
		if (((diff>=-182)&&(diff<0))||((diff>=182)&&(diff<365))) {
			idade++;
		}
		return idade;
	}
	
	/**
	 * Years between two dates
	 * @param dataReferencia
	 * @param dataFinal
	 * @return
	 */
	public static int ageByDate(Date dataNascimento, Date dataReferencia){
		int idade = getYear(dataReferencia) - getYear(dataNascimento);
		Date dataAtual = (new GregorianCalendar(getYear(dataNascimento) + idade, getMes(dataNascimento)-1, getDia(dataNascimento))).getTime();
		long diff = (dataReferencia.getTime() - dataAtual.getTime());
		if (diff<0) // O anivers�rio ainda vai ser
			idade--;
		return idade;
	}
	
	/**
	 * Years and Month between two dates
	 * @param dataNascimento
	 * @param dataReferencia
	 * @return
	 */
	public static double agePartByDate(Date dataNascimento, Date dataReferencia){
		DiferencaData agePart = getDateDifference(dataNascimento, dataReferencia);
		int ageYear = agePart.getAnos();
		int idade = ageYear;
		return idade;
	}
	
	private static final SimpleDateFormat sdfAno = new SimpleDateFormat("yyyy");
	private static final SimpleDateFormat sdfMes = new SimpleDateFormat("MM");
	private static final SimpleDateFormat sdfDia = new SimpleDateFormat("dd");
	
	private static int getYear(Date data){
		return Integer.parseInt(sdfAno.format(data));
	}
	
	private static int getMes(Date data){
		return Integer.parseInt(sdfMes.format(data));
	}
	
	private static int getDia(Date data){
		return Integer.parseInt(sdfDia.format(data));
	}
	
	public final static int MENSAL=61;
	public final static int ANUAL=63;
	public final static int DIARIO=62;
	
	/**
	 * Adds a determinied number of periods to a date
	 * @param data Reference date to work with
	 * @param multipleType Multiple type to be used
	 * @param qtde Quantity of periods to add
	 * @return Final date
	 */
	public static Date dateInFuture(Date data, int multipleType, int qtde) {
		Calendar retorno = GregorianCalendar.getInstance();
		retorno.setTime(data);
		switch (multipleType){
		case MENSAL: {
			retorno.add(Calendar.MONTH, qtde);
			break;
		}
			
		case DIARIO: {
			retorno.add(Calendar.DAY_OF_MONTH, qtde);
			break;
		}

		case ANUAL: {
			retorno.add(Calendar.YEAR, qtde);
			break;
		}
		
		default:
			throw new InvalidParameterException("multipleType not found: " + multipleType);
		}
		
		return retorno.getTime();
	}
	
	public static Date truncDate(Date inputDate){
		Calendar workCalendar = GregorianCalendar.getInstance();
		workCalendar.setTime( inputDate );
		workCalendar.set(Calendar.HOUR_OF_DAY, 0);
		workCalendar.set(Calendar.MINUTE, 0);
		workCalendar.set(Calendar.SECOND, 0);
		workCalendar.set(Calendar.MILLISECOND, 0);
		
		return workCalendar.getTime();
		
	}
	
	/**
	 * Calculate the number of days between two dates
	 */
	public static int diffDateInDays(Date iniDate, Date finalDate){

		iniDate = truncDate( iniDate );
		finalDate = truncDate( finalDate );
		
	    int response = 0;
	    if (finalDate.after(iniDate)){
	        long diff = (finalDate.getTime() - iniDate.getTime());
	        response = (int)(diff / MILLISECS_PER_DAY);
	    }else{
	        long diff = (iniDate.getTime() - finalDate.getTime());
	        response = (int)(diff / MILLISECS_PER_DAY);
	    }
	    return response;
	}

	/**
	 * Returns absolutes values for diferences between two dates
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static DiferencaData getDateDifference(Date date1, Date date2) {
		DateTime startDate;
		DateTime endDate;

		if (date1.compareTo(date2) <= 0) {
			startDate = toDateTime(date1);
			endDate = toDateTime(date2);
		}
		else {
			startDate = toDateTime(date2);
			endDate = toDateTime(date1);
		}

		return fillDateDifference(startDate, endDate);
	}
	
	static final int[] meses31 = new int[]{1,3,5,7,8,10,12};
	
	
	private static int maxDiasMes(int mes, int ano) {
		if (mes==2){
			if ((ano % 4)==0)
				return 29;
			return 28;
		}
		for(int aux = 0;aux<meses31.length; aux++)
			if (meses31[aux] == mes)
				return 31;
		return 30;
	}

	public static final class DiferencaData{
		int dias, meses, anos;
		/**
		 * Carrier for data diferences routines
		 * @param dias number of days
		 * @param meses Number of months
		 * @param anos Number of years
		 */
		public DiferencaData(int dias, int meses, int anos) {
			this.dias = dias;
			this.meses = meses;
			this.anos = anos;
		}
		/**
		 * @return the dias
		 */
		public int getDias() {
			return dias;
		}
		/**
		 * @return the meses
		 */
		public int getMeses() {
			return meses;
		}
		/**
		 * @return the anos
		 */
		public int getAnos() {
			return anos;
		}
	}
	
	public static Date addDays(Date data, int numDias){
		return new Date(data.getTime() + (DateUtilities.MILLISECS_PER_DAY * numDias)); 
	}
	
	public static Date addMonths(Date data, int months){
		Calendar instance = GregorianCalendar.getInstance();
		instance.setTime(data);
		instance.add(Calendar.MONTH, months);
		return instance.getTime();
	}
	
	public static Date addYears(Date data, int numYears){
		Calendar instance = GregorianCalendar.getInstance();
		instance.setTime(data);
		instance.add(Calendar.YEAR, numYears);
		return instance.getTime();
	}

	private static double roundDecimalPlaces(double oldValue, int decimalPalces) {
		// Pega como default a quantidade de casas decimais padr�o dos c�lculos

		BigDecimal valBig = new BigDecimal( oldValue );
		BigDecimal newValue = valBig.setScale( decimalPalces, BigDecimal.ROUND_HALF_UP );

		return newValue.doubleValue();
	}
	
	public static DateTime toDateTime(Date date) {
		Calendar startCal = Calendar.getInstance();
		startCal.setTime(date);
		return new DateTime(startCal.get(Calendar.YEAR), (startCal.get(Calendar.MONTH) + 1), startCal.get(Calendar.DAY_OF_MONTH), 0, 0, 0, 0, DateTimeZone.UTC);
	}

	public static DiferencaData fillDateDifference(DateTime startDate, DateTime endDate) {
		int days = Days.daysBetween(startDate, endDate).getDays();
		int months = Months.monthsBetween(startDate, endDate).getMonths();
		int years = Years.yearsBetween(startDate, endDate).getYears();
		return new DiferencaData(days, months, years);
	}
}
