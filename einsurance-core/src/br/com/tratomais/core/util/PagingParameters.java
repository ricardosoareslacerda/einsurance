package br.com.tratomais.core.util;

import java.io.Serializable;

import br.com.tratomais.core.dao.PersistentEntity;

public class PagingParameters implements Serializable, PersistentEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    private int currentPage;
    private int page;
    private int totalPage;
    private String componentId;
    
    
	public int getCurrentPage() {
		return currentPage;
	}
	
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	
	public int getPage() {
		return page;
	}
	
	public void setPage(int page) {
		this.page = page;
	}
	
	public int getTotalPage() {
		return totalPage;
	}
	
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public String getComponentId() {
		return componentId;
	}

	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}
    
}
