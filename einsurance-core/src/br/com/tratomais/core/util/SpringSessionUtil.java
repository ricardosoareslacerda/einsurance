package br.com.tratomais.core.util;

import java.util.Locale;

import javax.servlet.ServletContext;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import flex.messaging.FlexContext;


//nimer: rename from HibernateUtil
public class SpringSessionUtil 
{
	//nimer: make static
	public static Session getCurrentSession() 
	{		
		ServletContext ctx = FlexContext.getServletContext();
		return getCurrentSession(ctx);
    }
	
	public static Session getCurrentSession(ServletContext ctx){
		WebApplicationContext springContext = WebApplicationContextUtils.getRequiredWebApplicationContext(ctx);
		SessionFactory sessionFactory = (SessionFactory)springContext.getBean("sessionFactory");
		Session session = SessionFactoryUtils.getSession(sessionFactory, false);
		return session;
	}
	
	public static Object getBean(ServletContext ctx, String beanName){
		WebApplicationContext springContext = WebApplicationContextUtils.getRequiredWebApplicationContext(ctx);
		return springContext.getBean(beanName);		
	}
	
	public static Object getBean(String beanName) 
	{		
		ServletContext ctx = FlexContext.getServletContext();
		return getBean(ctx, beanName);
    }
	
	public static String getMessage(String message, Object [] args){
		ServletContext ctx = FlexContext.getServletContext();
		WebApplicationContext springContext = WebApplicationContextUtils.getRequiredWebApplicationContext(ctx);
		return springContext.getMessage(message, args, Locale.getDefault());		
	}
}
