package br.com.tratomais.core.util;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class HibernateUtil {
	private static Logger logger = Logger.getLogger(HibernateUtil.class);
	private static SessionFactory sessionFactory;
	
	static{
		sessionFactory = new AnnotationConfiguration().configure("config/hibernate-config-collection-cfg.xml").buildSessionFactory();
	}
	
	public static Session getSession(){
		logger.info("Openning new session");
		return sessionFactory.openSession();
	}

}
