package br.com.tratomais.core.util;

import java.io.ByteArrayOutputStream;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;

public class UtilFactory {
	public static final int JRRTF_EXPORTER = 1;
	public static final int JRODT_EXPORTER = 2;
	public static final int BYTE_ARRAY_OUTPUT_STREAM = 3;
	public static final int ARRAY_LIST = 4;
	public static final int HASH_MAP = 5;
	
	@SuppressWarnings("unchecked")
	public static Object create(int en) {
		Object obj = null;
		switch(en) {
			case ARRAY_LIST: obj = new ArrayList(); break;
			case HASH_MAP: obj = new HashMap(); break;
		}
		return obj;
	}
	public static JRResultSetDataSource createJRResultSetDataSource(ResultSet rs) {
		return new JRResultSetDataSource(rs);
	}
	public static Object createJasper(int en) {
		Object obj = null;
		switch(en) { 
			case JRRTF_EXPORTER: obj = new JRRtfExporter(); break; 
			case JRODT_EXPORTER: obj = new JROdtExporter(); break;
			case BYTE_ARRAY_OUTPUT_STREAM: obj = new ByteArrayOutputStream(); break;
		}
		return obj;
	}
}
