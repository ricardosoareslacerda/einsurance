package br.com.tratomais.core.util;

public class Constantes {

	public static final int TENTATIVAS_LOGIN = 3;

	public static final int ID_INSURER_ZURICH = 1;

	public static final int ID_DOMAIN_TRATO = 1;
	
	public static final int  PERSON_TYPE		= 11; 		//  PERSON_TYPE
	
	public static final int  LEGALLY_PERSON		= 64; 		//  PERSON_TYPE
	
	public static final int  NATURAL_PERSON		= 63; 		//  PERSON_TYPE
	
	public static final int  DOCUMENT_TYPE		= 14; 	//  DOCUMENT_TYPE
    
	public static final int  GENDER_TYPE		= 24; 		//  GENDER_TYPE
    
	public static final int  MARITAL_STATUS		= 25; 	//  MARITAL_STATUS
    
	public static final int  ADDRESS_TYPE		= 13; 	//  ADDRESS_TYPE
    
	public static final int  BENEFICIARY_TYPE	= 28; //  BENEFICIARY_TYPE
    
	public static final int  KINSHIP_TYPE		= 27; 	//  KINSHIP_TYPE
    
	public static final int  PROFESSION_TYPE	= 12; 	//  PROFESSION_TYPE
    
	public static final int  PHONE_TYPE			= 15; 		//  PHONE_TYPE
	
	public static final int  CONTACT_TYPE		= 16; 	//  CONTACT_TYPE
	
	public static final int  POLICY_TYPE		= 2;		//	POLICY_TYPE
	
	public static final int  PAYMENT_TYPE		= 31;		//	PAYMENT_TYPE	
	
	public static final Integer  PLAN_TYPE_COVERAGE		= 177;		// PLAN_TYPE_COVERAGE

	public static final Integer  PLAN_TYPE_RISK		= 180;		// PLAN_TYPE_COVERAGE
	
	public static final Integer ID_DATA_GROUP_HOUSING = 29;
	
	public static final Integer ID_PLAN_RISK_ALL = 11;
	
	public static final Integer ID_PLAN_RISK_CONTENT = 10;
	
	public static final Integer ID_PLAN_RISK_STRUCTURE = 9;
	
	public static final Integer COVERAGE_STRUCTURE = 14;

	public static final Integer COVERAGE_CONTENT = 15;
	
	public static final Integer ID_PLAN_RISK_DEFAULT = 1;
}
