package br.com.tratomais.core.util.findcep.yasuda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;

import br.com.tratomais.buscacep.BuscaCep;
import br.com.tratomais.buscacep.DadosCep;

/**
 * Implementa��o do buscacep da yasuda 
 * @author luiz.alberoni
 */
public class BuscaCepImpl implements BuscaCep {
	/** Pattern para encontrar a localiza��o do endere�o do WS */	
	private static final Pattern patternLocalWS = Pattern.compile("<[\\w\\d]*:address\\s*location=\"([^\"]*)\"\\s*/>");
	/** Pattern deixar somente n�meros */	
	private static final Pattern patternDeixaSomenteNumeros = Pattern.compile("\\D");
	/** Pattern para encontrar nome do bairro */	
	private static final Pattern patternBairro = Pattern.compile("<[a-zA-Z\\d]*bairro>(.*)</[a-zA-Z\\d]*bairro>");
	/** Pattern para encontrar o cep */	
	private static final Pattern patternCep = Pattern.compile("<[a-zA-Z\\d]*cep>(.*)</[a-zA-Z\\d]*cep>");
	/** Pattern para encontrar a cidade (nome) */	
	private static final Pattern patternCidade = Pattern.compile("<[a-zA-Z\\d]*:Cidade>(.*)</[a-zA-Z\\d]*:Cidade>");
	private static final Pattern patternLocalidade = Pattern.compile("<[a-zA-Z\\d]*localidade>(.*)</[a-zA-Z\\d]*localidade>");
	/** Pattern para encontrar o nome do logradouro */	
	private static final Pattern patternLogradouro = Pattern.compile("<[a-zA-Z\\d]*logradouro>(.*)</[a-zA-Z\\d]*logradouro>");
	/** Pattern para encontrar a sigla do estado */	
	private static final Pattern patternUF = Pattern.compile("<[a-zA-Z\\d]*uf>(.*)</[a-zA-Z\\d]*uf>");
	private static final Pattern patternSiglaUF = Pattern.compile("<[a-zA-Z\\d]*:SiglaUF>(.*)</[a-zA-Z\\d]*:SiglaUF>");
	/** Pattern para encontrar a tipo de logradouro */	
	private static final Pattern patternTipoLogradouro = Pattern.compile("<[a-zA-Z\\d]*:TipoLogradouro>(.*)</[a-zA-Z\\d]*:TipoLogradouro>");
	
	/** Localiza��o do endere�o do WS */	
	private String localServico = null;
	/** Localiza��o do wsdl */
	private String wsdlLocation;
	
	/** usu�rio */
	private String usuario;
	/**
	 * Ajusta do usu�rio para acesso ao ws
	 * @param usuario usu�rio
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	/** senha */
	private String senha;
	/**
	 * Ajussta a senha para acesso ao ws
	 * @param senha senha de acesso ao ws
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}

	/**
	 * Construtor
	 * @param WSDLLocation URL do wsdl
	 * @param usuario usu�rio 
	 * @param senha senha de acesso
	 */
	public BuscaCepImpl(String WSDLLocation) {
		this.wsdlLocation = WSDLLocation;
	}

	/**
	 * Construtor
	 * @param WSDLLocation URL do wsdl
	 * @param usuario usu�rio 
	 * @param senha senha de acesso
	 */
	public BuscaCepImpl(String WSDLLocation, String usuario, String senha) {
		this.wsdlLocation = WSDLLocation;
		this.usuario = usuario;
		this.senha = senha;
	}

	/** {@inheritDoc} */
	@Override
	public DadosCep pesquisa(String cep) {
		if (localServico == null)
			localizaServico();
		
		cep = patternDeixaSomenteNumeros.matcher(cep).replaceAll("");
		
		//return extract(callServer(criaString(cep)));
		return extractGET(callServerGET(cep));
	}
	

	/**
	 * Localiza do servi�o
	 */
	private void localizaServico() {
		GetMethod get = new GetMethod(this.wsdlLocation);
		try {
			new HttpClient().executeMethod(get);
			String responseStr = get.getResponseBodyAsString();
			localServico = getValue(patternLocalWS.matcher(responseStr));
		} catch (HttpException e) {
			localServico = null;
			throw new RuntimeException(e);
		} catch (IOException e) {
			localServico = null;
			throw new RuntimeException(e);
		}
	}

	/**
	 * Preenche ZipCodeAddress se baseando nos dados recuperados
	 * @param retorno Dados recuperados
	 * @return Inst�ncia de ZipCodeAddress e os dados recuperados
	 */
	@SuppressWarnings("unused")
	private DadosCep extract(String retorno) {
		DadosCep dadosCep = new DadosCep();
		dadosCep.setBairro(getValue(patternBairro.matcher(retorno)));
		String logradouro = getValue(patternLogradouro.matcher(retorno));
		String tipoLogradouro = getValue(patternTipoLogradouro.matcher(retorno));
		dadosCep.setLogradouro(((tipoLogradouro != null ? tipoLogradouro.trim() + " " : "") + (logradouro != null ? logradouro.trim() : "")).trim());
		dadosCep.setCep(getValue(patternCep.matcher(retorno)));
		dadosCep.setLocalidade(getValue(patternCidade.matcher(retorno)));
		dadosCep.setUf(getValue(patternSiglaUF.matcher(retorno)));
		return dadosCep;
	}

	/**
	 * Preenche ZipCodeAddress se baseando nos dados recuperados
	 * @param retorno Dados recuperados
	 * @return Inst�ncia de ZipCodeAddress e os dados recuperados
	 */
	private DadosCep extractGET(String retorno) {
		DadosCep dadosCep = new DadosCep();
		dadosCep.setBairro(getValue(patternBairro.matcher(retorno)));
		String logradouro = getValue(patternLogradouro.matcher(retorno));
		String tipoLogradouro = null;
		dadosCep.setLogradouro(((tipoLogradouro != null ? tipoLogradouro.trim() + " " : "") + (logradouro != null ? logradouro.trim() : "")).trim());
		dadosCep.setCep(getValue(patternCep.matcher(retorno)));
		dadosCep.setLocalidade(getValue(patternLocalidade.matcher(retorno)));
		dadosCep.setUf(getValue(patternUF.matcher(retorno)));
		return dadosCep;
	}

	/**
	 * Retorna o valor encontrado pelo matcher
	 * @param matcher Matcher usado
	 * @return Valor encontrado
	 */
	private String getValue(Matcher matcher) {
		if (matcher.find())
			return matcher.group(1);
		return null;
	}

	/**
	 * CHama o servidor
	 * @param payload payload de entrada
	 * @return payload de saida
	 */
	@SuppressWarnings("unused")
	private String callServer(String payload) {
		PostMethod post = new PostMethod(localServico);
		HttpClient httpClient = new HttpClient();
		try {
			post.addRequestHeader("SOAPAction", "\"http://tempuri.org/EnderecoServiceContract/GetEndereco\"");
			post.setRequestEntity(new StringRequestEntity(payload, "text/xml", "utf-8"));
			int responseCode = httpClient.executeMethod(post);
			if ((responseCode<200)||(responseCode>299))
				throw new RuntimeException("Error:"+responseCode);
			return post.getResponseBodyAsString();
		} catch (HttpException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * CHama o servidor
	 * @param payload payload de entrada
	 * @return payload de saida
	 */
	private String callServerGET(String cep) {
		HttpClient client = new HttpClient();
		HttpMethod method = new GetMethod(wsdlLocation + "/" + cep + "/xml");
		
        try {
            client.executeMethod(method);

            if (method.getStatusCode() == HttpStatus.SC_OK) {
                InputStream is = method.getResponseBodyAsStream();

                if (is != null) {
                    Writer writer = new StringWriter();
                    char[] buffer = new char[1024];
                    try {
                        Reader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
                        int length;
                        while ((length = reader.read(buffer)) != -1) {
                            writer.write(buffer, 0, length);
                        }
                    } finally {
                        is.close();
                    }
                    return writer.toString();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            method.releaseConnection();
        }
        
		return null;
	}
	/**
	 * Cria string de pesquisa
	 * @param cep cep pesquisado
	 * @return Payload de pesquisa
	 */
	@SuppressWarnings("unused")
	private String criaString(String cep) {
		return 
			"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">\n"+
			"   <soapenv:Header/>\n" +
			"   <soapenv:Body>\n" +
			"      <tem:GetEndereco>\n" +
			"         <tem:cep>"+cep+"</tem:cep>\n" +
			"         <tem:usuario>"+usuario+"</tem:usuario>\n" +
			"         <tem:senha>"+senha+"</tem:senha>\n" +
			"      </tem:GetEndereco>\n" +
			"   </soapenv:Body>\n" +
			"</soapenv:Envelope>";
	}
}
