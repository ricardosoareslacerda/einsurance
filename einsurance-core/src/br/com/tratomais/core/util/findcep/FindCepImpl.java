package br.com.tratomais.core.util.findcep;

import java.util.List;

import br.com.tratomais.buscacep.BuscaCep;
import br.com.tratomais.core.dao.IDaoCity;
import br.com.tratomais.core.dao.IDaoCountry;
import br.com.tratomais.core.dao.IDaoState;
import br.com.tratomais.core.model.City;
import br.com.tratomais.core.model.Country;
import br.com.tratomais.core.model.State;

public class FindCepImpl implements IFindCEP {
	private BuscaCep buscaCep;

	private IDaoCity daoCity;
	public void setDaoCity(IDaoCity daoCity) {
		this.daoCity = daoCity;
	}

	private IDaoState daoState;
	public void setDaoState(IDaoState daoState) {
		this.daoState = daoState;
	}

	private IDaoCountry daoCountry;
	public void setDaoCountry(IDaoCountry daoCountry) {
		this.daoCountry = daoCountry;
	}

	public FindCepImpl(br.com.tratomais.buscacep.BuscaCep buscaCep) {
		this.buscaCep = buscaCep;
	}

	/* (non-Javadoc)
	 * @see br.com.tratomais.einsurance.findcep.findCEP#pesquisa(java.lang.String)
	 */
	@Override
	public ZipCodeAddress search(String cep){
		ZipCodeAddress zipCodeAddress = null;
		try {
			zipCodeAddress = new ZipCodeAddress(buscaCep.pesquisa(cep));
		}
		catch(Throwable t){
			t.printStackTrace();
			zipCodeAddress = new ZipCodeAddress();
		}
		this.fillAddress(zipCodeAddress);
		return zipCodeAddress;
	}

	private void fillAddress(ZipCodeAddress zipCodeAddress) {
		State state = this.findState(zipCodeAddress.getStateUF());
		if (state != null) {
			Country country = this.findCountry(state.getCountryId());
			if (country != null) {
				zipCodeAddress.setCountryId(country.getCountryId());
				zipCodeAddress.setCountryName(country.getName());
			}
			zipCodeAddress.setStateId(state.getStateId());
			zipCodeAddress.setStateName(state.getName());
			City city = this.findCity(state, zipCodeAddress.getCityName());
			if (city != null) {
				zipCodeAddress.setCityId(city.getCityId());
				zipCodeAddress.setCityName(city.getName());
			}
		}
	}

	private Country findCountry(Integer countryId) {
		return daoCountry.findById(countryId);
	}

	/**
	 * Busca a cidade atrav�s do estado e nome da cidade
	 * @param estado Estado a que pertence a cidade
	 * @param cityName Nome da cidade
	 * @return Cidade
	 */
	private City findCity(State state, String cityName) {
		if((cityName == null) || (state == null))
			return null;
		List<City> list = daoCity.findByNameNoAccents(state, cityName);
		if(list.size() == 0)
			return null;
		return this.getCity(cityName, list);
	}

	private City getCity(String cityName, List<City> list) {
		if(list.size() == 1) // se encontrar 1, retorna ela 
			return list.get(0);
		for(City city : list){ // Caso contr�rio, tenta retornar a cujo nome seja id�ntico
			if(cityName.equals(city.getName()))
				return city;
		}
		return list.get(0); // Caso contr�rio, retorna a primeira da lista
	}

	private State findState(String stateUF) {
		if(stateUF == null)
			return null;
		List<State> estados = daoState.findByNameOrUf(stateUF);
		if(estados.size() == 0)
			return null;
		return this.getState(stateUF, estados);
	}

	private State getState(String stateUF, List<State> estados) {
		if(estados.size() == 1)
			return estados.get(0);
		for (State state : estados) {
			if(stateUF.equals(state.getUfCode()) || stateUF.equals(state.getName()))
				return state;
		}
		return estados.get(0);
	}
}
