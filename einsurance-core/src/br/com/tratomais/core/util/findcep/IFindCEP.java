package br.com.tratomais.core.util.findcep;

public interface IFindCEP {

	public abstract ZipCodeAddress search(String cep);

}