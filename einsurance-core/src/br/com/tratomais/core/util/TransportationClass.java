package br.com.tratomais.core.util;

import java.util.List;
/**
 * Generic Transportation class
 * used to pass information to the front end
 * 
 * @author bruno.souza
 *
 * @param <T>
 */
public class TransportationClass<T> {
	private List<T> objetcts;
	private int qtyRegs;
	private int qtyPages;
	private int actualPage;
	public List<T> getObjetcts() {
		return objetcts;
	}
	public void setObjetcts(List<T> objetcts) {
		this.objetcts = objetcts;
	}
	public TransportationClass() {
	}
	/**
	 * 
	 * @param objetcts
	 * @param qtyRegs
	 * @param qtyPages
	 */
	public TransportationClass(List<T> objetcts, int qtyRegs, int qtyPages) {
		this(objetcts,qtyRegs,qtyPages,0);
	}
	
	public TransportationClass(List<T> objetcts, int qtyRegs, int qtyPages,
			int actualPage) {
		super();
		this.objetcts = objetcts;
		this.qtyRegs = qtyRegs;
		this.qtyPages = qtyPages;
		this.actualPage = actualPage;
	}
	public int getQtyRegs() {
		return qtyRegs;
	}
	public void setQtyRegs(int qtyRegs) {
		this.qtyRegs = qtyRegs;
	}
	public int getQtyPages() {
		return qtyPages;
	}
	public void setQtyPages(int qtyPages) {
		this.qtyPages = qtyPages;
	}
	public int getActualPage() {
		return actualPage;
	}
	public void setActualPage(int actualPage) {
		this.actualPage = actualPage;
	}

}
