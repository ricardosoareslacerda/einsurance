package br.com.tratomais.core.util;

import java.sql.Timestamp;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.infrastructure.AuthenticationHelper;
import br.com.tratomais.core.service.impl.ServiceEinsuranceImpl;
import br.com.tratomais.core.service.impl.ServiceLoginImpl;
import br.com.tratomais.core.service.impl.ServiceProfileImpl;

/**
 * @author leo
 *         <p>
 *         Interceptor de servi�os
 * @since 2010
 * @version 1.0
 **/

@Aspect
@Component
public class ServiceEinsuranceInterceptor {
	private static final Logger logger = Logger.getLogger(ServiceEinsuranceInterceptor.class);

	/**
	 * <p>
	 * Metodo que define que o aspecto interceptar� todos os fontes do pacote br.com.tratomais.core.service.impl.
	 * 
	 * @return {@link Void}
	 */
	@Pointcut("execution(* br.com.tratomais.core.service.impl..*.*(..))")
	public void allMethod() {
	}

	/**
	 * M�todo executado antes dos servicos
	 * 
	 * @param joinPoint
	 *            Ponto de execu��o (spring)
	 * @return {@link Void}
	 * @throws ServiceException
	 *             para sess�o expirada Metodo executado toda vez que � feito a chamada para um service
	 *             <p>
	 *             Exce��o do serviceEinsurance pois n�o tem processamento e s� redirecionamento
	 *             <p>
	 *             Exce��o do serviceProfile e serviceLogin usados para efetuar o login
	 */
	@Before("allMethod()")
	public void loggerBeforeAllServiceMethod(JoinPoint joinPoint) {
		ArrayList<String> allowedClass = new ArrayList<String>();
		allowedClass.add(ServiceEinsuranceImpl.class.getCanonicalName());
		allowedClass.add(ServiceProfileImpl.class.getCanonicalName());
		allowedClass.add(ServiceLoginImpl.class.getCanonicalName());
		if (Logger.getLogger(ServiceEinsuranceInterceptor.class).isDebugEnabled()) {
			System.out.println(joinPoint.getTarget().getClass().getCanonicalName());
		}
		if ((!joinPoint.getTarget().getClass().getCanonicalName().toUpperCase().contains("test".toUpperCase()))&&(! "br.com.tratomais.core.service.ICalc".equals(joinPoint.getSignature().getDeclaringType().getCanonicalName()))) {
			if (allowedClass.contains(joinPoint.getTarget().getClass().getCanonicalName()) == false) {
				boolean sessionExpired = AuthenticationHelper.isSessionExpired(new Timestamp(System.currentTimeMillis()));
				if (sessionExpired) {
					throw new Error("sessionExpired: " + joinPoint.getSourceLocation());
//					throw new Error("sessionExpired: " + joinPoint.getThis().getClass().getCanonicalName().toUpperCase());
				}
			}
		}
	}

	/**
	 * M�todo executado depois dos servicos
	 * 
	 * @param joinPoint
	 *            Ponto de execu��o (spring)
	 * @return {@link Void}
	 */
	@After("allMethod()")
	public void loggerAfterAllServiceMethod(JoinPoint joinPoint) {
	}

}
