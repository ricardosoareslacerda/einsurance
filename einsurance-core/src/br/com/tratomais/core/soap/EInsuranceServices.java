package br.com.tratomais.core.soap;

import java.util.Date;

import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.esb.collections.model.response.CollectionResponseItem;
import br.com.tratomais.esb.interfaces.model.request.ClaimRequest;

public interface EInsuranceServices {
	public abstract void processInstallment(String login, String password, CollectionResponseItem[] collectionResponse);

	public abstract void receiveAdvice(String login, String password, CollectionResponseItem[] collectionResponse) throws ServiceException;

	public abstract void policyCancellationNoPayment(String login, String password, int contractId, Date effectiveDate, Date cancelDate) throws ServiceException;

	public abstract void policyReactivation(String login, String password, int contractId) throws ServiceException;

	public abstract void policyClaim(String login, String password, ClaimRequest[] claimRequests) throws ServiceException;

	public abstract void policyCancellation(String login, String password, int contractId, int issuanceType, Date cancelDate, int cancelReason) throws ServiceException;

	public abstract void renewal(String login, String password, int contractId, int endorsementId) throws ServiceException;

	public br.com.tratomais.core.soap.to.Endorsement policyRenewal(String login, String password, br.com.tratomais.core.soap.to.Endorsement endorsement, br.com.tratomais.core.soap.to.PaymentOption paymentOption) throws ServiceException;

	public br.com.tratomais.core.soap.to.Endorsement policyIssuance(String login, String password, br.com.tratomais.core.soap.to.Endorsement endorsement, br.com.tratomais.core.soap.to.PaymentOption paymentOption) throws ServiceException;
}