package br.com.tratomais.core.soap.to;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ItemAuto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final int AUTO_TABLE_TYPE_MAIN = 340;

	public static final int PERSONAL_RISK_TYPE_MOTORIST = 496;

	private Integer autoTableType;
	private String brandName;
	private String modelName;
	private Integer autoModelId;
	private Integer autoBrandId;
	private Integer autoFuelType;
	private Integer autoTransmissionType;
	private Integer autoClassType;
	private Integer autoTariffType;
	private Integer autoCategoryType;
	private Integer autoGroupType;
	private Integer autoUseType;
	private Integer countryID;
	private Integer stateID;
	private Integer cityID;
	private Integer localityCode;
	private Integer doorNumber;
	private Integer passengerNumber;
	private Integer yearFabrication;
	private Integer yearModel;
	private Boolean zeroKm;
	private Double valueRefer;
	private Integer modalityType;
	private Double variationFactor;
	private Integer tonneQuantity;
	private Integer kmQuantity;
	private Integer colorType;
	private String licensePlate;
	private String chassisSerial;
	private String engineSerial;
	private String licenseCode;
	private Integer invoiceNumber;
	private Date invoiceDate;
	private Integer inspectionNumber;
	private Date inspectionDate;

	private Motorist[] motorists = new Motorist[0];

	public Integer getAutoTableType() {
		return autoTableType;
	}

	public void setAutoTableType(Integer autoTableType) {
		this.autoTableType = autoTableType;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public Integer getAutoFuelType() {
		return autoFuelType;
	}

	public void setAutoFuelType(Integer autoFuelType) {
		this.autoFuelType = autoFuelType;
	}

	public Integer getAutoTransmissionType() {
		return autoTransmissionType;
	}

	public void setAutoTransmissionType(Integer autoTransmissionType) {
		this.autoTransmissionType = autoTransmissionType;
	}

	public Integer getAutoClassType() {
		return autoClassType;
	}

	public void setAutoClassType(Integer autoClassType) {
		this.autoClassType = autoClassType;
	}

	public Integer getAutoTariffType() {
		return autoTariffType;
	}

	public void setAutoTariffType(Integer autoTariffType) {
		this.autoTariffType = autoTariffType;
	}

	public Integer getAutoCategoryType() {
		return autoCategoryType;
	}

	public void setAutoCategoryType(Integer autoCategoryType) {
		this.autoCategoryType = autoCategoryType;
	}

	public Integer getAutoGroupType() {
		return autoGroupType;
	}

	public void setAutoGroupType(Integer autoGroupType) {
		this.autoGroupType = autoGroupType;
	}

	public Integer getAutoUseType() {
		return autoUseType;
	}

	public void setAutoUseType(Integer autoUseType) {
		this.autoUseType = autoUseType;
	}

	public Integer getCountryID() {
		return countryID;
	}

	public void setCountryID(Integer countryID) {
		this.countryID = countryID;
	}

	public Integer getStateID() {
		return stateID;
	}

	public void setStateID(Integer stateID) {
		this.stateID = stateID;
	}

	public Integer getCityID() {
		return cityID;
	}

	public void setCityID(Integer cityID) {
		this.cityID = cityID;
	}

	public Integer getLocalityCode() {
		return localityCode;
	}

	public void setLocalityCode(Integer localityCode) {
		this.localityCode = localityCode;
	}

	public Integer getDoorNumber() {
		return doorNumber;
	}

	public void setDoorNumber(Integer doorNumber) {
		this.doorNumber = doorNumber;
	}

	public Integer getPassengerNumber() {
		return passengerNumber;
	}

	public void setPassengerNumber(Integer passengerNumber) {
		this.passengerNumber = passengerNumber;
	}

	public Integer getYearFabrication() {
		return yearFabrication;
	}

	public void setYearFabrication(Integer yearFabrication) {
		this.yearFabrication = yearFabrication;
	}

	public Integer getYearModel() {
		return yearModel;
	}

	public void setYearModel(Integer yearModel) {
		this.yearModel = yearModel;
	}

	public Boolean getZeroKm() {
		return zeroKm;
	}

	public void setZeroKm(Boolean zeroKm) {
		this.zeroKm = zeroKm;
	}

	public Double getValueRefer() {
		return valueRefer;
	}

	public void setValueRefer(Double valueRefer) {
		this.valueRefer = valueRefer;
	}

	public Integer getModalityType() {
		return modalityType;
	}

	public void setModalityType(Integer modalityType) {
		this.modalityType = modalityType;
	}

	public Double getVariationFactor() {
		return variationFactor;
	}

	public void setVariationFactor(Double variationFactor) {
		this.variationFactor = variationFactor;
	}

	public Integer getTonneQuantity() {
		return tonneQuantity;
	}

	public void setTonneQuantity(Integer tonneQuantity) {
		this.tonneQuantity = tonneQuantity;
	}

	public Integer getKmQuantity() {
		return kmQuantity;
	}

	public void setKmQuantity(Integer kmQuantity) {
		this.kmQuantity = kmQuantity;
	}

	public Integer getColorType() {
		return colorType;
	}

	public void setColorType(Integer colorType) {
		this.colorType = colorType;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public String getChassisSerial() {
		return chassisSerial;
	}

	public void setChassisSerial(String chassisSerial) {
		this.chassisSerial = chassisSerial;
	}

	public String getEngineSerial() {
		return engineSerial;
	}

	public void setEngineSerial(String engineSerial) {
		this.engineSerial = engineSerial;
	}

	public String getLicenseCode() {
		return licenseCode;
	}

	public void setLicenseCode(String licenseCode) {
		this.licenseCode = licenseCode;
	}

	public Integer getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(Integer invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Integer getInspectionNumber() {
		return inspectionNumber;
	}

	public void setInspectionNumber(Integer inspectionNumber) {
		this.inspectionNumber = inspectionNumber;
	}

	public Date getInspectionDate() {
		return inspectionDate;
	}

	public void setInspectionDate(Date inspectionDate) {
		this.inspectionDate = inspectionDate;
	}

	public Motorist[] getMotorists() {
		return motorists;
	}

	public void setMotorists(Motorist[] motorists) {
		this.motorists = motorists;
	}

	public Integer getAutoModelId() {
		return autoModelId;
	}

	public void setAutoModelId(Integer autoModelId) {
		this.autoModelId = autoModelId;
	}

	public Integer getAutoBrandId() {
		return autoBrandId;
	}

	public void setAutoBrandId(Integer autoBrandId) {
		this.autoBrandId = autoBrandId;
	}

	public void addMotorist(Motorist motorist){
		List<Motorist> asList = Arrays.asList(this.motorists);
		asList.add(motorist);
		this.motorists = (Motorist[]) asList.toArray();
	}

	public final Motorist addMotorist(int motoristId) {
		Motorist newMotorist = new Motorist();
		newMotorist.setMotoristId( motoristId );
		this.addMotorist( newMotorist );
		return newMotorist;
	}
}
