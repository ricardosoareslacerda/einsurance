package br.com.tratomais.core.soap.to;

import java.io.Serializable;

public class ErrorList implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Erro[] lista = null;

	/**
	 * @param lista the lista to set
	 */
	public void setLista(Erro[] lista) {
		this.lista = lista;
	}

	/**
	 * @return the lista
	 */
	public Erro[] getLista() {
		return lista;
	}
}
