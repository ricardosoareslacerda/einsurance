package br.com.tratomais.core.soap.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

public class PolicyCertificateNumber implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigInteger policyNumber;
	private long certificateNumber;
	private BigDecimal valorAcum;
	
	public PolicyCertificateNumber() {
		// TODO Auto-generated constructor stub
	}
	
	public PolicyCertificateNumber(BigInteger policyNumber, long certificateNumber) {
		this(policyNumber, certificateNumber,new BigDecimal(0));
	}
	
	public PolicyCertificateNumber(BigInteger policyNumber, long certificateNumber, BigDecimal valorAcum) {
		super();
		this.policyNumber = policyNumber;
		this.certificateNumber = certificateNumber;
		this.valorAcum = valorAcum;
	}
	
	/**
	 * @return the policyNumber
	 */
	public BigInteger getPolicyNumber() {
		return policyNumber;
	}
	/**
	 * @param policyNumber the policyNumber to set
	 */
	public void setPolicyNumber(BigInteger policyNumber) {
		this.policyNumber = policyNumber;
	}
	/**
	 * @return the certificateNumber
	 */
	public long getCertificateNumber() {
		return certificateNumber;
	}
	/**
	 * @param certificateNumber the certificateNumber to set
	 */
	public void setCertificateNumber(long certificateNumber) {
		this.certificateNumber = certificateNumber;
	}
	/**
	 * @return the valorAcum
	 */
	public BigDecimal getValorAcum() {
		return valorAcum;
	}
	/**
	 * @param valorAcum the valorAcum to set
	 */
	public void setValorAcum(BigDecimal valorAcum) {
		this.valorAcum = valorAcum;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ (int) (certificateNumber ^ (certificateNumber >>> 32));
		result = prime * result
				+ ((policyNumber == null) ? 0 : policyNumber.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PolicyCertificateNumber other = (PolicyCertificateNumber) obj;
		if (certificateNumber != other.certificateNumber)
			return false;
		if (policyNumber == null) {
			if (other.policyNumber != null)
				return false;
		} else if (!policyNumber.equals(other.policyNumber))
			return false;
		return true;
	}
}
