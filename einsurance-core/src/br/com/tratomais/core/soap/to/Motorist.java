package br.com.tratomais.core.soap.to;

import java.io.Serializable;
import java.util.Date;

public class Motorist implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final int MOTORIST_TYPE_MAIN = 484;
	public static final int MOTORIST_TYPE_OTHERS = 485;

	public static final int PERSONAL_KINSHIP_TYPE_OWN = 294;

	private int motoristId;
	private int motoristType;
	private String name;
	private String lastName;
	private String firstName;
	private Integer kinshipType;
	private Integer gender;
	private Date birthDate;
	private Integer maritalStatus;
	private Date licenseDate;
	private String licenseCode;
	private Integer documentType;
	private String documentNumber;
	private String documentIssuer;
	private Date documentValidity;
	private Short displayOrder;

	public int getMotoristId() {
		return motoristId;
	}

	public void setMotoristId(int motoristId) {
		this.motoristId = motoristId;
	}

	public int getMotoristType() {
		return this.motoristType;
	}

	public void setMotoristType(int motoristType) {
		this.motoristType = motoristType;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Integer getKinshipType() {
		return this.kinshipType;
	}

	public void setKinshipType(Integer kinshipType) {
		this.kinshipType = kinshipType;
	}

	public Integer getGender() {
		return this.gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Integer getMaritalStatus() {
		return this.maritalStatus;
	}

	public void setMaritalStatus(Integer maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public Date getLicenseDate() {
		return this.licenseDate;
	}

	public void setLicenseDate(Date licenseDate) {
		this.licenseDate = licenseDate;
	}

	public String getLicenseCode() {
		return this.licenseCode;
	}

	public void setLicenseCode(String licenseCode) {
		this.licenseCode = licenseCode;
	}

	public Integer getDocumentType() {
		return this.documentType;
	}

	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	public String getDocumentNumber() {
		return this.documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getDocumentIssuer() {
		return this.documentIssuer;
	}

	public void setDocumentIssuer(String documentIssuer) {
		this.documentIssuer = documentIssuer;
	}

	public Date getDocumentValidity() {
		return this.documentValidity;
	}

	public void setDocumentValidity(Date documentValidity) {
		this.documentValidity = documentValidity;
	}

	public Short getDisplayOrder() {
		return this.displayOrder;
	}

	public void setDisplayOrder(Short displayOrder) {
		this.displayOrder = displayOrder;
	}
}
