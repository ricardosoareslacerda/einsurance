package br.com.tratomais.core.soap.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Endorsement implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public final static int ISSUING_STATUS_QUOTATION = 104;
	public final static int ISSUING_STATUS_PROPOSAL = 105;
	public final static int ISSUING_STATUS_EMISSION = 106;
	public final static int ISSUING_STATUS_CANCELLATION = 107;

	public final static int ISSUANCE_TYPE_EMISSION = 6;
	public final static int ISSUANCE_TYPE_REACTIVATION = 15;
	public final static int ISSUANCE_TYPE_CHANGE_REGISTER = 21;
	public final static int ISSUANCE_TYPE_CHANGE_TECHNICAL = 22;
	public final static int ISSUANCE_TYPE_CANCELLATION_CLAIM = 11;
	public final static int ISSUANCE_TYPE_CANCELLATION_INSURED = 8;
	public final static int ISSUANCE_TYPE_CANCELLATION_EMISSION_ERROR = 7;
	public final static int ISSUANCE_TYPE_CANCELLATION_NO_PAYMENT = 28;

	public final static int ENDORSEMENT_TYPE_COLLECTION = 30;
	public final static int ENDORSEMENT_TYPE_REFUND = 33;
	public final static int ENDORSEMENT_TYPE_NOT = 35;

	public static final int CANCEL_REASON_OTHERS = 567;

	public final static int BILLING_METHOD_ZURICH = 29;
	public final static int PAYMENT_TERM_UNIQUE = 13;

	private int endorsementId;
	private Contract contract;
	private Customer customerByInsuredId;
	private Customer customerByPolicyHolderId;
	private Integer endorsedId;
	private Date issuanceDate;
	private int issuanceType;
	private int userId;
	private int channelId;
	private int riskPlanId;
	private int termId;
	private int hierarchyType;
	//	private Integer authorizationType;
	private Integer paymentTermId;
	private Date referenceDate;
	private Date effectiveDate;
	private Date expiryDate;
	private Date refusalDate;
	private Date cancelDate;
	private Long quotationNumber;
	private Date quotationDate;
	private Long proposalNumber;
	private Date proposalDate;
	private Long endorsedNumber;
	private Integer endorsementType;
	private Long endorsementNumber;
	private Date endorsementDate;
	//	private Double purePremium;
	//	private Double tariffPremium;
	//	private Double commercialPremium;
	//	private Double profitPremium;
	//	private Double technicalPremium;
	//	private Double retainedPremium;
	//	private Double earnedPremium;
	//	private Double unearnedPremium;
	private Double netPremium;
	private Double commission;
	private Double commissionFactor;
	//	private Double labour;
	//	private Double labourFactor;
	//	private Double agency;
	//	private Double agencyFactor;
	private Double policyCost;
	private Double inspectionCost;
	private Double fractioningAdditional;
	private Integer taxExemptionType;
	private Double taxRate;
	private Double taxValue;
	private Double totalPremium;
	//	private boolean transmitted;
	private Integer issuingStatus;
	//	private Boolean changeLocked;
	private Integer cancelReason;
	//	private String claimNumber;
	//	private Date claimDate;
	//	private Date claimNotification;

	private Item[] items = new Item[0];
	private ErrorList errorList;

	public int getEndorsementId() {
		return this.endorsementId;
	}

	public void setEndorsementId(int endorsementId) {
		this.endorsementId = endorsementId;
	}

	public Contract getContract() {
		return this.contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Customer getCustomerByInsuredId() {
		return this.customerByInsuredId;
	}

	public void setCustomerByInsuredId(Customer customerByInsuredId) {
		this.customerByInsuredId = customerByInsuredId;
	}

	public Customer getCustomerByPolicyHolderId() {
		return this.customerByPolicyHolderId;
	}

	public void setCustomerByPolicyHolderId(Customer customerByPolicyHolderId) {
		this.customerByPolicyHolderId = customerByPolicyHolderId;
	}
	
	public Integer getEndorsedId() {
		return this.endorsedId;
	}

	public void setEndorsedId(Integer endorsedId) {
		if (endorsedId != null)
			if (endorsedId == 0)
				endorsedId = null;
		this.endorsedId = endorsedId;
	}	

	public Date getIssuanceDate() {
		return this.issuanceDate;
	}

	public void setIssuanceDate(Date issuanceDate) {
		this.issuanceDate = issuanceDate;
	}

	public int getIssuanceType() {
		return this.issuanceType;
	}

	public void setIssuanceType(int issuanceType) {
		this.issuanceType = issuanceType;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getChannelId() {
		return this.channelId;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	public int getRiskPlanId() {
		return this.riskPlanId;
	}

	public void setRiskPlanId(int riskPlanId) {
		this.riskPlanId = riskPlanId;
	}

	public int getTermId() {
		return this.termId;
	}

	public void setTermId(int termId) {
		this.termId = termId;
	}

	public int getHierarchyType() {
		return this.hierarchyType;
	}

	public void setHierarchyType(int hierarchyType) {
		this.hierarchyType = hierarchyType;
	}

	//	public Integer getAuthorizationType() {
	//		return this.authorizationType;
	//	}
	//
	//	public void setAuthorizationType(Integer authorizationType) {
	//		this.authorizationType = authorizationType;
	//	}

	public Integer getPaymentTermId() {
		return this.paymentTermId;
	}

	public void setPaymentTermId(Integer paymentTermId) {
		this.paymentTermId = paymentTermId;
	}

	public Date getReferenceDate() {
		return this.referenceDate;
	}

	public void setReferenceDate(Date referenceDate) {
		this.referenceDate = referenceDate;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Date getRefusalDate() {
		return this.refusalDate;
	}

	public void setRefusalDate(Date refusalDate) {
		this.refusalDate = refusalDate;
	}

	public Date getCancelDate() {
		return this.cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public long getQuotationNumber() {
		return this.quotationNumber;
	}

	public void setQuotationNumber(long quotationNumber) {
		this.quotationNumber = quotationNumber;
	}

	public Date getQuotationDate() {
		return this.quotationDate;
	}

	public void setQuotationDate(Date quotationDate) {
		this.quotationDate = quotationDate;
	}

	public Long getProposalNumber() {
		return this.proposalNumber;
	}

	public void setProposalNumber(Long proposalNumber) {
		this.proposalNumber = proposalNumber;
	}

	public Date getProposalDate() {
		return this.proposalDate;
	}

	public void setProposalDate(Date proposalDate) {
		this.proposalDate = proposalDate;
	}

	public Long getEndorsedNumber() {
		return this.endorsedNumber;
	}

	public void setEndorsedNumber(Long endorsedNumber) {
		this.endorsedNumber = endorsedNumber;
	}

	public Integer getEndorsementType() {
		return this.endorsementType;
	}

	public void setEndorsementType(Integer endorsementType) {
		this.endorsementType = endorsementType;
	}

	public Long getEndorsementNumber() {
		return this.endorsementNumber;
	}

	public void setEndorsementNumber(Long endorsementNumber) {
		this.endorsementNumber = endorsementNumber;
	}

	public Date getEndorsementDate() {
		return this.endorsementDate;
	}

	public void setEndorsementDate(Date endorsementDate) {
		this.endorsementDate = endorsementDate;
	}

	//	public Double getPurePremium() {
	//		return this.purePremium;
	//	}
	//
	//	public void setPurePremium(Double purePremium) {
	//		this.purePremium = purePremium;
	//	}
	//
	//	public Double getTariffPremium() {
	//		return this.tariffPremium;
	//	}
	//
	//	public void setTariffPremium(Double tariffPremium) {
	//		this.tariffPremium = tariffPremium;
	//	}
	//
	//	public Double getCommercialPremium() {
	//		return this.commercialPremium;
	//	}
	//
	//	public void setCommercialPremium(Double commercialPremium) {
	//		this.commercialPremium = commercialPremium;
	//	}
	//
	//	public Double getProfitPremium() {
	//		return this.profitPremium;
	//	}
	//
	//	public void setProfitPremium(Double profitPremium) {
	//		this.profitPremium = profitPremium;
	//	}
	//
	//	public void setTechnicalPremium(Double technicalPremium) {
	//		this.technicalPremium = technicalPremium;
	//	}
	//
	//	public Double getTechnicalPremium() {
	//		return technicalPremium;
	//	}
	//
	//	public Double getRetainedPremium() {
	//		return this.retainedPremium;
	//	}
	//
	//	public void setRetainedPremium(Double retainedPremium) {
	//		this.retainedPremium = retainedPremium;
	//	}
	//
	//	public Double getEarnedPremium() {
	//		return earnedPremium;
	//	}
	//
	//	public void setEarnedPremium(Double earnedPremium) {
	//		this.earnedPremium = earnedPremium;
	//	}
	//
	//	public Double getUnearnedPremium() {
	//		return unearnedPremium;
	//	}
	//
	//	public void setUnearnedPremium(Double unearnedPremium) {
	//		this.unearnedPremium = unearnedPremium;
	//	}

	public Double getNetPremium() {
		return this.netPremium;
	}

	public void setNetPremium(Double netPremium) {
		this.netPremium = netPremium;
	}

	public Double getCommission() {
		return this.commission;
	}

	public void setCommission(Double commission) {
		this.commission = commission;
	}

	public Double getCommissionFactor() {
		return this.commissionFactor;
	}

	public void setCommissionFactor(Double commissionFactor) {
		this.commissionFactor = commissionFactor;
	}

	//	public Double getLabour() {
	//		return this.labour;
	//	}
	//
	//	public void setLabour(Double labour) {
	//		this.labour = labour;
	//	}
	//
	//	public Double getLabourFactor() {
	//		return this.labourFactor;
	//	}
	//
	//	public void setLabourFactor(Double labourFactor) {
	//		this.labourFactor = labourFactor;
	//	}
	//
	//	public Double getAgency() {
	//		return this.agency;
	//	}
	//
	//	public void setAgency(Double agency) {
	//		this.agency = agency;
	//	}
	//
	//	public Double getAgencyFactor() {
	//		return this.agencyFactor;
	//	}
	//
	//	public void setAgencyFactor(Double agencyFactor) {
	//		this.agencyFactor = agencyFactor;
	//	}

	public Double getPolicyCost() {
		return this.policyCost;
	}

	public void setPolicyCost(Double policyCost) {
		this.policyCost = policyCost;
	}

	public Double getInspectionCost() {
		return this.inspectionCost;
	}

	public void setInspectionCost(Double inspectionCost) {
		this.inspectionCost = inspectionCost;
	}

	public Double getFractioningAdditional() {
		return this.fractioningAdditional;
	}

	public void setFractioningAdditional(Double fractioningAdditional) {
		this.fractioningAdditional = fractioningAdditional;
	}

	public Integer getTaxExemptionType() {
		return this.taxExemptionType;
	}

	public void setTaxExemptionType(Integer taxExemptionType) {
		this.taxExemptionType = taxExemptionType;
	}

	public Double getTaxRate() {
		return this.taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Double getTaxValue() {
		return this.taxValue;
	}

	public void setTaxValue(Double taxValue) {
		this.taxValue = taxValue;
	}

	public Double getTotalPremium() {
		return this.totalPremium;
	}

	public void setTotalPremium(Double totalPremium) {
		this.totalPremium = totalPremium;
	}

	//	public boolean isTransmitted() {
	//		return this.transmitted;
	//	}
	//
	//	public void setTransmitted(boolean transmitted) {
	//		this.transmitted = transmitted;
	//	}

	public int getIssuingStatus() {
		return this.issuingStatus;
	}

	public void setIssuingStatus(int issuingStatus) {
		this.issuingStatus = issuingStatus;
	}

	//	public Boolean getChangeLocked() {
	//		return this.changeLocked;
	//	}
	//
	//	public void setChangeLocked(Boolean changeLocked) {
	//		this.changeLocked = changeLocked;
	//	}

	public Integer getCancelReason() {
		return this.cancelReason;
	}

	public void setCancelReason(Integer cancelReason) {
		this.cancelReason = cancelReason;
	}

	//	/**
	//	 * @return the chaimNumber
	//	 */
	//	public String getClaimNumber() {
	//		return claimNumber;
	//	}
	//
	//	/**
	//	 * @param chaimNumber the chaimNumber to set
	//	 */
	//	public void setClaimNumber(String chaimNumber) {
	//		this.claimNumber = chaimNumber;
	//	}
	//
	//	/**
	//	 * @return the claimDate
	//	 */
	//	public Date getClaimDate() {
	//		return claimDate;
	//	}
	//
	//	/**
	//	 * @param claimDate the claimDate to set
	//	 */
	//	public void setClaimDate(Date claimDate) {
	//		this.claimDate = claimDate;
	//	}
	//
	//	/**
	//	 * @return the claimNotification
	//	 */
	//	public Date getClaimNotification() {
	//		return claimNotification;
	//	}
	//
	//	/**
	//	 * @param claimNotification the claimNotification to set
	//	 */
	//	public void setClaimNotification(Date claimNotification) {
	//		this.claimNotification = claimNotification;
	//	}

	public Item[] getItems() {
		return items;
	}

	public void setItems(Item[] items) {
		this.items = items;
	}

	public ErrorList getErrorList() {
		if (errorList == null)
			errorList = new ErrorList();
		return errorList;
	}

	public void setErrorList(ErrorList errorList){
		this.errorList = errorList;
	}

	public void addItems(List<Item> itensNovos){
		List<Item> asList = new ArrayList<Item>(Arrays.asList(this.items));
		asList.addAll(itensNovos);
		this.items = asList.toArray(new Item[asList.size()]);
	}

	public void addItem(Item item){
		List<Item> asList = new ArrayList<Item>(Arrays.asList(this.items));
		asList.add(item);
		this.items = asList.toArray(new Item[asList.size()]);
	}

	public Item addItem(int objectId, int itemId){
		Item newItem = new Item();
		switch ( objectId ) {
			case Item.OBJECT_LIFE:
			case Item.OBJECT_CREDIT:
			case Item.OBJECT_LIFE_CYCLE:
			case Item.OBJECT_PERSONAL_ACCIDENTS:
				newItem.setItemPersonalRisk(new ItemPersonalRisk());
				break;
			case Item.OBJECT_PURCHASE_PROTECTED:
				newItem.setItemPurchaseProtected(new ItemPurchaseProtected());
				break;
			case Item.OBJECT_HABITAT:
			case Item.OBJECT_CAPITAL:
				newItem.setItemProperty(new ItemProperty());
				break;
			case Item.OBJECT_AUTO:
				newItem.setItemAuto(new ItemAuto());
				break;
			default:
				throw new IllegalArgumentException();
		}		
		newItem.setItemId( itemId );
		newItem.setObjectId( objectId );
		this.addItem( newItem );
		return newItem;
	}
}
