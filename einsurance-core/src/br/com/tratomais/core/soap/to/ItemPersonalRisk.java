package br.com.tratomais.core.soap.to;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ItemPersonalRisk implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4313649792955707854L;

	public static final int PERSONAL_RISK_TYPE_HOLDER = 293;
	public static final int PERSONAL_RISK_TYPE_FAMILY_GROUP = 194;
	public static final int PERSONAL_RISK_TYPE_ADDITIONAL = 195;
	public static final int PERSONAL_RISK_TYPE_NEWBORN = 333;

	public static final int PERSONAL_KINSHIP_TYPE_OWN = 294;

	private String insuredName;
	private String lastName;
	private String firstName;
	private Integer gender;
	private Date birthDate;
	private Date hireDate;
	private Boolean smoker;
	private Integer ageActuarial;
	private Integer maritalStatus;
	private Short childrenNumber;
	private boolean undocumented;	
	private Integer documentType;
	private String documentNumber;
	private String documentIssuer;
	private Date documentValidity;
	private Integer professionId;
	private Double inflow;
	private Integer occupationId;
	private Integer occupationRiskType;
	private Double personalRiskValue;
	private Date creditEffectiveDate;
	private Date creditExpiryDate;
	private Integer personalRiskType;
	private Integer kinshipType;
	private Integer restrictionRiskType;

	private ItemPersonalRiskGroup[] itemPersonalRiskGroups = new ItemPersonalRiskGroup[0];

	public String getInsuredName() {
		return this.insuredName;
	}

	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Integer getGender() {
		return this.gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getHireDate() {
		return this.hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public Boolean getSmoker() {
		return this.smoker;
	}

	public void setSmoker(Boolean smoker) {
		this.smoker = smoker;
	}

	public Integer getAgeActuarial() {
		return this.ageActuarial;
	}

	public void setAgeActuarial(Integer ageActuarial) {
		this.ageActuarial = ageActuarial;
	}

	public Integer getMaritalStatus() {
		return this.maritalStatus;
	}

	public void setMaritalStatus(Integer maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public Short getChildrenNumber() {
		return this.childrenNumber;
	}

	public void setChildrenNumber(Short childrenNumber) {
		this.childrenNumber = childrenNumber;
	}

	public boolean isUndocumented() {
		return this.undocumented;
	}

	public void setUndocumented(boolean undocumented) {
		this.undocumented = undocumented;
	}

	public Integer getDocumentType() {
		return this.documentType;
	}

	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	public String getDocumentNumber() {
		return this.documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getDocumentIssuer() {
		return this.documentIssuer;
	}

	public void setDocumentIssuer(String documentIssuer) {
		this.documentIssuer = documentIssuer;
	}

	public Date getDocumentValidity() {
		return this.documentValidity;
	}

	public void setDocumentValidity(Date documentValidity) {
		this.documentValidity = documentValidity;
	}

	public Integer getProfessionId() {
		return this.professionId;
	}

	public void setProfessionId(Integer professionId) {
		this.professionId = professionId;
	}

	public Double getInflow() {
		return this.inflow;
	}

	public void setInflow(Double inflow) {
		this.inflow = inflow;
	}

	public Integer getOccupationId() {
		return this.occupationId;
	}

	public void setOccupationId(Integer occupationId) {
		this.occupationId = occupationId;
	}

	public Integer getOccupationRiskType() {
		return this.occupationRiskType;
	}

	public void setOccupationRiskType(Integer occupationRiskType) {
		this.occupationRiskType = occupationRiskType;
	}

	public Double getPersonalRiskValue() {
		return this.personalRiskValue;
	}

	public void setPersonalRiskValue(Double personalRiskValue) {
		this.personalRiskValue = personalRiskValue;
	}

	public Date getCreditEffectiveDate() {
		return this.creditEffectiveDate;
	}

	public void setCreditEffectiveDate(Date creditEffectiveDate) {
		this.creditEffectiveDate = creditEffectiveDate;
	}

	public Date getCreditExpiryDate() {
		return this.creditExpiryDate;
	}

	public void setCreditExpiryDate(Date creditExpiryDate) {
		this.creditExpiryDate = creditExpiryDate;
	}

	public Integer getPersonalRiskType() {
		return this.personalRiskType;
	}

	public void setPersonalRiskType(Integer personalRiskType) {
		this.personalRiskType = personalRiskType;
	}

	public Integer getKinshipType() {
		return this.kinshipType;
	}

	public void setKinshipType(Integer kinshipType) {
		this.kinshipType = kinshipType;
	}

	public Integer getRestrictionRiskType() {
		return this.restrictionRiskType;
	}

	public void setRestrictionRiskType(Integer restrictionRiskType) {
		this.restrictionRiskType = restrictionRiskType;
	}

	public ItemPersonalRiskGroup [] getItemPersonalRiskGroups() {
		return this.itemPersonalRiskGroups;
	}

	public void setItemPersonalRiskGroups(ItemPersonalRiskGroup [] itemPersonalRiskGroups) {
		this.itemPersonalRiskGroups = itemPersonalRiskGroups;
	}

	public void addItemPersonalRiskGroup(ItemPersonalRiskGroup itemPersonalRiskGroup){
		List<ItemPersonalRiskGroup> asList = Arrays.asList(this.itemPersonalRiskGroups);
		asList.add(itemPersonalRiskGroup);
		this.itemPersonalRiskGroups = (ItemPersonalRiskGroup[]) asList.toArray();
	}

	public final ItemPersonalRiskGroup addItemPersonalRiskGroup(int riskGroupId) {
		ItemPersonalRiskGroup newItemPersonalRiskGroup = new ItemPersonalRiskGroup();
		newItemPersonalRiskGroup.setRiskGroupId( riskGroupId );
		this.addItemPersonalRiskGroup( newItemPersonalRiskGroup );
		return newItemPersonalRiskGroup;
	}
}
