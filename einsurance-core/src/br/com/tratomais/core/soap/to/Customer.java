package br.com.tratomais.core.soap.to;

import java.io.Serializable;
import java.util.Date;

public class Customer implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1;

	public static final int PERSON_TYPE_NATURAL = 63;
	public static final int PERSON_TYPE_JURIDICAL = 64;
	public static final int PERSON_TYPE_GOVERNMENTAL = 66;

	private int customerId;
	private String name;
	private String lastName;
	private String firstName;
	private String tradeName;
	private Integer personType;
	private Integer gender;
	private Date birthDate;
	private Date registerDate;
	private Integer maritalStatus;
	private Integer documentType;
	private String documentNumber;
	private String documentIssuer;
	private Date documentValidity;
	private Integer professionType;
	private Integer professionId;
	private Integer occupationId;
	private Integer activityId;
	private Integer inflowId;
	private String phoneNumber;
	private String emailAddress;
	private boolean active;

	private Contact [] contacts = new Contact[0];
	private Address [] addresses = new Address[0];

	public int getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getTradeName() {
		return this.tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public Integer getPersonType() {
		return this.personType;
	}

	public void setPersonType(Integer personType) {
		this.personType = personType;
	}

	public Integer getGender() {
		return this.gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getRegisterDate() {
		return this.registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public Integer getMaritalStatus() {
		return this.maritalStatus;
	}

	public void setMaritalStatus(Integer maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public Integer getDocumentType() {
		return this.documentType;
	}

	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	public String getDocumentNumber() {
		return this.documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getDocumentIssuer() {
		return this.documentIssuer;
	}

	public void setDocumentIssuer(String documentIssuer) {
		this.documentIssuer = documentIssuer;
	}

	public Date getDocumentValidity() {
		return this.documentValidity;
	}

	public void setDocumentValidity(Date documentValidity) {
		this.documentValidity = documentValidity;
	}

	public Integer getProfessionType() {
		return this.professionType;
	}

	public void setProfessionType(Integer professionType) {
		if (professionType != null)
			if (professionType == 0)
				professionType = null;		
		this.professionType = professionType;
	}

	public Integer getProfessionId() {
		return this.professionId;
	}

	public void setProfessionId(Integer professionId) {
		this.professionId = professionId;
	}

	public Integer getOccupationId() {
		return this.occupationId;
	}

	public void setOccupationId(Integer occupationId) {
		this.occupationId = occupationId;
	}

	public Integer getActivityId() {
		return this.activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}

	public Integer getInflowId() {
		return this.inflowId;
	}

	public void setInflowId(Integer inflowId) {
		if (inflowId != null)
			if (inflowId == 0)
				inflowId = null;
		this.inflowId = inflowId;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Contact[] getContacts() {
		return this.contacts;
	}

	public void setContacts(Contact[] contacts) {
		this.contacts = contacts;
	}

	public Address[] getAddresses() {
		return this.addresses;
	}

	public void setAddresses(Address[] addresses) {
		this.addresses = addresses;
	}
}
