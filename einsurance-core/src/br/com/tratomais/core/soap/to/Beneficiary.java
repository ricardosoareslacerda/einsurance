package br.com.tratomais.core.soap.to;

import java.io.Serializable;
import java.util.Date;

public class Beneficiary implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final int BENEFICIARY_TYPE_DESIGNATE = 146;
	public static final int BENEFICIARY_TYPE_PREFERENCE = 147;

	private int beneficiaryId;	
	private int beneficiaryType;
	private String name;
	private String lastName;
	private String firstName;
	private int kinshipType;
	private int personType;
	private Integer gender;
	private Date birthDate;
	private Integer maritalStatus;
	private boolean undocumented;
	private Integer documentType;
	private String documentNumber;
	private String documentIssuer;
	private Date documentValidity;
	private Double participationPercentage;
	private Short displayOrder;

	public Beneficiary() {
	}

	public int getBeneficiaryId() {
		return beneficiaryId;
	}

	public void setBeneficiaryId(int beneficiaryId) {
		this.beneficiaryId = beneficiaryId;
	}

	public int getBeneficiaryType() {
		return this.beneficiaryType;
	}

	public void setBeneficiaryType(int beneficiaryType) {
		this.beneficiaryType = beneficiaryType;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getKinshipType() {
		return this.kinshipType;
	}

	public void setKinshipType(int kinshipType) {
		this.kinshipType = kinshipType;
	}

	public int getPersonType() {
		return this.personType;
	}

	public void setPersonType(int personType) {
		this.personType = personType;
	}

	public Integer getGender() {
		return this.gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Integer getMaritalStatus() {
		return this.maritalStatus;
	}

	public void setMaritalStatus(Integer maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public boolean isUndocumented() {
		return this.undocumented;
	}

	public void setUndocumented(boolean undocumented) {
		this.undocumented = undocumented;
	}

	public Integer getDocumentType() {
		return this.documentType;
	}

	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	public String getDocumentNumber() {
		return this.documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getDocumentIssuer() {
		return this.documentIssuer;
	}

	public void setDocumentIssuer(String documentIssuer) {
		this.documentIssuer = documentIssuer;
	}

	public Date getDocumentValidity() {
		return this.documentValidity;
	}

	public void setDocumentValidity(Date documentValidity) {
		this.documentValidity = documentValidity;
	}

	public Double getParticipationPercentage() {
		return this.participationPercentage;
	}

	public void setParticipationPercentage(Double participationPercentage) {
		this.participationPercentage = participationPercentage;
	}

	public Short getDisplayOrder() {
		return this.displayOrder;
	}

	public void setDisplayOrder(Short displayOrder) {
		this.displayOrder = displayOrder;
	}
}
