package br.com.tratomais.core.soap.to;

import java.io.Serializable;

public class ItemCoverage implements Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4034671856610385017L;

	private int coverageId;
	private String coverageCode;
	private String coverageName;
	private boolean basicCoverage;
	private Integer groupCoverageId;
	private Boolean coverageCancel;
	private String goodsCode;
	private Integer branchId;
	private Double insuredValue;
	private Integer rangeValueId;
//	private Double pureRate;
//	private Double tariffRate;
//	private Double commercialRate;
//	private Double spendingRate;
//	private Double profitRate;
//	private Double netRate;
//	private Double tariffFactor;
//	private Double commercialFactor;
//	private Double purePremium;
//	private Double tariffPremium;
//	private Double commercialPremium;
//	private Double profitPremium;
//	private Double technicalPremium;
//	private Double retainedPremium;
//	private Double earnedPremium;
//	private Double unearnedPremium;
	private Double netPremium;
	private Double fractioningAdditional;
	private Boolean taxIncidence;
	private Double taxValue;
	private Double totalPremium;
	private Integer deductibleId;
	private Double deductibleValue;
	private Double deductibleValueMinimum;
	private Double deductibleValueMaximum;
	private String deductibleDescription;
	private Double commission;
	private Double commissionFactor;
	private Double commissionFactorOriginal;
//	private Double labour;
//	private Double labourFactor;
//	private Double agency;
//	private Double agencyFactor;
	private Double bonus;
	private Double bonusFactor;
	private Short displayOrder;

	public int getCoverageId() {
		return coverageId;
	}

	public void setCoverageId(int coverageId) {
		this.coverageId = coverageId;
	}

	public String getCoverageCode() {
		return this.coverageCode;
	}

	public void setCoverageCode(String coverageCode) {
		this.coverageCode = coverageCode;
	}

	public String getCoverageName() {
		return this.coverageName;
	}

	public void setCoverageName(String coverageName) {
		this.coverageName = coverageName;
	}

	public boolean isBasicCoverage() {
		return this.basicCoverage;
	}

	public void setBasicCoverage(boolean basicCoverage) {
		this.basicCoverage = basicCoverage;
	}

	public Integer getGroupCoverageId() {
		return this.groupCoverageId;
	}

	public void setGroupCoverageId(Integer groupCoverageId) {
		this.groupCoverageId = groupCoverageId;
	}

	public Boolean getCoverageCancel() {
		return this.coverageCancel;
	}

	public void setCoverageCancel(Boolean coverageCancel) {
		this.coverageCancel = coverageCancel;
	}

	public String getGoodsCode() {
		return this.goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	public Integer getBranchId() {
		return this.branchId;
	}

	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}

	public Double getInsuredValue() {
		return this.insuredValue;
	}

	public void setInsuredValue(Double insuredValue) {
		this.insuredValue = insuredValue;
	}

	public Integer getRangeValueId() {
		return this.rangeValueId;
	}

	public void setRangeValueId(Integer rangeValueId) {
		this.rangeValueId = rangeValueId;
	}

//	public Double getPureRate() {
//		return this.pureRate;
//	}
//
//	public void setPureRate(Double pureRate) {
//		this.pureRate = pureRate;
//	}
//
//	public Double getTariffRate() {
//		return this.tariffRate;
//	}
//
//	public void setTariffRate(Double tariffRate) {
//		this.tariffRate = tariffRate;
//	}
//
//	public Double getCommercialRate() {
//		return this.commercialRate;
//	}
//
//	public void setCommercialRate(Double commercialRate) {
//		this.commercialRate = commercialRate;
//	}
//
//	public Double getSpendingRate() {
//		return this.spendingRate;
//	}
//
//	public void setSpendingRate(Double spendingRate) {
//		this.spendingRate = spendingRate;
//	}
//
//	public Double getProfitRate() {
//		return this.profitRate;
//	}
//
//	public void setProfitRate(Double profitRate) {
//		this.profitRate = profitRate;
//	}
//
//	public Double getNetRate() {
//		return this.netRate;
//	}
//
//	public void setNetRate(Double netRate) {
//		this.netRate = netRate;
//	}
//
//	public Double getTariffFactor() {
//		return this.tariffFactor;
//	}
//
//	public void setTariffFactor(Double tariffFactor) {
//		this.tariffFactor = tariffFactor;
//	}
//
//	public Double getCommercialFactor() {
//		return this.commercialFactor;
//	}
//
//	public void setCommercialFactor(Double commercialFactor) {
//		this.commercialFactor = commercialFactor;
//	}
//
//	public Double getPurePremium() {
//		return this.purePremium;
//	}
//
//	public void setPurePremium(Double purePremium) {
//		this.purePremium = purePremium;
//	}
//
//	public Double getTariffPremium() {
//		return this.tariffPremium;
//	}
//
//	public void setTariffPremium(Double tariffPremium) {
//		this.tariffPremium = tariffPremium;
//	}
//
//	public Double getCommercialPremium() {
//		return this.commercialPremium;
//	}
//
//	public void setCommercialPremium(Double commercialPremium) {
//		this.commercialPremium = commercialPremium;
//	}
//
//	public Double getProfitPremium() {
//		return this.profitPremium;
//	}
//
//	public void setProfitPremium(Double profitPremium) {
//		this.profitPremium = profitPremium;
//	}
//
//	public Double getTechnicalPremium() {
//		return technicalPremium;
//	}
//
//	public void setTechnicalPremium(Double technicalPremium) {
//		this.technicalPremium = technicalPremium;
//	}
//
//	public Double getRetainedPremium() {
//		return this.retainedPremium;
//	}
//
//	public void setRetainedPremium(Double retainedPremium) {
//		this.retainedPremium = retainedPremium;
//	}
//
//	public Double getEarnedPremium() {
//		return earnedPremium;
//	}
//
//	public void setEarnedPremium(Double earnedPremium) {
//		this.earnedPremium = earnedPremium;
//	}
//
//	public Double getUnearnedPremium() {
//		return unearnedPremium;
//	}
//
//	public void setUnearnedPremium(Double unearnedPremium) {
//		this.unearnedPremium = unearnedPremium;
//	}

	public Double getNetPremium() {
		return this.netPremium;
	}

	public void setNetPremium(Double netPremium) {
		this.netPremium = netPremium;
	}

	public Double getFractioningAdditional() {
		return this.fractioningAdditional;
	}

	public void setFractioningAdditional(Double fractioningAdditional) {
		this.fractioningAdditional = fractioningAdditional;
	}

	public Boolean getTaxIncidence() {
		return this.taxIncidence;
	}

	public void setTaxIncidence(Boolean taxIncidence) {
		this.taxIncidence = taxIncidence;
	}

	public Double getTaxValue() {
		return this.taxValue;
	}

	public void setTaxValue(Double taxValue) {
		this.taxValue = taxValue;
	}

	public Double getTotalPremium() {
		return this.totalPremium;
	}

	public void setTotalPremium(Double totalPremium) {
		this.totalPremium = totalPremium;
	}

	public Integer getDeductibleId() {
		return this.deductibleId;
	}

	public void setDeductibleId(Integer deductibleId) {
		this.deductibleId = deductibleId;
	}

	public Double getDeductibleValue() {
		return deductibleValue;
	}

	public void setDeductibleValue(Double deductibleValue) {
		this.deductibleValue = deductibleValue;
	}

	public Double getDeductibleValueMinimum() {
		return this.deductibleValueMinimum;
	}

	public void setDeductibleValueMinimum(Double deductibleValueMinimum) {
		this.deductibleValueMinimum = deductibleValueMinimum;
	}

	public Double getDeductibleValueMaximum() {
		return this.deductibleValueMaximum;
	}

	public void setDeductibleValueMaximum(Double deductibleValueMaximum) {
		this.deductibleValueMaximum = deductibleValueMaximum;
	}

	public String getDeductibleDescription() {
		return this.deductibleDescription;
	}

	public void setDeductibleDescription(String deductibleDescription) {
		this.deductibleDescription = deductibleDescription;
	}

	public Double getCommission() {
		return this.commission;
	}

	public void setCommission(Double commission) {
		this.commission = commission;
	}

	public Double getCommissionFactor() {
		return this.commissionFactor;
	}

	public void setCommissionFactor(Double commissionFactor) {
		this.commissionFactor = commissionFactor;
	}

	public Double getCommissionFactorOriginal() {
		return this.commissionFactorOriginal;
	}

	public void setCommissionFactorOriginal(Double commissionFactorOriginal) {
		this.commissionFactorOriginal = commissionFactorOriginal;
	}

//	public Double getLabour() {
//		return this.labour;
//	}
//
//	public void setLabour(Double labour) {
//		this.labour = labour;
//	}
//
//	public Double getLabourFactor() {
//		return this.labourFactor;
//	}
//
//	public void setLabourFactor(Double labourFactor) {
//		this.labourFactor = labourFactor;
//	}
//
//	public Double getAgency() {
//		return this.agency;
//	}
//
//	public void setAgency(Double agency) {
//		this.agency = agency;
//	}
//
//	public Double getAgencyFactor() {
//		return this.agencyFactor;
//	}
//
//	public void setAgencyFactor(Double agencyFactor) {
//		this.agencyFactor = agencyFactor;
//	}

	public Double getBonus() {
		return this.bonus;
	}

	public void setBonus(Double bonus) {
		this.bonus = bonus;
	}

	public Double getBonusFactor() {
		return this.bonusFactor;
	}

	public void setBonusFactor(Double bonusFactor) {
		this.bonusFactor = bonusFactor;
	}

	public Short getDisplayOrder() {
		return this.displayOrder;
	}

	public void setDisplayOrder(Short displayOrder) {
		this.displayOrder = displayOrder;
	}
}
