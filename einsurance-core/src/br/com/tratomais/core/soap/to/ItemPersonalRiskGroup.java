package br.com.tratomais.core.soap.to;

import java.io.Serializable;
import java.util.Date;

public class ItemPersonalRiskGroup implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1053230345233749067L;

	private int riskGroupId;
	private int personalRiskType;
	private String insuredName;
	private String lastName;
	private String firstName;
	private int kinshipType;
	private Integer gender;
	private Date birthDate;
	private Date hireDate;
	private Integer maritalStatus;
	private Short childrenNumber;
	private boolean undocumented;
	private Integer documentType;
	private String documentNumber;
	private String documentIssuer;
	private Date documentValidity;
	private Date effectiveDate;
	private Date expiryDate;
	//	private String claimNumber;
	//	private Date claimDate;
	//	private Date claimNotification;

	public int getRiskGroupId() {
		return riskGroupId;
	}

	public void setRiskGroupId(int riskGroupId) {
		this.riskGroupId = riskGroupId;
	}

	public int getPersonalRiskType() {
		return this.personalRiskType;
	}

	public void setPersonalRiskType(int personalRiskType) {
		this.personalRiskType = personalRiskType;
	}

	public String getInsuredName() {
		return this.insuredName;
	}

	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getKinshipType() {
		return this.kinshipType;
	}

	public void setKinshipType(int kinshipType) {
		this.kinshipType = kinshipType;
	}

	public Integer getGender() {
		return this.gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getHireDate() {
		return this.hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public Integer getMaritalStatus() {
		return this.maritalStatus;
	}

	public void setMaritalStatus(Integer maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public Short getChildrenNumber() {
		return this.childrenNumber;
	}

	public void setChildrenNumber(Short childrenNumber) {
		this.childrenNumber = childrenNumber;
	}

	public boolean isUndocumented() {
		return this.undocumented;
	}

	public void setUndocumented(boolean undocumented) {
		this.undocumented = undocumented;
	}

	public Integer getDocumentType() {
		return this.documentType;
	}

	public void setDocumentType(Integer documentType) {
		this.documentType = documentType;
	}

	public String getDocumentNumber() {
		return this.documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getDocumentIssuer() {
		return this.documentIssuer;
	}

	public void setDocumentIssuer(String documentIssuer) {
		this.documentIssuer = documentIssuer;
	}

	public Date getDocumentValidity() {
		return this.documentValidity;
	}

	public void setDocumentValidity(Date documentValidity) {
		this.documentValidity = documentValidity;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	//	public String getClaimNumber() {
	//		return claimNumber;
	//	}
	//
	//	public void setClaimNumber(String claimNumber) {
	//		this.claimNumber = claimNumber;
	//	}
	//
	//	public Date getClaimDate() {
	//		return claimDate;
	//	}
	//
	//	public void setClaimDate(Date claimDate) {
	//		this.claimDate = claimDate;
	//	}
	//
	//	public Date getClaimNotification() {
	//		return claimNotification;
	//	}
	//
	//	public void setClaimNotification(Date claimNotification) {
	//		this.claimNotification = claimNotification;
	//	}
}
