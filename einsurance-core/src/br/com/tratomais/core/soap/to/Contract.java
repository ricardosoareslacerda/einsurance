package br.com.tratomais.core.soap.to;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

public class Contract implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5410996474474232440L;

	public final static int POLICY_TYPE_INDIVIDUAL = 3;
	public final static int POLICY_TYPE_COLLECTIVE = 4;

	public static final int CONTRACT_STATUS_ACTIVE = 101;
	public static final int CONTRACT_STATUS_ABROGATED = 102;
	public static final int CONTRACT_STATUS_FINALIZED = 632;

	private int contractId;
	private Integer parentId;
	private Integer renewalId;
	private int insurerId;
	private int subsidiaryId;
	private Integer brokerId;
	private int partnerId;
	private Integer masterPolicyId;
	private Integer policyType;
	private boolean policyInformed;
	private BigInteger policyNumber;
	private Long certificateNumber;
	private String externalKey;
	private int productId;
	private int currencyId;
	private Integer termId;
	//	private boolean renewed;
	private Date referenceDate;
	private Date effectiveDate;
	private Date expiryDate;
	private Date renewalLimitDate;
	private Date cancelDate;
	private int contractStatus;

	public int getContractId() {
		return this.contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Integer getParentId() {
		return this.parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getRenewalId() {
		return this.renewalId;
	}

	public void setRenewalId(Integer renewalId) {
		this.renewalId = renewalId;
	}

	public int getInsurerId() {
		return this.insurerId;
	}

	public void setInsurerId(int insurerId) {
		this.insurerId = insurerId;
	}

	public int getSubsidiaryId() {
		return this.subsidiaryId;
	}

	public void setSubsidiaryId(int subsidiaryId) {
		this.subsidiaryId = subsidiaryId;
	}

	public Integer getBrokerId() {
		return this.brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	public int getPartnerId() {
		return this.partnerId;
	}

	public void setPartnerId(int partnerId) {
		this.partnerId = partnerId;
	}

	public Integer getMasterPolicyId() {
		return this.masterPolicyId;
	}

	public void setMasterPolicyId(Integer masterPolicyId) {
		this.masterPolicyId = masterPolicyId;
	}

	public Integer getPolicyType() {
		return this.policyType;
	}

	public void setPolicyType(Integer policyType) {
		this.policyType = policyType;
	}

	public boolean isPolicyInformed() {
		return this.policyInformed;
	}

	public void setPolicyInformed(boolean policyInformed) {
		this.policyInformed = policyInformed;
	}

	public BigInteger getPolicyNumber() {
		return this.policyNumber;
	}

	public void setPolicyNumber(BigInteger policyNumber) {
		this.policyNumber = policyNumber;
	}

	public Long getCertificateNumber() {
		return this.certificateNumber;
	}

	public void setCertificateNumber(Long certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	public String getExternalKey() {
		return this.externalKey;
	}

	public void setExternalKey(String externalKey) {
		this.externalKey = externalKey;
	}

	public int getProductId() {
		return this.productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getCurrencyId() {
		return this.currencyId;
	}

	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}

	public Integer getTermId() {
		return this.termId;
	}

	public void setTermId(Integer termId) {
		this.termId = termId;
	}

	//	public boolean isRenewed() {
	//		return this.renewed;
	//	}
	//
	//	public void setRenewed(boolean renewed) {
	//		this.renewed = renewed;
	//	}

	public Date getReferenceDate() {
		return this.referenceDate;
	}

	public void setReferenceDate(Date referenceDate) {
		this.referenceDate = referenceDate;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Date getRenewalLimitDate() {
		return this.renewalLimitDate;
	}

	public void setRenewalLimitDate(Date renewalLimitDate) {
		this.renewalLimitDate = renewalLimitDate;
	}

	public Date getCancelDate() {
		return this.cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public int getContractStatus() {
		return this.contractStatus;
	}

	public void setContractStatus(int contractStatus) {
		this.contractStatus = contractStatus;
	}
}
