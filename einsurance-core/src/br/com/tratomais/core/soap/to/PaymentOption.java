package br.com.tratomais.core.soap.to;

import br.com.tratomais.core.dao.PersistentEntity;

public class PaymentOption implements PersistentEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7584375132198391065L;

	private int paymentId;
	private int paymentType;
	private int billingMethodId;
	private String paymentTermName;

	private int quantityInstallment;
	private double firstInstallmentValue;
	private double nextInstallmentValue;

	private double netPremium;
	private double fractioningAdditional;
	private double taxRate;
	private double totalPremium;

	private Integer bankNumber;
	private Integer bankAgencyNumber;
	private String bankAccountNumber;
	private String bankCheckNumber;
	private Integer cardType;
	private Integer cardBrandType;
	private String cardNumber;
	private Integer cardExpirationDate;
	private String cardHolderName;
	private Integer invoiceNumber;
	private String otherAccountNumber;
	private Long debitAuthorizationCode;
	private Long creditAuthorizationCode;

	public int getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}

	public int getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(int paymentType) {
		this.paymentType = paymentType;
	}

	public int getBillingMethodId() {
		return billingMethodId;
	}

	public void setBillingMethodId(int billingMethodId) {
		this.billingMethodId = billingMethodId;
	}

	public String getPaymentTermName() {
		return paymentTermName;
	}

	public void setPaymentTermName(String paymentTermName) {
		this.paymentTermName = paymentTermName;
	}

	public int getQuantityInstallment() {
		return quantityInstallment;
	}

	public void setQuantityInstallment(int quantityInstallment) {
		this.quantityInstallment = quantityInstallment;
	}

	public double getFirstInstallmentValue() {
		return firstInstallmentValue;
	}

	public void setFirstInstallmentValue(double firstInstallmentValue) {
		this.firstInstallmentValue = firstInstallmentValue;
	}

	public double getNextInstallmentValue() {
		return nextInstallmentValue;
	}

	public void setNextInstallmentValue(double nextInstallmentValue) {
		this.nextInstallmentValue = nextInstallmentValue;
	}

	public double getNetPremium() {
		return netPremium;
	}

	public void setNetPremium(double netPremium) {
		this.netPremium = netPremium;
	}

	public double getFractioningAdditional() {
		return fractioningAdditional;
	}

	public void setFractioningAdditional(double fractioningAdditional) {
		this.fractioningAdditional = fractioningAdditional;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public double getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(double totalPremium) {
		this.totalPremium = totalPremium;
	}

	public Integer getBankNumber() {
		return bankNumber;
	}

	public void setBankNumber(Integer bankNumber) {
		this.bankNumber = bankNumber;
	}

	public Integer getBankAgencyNumber() {
		return bankAgencyNumber;
	}

	public void setBankAgencyNumber(Integer bankAgencyNumber) {
		this.bankAgencyNumber = bankAgencyNumber;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getBankCheckNumber() {
		return bankCheckNumber;
	}

	public void setBankCheckNumber(String bankCheckNumber) {
		this.bankCheckNumber = bankCheckNumber;
	}

	public Integer getCardType() {
		return cardType;
	}

	public void setCardType(Integer cardType) {
		this.cardType = cardType;
	}

	public Integer getCardBrandType() {
		return cardBrandType;
	}

	public void setCardBrandType(Integer cardBrandType) {
		this.cardBrandType = cardBrandType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Integer getCardExpirationDate() {
		return cardExpirationDate;
	}

	public void setCardExpirationDate(Integer cardExpirationDate) {
		this.cardExpirationDate = cardExpirationDate;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public Integer getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(Integer invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getOtherAccountNumber() {
		return otherAccountNumber;
	}

	public void setOtherAccountNumber(String otherAccountNumber) {
		this.otherAccountNumber = otherAccountNumber;
	}

	public Long getDebitAuthorizationCode() {
		return debitAuthorizationCode;
	}

	public void setDebitAuthorizationCode(Long debitAuthorizationCode) {
		this.debitAuthorizationCode = debitAuthorizationCode;
	}

	public Long getCreditAuthorizationCode() {
		return creditAuthorizationCode;
	}

	public void setCreditAuthorizationCode(Long creditAuthorizationCode) {
		this.creditAuthorizationCode = creditAuthorizationCode;
	}
}
