package br.com.tratomais.core.soap.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Item implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public final static int ITEM_STATUS_ACTIVE = 110;
	public final static int ITEM_STATUS_INACTIVE = 111;

	public final static int RENEW_TYPE_EMISSION = 91;
	public final static int RENEW_TYPE_RENEWAL = 93;

	public static final int OBJECT_LIFE = 1;
	public static final int OBJECT_PERSONAL_ACCIDENTS = 2;
	public static final int OBJECT_LIFE_CYCLE = 3;
	public static final int OBJECT_HABITAT = 4;
	public static final int OBJECT_CREDIT = 5;
	public static final int OBJECT_AUTO = 6;
	public static final int OBJECT_PURCHASE_PROTECTED = 7;
	public static final int OBJECT_CAPITAL = 8;

	private int itemId;
//	private Customer customer;
	private int objectId;
	private int coveragePlanId;
	private int renewalType;
	private Integer renewalInsurerId;
	private String renewalPolicyNumber;
	private Integer bonusType;
	private Date effectiveDate;
	private Date cancelDate;
	//	private Double tariffFactor;
	//	private Double commercialFactor;
	//	private Double purePremium;
	//	private Double tariffPremium;
	//	private Double commercialPremium;
	//	private Double profitPremium;
	//	private Double technicalPremium;
	//	private Double retainedPremium;
	//	private Double earnedPremium;
	//	private Double unearnedPremium;
	private Double netPremium;
	private Double commission;
	private Double commissionFactor;
	//	private Double labour;
	//	private Double labourFactor;
	//	private Double agency;
	//	private Double agencyFactor;
	private Double fractioningAdditional;
	private Double taxValue;
	private Double totalPremium;
	private Integer authorizationLevel;
	private boolean calculationValid;
	private Integer itemStatus;
	//	private String claimNumber;
	//	private Date claimDate;
	//	private Date claimNotification;

	private Beneficiary[] beneficiaries = new Beneficiary[0];
	private ItemClause[] itemClauses = new ItemClause[0];
	private ItemCoverage[] itemCoverages = new ItemCoverage[0];
	private ItemProfile[] itemProfiles = new ItemProfile[0];
	private ItemRiskType[] itemRiskTypes = new ItemRiskType[0];

	private ItemPersonalRisk itemPersonalRisk;
	private ItemPurchaseProtected itemPurchaseProtected;
	private ItemProperty itemProperty;
	private ItemAuto itemAuto;

	public Item() {
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public int getItemId() {
		return itemId;
	}

//	public Customer getCustomer() {
//		return this.customer;
//	}
//
//	public void setCustomer(Customer customer) {
//		this.customer = customer;
//	}

	public int getObjectId() {
		return this.objectId;
	}

	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}

	public int getCoveragePlanId() {
		return this.coveragePlanId;
	}

	public void setCoveragePlanId(int coveragePlanId) {
		this.coveragePlanId = coveragePlanId;
	}

	public int getRenewalType() {
		return this.renewalType;
	}

	public void setRenewalType(int renewalType) {
		this.renewalType = renewalType;
	}

	public Integer getRenewalInsurerId() {
		return this.renewalInsurerId;
	}

	public void setRenewalInsurerId(Integer renewalInsurerId) {
		this.renewalInsurerId = renewalInsurerId;
	}

	public String getRenewalPolicyNumber() {
		return this.renewalPolicyNumber;
	}

	public void setRenewalPolicyNumber(String renewalPolicyNumber) {
		this.renewalPolicyNumber = renewalPolicyNumber;
	}

	public Integer getBonusType() {
		return this.bonusType;
	}

	public void setBonusType(Integer bonusType) {
		this.bonusType = bonusType;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getCancelDate() {
		return this.cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	//	public Double getTariffFactor() {
	//		return this.tariffFactor;
	//	}
	//
	//	public void setTariffFactor(Double tariffFactor) {
	//		this.tariffFactor = tariffFactor;
	//	}
	//
	//	public Double getCommercialFactor() {
	//		return this.commercialFactor;
	//	}
	//
	//	public void setCommercialFactor(Double commercialFactor) {
	//		this.commercialFactor = commercialFactor;
	//	}
	//
	//	public Double getPurePremium() {
	//		return this.purePremium;
	//	}
	//
	//	public void setPurePremium(Double purePremium) {
	//		this.purePremium = purePremium;
	//	}
	//
	//	public Double getTariffPremium() {
	//		return this.tariffPremium;
	//	}
	//
	//	public void setTariffPremium(Double tariffPremium) {
	//		this.tariffPremium = tariffPremium;
	//	}
	//
	//	public Double getCommercialPremium() {
	//		return this.commercialPremium;
	//	}
	//
	//	public void setCommercialPremium(Double commercialPremium) {
	//		this.commercialPremium = commercialPremium;
	//	}
	//
	//	public Double getProfitPremium() {
	//		return this.profitPremium;
	//	}
	//
	//	public void setProfitPremium(Double profitPremium) {
	//		this.profitPremium = profitPremium;
	//	}
	//
	//	public void setTechnicalPremium(Double technicalPremium) {
	//		this.technicalPremium = technicalPremium;
	//	}
	//
	//	public Double getTechnicalPremium() {
	//		return technicalPremium;
	//	}
	//
	//	public Double getRetainedPremium() {
	//		return this.retainedPremium;
	//	}
	//
	//	public void setRetainedPremium(Double retainedPremium) {
	//		this.retainedPremium = retainedPremium;
	//	}
	//
	//	public Double getEarnedPremium() {
	//		return earnedPremium;
	//	}
	//
	//	public void setEarnedPremium(Double earnedPremium) {
	//		this.earnedPremium = earnedPremium;
	//	}
	//
	//	public Double getUnearnedPremium() {
	//		return unearnedPremium;
	//	}
	//
	//	public void setUnearnedPremium(Double unearnedPremium) {
	//		this.unearnedPremium = unearnedPremium;
	//	}

	public Double getNetPremium() {
		return this.netPremium;
	}

	public void setNetPremium(Double netPremium) {
		this.netPremium = netPremium;
	}

	public Double getCommission() {
		return this.commission;
	}

	public void setCommission(Double commission) {
		this.commission = commission;
	}

	public Double getCommissionFactor() {
		return this.commissionFactor;
	}

	public void setCommissionFactor(Double commissionFactor) {
		this.commissionFactor = commissionFactor;
	}

	//	public Double getLabour() {
	//		return this.labour;
	//	}
	//
	//	public void setLabour(Double labour) {
	//		this.labour = labour;
	//	}
	//
	//	public Double getLabourFactor() {
	//		return this.labourFactor;
	//	}
	//
	//	public void setLabourFactor(Double labourFactor) {
	//		this.labourFactor = labourFactor;
	//	}
	//
	//	public Double getAgency() {
	//		return this.agency;
	//	}
	//
	//	public void setAgency(Double agency) {
	//		this.agency = agency;
	//	}
	//
	//	public Double getAgencyFactor() {
	//		return this.agencyFactor;
	//	}
	//
	//	public void setAgencyFactor(Double agencyFactor) {
	//		this.agencyFactor = agencyFactor;
	//	}

	public Double getFractioningAdditional() {
		return this.fractioningAdditional;
	}

	public void setFractioningAdditional(Double fractioningAdditional) {
		this.fractioningAdditional = fractioningAdditional;
	}

	public Double getTaxValue() {
		return this.taxValue;
	}

	public void setTaxValue(Double taxValue) {
		this.taxValue = taxValue;
	}

	public Double getTotalPremium() {
		return this.totalPremium;
	}

	public void setTotalPremium(Double totalPremium) {
		this.totalPremium = totalPremium;
	}

	public Integer getAuthorizationLevel() {
		return this.authorizationLevel;
	}

	public void setAuthorizationLevel(Integer authorizationLevel) {
		this.authorizationLevel = authorizationLevel;
	}

	public boolean isCalculationValid() {
		return this.calculationValid;
	}

	public void setCalculationValid(boolean calculationValid) {
		this.calculationValid = calculationValid;
	}

	public int getItemStatus() {
		return this.itemStatus;
	}

	public void setItemStatus(int itemStatus) {
		this.itemStatus = itemStatus;
	}

	//	/**
	//	 * @return the chaimNumber
	//	 */
	//	public String getClaimNumber() {
	//		return claimNumber;
	//	}
	//
	//	/**
	//	 * @param chaimNumber the chaimNumber to set
	//	 */
	//	public void setClaimNumber(String chaimNumber) {
	//		this.claimNumber = chaimNumber;
	//	}
	//
	//	/**
	//	 * @return the claimDate
	//	 */
	//	public Date getClaimDate() {
	//		return claimDate;
	//	}
	//
	//	/**
	//	 * @param claimDate the claimDate to set
	//	 */
	//	public void setClaimDate(Date claimDate) {
	//		this.claimDate = claimDate;
	//	}
	//
	//	/**
	//	 * @return the claimNotification
	//	 */
	//	public Date getClaimNotification() {
	//		return claimNotification;
	//	}
	//
	//	/**
	//	 * @param claimNotification the claimNotification to set
	//	 */
	//	public void setClaimNotification(Date claimNotification) {
	//		this.claimNotification = claimNotification;
	//	}

	public Beneficiary[] getBeneficiaries() {
		return this.beneficiaries;
	}

	public void setBeneficiaries(Beneficiary[] beneficiaries) {
		this.beneficiaries = beneficiaries;
	}

	public ItemClause[] getItemClauses() {
		return this.itemClauses;
	}

	public void setItemClauses(ItemClause[] itemClauses) {
		this.itemClauses = itemClauses;
	}

	public ItemCoverage[] getItemCoverages() {
		return this.itemCoverages;
	}

	public void setItemCoverages(ItemCoverage[] itemCoverages) {
		this.itemCoverages = itemCoverages;
	}

	public ItemProfile[] getItemProfiles() {
		return this.itemProfiles;
	}

	public void setItemProfiles(ItemProfile[] itemProfiles) {
		this.itemProfiles = itemProfiles;
	}

	public ItemRiskType[] getItemRiskTypes() {
		return itemRiskTypes;
	}

	public void setItemRiskTypes(ItemRiskType[] itemRiskTypes) {
		this.itemRiskTypes = itemRiskTypes;
	}

	public ItemPersonalRisk getItemPersonalRisk() {
		return itemPersonalRisk;
	}

	public void setItemPersonalRisk(ItemPersonalRisk itemPersonalRisk) {
		this.itemPersonalRisk = itemPersonalRisk;
	}

	public ItemPurchaseProtected getItemPurchaseProtected() {
		return itemPurchaseProtected;
	}

	public void setItemPurchaseProtected(ItemPurchaseProtected itemPurchaseProtected) {
		this.itemPurchaseProtected = itemPurchaseProtected;
	}

	public ItemProperty getItemProperty() {
		return itemProperty;
	}

	public void setItemProperty(ItemProperty itemProperty) {
		this.itemProperty = itemProperty;
	}

	public ItemAuto getItemAuto() {
		return itemAuto;
	}

	public void setItemAuto(ItemAuto itemAuto) {
		this.itemAuto = itemAuto;
	}

	public void addClause(ItemClause itemClause){
		List<ItemClause> asList = new ArrayList<ItemClause>(Arrays.asList(this.itemClauses));
		asList.add(itemClause);
		this.itemClauses = asList.toArray(new ItemClause[asList.size()]);
	}

	public final ItemClause addClause(int clauseId) {
		ItemClause newClause = new ItemClause();
		newClause.setClauseId( clauseId );
		this.addClause( newClause );
		return newClause;
	}

	public void addCoverage(ItemCoverage itemCoverage){
		List<ItemCoverage> asList = new ArrayList<ItemCoverage>(Arrays.asList(this.itemCoverages));
		asList.add(itemCoverage);
		this.itemCoverages = asList.toArray(new ItemCoverage[asList.size()]);
	}

	public final ItemCoverage addCoverage(int coverageId){
		ItemCoverage newCoverage = new ItemCoverage();
		newCoverage.setCoverageId( coverageId );
		this.addCoverage( newCoverage );
		return newCoverage;
	}

	public void addProfile(ItemProfile itemProfile){
		List<ItemProfile> asList = new ArrayList<ItemProfile>(Arrays.asList(this.itemProfiles));
		asList.add(itemProfile);
		this.itemProfiles = asList.toArray(new ItemProfile[asList.size()]);
	}

	public final ItemProfile addProfile(int questionnaireId, int questionId, int responseId) {
		ItemProfile newProfile = new ItemProfile();
		newProfile.setQuestionnaireId(questionnaireId);
		newProfile.setQuestionId(questionId);
		newProfile.setResponseId(responseId);
		this.addProfile( newProfile );
		return newProfile;
	}

	public void addBeneficiary(Beneficiary beneficiary){
		List<Beneficiary> asList = new ArrayList<Beneficiary>(Arrays.asList(this.beneficiaries));
		asList.add(beneficiary);
		this.beneficiaries = asList.toArray(new Beneficiary[asList.size()]);
	}

	public final Beneficiary addBeneficiary(int beneficiaryId) {
		Beneficiary newBeneficiary = new Beneficiary();
		newBeneficiary.setBeneficiaryId( beneficiaryId );
		this.addBeneficiary( newBeneficiary );
		return newBeneficiary;
	}

	public void addRiskType(ItemRiskType itemRiskType){
		List<ItemRiskType> asList = new ArrayList<ItemRiskType>(Arrays.asList(this.itemRiskTypes));
		asList.add(itemRiskType);
		this.itemRiskTypes = asList.toArray(new ItemRiskType[asList.size()]);
	}

	public final ItemRiskType addRiskType(int riskType){
		ItemRiskType newItemRiskType = new ItemRiskType();
		newItemRiskType.setRiskType(riskType);
		this.addRiskType( newItemRiskType );
		return newItemRiskType;
	}
}
