package br.com.tratomais.core.soap.to;

import java.io.Serializable;
import java.util.Date;

public class ItemProperty implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7724854522539976156L;

	private int propertyType;
	private Integer activityType;
	private String addressStreet;
	private String districtName;
	private Integer cityId;
	private String cityName;
	private Integer regionCode;
	private String regionName;
	private Integer stateId;
	private String stateName;
	private String zipCode;
	private Integer countryId;
	private String countryName;
	private int propertyRiskType;
	private Double contentValue;
	private Double structureValue;
	private Double propertyValue;
	private Date acquisitionDate;
	private Date registrationDate;
	private String registrationName;
	private String registrationTome;
	private String registrationNumber;
	private String registrationProtocol;
	private String boundaryNorth;
	private String boundarySouth;
	private String boundaryEast;
	private String boundaryWest;
	private Boolean singleAddress;

	public ItemProperty() {
	}

	public int getPropertyType() {
		return this.propertyType;
	}

	public void setPropertyType(int propertyType) {
		this.propertyType = propertyType;
	}

	public Integer getActivityType() {
		return activityType;
	}

	public void setActivityType(Integer activityType) {
		this.activityType = activityType;
	}

	public String getAddressStreet() {
		return this.addressStreet;
	}

	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}

	public String getDistrictName() {
		return this.districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public Integer getCityId() {
		return this.cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Integer getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(Integer regionCode) {
		this.regionCode = regionCode;
	}

	public String getRegionName() {
		return this.regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public Integer getStateId() {
		return this.stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return this.stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public Integer getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public int getPropertyRiskType() {
		return this.propertyRiskType;
	}

	public void setPropertyRiskType(int propertyRiskType) {
		this.propertyRiskType = propertyRiskType;
	}

	public Double getContentValue() {
		return this.contentValue;
	}

	public void setContentValue(Double contentValue) {
		this.contentValue = contentValue;
	}

	public Double getStructureValue() {
		return this.structureValue;
	}

	public void setStructureValue(Double structureValue) {
		this.structureValue = structureValue;
	}

	public Double getPropertyValue() {
		return this.propertyValue;
	}

	public void setPropertyValue(Double propertyValue) {
		this.propertyValue = propertyValue;
	}

	public Date getAcquisitionDate() {
		return this.acquisitionDate;
	}

	public void setAcquisitionDate(Date acquisitionDate) {
		this.acquisitionDate = acquisitionDate;
	}

	public Date getRegistrationDate() {
		return this.registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getRegistrationName() {
		return this.registrationName;
	}

	public void setRegistrationName(String registrationName) {
		this.registrationName = registrationName;
	}

	public String getRegistrationTome() {
		return this.registrationTome;
	}

	public void setRegistrationTome(String registrationTome) {
		this.registrationTome = registrationTome;
	}

	public String getRegistrationNumber() {
		return this.registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getRegistrationProtocol() {
		return this.registrationProtocol;
	}

	public void setRegistrationProtocol(String registrationProtocol) {
		this.registrationProtocol = registrationProtocol;
	}

	public String getBoundaryNorth() {
		return this.boundaryNorth;
	}

	public void setBoundaryNorth(String boundaryNorth) {
		this.boundaryNorth = boundaryNorth;
	}

	public String getBoundarySouth() {
		return this.boundarySouth;
	}

	public void setBoundarySouth(String boundarySouth) {
		this.boundarySouth = boundarySouth;
	}

	public String getBoundaryEast() {
		return this.boundaryEast;
	}

	public void setBoundaryEast(String boundaryEast) {
		this.boundaryEast = boundaryEast;
	}

	public String getBoundaryWest() {
		return this.boundaryWest;
	}

	public void setBoundaryWest(String boundaryWest) {
		this.boundaryWest = boundaryWest;
	}

	public Boolean getSingleAddress() {
		return this.singleAddress;
	}

	public void setSingleAddress(Boolean singleAddress) {
		this.singleAddress = singleAddress;
	}	
}
