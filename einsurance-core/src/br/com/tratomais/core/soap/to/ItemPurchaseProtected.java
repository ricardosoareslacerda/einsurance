package br.com.tratomais.core.soap.to;

public class ItemPurchaseProtected extends ItemPersonalRisk {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1237074546827848429L;

	public static final int CARD_TYPE_CREDIT = 328;
	public static final int CARD_TYPE_DEBIT = 329;

	private int cardType;
	private int cardBrandType;
	private String cardNumber;
	private Integer cardExpirationDate;
	private String cardHolderName;

	public ItemPurchaseProtected() {
	}

	public int getCardType() {
		return this.cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

	public int getCardBrandType() {
		return this.cardBrandType;
	}

	public void setCardBrandType(int cardBrandType) {
		this.cardBrandType = cardBrandType;
	}

	public String getCardNumber() {
		return this.cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Integer getCardExpirationDate() {
		return this.cardExpirationDate;
	}

	public void setCardExpirationDate(Integer cardExpirationDate) {
		this.cardExpirationDate = cardExpirationDate;
	}

	public String getCardHolderName() {
		return this.cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}
}
