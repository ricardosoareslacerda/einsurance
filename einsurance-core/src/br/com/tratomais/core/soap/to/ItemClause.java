package br.com.tratomais.core.soap.to;

import java.io.Serializable;
import java.util.Date;

public class ItemClause implements Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8879883191109407212L;

	private int clauseId;
	private String clauseCode;
	private Double clauseValue;
	private String clauseDescription;
	private boolean clauseAutomatically;
	private Date effectiveDate;
	private Date expiryDate;

	public ItemClause() {
	}

	public int getClauseId() {
		return clauseId;
	}

	public void setClauseId(int clauseId) {
		this.clauseId = clauseId;
	}

	public String getClauseCode() {
		return this.clauseCode;
	}

	public void setClauseCode(String clauseCode) {
		this.clauseCode = clauseCode;
	}

	public Double getClauseValue() {
		return this.clauseValue;
	}

	public void setClauseValue(Double clauseValue) {
		this.clauseValue = clauseValue;
	}

	public String getClauseDescription() {
		return this.clauseDescription;
	}

	public void setClauseDescription(String clauseDescription) {
		this.clauseDescription = clauseDescription;
	}

	public boolean isClauseAutomatically() {
		return this.clauseAutomatically;
	}

	public void setClauseAutomatically(boolean clauseAutomatically) {
		this.clauseAutomatically = clauseAutomatically;
	}

	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return this.expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
}
