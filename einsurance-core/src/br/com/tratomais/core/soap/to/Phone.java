package br.com.tratomais.core.soap.to;

import java.io.Serializable;

public class Phone implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8835733885678275727L;

	public static final int PHONE_TYPE_CELLULAR = 77;
	public static final int PHONE_TYPE_FIXED = 78;
	public static final int PHONE_TYPE_FAX = 79;

	private int phoneId;
	private int phoneType;
	private String phoneNumber;
	private boolean active;

	public Phone() {
	}

	public Phone(int phoneId, int phoneType, String phoneNumber, boolean active) {
		this.phoneId = phoneId;
		this.phoneType = phoneType;
		this.phoneNumber = phoneNumber;
		this.active = active;
	}

	public int getPhoneId() {
		return this.phoneId;
	}

	public void setPhoneId(int phoneId) {
		this.phoneId = phoneId;
	}

	public int getPhoneType() {
		return this.phoneType;
	}

	public void setPhoneType(int phoneType) {
		this.phoneType = phoneType;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
