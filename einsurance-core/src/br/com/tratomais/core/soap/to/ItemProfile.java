package br.com.tratomais.core.soap.to;

import java.io.Serializable;

public class ItemProfile implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4103642192005569728L;

	public static final int QUESTIONNAIRE_TYPE_PROFILE = 196;
	public static final int QUESTIONNAIRE_TYPE_QUESTIONNAIRE = 197;

	private int questionnaireId;	
	private int questionId;
	private int responseId;
	private int questionnaireType;
	private String questionName;
	private String responseName;
	private String responseValue;
	private Double responsePoints;
	private Short displayOrder;

	public ItemProfile() {
	}

	public int getQuestionnaireId() {
		return this.questionnaireId;
	}

	public void setQuestionnaireId(int questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

	public int getQuestionId() {
		return this.questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public int getResponseId() {
		return this.responseId;
	}

	public void setResponseId(int responseId) {
		this.responseId = responseId;
	}

	public int getQuestionnaireType() {
		return this.questionnaireType;
	}

	public void setQuestionnaireType(int questionnaireType) {
		this.questionnaireType = questionnaireType;
	}

	public String getQuestionName() {
		return this.questionName;
	}

	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}

	public String getResponseName() {
		return this.responseName;
	}

	public void setResponseName(String responseName) {
		this.responseName = responseName;
	}

	public String getResponseValue() {
		return this.responseValue;
	}

	public void setResponseValue(String responseValue) {
		this.responseValue = responseValue;
	}

	public Double getResponsePoints() {
		return this.responsePoints;
	}

	public void setResponsePoints(Double responsePoints) {
		this.responsePoints = responsePoints;
	}

	public Short getDisplayOrder() {
		return this.displayOrder;
	}

	public void setDisplayOrder(Short displayOrder) {
		this.displayOrder = displayOrder;
	}
}
