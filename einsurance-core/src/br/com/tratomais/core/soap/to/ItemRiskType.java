package br.com.tratomais.core.soap.to;

import java.io.Serializable;

public class ItemRiskType implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int riskType;	
	private Double riskTypeValue;
	private String riskTypeName;

	public int getRiskType() {
		return riskType;
	}

	public void setRiskType(int riskType) {
		this.riskType = riskType;
	}

	public Double getRiskTypeValue() {
		return riskTypeValue;
	}

	public void setRiskTypeValue(Double riskTypeValue) {
		this.riskTypeValue = riskTypeValue;
	}

	public String getRiskTypeName() {
		return riskTypeName;
	}

	public void setRiskTypeName(String riskTypeName) {
		this.riskTypeName = riskTypeName;
	}
}
