package br.com.tratomais.core.soap;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;

import org.hibernate.Session;
import org.springframework.beans.BeanUtils;

import br.com.tratomais.core.exception.ServiceException;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.customer.Address;
import br.com.tratomais.core.model.customer.Customer;
import br.com.tratomais.core.model.customer.Phone;
import br.com.tratomais.core.model.policy.Beneficiary;
import br.com.tratomais.core.model.policy.Contract;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.policy.ItemAuto;
import br.com.tratomais.core.model.policy.ItemClause;
import br.com.tratomais.core.model.policy.ItemCoverage;
import br.com.tratomais.core.model.policy.ItemPersonalRisk;
import br.com.tratomais.core.model.policy.ItemPersonalRiskGroup;
import br.com.tratomais.core.model.policy.ItemProfile;
import br.com.tratomais.core.model.policy.ItemProperty;
import br.com.tratomais.core.model.policy.ItemPurchaseProtected;
import br.com.tratomais.core.model.policy.ItemRiskType;
import br.com.tratomais.core.model.policy.Motorist;
import br.com.tratomais.core.model.product.PaymentOption;
import br.com.tratomais.core.service.IServiceLogin;
import br.com.tratomais.core.service.IServicePolicy;
import br.com.tratomais.core.service.erros.Erro;
import br.com.tratomais.esb.collections.model.response.CollectionResponseItem;
import br.com.tratomais.esb.interfaces.model.request.ClaimRequest;

public class EInsuranceServicesImpl implements EInsuranceServices {

	public Session getSession() {
		return getSession();
	}

	private IServiceLogin serviceLogin;
	private IServicePolicy servicePolicy;

	public void setServiceLogin(IServiceLogin serviceLogin) {
		this.serviceLogin = serviceLogin;
	}

	public void setServicePolicy(IServicePolicy servicePolicy) {
		this.servicePolicy = servicePolicy;
	}

	// =======================================================================
	// M � T O D O S   E X P O S T O S   P E L O   S E R V I � O
	// -----------------------------------------------------------------------
	
	/** {@inheritDoc} */
	public void processInstallment(String login, String password, CollectionResponseItem[] collectionResponse){
		try {
			User user = this.logar(login, password);
			if (user != null) {
				servicePolicy.processInstallment(collectionResponse);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
	}

	public void receiveAdvice(String login, String password, CollectionResponseItem[] collectionResponse) throws ServiceException {
		try {
			User user = this.logar(login, password);
			if (user != null) {
				servicePolicy.receiveAdvice(collectionResponse);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to log into the system
	 * @param login login to be used
	 * @param password password to be used
	 * @return Logged user
	 * @throws ServiceException
	 */
	private User logar(String login, String password) throws ServiceException {
		User user = serviceLogin.validateLogin(login, password);
		if (user == null) {
			throw new ServiceException("El usu�rio o clave no est�n correctos");
		}
		return user; 
	}

	@Override
	public void policyCancellationNoPayment(String login, String password, int contractId, Date effectiveDate, Date cancelDate) throws ServiceException {
		try {
			User user = this.logar(login, password);
			if (user != null) {
				servicePolicy.policyCancellationNoPayment(contractId, effectiveDate, cancelDate);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void policyReactivation(String login, String password, int contractId) throws ServiceException {
		try {
			User user = this.logar(login, password);
			if (user != null) {
				servicePolicy.policyReactivation(contractId);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void policyClaim(String login, String password, ClaimRequest[] claimRequests) throws ServiceException {
		try {
			User user = this.logar(login, password);
			if (user != null) {
				servicePolicy.policyClaim(claimRequests);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}				
	}

	@Override
	public void policyCancellation(String login, String password, int contractId, int issuanceType, Date cancelDate, int cancelReason) throws ServiceException {
		try {
			User user = this.logar(login, password);
			if (user != null) {
				servicePolicy.policyCancellation(contractId, issuanceType, cancelDate, cancelReason);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void renewal(String login, String password, int contractId, int endorsementId) throws ServiceException {
		try {
			User user = this.logar(login, password);
			if (user != null) {
				servicePolicy.policyRenewal(contractId, endorsementId);
			}
			this.serviceLogin.logout();
		} catch (ServiceException e) {
			e.printStackTrace();
		}		
	}

	@Override
	public br.com.tratomais.core.soap.to.Endorsement policyRenewal(String login, String password, br.com.tratomais.core.soap.to.Endorsement endorsement, br.com.tratomais.core.soap.to.PaymentOption paymentOption) throws ServiceException {
		try {
			User user = this.logar(login, password);
			if (user != null) {
				return this.parseEndorsement(servicePolicy.policyRenewal(this.parseEndorsement(endorsement), this.parsePaymentOption(paymentOption)));
			}
			this.serviceLogin.logout();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return endorsement;
	}
	
	@Override
	public br.com.tratomais.core.soap.to.Endorsement policyIssuance(String login, String password, br.com.tratomais.core.soap.to.Endorsement endorsement, br.com.tratomais.core.soap.to.PaymentOption paymentOption) throws ServiceException {
		try {
			User user = this.logar(login, password);
			if (user != null) {
				return this.parseEndorsement(servicePolicy.policyIssuance(this.parseEndorsement(endorsement), this.parsePaymentOption(paymentOption)));
			}
			this.serviceLogin.logout();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return endorsement;
	}

	private Endorsement parseEndorsement(br.com.tratomais.core.soap.to.Endorsement inputEndorsement) {
		// set Contract
		Contract outputContract = new Contract();
		Endorsement outputEndorsement = outputContract.addEndorsement((inputEndorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_EMISSION ? 1 : inputEndorsement.getEndorsementId()));

		// parse Contract
		BeanUtils.copyProperties(inputEndorsement.getContract(), outputContract, new String[]{"contractId", "parentId", "renewalId", "cancelDate", "contractStatus"});

		// parse Endorsement
		BeanUtils.copyProperties(inputEndorsement, outputEndorsement, new String[]{"endorsementId", "contract", "customerByInsuredId", "customerByPolicyHolderId", "items", "errorList",
				"issuanceDate", "refusalDate", "cancelDate", "quotationNumber", "quotationDate", "proposalNumber", "proposalDate", "endorsementNumber", "endorsementDate",
				"netPremium", "commission", "policyCost", "inspectionCost", "fractioningAdditional", "taxRate", "taxValue", "totalPremium", "issuingStatus", "cancelReason",
				"doNotUseItemAuto", "doNotUseItemPersonalRisk", "doNotUseItemProperty", "doNotUseItemPurchaseProtected"});

		// parse IssuanceType
		if (inputEndorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_INSURED ||
				inputEndorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR) {
			outputEndorsement.setCancelDate(inputEndorsement.getCancelDate());
		}
		
		// parse Customer by Insured
		if (inputEndorsement.getCustomerByInsuredId() != null) {
			outputEndorsement.setCustomerByInsuredId(this.parseCustomer(inputEndorsement.getCustomerByInsuredId()));
		}

		// parse Customer by PolicyHolder
		if (inputEndorsement.getCustomerByPolicyHolderId() != null) {
			outputEndorsement.setCustomerByPolicyHolderId(this.parseCustomer(inputEndorsement.getCustomerByPolicyHolderId()));
		} else {
			outputEndorsement.setCustomerByPolicyHolderId(outputEndorsement.getCustomerByInsuredId());
		}

		// parse Item
		for (br.com.tratomais.core.soap.to.Item inputItem : inputEndorsement.getItems()) {
			Item outputItem = outputEndorsement.addItem(inputItem.getObjectId());
			this.parseItem(inputItem, outputItem);
		}
		
		return outputEndorsement;
	}

	private Customer parseCustomer(br.com.tratomais.core.soap.to.Customer inputCustomer) {
		Customer outputCustomer = new Customer();

		// parse Customer
		BeanUtils.copyProperties(inputCustomer, outputCustomer, new String[]{"customerId", "contacts", "addresses",
				"addressId", "phoneId", "active"});
		outputCustomer.setRegisterDate(new Date());
		outputCustomer.setActive(true);

		// parse Address
		for (br.com.tratomais.core.soap.to.Address inputAddress : inputCustomer.getAddresses()) {
			Address outputAddress = new Address();
			if (outputCustomer.getAddresses() == null) {
				outputCustomer.setAddresses(new HashSet<Address>());
			}
			outputAddress.setActive(true);
			BeanUtils.copyProperties(inputAddress, outputAddress, new String[]{"addressId", "phones", "active"});
			outputCustomer.getAddresses().add(outputAddress);

			// parse Phone
			for (br.com.tratomais.core.soap.to.Phone inputPhone : inputAddress.getPhones()) {
				Phone outputPhone = new Phone();
				if (outputAddress.getPhones() == null) {
					outputAddress.setPhones(new HashSet<Phone>());
				}
				outputPhone.setActive(true);
				BeanUtils.copyProperties(inputPhone, outputPhone, new String[]{"phoneId", "active"});
				outputAddress.getPhones().add(outputPhone);
			}
		}
		return outputCustomer;
	}

	private void parseItem(br.com.tratomais.core.soap.to.Item inputItem, Item outputItem) {
		String[] ignoreProperties = new String[]{"itemId", "itemClauses", "itemCoverages", "itemProfiles", "beneficiaries", "itemRiskTypes",
				"cancelDate", "netPremium", "commission", "fractioningAdditional", "taxValue", "totalPremium", "calculationValid", "itemStatus"};

		// parse Item
		switch ( inputItem.getObjectId() ) {
			case br.com.tratomais.core.soap.to.Item.OBJECT_LIFE:
			case br.com.tratomais.core.soap.to.Item.OBJECT_CREDIT:
			case br.com.tratomais.core.soap.to.Item.OBJECT_LIFE_CYCLE:
			case br.com.tratomais.core.soap.to.Item.OBJECT_PERSONAL_ACCIDENTS:
				BeanUtils.copyProperties(inputItem, (Item)outputItem, ignoreProperties);
				BeanUtils.copyProperties(inputItem.getItemPersonalRisk(), (ItemPersonalRisk)outputItem, new String[]{"itemPersonalRiskGroups"});

				// parse RiskGroup
				for (br.com.tratomais.core.soap.to.ItemPersonalRiskGroup inputRiskGroup : inputItem.getItemPersonalRisk().getItemPersonalRiskGroups()) {
					ItemPersonalRiskGroup outputRiskGroup = ((ItemPersonalRisk)outputItem).addItemPersonalRiskGroup();
					BeanUtils.copyProperties(inputRiskGroup, outputRiskGroup, new String[]{"riskGroupId"});
				}
				break;
			case br.com.tratomais.core.soap.to.Item.OBJECT_PURCHASE_PROTECTED:
				BeanUtils.copyProperties(inputItem, (Item)outputItem, ignoreProperties);
				BeanUtils.copyProperties(inputItem.getItemPurchaseProtected(), (ItemPurchaseProtected)outputItem, new String[]{"itemPersonalRiskGroups"});
				break;
			case br.com.tratomais.core.soap.to.Item.OBJECT_HABITAT:
			case br.com.tratomais.core.soap.to.Item.OBJECT_CAPITAL:
				BeanUtils.copyProperties(inputItem, (Item)outputItem, ignoreProperties);
				BeanUtils.copyProperties(inputItem.getItemProperty(), (ItemProperty)outputItem);
				break;
			case br.com.tratomais.core.soap.to.Item.OBJECT_AUTO:
				BeanUtils.copyProperties(inputItem, (Item)outputItem, ignoreProperties);
				BeanUtils.copyProperties(inputItem.getItemAuto(), (ItemAuto)outputItem, new String[]{"motorist"});

				// parse Motorist
				for (br.com.tratomais.core.soap.to.Motorist inputMotorist : inputItem.getItemAuto().getMotorists()) {
					Motorist outputMotorist = ((ItemAuto)outputItem).addMotorist();
					BeanUtils.copyProperties(inputMotorist, outputMotorist, new String[]{"motoristId"});
				}
				break;
			default:
				throw new IllegalArgumentException();
		}

		// parse Clause
		for (br.com.tratomais.core.soap.to.ItemClause inputItemClause : inputItem.getItemClauses()) {
			ItemClause outputItemClause = outputItem.addClause(inputItemClause.getClauseId());
			BeanUtils.copyProperties(inputItemClause, outputItemClause);
		}

		// parse Coverage
		for (br.com.tratomais.core.soap.to.ItemCoverage inputItemCoverage : inputItem.getItemCoverages()) {
			ItemCoverage outputItemCoverage = outputItem.addCoverage(inputItemCoverage.getCoverageId());
			BeanUtils.copyProperties(inputItemCoverage, outputItemCoverage, new String[]{
					"netPremium", "fractioningAdditional", "taxIncidence", "taxValue", "totalPremium",
					"deductibleValueMinimum", "deductibleValueMaximum", "deductibleDescription", "commission", "commissionFactorOriginal", "bonus", "bonusFactor"});
		}

		// parse Profile
		for (br.com.tratomais.core.soap.to.ItemProfile inputItemProfile : inputItem.getItemProfiles()) {
			ItemProfile outputItemProfile = outputItem.addProfile(inputItemProfile.getQuestionnaireId(), inputItemProfile.getQuestionId(), inputItemProfile.getResponseId());
			BeanUtils.copyProperties(inputItemProfile, outputItemProfile, new String[]{"responsePoints"});
		}

		// parse Beneficiary
		for (br.com.tratomais.core.soap.to.Beneficiary inputBeneficiary : inputItem.getBeneficiaries()) {
			Beneficiary outputBeneficiary = outputItem.addBeneficiary();
			BeanUtils.copyProperties(inputBeneficiary, outputBeneficiary, new String[]{"beneficiaryId"});
		}

		// parse RiskType
		for (br.com.tratomais.core.soap.to.ItemRiskType inputItemRiskType : inputItem.getItemRiskTypes()) {
			ItemRiskType outputItemRiskType = outputItem.addRiskType(inputItemRiskType.getRiskType());
			BeanUtils.copyProperties(inputItemRiskType, outputItemRiskType);
		}
	}

	private PaymentOption parsePaymentOption(br.com.tratomais.core.soap.to.PaymentOption inputPaymentOption) {
		if (inputPaymentOption == null)
				return null;
		PaymentOption outputPaymentOption = new PaymentOption();
		BeanUtils.copyProperties(inputPaymentOption, outputPaymentOption);
		return outputPaymentOption;
	}
	
	private br.com.tratomais.core.soap.to.Endorsement parseEndorsement(Endorsement inputEndorsement) {
		br.com.tratomais.core.soap.to.Contract outputContract = new br.com.tratomais.core.soap.to.Contract();
		br.com.tratomais.core.soap.to.Endorsement outputEndorsement = new br.com.tratomais.core.soap.to.Endorsement();

		// set Contract
		outputEndorsement.setContract(outputContract);

		// parse Contract
		BeanUtils.copyProperties(inputEndorsement.getContract(), outputContract, new String[]{"endorsements", "itemSequences"});

		// parse Endorsement
		BeanUtils.copyProperties(inputEndorsement, outputEndorsement, new String[]{"id", "contract", "customerByInsuredId", "customerByPolicyHolderId",
				"items", "errorList", "renewalAlerts"});

		// parse Customer by Insured
		outputEndorsement.setCustomerByInsuredId(this.parseCustomer(inputEndorsement.getCustomerByInsuredId()));

		// parse Customer by PolicyHolder
		if (inputEndorsement.getCustomerByInsuredId() != inputEndorsement.getCustomerByPolicyHolderId()) {
			outputEndorsement.setCustomerByPolicyHolderId(this.parseCustomer(inputEndorsement.getCustomerByPolicyHolderId()));
		} else {
			outputEndorsement.setCustomerByPolicyHolderId(outputEndorsement.getCustomerByInsuredId());
		}
		
		// parse Item
		for (Item inputItem : inputEndorsement.getItems()) {
			br.com.tratomais.core.soap.to.Item outputItem = outputEndorsement.addItem(inputItem.getObjectId(), inputItem.getId().getItemId());
			this.parseItem(inputItem, outputItem);
		}
		
		// parse Erro
		br.com.tratomais.core.soap.to.ErrorList outputErrorList = new br.com.tratomais.core.soap.to.ErrorList();
		LinkedList<br.com.tratomais.core.soap.to.Erro> outputErros = new LinkedList<br.com.tratomais.core.soap.to.Erro>();	
		for (Erro inputErro : inputEndorsement.getErrorList().getListaErros()) {
			br.com.tratomais.core.soap.to.Erro outputErro = new br.com.tratomais.core.soap.to.Erro();
			BeanUtils.copyProperties(inputErro, outputErro, new String[]{"local"});
			outputErros.add(outputErro);
		}
		outputErrorList.setLista(outputErros.toArray(new br.com.tratomais.core.soap.to.Erro[outputErros.size()]));
		outputEndorsement.setErrorList(outputErrorList);

		return outputEndorsement;
	}
	
	private br.com.tratomais.core.soap.to.Customer parseCustomer(Customer inputCustomer) {
		br.com.tratomais.core.soap.to.Customer outputCustomer = new br.com.tratomais.core.soap.to.Customer();

		// parse Customer
		BeanUtils.copyProperties(inputCustomer, outputCustomer, new String[]{"contacts", "addresses"});

		// parse Address
		LinkedList<br.com.tratomais.core.soap.to.Address> outputAddresses = new LinkedList<br.com.tratomais.core.soap.to.Address>();
		for (Address inputAddress : inputCustomer.getAddresses()) {
			br.com.tratomais.core.soap.to.Address outputAddress = new br.com.tratomais.core.soap.to.Address();
			BeanUtils.copyProperties(inputAddress, outputAddress, new String[]{"phones"});
			outputAddresses.add(outputAddress);

			// parse Phone
			LinkedList<br.com.tratomais.core.soap.to.Phone> outputPhones = new LinkedList<br.com.tratomais.core.soap.to.Phone>();
			for (Phone inputPhone : inputAddress.getPhones()) {
				br.com.tratomais.core.soap.to.Phone outputPhone = new br.com.tratomais.core.soap.to.Phone();
				BeanUtils.copyProperties(inputPhone, outputPhone);
				outputPhones.add(outputPhone);
			}
			outputAddress.setPhones(outputPhones.toArray(new br.com.tratomais.core.soap.to.Phone[outputPhones.size()]));
		}
		outputCustomer.setAddresses(outputAddresses.toArray(new br.com.tratomais.core.soap.to.Address[outputAddresses.size()]));

		return outputCustomer;
	}

	private void parseItem(Item inputItem, br.com.tratomais.core.soap.to.Item outputItem) {
		String[] ignoreProperties = new String[]{"id", "customer", "endorsement",
				"itemClauses", "itemCoverages", "itemProfiles", "beneficiaries", "itemRiskTypes",
				"itemPersonalRiskGroups", "motorist",
				"itemPersonalRisk", "itemPurchaseProtected", "itemProperty", "itemAuto"};

		// parse Item
		if (inputItem instanceof ItemPurchaseProtected) {
			BeanUtils.copyProperties((Item)inputItem, outputItem, ignoreProperties);
			BeanUtils.copyProperties((ItemPurchaseProtected)inputItem, outputItem.getItemPurchaseProtected(), ignoreProperties);		
		}
		else if (inputItem instanceof ItemPersonalRisk) {
			BeanUtils.copyProperties((Item)inputItem, outputItem, ignoreProperties);
			BeanUtils.copyProperties((ItemPersonalRisk)inputItem, outputItem.getItemPersonalRisk(), ignoreProperties);

			// parse RiskGroup
			for (ItemPersonalRiskGroup inputRiskGroup : ((ItemPersonalRisk)inputItem).getItemPersonalRiskGroups()) {
				br.com.tratomais.core.soap.to.ItemPersonalRiskGroup outputRiskGroup = outputItem.getItemPersonalRisk().addItemPersonalRiskGroup(inputRiskGroup.getId().getRiskGroupId());
				BeanUtils.copyProperties(inputRiskGroup, outputRiskGroup, new String[]{"id", "itemPersonalRisk"});
			}
		}
		else if (inputItem instanceof ItemProperty) {
			BeanUtils.copyProperties((Item)inputItem, outputItem, ignoreProperties);
			BeanUtils.copyProperties((ItemProperty)inputItem, outputItem.getItemProperty(), ignoreProperties);
		}
		else if (inputItem instanceof ItemAuto) {
			BeanUtils.copyProperties((Item)inputItem, outputItem, ignoreProperties);
			BeanUtils.copyProperties((ItemAuto)inputItem, outputItem.getItemAuto(), ignoreProperties);

			// parse Motorist
			for (Motorist inputMotorist : ((ItemAuto)inputItem).getMotorists()) {
				br.com.tratomais.core.soap.to.Motorist outputMotorist = outputItem.getItemAuto().addMotorist(inputMotorist.getId().getMotoristId());
				BeanUtils.copyProperties(inputMotorist, outputMotorist, new String[]{"id", "itemAuto"});
			}
		}

		// parse Clause
		for (ItemClause inputItemClause : inputItem.getItemClauses()) {
			br.com.tratomais.core.soap.to.ItemClause outputItemClause = outputItem.addClause(inputItemClause.getId().getClauseId());
			BeanUtils.copyProperties(inputItemClause, outputItemClause, new String[]{"id", "item"});
		}

		// parse Coverage
		for (ItemCoverage inputItemCoverage : inputItem.getItemCoverages()) {
			br.com.tratomais.core.soap.to.ItemCoverage outputItemCoverage = outputItem.addCoverage(inputItemCoverage.getId().getCoverageId());
			BeanUtils.copyProperties(inputItemCoverage, outputItemCoverage, new String[]{"id", "item"});
		}

		// parse Profile
		for (ItemProfile inputItemProfile : inputItem.getItemProfiles()) {
			br.com.tratomais.core.soap.to.ItemProfile outputItemProfile = outputItem.addProfile(inputItemProfile.getId().getQuestionnaireId(), inputItemProfile.getId().getQuestionId(), inputItemProfile.getId().getResponseId());
			BeanUtils.copyProperties(inputItemProfile, outputItemProfile, new String[]{"id", "item"});
		}

		// parse Beneficiary
		for (Beneficiary inputBeneficiary : inputItem.getBeneficiaries()) {
			br.com.tratomais.core.soap.to.Beneficiary outputBeneficiary = outputItem.addBeneficiary(inputBeneficiary.getId().getBeneficiaryId());
			BeanUtils.copyProperties(inputBeneficiary, outputBeneficiary, new String[]{"id", "item"});
		}

		// parse RiskType
		for (ItemRiskType inputItemRiskType : inputItem.getItemRiskTypes()) {
			br.com.tratomais.core.soap.to.ItemRiskType outputItemRiskType = outputItem.addRiskType(inputItemRiskType.getId().getRiskType());
			BeanUtils.copyProperties(inputItemRiskType, outputItemRiskType, new String[]{"id", "item"});
		}
	}
}