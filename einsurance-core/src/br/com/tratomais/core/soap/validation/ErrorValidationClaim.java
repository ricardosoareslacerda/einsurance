package br.com.tratomais.core.soap.validation;

public enum ErrorValidationClaim {
	ITEM_ERROR(1, "Item Afectado no encontrado"),
	CLAIM_STATUS_ERROR(2, "Estatus no encontrado"),
	CLAIM_ACTION_TYPE_ERROR(3, "Tipo de acci�n del siniestro no encontrado"),
	CLAIM_CAUSE_ERROR(4, "Causa no encontrado"),
	CLAIM_EFFECT_ERROR(5, "Efecto no encontrado"),
	COVERAGE_ERROR(6, "Cobertura no encontrado"),
	ANNULMENT_CLAIM_ERROR(7, "P�liza no localizada por siniestro"),
	POLICY_ANNULLMENT_CLAIM(8, "P�liza anulada por siniestro");

	private final int error;
	private final String descricao;

	ErrorValidationClaim(int error, String descricao) {
		this.error = error;
		this.descricao = descricao;
	}

	public int getError() {
		return error;
	}

	public String toString() {
		return descricao;
	}
}
