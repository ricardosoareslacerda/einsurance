package br.com.tratomais.core.soap.validation;

import java.math.BigInteger;
import java.util.Date;

import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.model.claim.ClaimCause;
import br.com.tratomais.core.model.claim.ClaimEffect;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.policy.Contract;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.policy.ItemPersonalRiskGroup;
import br.com.tratomais.core.model.product.Coverage;
import br.com.tratomais.core.model.product.DataOption;
import br.com.tratomais.core.service.IServicePolicy;
import br.com.tratomais.esb.interfaces.model.request.ClaimRequest;
import br.com.tratomais.esb.interfaces.model.response.ClaimResponse;

public class ValidationInterfaces {
	
	private IServicePolicy servicePolicy;
	private IDaoPolicy daoPolicy;
	private ErrorValidationClaim lastError;
	
	/**
	 * Represents the service of Activity version.
	 */
	public void setServicePolicy(IServicePolicy servicePolicy) {
		this.servicePolicy = servicePolicy;

	}

	public void setDaoPolicy(IDaoPolicy daoPolicy) {
		this.daoPolicy = daoPolicy;
	}
	
	/**
	 * return last error from validate
	 * @return {@link String}
	 */
	public ErrorValidationClaim getLastError(){
		return lastError;
	}
	
	/**
	 * validate field from envelope from the ESB interfaces
	 * @param {@link ClaimResponse}
	 * @return {@link Boolean}
	 */
	public Boolean validateFieldEnvelope(ClaimResponse claim){
		return validateItem(claim.getPolicyNumber(), claim.getCertificateNumber(), claim.getItemId(), claim.getRiskGroupId(), claim.getClaimDate())
			&& validateClaimStatus(claim.getClaimStatus())
			&& validateClaimCause(claim.getClaimCauseID())
			&& validateClaimEffect(claim.getClaimEffectID())
			&& validateCoverage(claim.getCoverageId());
	}

	/**
	 * validate the endorsement to the Claim request
	 * 
	 * @param {@link ClaimResponse}
	 * @return{@link Boolean}
	 */
	public Boolean validateEndorsementToClaimRequest(ClaimResponse claimResponse){
		boolean validate = false;
		
		Contract contract = daoPolicy.getContractNumber(claimResponse.getPolicyNumber(), claimResponse.getCertificateNumber(), claimResponse.getClaimDate());
		if (contract != null) {
			Endorsement endorsement = servicePolicy.lastValidEndosementForContractNumber(contract.getContractId());	
			if (endorsement != null) {
				if (endorsement.getIssuanceType() != Endorsement.ISSUANCE_TYPE_POLICY_CLAIM)
					validate = true;
				
				Channel channel = daoPolicy.getChannel(endorsement.getChannelId());
				if (channel != null) claimResponse.setChannelCode(channel.getExternalCode().trim());
			}
		}
		
		if (!validate) lastError = ErrorValidationClaim.ANNULMENT_CLAIM_ERROR;
		
		return validate;
	}
	
	/**
	 * validate itemId, policyNumber, certificateNumber, effectiveDate
	 * @param ({@link ClaimRequest}) - ({@link Integer}) itemId
	 * @param ({@link ClaimRequest}) - ({@link BigInteger}) policyNumber
	 * @param ({@link ClaimRequest}) - ({@link Long}) certificateNumber
	 * @param ({@link ClaimRequest}) - ({@link Date}) effectiveDate
	 * @return {@link Boolean}
	 */
	public Boolean validateItem(BigInteger policyNumber, Long certificateNumber, Integer itemId, Integer riskGroupId, Date effectiveDate){
		boolean validate = false;
		
		Contract contract = daoPolicy.getContractNumber(policyNumber, certificateNumber, effectiveDate);
		if (contract != null) {
			Endorsement endorsement = servicePolicy.lastValidEndosementForContractNumber(contract.getContractId(), effectiveDate);		
			if (endorsement != null) {
				Item item = daoPolicy.findItem(contract.getContractId(), endorsement.getId().getEndorsementId(), itemId);
				if (item != null){
					if (riskGroupId != null && riskGroupId != 0){
						ItemPersonalRiskGroup itemPersonalRiskGroup = daoPolicy.findItemPersonalRiskGroup(contract.getContractId(), endorsement.getId().getEndorsementId(), item.getId().getItemId(), riskGroupId);
						if (itemPersonalRiskGroup != null) validate = true;
					}
					else validate = true;
				}
			}
		}
		
		if (!validate) lastError = ErrorValidationClaim.ITEM_ERROR;
		
		return validate;
	}
	
	/**
	 * validate ClaimStatus
	 * @param ({@link ClaimRequest}) - ({@link Integer}) claimStatus
	 * @return {@link Boolean}
	 */
	public Boolean validateClaimStatus(Integer claimStatus){
		boolean validate = false;
		
		DataOption dataOption = daoPolicy.getDataOption(claimStatus);
		
		if (dataOption != null) {
			validate = true;
		} else {
			lastError = ErrorValidationClaim.CLAIM_STATUS_ERROR;
		}
		
		return validate;
	}
	
	/**
	 * validate ClaimActionType
	 * @param ({@link ClaimRequest}) - (int) claimActionType
	 * @return {@link Boolean}
	 */
	public Boolean validateClaimActionType(int claimActionType){
		Boolean validate = false;
		
		DataOption dataOption = daoPolicy.getDataOption(claimActionType);
		
		if (dataOption != null) {
			validate = true;
		} else {
			lastError = ErrorValidationClaim.CLAIM_ACTION_TYPE_ERROR;
		}
		
		return validate;		
	}
	
	/**
	 * validate ClaimCauseId
	 * @param ({@link ClaimRequest}) - ({@link Integer}) claimCauseID
	 * @return {@link Boolean}
	 */
	public Boolean validateClaimCause(Integer claimCauseID){
		boolean validate = false;
		
		ClaimCause claimCause = daoPolicy.getClaimCause(claimCauseID);
		
		if (claimCause != null) {
			validate = true;
		} else {
			lastError = ErrorValidationClaim.CLAIM_CAUSE_ERROR;
		}
		
		return validate;		
	}

	/**
	 * validate ClaimEffectId
	 * @param ({@link ClaimRequest}) - ({@link Integer}) claimEffectID
	 * @return {@link Boolean}
	 */
	public Boolean validateClaimEffect(Integer claimEffectID){
		boolean validate = false;
		
		ClaimEffect claimEffect = daoPolicy.getClaimEffect(claimEffectID);
		
		if (claimEffect != null) {
			validate = true;
		} else {
			lastError = ErrorValidationClaim.CLAIM_EFFECT_ERROR;
		}
		
		return validate;		
	}
	
	/**
	 * validate coverageId
	 * @param ({@link ClaimRequest}) - ({@link Integer}) coverageId
	 * @return {@link Boolean}
	 */
	public Boolean validateCoverage(Integer coverageId){
		boolean validate = false;
		
		Coverage coverage = daoPolicy.getCoverage(coverageId);
		
		if (coverage != null) {
			validate = true;
		} else {
			lastError = ErrorValidationClaim.COVERAGE_ERROR;
		}
		
		return validate;		
	}
}