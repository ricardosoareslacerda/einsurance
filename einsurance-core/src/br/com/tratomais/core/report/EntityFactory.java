package br.com.tratomais.core.report;

import org.apache.log4j.Logger;

import br.com.tratomais.core.model.report.Report;

public class EntityFactory {
	private static final Logger logger = Logger.getLogger( EntityFactory.class );

	public static final int REPORT = 1;
	
	public static Object createEntity(int en) {
		Object obj = null;
		switch(en) {
			case REPORT: obj = new Report();
		}
		return obj;
	}
	
	/*
	 * Converte ReportTO em Report
	 */
	public static Report convToReport(ReportTO to) {
		Report r = (Report)new Report();
		r.setDataType(to.getDataType());
		r.setExecutionHour(to.getExecutionHour());
		r.setExportReportFormat(to.getExportReportFormat());
		r.setFinalDate(to.getFinalDate());
		r.setGoal(to.getGoal());
		r.setId(Long.valueOf(to.getId()));
		r.setLastExecution(to.getLastExecution());
		r.setLocale(to.getLocale());
		r.setName(to.getName());
		r.setProfile(to.getProfile());
		r.setReportFileName(to.getReportFileName());
		r.setReportPerspectiveId(to.getReportPerspectiveId());
		r.setReportPerspectiveId(to.getReportPerspectiveId());
		r.setSqlStement(to.getSqlStement());
		r.setTolerance(to.getTolerance());
		r.setToleranceType(to.getToleranceType());
		r.setType(to.getType());
		return r;
	}
}
