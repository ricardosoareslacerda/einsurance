package br.com.tratomais.core.report;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Locale;

import br.com.tratomais.core.exception.BusinessException;
import br.com.tratomais.core.util.DateUtil;
import br.com.tratomais.core.util.StringUtil;

public class ReportFieldTO extends TransferObjectReport {
	private static final long serialVersionUID = 1L;

    /** Type of report field */
    public static final Integer TYPE_STRING       = new Integer(1);
    
    /** Type of report field */
    public static final Integer TYPE_TIMESTAMP    = new Integer(2);
    
    /** Type of report field */
    public static final Integer TYPE_INTEGER      = new Integer(3);
    
    /** Type of report field */
    public static final Integer TYPE_FLOAT        = new Integer(4);

    /** Type of report field */
    public static final Integer TYPE_OBJECT       = new Integer(5);

    /** Type of report field */
    public static final Integer TYPE_BOOLEAN      = new Integer(6);
    
    
    /** The type of a inner report field of a customized SQL */
    private Integer reportFieldType;
    
    /** The label of the report field */
    private String label;

    /** The value of report field set by user at report viewer */
    private String value;

    /** The project of the report related to current report field */
    //private ProjectTO project;
    
    /** If true, the report field should be displayed on GUI */
    private boolean isVisible = true;
    
    
    public void setReportFieldType(String content) {
        try {
            if (content!=null) {
                Integer type = new Integer(content);
                if (type.equals(TYPE_STRING) || type.equals(TYPE_TIMESTAMP) || type.equals(TYPE_INTEGER) ||
                        type.equals(TYPE_FLOAT) || type.equals(TYPE_OBJECT) || type.equals(TYPE_BOOLEAN)  ) {
                    this.reportFieldType = type;  
                    
                } else {
                    this.reportFieldType = null;
                }
            }
        } catch(Exception e) {
            this.reportFieldType = null;   
        }
    }
    
    
	/**
	 * Format a field name to be used such a html form id 
	 * (replace spaces to underline, create a standard name, etc)
	 */
	public String getFieldToHtml(ReportTO rto){
	    String response = "";
	    response = this.getId().replaceAll(" ", "_");
	    response = response + "_" + rto.getId();
	    return response;
	}    
    
	/**
	 * This method set a current Report Field value into the preparedStatement object
	 * with the appropriate type. 
	 * @throws BusinessException
	 */
	public boolean setValueIntoPreparedStatement(int index, PreparedStatement pstmt, Locale loc) throws SQLException, BusinessException{
	    boolean response = true;
	    
	    Integer type = this.getReportFieldType();
	    if (type!=null) {
	        if (loc!=null) {
		        if (type.equals(TYPE_STRING)) {
		        	if( !this.value.equalsIgnoreCase("null")){
		        		pstmt.setString(index, this.value);
		        	}else{
		        		pstmt.setNull(index, Types.NULL ); 
		        	}
		            
		        } else if (type.equals(TYPE_TIMESTAMP)) {
		        	if( !this.value.equalsIgnoreCase("null")){
		        		pstmt.setTimestamp(index, DateUtil.getDate(this.value, loc));
		        	}else{
		        		pstmt.setNull(index, Types.NULL ); 
		        	}
		            
		        } else if (type.equals(TYPE_INTEGER)) {
		        	if( !this.value.equalsIgnoreCase("null")){
		        		pstmt.setInt(index, Integer.parseInt(this.value));
		        	}else{
		        		pstmt.setNull(index, Types.NULL ); 
		        	}
		            
		        } else if (type.equals(TYPE_FLOAT)) {
		        	if( !this.value.equalsIgnoreCase("null")){
		        		 pstmt.setFloat(index, StringUtil.getStringToFloat(this.value, loc));
		        	}else{
		        		pstmt.setNull(index, Types.NULL ); 
		        	}
		        	
		        } else if (type.equals(TYPE_BOOLEAN)) {
		        	if( !this.value.equalsIgnoreCase("null")){
			        	String val = this.value.trim();
			            pstmt.setBoolean(index, (val.equals("1") || val.equalsIgnoreCase("true")));
		        	}else{
		        		pstmt.setNull(index, Types.NULL ); 
		        	}
		            
		        } else {
		            response = false;
		        }	            
	        } else {
	            response = false;	            
	            throw new BusinessException("The locale used to set the value of report field into preparedStatement cannot be null.");
	        }
	    } else {
            response = false;	        
            throw new BusinessException("The type of report field cannot be null.");	        
	    }
	    
	    return response;
	}

	
    /////////////////////////////////////////////////
    public Integer getReportFieldType() {
        return reportFieldType;
    }
    
    /////////////////////////////////////////////////    
    public String getLabel() {
        return label;
    }
    public void setLabel(String newValue) {
        this.label = newValue;
    }
    
    /////////////////////////////////////////////////     
    public String getValue() {
        return value;
    }
    public void setValue(String newValue) {
        this.value = newValue;
    }
    
    /////////////////////////////////////////////////        
    /*public ProjectTO getProject() {
        return project;
    }
    public void setProject(ProjectTO newValue) {
        this.project = newValue;
    }*/
    
    /////////////////////////////////////////////////      
    public boolean isVisible() {
        return isVisible;
    }
    public void setVisible(boolean newValue) {
        this.isVisible = newValue;
    }
}
