package br.com.tratomais.core.report;

import br.com.tratomais.core.model.report.Report;


public class TOFactory {
	
	public static final int REPORT_TO = 1;
	public static final int REPORT_FIELD_TO = 2;
	
	public static Object createTO(int en) {
		Object obj = null;
		switch(en) {
			case REPORT_TO: obj = new ReportTO();
			case REPORT_FIELD_TO: obj = new ReportFieldTO();
		}
		return obj;
	}
	/*
	 * Converte Report em ReportTO
	 */
	public static ReportTO convToReportTO (Report r) {
		ReportTO rto = new ReportTO();
		rto.setDataType(r.getDataType());
		rto.setExecutionHour(r.getExecutionHour());
		rto.setExportReportFormat(r.getExportReportFormat());
		rto.setFinalDate(r.getFinalDate());
		rto.setGoal(r.getGoal());
		rto.setId(r.getId().toString());
		rto.setLastExecution(r.getLastExecution());
		//rto.setLocale(r.getLocale());
		rto.setName(r.getName());
		rto.setProfile(r.getProfile());
		rto.setReportFileName(r.getReportFileName());
		rto.setReportPerspectiveId(r.getReportPerspectiveId());
		rto.setSqlStement(r.getSqlStement());
		rto.setTolerance(r.getTolerance());
		rto.setToleranceType(r.getToleranceType());
		rto.setType(r.getType());
		rto.setReportConnection(r.getReportConnection());
		return rto;
	}
}
