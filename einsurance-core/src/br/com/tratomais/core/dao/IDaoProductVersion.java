/**
 * 
 */
package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.ProductPaymentTerm;
import br.com.tratomais.core.model.product.ProductTerm;
import br.com.tratomais.core.model.product.ProductVersion;

/**
 * @author eduardo.venancio
 *
 */
public interface IDaoProductVersion extends IDaoGeneric<ProductVersion, Integer> {

	public List<ProductVersion> findByName(String name);
	
	public ProductVersion getProductVersionByBetweenRefDate(int productId, Date RefDate);
	
	public ProductVersion getProductVersionByRefDate(int productId, Date refDate);

	public List<ProductVersion> getProductVersionByProduct(int productId);
	
	public ProductVersion saveProductVersionCopy(ProductVersion inputProductVersion);

	public List<ProductVersion> listProductVersion(int productID, Date refDate);

	public List<ProductTerm> listProductTermByRefDate(int productID, Date versionDate);

	public List<ProductPaymentTerm> listProductPaymentTermByRefDate(int productID, Date versionDate);

	public ProductPaymentTerm getProductPaymentTerm(int productID, int billingMethodID, Date versionDate);

	public ProductPaymentTerm getProductPaymentTerm(int productID, int paymentTermID, int billingMethodID, Date versionDate);
}
