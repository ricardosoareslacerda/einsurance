package br.com.tratomais.core.dao;

import java.util.List;

import br.com.tratomais.core.model.product.Coverage;

public interface IDaoCoverage extends IDaoGeneric<Coverage, Integer> {
	public List<Coverage> findByName(String name);
	public List<Coverage> findByObject(Integer objectId);
	public Coverage getById(int id);
	/**
	 * @param objectId
	 * @param coverageCode
	 * @param goodsCode
	 * @return
	 */
	public Coverage getByCoverageCode(int objectId, String coverageCode, String goodsCode);
}
