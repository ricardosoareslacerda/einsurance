package br.com.tratomais.core.dao;

import br.com.tratomais.core.model.interfaces.FileLot;
import br.com.tratomais.core.model.product.IdentifiedList;

public interface IDaoFileLot extends IDaoGeneric< FileLot, Integer >{

	public FileLot getFileLot(Integer fileLotId, Integer lotId);

	public IdentifiedList listFileLot(IdentifiedList identifiedList, Integer lotId);

}
