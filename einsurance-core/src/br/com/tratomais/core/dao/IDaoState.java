package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.State;
import br.com.tratomais.core.model.product.IdentifiedList;

public interface IDaoState extends IDaoGeneric< State, Integer > {

	public List<State> findByName(String name); 
	
	public List<State> findByName(Integer id);
	
	public List<State> findByCountryId(Integer Id);
	
	public IdentifiedList listStateByCountry(IdentifiedList identifiedList, int countryId);

	public List<State> listStateByRefDate(int countryID, Date refDate);
	
	public List<State> findByNameOrUf(String nameOrUF);
	
}
