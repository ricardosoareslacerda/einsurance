package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.Domain;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.util.TransportationClass;

public interface IDaoUser extends IDaoGeneric<User, Integer> {

	public User findByLogin(String userName);

	public User findByLogin(String userName, Domain domain);

	public List<User> findByName(String name);

	public List<User> listAllAdmin();

	public List<User> listUserAllowed(List<Integer> listOfPartnersId, boolean isRoot) ;

	public List<User> listUserPerPartner(int partnerId);
	
	/**
	 * Query to list the users (used in gridview)
	 * 
	 * @param limit: Number of rows to return.  If this is not set all rows will be returned.
	 * @param page: The page number to return (if paginating).
	 * @param columnSorted: Field to sort the output by.
	 * @param typeSorted: Sort order.  Either 'asc' (ascending) or 'desc' (descending).
	 * @param search: True or False (whether or not this is a search). Default is False. 
	 * Also takes strings of 'true' and 'false'.
	 * @param searchField: The field we're searching on.
	 * @param searchString: The string to match for our search.
	 * @return List<User>
	 */
	public TransportationClass<User> listUserToGrid(int limit, int page, String columnSorted, String typeSorted,
													String search, String searchField, String searchString,
													List<Integer> listOfPartnersId, boolean isRoot);
	
	/**
	 * Query to list the users and partners active (used in gridview)
	 * 
	 * @param limit: Number of rows to return.  If this is not set all rows will be returned.
	 * @param page: The page number to return (if paginating).
	 * @param userId: Id User
	 * @return {@link TransportationClass <{@link User}>}
	 */
	public TransportationClass<User> listUserAllActivePartnerToGrid(int limit, int page, int userId);

	public List<User> listUser(int insurerID, int partnerID, int channelID, Date refDate);
}
