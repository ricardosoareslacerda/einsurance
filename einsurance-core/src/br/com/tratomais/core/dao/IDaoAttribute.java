package br.com.tratomais.core.dao;

import java.util.Date;

import br.com.tratomais.core.model.product.Attribute;

public interface IDaoAttribute {

	public Attribute getAttribute( Integer attributeId, Date referenceDate );
	
}
