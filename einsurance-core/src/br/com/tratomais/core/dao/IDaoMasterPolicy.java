package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.policy.MasterPolicy;

public interface IDaoMasterPolicy extends IDaoGeneric<MasterPolicy, Integer> {

	public List< MasterPolicy > findByName(String name);
	
	public List<MasterPolicy> listAllWithDescriptions();
	
	public List<MasterPolicy> getRenewableMasterPolicies();

	public List<MasterPolicy> listMasterPolicy(int insurerID, int partnerID, Date refDate);
}
