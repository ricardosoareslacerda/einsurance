package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.BillingAgency;
import br.com.tratomais.core.model.product.BillingMethod;

public interface IDaoBillingMethod extends IDaoGeneric< BillingMethod, Integer > {
	
	public BillingMethod getById(Integer id);

	public List<BillingMethod> listBillingMethod(int productID, Date versionDate);

	public BillingAgency getBillingAgencyById(int billingAgencyID, Date refDate);

	/**
	 * List of payment methods, by product and term, returning only a few attributes
	 * @param paymentTermType Payment term type
	 * @param productId Product identifier
	 * @param versionDate Date of version
	 * @return List of {@link BillingMethod} instance found, with lazy connections
	 */
	public List<BillingMethod> listPaymentType(int productId, Integer paymentTermType, Date versionDate);
}
