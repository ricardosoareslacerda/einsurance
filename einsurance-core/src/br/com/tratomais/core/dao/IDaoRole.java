package br.com.tratomais.core.dao;

import br.com.tratomais.core.model.Role;

public interface IDaoRole extends IDaoGeneric<Role, Integer> {

}
