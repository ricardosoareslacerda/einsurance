package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.Currency;

public interface IDaoCurrency extends IDaoGeneric< Currency, Integer > {

	public List<Currency> listCurrency(boolean indexer, Date refDate);

	public Currency getCurrency(int currencyID, Date refDate);

}
