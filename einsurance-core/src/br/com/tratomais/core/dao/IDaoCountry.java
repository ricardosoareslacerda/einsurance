package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.Country;
import br.com.tratomais.core.model.product.IdentifiedList;

public interface IDaoCountry extends IDaoGeneric< Country, Integer > {

	public List<Country> findByName(String name); 
	
	public List<Country> findByName(Integer id);
	
	public IdentifiedList listCountry(IdentifiedList identifiedList);

	public List<Country> listCountryByRefDate(Date refDate);
}
