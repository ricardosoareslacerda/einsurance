package br.com.tratomais.core.dao;

import java.util.List;

import br.com.tratomais.core.model.interfaces.Exchange;

public interface IDaoExchange {

	public Exchange getExchange(Integer exchangeId);
	
	public List<Exchange> listExchange();
}
