package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.PartnerAndChannel;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.customer.Partner;

public interface IDaoPartner extends IDaoGeneric<Partner, Integer>{

	public List<Partner> findByName(String name);
	public List<Partner> listAllByUser(User user);
	public List<Partner> listAllAllowedByUser(User user);
	public List<PartnerAndChannel> listPartnersAndChannel(List<Integer> partnersIdList) ;
	public List<Partner> listPartnerPerProductOrBroker(Integer partnerId, Integer productId, Integer brokerId, Integer userId);
	
	/**
	 * Query to list partners active by user
	 * 
	 * @param userId: Id User
	 * @return {List<{@link Partner}>}
	 */
	public List<Partner> listAllActivePartnerByUser(int userId);
	public List<Partner> listPartner(int insurerID, Date refDate);
}
