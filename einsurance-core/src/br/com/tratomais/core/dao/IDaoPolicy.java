package br.com.tratomais.core.dao;


import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.hibernate.Transaction;

import br.com.tratomais.core.model.ApplicationProperty;
import br.com.tratomais.core.model.Inflow;
import br.com.tratomais.core.model.claim.ClaimAction;
import br.com.tratomais.core.model.claim.ClaimActionLock;
import br.com.tratomais.core.model.claim.ClaimCause;
import br.com.tratomais.core.model.claim.ClaimEffect;
import br.com.tratomais.core.model.customer.Activity;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Customer;
import br.com.tratomais.core.model.customer.Occupation;
import br.com.tratomais.core.model.customer.Profession;
import br.com.tratomais.core.model.pendencies.LockPendency;
import br.com.tratomais.core.model.policy.Contract;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.EndorsementId;
import br.com.tratomais.core.model.policy.Installment;
import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.policy.ItemPersonalRiskGroup;
import br.com.tratomais.core.model.policy.MasterPolicy;
import br.com.tratomais.core.model.product.BillingMethod;
import br.com.tratomais.core.model.product.ConversionRate;
import br.com.tratomais.core.model.product.Coverage;
import br.com.tratomais.core.model.product.DataOption;
import br.com.tratomais.core.model.product.PaymentOption;
import br.com.tratomais.core.model.renewal.RenewalAlert;
import br.com.tratomais.esb.interfaces.model.response.ClaimResponse;

public interface IDaoPolicy {
	public void evict(Endorsement endorsement);
	
	public List<Occupation> listAllOccupation();
	
	public List< Profession > listAllProfession();

	public List< Activity > listAllActivity();

	public List< Inflow > listAllInflow(String inflowType);

	public Endorsement saveEndorsement(Endorsement endorsement);
	
	public Customer saveCustomer ( Customer customer );
	
	public Endorsement adjustEndorsementToCalc(Endorsement inputEndorsement, int issuanceType, IDaoPolicy.AdjustmentsToEndorsement adjustments);
	
	public Contract saveContract(Contract contract);

	public MasterPolicy findMasterPolicyById(int i);
	
	public Contract listContract(int contractId);

	public Endorsement loadEndorsement(int endorsementId, int contractId) ;

	public int cancellationEndorsement(int endorsementId, int contractId, int reason, int issuingStatus, Date cancelDate ) ;

	public int cancellationContract(int contractId, int contractStatus, Date cancelDate);

	public int cancellationItem(int endorsementId, int contractId, int itemStatusInactive, Date date);
	
	/**
	 * cancellation of the item
	 * @param endorsementId
	 * @param contractId
	 * @param itemId
	 * @param itemStatus
	 * @param cancelDate
	 * @return
	 */
	public int cancellationItem(int endorsementId, int contractId, int itemId, int itemStatus, Date cancelDate);

	public int cancellationClaimItem(int endorsementId, int contractId, int itemId, int itemStatus, Date cancelDate, String claimNumber, Date claimDate, Date claimNotification);

	public int cancellationClaimItemRiskGroup(int endorsementId, int contractId, int itemId, int riskGroupId, Date cancelDate, String claimNumber, Date claimDate, Date claimNotification);

	public int cancellationInstallment(int endorsementId, int contractId, int installmentStatusAnnulment, Date date);

	public int inactivationCustomerbyCustomerId(int customerId);

	public int inactivationAddressbyAddressId(int addressId);
	
	public int inactivationPhonebyAddressId(int addressId);
	
	public int removePhonebyAddressId(int addressId);

	public List<BillingMethod> listPaymentType(int productId, Date referenceDate);

	public void deleteInstallment(Installment installment);
	
	public Long getNextApplicationPropertyValue(int applicationId, String propertyName);
	
	public void updateApplicationPropertyValue(ApplicationProperty appProperty);

	public void adjustEndorsementToEffectivate(Endorsement endorsement, IDaoPolicy.AdjustmentsToEndorsement adjustments);
	
	/**
	 * Recupera a parcela de acordo com os paramentros. 
	 * @param contractId
	 * @param endorsementId
	 * @param intallmentId
	 * @return Obejto Isntallment, contendo a parcela selecionada
	 */
	public Installment getInstallment(int contractId, int endorsementId, int intallmentId);

	/**
	 * Recupera a lista de parcelas de um contrato e endosso.
	 * @param contractId
	 * @param endorsementId
	 * @return lista de Parcelas
	 */
	public List<Installment> getInstallmentForEndorsementId(int contractId,	int endorsementId);
	
	/**
	 * Recupera o ultimo endosso v�lido de acordo com o numero do contrato.
	 * @param numContrato - Numero do contrato - contractId
	 * @return Endoserment - O endosso encontrado.
	 */
	public Endorsement lastValidEndosementForContractNumber(Integer numContrato);

	public Endorsement lastValidEndorsementByEvict(Integer contractId);
	
	/**
	 * Recupera o ultimo endosso v�lido de acordo com o numero do contrato e data efetivacao.
	 * @param {@link Integer} contractNumber
	 * @param {@link Date} effectiveDate
	 * @return {@link Endorsement}
	 */
	public Endorsement lastValidEndosementForContractNumber(Integer contractNumber, Date effectiveDate);
	
	/**
	 * Return the Contract Number
	 * 
	 * @param {@link BigInteger} policyNumber
	 * @param {@link Long} certificateNumber
	 * @return {@link Contract}
	 */
	public Contract getContractNumber(BigInteger policyNumber, Long certificateNumber, Date effectiveDate);

	/**
	 * 
	 * @param contractId
	 * @param endorsementId
	 * @return
	 */
	public Endorsement getEndosement(int contractId, int endorsementId);

	public static class AdjustmentsToEndorsement {
		private Integer endorsementType;
		private Date effectiveDate;
		private Date cancelDate;
		private String claimNumber;
		private Date claimDate;
		private Date claimNotification;
		private Integer paymentTermId;
		private Double premiumPending;
		private Date expiryDate;
		private Integer cancelReason;
		private boolean proposal;
		private PaymentOption paymentOption;
		private boolean recalculation;

		public AdjustmentsToEndorsement(Date effectiveDate) {
			super();
			this.effectiveDate = effectiveDate;
		}
		
		public AdjustmentsToEndorsement(Date effectiveDate, 
										Date cancelDate) {
			super();
			this.effectiveDate = effectiveDate;
			this.cancelDate = cancelDate;
		}
		
		public AdjustmentsToEndorsement(Date effectiveDate, 
										Date cancelDate,
										PaymentOption paymentOption) {
				super();
				this.effectiveDate = effectiveDate;
				this.cancelDate = cancelDate;
				this.paymentOption = paymentOption;
		}
		
		public AdjustmentsToEndorsement(Date effectiveDate,
										boolean isRecalculation,
										boolean isProposal,
										PaymentOption paymentOption) {
			super();
			this.effectiveDate = effectiveDate;
			this.recalculation = isRecalculation;
			this.proposal = isProposal;
			this.paymentOption = paymentOption;
		}		
		
		public AdjustmentsToEndorsement(Date effectiveDate, 
										Date cancelDate,
										Integer cancelReason,
										Integer endorsementType) {
			super();
			this.effectiveDate = effectiveDate;
			this.cancelDate = cancelDate;
			this.cancelReason = cancelReason;
			this.endorsementType = endorsementType;
		}
				
		public AdjustmentsToEndorsement(Date effectiveDate, 
										Date cancelDate,
										Integer paymentTermId) {
			super();
			this.effectiveDate = effectiveDate;
			this.cancelDate = cancelDate;
			this.paymentTermId = paymentTermId;
		}
		
		public AdjustmentsToEndorsement(Date effectiveDate,
										Date cancelDate, 
										Integer endorsementType, 
										String claimNumber,
										Date claimDate, 
										Date claimNotification) {
			super();
			this.effectiveDate = effectiveDate;
			this.cancelDate = cancelDate;
			this.endorsementType = endorsementType;
			this.claimNumber = claimNumber;
			this.claimDate = claimDate;
			this.claimNotification = claimNotification;
		}
		
		public AdjustmentsToEndorsement(Double premiumPending, 
										Date expiryDate) {
			super();
			this.premiumPending = premiumPending;
			this.expiryDate = expiryDate;
		}
		
		public AdjustmentsToEndorsement(Double premiumPending, 
										Date expiryDate,
										Date effectiveDate) {
			super();
			this.premiumPending = premiumPending;
			this.expiryDate = expiryDate;
			this.effectiveDate = effectiveDate;
		}
		
		public AdjustmentsToEndorsement(Double premiumPending, 
										Date expiryDate,
										String claimNumber,
										Date claimDate, 
										Date claimNotification) {
			super();
			this.premiumPending = premiumPending;
			this.expiryDate = expiryDate;
			this.claimNumber = claimNumber;
			this.claimDate = claimDate;
			this.claimNotification = claimNotification;
		}

		/**
		 * @return the endorsementType
		 */
		public Integer getEndorsementType() {
			return endorsementType;
		}
		/**
		 * @param endorsementType the endorsementType to set
		 */
		public void setEndorsementType(Integer endorsementType) {
			this.endorsementType = endorsementType;
		}
		/**
		 * @return the newEffectiveDate
		 */
		public Date getEffectiveDate() {
			return effectiveDate;
		}
		/**
		 * @param newEffectiveDate the newEffectiveDate to set
		 */
		public void setEffectiveDate(Date effectiveDate) {
			this.effectiveDate = effectiveDate;
		}
		/**
		 * @return the cancelDate
		 */
		public Date getCancelDate() {
			return cancelDate;
		}
		/**
		 * @param cancelDate the cancelDate to set
		 */
		public void setCancelDate(Date cancelDate) {
			this.cancelDate = cancelDate;
		}
		/**
		 * @return the claimNumber
		 */
		public String getClaimNumber() {
			return claimNumber;
		}
		/**
		 * @param claimNumber the claimNumber to set
		 */
		public void setClaimNumber(String claimNumber) {
			this.claimNumber = claimNumber;
		}
		/**
		 * @return the claimDate
		 */
		public Date getClaimDate() {
			return claimDate;
		}
		/**
		 * @param claimDate the claimDate to set
		 */
		public void setClaimDate(Date claimDate) {
			this.claimDate = claimDate;
		}
		/**
		 * @return the claimNotification
		 */
		public Date getClaimNotification() {
			return claimNotification;
		}
		/**
		 * @param claimNotification the claimNotification to set
		 */
		public void setClaimNotification(Date claimNotification) {
			this.claimNotification = claimNotification;
		}
		/**
		 * @param paymentTermId the paymentTermId to set
		 */
		public void setPaymentTermId(Integer paymentTermId) {
			this.paymentTermId = paymentTermId;
		}
		/**
		 * @return the paymentTermId
		 */
		public Integer getPaymentTermId() {
			return paymentTermId;
		}

		public Double getPremiumPending() {
			return premiumPending;
		}

		public void setPremiumPending(Double premiumPending) {
			this.premiumPending = premiumPending;
		}

		public Date getExpiryDate() {
			return expiryDate;
		}

		public void setExpiryDate(Date expiryDate) {
			this.expiryDate = expiryDate;
		}

		public void setCancelReason(Integer cancelReason) {
			this.cancelReason = cancelReason;
		}

		public Integer getCancelReason() {
			return cancelReason;
		}
		
		public boolean isProposal() {
			return this.proposal;
		}

		public void setProposal(boolean proposal) {
			this.proposal = proposal;
		}
		
		public PaymentOption getPaymentOption() {
			return this.paymentOption;
		}

		public void setPaymentOption(PaymentOption paymentOption) {
			this.paymentOption = paymentOption;
		}
		
		public boolean isRecalculation() {
			return recalculation;
		}

		public void setRecalculation(boolean recalculation) {
			this.recalculation = recalculation;
		}		
	}

	//WBB: 2011-01-11
	public abstract List<LockPendency> findLockPendencies(int endorsementId, int contractId, Integer itemId, int pendencyId);

	public abstract List<LockPendency> findLockPendencies(int endorsementId, int contractId, int pendencyId);

	public abstract List<LockPendency> findLockPendencies(int endorsementId, int contractId);

	public abstract Installment findInstallment(int contractId, int endorsementId, int installmentId);

	public abstract Installment findInstallment(long proposalNumber, int installmentID);

	public abstract void salvaHistorico(LockPendency lockPendency,
			String descricao, int historyStatus, Date issueDate,
			Integer userID, Integer lockPendencyStatus);

	public abstract void historico(LockPendency lockPendency, String descricao,
			int historyStatus, Date issueDate, Integer userID,
			Integer lockPendencyStatus);

	public abstract DataOption getDataOption(int DataOptionId);

	public abstract LockPendency findPendency(int contractId, int endorsementId, Integer itemId, int pendencyId);

	public abstract Object save(Object obj);

	public ClaimAction getClaimAction(ClaimResponse claimResponse);

	public ClaimCause getClaimCause(Integer claimCauseID);

	public ClaimEffect getClaimEffect(Integer claimEffectID);

	public Channel getChannel(Integer channelId);

	public Coverage getCoverage(Integer coverageId);

	public Item findItem(int contractId, int endorsementId, int itemId);

	public ItemPersonalRiskGroup findItemPersonalRiskGroup(int contractId, int endorsementId, int itemId, int riskGroupId);
	public ClaimActionLock getClaimActionLock(ClaimResponse claimResponse, int issuanceType);

	public List<Contract> listContractsPerMasterPolicy(MasterPolicy masterPolicy);

	public List<Endorsement> listEndorsementForEmission();
	
	public Contract getContract(int contractId);

	public Endorsement getLastValidEndosementWithPaymentOptions(int contractId);

	public void flush();

	public Contract updateContract(Contract inputContract);

	public Transaction beginTransaction();

	public void saveRenewalAlert(List<RenewalAlert> renewalAlerts);

	public RenewalAlert saveRenewalAlert(RenewalAlert renewalAlerts);
	
	public List<RenewalAlert> listRenewalAlert(EndorsementId endorsement);
	public List<RenewalAlert> listRenewalAlert(int contractId, int endorsementId);

	public List<RenewalAlert> listRenewalAlert(EndorsementId endorsement, boolean resolved);
	public List<RenewalAlert> listRenewalAlert(int contractId, int endorsementId, boolean resolved);
	
	public List<Contract> listEndorsementRenewed();
	public List<Contract> listEndorsementNotRenewed();
	
	public List<Contract> listContractRenewed(BigInteger policyNumber, Long certificateNumber);

	public ApplicationProperty loadApplicationProperty(int applicationId, String propertyName);
	
	public ConversionRate findConversionRateByRefDate(Integer currencyId, Date refDate);

	public List<Profession> listProfessionByRefDate(Date refDate);

	public List<Occupation> listOccupationByRefDate(Date refDate);

	public List<Activity> listActivityByRefDate(Date refDate);

	public List<Inflow> listInflowByRefDate(Date refDate);
}
