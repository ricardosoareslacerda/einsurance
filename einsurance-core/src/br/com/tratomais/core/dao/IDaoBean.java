package br.com.tratomais.core.dao;

import java.io.Serializable;

@SuppressWarnings("unchecked")
public interface IDaoBean {

	PersistentEntity load(Class clazz, Serializable id);

}