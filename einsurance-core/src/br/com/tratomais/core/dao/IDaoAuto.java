package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.auto.AutoBrand;
import br.com.tratomais.core.model.auto.AutoModel;
import br.com.tratomais.core.model.auto.AutoVersion;
import br.com.tratomais.core.model.auto.AutoVersionID;

public interface IDaoAuto {
	/**
	 * Retorna lista de todas as marcas
	 * @return
	 */
	public List<AutoBrand> listBrands();
	/** 
	 * Lista todos os modelos por marca
	 * @param mark Marca do ve�culo
	 * @return Lista todos os modelos
	 */
	public List<AutoModel> listModels(int brandId);
	
	public List<Integer> listModelYear(int autoModelId, Date referDate);
	
	public Double getAutoCost(int autoModelId, Integer year, Date referenceDate);
	
	public AutoVersion getAutoVersion(AutoVersionID autoVersionID ); 
	
	public AutoVersion getAutoVersion(Integer autoModeID, Date effectiveDate );
	
	public AutoVersion findAutoVersion(int autoModelId, Date referenceDate);
}
