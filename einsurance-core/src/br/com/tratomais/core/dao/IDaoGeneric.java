package br.com.tratomais.core.dao;

import java.util.List;

public interface IDaoGeneric<T extends PersistentEntity, ID> {

	T findById(ID id);

	List<T> listAll();

	List<T> listAllActive();

	T save(T entity);

	void delete(T entity);

}
