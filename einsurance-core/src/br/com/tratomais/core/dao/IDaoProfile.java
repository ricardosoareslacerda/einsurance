package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.Profile;
import br.com.tratomais.core.model.product.ProfileId;

public interface IDaoProfile extends IDaoGeneric<Profile, ProfileId> {
	public List<Profile> findByProductIdReferenceDate(int productId, Date referenceDate);
	
}
