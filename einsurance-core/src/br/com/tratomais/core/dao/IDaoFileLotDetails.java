package br.com.tratomais.core.dao;

import br.com.tratomais.core.model.interfaces.FileLotDetails;
import br.com.tratomais.core.model.product.IdentifiedList;

public interface IDaoFileLotDetails extends IDaoGeneric< FileLotDetails, Integer >{

	public FileLotDetails getFileLotDetails(Integer fileLotDetailsId, Integer fileLotId);

	public IdentifiedList listFileLotDetails(IdentifiedList identifiedList, Integer fileLotId);

}
