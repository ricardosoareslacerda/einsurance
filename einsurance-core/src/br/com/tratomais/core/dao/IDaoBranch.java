package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.Branch;

public interface IDaoBranch extends IDaoGeneric< Branch, Integer > {

	public List<Branch> listBranch(Date refDate);

	public Branch getBranch(int branchID, Date refDate);

}
