package br.com.tratomais.core.dao;

import java.util.List;

import br.com.tratomais.core.model.Module;

public interface IDaoModule extends IDaoGeneric<Module, Integer>{
	
	public List<Module> findByName(String name);

}
