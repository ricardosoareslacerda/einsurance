package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.InsuredObject;

public interface IDaoObject extends IDaoGeneric<InsuredObject, Integer>{
	public List<InsuredObject> findByName(String name);

	public List<InsuredObject> listObject(Date refDate);

	public InsuredObject getObject(int objectID, Date refDate);

	public InsuredObject getObjectByID(int objectID);
}
