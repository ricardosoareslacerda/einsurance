package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.Inflow;
import br.com.tratomais.core.model.customer.Activity;
import br.com.tratomais.core.model.customer.Insurer;
import br.com.tratomais.core.model.customer.Occupation;
import br.com.tratomais.core.model.customer.Profession;

public interface IDaoInsurer extends IDaoGeneric< Insurer, Integer >{
	
	public List<Insurer> findByName(String name); 
	
	public List<Insurer> findByName(Integer id);

	public List<Insurer> listInsurerByRefDate(Date refDate); 
	
	public Occupation getOccupationById(int occupationId);
	
	public Profession getProfessionById(int professionId);
	
	public Activity getActivityById(int activityId);
	
	public Inflow getInflowById(int inflowId);

}
