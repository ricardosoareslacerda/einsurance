package br.com.tratomais.core.dao;

import java.util.Collection;

import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.UserPartner;

public interface IDaoUserPartner extends IDaoGeneric<UserPartner, Integer>{

	public Collection<UserPartner> findByUser(User user);

}
