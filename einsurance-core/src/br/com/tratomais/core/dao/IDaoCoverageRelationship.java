package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.CoverageRelationship;

public interface IDaoCoverageRelationship extends IDaoGeneric<CoverageRelationship, Integer> {

	public List<CoverageRelationship> findByName(String name);
	
	public List <CoverageRelationship> listCoverageRelationship(int coverageId, int parentId, int coverageRelationshipType, Date refDate);
	
	public List <CoverageRelationship> listCoverageRelationship(int coverageId, int coverageRelationshipType, Date refDate);
	
	public List <CoverageRelationship> listCoverageRelationship(int coverageId, Date refDate);
	
	public List <CoverageRelationship> listCoverageRelationshipByProductId(int productId, int planId, int coverageId, int coverageRelationshipType, Date refDate);
}
