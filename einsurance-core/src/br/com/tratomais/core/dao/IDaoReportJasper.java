package br.com.tratomais.core.dao;

import java.util.List;

import br.com.tratomais.core.model.report.Report;

public interface IDaoReportJasper extends IDaoGeneric<Report, Long> {
	
	public Report getObject(Long id);
	
	public List<Object> getListReport(String query);
    
	public Integer getObjectId(Integer contractId);
    
	public List<Report> listReportIdByType(Integer contractId, Integer endorsementId);
    
	public Integer getReportIdByType(Integer contractId, Integer endorsementId, Integer reportType);
}
