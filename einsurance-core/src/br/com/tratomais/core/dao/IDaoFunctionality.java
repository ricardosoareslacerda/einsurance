package br.com.tratomais.core.dao;

import java.util.List;

import br.com.tratomais.core.model.Functionality;

public interface IDaoFunctionality  extends IDaoGeneric<Functionality, Integer>{

	public List<Functionality> findByName(String name);

}
