package br.com.tratomais.core.dao;

import java.util.List;

import br.com.tratomais.core.model.policy.ItemPersonalRisk;

public interface IDaoItemPersonalRisk extends IDaoGeneric<ItemPersonalRisk, Integer>{

	public List<ItemPersonalRisk> findByName(String name);
	
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId, int endorsementId, int itemId);
	
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId, int endorsementId);
	
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId);

}
