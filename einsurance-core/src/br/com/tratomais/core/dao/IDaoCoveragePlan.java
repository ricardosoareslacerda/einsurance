package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.CoveragePlan;

public interface IDaoCoveragePlan extends IDaoGeneric<CoveragePlan, Integer> {
	public List<CoveragePlan> findByName(String name);
	public CoveragePlan getCoveragePlanByRefDate(int productId, int planId, int coverageId, Date refDate);
	public List<CoveragePlan> getCoveragePlanByRefDate(int productId, int planId, Date refDate);
	public CoveragePlan loadCoveragePlan(int coveragePlanId);
	public List<CoveragePlan> listCoveragePlan(int productID, Date versionDate);
}
