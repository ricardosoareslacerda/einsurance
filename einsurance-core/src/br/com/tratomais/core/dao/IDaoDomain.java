package br.com.tratomais.core.dao;

import java.util.List;

import br.com.tratomais.core.model.Domain;

public interface IDaoDomain extends IDaoGeneric<Domain, Integer> {

	public List<Domain> findByName(String name);
	public Domain findDomainByName(String name);

}
