package br.com.tratomais.core.dao;

import java.util.List;

import br.com.tratomais.core.model.product.Plan;

public interface IDaoPlan extends IDaoGeneric<Plan, Integer> {
	public List<Plan> findByName(String name);
}
