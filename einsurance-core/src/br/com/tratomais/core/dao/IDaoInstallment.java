package br.com.tratomais.core.dao;

import br.com.tratomais.core.model.policy.Installment;

/**
 * Dao para manipulação dos installments
 * @author luiz.alberoni
 *
 */
public interface IDaoInstallment {
	/**
	 * Grava forçando a atualização em banco neste momento
	 * @param installment a ter os dados persistidos/atualizados
	 * @return
	 */
	public abstract Installment mergeAndFlush(Installment installment);
}