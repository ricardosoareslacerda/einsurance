package br.com.tratomais.core.dao;

import java.util.List;

import br.com.tratomais.core.model.report.CollectionDetailsMainParameters;
import br.com.tratomais.core.model.report.CollectionMainParameters;
import br.com.tratomais.core.model.report.CollectionDetailsMainResult;
import br.com.tratomais.core.model.report.BillingMethodResult;
import br.com.tratomais.core.model.report.CollectionMainResult;
import br.com.tratomais.core.model.report.StatusLote;

public interface IDaoCollectionMain extends IDaoGeneric<CollectionDetailsMainResult, Integer>{
	
	public List<CollectionDetailsMainResult> listCollectionDetailsMain(CollectionDetailsMainParameters collectionDetailsMainParameters);

	public int listCollectionDetailsMainTotalRecords(CollectionDetailsMainParameters collectionDetailsMainParameters);

	public List<BillingMethodResult> listBillingMethod();

	public List<CollectionMainResult> listCollectionMain(CollectionMainParameters collectionMainParameters);
	
	public int listCollectionMainTotalRecords(CollectionMainParameters collectionMainParameters);

	public List<StatusLote> listStatusLote();
	
	public List<BillingMethodResult> listExchangeCollection();

}
