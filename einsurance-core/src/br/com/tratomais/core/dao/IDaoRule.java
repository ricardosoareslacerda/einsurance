package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.Clause;
import br.com.tratomais.core.model.product.CoverageRangeValue;
import br.com.tratomais.core.model.product.Rule;
import br.com.tratomais.core.model.product.RuleFeature;
import br.com.tratomais.core.model.product.RuleTarget;
import br.com.tratomais.core.model.product.RuleType;

public interface IDaoRule extends IDaoGeneric<Rule, Integer> {

	public List<RuleTarget> listRuleTarget(int productId, int ruleType, int ruleActionType);
	
	public List<RuleTarget> listRuleTarget(int productId, int ruleType, int ruleActionType, int attributeValue1);
	
	public List<Rule> listRuleByTargetId(int ruleTargetId, Date refDate);
	
	public List<RuleFeature> listRuleFeatureByRuleId(int ruleId, Date referenceDate);
	
	public Clause getClauseById(int clauseId);
	
	public Clause getClauseByProductId(int productId, int clauseId, Date refDate);
	
	public RuleType getRuleTypeById(int ruleType);
	
	public CoverageRangeValue getCoverageRangeValueByInsuredValueLimit(int productId, int planId, int coverageId, double insuredValue, Date refDate);
}
