package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.CoveragePersonalRisk;
import br.com.tratomais.core.model.product.CoveragePlan;
import br.com.tratomais.core.model.product.CoverageRangeValue;
import br.com.tratomais.core.model.product.PlanKinship;
import br.com.tratomais.core.model.product.PlanPersonalRisk;
import br.com.tratomais.core.model.product.PlanRiskType;
import br.com.tratomais.core.model.product.Product;
import br.com.tratomais.core.model.product.ProductOption;
import br.com.tratomais.core.model.product.ProductPlan;
import br.com.tratomais.core.model.product.ProductTerm;

public interface IDaoProduct extends IDaoGeneric<Product, Integer> {

	public List<ProductOption> findProductsbyPartner(int partnerId, int insurerId, Date refDate);

	public List<ProductPlan> findPlanByProductId(int productId, Date refDate);

	public List<ProductPlan> findRiskPlanByProductId(int productId, Date refDate);

	public List<ProductPlan> listPersonalRiskPlanByProductId(int productId, Date refDate);

	public Product findProductToBeCopy(int productId, Date referenceDate);

	public List<Product> findProductsPerPartner(int partnerId);

	public ProductPlan getPlanById(int productId, int planId, Date refDate);

	public ProductPlan getRiskPlanById(int productId, int planId, Date refDate);
	
	public List <CoverageRangeValue> listCoverageRangeValueByRefDate(int productId, int coveragePlanId, int coverageId ,Date refDate);	
	
	public ProductTerm getProductTermById(int productId, int termId, Date refDate);

	public CoverageRangeValue getCoverageRangeValueById(int productId, int coveragePlanId, int coverageId, int rangeId, Date refDate);

	public CoverageRangeValue getCoverageRangeValueByInsuredValue(int productId, int coveragePlanId, int coverageId, double insuredValue, Date refDate);

	public List <CoverageRangeValue> listCoverageFractionValueByInsuredValue(int productId, int coveragePlanId, int coverageId, double insuredValue, Date refDate);

	/**
	 * 
	 * @param productId ProductId
	 * @param coveragePlanId PlanId
	 * @param refDate
	 * @return
	 */
	public List <CoveragePlan> listCoveragePlanByRefDate(int productId, int coveragePlanId, Date refDate);

	public List <PlanRiskType> listPlanRiskTypeByRefDate(int productId, int riskPlanId, Date refDate);

	public PlanRiskType getPlanRiskTypeById(int productId, int riskPlanId, int riskType, Date refDate);

	public List <PlanRiskType> listPlanRiskTypeDescriptionByRefDate(int productId, int riskPlanId, Date refDate);	

	public List<PlanKinship> listPlanKinshipByRiskPlan(int productId, int riskPlanId, int renewalType, Date refDate);

	public PlanKinship listPlanKinshipByPersonalRiskType(int productId, int riskPlanId, int personalRiskType, int renewalType, Date refDate);

	public List <CoveragePersonalRisk> listCoveragePersonalRiskByPersonalRiskType(int productId, int coveragePlanId,int personalRiskType, Date refDate);

	public ProductOption getProduct(int productId, Date referenceDate) ;

	public Product getProductById(int productId) ;

	public ProductPlan getProductPlanByStandard(int productId, int riskPlanId, Date refDate);	

	public List<ProductPlan> listCoveragePlanByProductId(int productId, int riskPlanId, Date refDate);	

	public PlanPersonalRisk findPlanPersonalRisk(Date effectiveDate, int personType, int planId, int productId);

	public List<Product> listProductPerPartnerOrBroker(Integer partnerId, Integer productId, Integer brokerId, Integer userId);

	public PlanKinship getPlanKinship(int productId, int riskPlan, Integer personalRiskType, Integer kinshipType, int renewalType, Date effectiveDate);

	public PlanPersonalRisk getPlanPersonalRisk(int productId, int planId, Integer personType, Date effectiveDate);

	public ProductPlan getProductPlanById(int productId, int planId, Date referenceDate);

	public int getNextProductId();

	public List<Product> listProduct(int objectID, Date refDate);

	public List<ProductPlan> listProductPlan(int productID, Date versionDate);

	public CoverageRangeValue getCoverageRangeValue(int productID, int planID, int coverageID, int rangeValueID, Date versionDate);
}
