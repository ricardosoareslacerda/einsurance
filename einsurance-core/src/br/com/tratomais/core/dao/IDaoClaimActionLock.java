package br.com.tratomais.core.dao;

import br.com.tratomais.core.model.claim.ClaimActionLock;
import br.com.tratomais.core.model.claim.ClaimActionLockId;

/** Dao de ClaimActionLock */
public interface IDaoClaimActionLock {
	/**
	 * Persiste a entidade
	 * @param entity Entidade a ser persistida
	 * @return C�pia da entidade
	 */
	public ClaimActionLock save(ClaimActionLock entity);
	/**
	 * Busca por chave prim�ria
	 * @param id Identificador da chave prim�ria
	 * @return Inst�ncia de ClaimActionLock correspondente
	 */
	public ClaimActionLock findById(ClaimActionLockId id);
}