package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.ResponseRelationship;

public interface IDaoResponseRelationship {

	public List<ResponseRelationship> getResponseRelationship( Integer productId, Integer questionnaireId, Integer questionId, Integer responseId, Date referenceDate );
	
}
