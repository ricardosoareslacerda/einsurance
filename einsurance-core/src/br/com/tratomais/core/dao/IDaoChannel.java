package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Partner;
import br.com.tratomais.core.util.TransportationClass;

public interface IDaoChannel extends IDaoGeneric<Channel, Integer> {
	public List<Channel> findByNickName(String nickName);
	public List<Channel> findByName(String name);
	public List<Channel> findByPartnerId(int partnerId);
	public List< Channel > findByNameAndPartner( String name, Partner partner );
	/**
	 * 
	 * @param user
	 * @param partner
	 * @return
	 */
	public List<Channel> findByUserAndPartner(User user, int partnerId);
	/**
	 * Find Partners by parterId
	 * @param partnerID
	 * @param active
	 * @return
	 */
	public List<Channel> findByPartnerId(int partnerID, boolean onlyActive);
	public TransportationClass<Channel> listChannelToGrid(int limit, int page, String columnSorted, String typeSorted, 
			String search, String searchField, String searchString, int partnerID);
	public List<Channel> listChannel(int insurerID, int partnerID, Date refDate);
}
