package br.com.tratomais.core.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoUser;
import br.com.tratomais.core.infrastructure.AuthenticationHelper;
import br.com.tratomais.core.model.Domain;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.UserPartner;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Partner;
import br.com.tratomais.core.util.TransportationClass;

@SuppressWarnings("unchecked")
public final class DaoUserImpl extends DaoGenericImpl<User, Integer> implements IDaoUser {

	public DaoUserImpl() {
		super();
	}

	public User findByLogin(String userName) {
		return super.findUniqueEntityByQuery("From User where login = ?", userName);
	}

	public List<User> findByName(String name) {
		return super.findByCriteria(Restrictions.ilike("name", name, MatchMode.START));
	}

	public User findByLogin(String userName, Domain domain) {
		Object[] object = { userName, domain };
		return super.findUniqueEntityByQuery("From User where login = ? and domain = ?", object);
	}

	public List<User> listAllAdmin() {
		return super.getHibernateTemplate().find("From User u where u.administrator = true");
	}

	public List<User> listUserAllowed(List<Integer> listOfPartnersId, boolean isRoot) {
		StringBuilder hql = new StringBuilder();
		hql.append("Select distinct u from User u, UserPartner up, Partner p ");
		hql.append("where u.userId = up.id.userId ");
		hql.append("and p.partnerId = up.id.partnerId ");
		hql.append("and p.partnerId in (:listOfPartnersId) ");
		if(isRoot == false){
			hql.append("and u.administrator = false ");
		}
		Query query = super.getQuery(hql.toString());
		query.setParameterList("listOfPartnersId", listOfPartnersId);
		List<User> myList = query.list();
		return myList;
	}

	@Override
	public List<User> listUserPerPartner(int partnerId) {
		Partner partner = (Partner) getSession().get(Partner.class, partnerId);
		List<User> lista = new LinkedList<User>();
		for(UserPartner up:partner.getUserPartners())
			lista.add(up.getUser());
		return lista;
	}

	/**
	 * Query to list the users (used in gridview)
	 * 
	 * @param limit: Number of rows to return.  If this is not set all rows will be returned.
	 * @param page: The page number to return (if paginating).
	 * @param columnSorted: Field to sort the output by.
	 * @param typeSorted: Sort order.  Either 'asc' (ascending) or 'desc' (descending).
	 * @param search: True or False (whether or not this is a search). Default is False. 
	 * Also takes strings of 'true' and 'false'.
	 * @param searchField: The field we're searching on.
	 * @param searchString: The string to match for our search.
	 * @param listOfPartnersId:
	 * @param isRoot:
	 * @return TransportationClass<User>
	 */
	public TransportationClass<User> listUserToGrid(int limit, int page, String columnSorted, String typeSorted,
																	String search, String searchField, String searchString,
																	List<Integer> listOfPartnersId, boolean isRoot){	
		int total_pages;
		int start;
		int count;
		
		if(search.equalsIgnoreCase("true")){
			
			String field = "";
			
			if(searchField.equalsIgnoreCase("u.name")){
				field = "name";
			}else if(searchField.equalsIgnoreCase("u.login")){
				field = "login";
			}			
			
			count = (Integer)getSession().createCriteria(User.class)
			.add(Restrictions.ilike(field, searchString, MatchMode.ANYWHERE))
			.setProjection(Projections.count("userId"))
			.uniqueResult();			
		} else {
			count = (Integer)getSession().createCriteria(User.class)
			.setProjection(Projections.count("userId"))
			.uniqueResult();			
		}		
		
		// calculation of total pages for the query
		total_pages = count/limit + ((count%limit)!=0?1:0);
		
		// if for some reasons the requested page is greater than the total
		// set the requested page to total page
		if (page > total_pages) page = total_pages;

		// calculate the starting position of the rows
		start = limit*page - limit; // do not put $limit*($page - 1)
		
		// if for some reasons start position is negative set it to 0
		// typical case is that the user type 0 for the requested page
		if(start < 0) start = 0; 		
				
		StringBuilder hql = new StringBuilder();
		hql.append("SELECT DISTINCT u FROM User u, UserPartner up, Partner p");
		hql.append(" WHERE u.userId = up.id.userId");
		hql.append(" AND p.partnerId = up.id.partnerId");
		hql.append(" AND p.partnerId IN (:listOfPartnersId)");
		
		if(isRoot == false){
			hql.append("AND u.administrator = false ");
		}

		if(search.equalsIgnoreCase("true")){
			hql.append(" AND " + searchField + " like '%" + searchString + "%'");
		}		
		
		if ("asc".equals(typeSorted)){
			hql.append(" ORDER BY " + columnSorted + " ASC");
		} else {
			hql.append(" ORDER BY " + columnSorted + " DESC");
		}		
		
		Query query = super.getQuery(hql.toString());
		query.setParameterList("listOfPartnersId", listOfPartnersId);
		
		List<User> users = query.list();		
		
		for (User userWork : users) {
			Hibernate.initialize(userWork.getDomain());
		}
		
		return new TransportationClass<User>(users, count, total_pages);
	}
	
	/**
	 * Query to list the users and partners active (used in gridview)
	 * 
	 * @param limit: Number of rows to return.  If this is not set all rows will be returned.
	 * @param page: The page number to return (if paginating).
	 * @param userId: Id User
	 * @return {@link TransportationClass <{@link User}>}
	 */
	public TransportationClass<User> listUserAllActivePartnerToGrid(int limit, int page, int userId){
		int total_pages;
		int start;
		int count;
		
		//used to new user
		if(userId == 0){
			userId = AuthenticationHelper.getLoggedUser().getUserId();
		}
		
		count = (Integer)getSession().createCriteria(User.class)
		.setProjection(Projections.count("userId"))
		.uniqueResult();			
		
		// calculation of total pages for the query
		total_pages = count/limit + ((count%limit)!=0?1:0);
		
		// if for some reasons the requested page is greater than the total
		// set the requested page to total page
		if (page > total_pages) page = total_pages;

		// calculate the starting position of the rows
		start = limit*page - limit; // do not put $limit*($page - 1)
		
		// if for some reasons start position is negative set it to 0
		// typical case is that the user type 0 for the requested page
		if(start < 0) start = 0; 		
		
		StringBuilder hql = new StringBuilder();
		hql.append("Select u from `User` u, UserPartner up, Partner p, Channel c ");
		hql.append("where u.userId = up.id.userId ");
		hql.append("and p.partnerId = up.id.partnerId ");
		hql.append("and p.partnerId = c.channelId ");
		hql.append("and p.active = true ");
		hql.append("and c.active = true ");
		hql.append("and u.userId = ? ");
		hql.append("order by p.name asc");	
		
		Object[] parameters = new Object[] { userId };		
		List<User> users = getHibernateTemplate().find(hql.toString(), parameters); 
		
		for (User userWork : users) {
			Hibernate.initialize(userWork.getUserPartners());
			
			for (UserPartner userPartnerWork : userWork.getUserPartners()) {
				Hibernate.initialize(userPartnerWork.getPartner());
			}
		}
		
		return new TransportationClass<User>(users, count, total_pages);
	}

	@Override
	public List<User> listUser(int insurerID, int partnerID, int channelID,	Date refDate) {
		StringBuilder hql = new StringBuilder();
		hql.append("Select u from User u, UserPartner up, Partner p, Channel c ");
		hql.append("where u.userId = up.id.userId ");
		hql.append("and p.partnerId = up.id.partnerId ");
		hql.append("and p.partnerId = c.partnerId ");
		hql.append("and p.active = true ");
		hql.append("and c.active = true ");
		hql.append("and u.active = true ");
		hql.append("and p.partnerId = ? ");
		hql.append("and c.channelId = ? ");
		hql.append("order by u.name asc");
		
		Object[] parameters = new Object[] { partnerID, channelID};		
		List<User> users = getHibernateTemplate().find(hql.toString(), parameters);
		List<User> listUserByChannel = new ArrayList<User>();
		for (User user : users) {
			for (Channel channel : user.getChannels()) {
				if (channel.getChannelId() == channelID) {
					listUserByChannel.add(user);
					break;
				}
			}
		}
		return listUserByChannel;
	}	
}