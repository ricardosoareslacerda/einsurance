package br.com.tratomais.core.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * Helper
 * @author luiz.alberoni
 *
 * @param <T> Classe da entidade
 * @param <I> Identificador
 */
public abstract class DaoHibernateSupport<T,I extends Serializable> extends HibernateDaoSupport {
	private static final Log LOG = LogFactory.getLog(DaoGenericImpl.class);

	/** 
	 * Representa a classe-refer�ncia para o DAO
	 */
	private Class<T> classePersistida;

	/**
	 * �nico construtor, obriga sua inicializa��o por aqui (N�o � um bean).
	 * @param clazz
	 */
	public DaoHibernateSupport(Class<T> clazz) {
		super();
		this.classePersistida = clazz;
	}

	/**
	 * Retorna a classe persistida
	 * @return Classe persistida
	 */
	public Class<T> getClassePersistida() {
		return this.classePersistida;
	}

	/**
	 * Apaga a entidade
	 * @param entity Entidade a ser apagada
	 */
	protected void delete(T entity) {
		try {
			getHibernateTemplate().delete(entity);
		} catch (final HibernateException ex) {
			DaoHibernateSupport.LOG.error(ex);
			throw ex;
		}
	}

	/**
	 * Encontra inst�ncia por ID
	 * @param id
	 * @return inst�ncia atrav�s do ID
	 */
	@SuppressWarnings("unchecked")
	protected T findById(I id) {
		try {
			return (T) super.getHibernateTemplate().get( getClassePersistida(), id);
		} catch (final HibernateException ex) {
			DaoHibernateSupport.LOG.error(ex);
			throw ex;
		}
	}

	/**
	 * Lista de todas as entidades
	 * @return Lista de todas as entidades daquele tipo
	 */
	@SuppressWarnings("unchecked")
	protected List<T> listAll() {
		try {
			return super.getHibernateTemplate().loadAll(getClassePersistida());
		} catch (final HibernateException ex) {
			DaoHibernateSupport.LOG.error(ex);
			throw ex;
		}
	}

	/**
	 * Grava registro
	 * @param entity
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected T save(T entity) {
		try {
			return (T) super.getHibernateTemplate().save(entity);
		} catch (final HibernateException ex) {
			DaoHibernateSupport.LOG.error(ex);
			throw ex;
		}
	}
	
	/**
	 * Realiza um merge com as entidades existentes na session
	 * @param entity 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected T merge(T entity) {
		try {
			return (T) getHibernateTemplate().merge(entity);
		} catch (final HibernateException ex) {
			DaoHibernateSupport.LOG.error(ex);
			throw ex;
		}
	}
	
	/**
	 * Realiza um update com as entidades existentes na session
	 * @param entity 
	 * @return
	 */
	protected void update(T entity) {
		try {
			getHibernateTemplate().update(entity);
		} catch (final HibernateException ex) {
			DaoHibernateSupport.LOG.error(ex);
			throw ex;
		}
	}

	/**
	 * Realiza um save(ou um update, de acordo com a necessidade) com as entidades existentes na session
	 * @param entity 
	 * @return
	 */
	protected void saveOrUpdate(T entity) {
		try {
			getHibernateTemplate().saveOrUpdate(entity);
		} catch (final HibernateException ex) {
			DaoHibernateSupport.LOG.error(ex);
			throw ex;
		}
	}
	
	/**
	 * Pesquisa atrav�s dos crit�rios passados
	 * @param criterion os v�rios crit�rios a serem utilizados (Ser�o usados em AND)
	 * @return registros de acordo com os crit�rios passados
	 */
	@SuppressWarnings("unchecked")
	protected List<T> findByCriteria(Criterion... criterion) {
		try {
			Criteria crit = this.createCriteria();
			for (Criterion c : criterion) {
				crit.add(c);
			}
			return crit.list();
		} catch (final HibernateException ex) {
			DaoHibernateSupport.LOG.error(ex);
			throw ex;
		}
	}

	/**
	 * Pesquisa atrav�s dos crit�rios passados
	 * @param criterion os v�rios crit�rios a serem utilizados (Ser�o usados em AND)
	 * @return registros de acordo com os crit�rios passados
	 */
	@SuppressWarnings("unchecked")
	protected T findUniqueByCriteria(Criterion... criterion) {
		try {
			Criteria crit = this.createCriteria();
			for (Criterion c : criterion) {
				crit.add(c);
			}
			crit.setFirstResult(0);
			crit.setMaxResults(1);
			return (T) crit.uniqueResult();
		} catch (final HibernateException ex) {
			DaoHibernateSupport.LOG.error(ex);
			throw ex;
		}
	}
	
	/**
	 * Retorna �nica entidade por quer
	 * @param query
	 * @param parameter
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected T findUniqueEntityByQuery(String query, Object... parameter) {
		List<T> list = super.getHibernateTemplate().find(query, parameter);
		if (list.size() == 0) {
			return null;
		}
		return (T) list.get(0);
	}

	/**
	 * Cria criteria
	 * @return Criteria pronto para ser usado
	 */
	protected Criteria createCriteria() {
		return getCurrentSession().createCriteria(getClassePersistida());
	}

	/**
	 * Retorna query para namedQuery (query deve constar na configura��o do Hibernate)
	 * @param name Nome da query
	 * @return Query pronta para ser chamada
	 */
	protected Query getNamedQuery(String name) {
		return getCurrentSession().getNamedQuery(name);
	}

	/**
	 * Query HQL pronta para ser usada
	 * @param query
	 * @return
	 */
	protected Query getQuery(String query) {
		return getCurrentSession().createQuery(query);
	}
	
	/**
	 * Retorna a sess�o corrente para o usu�rio
	 * @return
	 */
	protected Session getCurrentSession() {
		return super.getHibernateTemplate().getSessionFactory().getCurrentSession();
	}
}
