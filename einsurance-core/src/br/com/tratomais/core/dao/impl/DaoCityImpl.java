package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoCity;
import br.com.tratomais.core.model.City;
import br.com.tratomais.core.model.State;
import br.com.tratomais.core.model.product.IdentifiedList;

@SuppressWarnings("unchecked")
public class DaoCityImpl extends DaoGenericImpl< City, Integer > implements IDaoCity{
//	private static final Logger logger = Logger.getLogger( DaoCityImpl.class );

	
	public List< City > findByName(String name) {
		return this.getHibernateTemplate().find( "From City where name = ?", name );
	}

	public List< City > findByName(Integer id) {
		return this.getHibernateTemplate().find( "From City where cityId = ?", id );
	}

	public List< City > listCity(Integer stateId) {
		return this.getHibernateTemplate().find( "From City where stateId = ?", stateId );
	}
	
	public IdentifiedList listCityByState(IdentifiedList identifiedList, int stateId){
		
		List<City> cityList = this.getHibernateTemplate().find( "From City where stateId = ?", stateId);
		
		identifiedList.setResultArray(cityList);
		
		return identifiedList;
	}
	
	public IdentifiedList listIdentifiedListCity(IdentifiedList identifiedList){
		
		List<City> cityList = this.getHibernateTemplate().find( "From City" );
		
		identifiedList.setResultArray(cityList);
		
		return identifiedList;
	}

	@Override
	public List<City> listCityByRefDate(int countryID, int stateID, Date refDate) {
		Criteria crit = getSession().createCriteria(City.class);
		crit.createAlias("state", "_state");
		crit.add(Restrictions.eq("_state.countryId", countryID));
		crit.add(Restrictions.eq("stateId", stateID));
		crit.addOrder( Order.asc( "name" ) );
		
		return crit.list();
	}

	public List<City> findByNameNoAccents(State state, String name) {
		Query sqlQuery = getSession().getNamedQuery("findCityByNameNoAccents");
		sqlQuery.setInteger("stateId", state.getStateId());
		sqlQuery.setString("name", name);
		return (List<City>) sqlQuery.list();
	}
}
