/**
 * 
 */
package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoObject;
import br.com.tratomais.core.model.product.InsuredObject;

/**
 * @author eduardo.venancio
 *
 */
public class DaoObjectImpl extends DaoGenericImpl<InsuredObject, Integer> implements IDaoObject {

	public DaoObjectImpl(){
		super();
	}
	
	public List<InsuredObject> findByName(String name) {
		return super.findByCriteria(Restrictions.ilike("name", name, MatchMode.START));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InsuredObject> listObject(Date refDate) {
		return getSession().createCriteria(InsuredObject.class).list();
	}

	@Override
	public InsuredObject getObject(int objectID, Date refDate) {
		Criteria crit = getSession().createCriteria(InsuredObject.class);
		crit.add(Restrictions.eq("objectId", objectID));
		
		return (InsuredObject) crit.uniqueResult();
	}

	@Override
	public InsuredObject getObjectByID(int objectID) {
		Criteria crit = getSession().createCriteria(InsuredObject.class);
		crit.add(Restrictions.eq("objectId", objectID));
		
		return (InsuredObject) crit.uniqueResult();
	}
}