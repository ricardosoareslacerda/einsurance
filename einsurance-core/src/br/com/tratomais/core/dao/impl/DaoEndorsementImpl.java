package br.com.tratomais.core.dao.impl;

import org.hibernate.Query;
import org.hibernate.transform.Transformers;

import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.EndorsementId;

public class DaoEndorsementImpl extends DaoHibernateSupport<Endorsement, EndorsementId> {
	/**
	 * Construtor padr�o
	 */
	public DaoEndorsementImpl() {
		super(Endorsement.class);
	}
	
	/** 
	 * Grava endorsement 
	 * @param endorsement Endorsement a ser gravador
	 * @return Inst�ncia j� marcada para grava��o
	 */
	public Endorsement saveEndorsement(Endorsement endorsement){
		return super.save(endorsement);
	}
	
	/**
	 * Encontra atrav�s do Identificador
	 */
	public Endorsement findById(EndorsementId id){
		return super.findById(id);
	}
	
	/**
	 * Encontra atrav�s dos IDs
	 */
	public Endorsement getStatusById(int contractId, int endorsementId){
		Query query = getSession().getNamedQuery("getIssuingStatusById");
		query.setResultTransformer(Transformers.aliasToBean(Endorsement.class));
		query.setParameter("contractId", contractId);
		query.setParameter("endorsementId", endorsementId);
		return (Endorsement)query.uniqueResult();
	}
}