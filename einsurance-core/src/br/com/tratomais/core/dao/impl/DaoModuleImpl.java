package br.com.tratomais.core.dao.impl;

import java.util.List;

import br.com.tratomais.core.dao.IDaoModule;
import br.com.tratomais.core.model.Module;

@SuppressWarnings("unchecked")
public class DaoModuleImpl extends DaoGenericImpl<Module, Integer> implements IDaoModule {

	public DaoModuleImpl() {
		super();
	}

	
	public List<Module> findByName(String name) {
		return this.getHibernateTemplate().find("From Module where name = ?", name);
	}

	@Override
	public List<Module> listAll() {
		return this.getHibernateTemplate().find("From Module m where m.module is null and m.active=true");
	}
}