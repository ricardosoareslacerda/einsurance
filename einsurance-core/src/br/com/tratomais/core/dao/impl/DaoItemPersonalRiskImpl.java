package br.com.tratomais.core.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoItemPersonalRisk;
import br.com.tratomais.core.model.policy.ItemPersonalRisk;

/**
 * @author jones.silva
 *
 */
public class DaoItemPersonalRiskImpl extends DaoGenericImpl<ItemPersonalRisk, Integer> implements IDaoItemPersonalRisk{

	/**
	 * Lista ItemPersonalRisk
	 * @param contractId
	 * @param endorsementId
	 * @param itemId
	 * @return Lista de ItemPersonalRisk
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId, int endorsementId, int itemId) {
		Criteria criteria = createCriteria();
		
		criteria.add(Restrictions.eq("id.contractId", contractId));
		criteria.add(Restrictions.eq("id.endorsementId", endorsementId));
		criteria.add(Restrictions.eq("id.itemId", itemId));
		
		return criteria.list();
	}

	/**
	 * Lista ItemPersonalRisk
	 * @param contractId
	 * @param endorsementId
	 * @return Lista de ItemPersonalRisk
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId, int endorsementId) {
		Criteria criteria = createCriteria();
		
		criteria.add(Restrictions.eq("id.contractId", contractId));
		criteria.add(Restrictions.eq("id.endorsementId", endorsementId));
		
		return criteria.list();
	}

	/**
	 * Lista ItemPersonalRisk 
	 * @param contractId
	 * @return Lista de ItemPersonalRisk
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ItemPersonalRisk> listItemPersonalRisk(int contractId) {
		Criteria criteria = createCriteria();
		
		criteria.add(Restrictions.eq("id.contractId", contractId));
		
		return criteria.list();
	}

	/**
	 * Lista ItemPersonalRisk 
	 * @param name
	 * @return Lista de ItemPersonalRisk
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ItemPersonalRisk> findByName(String name) {
		Criteria criteria = createCriteria();
		
		criteria.add(Restrictions.eq("insuredName", name));
		
		return criteria.list();
	}
}