package br.com.tratomais.core.dao.impl;

import java.util.List;

import br.com.tratomais.core.dao.IDaoFunctionality;
import br.com.tratomais.core.model.Functionality;

@SuppressWarnings("unchecked")
public class DaoFunctionalityImpl extends DaoGenericImpl<Functionality, Integer> implements IDaoFunctionality{

	@Override
	public List<Functionality> findByName(String name) {
		return this.getHibernateTemplate().find("From Functionality where name = ?", name);
	}
	
	@Override
	public List<Functionality> listAll() {
		return this.getHibernateTemplate().find("From Functionality f where f.functionality is null and f.active=true");
	}
}