package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

//import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;

import br.com.tratomais.core.dao.IDaoPersonalRiskOption;
import br.com.tratomais.core.model.product.PersonalRiskOption;

public class DaoPersonalRiskOption extends DaoGenericImpl<PersonalRiskOption, Integer> implements IDaoPersonalRiskOption {

	@SuppressWarnings("unchecked")
	public List<PersonalRiskOption> findPersonalRiskOptions(int productId, int riskPlanId, Date referenceDate) {
		StringBuilder hql = new StringBuilder();

		hql.append("SELECT	new br.com.tratomais.core.model.product.PersonalRiskOption(");
		hql.append("		planRisk.id.personType,");
		hql.append("		person.fieldValue,");
		hql.append("		person.fieldDescription,");
		hql.append("		planRisk.quantityPersonalRisk,");
		hql.append("		planRisk.quantityAdditional,");
		hql.append("		planRisk.additionalNewborn)");
		hql.append("  FROM	PlanPersonalRisk planRisk,DataOption person " );
		hql.append(" WHERE	planRisk.id.personType = person.dataOptionId");
		hql.append("   AND	planRisk.id.productId = ?");
		hql.append("   AND	planRisk.id.planId = ?");
		hql.append("   AND	planRisk.id.effectiveDate <= ?");
		hql.append("   AND 	planRisk.expiryDate >= ?");

		Object[] parameters = new Object[] { productId, riskPlanId, referenceDate, referenceDate };

		return this.getHibernateTemplate().find(hql.toString(), parameters);
	}

	@SuppressWarnings("unchecked")
	public List<PersonalRiskOption> findPersonalRiskOptions() {
		Query query = super.getNamedQuery("listAllPersonalRiskOption");
		query.setResultTransformer(Transformers.aliasToBean(PersonalRiskOption.class));
		List<PersonalRiskOption> persons = query.list();
		return persons;
	}
}