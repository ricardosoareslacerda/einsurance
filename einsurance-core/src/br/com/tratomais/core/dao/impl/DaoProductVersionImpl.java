/**
 * 
 */
package br.com.tratomais.core.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeanUtils;

import br.com.tratomais.core.dao.IDaoProductVersion;
import br.com.tratomais.core.model.product.CoveragePersonalRisk;
import br.com.tratomais.core.model.product.CoveragePlan;
import br.com.tratomais.core.model.product.CoverageRangeValue;
import br.com.tratomais.core.model.product.CoverageRuleRoadmap;
import br.com.tratomais.core.model.product.Plan;
import br.com.tratomais.core.model.product.PlanKinship;
import br.com.tratomais.core.model.product.PlanPersonalRisk;
import br.com.tratomais.core.model.product.PlanRiskType;
import br.com.tratomais.core.model.product.Product;
import br.com.tratomais.core.model.product.ProductPaymentTerm;
import br.com.tratomais.core.model.product.ProductPlan;
import br.com.tratomais.core.model.product.ProductTerm;
import br.com.tratomais.core.model.product.ProductVersion;
import br.com.tratomais.core.model.product.Roadmap;
import br.com.tratomais.core.util.Constantes;

/**
 * @author eduardo.venancio
 *
 */
@SuppressWarnings("unchecked")
public class DaoProductVersionImpl extends DaoGenericImpl<ProductVersion, Integer> implements IDaoProductVersion {

	private DaoProductImpl DaoProduct;

	public DaoProductVersionImpl() {
		super(ProductVersion.class);
	}

	public void setDaoProduct(DaoProductImpl daoProduct) {
		DaoProduct = daoProduct;
	}	

	public List<ProductVersion> findByName(String name) {
		return super.findByCriteria(Restrictions.ilike("name", name, MatchMode.START));
	}

	public ProductVersion getProductVersionByBetweenRefDate(int productId, Date RefDate){
		Criteria crit = getSession().createCriteria(ProductVersion.class);
		crit.add(Restrictions.eq("id.productId", productId));
		crit.add(Restrictions.le("id.effectiveDate", RefDate));
		crit.add(Restrictions.ge("expiryDate", RefDate));
		crit.addOrder(Order.desc("referenceDate"));
		crit.setMaxResults(1);
		return (ProductVersion) crit.uniqueResult();
	}

	public ProductVersion getProductVersionByRefDate(int productId, Date refDate){
		ProductVersion newProductVersion = null;
		Criteria crit = getSession().createCriteria(ProductVersion.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "referenceDate", refDate ) );
		
		List <ProductVersion> list = crit.list();
		for ( ProductVersion productVersion : list ) {
			newProductVersion = productVersion;
			break;
		}
		
		return newProductVersion;
	}

	public List<ProductVersion> getProductVersionByProduct(int productId){
		
		List<ProductVersion> newProductVersion = new ArrayList<ProductVersion>();
		Product product = null;
		Criteria crit = createCriteria();
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		
		List <ProductVersion> list = crit.list();
		
		for ( ProductVersion productVersion : list ) {
			product = DaoProduct.findProductToBeCopy(productVersion.getId().getProductId(), 
													productVersion.getReferenceDate());
			productVersion.setProduct(product);
			newProductVersion.add(productVersion);
		}
		
		return newProductVersion;
	}

	public ProductVersion saveProductVersionCopy(ProductVersion inputProductVersion){
		ProductVersion workProductVersion = new ProductVersion();
		String[] ignoreProperties = { "product" };
		BeanUtils.copyProperties( inputProductVersion, workProductVersion, ignoreProperties );

		Product workProduct = new Product();
		BeanUtils.copyProperties( inputProductVersion.getProduct(), workProduct ); 
		
		StringBuilder hql = new StringBuilder();
		String strSql = "";
		Object[] parameters = null;
		
		//***********************************************************************
		//Trata Plano de Risco
		//***********************************************************************
		for (ProductPlan workProductPlan : workProduct.getProductPlans() ) {
			hql.append(workProductPlan.getId().getPlanId() + ",");
		}

		List<ProductPlan> workPPlan = listProductPlanRisk(workProduct.getProductId(), hql.toString());
		
		for (ProductPlan productPlan : workPPlan) {
			ProductPlan	newProductPlan = new ProductPlan(); 
			BeanUtils.copyProperties(productPlan, newProductPlan);
			
			// for each ProductPlan we get the CovergarePlan by productId and PlanId
			parameters = new Object[] { newProductPlan.getId().getProductId(), 
										newProductPlan.getId().getPlanId(), 
										workProductVersion.getReferenceDate(), 
										workProductVersion.getReferenceDate() };
			
			strSql = "From PlanRiskType where id.productId = ? and id.planId = ?";
			strSql += " and id.effectiveDate <= ?";
			strSql += " and expiryDate >= ?";
			List<PlanRiskType> planRiskTypes = (List<PlanRiskType>) getHibernateTemplate().find(strSql, parameters);
			
			if(planRiskTypes.size()>0){
				newProductPlan.setPlanRiskTypes(planRiskTypes);
			}
			
			//Busca cada a lista de coverageRageValues para cada plano de Risco.
			parameters = new Object[] { newProductPlan.getId().getProductId(), 
										newProductPlan.getId().getPlanId(), 
										workProductVersion.getReferenceDate(), 
										workProductVersion.getReferenceDate() };
			
			strSql = "From CoverageRangeValue where id.productId = ? and id.planId = ?";
			strSql += " and id.effectiveDate <= ?";
			strSql += " and expiryDate >= ?";
			List<CoverageRangeValue> riskCoverageRangeValues = (List<CoverageRangeValue>) getHibernateTemplate().find(strSql, parameters);
			
			if(planRiskTypes.size()>0){
				newProductPlan.setRiskCoverageRangeValues(riskCoverageRangeValues);
			}
			
			workProduct.getProductPlans().add(newProductPlan);	
		}
		//***********************************************************************
		
		List<PlanPersonalRisk> listPlanPersonalRisk =  listPlanPersonalRiskByRefDate(workProduct.getProductId(), workProductVersion.getReferenceDate());
		List<PlanKinship> listKinship = listPlanKinshipByRefDate(workProduct.getProductId(), workProductVersion.getReferenceDate());
		
		List<ProductPaymentTerm> listProdPay =  this.listProductPaymentTermByRefDate(workProduct.getProductId(), workProductVersion.getReferenceDate());
		workProduct.setProductPaymentTerms(new ArrayList<ProductPaymentTerm>());
		for (ProductPaymentTerm productPaymentTerm : listProdPay) {
			ProductPaymentTerm workPaymentTerm = new ProductPaymentTerm();
			BeanUtils.copyProperties(productPaymentTerm, workPaymentTerm);
			workProduct.getProductPaymentTerms().add(workPaymentTerm);
		}

		List<ProductTerm> listPrdTerm =   this.listProductTermByRefDate(workProduct.getProductId(), workProductVersion.getReferenceDate());
		workProduct.setProductTerms(new ArrayList<ProductTerm>());
		for (ProductTerm productTerm : listPrdTerm) {
			ProductTerm workProductTerm = new ProductTerm();
			BeanUtils.copyProperties(productTerm, workProductTerm);
			workProduct.getProductTerms().add(workProductTerm);
		}
		
		List<Roadmap> roadList = this.listRoadmapByRefDate(workProduct.getProductId(), workProductVersion.getReferenceDate());
		workProduct.setRoadmaps( new ArrayList<Roadmap>());
		int roadmapId = roadList.size() > 0 ? this.getLastRoadmapId() : 0;
		for (Roadmap roadmap : roadList) {
			Roadmap workRoadmap = new Roadmap();
			BeanUtils.copyProperties(roadmap, workRoadmap);
			workRoadmap.setRoadmapId(++roadmapId);
			workProduct.getRoadmaps().add(workRoadmap);
		}
		
		List<CoverageRuleRoadmap> coverageRuleRoadmapList = this.listCoverageRuleRoadmapByRefDate(workProduct.getProductId(), workProductVersion.getReferenceDate());
		
		//limpa a Chave e Cria novo produto
		Product savedProduct;
		workProduct.setProductId(DaoProduct.getNextProductId());
		savedProduct = DaoProduct.save( workProduct );
		workProduct.setProductId(savedProduct.getProductId());
		
		//Atualiza chave dos filhos de produto
		workProduct.updateKey();
		workProductVersion.updateKey( workProduct );
		
		this.save( workProductVersion );
		
		// salva Filhos
		for ( ProductPlan workProductPlan : workProduct.getProductPlans()){
			for ( CoveragePlan workCoveragePlan : workProductPlan.getCoveragePlans()){
				
				//For each coverageRangeValue set the new product key until save
				for (CoverageRangeValue coverageRangeValue : workCoveragePlan.getCoverageRangeValues()){
					CoverageRangeValue workCoverageRangeValue = new CoverageRangeValue();
					BeanUtils.copyProperties(coverageRangeValue, workCoverageRangeValue);
					workCoverageRangeValue.getId().setProductId(workProduct.getProductId());
					this.getHibernateTemplate().save(workCoverageRangeValue);
				}
				
				//For each CoveragePersonalRisk set the new product key until save
				for (CoveragePersonalRisk coveragePersonalRisk : workCoveragePlan.getCoveragePersonalRisks()){
					CoveragePersonalRisk workCoveragePersonalRisk = new CoveragePersonalRisk();
					BeanUtils.copyProperties(coveragePersonalRisk, workCoveragePersonalRisk);
					workCoveragePersonalRisk.getId().setProductId(workProduct.getProductId());
					this.getHibernateTemplate().save(workCoveragePersonalRisk);
				}
				
				this.getHibernateTemplate().save(workCoveragePlan);
			}
			
			//tratamento de chave para coveragerangevaluesrisk
			//For each RiskCoverageRangeValue set the new product key until save
			for (CoverageRangeValue riskCoverageRangeValue : workProductPlan.getRiskCoverageRangeValues()){
				CoverageRangeValue workRiskCoverageRangeValue = new CoverageRangeValue();
				BeanUtils.copyProperties(riskCoverageRangeValue, workRiskCoverageRangeValue);
				workRiskCoverageRangeValue.getId().setProductId(workProduct.getProductId());
				this.getHibernateTemplate().save(workRiskCoverageRangeValue);
			}
			
			for (PlanPersonalRisk workPlanPersonalRisk : workProductPlan.getPlanPersonalRisks() ) {
				this.getHibernateTemplate().save(workPlanPersonalRisk);
			}
			
			//For each CoveragePersonalRisk set the new product key until save
			for ( PlanRiskType planRiskType : workProductPlan.getPlanRiskTypes() ) {
				PlanRiskType workPlanRiskType = new PlanRiskType();
				BeanUtils.copyProperties(planRiskType, workPlanRiskType);
				workPlanRiskType.getId().setProductId(workProduct.getProductId());
				this.getHibernateTemplate().save(workPlanRiskType);
			}			
			this.getHibernateTemplate().save(workProductPlan);
		}
		
		for (PlanPersonalRisk planPersonalRisk : listPlanPersonalRisk) {
			PlanPersonalRisk workPlanPersonalRisk = new PlanPersonalRisk();
			BeanUtils.copyProperties(planPersonalRisk, workPlanPersonalRisk);
			workPlanPersonalRisk.getId().setProductId(workProduct.getProductId());
			this.getHibernateTemplate().save(workPlanPersonalRisk);
		}
		
		for (PlanKinship planKinship : listKinship) {
			PlanKinship workKinship = new PlanKinship();
			BeanUtils.copyProperties(planKinship, workKinship);
			workKinship.getId().setProductId(workProduct.getProductId());
			this.getHibernateTemplate().save(workKinship);
		}
		
		for ( ProductPaymentTerm workProductPaymentTerm : workProduct.getProductPaymentTerms() ) {
			this.getHibernateTemplate().save(workProductPaymentTerm);
		}
		
		for ( ProductTerm workProductTerm : workProduct.getProductTerms() ) {
			this.getHibernateTemplate().save(workProductTerm);
		}
		
		for ( Roadmap workRoadmap : workProduct.getRoadmaps() ) {
			this.getHibernateTemplate().save(workRoadmap);
		}
		
		for (CoverageRuleRoadmap coverageRuleRoadmap : coverageRuleRoadmapList) {
			CoverageRuleRoadmap workCoverageRuleRoadmap = new CoverageRuleRoadmap();
			String[] ignorePropertiesCoverageRule = { "product", "coverage", "coverageRule" };
			BeanUtils.copyProperties(coverageRuleRoadmap, workCoverageRuleRoadmap, ignorePropertiesCoverageRule);
			workCoverageRuleRoadmap.getId().setProductId(workProduct.getProductId());
			this.getHibernateTemplate().save(workCoverageRuleRoadmap);
		}
		
		return workProductVersion;
	}

	@SuppressWarnings("unused")
	private List <PlanKinship> listPlanKinshipByRefDate(int productId, int planId, Date refDate){
		Criteria crit = getSession().createCriteria(PlanKinship.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", planId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		
		List <PlanKinship> list = crit.list();
		
		return list;
	}			

	private List <PlanKinship> listPlanKinshipByRefDate(int productId, Date refDate){
		Criteria crit = getSession().createCriteria(PlanKinship.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		
		List <PlanKinship> list = crit.list();
		
		return list;
	}			

	private List <PlanPersonalRisk> listPlanPersonalRiskByRefDate(int productId, Date refDate){
		Criteria crit = getSession().createCriteria(PlanPersonalRisk.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		
		List <PlanPersonalRisk> list = crit.list();
		
		return list;
	}			

	@SuppressWarnings("unused")
	private List <ProductPlan> listProductPlanByRefDate_old(int productId, Date refDate){
		Criteria crit = getSession().createCriteria(ProductPlan.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		
		List <ProductPlan> list = crit.list();
		
		return list;
		
	}		

	public List<Plan> listProductPlanByRefDate(int productId, Date refDate) {
		StringBuilder hql = new StringBuilder();
		hql.append("select pp.plan from ProductPlan pp, Product prod, Plan pl ");
		hql.append("where pp.id.planId = pl.planId ");
		hql.append("and pp.id.productId = prod.productId ");
		hql.append("and prod.productId  = ? ");
		hql.append("and pp.plan.planType = ?");
		hql.append("and pp.id.effectiveDate <= ? ");
		hql.append("and pp.expiryDate >=  ? ");
		Object[] parameters = new Object[] { productId, Constantes.PLAN_TYPE_RISK, refDate, refDate };
		return getHibernateTemplate().find(hql.toString(), parameters);
	}

	public List<ProductPlan> listProductPlanRisk(int productId, String planRange) {
		
		StringBuilder hql = new StringBuilder();

		hql.append(    "FROM ProductPlan pp ");
		hql.append(  "WHERE pp.id.productId = ? ");
		hql.append(    "AND EXISTS ( ");
		hql.append(  		"SELECT p.planId ");
		hql.append(  		  "FROM Plan p ");
		hql.append(  			 ", PlanRelationship pr ");
		hql.append(  		 "WHERE p.planId = pp.id.planId ");
		hql.append(  		   "AND p.planType = 180 ");
		hql.append(  		   "AND pr.id.parentId = p.planId ");
		hql.append(  		   "AND pr.id.planId IN ( " );
		String[] arrValues = planRange.split(",");
		for (int i = 0; i < arrValues.length; i++) {
			hql.append(arrValues[i]);
			if(i < arrValues.length-1)
				hql.append(",");
		}		
		hql.append(  		")) ");

		Object[] parameters = new Object[] { productId };
		List<ProductPlan> returnProductPlan = getHibernateTemplate().find(hql.toString(), parameters);
		
		if(returnProductPlan.size() == 0 )
			returnProductPlan = null;
		
		return returnProductPlan;
	}

	@SuppressWarnings("unused")
	private List <PlanPersonalRisk> listPlanPersonalRiskByRefDate(int productId, int planId, Date refDate){
		Criteria crit = getSession().createCriteria(PlanPersonalRisk.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", planId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );

		List <PlanPersonalRisk> list = crit.list();
		
		return list;
	}			

	@Override
	public List <ProductPaymentTerm> listProductPaymentTermByRefDate(int productId, Date refDate){
		Criteria crit = getSession().createCriteria(ProductPaymentTerm.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );

		List <ProductPaymentTerm> list = crit.list();
		
		return list;
	}			

	@Override
	public List <ProductTerm> listProductTermByRefDate(int productId, Date refDate){
		Criteria crit = getSession().createCriteria(ProductTerm.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		
		List <ProductTerm> list = crit.list();
		
		return list;
	}			

	private List <Roadmap> listRoadmapByRefDate(int productId, Date refDate){
		Criteria crit = getSession().createCriteria(Roadmap.class);
		
		crit.add( Restrictions.eq( "productId", productId ) );
		crit.add( Restrictions.le( "effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );

		crit.addOrder( Order.asc( "roadmapId" ) );
		
		List <Roadmap> list = crit.list();
		
		return list;
	}			

	private int getLastRoadmapId(){
		Integer result = (Integer) getSession().createCriteria(Roadmap.class).setProjection(Projections.max("roadmapId")).uniqueResult();
		if (result == null)
			return 0;
		else
			return result.intValue();
	}

	private List <CoverageRuleRoadmap> listCoverageRuleRoadmapByRefDate(int productId, Date refDate){
		Criteria crit = getSession().createCriteria(CoverageRuleRoadmap.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		
		List <CoverageRuleRoadmap> list = crit.list();
		
		return list;
	}

	@Override
	public List<ProductVersion> listProductVersion(int productID, Date refDate) {
		Criteria crit = getSession().createCriteria(ProductVersion.class);
		crit.add( Restrictions.eq( "product.productId", productID ) );
		crit.add(Restrictions.le("id.effectiveDate", refDate));
		crit.add(Restrictions.ge("expiryDate", refDate));

		return crit.list();
	}

	@Override
	public ProductPaymentTerm getProductPaymentTerm(int productID, int billingMethodID, Date versionDate) {
		Criteria crit = getSession().createCriteria(ProductPaymentTerm.class);
		
		crit.add( Restrictions.eq( "id.productId", productID ) );
		crit.add( Restrictions.eq( "id.billingMethodId", billingMethodID ) );
		crit.add( Restrictions.le( "id.effectiveDate", versionDate ) );
		crit.add( Restrictions.ge( "expiryDate", versionDate ) );
		crit.setMaxResults(1);
		
		return (ProductPaymentTerm) crit.uniqueResult();
	}
	
	@Override
	public ProductPaymentTerm getProductPaymentTerm(int productID, int paymentTermID, int billingMethodID, Date versionDate) {
		Criteria crit = getSession().createCriteria(ProductPaymentTerm.class);
		
		crit.add( Restrictions.eq( "id.productId", productID ) );
		crit.add( Restrictions.eq( "id.paymentTermId", paymentTermID ) );
		crit.add( Restrictions.eq( "id.billingMethodId", billingMethodID ) );
		crit.add( Restrictions.le( "id.effectiveDate", versionDate ) );
		crit.add( Restrictions.ge( "expiryDate", versionDate ) );
		crit.setMaxResults(1);
		
		return (ProductPaymentTerm) crit.uniqueResult();
	}
}
