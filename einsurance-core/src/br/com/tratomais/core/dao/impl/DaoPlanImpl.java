package br.com.tratomais.core.dao.impl;

import java.util.List;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoPlan;
import br.com.tratomais.core.model.product.Plan;

public class DaoPlanImpl extends DaoGenericImpl<Plan, Integer> implements IDaoPlan {

	public DaoPlanImpl() {
		super();
	}
	
	public List<Plan> findByName(String name) {
		return super.findByCriteria(Restrictions.ilike("name", name, MatchMode.START));
	}
}