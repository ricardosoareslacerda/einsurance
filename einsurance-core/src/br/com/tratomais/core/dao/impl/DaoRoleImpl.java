package br.com.tratomais.core.dao.impl;

import br.com.tratomais.core.dao.IDaoRole;
import br.com.tratomais.core.model.Role;

public class DaoRoleImpl extends DaoGenericImpl<Role, Integer> implements IDaoRole {

	public DaoRoleImpl() {
	}
}