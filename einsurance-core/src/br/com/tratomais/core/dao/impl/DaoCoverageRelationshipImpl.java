package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoCoverageRelationship;
import br.com.tratomais.core.model.product.CoverageRelationship;

public class DaoCoverageRelationshipImpl extends DaoGenericImpl<CoverageRelationship, Integer> implements IDaoCoverageRelationship {

	@Override
	public List<CoverageRelationship> findByName(String name) {
		return null;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<CoverageRelationship> listCoverageRelationship(int coverageId, int parentId, int coverageRelationshipType, Date refDate) {
		Criteria crit = getSession().createCriteria(CoverageRelationship.class);
		
		crit.add(Restrictions.eq("id.coverageId", coverageId));
		crit.add(Restrictions.eq("id.parentId", parentId));
		crit.add(Restrictions.eq("id.coverageRelationshipType", coverageRelationshipType));
		crit.add(Restrictions.le("id.effectiveDate", refDate));
		crit.add(Restrictions.ge("expiryDate", refDate));
		
		return crit.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<CoverageRelationship> listCoverageRelationship(int coverageId, int coverageRelationshipType, Date refDate) {
		Criteria crit = getSession().createCriteria(CoverageRelationship.class);
		
		crit.add(Restrictions.eq("id.coverageId", coverageId));
		crit.add(Restrictions.eq("id.coverageRelationshipType", coverageRelationshipType));
		crit.add(Restrictions.le("id.effectiveDate", refDate));
		crit.add(Restrictions.ge("expiryDate", refDate));
		
		return crit.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<CoverageRelationship> listCoverageRelationship(int coverageId, Date refDate) {
		Criteria crit = getSession().createCriteria(CoverageRelationship.class);
		
		crit.add(Restrictions.eq("id.coverageId", coverageId));
		crit.add(Restrictions.le("id.effectiveDate", refDate));
		crit.add(Restrictions.ge("expiryDate", refDate));
		
		return crit.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<CoverageRelationship> listCoverageRelationshipByProductId(int productId, int planId, int coverageId, int coverageRelationshipType, Date refDate) {
		StringBuilder hql = new StringBuilder();
		hql.append("SELECT 	cr.CoverageID, ");
		hql.append("		cr.ParentID, ");
		hql.append("		cr.CoverageRelationshipType, ");
		hql.append("		cr.EffectiveDate, ");
		hql.append("		cr.ExpiryDate, ");
		hql.append("		cr.RelationshipFactor, ");
		hql.append("		cr.MinimumInsuredValue, ");
		hql.append("		cr.MaximumInsuredValue, ");
		hql.append("		cr.Registred, ");
		hql.append("		cr.Updated ");
		hql.append("FROM CoverageRelationship cr, ");
		hql.append("	 CoveragePlan cp ");
		hql.append("WHERE cp.CoverageID = cr.ParentID ");
		hql.append("  AND cp.ProductID = :productId ");
		hql.append("  AND cp.PlanID = :planId ");
		hql.append("  AND cr.CoverageID = :coverageId ");
		hql.append("  AND cr.CoverageRelationshipType = :coverageRelationshipType ");
		hql.append("  AND :refDate BETWEEN cr.EffectiveDate AND cr.ExpiryDate ");
		hql.append("  AND :refDate BETWEEN cp.EffectiveDate AND cp.ExpiryDate ");
		hql.append("ORDER BY cp.DisplayOrder ");
		
		SQLQuery query = getSession().createSQLQuery(hql.toString());
				 query.addEntity(CoverageRelationship.class);
				 query.setInteger("productId", productId);
				 query.setInteger("planId", planId);
				 query.setInteger("coverageId", coverageId);
				 query.setInteger("coverageRelationshipType", coverageRelationshipType);
				 query.setDate("refDate", refDate);	
		
		List<CoverageRelationship> list = query.list();
		return list;
	}
}