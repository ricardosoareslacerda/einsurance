package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoCurrency;
import br.com.tratomais.core.model.product.Currency;

public class DaoCurrencyImpl extends DaoGenericImpl<Currency, Integer> implements IDaoCurrency{

	@SuppressWarnings("unchecked")
	@Override
	public List<Currency> listCurrency(boolean indexer, Date refDate) {
		Criteria crit = getSession().createCriteria(Currency.class);
		crit.add(Restrictions.eq("indexer", indexer));
		
		return crit.list();
	}

	@Override
	public Currency getCurrency(int currencyID, Date refDate) {
		Criteria crit = getSession().createCriteria(Currency.class);
		crit.add(Restrictions.eq("currencyId", currencyID));
		
		return (Currency) crit.uniqueResult();
	}
}