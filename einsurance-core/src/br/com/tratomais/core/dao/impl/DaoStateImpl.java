package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoState;
import br.com.tratomais.core.model.State;
import br.com.tratomais.core.model.product.IdentifiedList;

@SuppressWarnings("unchecked")
public class DaoStateImpl extends DaoGenericImpl< State, Integer > implements IDaoState{
	
	public List< State > findByName(String name) {
		return this.getHibernateTemplate().find( "From State where name = ?", name );
	}

	public List< State > findByName(Integer id) {
		return this.getHibernateTemplate().find( "From State where stateId = ?", id );
	}

	public List< State > findByCountryId(Integer id) {
		return this.getHibernateTemplate().find( "From State where countryId = ?", id );
	}

	public IdentifiedList listStateByCountry(IdentifiedList identifiedList, int countryId){
		
		List<State> stateList = this.getHibernateTemplate().find( "From State where countryId = ?", countryId);
		
		identifiedList.setResultArray(stateList);
		
		return identifiedList;
	}

	@Override
	public List<State> listStateByRefDate(int countryID, Date refDate) {
		Criteria crit = getSession().createCriteria(State.class);
		crit.add(Restrictions.eq("countryId", countryID));
		crit.addOrder( Order.asc( "name" ) );
		
		return crit.list();
	}
	
	@Override
	public List<State> findByNameOrUf(String nameOrUF) {
		Criteria criteria = getSession().createCriteria(State.class);
		criteria.add(Restrictions.or(
				Restrictions.eq("ufCode", nameOrUF), 
				Restrictions.eq("name", nameOrUF))
		);
		criteria.addOrder(Order.asc("name"));
		return (List<State>) criteria.list();
	}
}