package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoInsurer;
import br.com.tratomais.core.model.Inflow;
import br.com.tratomais.core.model.customer.Activity;
import br.com.tratomais.core.model.customer.Insurer;
import br.com.tratomais.core.model.customer.Occupation;
import br.com.tratomais.core.model.customer.Profession;

@SuppressWarnings("unchecked")
public class DaoInsurerImpl extends DaoGenericImpl< Insurer, Integer >  implements IDaoInsurer{
	
	public List< Insurer > findByName(String name) {
		return this.getHibernateTemplate().find( "From Insurer where name = ?", name );
	}
	
	public List< Insurer > findByNickName(String nickName) {
		return this.getHibernateTemplate().find("From Insurer where nickName = ?", nickName);
	}	
	
	public List< Insurer > findByName(Integer id) {
		return this.getHibernateTemplate().find( "From Insurer where InsurerID = ?", id );
	}

	@Override
	public List<Insurer> listInsurerByRefDate(Date refDate) {
		Criteria crit = getSession().createCriteria(Insurer.class);
		crit.add(Restrictions.eq("active", true));
		
		return crit.list();
	}

	/**
	 * @param occupationId
	 * @return
	 */	
	public Occupation getOccupationById(int occupationId) {
		Criteria criteria = getSession().createCriteria(Occupation.class);
		criteria.add(Restrictions.eq("occupationId", occupationId));
		criteria.setFirstResult(0);
		criteria.setMaxResults(1);
		return (Occupation) criteria.uniqueResult();
	}

	/**
	 * @param professionId
	 * @return
	 */	
	public Profession getProfessionById(int professionId) {
		Criteria criteria = getSession().createCriteria(Profession.class);
		criteria.add(Restrictions.eq("professionId", professionId));
		criteria.setFirstResult(0);
		criteria.setMaxResults(1);
		return (Profession) criteria.uniqueResult();
	}

	/**
	 * @param activityId
	 * @return
	 */	
	public Activity getActivityById(int activityId) {
		Criteria criteria = getSession().createCriteria(Activity.class);
		criteria.add(Restrictions.eq("activityId", activityId));
		criteria.setFirstResult(0);
		criteria.setMaxResults(1);
		return (Activity) criteria.uniqueResult();
	}

	/**
	 * @param inflowId
	 * @return
	 */	
	public Inflow getInflowById(int inflowId) {
		Criteria criteria = getSession().createCriteria(Inflow.class);
		criteria.add(Restrictions.eq("inflowId", inflowId));
		criteria.setFirstResult(0);
		criteria.setMaxResults(1);
		return (Inflow) criteria.uniqueResult();
	}
}