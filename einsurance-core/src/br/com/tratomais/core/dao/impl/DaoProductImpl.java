package br.com.tratomais.core.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import br.com.tratomais.core.dao.IDaoDataOption;
import br.com.tratomais.core.dao.IDaoProduct;
import br.com.tratomais.core.model.product.CoveragePersonalRisk;
import br.com.tratomais.core.model.product.CoveragePlan;
import br.com.tratomais.core.model.product.CoverageRangeValue;
import br.com.tratomais.core.model.product.DataOption;
import br.com.tratomais.core.model.product.PlanKinship;
import br.com.tratomais.core.model.product.PlanKinshipId;
import br.com.tratomais.core.model.product.PlanPersonalRisk;
import br.com.tratomais.core.model.product.PlanPersonalRiskId;
import br.com.tratomais.core.model.product.PlanRiskType;
import br.com.tratomais.core.model.product.Product;
import br.com.tratomais.core.model.product.ProductOption;
import br.com.tratomais.core.model.product.ProductPlan;
import br.com.tratomais.core.model.product.ProductTerm;
import br.com.tratomais.core.model.product.ProductVersion;
import br.com.tratomais.core.util.Constantes;

/**
 * @author eduardo.venancio
 *
 */
@SuppressWarnings("unchecked")
public class DaoProductImpl extends DaoGenericImpl<Product, Integer> implements IDaoProduct {

	private IDaoDataOption daoDataOption;

	public void setDaoDataOption(IDaoDataOption daoDataOption) {
		this.daoDataOption = daoDataOption;
	}

	public List<ProductOption> findProductsbyPartner(int partnerId, int insurerId, Date refDate) {
		
		SimpleDateFormat workDate = new SimpleDateFormat("yyyy/MM/dd");
		String newDate = workDate.format(refDate);
		
		List<ProductOption> products = null; 
		
		try {
			Query query = super.getNamedQuery("getProducts");
			query.setResultTransformer(Transformers.aliasToBean(ProductOption.class));
			query.setParameter("partnerId", partnerId);
			query.setParameter("insurerId", insurerId);
			query.setParameter("refDate", workDate.parse(newDate));
			products = query.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return products;
	}

	public List <CoveragePlan> listCoveragePlanByRefDate(int productId, int planId, Date refDate){
		Criteria crit = getSession().createCriteria(CoveragePlan.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", planId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		
		crit.addOrder( Order.asc( "displayOrder" ) );
		
		List <CoveragePlan> list = crit.list();
		
		return list;
	}		

	public List<ProductPlan> findPlanByProductId(int productId, Date refDate) {
		StringBuilder hql = new StringBuilder();
		hql.append("select pp from ProductPlan pp, Product prod, Plan pl ");
		hql.append("where pp.id.planId = pl.planId ");
		hql.append("and pp.id.productId = prod.productId ");
		hql.append("and prod.productId  = ? ");
		hql.append("and pp.plan.planType = ?");
		hql.append("and pp.id.effectiveDate <= ? ");
		hql.append("and pp.expiryDate >=  ? ");
		hql.append("order by pp.displayOrder, pp.id.planId ");
		Object[] parameters = new Object[] { productId, Constantes.PLAN_TYPE_COVERAGE, refDate, refDate };
		return getHibernateTemplate().find(hql.toString(), parameters);
	}

	public ProductPlan getProductPlanByStandard(int productId, int riskPlanId, Date refDate) {
		StringBuilder hql = new StringBuilder();
		hql.append("select pp from ProductPlan pp, Product prod, Plan pl ");
		hql.append("where pp.id.planId = pl.planId ");
		hql.append("and pp.id.productId = prod.productId ");
		hql.append("and prod.productId  = ? ");
		hql.append("and pp.plan.planType = ?");
		hql.append("and pp.id.effectiveDate <= ? ");
		hql.append("and pp.expiryDate >=  ? ");
		hql.append("and pp.standard = ? ");
		hql.append("and exists(from PlanRelationship pr where pp.id.planId = pr.id.planId and pr.id.parentId = ?) ");
		hql.append("order by pp.displayOrder, pp.id.planId ");
		Object[] parameters = new Object[] { productId, Constantes.PLAN_TYPE_COVERAGE, refDate, refDate, true, riskPlanId };		
		List<ProductPlan> productPlans = getHibernateTemplate().find(hql.toString(), parameters);
		if (productPlans != null && productPlans.size() > 0) {
			return (ProductPlan)productPlans.get(0);
		}
		return null;
	}

	public List<ProductPlan> listCoveragePlanByProductId(int productId, int riskPlanId, Date refDate) {
		StringBuilder hql = new StringBuilder();
		hql.append("select pp from ProductPlan pp, Product prod, Plan pl ");
		hql.append("where pp.id.planId = pl.planId ");
		hql.append("and pp.id.productId = prod.productId ");
		hql.append("and prod.productId  = ? ");
		hql.append("and pp.plan.planType = ?");
		hql.append("and pp.id.effectiveDate <= ? ");
		hql.append("and pp.expiryDate >=  ? ");
		hql.append("and exists(from PlanRelationship pr where pp.id.planId = pr.id.planId and pr.id.parentId = ?) ");
		hql.append("order by pp.displayOrder, pp.id.planId ");
		Object[] parameters = new Object[] { productId, Constantes.PLAN_TYPE_COVERAGE, refDate, refDate, riskPlanId };
		return getHibernateTemplate().find(hql.toString(), parameters);
	}

	public List<ProductPlan> findRiskPlanByProductId(int productId, Date refDate) {
		StringBuilder hql = new StringBuilder();
		hql.append("select pp from ProductPlan pp, Product prod, Plan pl ");
		hql.append("where pp.id.planId = pl.planId ");
		hql.append("and pp.id.productId = prod.productId ");
		hql.append("and prod.productId  = ? ");
		hql.append("and pp.plan.planType = ?");
		hql.append("and pp.id.effectiveDate <= ? ");
		hql.append("and pp.expiryDate >=  ? ");
		hql.append("order by pp.displayOrder, pp.id.planId ");
		Object[] parameters = new Object[] { productId, Constantes.PLAN_TYPE_RISK, refDate, refDate };
		
		List<ProductPlan> productPlans = getHibernateTemplate().find(hql.toString(), parameters);
		for (ProductPlan productPlan : productPlans)
			productPlan.setPlanRiskTypes(listPlanRiskTypeByRefDate(productId, productPlan.getId().getPlanId(), refDate));
		
		return productPlans;
	}

	public List<ProductPlan> listPersonalRiskPlanByProductId(int productId, Date refDate) {
		StringBuilder hql = new StringBuilder();
		hql.append("select pp from ProductPlan pp, Product prod, Plan pl ");
		hql.append("where pp.id.planId = pl.planId ");
		hql.append("and pp.id.productId = prod.productId ");
		hql.append("and prod.productId  = ? ");
		hql.append("and pp.plan.planType = ?");
		hql.append("and pp.id.effectiveDate <= ? ");
		hql.append("and pp.expiryDate >=  ? ");
		hql.append("order by pp.displayOrder, pp.id.planId ");
		Object[] parameters = new Object[] { productId, Constantes.PLAN_TYPE_RISK, refDate, refDate };
		
		List<ProductPlan> listPlan = getHibernateTemplate().find(hql.toString(), parameters); 
		
		for ( ProductPlan productPlan : listPlan ) {
			Criteria crit = getSession().createCriteria(PlanPersonalRisk.class);
			
			crit.add( Restrictions.eq( "id.productId", productPlan.getId().getProductId() ) );
			crit.add( Restrictions.eq( "id.planId", productPlan.getId().getPlanId() ) );
			crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
			crit.add( Restrictions.ge( "expiryDate", refDate ) );
			
			List <PlanPersonalRisk> listPlanPersonalRisk = crit.list();
			for ( PlanPersonalRisk planPersonalRisk : listPlanPersonalRisk ) {
				productPlan.setQuantityPersonalRisk( planPersonalRisk.getQuantityPersonalRisk() );
				productPlan.setQuantityAdditional( planPersonalRisk.getQuantityAdditional() );
				productPlan.setAdditionalNewborn( planPersonalRisk.isAdditionalNewborn() );
				break;
			}
		}
		return listPlan ;
	}

	public Product findProductToBeCopy(int productId, Date referenceDate) {

		String strSql = "";

		Product product = null;
		Object[] parameters = null;

		List<Product> productList = (List<Product>) getHibernateTemplate().find("From Product where productId = ?", productId);

		for (Product product2 : productList) {
			product = product2;
			break;
		}

		if (product == null) {
			return null;
		}

		// get the product version by productId
		strSql = "From ProductVersion where id.productId = ? and referenceDate = ?";
		parameters = new Object[] { productId, referenceDate };
		List<ProductVersion> productVersions = (List<ProductVersion>) getHibernateTemplate().find(strSql, parameters);
		product.setProductVersions(productVersions);

		// get the product plan by productId
		strSql = "From ProductPlan pp";
		strSql += " Where pp.id.productId = ?";
		strSql += " and pp.plan.planType = 177";
		strSql += " and pp.id.effectiveDate <= ?";
		strSql += " and pp.expiryDate >= ?";
		parameters = new Object[] { productId, referenceDate, referenceDate };
		List<ProductPlan> productPlans = (List<ProductPlan>) getHibernateTemplate().find(strSql, parameters);

		List<ProductPlan> productPlansWork = new ArrayList<ProductPlan>();
		List<CoveragePlan> coveragePlansWork = null;
		
		for (ProductPlan productPlan : productPlans) {

			coveragePlansWork = new ArrayList<CoveragePlan>();
			
			// get the CovergarePlan by productId and PlanId
			parameters = new Object[] { productPlan.getId().getProductId(), productPlan.getId().getPlanId(), referenceDate, referenceDate };
			
			strSql = "From CoveragePlan where id.productId = ? and id.planId = ?";
			strSql += " and id.effectiveDate <= ?";
			strSql += " and expiryDate >= ?";
			List<CoveragePlan> coveragePlans = (List<CoveragePlan>) getHibernateTemplate().find(strSql, parameters);
			
			for (CoveragePlan coveragePlan : coveragePlans) {
				//for each coveragePlan we find a CoverageRangeValues
				parameters = new Object[] { coveragePlan.getId().getProductId(), 
											coveragePlan.getId().getPlanId(), 
											coveragePlan.getId().getCoverageId(),
											coveragePlan.getId().getEffectiveDate()};

				strSql = "From CoverageRangeValue";
				strSql += " where id.productId = ?";
				strSql += " and id.planId = ?";
				strSql += " and id.coverageId = ?";
				strSql += " and id.effectiveDate <= ?";
				
				List<CoverageRangeValue> coverageRangeValues = (List<CoverageRangeValue>)getHibernateTemplate().find(strSql, parameters);				
				
				if(coverageRangeValues.size() > 0){
					coveragePlan.setCoverageRangeValues(coverageRangeValues);
				}
				
				//for each coverage plan we find and set the list of CoveragePersonalRisk
				parameters = new Object[] { coveragePlan.getId().getProductId(), 
											coveragePlan.getId().getPlanId(), 
											coveragePlan.getId().getCoverageId(),
											coveragePlan.getId().getEffectiveDate()};

				
				
				strSql = "From CoveragePersonalRisk";
				strSql += " where id.productId = ?";
				strSql += " and id.planId = ?";
				strSql += " and id.coverageId = ?";
				strSql += " and id.effectiveDate <= ?";				
				
				List<CoveragePersonalRisk> coveragePersonalRisks = (List<CoveragePersonalRisk>)getHibernateTemplate().find(strSql, parameters);				
				
				if (coveragePersonalRisks.size() > 0 ){
					coveragePlan.setCoveragePersonalRisks(coveragePersonalRisks);
				}
				
				coveragePlansWork.add(coveragePlan);
			}
			
			productPlan.setCoveragePlans(coveragePlansWork);
			productPlansWork.add(productPlan);
		}

		product.setProductPlans(productPlansWork);

		return product;
	}

	private final static String LINHA_PRODUCT_PER_PARTNER = "select p.* from product p inner join masterpolicy mp on ( p.productId = mp.productId) and ( mp.partnerId=:partnerId )";

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoProduct#findProductsPerPartner(int)
	 */
	public List<Product> findProductsPerPartner(int partnerId) {
		return getSession().createSQLQuery(LINHA_PRODUCT_PER_PARTNER).addEntity(Product.class).setInteger("partnerId", partnerId).list();
	}

	@Override
	public ProductPlan getPlanById(int productId, int planId, Date refDate) {
		Criteria crit = getSession().createCriteria(ProductPlan.class);
		crit.createAlias("plan", "_plan");
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", planId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		crit.add( Restrictions.eq( "_plan.planType", Constantes.PLAN_TYPE_COVERAGE) );
		ProductPlan productPlan = (ProductPlan) crit.uniqueResult();
		
		return productPlan;	
	}

	@Override
	public ProductPlan getRiskPlanById(int productId, int planId, Date refDate) {
		Criteria crit = getSession().createCriteria(ProductPlan.class);
		crit.createAlias("plan", "_plan");
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", planId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		crit.add( Restrictions.eq( "_plan.planType", Constantes.PLAN_TYPE_RISK ) );
		
		ProductPlan productPlan = (ProductPlan) crit.uniqueResult();
		if (productPlan != null)
			productPlan.setPlanRiskTypes(listPlanRiskTypeByRefDate(productId, planId, refDate));
		
		return productPlan;	
	}
	
	public List <CoverageRangeValue> listCoverageRangeValueByRefDate(int productId, int coveragePlanId, int coverageId ,Date refDate){
		Criteria crit = getSession().createCriteria(CoverageRangeValue.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", coveragePlanId ) );
		crit.add( Restrictions.eq( "id.coverageId", coverageId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		crit.addOrder(Order.asc("displayOrder"));
		
		List <CoverageRangeValue> list = crit.list();
		
		return list;
	}

	public ProductTerm getProductTermById(int productId, int termId, Date refDate){
		Criteria crit = getSession().createCriteria(ProductTerm.class);
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.termId", termId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		return (ProductTerm) crit.uniqueResult();
	}

	public ProductPlan getProductPlanById(int productId, int planId, Date refDate){
		Criteria crit = getSession().createCriteria(ProductPlan.class);
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", planId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		return (ProductPlan) crit.uniqueResult();
	}

	public CoverageRangeValue getCoverageRangeValueById(int productId, int coveragePlanId, int coverageId, int rangeId, Date refDate){
		Criteria crit = getSession().createCriteria(CoverageRangeValue.class);
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", coveragePlanId ) );
		crit.add( Restrictions.eq( "id.coverageId", coverageId ) );
		crit.add( Restrictions.eq( "id.rangeId", rangeId ) );
		crit.add( Restrictions.eq( "id.rangeValueType", CoverageRangeValue.RANGE_VALUE_TYPE_LMI ) );	
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		return (CoverageRangeValue) crit.uniqueResult();
	}

	public CoverageRangeValue getCoverageRangeValueByInsuredValue(int productId, int coveragePlanId, int coverageId, double insuredValue, Date refDate){
		Criteria crit = getSession().createCriteria(CoverageRangeValue.class);
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", coveragePlanId ) );
		crit.add( Restrictions.eq( "id.coverageId", coverageId ) );
		crit.add( Restrictions.eq( "id.rangeValueType", CoverageRangeValue.RANGE_VALUE_TYPE_LMI ) );	
		crit.add( Restrictions.le( "startValue", insuredValue ) );
		Criterion eq1 = Restrictions.ge("endValue", insuredValue);
		Criterion eq2 = Restrictions.eq("endValue", 0d);
		crit.add(Restrictions.or(eq1, eq2));
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		crit.setMaxResults(1);		
		return (CoverageRangeValue) crit.uniqueResult();
	}

	public List <CoverageRangeValue> listCoverageFractionValueByInsuredValue(int productId, int coveragePlanId, int coverageId, double insuredValue, Date refDate){
		Criteria crit = getSession().createCriteria(CoverageRangeValue.class);
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", coveragePlanId ) );
		crit.add( Restrictions.eq( "id.coverageId", coverageId ) );
		crit.add( Restrictions.eq( "id.rangeValueType", CoverageRangeValue.RANGE_VALUE_TYPE_LMF ) );
		Criterion eq1 = Restrictions.le( "startValue", insuredValue );
		Criterion eq2 = Restrictions.le( "endValue", insuredValue );
		Criterion eq3 = Restrictions.ne( "endValue", 0d );
		crit.add(Restrictions.or(eq1, Restrictions.and(eq2, eq3)));
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		crit.addOrder(Order.asc("id.rangeId"));
		List <CoverageRangeValue> list = crit.list();
		return list;
	}	

	public List <PlanRiskType> listPlanRiskTypeByRefDate(int productId, int riskPlanId, Date refDate){
		Criteria crit = getSession().createCriteria(PlanRiskType.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", riskPlanId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		
		List <PlanRiskType> list = crit.list();
		
		return list;
	}

	public PlanRiskType getPlanRiskTypeById(int productId, int riskPlanId, int riskType, Date refDate){
		Criteria crit = getSession().createCriteria(PlanRiskType.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", riskPlanId ) );
		crit.add( Restrictions.eq( "id.riskType", riskType ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		crit.setMaxResults(1);
		return (PlanRiskType) crit.uniqueResult();
	}

	public List <PlanRiskType> listPlanRiskTypeDescriptionByRefDate(int productId, int riskPlanId, Date refDate){
		Criteria crit = getSession().createCriteria(PlanRiskType.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", riskPlanId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
			
		List <PlanRiskType> list = crit.list();
		
		for (PlanRiskType planRiskType : list) {
			planRiskType.setDescriptionRisk(daoDataOption.getDataOption(planRiskType.getId().getRiskType()).getFieldDescription());
			planRiskType.setDescriptionRiskId(daoDataOption.getDataOption(planRiskType.getId().getRiskType()).getDataOptionId());
		}
		
		return list;
	}

	/**
	 * Gets a list of planKingShip
	 * @param productId Product Identificator
	 * @param planId  Plan Identificator
	 * @param renewalType Renewal Type
	 * @param refDate Reference Date
	 * @return PlanKinShip 
	 */
	public List<PlanKinship> listPlanKinshipByRiskPlan(int productId, int riskPlanId, int renewalType, Date refDate){
		Criteria crit = getSession().createCriteria(PlanKinship.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", riskPlanId ) );
		crit.add( Restrictions.eq( "id.renewalType", renewalType));
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );

		crit.addOrder( Order.desc( "id.effectiveDate" ) );
		
		List <PlanKinship> list = crit.list();
		
		for ( PlanKinship planKinship : list ) {
			
			DataOption option = ( DataOption ) getSession().get( DataOption.class, planKinship.getId().getKinshipType() );
			
			if ( option != null ) {
				planKinship.setTransientKinshipDescription( option.getFieldDescription() );
			}
			
		}
		
		return list;
	}

	public PlanKinship listPlanKinshipByPersonalRiskType(int productId, int riskPlanId, int personalRiskType, int renewalType, Date refDate){
		Criteria crit = getSession().createCriteria(PlanKinship.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", riskPlanId ) );
		crit.add( Restrictions.eq( "id.personalRiskType", personalRiskType ) );
		crit.add( Restrictions.eq( "id.renewalType", renewalType));
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		
		return (PlanKinship) crit.uniqueResult();
	}	

	public List <CoveragePersonalRisk> listCoveragePersonalRiskByPersonalRiskType(int productId, int coveragePlanId,int personalRiskType, Date refDate){
		Criteria crit = getSession().createCriteria(CoveragePersonalRisk.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", coveragePlanId ) );
		crit.add( Restrictions.eq( "id.personalRiskType", personalRiskType ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		
		List <CoveragePersonalRisk> list = crit.list();
		
		return list;
	}		
	
	/**
	 * @param productId
	 * @param referenceDate
	 * 
	 * @return {@link ProductOption} uniqueResult
	 * 
	 * **/
	public ProductOption getProduct(int productId, Date referenceDate) {
		Query query = getSession().getNamedQuery("getProductByProductIdReferenceDate");
		query.setResultTransformer(Transformers.aliasToBean(ProductOption.class));
		query.setParameter("productId", productId);
		query.setParameter("referenceDate", referenceDate);
		List<ProductOption> productOptions = (List<ProductOption>) query.list();
		for (ProductOption productOption : productOptions) {
			return productOption;
		}
		return null;
	}

	public Product getProductById(int productId) {
		Criteria crit = getSession().createCriteria(Product.class);
		crit.add(Restrictions.eq("productId", productId));
		crit.setMaxResults(1);
		return (Product) crit.uniqueResult();
	}

	public PlanPersonalRisk findPlanPersonalRisk(Date refDate, int personType, int planId, int productId) {

		Criteria crit = getSession().createCriteria(PlanPersonalRisk.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId",planId ) );
		crit.add( Restrictions.eq( "id.personType", personType ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		
		PlanPersonalRisk returnPlanPersonalRisk = (PlanPersonalRisk) crit.uniqueResult();		
		
		return returnPlanPersonalRisk;
	}
    /**
     * list all Product from the user
     * @param partnerId
     * @param productId
     * @param brokerId
     * @param userId
     * @return List<Product>
     */
	public List<Product> listProductPerPartnerOrBroker(Integer partnerId, Integer productId, Integer brokerId, Integer userId){
		Query query = super.getNamedQuery("selectProductPerPartnerOrBroker");
		query.setResultTransformer(Transformers.aliasToBean(Product.class));
		query.setParameter("partnerId", partnerId);
		query.setParameter("productId", productId);
		query.setParameter("brokerId", brokerId);
		query.setParameter("userId", userId);
		List<Product> listData = (List<Product>) query.list();

		return listData;
	}

	@Override
	public PlanKinship getPlanKinship(int productId, int riskPlan,
			Integer personalRiskType, Integer kinshipType, int renewalType,
			Date effectiveDate) {
		PlanKinshipId planKinshipId = new PlanKinshipId();
		planKinshipId.setProductId(productId);
		planKinshipId.setKinshipType(kinshipType);
		planKinshipId.setPersonalRiskType(personalRiskType);
		planKinshipId.setEffectiveDate(effectiveDate);
		planKinshipId.setPlanId(riskPlan);
		planKinshipId.setRenewalType(renewalType);
		return (PlanKinship) getSession().load(PlanKinship.class, planKinshipId);
	}

	@Override
	public PlanPersonalRisk getPlanPersonalRisk(int productId, int planId,
			Integer personType, Date effectiveDate) {
		PlanPersonalRiskId id = new PlanPersonalRiskId();
		id.setProductId(productId);
		id.setPlanId(planId);
		id.setPersonType(personType);
		id.setEffectiveDate(effectiveDate);
		return (PlanPersonalRisk) getSession().load(PlanPersonalRisk.class, id);
	}

	@Override
	public int getNextProductId() {
		Integer result = (Integer) getSession().createCriteria(Product.class).setProjection(Projections.max("productId")).uniqueResult();
		if (result == null)
			return 1;
		else
			return (result.intValue() + 1);
	}

	@Override
	public List<Product> listProduct(int objectID, Date refDate) {
		Criteria crit = getSession().createCriteria(Product.class);
		crit.add( Restrictions.eq("objectId", objectID));
		
		return crit.list();
	}

	@Override
	public List<ProductPlan> listProductPlan(int productID, Date versionDate) {
		Criteria crit = getSession().createCriteria(ProductPlan.class);
		crit.add( Restrictions.eq("id.productId", productID));
		crit.add( Restrictions.le("id.effectiveDate", versionDate));
		crit.add( Restrictions.ge( "expiryDate", versionDate ) );
		crit.addOrder( Order.asc( "id.effectiveDate" ) );
		
		return crit.list();
	}

	@Override
	public CoverageRangeValue getCoverageRangeValue(int productID, int planID, int coverageID, int rangeValueID, Date versionDate) {
		Criteria crit = getSession().createCriteria(CoverageRangeValue.class);
		crit.add( Restrictions.eq( "id.productId", productID ) );
		crit.add( Restrictions.eq( "id.planId", planID ) );
		crit.add( Restrictions.eq( "id.coverageId", coverageID ) );
		crit.add( Restrictions.eq( "id.rangeId", rangeValueID ) );
		crit.add( Restrictions.le( "id.effectiveDate", versionDate ) );
		crit.add( Restrictions.ge( "expiryDate", versionDate ) );
		crit.addOrder( Order.asc( "displayOrder" ) );
		
		return (CoverageRangeValue) crit.uniqueResult();
	}
}