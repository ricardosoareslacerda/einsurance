package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import br.com.tratomais.core.dao.IDaoDataOption;
import br.com.tratomais.core.model.product.DataOption;
import br.com.tratomais.core.model.product.IdentifiedList;

public class DaoDataOptionImpl extends DaoGenericImpl<DataOption, Integer>  implements IDaoDataOption{

	@SuppressWarnings("unchecked")
	public List< DataOption > findByName(String dataGroupId) {
		Criteria criteria = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(DataOption.class);
		criteria.add(Restrictions.eq("dataGroup.dataGroupId", dataGroupId));
		criteria.add(Restrictions.eq("active", true));
		criteria.addOrder(Order.asc("displayOrder"));
		
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List< DataOption > listDataOption(int dataGroupId) {
		Criteria criteria = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(DataOption.class);
		criteria.add(Restrictions.eq("dataGroup.dataGroupId", dataGroupId));
		criteria.add(Restrictions.eq("active", true));
		criteria.addOrder(Order.asc("displayOrder"));
		
		return criteria.list();
	}
	
	/**
	 * List Data Options by @see DataGroup 
	 * @param dataGroupId
	 * @param bringAll true/false (list all DataOption Active=true and Active=false)  
	 * @return List of @see DataGroup
	 */
	@SuppressWarnings("unchecked")
	public List< DataOption > listDataOption(int dataGroupId, boolean bringAll) {
		Criteria criteria = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(DataOption.class);
		criteria.add(Restrictions.eq("dataGroup.dataGroupId", dataGroupId));
		criteria.addOrder(Order.asc("displayOrder"));
		if(!bringAll){
			criteria.add(Restrictions.eq("active", true));
		}
		
		return criteria.list();
	}	
	
	/**
	 * List Data Options by @see DataGroup 
	 * @param dataGroupId
	 * @param bringAll true/false (list all DataOption Active=true and Active = false)
	 * @param externalCode  
	 * @return List of @see DataGroup
	 */
	@SuppressWarnings("unchecked")
	public List<DataOption> listDataOption(int dataGroupId, boolean bringAll, String externalCode) {
		Criteria criteria = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(DataOption.class);
		criteria.add(Restrictions.eq("dataGroup.dataGroupId", dataGroupId));
		criteria.add(Restrictions.eq("externalCode", externalCode));
		criteria.addOrder(Order.asc("displayOrder"));
		
		if(!bringAll){
			criteria.add(Restrictions.eq("active", true));
		}
		
		return criteria.list();
	}
	
	@Deprecated
	@SuppressWarnings("unchecked")
	public List<DataOption> listPaymentType(int dataGroupId, int productId, Date referenceDate){
		final String SQL =  
			"SELECT	DISTINCT \n"+
			"		dataOpt.dataOptionId, \n"+
			"		dataOpt.effectiveDate, \n"+
			"		dataOpt.expiryDate, \n"+
			"		dataOpt.fieldValue, \n"+
			"		dataOpt.fieldDescription, \n"+
			"		dataOpt.fieldExclusive, \n"+
			"		dataOpt.externalCode, \n"+
			"		dataOpt.description, \n"+
			"		dataOpt.displayOrder, \n"+
			"		dataOpt.registred, \n"+
			"		dataOpt.updated, \n"+
			"		dataOpt.active \n"+
			" FROM	ProductPaymentTerm prdPayment, \n"+
			"		PaymentTerm pmtTerm, \n"+
			"		DataOption dataOpt \n"+
			" WHERE	prdPayment.paymentTermId = pmtTerm.paymentTermId \n"+
			"   AND	pmtTerm.firstPaymentType = dataOpt.dataOptionId \n"+
			"   AND  dataOpt.dataGroupId = :dataGroupId \n"+
			"   AND	prdPayment.productId = :productId \n"+
			"   AND	:referenceDate between prdPayment.effectiveDate and prdPayment.expiryDate \n"+
			"   AND	:referenceDate between dataOpt.effectiveDate and dataOpt.expiryDate \n";
		
		SQLQuery query = getSession().createSQLQuery(SQL);
		
		query.setInteger("dataGroupId", dataGroupId)
			 .setInteger("productId", productId) 
			 .setDate("referenceDate", referenceDate);
	 	
		query.setResultTransformer( Transformers.aliasToBean( DataOption.class ) );
		List<DataOption> myList = query.list();
		
		return myList;		
	}

	@SuppressWarnings("unchecked")
	public IdentifiedList findIdentifiedListDataOptionByDataGroup(int dataGroupId, String identifierString, Date refDate){
		Date data = (refDate == null)?new Date():refDate;
		Criteria criteria = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(DataOption.class);
		criteria.add(Restrictions.eq("dataGroup.dataGroupId", dataGroupId));
		criteria.add(Restrictions.le("effectiveDate", data));
		criteria.add(Restrictions.ge("expiryDate", data));
		criteria.add(Restrictions.eq("active", true));
		criteria.addOrder(Order.asc("displayOrder"));
		List<DataOption> myList = criteria.list();
		IdentifiedList retorno = new IdentifiedList();
		retorno.setGroupId(identifierString);
		retorno.setResultArray(myList);
		return retorno;		
	}

	@SuppressWarnings("unchecked")
	public IdentifiedList findIdentifiedListDataOptionByDataGroupParentId(int dataGroupId, int parentId, String identifierString, Date refDate){
		Date data = (refDate == null)?new Date():refDate;
		Criteria criteria = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(DataOption.class);
		criteria.add(Restrictions.eq("dataGroup.dataGroupId", dataGroupId));
		criteria.add(Restrictions.eq("parent.dataOptionId", parentId));
		criteria.add(Restrictions.le("effectiveDate", data));
		criteria.add(Restrictions.ge("expiryDate", data));
		criteria.add(Restrictions.eq("active", true));
		criteria.addOrder(Order.asc("displayOrder"));
		List<DataOption> myList = criteria.list();
		IdentifiedList retorno = new IdentifiedList();
		retorno.setGroupId(identifierString);
		retorno.setResultArray(myList);
		return retorno;		
	}
	
	@SuppressWarnings("unchecked")
	public List<DataOption> listDataOptionByIdAndParent(int dataGroupId, int parentId) {
		Criteria criteria = getSession().createCriteria(DataOption.class);
		criteria.createAlias("dataGroup", "dgp");
		criteria.add(Restrictions.eq("dgp.dataGroupId", dataGroupId));
		criteria.add(Restrictions.eq("parent.dataOptionId", parentId));
		criteria.add(Restrictions.eq("active", true));
		criteria.addOrder(Order.asc("displayOrder"));
		return criteria.list();
	}
	
	public DataOption getDataOption(int dataOptionId){
		Session session = getSession();
		return (DataOption) session.get(DataOption.class, dataOptionId);
	}

	public DataOption getDataOption(Integer dataOptionId){
		if (dataOptionId != null)
			return this.getDataOption(dataOptionId.intValue());
		return null;
	}

	public DataOption getDataOption(int dataGroupId, Integer dataOptionId){
		Criteria criteria = getSession().createCriteria(DataOption.class);
		criteria.createAlias("dataGroup", "_dataGroup");
		criteria.add(Restrictions.eq("_dataGroup.dataGroupId", dataGroupId));
		criteria.add(Restrictions.eq("dataOptionId", dataOptionId));
		return (DataOption) criteria.uniqueResult();
	}
	
	@Override
	public DataOption getByExternalCode(int dataGroupId, String externalCode) {
		Criteria criteria = getSession().createCriteria(DataOption.class);
		criteria.add(Restrictions.eq("dataGroup.dataGroupId", dataGroupId));
		criteria.add(Restrictions.eq("externalCode", externalCode.trim()));
		return (DataOption) criteria.uniqueResult();
	}

	@Override
	public DataOption getByFieldName(String fieldName, int dataOptionId) {
		Criteria criteria = getSession().createCriteria(DataOption.class);
		criteria.createAlias("dataGroup", "_dataGroup");
		criteria.add(Restrictions.eq("_dataGroup.fieldName", fieldName));
		criteria.add(Restrictions.eq("dataOptionId", dataOptionId));
		return (DataOption) criteria.uniqueResult();
	}

	@Override
	public DataOption getByFieldName(String fieldName, int dataOptionId, int parentId) {
		Criteria criteria = getSession().createCriteria(DataOption.class);
		criteria.createAlias("dataGroup", "_dataGroup");
		criteria.add(Restrictions.eq("_dataGroup.fieldName", fieldName));
		criteria.add(Restrictions.eq("parent.dataOptionId", parentId));
		criteria.add(Restrictions.eq("dataOptionId", dataOptionId));
		return (DataOption) criteria.uniqueResult();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<DataOption> listDataOptionByRefDate(int dataGroupID, Date refDate) {
		Criteria criteria = getSession().createCriteria(DataOption.class);
		criteria.add(Restrictions.eq("dataGroup.dataGroupId", dataGroupID));
		criteria.add(Restrictions.eq("active", true));
		criteria.addOrder(Order.asc("displayOrder"));
		
		return criteria.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<DataOption> listDataOptionByFieldValue(String fieldValue, Date refDate) {
		Criteria criteria = getSession().createCriteria(DataOption.class);
		criteria.createAlias("dataGroup", "_dataGroup");
		criteria.add(Restrictions.eq("_dataGroup.fieldName", fieldValue));
		criteria.add(Restrictions.le("effectiveDate", refDate));
		criteria.add(Restrictions.ge("expiryDate", refDate));
		criteria.add(Restrictions.eq("active", true));
		criteria.addOrder(Order.asc("displayOrder"));
		
		return criteria.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<DataOption> listDataOptionByParentID(int parentID, Date refDate) {
		Criteria criteria = getSession().createCriteria(DataOption.class);
		criteria.add(Restrictions.eq("parent.dataOptionId", parentID));
		criteria.add(Restrictions.le("effectiveDate", refDate));
		criteria.add(Restrictions.ge("expiryDate", refDate));
		criteria.add(Restrictions.eq("active", true));
		criteria.addOrder(Order.asc("displayOrder"));
		
		return criteria.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<DataOption> listDataOptionByParentID(String fieldValue, int parentID, Date refDate) {
		Criteria criteria = getSession().createCriteria(DataOption.class);
		criteria.createAlias("dataGroup", "_dataGroup");
		criteria.add(Restrictions.like("_dataGroup.fieldName", "%"+fieldValue.trim()+"%"));
		criteria.add(Restrictions.eq("parent.dataOptionId", parentID));
		criteria.add(Restrictions.le("effectiveDate", refDate));
		criteria.add(Restrictions.ge("expiryDate", refDate));
		criteria.add(Restrictions.eq("active", true));
		criteria.addOrder(Order.asc("displayOrder"));
		
		return criteria.list();
	}
}