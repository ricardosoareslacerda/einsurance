package br.com.tratomais.core.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import br.com.tratomais.core.dao.IDaoCollectionMain;
import br.com.tratomais.core.model.report.CollectionDetailsMainParameters;
import br.com.tratomais.core.model.report.CollectionMainParameters;
import br.com.tratomais.core.model.report.CollectionDetailsMainResult;
import br.com.tratomais.core.model.report.BillingMethodResult;
import br.com.tratomais.core.model.report.CollectionMainResult;
import br.com.tratomais.core.model.report.StatusLote;
import br.com.tratomais.core.util.DateUtil;
import br.com.tratomais.core.util.HibernateUtil;

public class DaoCollectionMainImpl extends DaoGenericImpl<CollectionDetailsMainResult, Integer> implements IDaoCollectionMain{
	
	public StringBuilder queryListCollectionDetailsMain(){
		StringBuilder strSql = new StringBuilder("SELECT (SELECT FieldDescription FROM DataOption d2 WHERE d2.DataOptionId = fd.FileLotDetailsStatus) AS detailsStatus " +  
				 ", fd.lineNumber, i.clientReference, i.billingReference, i.installmentValue AS installmentValue, d.fieldDescription AS RecordType " +
				 ", fd.responseCode, fd.responseDescription, fd.textLine, fd.errorMessage, fd.fileLotDetailsStatus FROM Lot l LEFT JOIN FileLot f ON l.lotid = f.lotid " +   
				 " LEFT JOIN FileLotDetails fd ON fd.FileLotId = f.FileLotId LEFT JOIN Installment i ON i.InstallmentId = fd.InstallmentId LEFT JOIN DataOption d "+ 
				 " ON d.DataOptionId = fd.RecordType ");

		strSql.append(" WHERE l.lotId = ?");
		strSql.append(" ORDER BY fd.lineNumber");
		
		return strSql;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CollectionDetailsMainResult> listCollectionDetailsMain(CollectionDetailsMainParameters collectionDetailsMainParameters) {
		Session session = HibernateUtil.getSession();
		Query query;
		
		query = session.createSQLQuery(queryListCollectionDetailsMain().toString());
		
		if(collectionDetailsMainParameters.getTotalPage() > 0){
			query.setFirstResult(collectionDetailsMainParameters.getPage());
			query.setMaxResults(collectionDetailsMainParameters.getTotalPage());
		}
		
		
		query.setInteger(0, collectionDetailsMainParameters.getLotId());
		query.setResultTransformer(Transformers.aliasToBean(CollectionDetailsMainResult.class));
		
		return query.list();
	}

	@Override
	public int listCollectionDetailsMainTotalRecords(CollectionDetailsMainParameters collectionDetailsMainParameters) {
		Session session = HibernateUtil.getSession();
		Query query;
		
		query = session.createSQLQuery(queryListCollectionDetailsMain().toString());
		
		query.setInteger(0, collectionDetailsMainParameters.getLotId());
		query.setResultTransformer(Transformers.aliasToBean(CollectionDetailsMainResult.class));
		
		return query.list().size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BillingMethodResult> listBillingMethod() {
		Session session = HibernateUtil.getSession();
		Query query = session.createSQLQuery("SELECT     Exchange.exchangeId, ChartAccount.chartAccountId, ChartAccount.externalCode as name " + 
											 "FROM         ChartAccount INNER JOIN Exchange ON ChartAccount.ChartAccountID = Exchange.ChartAccountID " +
											 "WHERE     (Exchange.ExchangeType = 596)");
		query.setResultTransformer(Transformers.aliasToBean(BillingMethodResult.class));
		return query.list();
	}
	
	public StringBuilder queryListCollectionMain(CollectionMainParameters collectionMainParameters){
		StringBuilder strSql = new StringBuilder("SELECT l.lotId, d.fieldDescription, l.lotStatus, l.lotCode, l.fileName, l.sendDate, " +
				 " l.recieveDate, l.recordsQuantity as quantity, l.recordsAmount as amount, l.errorMessage " + 
				 " FROM lot l LEFT JOIN dataoption d ON d.dataoptionid = l.lotstatus ");
		
		if(collectionMainParameters.getLotId() > 0){
			strSql.append(" WHERE l.parentId = " + collectionMainParameters.getLotId());
		} else {
		
			strSql.append(" WHERE l.exchangeId = ? ");
			strSql.append(" AND l.lotType = ? ");
			
			if(collectionMainParameters.getStatusId() > 0)
				strSql.append("AND l.lotStatus = " + collectionMainParameters.getStatusId());
			
			if(collectionMainParameters.getLotCode().trim() != "")
				strSql.append(" AND l.LotCode = '" + collectionMainParameters.getLotCode() + "'");
			
			if( collectionMainParameters.getEffectiveDate() != null && collectionMainParameters.getExpiryDate() != null){
				strSql.append(" AND l.sendDate BETWEEN '" +  DateUtil.getDateTime(collectionMainParameters.getEffectiveDate(), "yyyy-MM-dd") + "'");
				strSql.append(" AND '" + DateUtil.getDateTime(collectionMainParameters.getExpiryDate(), "yyyy-MM-dd") + "'");
			}
			
			strSql.append("	AND EXISTS ( SELECT 1 FROM fileLot fl, fileLotDetails fld, installment i WHERE fl.lotId = l.lotId AND fld.fileLotId = fl.fileLotId AND i.installmentId = fld.installmentId ");
			
			if(collectionMainParameters.getClientReference().trim() != "")
				strSql.append(" AND i.ClientReference = '" + collectionMainParameters.getClientReference()+ "'");
			
			if(collectionMainParameters.getBillingReference().trim() != "")
				strSql.append(" AND i.BillingReference = '" + collectionMainParameters.getBillingReference() + "'");		
			
			strSql.append(")");
		}
		
		strSql.append(" ORDER BY l.lotCode, l.lotId");	
		
		return strSql;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CollectionMainResult> listCollectionMain(CollectionMainParameters collectionMainParameters) {
		Session session = HibernateUtil.getSession();
		
		Query query = session.createSQLQuery(queryListCollectionMain(collectionMainParameters).toString());
		
		if(collectionMainParameters.getTotalPage() > 0){
			query.setFirstResult(collectionMainParameters.getPage());
			query.setMaxResults(collectionMainParameters.getTotalPage());
		}		
		
		if(collectionMainParameters.getLotId() == 0){
			query.setInteger(0, collectionMainParameters.getExchangeId());
			query.setInteger(1, collectionMainParameters.getLotType());
		}
		
		query.setResultTransformer(Transformers.aliasToBean(CollectionMainResult.class));
		
		return query.list();
	}

	@Override
	public int listCollectionMainTotalRecords(CollectionMainParameters collectionMainParameters) {
		Session session = HibernateUtil.getSession();
		
		Query query = session.createSQLQuery(queryListCollectionMain(collectionMainParameters).toString());
		
		if(collectionMainParameters.getLotId() == 0){		
			query.setInteger(0, collectionMainParameters.getExchangeId());
			query.setInteger(1, collectionMainParameters.getLotType());
		}
		
		query.setResultTransformer(Transformers.aliasToBean(CollectionMainResult.class));
		
		return query.list().size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StatusLote> listStatusLote() {
		Session session = HibernateUtil.getSession();
 
		Query query = session.createSQLQuery("SELECT fieldDescription, dataOptionId as statusId FROM DataOption WHERE DataGroupId = 62");
		query.setResultTransformer(Transformers.aliasToBean(StatusLote.class));
	
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BillingMethodResult> listExchangeCollection() {
		Session session = HibernateUtil.getSession();
		Query query = session.createSQLQuery("SELECT Exchange.exchangeId, Exchange.exchangeType, Exchange.chartAccountId, Exchange.name " + 
											 "  FROM Exchange ");
		query.setResultTransformer(Transformers.aliasToBean(BillingMethodResult.class));
		return query.list();
	}
}