package br.com.tratomais.core.dao.impl;

import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoConversionRate;
import br.com.tratomais.core.model.product.ConversionRate;

public class DaoConversionRateImpl extends DaoGenericImpl< ConversionRate, Integer > implements IDaoConversionRate{

	public ConversionRate findConversionRateByRefDate(Integer currencyId, Date refDate){
		if(refDate==null) {
			refDate = new Date();
		}
		Criteria criteria = getSession().createCriteria(ConversionRate.class);
		criteria.add(Restrictions.eq("currencyId", currencyId));
		criteria.add(Restrictions.le("currencyDate", refDate));
		criteria.addOrder(Order.desc("currencyDate"));
		criteria.setMaxResults(1);
		return (ConversionRate) criteria.uniqueResult();
	}
}