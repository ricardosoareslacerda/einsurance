package br.com.tratomais.core.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.model.pendencies.LockPendency;
import br.com.tratomais.core.model.pendencies.LockPendencyId;
import br.com.tratomais.core.model.pendencies.Pendency;

public class DaoLockPendency extends DaoGenericImpl<LockPendency, LockPendencyId> {
	private Logger logger = Logger.getLogger(DaoLockPendency.class);
	
	@Override
	public LockPendency save(LockPendency entity) {
		try {
			if (entity.getId().getLockID() == null)
				entity.getId().setLockID(getNextLockId(entity.getId()));
			LockPendency newObject = null;
			
			newObject =(LockPendency) super.getHibernateTemplate().merge(entity);
			
			return newObject;
		} catch (final HibernateException ex) {
			logger.error(ex);
			throw new HibernateException(ex);
		}
	}

	private int getNextLockId(LockPendencyId id) {
		Integer result = (Integer) getSession().createCriteria(LockPendency.class)
			.add(Restrictions.eq("id.endorsementID", id.getEndorsementID()))
			.add(Restrictions.eq("id.contractID", id.getContractID()))
			.setProjection(Projections.max("id.lockID")).uniqueResult();
		if (result == null)
			return 1;
		else
			return result.intValue() + 1;
	}
	
	/**
	 * @param contractId Contract identificator
	 * @param endorsementId Endorsement Identificator 
	 * @param itemId Item Identificator 
	 * @return List of Lock Pendencies
	 */
	@SuppressWarnings("unchecked")
	public List<LockPendency> findLockPendencies(int endorsementId, int contractId, Integer itemId, int pendencyId){
		Criteria criteria = getSession().createCriteria(LockPendency.class);
		criteria.add(Restrictions.eq("id.contractID", contractId));
		criteria.add(Restrictions.eq("id.endorsementID", endorsementId));
		if (itemId != null)
			criteria.add(Restrictions.eq("itemID", itemId));
		criteria.add(Restrictions.eq("pendencyID", pendencyId));	
		return criteria.list();
	}
	
	/**
	 * @param contractId Contract identificator
	 * @param endorsementId Endorsement Identificator 
	 * @return List of Lock Pendencies
	 */
	@SuppressWarnings("unchecked")
	public List<LockPendency> findLockPendencies(int endorsementId, int contractId, int pendencyId){
		Criteria criteria = getSession().createCriteria(LockPendency.class);
		criteria.add(Restrictions.eq("id.contractID", contractId));
		criteria.add(Restrictions.eq("id.endorsementID", endorsementId));
		criteria.add(Restrictions.eq("pendencyID", pendencyId));	
		return criteria.list();
	}

	/**
	 * @param contractId Contract identificator
	 * @param endorsementId Endorsement Identificator 
	 * @return List of Lock Pendencies
	 */
	@SuppressWarnings("unchecked")
	public List<LockPendency> findLockPendencies(int endorsementId, int contractId){
		Criteria criteria = getSession().createCriteria(LockPendency.class);
		criteria.add(Restrictions.eq("id.contractID", contractId));
		criteria.add(Restrictions.eq("id.endorsementID", endorsementId));
		return criteria.list();
	}

	/**
	 * Lists Pendencies to endorsement
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Pendency> findPendencyList(){
		Criteria criteria = getSession().createCriteria(Pendency.class);
		criteria.add(Restrictions.le("id", 2));
		return criteria.list();
	}
	
	/**
	 * 
	 * @param contractId
	 * @param endorsementId
	 * @param itemId
	 * @param pendencyId
	 * @return
	 */
	public LockPendency findPendency(int contractId, int endorsementId, Integer itemId, int pendencyId){
		Criteria criteria = getSession().createCriteria(LockPendency.class);
		criteria.add(Restrictions.eq("id.contractID", contractId));
		criteria.add(Restrictions.eq("id.endorsementID", endorsementId));
		if (itemId != null)
			criteria.add(Restrictions.eq("itemID", itemId));
		criteria.add(Restrictions.eq("pendencyID", pendencyId));
		return (LockPendency) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public void deleteUntouchedPendencies(int contractId, int endorsementId) {
		Session session = getSession();
		Criteria criteriaPendency = session.createCriteria(LockPendency.class);
		criteriaPendency.add(Restrictions.eq("id.contractID", contractId));
		criteriaPendency.add(Restrictions.eq("id.endorsementID", endorsementId));
		criteriaPendency.add(Restrictions.eq("lockPendencyStatus", LockPendency.LOCK_PENDENCY_STATUS_PENDENT));
		List<LockPendency> pendencies = criteriaPendency.list();
		Query queryHistory = getSession().createQuery("delete from HistoryPendency where (id.contractID = :contractID) and (id.endorsementID = :endorsementID) and (id.lockID = :lockID)");
		for(LockPendency pendency:pendencies){
			queryHistory.setInteger("contractID", pendency.getId().getContractID());
			queryHistory.setInteger("endorsementID", pendency.getId().getEndorsementID());
			queryHistory.setInteger("lockID", pendency.getPendencyID());
			queryHistory.executeUpdate();
			session.delete(pendency);
		}
	}

	/**
	 * M�todo que retorna a lista de Pendencias da tabela
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Pendency> listPendencyList(int objectId){
		Criteria criteria = getSession().createCriteria(Pendency.class);
		criteria.add(Restrictions.eq("active", true));
		Criterion eq1 = Restrictions.eq("objectID", objectId);
		Criterion eq2 = Restrictions.isNull("objectID");
		criteria.add(Restrictions.or(eq1, eq2));
		return criteria.list();
	}
}
