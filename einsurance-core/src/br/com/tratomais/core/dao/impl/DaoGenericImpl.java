package br.com.tratomais.core.dao.impl;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import br.com.tratomais.core.dao.IDaoGeneric;
import br.com.tratomais.core.dao.PersistentEntity;

@SuppressWarnings("unchecked")
public abstract class DaoGenericImpl<T extends PersistentEntity, ID extends Serializable> extends HibernateDaoSupport implements IDaoGeneric<T, ID> {

	private static Log LOG = LogFactory.getLog(DaoGenericImpl.class);

	public DaoGenericImpl() {
		super();
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	public DaoGenericImpl(Class<T> clazz) {
		super();
		this.persistentClass = clazz;
	}

	private Class<T> persistentClass;

	public Class<T> getPersistentClass() {
		return this.persistentClass;
	}

	public void setPersistentClass(Class<T> clazz) {
		this.persistentClass = clazz;
	}

	public void delete(T entity) {
		try {
			super.getHibernateTemplate().delete(entity);
		} catch (final HibernateException ex) {
			DaoGenericImpl.LOG.error(ex);
		}
	}

	public T findById(ID id) {
		try {
			return (T) super.getHibernateTemplate().get(getPersistentClass(), id);
		} catch (final HibernateException ex) {
			DaoGenericImpl.LOG.error(ex);
			throw new HibernateException(ex);
		}
	}

	public List<T> listAll() {
		try {
			return super.getHibernateTemplate().loadAll(getPersistentClass());
		} catch (final HibernateException ex) {
			DaoGenericImpl.LOG.error(ex);
			throw new HibernateException(ex);
		}
	}

	public List<T> listAllActive() {
		try {
			Criteria crit = this.createCriteria();
			BeanInfo beanInfo = Introspector.getBeanInfo(getPersistentClass());
			for (PropertyDescriptor propertyDescriptor : beanInfo.getPropertyDescriptors()) {
				if (propertyDescriptor.getName().equals("active")) {
					crit.add(Restrictions.eq("active", true));
					break;
				}
			}
			return crit.list();
		} catch (final HibernateException ex) {
			DaoGenericImpl.LOG.error(ex);
			throw new HibernateException(ex);
		} catch (IntrospectionException e) {
			DaoGenericImpl.LOG.error(e);
			throw new HibernateException(e);
		}
	}

	public T save(T entity) {
		try {
			Object newObject = null;
			
			newObject = super.getHibernateTemplate().merge(entity);
			
			return (T) newObject;
		} catch (final HibernateException ex) {
			DaoGenericImpl.LOG.error(ex);
			throw new HibernateException(ex);
		}
	}

	protected List<T> findByCriteria(Criterion... criterion) {
		try {
			Criteria crit = this.createCriteria();
			for (Criterion c : criterion) {
				crit.add(c);
			}
			return crit.list();
		} catch (final HibernateException ex) {
			DaoGenericImpl.LOG.error(ex);
			throw new HibernateException(ex);
		}
	}

	protected T findUniqueEntityByQuery(String query, Object... parameter) {
		List<T> list = super.getHibernateTemplate().find(query, parameter);
		if (list.size() > 1) {
			// /throw new NonUniqueObjectException("Non unique object", ID, persistentClass.getClass());
		}
		if (list.size() == 0) {
			return null;
		}
		return (T) list.get(0);
	}

	public Criteria createCriteria() {
		return super.getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(getPersistentClass());
	}

	public Query getNamedQuery(String name) {
		return super.getHibernateTemplate().getSessionFactory().getCurrentSession().getNamedQuery(name);
	}

	public Query getQuery(String query) {
		return super.getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(query);
	}
}