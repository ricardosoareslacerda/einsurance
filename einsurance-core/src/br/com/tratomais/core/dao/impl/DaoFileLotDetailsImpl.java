package br.com.tratomais.core.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import br.com.tratomais.core.dao.IDaoFileLotDetails;
import br.com.tratomais.core.model.interfaces.FileLotDetails;
import br.com.tratomais.core.model.paging.Paginacao;
import br.com.tratomais.core.model.product.IdentifiedList;
import br.com.tratomais.core.util.PagingParameters;

public class DaoFileLotDetailsImpl extends DaoGenericImpl<FileLotDetails, Integer> implements IDaoFileLotDetails{

	@Override
	public FileLotDetails getFileLotDetails(Integer fileLotDetailsId, Integer fileLotId) {
		Criteria criteria = getSession().createCriteria(FileLotDetails.class);
		criteria.add(Restrictions.eq("fileLotDetailsID", fileLotDetailsId));
		criteria.add(Restrictions.eq("fileLot.fileLotID", fileLotId));
		
		return (FileLotDetails) criteria.uniqueResult(); 
	}

	@SuppressWarnings("unchecked")
	@Override
	public IdentifiedList listFileLotDetails(IdentifiedList identifiedList, Integer fileLotId) {
		StringBuilder sql = new StringBuilder("SELECT f.fileLotDetailsID, CONVERT (c.policyNumber, CHAR) as policyNumber, CONVERT (c.certificateNumber, CHAR ) as certificateNumber, ");
		sql.append("f.contractID, f.endorsementID, c.subsidiaryID, StatusOption.fieldDescription AS statusDescription, f.installmentID, ");
		sql.append("f.lineNumber, f.textLine, f.errorMessage, f.fileLotDetailsStatus FROM Contract c LEFT JOIN ");
		sql.append("Endorsement e ON c.ContractID = e.ContractID ");
		sql.append("right join FileLotDetails f ON e.ContractID = f.ContractID and e.EndorsementID = f.EndorsementID ");
		sql.append("LEFT JOIN DataOption AS StatusOption ON f.FileLotDetailsStatus = StatusOption.DataOptionID ");
		sql.append("WHERE f.FileLotID = " + fileLotId);
		sql.append(" ORDER BY f.LineNumber, f.FileLotDetailsID");

		SQLQuery query = getSession().createSQLQuery(sql.toString());
		
		PagingParameters prm = (PagingParameters) identifiedList.getValue();

		Paginacao paginacao = new Paginacao();
		paginacao.setTotalDados(query.list() == null? 0 : query.list().size());
		
		if(prm.getTotalPage() > 0){
			query.setFirstResult(prm.getPage());
			query.setMaxResults(prm.getTotalPage());
		}
		
		query.setResultTransformer(Transformers.aliasToBean(FileLotDetails.class));
		
		paginacao.setListaDados(query.list());
		
		identifiedList.setValue(paginacao);		
		
		return identifiedList;
	}
}