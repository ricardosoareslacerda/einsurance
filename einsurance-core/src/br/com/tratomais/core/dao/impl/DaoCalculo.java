package br.com.tratomais.core.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.policy.ItemCoverage;
import br.com.tratomais.core.model.policy.ItemCoverageId;
import br.com.tratomais.core.model.policy.ItemProfile;
import br.com.tratomais.core.model.product.Attribute;
import br.com.tratomais.core.model.product.BillingMethod;
import br.com.tratomais.core.model.product.Factor;
import br.com.tratomais.core.model.product.PaymentTerm;
import br.com.tratomais.core.model.product.PlanKinship;
import br.com.tratomais.core.model.product.PlanPersonalRisk;
import br.com.tratomais.core.model.product.PlanPersonalRiskId;
import br.com.tratomais.core.model.product.ProductPaymentTerm;
import br.com.tratomais.core.model.product.Rate;
import br.com.tratomais.core.model.product.Term;
import br.com.tratomais.general.utilities.NumberUtilities;

/**
 * Classe representando o Calculo
 * @author luiz.alberoni
 */
public class DaoCalculo extends HibernateDaoSupport {
	/**
	 * Logger
	 */
	private static final Logger logger = Logger.getLogger(DaoCalculo.class);

	public static final int FATOR_TECNICO = 284;
	public static final int FATOR_COMERCIAL = 285;
	public static final int FATOR_GASTO = 286;
	public static final int ROADMAP_TYPE_TOTAL = 283;
	
	
	/**
	 * getFactorList
	 * @param productId
	 * @param coverageId
	 * @param roadmapType
	 * @param factorType
	 * @param refDate
	 * @return List<Factor>
	 */
	@SuppressWarnings("unchecked")
	public List<Factor> getFactorList(int productId, int coverageId, int roadmapType, int factorType, Date refDate) {
		logger.debug(String.format("getFactorList(productId: \"{0}\", coverageId: \"{1}\", roadmapType: \"{2}\", factorType: \"{3}\", refDate: \"{4}\")", new Object[]{productId, coverageId, roadmapType, factorType, refDate}));
		/*final String sqlStatement =
			"select " +
			" f.factorId " +
			",f.factorType " +
			",f.description " +
			",f.operatorType " +
			",f.attributeId_1 as attributeId1 " +
			",f.attributeId_2 as attributeId2 " +
			",f.attributeId_3 as attributeId3 " +
			",f.attributeId_4 as attributeId4 " +
			",f.attributeId_5 as attributeId5 " +
			",f.displayOrder " +
			",f.registred " +
			",f.updated " +
			",r.compulsory as compulsory " +
			"  from Factor f " +
			" inner join Roadmap r " +
			"    on (r.factorId = f.factorId) " +
			" where ( r.roadmapType = :roadmapType ) " +			
			"   and ( f.factorType  = :factorType ) " +
			"   and ( r.productId   = :productId ) " +
			"   and ( r.coverageId  = :coverageId ) " +
			"   and ( r.effectiveDate <= :refDate ) " +
			"   and ( r.expiryDate    >= :refDate ) " +
			" order by r.ApplyOrder ";*/
		Query query = getSession().getNamedQuery("einsurance.daoCalculo.getFactorList");
		query.setResultTransformer(Transformers.aliasToBean(Factor.class));
		query.setInteger("roadmapType", roadmapType);
		query.setInteger("factorType", factorType);
		query.setInteger("productId", productId);
		query.setInteger("coverageId", coverageId);
		query.setDate("refDate", refDate);
		List<Factor> factorList = query.list();
		if (factorList.size() == 0) {
			logger.debug("Factor not found!");
		}
		else {
			logger.debug(String.format("Factor found - returning: \"{0}\" records", new Object[]{factorList.size()}));
		}
		return factorList;
	}
	

	public List<Rate> getRateListByFactor(Factor factor, Item item, Date refDate ) {
		List<Rate> rateList = new ArrayList<Rate>();
		
		if (factor.hasProfileAttribute()) {
			Set<ItemProfile> itemProfileSet = item.getItemProfiles();
			
			for (ItemProfile itemProfile : itemProfileSet) {
				Rate rate = getProfileRateByFator(factor, item, itemProfile, refDate);
				
				if (rate != null) {
					rateList.add(rate);
				}
			}
		}
		else {
			Rate rate = getRateByFator(factor, item, refDate);
			if(rate != null)
				rateList.add(rate);
		}
		
		return rateList;
	}

	/**
	 * getProfileRateByFator
	 * @param factor Factor
	 * @param item Item
	 * @param itemProfile ItemProfile
	 * @param refDate Date of reference
	 * @return Rate
	 */
	@SuppressWarnings("unchecked")
	public Rate getProfileRateByFator(Factor factor, Item item, ItemProfile itemProfile, Date refDate ) {
		logger.debug(String.format("getRatebyFator(factor: \"{0}\", item: \"{1}\", refDate: \"{2}\", itemProfile:  \"{3}\")", new Object[]{factor, item, refDate, itemProfile}));
		Criteria criteria = getSession().createCriteria(Rate.class);
		criteria.add(Restrictions.eq("factorId", factor.getFactorId()));
		criteria.add(Restrictions.eq("attributeValue1", getItemProfileAttributeValue(factor.getAttributeId1(), item, itemProfile)));
		criteria.add(Restrictions.eq("attributeValue2", getItemProfileAttributeValue(factor.getAttributeId2(), item, itemProfile)));
		criteria.add(Restrictions.eq("attributeValue3", getItemProfileAttributeValue(factor.getAttributeId3(), item, itemProfile)));
		criteria.add(Restrictions.eq("attributeValue4", getItemProfileAttributeValue(factor.getAttributeId4(), item, itemProfile)));
		criteria.add(Restrictions.eq("attributeValue5", getItemProfileAttributeValue(factor.getAttributeId5(), item, itemProfile)));
		criteria.add(Restrictions.le("effectiveDate", refDate));
		criteria.add(Restrictions.ge("expiryDate", refDate));
		List<Rate> rateList = criteria.list();
		if (rateList.size() == 0) {
			logger.debug("Rate not found!");
			return null;
		}
		Rate rate = rateList.get(0); 
		logger.debug(String.format("Rate found: \"{0}\"", new Object[]{rate.getTariffId()}));
		return rate;
	}
	
	/**
	 * getRateByFator
	 * @param factor Factor
	 * @param item Item
	 * @param refDate Date of reference
	 * @return Rate
	 */
	@SuppressWarnings("unchecked")
	public Rate getRateByFator(Factor factor, Item item, Date refDate ) {
		logger.debug(String.format("getRatebyFator(factor: \"{0}\", item: \"{1}\", refDate: \"{2}\")", new Object[]{factor, item, refDate}));
		Criteria criteria = getSession().createCriteria(Rate.class);
		criteria.add(Restrictions.eq("factorId", factor.getFactorId()));
		criteria.add(Restrictions.eq("attributeValue1", (factor.getAttributeId1()!=null && factor.getAttributeId1()!=0)?item.getAttributeValue(factor.getAttributeId1()):Integer.valueOf(0)));
		criteria.add(Restrictions.eq("attributeValue2", (factor.getAttributeId2()!=null && factor.getAttributeId2()!=0)?item.getAttributeValue(factor.getAttributeId2()):Integer.valueOf(0)));
		criteria.add(Restrictions.eq("attributeValue3", (factor.getAttributeId3()!=null && factor.getAttributeId3()!=0)?item.getAttributeValue(factor.getAttributeId3()):Integer.valueOf(0)));
		criteria.add(Restrictions.eq("attributeValue4", (factor.getAttributeId4()!=null && factor.getAttributeId4()!=0)?item.getAttributeValue(factor.getAttributeId4()):Integer.valueOf(0)));
		criteria.add(Restrictions.eq("attributeValue5", (factor.getAttributeId5()!=null && factor.getAttributeId5()!=0)?item.getAttributeValue(factor.getAttributeId5()):Integer.valueOf(0)));
		criteria.add(Restrictions.le("effectiveDate", refDate));
		criteria.add(Restrictions.ge("expiryDate", refDate));
		List<Rate> rateList = criteria.list();
		if (rateList.size() == 0) {
			logger.debug("Rate not found!");
			return null;
		}
		Rate rate = rateList.get(0); 
		logger.debug(String.format("Rate found: \"{0}\"", new Object[]{rate.getTariffId()}));
		return rate;
	}
	
	/**
	 * getTermById
	 * @param termId
	 * @return Term
	 */
	public Term getTermById(Integer termId) {
		Criteria criteria = super.getSession().createCriteria(Term.class);
		criteria.add(Restrictions.eq("termId", termId));
		return (Term)criteria.uniqueResult();
	}
	
	/**
	 * Gets Payments terms per Product Id
	 * @param productId Id for product
	 * @param refDate 
	 */
	@SuppressWarnings("unchecked")
	public List<ProductPaymentTerm> getPaymentsTermsDefault(int productId, Date refDate){
		Criteria add = getSession().createCriteria(ProductPaymentTerm.class)
			.add(Restrictions.eq("id.productId", productId))
			.add(Restrictions.le("id.effectiveDate", refDate))
			.add(Restrictions.ge("expiryDate", refDate))
			.add(Restrictions.eq("paymentTermDefault", true));
		List list = add.list();
		return list;
	}
	
	/**
	 * Gets Payments terms per Product Id and billingMethodId
	 * @param productId Id for product
	 * @param billingMethodId Identifier billingMethod
	 * @param refDate 
	 */
	@SuppressWarnings("unchecked")
	public List<ProductPaymentTerm> getPaymentsTermsByBillingMethodId(int productId, Date refDate, int billingMethodId){
		Criteria crit = super.getSession().createCriteria(ProductPaymentTerm.class);
		crit.add(Restrictions.eq("id.productId", productId));
		crit.add(Restrictions.eq("id.billingMethodId", billingMethodId));
		crit.add(Restrictions.le("id.effectiveDate", refDate));
		crit.add(Restrictions.ge("expiryDate", refDate));
		List< ProductPaymentTerm > list = crit.list();
		return list;
	}

	/**
	 * Gets ProductPaymentTerm per Product Id and billingMethodId and paymentTermId
	 * @param productId Id for product
	 * @param billingMethodId
	 * @param paymentTermId
	 * @param refDate 
	 */
	public ProductPaymentTerm getProductPaymentTerm (int productId, Date refDate, int billingMethodId, int paymentTermId ){
		Criteria crit = super.getSession().createCriteria(ProductPaymentTerm.class);
		crit.add(Restrictions.eq("id.productId", productId));
		crit.add(Restrictions.eq("id.paymentTermId", paymentTermId));
		crit.add(Restrictions.eq("id.billingMethodId", billingMethodId));
		crit.add(Restrictions.le("id.effectiveDate", refDate));
		crit.add(Restrictions.ge("expiryDate", refDate));
		return (ProductPaymentTerm) crit.uniqueResult();
	}
	
	/**
	 * Gets a list of planKingShip
	 * @param productId Product Identificator
	 * @param planId  Plan Identificator
	 * @param personalRiskType Personal Risk Type
	 * @param kinShipType SikShipType
	 * @param renewalType Renewal Type
	 * @param refDate Reference Date
	 * @return PlanKinShip 
	 */
	@SuppressWarnings("unchecked")
	public PlanKinship getPlanKinshipByRefDate(int productId, int planId, int personalRiskType, int kinShipType, int renewalType, Date refDate){
		PlanKinship newPlanKinship = null;
		Criteria crit = getSession().createCriteria(PlanKinship.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", planId ) );
		crit.add( Restrictions.eq( "id.personalRiskType", personalRiskType));
		crit.add( Restrictions.eq( "id.kinshipType", kinShipType));
		crit.add( Restrictions.eq( "id.renewalType", renewalType));
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );

		crit.addOrder( Order.desc( "id.effectiveDate" ) );
		
		List <PlanKinship> list = crit.list();

		for ( PlanKinship planKinship : list ) {
			newPlanKinship = planKinship;
			break;
		}
		
		return newPlanKinship;
	}
	
	/**
	 * 
	 * @param paymentTerm
	 * @return
	 */
	public PaymentTerm getPaymentTerm(int paymentTerm){
		return (PaymentTerm) getSession().get(PaymentTerm.class, paymentTerm);
	}
	
	public BillingMethod getBillingMethod(int billingMethodId){
		return (BillingMethod) getSession().get(BillingMethod.class, billingMethodId);
	}

	public PlanPersonalRisk findPlanPersonalRisk(Date effectiveDate, int personType, int planId, int productId) {
		PlanPersonalRiskId id = new PlanPersonalRiskId(effectiveDate, personType, planId, productId);
		return (PlanPersonalRisk) getSession().get(PlanPersonalRisk.class, id);
	}

	/**
	 * @param masterPolicyId masterPolicy identificator to extract number from 
	 * @return the new certificate number
	 */
	public Long getCertificateNumber(Integer masterPolicyId) {
		Integer retorno = (Integer) getSession()
					.getNamedQuery("getSequenceCertificateNumber")
					.setParameter("id", masterPolicyId)
					.uniqueResult();
		retorno = retorno == null?1:retorno + 1;
		Query query = getSession().getNamedQuery("updateSequenceCertificateNumber"); 			
		query.setInteger("id", masterPolicyId);
		query.setInteger("newValue", retorno.intValue());
		query.executeUpdate();
		return retorno.longValue();
	}
	
	/**
	 * @return Next Proposal Number
	 */
	public Long getNextProposalNumber(){
		
		String retornoQuery = (String) getSession()
			.getNamedQuery("getLastSequenceProposalNumber")
			.uniqueResult();
		
		Integer newProposalNumber = retornoQuery == null?1:Integer.parseInt(retornoQuery) + 1;
		
		Query query = getSession().getNamedQuery("updateSequenceProposalNumber"); 			
		query.setInteger("newValue", newProposalNumber);
		query.executeUpdate();
		
		return newProposalNumber.longValue();
	}

	/**
	 * @return Next Endorsement Number
	 */
	public Long getNextEndorsementNumber(){
		
		String retornoQuery = (String) getSession()
			.getNamedQuery("getLastSequenceEndorsementNumber")
			.uniqueResult();
		
		Integer newEndorsementNumber = retornoQuery == null?1:Integer.parseInt(retornoQuery) + 1;
		
		Query query = getSession().getNamedQuery("updateSequenceEndorsementNumber"); 			
		query.setInteger("newValue", newEndorsementNumber);
		query.executeUpdate();
		
		return newEndorsementNumber.longValue();
	}
	
	/**
	 * Returns the number of occurrences for valid contracts
	 * @param productId Product
	 * @param docNumber Document number
	 * @param docType Document type
	 * @param dateIni EffectiveDate
	 * @param contractId ContractId
	 * @return number of occurrences for valid contracts
	 */
	public Integer getQuantityOfContracts(int productId, String docNumber, int docType, Date dateIni, Date dateFin, int contractId) {
		Query sqlQuery = getSession().getNamedQuery("getContractQuantityValidation");
		sqlQuery.setLong("contractId", contractId);
		sqlQuery.setLong("productId", productId);
		sqlQuery.setLong("documentType", docType);
		sqlQuery.setString("documentNumber", docNumber);
		sqlQuery.setDate("effectiveDate", dateIni);
		Object result = sqlQuery.uniqueResult();
		if (result instanceof BigInteger) {
			result = ((BigInteger)sqlQuery.uniqueResult()).intValue();
		}
		else if (result instanceof Integer) {
			result = (Integer) sqlQuery.uniqueResult();
		}
		return (Integer) result;
	}
	
	/**
	 * Returns the number of occurrences for valid contracts for Auto
	 * @param objectId Object
	 * @param licensePlate LicensePlate
	 * @param chassisSerial ChassisSerial
	 * @param engineSerial EngineSerial
	 * @param dateIni EffectiveDate
	 * @param contractId ContractId
	 * @return number of occurrences for valid contracts
	 */
	public Integer getQuantityOfContracts(int objectId, String licensePlate, String chassisSerial, String engineSerial, Date dateIni, int contractId) {
		Query sqlQuery = getSession().getNamedQuery("getContractQuantityValidationAuto");
		sqlQuery.setLong("contractId", contractId);
		sqlQuery.setLong("objectId", objectId);
		sqlQuery.setString("licensePlate", licensePlate);
		sqlQuery.setString("chassisSerial", chassisSerial);
		sqlQuery.setString("engineSerial", engineSerial);
		sqlQuery.setDate("effectiveDate", dateIni);
		Object result = sqlQuery.uniqueResult();
		if (result instanceof BigInteger) {
			result = ((BigInteger)sqlQuery.uniqueResult()).intValue();
		}
		else if (result instanceof Integer) {
			result = (Integer) sqlQuery.uniqueResult();
		}
		return (Integer) result;
	}
	
	/**
	 * Returns the number of occurrences for valid contracts for Purchase Protected
	 * @param objectId Object
	 * @param cardNumber CardNumber
	 * @param dateIni EffectiveDate
	 * @param contractId ContractId
	 * @return number of occurrences for valid contracts
	 */
	public Integer getQuantityOfContracts(int objectId, String cardNumber, Date dateIni, int contractId) {
		Query sqlQuery = getSession().getNamedQuery("getContractQuantityValidationPurchaseProtected");
		sqlQuery.setLong("contractId", contractId);
		sqlQuery.setLong("objectId", objectId);
		sqlQuery.setString("cardNumber", cardNumber);
		sqlQuery.setDate("effectiveDate", dateIni);
		Object result = sqlQuery.uniqueResult();
		if (result instanceof BigInteger) {
			result = ((BigInteger)sqlQuery.uniqueResult()).intValue();
		}
		else if (result instanceof Integer) {
			result = (Integer) sqlQuery.uniqueResult();
		}
		return (Integer) result;
	}
	
	/**
	 * @param productId Product Identifier
	 * @param docNumber Document Number
	 * @param docType Document Type
	 * @param dateIni Inicial date
	 * @param contractId Contract Identifier
	 * @return Sum of polices values
	 */
	public Double getValueSumOfContracts(int productId, String docNumber, int docType, Date dateIni, int contractId) {
		Query sqlQuery = getSession().getNamedQuery("getValueValidations");
		sqlQuery.setLong("contractId", contractId);
		sqlQuery.setLong("productId", productId);
		sqlQuery.setLong("documentType", docType);
		sqlQuery.setString("documentNumber", docNumber);
		sqlQuery.setDate("effectiveDate", dateIni);
		BigDecimal resultado = (BigDecimal) sqlQuery.uniqueResult();
		return (resultado==null)?0d:resultado.doubleValue();		
	}
	
	/**
	 * 
	 * @param contractId
	 * @param endorsementId
	 * @return
	 */
	public ItemCoverage getItemCoverageById(int contractId, int endorsementId, int itemId, int coverageId) {
		ItemCoverageId id = new ItemCoverageId(coverageId, itemId, endorsementId, contractId);
		return (ItemCoverage)getSession().get(ItemCoverage.class, id);
	}
	
	private int getItemProfileAttributeValue(Integer attributeId, Item item, ItemProfile itemProfile) {
		int attributeValue = 0; 
		
		if (attributeId != null && attributeId != 0) {
		
			if (Attribute.isProfileAttribute(attributeId)) {
				attributeValue = itemProfile.getAttributeValue(attributeId);
			}
			else {
				attributeValue = item.getAttributeValue(attributeId);
			}
		}
		
		return attributeValue;
	}

	@SuppressWarnings("unchecked")
	public List<ProductPaymentTerm> getPaymentsTermsByBillingMethodId(int productId, Date refDate, int billingMethodId, Integer paymentTermType) {
		Criteria criteria = super.getSession().createCriteria(ProductPaymentTerm.class);
		criteria.add(Restrictions.eq("id.productId", productId));
		criteria.add(Restrictions.eq("id.billingMethodId", billingMethodId));
		criteria.add(Restrictions.le("id.effectiveDate", refDate));
		criteria.add(Restrictions.ge("expiryDate", refDate));
		// filter by paymentTermType
		Criterion cs1 = Restrictions.eq("paymentTermType", ProductPaymentTerm.PAYMENT_TERM_TYPE_ALL);
		if (NumberUtilities.parseNull(paymentTermType).equals(0)) {
			criteria.add(cs1);
		}
		else {
			Criterion cs2 = Restrictions.eq("paymentTermType", paymentTermType);
			criteria.add(Restrictions.or(cs1, cs2));
		}
		List<ProductPaymentTerm> list = (List<ProductPaymentTerm>) criteria.list();
		return list;
	}
}
