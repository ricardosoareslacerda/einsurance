package br.com.tratomais.core.dao.impl;

import br.com.tratomais.core.dao.IDaoClaimActionLock;
import br.com.tratomais.core.model.claim.ClaimActionLock;
import br.com.tratomais.core.model.claim.ClaimActionLockId;

/**
 * Dao de aceso ao DaoClaimAction
 * @author luiz.alberoni
 *
 */
public class DaoClaimActionLockImpl extends DaoGenericImpl< ClaimActionLock, ClaimActionLockId > implements IDaoClaimActionLock {

}