package br.com.tratomais.core.dao.impl;

import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import br.com.tratomais.core.model.pendencies.LockPendency;
import br.com.tratomais.core.model.pendencies.LockPendencyHistory;

public class DaoHistoryPendency extends DaoGenericImpl<LockPendencyHistory, Integer> {
	private Logger logger = Logger.getLogger(DaoHistoryPendency.class);
	
	@Override
	public LockPendencyHistory save(LockPendencyHistory entity) {
		try {
			LockPendencyHistory newObject = null;
			newObject = (LockPendencyHistory)super.getHibernateTemplate().merge(entity);
			return newObject;
		} catch (final HibernateException ex) {
			logger.error(ex);
			throw new HibernateException(ex);
		}
	}
	
	public void historico(LockPendency lockPendency, String descricao, int historyStatus, Date issueDate, Integer userID, Integer lockPendencyStatus){
		LockPendencyHistory historyPendency = new LockPendencyHistory();
		historyPendency.setContractID(lockPendency.getId().getContractID());
		historyPendency.setEndorsementID(lockPendency.getId().getEndorsementID());
		historyPendency.setLockID(lockPendency.getId().getLockID());
		historyPendency.setDescription(descricao);
		historyPendency.setHistoryType(historyStatus);
		historyPendency.setIssueDate(issueDate==null?new Date():issueDate);
		historyPendency.setUserID(userID==null?1:userID);
		historyPendency.setLockPendencyStatus(lockPendencyStatus);
		save(historyPendency);
	}
}