package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import br.com.tratomais.core.dao.IDaoBroker;
import br.com.tratomais.core.model.customer.Broker;

@SuppressWarnings("unchecked")
public class DaoBrokerImpl extends DaoGenericImpl<Broker, Integer> implements IDaoBroker {

	public List<Broker> findByName(String name) {
		return super.findByCriteria(Restrictions.ilike("name", name, MatchMode.START));
	}

	public List<Broker> findByNickName(String nickName) {
		return this.getHibernateTemplate().find("From Broker where nickName = ?", nickName);
	}

	public boolean brokerExternalCodeExists(int insurerId, int brokerId, String externalCode) {
		Query query = getSession().createQuery("select count(b.externalCode) from Broker b where ( b.insurerId = :insurerId ) and (b.brokerId <> :brokerId ) and (b.externalCode = :externalCode)" );
		query.setInteger("insurerId", insurerId);
		query.setInteger("brokerId", brokerId);
		query.setString("externalCode", externalCode);
		Long retorno = ((Long) query.uniqueResult()).longValue();
		return retorno > 0;
	}

    /**
     * list all Broker from the user
     * @param partnerId
     * @param productId
     * @param brokerId
     * @param userId
     * @return List<Broker>
     */
	public List<Broker> listBrokerPerProductOrPartner(Integer partnerId, Integer productId, Integer brokerId, Integer userId) {
		Query query = super.getNamedQuery("selectBrokerPerProductOrPartner");
		query.setResultTransformer(Transformers.aliasToBean(Broker.class));
		query.setParameter("partnerId", partnerId);
		query.setParameter("productId", productId);
		query.setParameter("brokerId", brokerId);
		query.setParameter("userId", userId);
		List<Broker> listData = (List<Broker>) query.list();

		return listData;
	}

	@Override
	public List<Broker> listBroker(int insurerId, String regulatoryCode, Date refDate) {
		Criteria crit = getSession().createCriteria(Broker.class);
		crit.add(Restrictions.eq("insurerId", insurerId));
		crit.add(Restrictions.eq("regulatoryCode", regulatoryCode));
		crit.add(Restrictions.eq("active", true));
		
		return crit.list();
	}
}