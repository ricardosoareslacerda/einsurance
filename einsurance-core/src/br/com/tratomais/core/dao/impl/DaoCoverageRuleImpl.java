package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import br.com.tratomais.core.dao.IDaoCoverageRule;
import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.product.CoverageRule;
import br.com.tratomais.core.model.product.CoverageRuleValue;

/**
 * 
 * @author daniel.matuki
 *
 */
public class DaoCoverageRuleImpl  extends DaoGenericImpl<CoverageRule, Integer>   implements IDaoCoverageRule {

	@Override
	@SuppressWarnings("unchecked")
	public List<CoverageRule> getCoverageRuleList(int productId, int coverageId, int ruleType, Date refDate) {
		logger.debug(String.format("getCoverageRuleList(productId: \"{0}\", coverageId: \"{1}\", ruleType: \"{2}\", refDate: \"{3}\")", new Object[]{productId, coverageId, ruleType, refDate}));
		/*final String sqlStatement =
			"select ru.ruleID " +
			", ru.ruleType " +
			", ru.description " +
			", ru.displayOrder " +
			", ru.registred " +
			", ru.updated " +
			", ru.attributeId_1 as attributeId1 " +
			", ru.attributeId_2 as attributeId2 " +
			", ru.attributeId_3 as attributeId3 " +
			", ru.attributeId_4 as attributeId4 " +
			", ru.attributeId_5 as attributeId5 " +
			" from CoverageRule ru, CoverageRuleRoadmap ro " +
			" where (ru.ruleId = ro.ruleId)" +
			"	and ( ru.ruleType  = :ruleType ) " +
			"   and ( ro.productId   = :productId ) " +
			"   and ( ro.coverageId  = :coverageId ) " +
			"   and ( ro.effectiveDate <= :refDate ) " +
			"   and ( ro.expiryDate    >= :refDate ) " +
			" order by ro.ApplyOrder ";
		SQLQuery sqlQuery = getSession().createSQLQuery(sqlStatement);*/
		
		Query sqlQuery = getSession().getNamedQuery("einsurance.daoCoverageRule.getCoverageRuleList");
		sqlQuery.setResultTransformer(Transformers.aliasToBean(CoverageRule.class));
		sqlQuery.setInteger("ruleType", ruleType);
		sqlQuery.setInteger("productId", productId);
		sqlQuery.setInteger("coverageId", coverageId);
		sqlQuery.setDate("refDate", refDate);
		List<CoverageRule> coverageRuleList = sqlQuery.list();
		
		/*Criteria criteria = getSession().createCriteria(CoverageRule.class, "cr");
		criteria.createAlias("coverageRuleRoadmaps", "crr1_");
		criteria.add(Restrictions.eq("crr1_.id.productId", productId));
		criteria.add(Restrictions.eq("crr1_.id.coverageId", coverageId));
		criteria.add(Restrictions.eq("cr.ruleType", ruleType));
		criteria.add(Restrictions.le("crr1_.id.effectiveDate", refDate));
		criteria.add(Restrictions.ge("crr1_.expiryDate", refDate));
		criteria.setResultTransformer(Transformers.aliasToBean(CoverageRule.class));
		criteria.addOrder(Order.asc("crr1_.applyOrder"));
		List<CoverageRule> coverageRuleList = criteria.list();*/
		
		if (coverageRuleList.size() == 0) {
			logger.debug("Coverage Rule not found!");
		}
		else {
			logger.debug(String.format("Coverage Rule found - returning: \"{0}\" records", new Object[]{coverageRuleList.size()}));
		}
		
		return coverageRuleList;
	}

	@Override
	@SuppressWarnings("unchecked")
	public CoverageRuleValue getValueByCoverageRule(CoverageRule coverageRule, Item item, Date refDate) {
		logger.debug(String.format("getValueByCoverageRule(coverageRule: \"{0}\", item: \"{1}\", refDate: \"{2}\")", new Object[]{coverageRule, item, refDate}));
		Criteria criteria = getSession().createCriteria(CoverageRuleValue.class);
		criteria.add(Restrictions.eq("id.ruleId", coverageRule.getRuleID()));
		criteria.add(Restrictions.eq("id.attributeValue1", (coverageRule.getAttributeId1()!=null && coverageRule.getAttributeId1()!=0)?item.getAttributeValue(coverageRule.getAttributeId1()):Integer.valueOf(0)));
		criteria.add(Restrictions.eq("id.attributeValue2", (coverageRule.getAttributeId2()!=null && coverageRule.getAttributeId2()!=0)?item.getAttributeValue(coverageRule.getAttributeId2()):Integer.valueOf(0)));
		criteria.add(Restrictions.eq("id.attributeValue3", (coverageRule.getAttributeId3()!=null && coverageRule.getAttributeId3()!=0)?item.getAttributeValue(coverageRule.getAttributeId3()):Integer.valueOf(0)));
		criteria.add(Restrictions.eq("id.attributeValue4", (coverageRule.getAttributeId4()!=null && coverageRule.getAttributeId4()!=0)?item.getAttributeValue(coverageRule.getAttributeId4()):Integer.valueOf(0)));
		criteria.add(Restrictions.eq("id.attributeValue5", (coverageRule.getAttributeId5()!=null && coverageRule.getAttributeId5()!=0)?item.getAttributeValue(coverageRule.getAttributeId5()):Integer.valueOf(0)));
		criteria.add(Restrictions.le("id.effectiveDate", refDate));
		criteria.add(Restrictions.ge("expiryDate", refDate));
		List<CoverageRuleValue> coverageRuleValueList = criteria.list();
		if (coverageRuleValueList.size() == 0) {
			logger.debug("Coverage Rule not found!");
			return null;
		}
		CoverageRuleValue coverageRuleValue = coverageRuleValueList.get(0); 
		logger.debug(String.format("CoverageRuleValue found: \"{0}\"", new Object[]{coverageRuleValue}));
		return coverageRuleValue;
	}
}