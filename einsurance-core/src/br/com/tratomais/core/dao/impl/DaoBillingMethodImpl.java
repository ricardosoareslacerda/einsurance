package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import br.com.tratomais.core.dao.IDaoBillingMethod;
import br.com.tratomais.core.model.product.BillingAgency;
import br.com.tratomais.core.model.product.BillingMethod;

@SuppressWarnings("unchecked")
public class DaoBillingMethodImpl extends DaoGenericImpl<BillingMethod, Integer> implements IDaoBillingMethod {

	@Override
	public BillingMethod getById(Integer id) {
		return (BillingMethod) getSession().load(BillingMethod.class, id);
	}

	@Override
	public List<BillingMethod> listBillingMethod(int productID, Date versionDate) {

		/*StringBuilder sb = new StringBuilder();
		sb.append("select distinct(b) from ProductPaymentTerm ");
		sb.append("p left join p.billingMethod b where p.id.productId = :productID ");
		sb.append("and b.effectiveDate <= :versionDate ");
		sb.append("and b.expiryDate >= :versionDate");*/

		Query query = super.getNamedQuery("br.com.tratomais.core.dao.impl.listBillingMethod");
		// parameters
		query.setInteger("productId", productID);
		query.setDate("versionDate", versionDate);
		// transformer
		query.setResultTransformer(Transformers.aliasToBean(BillingMethod.class));
		query.setCacheable(false);
		query.setReadOnly(true);
		// result
		List<BillingMethod> list = (List<BillingMethod>) query.list();
		return list;
	}

	@Override
	public BillingAgency getBillingAgencyById(int billingAgencyId, Date refDate) {
		Criteria crit = getSession().createCriteria(BillingAgency.class);
		crit.add(Restrictions.eq("billingAgencyId", billingAgencyId));
		crit.add(Restrictions.eq("active", true));
		
		return (BillingAgency) crit.uniqueResult();
	}

	@Override
	public List<BillingMethod> listPaymentType(int productId, Integer paymentTermType, Date versionDate) {
		Query query = super.getNamedQuery("einsurance.daoBillingMethod.listPaymentType");
		// parameters
		query.setInteger("productId", productId);
		query.setParameter("paymentTermType", paymentTermType);
		query.setDate("referenceDate", versionDate);
		// transformer
		query.setResultTransformer(Transformers.aliasToBean(BillingMethod.class));
		query.setCacheable(false);
		query.setReadOnly(true);
		// result
		List<BillingMethod> list = (List<BillingMethod>) query.list();
		return list;
	}
}