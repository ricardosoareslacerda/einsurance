package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoProfile;
import br.com.tratomais.core.model.product.Profile;
import br.com.tratomais.core.model.product.ProfileId;
/**
 * 
 * @author daniel.matuki
 *
 */
public class DaoProfileImpl  extends DaoGenericImpl<Profile, ProfileId> implements IDaoProfile {

	@Override
	@SuppressWarnings("unchecked")
	public List<Profile> findByProductIdReferenceDate(int productId, Date referenceDate) {
		Criteria crit = getSession().createCriteria(Profile.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.le( "id.effectiveDate", referenceDate ) );
		crit.add( Restrictions.ge( "expiryDate", referenceDate ) );
		crit.createCriteria("question").addOrder(Order.asc("displayOrder"));
		crit.createCriteria("response").addOrder(Order.asc("displayOrder"));
		List <Profile> list = crit.list();
		
		return list;
	}	
}