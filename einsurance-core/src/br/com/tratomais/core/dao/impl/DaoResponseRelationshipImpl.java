package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoResponseRelationship;
import br.com.tratomais.core.model.product.ResponseRelationship;

/**
 * M�todo que retorna a lista de ResponseRelationship
 * 
 * @author recurso.externo
 *
 */
@SuppressWarnings("unchecked")
public class DaoResponseRelationshipImpl extends DaoGenericImpl<ResponseRelationship, Integer> implements IDaoResponseRelationship {

	@Override
	public List<ResponseRelationship> getResponseRelationship(Integer productId, Integer questionnaireId, Integer questionId, Integer responseId, Date refDate) {
		
		Criteria crit = getSession().createCriteria(ResponseRelationship.class);
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.questionnaireId", questionnaireId ) );
		crit.add( Restrictions.eq( "id.questionId", questionId ) );
		crit.add( Restrictions.eq( "id.responseId", responseId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		List <ResponseRelationship> list = crit.list();
		return list;		
	}
}