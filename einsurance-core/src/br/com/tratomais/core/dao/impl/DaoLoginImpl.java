package br.com.tratomais.core.dao.impl;

import java.util.List;

import br.com.tratomais.core.dao.IDaoLogin;
import br.com.tratomais.core.model.Login;

@SuppressWarnings("unchecked")
public class DaoLoginImpl extends DaoGenericImpl<Login, Integer> implements IDaoLogin {

	public DaoLoginImpl() {
		super();
	}

	public List<Login> findByNameDomain(String domain) {
		return this.getHibernateTemplate().find("From Login where domain = ? ", domain);
	}

	public Login findByLogin(String userName, String domain) {
		Object[] object = { userName, domain };
		return this.findUniqueEntityByQuery("From Login where login = ? and domain = ?", object);
	}
}
