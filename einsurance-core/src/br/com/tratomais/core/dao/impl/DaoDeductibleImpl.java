package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.dao.IDaoDeductible;
import br.com.tratomais.core.model.product.Deductible;

public class DaoDeductibleImpl extends DaoGenericImpl< Deductible, Integer > implements IDaoDeductible{
	
	@SuppressWarnings("unchecked")
	public List<Deductible> findByCoverageIdReferenceDate(int coverageId, Date refDate) {
		StringBuilder hql = new StringBuilder();
		hql.append("select d from Deductible d, CoverageDeductible c ");
		hql.append("where d.deductibleId = c.id.deductibleId ");
		hql.append("and c.id.coverageId = ? ");
		hql.append("and c.id.effectiveDate <= ? ");
		hql.append("and c.expiryDate >=  ? ");
		Object[] parameters = new Object[] { coverageId, refDate, refDate  };
		return getHibernateTemplate().find(hql.toString(), parameters);
	}
}