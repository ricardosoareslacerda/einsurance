package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;

import br.com.tratomais.core.dao.IDaoCountry;
import br.com.tratomais.core.model.Country;
import br.com.tratomais.core.model.product.IdentifiedList;

public class DaoCountryImpl extends DaoGenericImpl<Country, Integer> implements IDaoCountry {

	@SuppressWarnings("unchecked")
	public List<Country> findByName(String name) {
		return this.getHibernateTemplate().find("From Country where name = ?", name);
	}

	@SuppressWarnings("unchecked")
	public List<Country> findByName(Integer id) {
		return this.getHibernateTemplate().find("From Country where countryId = ?", id);
	}
	
	@SuppressWarnings("unchecked")
	public IdentifiedList listCountry(IdentifiedList identifiedList) {
		List<Country> listCountry = this.getHibernateTemplate().find("From Country where active=true");
		identifiedList.setResultArray(listCountry);
		return identifiedList;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Country> listCountryByRefDate(Date refDate) {
		Criteria crit = getSession().createCriteria(Country.class);
		return crit.list();
	}
}