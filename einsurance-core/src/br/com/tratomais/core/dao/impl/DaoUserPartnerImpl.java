package br.com.tratomais.core.dao.impl;

import java.util.Collection;

import br.com.tratomais.core.dao.IDaoUserPartner;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.UserPartner;
 
@SuppressWarnings("unchecked")
public final class DaoUserPartnerImpl extends DaoGenericImpl<UserPartner, Integer> implements IDaoUserPartner {

	public DaoUserPartnerImpl() {
	}

	public Collection<UserPartner> findByUser(User user) {
		return this.getHibernateTemplate().find("From UserPartner where user = ?", user);
	}
}