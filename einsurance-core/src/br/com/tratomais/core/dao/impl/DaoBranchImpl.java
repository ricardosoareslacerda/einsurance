package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoBranch;
import br.com.tratomais.core.model.product.Branch;

public class DaoBranchImpl extends DaoGenericImpl<Branch, Integer> implements IDaoBranch{

	@SuppressWarnings("unchecked")
	@Override
	public List<Branch> listBranch(Date refDate) {
		Criteria crit = getSession().createCriteria(Branch.class);
		/*crit.add( Restrictions.le( "effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );*/
		crit.addOrder(Order.asc("displayOrder"));
		return crit.list();
	}

	@Override
	public Branch getBranch(int branchID, Date refDate) {
		Criteria crit = getSession().createCriteria(Branch.class);
		crit.add( Restrictions.eq( "branchId", branchID ) );
		return (Branch) crit.uniqueResult();
	}
}