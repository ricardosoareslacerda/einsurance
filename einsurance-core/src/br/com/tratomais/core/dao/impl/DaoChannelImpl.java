package br.com.tratomais.core.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoChannel;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.UserPartner;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Partner;
import br.com.tratomais.core.util.TransportationClass;

@SuppressWarnings("unchecked")
public class DaoChannelImpl extends DaoGenericImpl<Channel, Integer> implements IDaoChannel {

	public List<Channel> findByName(String name) {
		return super.findByCriteria(Restrictions.ilike("name", name, MatchMode.START));
	}	
	
	public List<Channel> findByNickName(String nickName) {
		return this.getHibernateTemplate().find("From Channel where nickName = ?", nickName);
	}

	public List<Channel> findByPartnerId(int partnerID) {
		return this.findByPartnerId(partnerID, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.tratomais.core.dao.IDaoChannel#findByPartnerId(int, boolean)
	 */
	@Override
	public List<Channel> findByPartnerId(int partnerID, boolean onlyActive) {
		 Criteria criteria = super.getSession().createCriteria(Channel.class);
		 criteria.add(Restrictions.eq("partnerId", partnerID));
		 if (onlyActive) {
			 criteria.add(Restrictions.eq("active", onlyActive));
		 }
		 List result = criteria.list();
		 return result;
	}
 
	public List<Channel> findByUserAndPartner(User user, int partnerId) {
		//Partner partner = (Partner)getSession().get(Partner.class, partnerId);
		boolean userIsAllchannel = false;
		for (UserPartner workUserPartner : user.getUserPartners()) {
			if (workUserPartner.getId().getPartnerId() == partnerId) {
				userIsAllchannel = workUserPartner.isAllChannel();
				break;
			}
		}
		if (userIsAllchannel)
			return this.findByPartnerId(partnerId, true);
		else {
			return new ArrayList<Channel>(user.getChannels());
		}
	}

	public List<Channel> findByNameAndPartner(String name, Partner partner) {
		return super.findByCriteria(Restrictions.ilike("name", name, MatchMode.START), Restrictions.eq("partner", partner));
	}

	@Override
	public TransportationClass<Channel> listChannelToGrid(int limit, int page,
			String columnSorted, String typeSorted, String search,
			String searchField, String searchString, int partnerID) {
		// TODO Auto-generated method stub

		int total_pages = 0;
		int start = 0;
		int count = 0;

		Criteria criteria = getSession().createCriteria(Channel.class);
		criteria.add(Restrictions.eq("partnerId", partnerID));

		if (search.equalsIgnoreCase("true")) {
			criteria.add(Restrictions.ilike(searchField, searchString, MatchMode.ANYWHERE));
		}

		if ("asc".equals(typeSorted)) {
			criteria.addOrder(Order.asc(columnSorted));
		} else {
			criteria.addOrder(Order.desc(columnSorted));
		}

		count = criteria.list().size();
		List<Channel> channels = criteria.list();

		// calculation of total pages for the query
		total_pages = count / limit + ((count % limit) != 0 ? 1 : 0);

		// if for some reasons the requested page is greater than the total
		// set the requested page to total page
		if (page > total_pages)
			page = total_pages;

		// calculate the starting position of the rows
		start = limit * page - limit; // do not put $limit*($page - 1)

		// if for some reasons start position is negative set it to 0
		// typical case is that the user type 0 for the requested page
		if (start < 0)
			start = 0;

		criteria.setFirstResult(start);
		criteria.setMaxResults(limit);

		return new TransportationClass<Channel>(channels, count, total_pages);
	}

	@Override
	public List<Channel> listChannel(int insurerID, int partnerID, Date refDate) {
		 Criteria criteria = super.getSession().createCriteria(Channel.class);
		 criteria.add(Restrictions.eq("partnerId", partnerID));
		 criteria.add(Restrictions.eq("active", true));
		 List result = criteria.list();
		 return result;

//		Query query = null;
//		StringBuilder sb = new StringBuilder("select c from Channel c, PartnerInsurer p where p.partnerID = c.parnterID ");
//		sb.append("p.insurerID = :insurerID ");
//		sb.append("p.partnerID = :partnerID ");
//		sb.append("c.active = :active");
//		
//		query = getSession().createQuery(sb.toString());
//		query.setInteger("insurerID", insurerID);
//		query.setInteger("partnerID", partnerID);
//		query.setBoolean("active", true);
//		query.setResultTransformer(Transformers.aliasToBean(Channel.class));
//		
//		return query.list();
	}
}