package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import br.com.tratomais.core.dao.IDaoMasterPolicy;
import br.com.tratomais.core.model.policy.MasterPolicy;

public class DaoMasterPolicyImpl extends DaoGenericImpl<MasterPolicy, Integer>  implements IDaoMasterPolicy{

	public List<MasterPolicy> findByName(String name) {
		return super.findByCriteria(Restrictions.ilike("name", name, MatchMode.START));
	}

	@SuppressWarnings("unchecked")
	public List<MasterPolicy> listAllWithDescriptions() {
		Query query =  super.getNamedQuery("listMasterPolicyWithDescriptions");
	 	query.setResultTransformer(Transformers.aliasToBean(MasterPolicy.class));
		List<MasterPolicy> myList = query.list();
		return myList;
	}

	@SuppressWarnings("unchecked")
	public List<MasterPolicy> getRenewableMasterPolicies() {
		return getSession().createCriteria(MasterPolicy.class)
			.add(Restrictions.eq("automaticRenewal", Boolean.TRUE))
			.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<MasterPolicy> listMasterPolicy(int insurerID, int partnerID, Date refDate) {
		Criteria crit = getSession().createCriteria(MasterPolicy.class);
		crit.add(Restrictions.eq("insurerId", insurerID));
		crit.add(Restrictions.eq("partnerId", partnerID));
		crit.add(Restrictions.le("effectiveDate", refDate));
		crit.add(Restrictions.ge("expiryDate", refDate));
		return crit.list();
	}
}