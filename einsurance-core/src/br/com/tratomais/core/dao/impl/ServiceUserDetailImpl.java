package br.com.tratomais.core.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.userdetails.UserDetails;
import org.springframework.security.userdetails.UserDetailsService;
import org.springframework.security.userdetails.UsernameNotFoundException;

import br.com.tratomais.core.model.User;

@SuppressWarnings("unchecked")
public class ServiceUserDetailImpl extends HibernateDaoSupport implements UserDetailsService {
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException, DataAccessException {
		List<User> results = super.getHibernateTemplate().find("from User where login = ?", new Object[] { userName });

		if (results.size() != 1) {
			throw new UsernameNotFoundException(userName + "not found");
		}
		return (UserDetails) results.get(0);
	}
	
	public UserDetails createUserDetails(String username, UserDetails userFromUserQuery, GrantedAuthority[] combinedAuthorities) {
		// TODO Login > User = LINKAR COM TABELA DE USUARIO OU ADICIONAR UM CAMPO DE ATIVO (dependencia)
		// User user = new User();
		// user.setActive(true);
		// user.setAdministrator(false);
		// user.setLogin(username);
		// user.setPassword(userFromUserQuery.getPassword());
		// user.setAuthorities(combinedAuthorities);
		return null;
	}

	// @Override
	// public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
	// return userDAOImpl.findByLogin(username);
	// }
}