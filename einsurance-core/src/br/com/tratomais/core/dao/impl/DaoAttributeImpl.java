package br.com.tratomais.core.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.tratomais.core.dao.IDaoAttribute;
import br.com.tratomais.core.model.product.Attribute;
import br.com.tratomais.core.model.product.ResponseRelationship;

/**
 * Classe que executa buscas na tabela Attribute 
 * @author recurso.externo
 *
 */
@SuppressWarnings("unchecked")
public class DaoAttributeImpl extends DaoGenericImpl<ResponseRelationship, Integer> implements IDaoAttribute {

	/**
	 * M�todo que retorna o atribute pelo Id
	 */
	@Override
	public Attribute getAttribute(Integer attributeId, Date referenceDate) {
		Attribute attributeRetorno = null;
		
		ArrayList parametros = new ArrayList<Integer>();
		parametros.add(attributeId);
		parametros.add(referenceDate);
		
		List<Attribute> listaAtributo = this.getHibernateTemplate().find("FROM Attribute WHERE AttributeID = ? AND ? BETWEEN EffectiveDate AND ExpiryDate", parametros.toArray());
		
		if(listaAtributo != null && listaAtributo.size() > 0){
			attributeRetorno = listaAtributo.get(0);
		}
		
		return attributeRetorno;
	}
}