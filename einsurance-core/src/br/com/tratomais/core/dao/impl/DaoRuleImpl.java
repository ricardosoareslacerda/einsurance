package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoRule;
import br.com.tratomais.core.model.product.Clause;
import br.com.tratomais.core.model.product.CoverageRangeValue;
import br.com.tratomais.core.model.product.ProductClause;
import br.com.tratomais.core.model.product.Rule;
import br.com.tratomais.core.model.product.RuleFeature;
import br.com.tratomais.core.model.product.RuleTarget;
import br.com.tratomais.core.model.product.RuleType;

/**
 * Classe que opera sobre registros referentes � tabela Rule
 * 
 * @SuppressWarnings("unchecked")
 * @author recurso.externo
 * 
 */
public class DaoRuleImpl extends DaoGenericImpl<Rule, Integer> implements IDaoRule {

	/**
	 * M�todo que retorna a lista de registros da tabela RuleTarget a partir do
	 * c�digo de ruleActionType
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<RuleTarget> listRuleTarget(int productId, int ruleType, int ruleActionType) {
		Criteria crit = getSession().createCriteria(RuleTarget.class);
		crit.add(Restrictions.eq("productId", productId));
		crit.add(Restrictions.eq("ruleType", ruleType));
		crit.add(Restrictions.eq("ruleActionType", ruleActionType));
		List<RuleTarget> list = crit.list();
		return list;
	}

	/**
	 * M�todo que retorna a lista de RuleTarget, considerando tamb�m o valor do pendencyId
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<RuleTarget> listRuleTarget(int productId, int ruleType, int ruleActionType, int attributeValue1) {
		Criteria crit = getSession().createCriteria(RuleTarget.class);
		crit.add(Restrictions.eq("productId", productId));
		crit.add(Restrictions.eq("ruleType", ruleType));
		crit.add(Restrictions.eq("ruleActionType", ruleActionType));
		crit.add(Restrictions.eq("attributeValue_1", attributeValue1));
		List<RuleTarget> list = crit.list();
		return list;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Rule> listRuleByTargetId(int ruleTargetId, Date refDate) {
		StringBuilder hql = new StringBuilder();
		hql.append("SELECT 	r.RuleID, ");
		hql.append("		r.RuleType, ");
		hql.append("		r.Description, ");
		hql.append("		r.ObjectID, ");
		hql.append("		r.AttributeID_1, ");
		hql.append("		r.AttributeID_2, ");
		hql.append("		r.AttributeID_3, ");
		hql.append("		r.AttributeID_4, ");
		hql.append("		r.AttributeID_5, ");
		hql.append("		r.DisplayOrder, ");
		hql.append("		r.Registred, ");
		hql.append("		r.Updated ");
		hql.append("FROM Rule r, ");
		hql.append("	 RuleRoadmap rr ");
		hql.append("WHERE r.RuleId = rr.RuleId ");
		hql.append("  AND rr.RuleTargetId = :ruleTargetId ");
		hql.append("  AND :refDate BETWEEN rr.EffectiveDate AND rr.ExpiryDate ");
		hql.append("ORDER BY rr.ApplyOrder ");

		SQLQuery query = getSession().createSQLQuery(hql.toString());
		query.addEntity(Rule.class);
		query.setInteger("ruleTargetId", ruleTargetId);
		query.setDate("refDate", refDate);
		
		List<Rule> list = query.list();
		return list;
	}

	@Override
	public Clause getClauseById(int clauseId) {
		Criteria crit = getSession().createCriteria(Clause.class);		
		crit.add(Restrictions.eq("id.clauseId", clauseId));
		crit.setFirstResult(0);
		crit.setMaxResults(1);
		return (Clause)crit.uniqueResult();
	}

	@Override
	@SuppressWarnings("unchecked")
	public Clause getClauseByProductId(int productId, int clauseId, Date refDate) {
		Clause clauseReturn = null;
		
		Criteria crit = getSession().createCriteria(ProductClause.class);		
		crit.add(Restrictions.eq("id.productId", productId));
		crit.add(Restrictions.eq("id.clauseId", clauseId));
		crit.add(Restrictions.le("id.effectiveDate", refDate));
		crit.add(Restrictions.ge("expiryDate", refDate));
		
		List <ProductClause> list = crit.list();
		for (ProductClause productClause : list) {
			clauseReturn = getClauseById(productClause.getId().getClauseId());			
			break;
		}
		return clauseReturn;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<RuleFeature> listRuleFeatureByRuleId(int ruleId, Date refDate) {
		Criteria crit = getSession().createCriteria(RuleFeature.class);
		crit.add(Restrictions.eq("id.ruleId", ruleId));
		crit.add(Restrictions.le("id.effectiveDate", refDate));
		crit.add(Restrictions.ge("expiryDate", refDate));
		List<RuleFeature> list = crit.list();
		return list;
	}
	
	@Override
	public RuleType getRuleTypeById(int ruleType) {
		Criteria crit = getSession().createCriteria(RuleType.class);
		crit.add(Restrictions.eq("id.ruleType", ruleType));
		crit.setFirstResult(0);
		crit.setMaxResults(1);
		return (RuleType) crit.uniqueResult();
	}
	
	@Override
	public CoverageRangeValue getCoverageRangeValueByInsuredValueLimit(int productId, int planId, int coverageId, double insuredValue, Date refDate) {
		Criteria crit = getSession().createCriteria(CoverageRangeValue.class);
		crit.add(Restrictions.eq("id.productId", productId));
		crit.add(Restrictions.eq("id.planId", planId));
		crit.add(Restrictions.eq("id.coverageId", coverageId));
		crit.add(Restrictions.eq("id.rangeValueType", CoverageRangeValue.RANGE_VALUE_TYPE_IVL));
		crit.add(Restrictions.le("id.effectiveDate", refDate));
		crit.add(Restrictions.ge("expiryDate", refDate));
		crit.add(Restrictions.le("startValue", insuredValue));
		crit.add(Restrictions.ge("endValue", insuredValue));
		crit.setFirstResult(0);
		crit.setMaxResults(1);
		return (CoverageRangeValue) crit.uniqueResult();
	}
}