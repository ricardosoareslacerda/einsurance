/**
 * 
 */
package br.com.tratomais.core.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoCoverage;
import br.com.tratomais.core.model.product.Coverage;

/**
 * @author eduardo.venancio
 *
 */
@SuppressWarnings("unchecked")
public class DaoCoverageImpl extends DaoGenericImpl<Coverage, Integer>implements IDaoCoverage {
//	private static final Logger logger = Logger.getLogger( DaoCoverageImpl.class );

	public DaoCoverageImpl(){
		super();
	}

	public List<Coverage> findByName(String name) {
		return super.findByCriteria(Restrictions.ilike("name", name, MatchMode.START));
	}
	
	public List<Coverage> findByObject(Integer objectId){
		return this.getHibernateTemplate().find("From Coverage where objectId = ?", objectId);
	}

	@Override
	public Coverage getById(int id) {
		return (Coverage)this.getHibernateTemplate().get(Coverage.class, new Integer(id));
	}
	/**
	 * @param objectId
	 * @param coverageCode
	 * @param goodsCode
	 * @return
	 */
	@Override
	public Coverage getByCoverageCode(int objectId, String coverageCode, String goodsCode) {
		Criteria criteria = getSession().createCriteria(Coverage.class);
		criteria.add(Restrictions.eq("objectId", objectId));
		criteria.add(Restrictions.eq("coverageCode", coverageCode.trim()));
		criteria.add(Restrictions.eq("goodsCode", goodsCode.trim()));
		return (Coverage) criteria.uniqueResult();
	}
}