package br.com.tratomais.core.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;

import br.com.tratomais.core.dao.IDaoReportJasper;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.report.Report;

public class DaoReportJasperImpl extends DaoGenericImpl<Report, Long> implements IDaoReportJasper {

	/**
	 * Get a Report object from data base.
	 */
	public Report getObject(Long id) {
		return this.findById(id);
	}

	/**
	 * Get a Report object from data base.
	 */
	@SuppressWarnings("unchecked")
	public List<Object> getListReport(String query) {
		return this.getHibernateTemplate().find(query);
	}

	public Integer getObjectId(Integer contractId){
		Query query = getSession().getNamedQuery("getObjectId");
		query.setParameter("contractId", contractId);
		Integer objectId = (Integer) query.uniqueResult();
		if (objectId != null)
			return objectId;
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Report> listReportIdByType(Integer contractId, Integer endorsementId) {
		SQLQuery sqlQuery = (SQLQuery)getSession().getNamedQuery("listReportIdByType");
		sqlQuery.setResultTransformer(Transformers.aliasToBean(Report.class));
		sqlQuery.setParameter("contractId", contractId);
		sqlQuery.setParameter("endorsementId", endorsementId);
		List<Report> listResult = sqlQuery.list();
		if (listResult != null && listResult.size() > 0) {
			return listResult;
		}
		return null;
	}

	public Integer getReportIdByType(Integer contractId, Integer endorsementId, Integer reportType) {
		// check tipo de anulación
		Endorsement endorsement = this.getStatusById(contractId, endorsementId);
		if (endorsement != null && 
				endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_NO_PAYMENT_CANCEL) {
			return 22; // Carta de Anulación
		}
		
		// find ReportId
		SQLQuery sqlQuery = (SQLQuery)getSession().getNamedQuery("getReportIdByType");
		sqlQuery.setParameter("contractId", contractId);
		sqlQuery.setParameter("endorsementId", endorsementId);
		sqlQuery.setParameter("reportType", reportType);
		BigDecimal reportId = (BigDecimal) sqlQuery.uniqueResult();
		if (reportId != null) {
			return reportId.intValue();
		}
		else {
			// find report by objectId
			Integer objectId = this.getObjectId(contractId);
			switch(objectId) {
				case Item.OBJECT_VIDA:
					return 7;
				case Item.OBJECT_ACCIDENTES_PERSONALES:
					return 6;
				case Item.OBJECT_CICLO_VITAL:
					return 13;
				case Item.OBJECT_HABITAT:
					return 11;
				case Item.OBJECT_CREDITO:
					return 10;
				case Item.OBJECT_AUTO:
					return 26;
				case Item.OBJECT_PURCHASE_PROTECTED:
					return 27;
				case Item.OBJECT_CAPITAL:
					return 28;
				case Item.OBJECT_CONDOMINIUM:
					return 25;
			}
		}
		return null;
	}
	
	/**
	 * Encontra através dos IDs
	 */
	private Endorsement getStatusById(int contractId, int endorsementId){
		Query query = getSession().getNamedQuery("getIssuingStatusById");
		query.setResultTransformer(Transformers.aliasToBean(Endorsement.class));
		query.setParameter("contractId", contractId);
		query.setParameter("endorsementId", endorsementId);
		return (Endorsement)query.uniqueResult();
	}
}