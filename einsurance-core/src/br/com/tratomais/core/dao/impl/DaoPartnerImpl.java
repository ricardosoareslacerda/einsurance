package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import br.com.tratomais.core.dao.IDaoPartner;
import br.com.tratomais.core.model.PartnerAndChannel;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.customer.Partner;

@SuppressWarnings("unchecked")
public final class DaoPartnerImpl extends DaoGenericImpl<Partner, Integer> implements IDaoPartner {

	public DaoPartnerImpl() {
		super();
	}

	public List<Partner> findByName(String name) {
		return super.findByCriteria(Restrictions.ilike("name", name, MatchMode.START));
	}

	public List<Partner> listAllByUser(User user) {
		Query query = super.getNamedQuery("partnersByUser");
		query.setResultTransformer(Transformers.aliasToBean(Partner.class));
		query.setParameter("userId", user.getUserId());
		List<Partner> myList = query.list();
		return myList;
	}

	public List<PartnerAndChannel> listPartnersAndChannel(List<Integer> partnersIdList) {
		StringBuilder hql = new StringBuilder();
		hql.append("Select distinct p.name as partnerName,p.partnerId as partnerId, ");
		hql.append("c.name as channelName, c.channelId as channelId ");
		hql.append("From Channel c, Partner p, User u ");
		hql.append("where c.partnerId = p.partnerId ");
		hql.append("and c.active = true ");
		hql.append("and p.active = true ");
		hql.append("and p.partnerId in (:partnersIdList) ");
		hql.append("order by p.name ");
		Query query = super.getQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(PartnerAndChannel.class));
		query.setParameterList("partnersIdList", partnersIdList);
		List<PartnerAndChannel> myList = query.list();
		return myList;
	}

	public List<Partner> listAllAllowedByUser(User user) {
		Query query = super.getNamedQuery("partnersAllowedByUser");
		query.setResultTransformer(Transformers.aliasToBean(Partner.class));
		query.setParameter("userId", user.getUserId());
		List<Partner> myList = query.list();
		return myList;
	}

    /**
     * list all partner from the user
     * @param partnerId
     * @param productId
     * @param brokerId
     * @param userId
     * @return List<Partner>
     */
	public List<Partner> listPartnerPerProductOrBroker(Integer partnerId, Integer productId, Integer brokerId, Integer userId){
		Query query = super.getNamedQuery("selectPartnerPerProductOrBroker");
		query.setResultTransformer(Transformers.aliasToBean(Partner.class));
		query.setParameter("partnerId", partnerId);
		query.setParameter("productId", productId);
		query.setParameter("brokerId", brokerId);
		query.setParameter("userId", userId);
		List<Partner> listData = (List<Partner>) query.list();

		return listData;
	}

	/**
	 * Query to list partners active by user
	 * 
	 * @param userId: Id User
	 * @return {List<{@link Partner}>}
	 */
	public List<Partner> listAllActivePartnerByUser(int userId){
		Criteria criteria = getSession().createCriteria(Partner.class);
		Criteria channelCriteria = criteria.createCriteria("channels");
		Criteria userPartnerCriteria = criteria.createCriteria("userPartners");
		channelCriteria.add(Restrictions.eq("active", true));
		criteria.add(Restrictions.eq("active", true));
		userPartnerCriteria.add(Restrictions.eq("user.userId", userId));
		criteria.addOrder(Order.asc("name"));
		
		List<Partner> partners = criteria.list();
		
		/*StringBuilder hql = new StringBuilder();
		hql.append("Select p from User u, UserPartner up, Partner p, Channel c ");
		hql.append("where u.userId = up.id.userId ");
		hql.append("and p.partnerId = up.id.partnerId ");
		hql.append("and p.partnerId = c.channelId ");
		hql.append("and p.active = true ");
		hql.append("and c.active = true ");
		hql.append("and u.userId = ? ");
		hql.append("order by p.name asc");	
		
		Object[] parameters = new Object[] { userId };		
		List<Partner> partners = getHibernateTemplate().find(hql.toString(), parameters); */
		
		return partners;		
	}

	@Override
	public List<Partner> listPartner(int insurerID, Date refDate) {
		Criteria crit = getSession().createCriteria(Partner.class);
		crit.add(Restrictions.eq("active", true));
		
		return crit.list();

//		Query query = null;
//		StringBuilder sb = new StringBuilder("select p from Partner p, PartnerInsurer pi where pi.partnerID = p.parnterID and pi.insurerID = :insurerID and p.active = :active ");
//		
//		query = getSession().createQuery(sb.toString());
//		query.setInteger("insurerID", insurerID);
//		query.setBoolean("active", true);
//		query.setResultTransformer(Transformers.aliasToBean(Partner.class));
//		
//		return query.list();
	}
}