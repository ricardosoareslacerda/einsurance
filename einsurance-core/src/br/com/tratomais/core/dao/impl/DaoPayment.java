package br.com.tratomais.core.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import br.com.tratomais.core.model.policy.Installment;

public class DaoPayment extends HibernateDaoSupport {
	public Installment findInstallment(int contractID, int endorsementID, int installmentID) {
		Criteria criteria = getSession().createCriteria(Installment.class);
		criteria.add(Restrictions.eq("id.contractId", contractID));
		criteria.add(Restrictions.eq("id.endorsementId", endorsementID));
		criteria.add(Restrictions.eq("id.installmentId", installmentID));
		return (Installment) criteria.uniqueResult();
	}

	public Installment findInstallment(int proposalNumber, int installmentID) {
		Criteria criteria = getSession().createCriteria(Installment.class);
		criteria.add(Restrictions.eq("endorsement.proposalNumber", proposalNumber));
		criteria.add(Restrictions.eq("id.installmentId", installmentID));
		return (Installment) criteria.uniqueResult();
	}
}