package br.com.tratomais.core.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import br.com.tratomais.core.dao.IDaoFileLot;
import br.com.tratomais.core.model.interfaces.FileLot;
import br.com.tratomais.core.model.paging.Paginacao;
import br.com.tratomais.core.model.product.IdentifiedList;
import br.com.tratomais.core.util.PagingParameters;

public class DaoFileLotImpl extends DaoGenericImpl<FileLot, Integer> implements IDaoFileLot{

	@Override
	public FileLot getFileLot(Integer fileLotId, Integer lotId) {
		Criteria criteria = getSession().createCriteria(FileLot.class);
		criteria.add(Restrictions.eq("fileLotID", fileLotId));
		criteria.add(Restrictions.eq("lot.lotID", lotId));
		
		return (FileLot) criteria.uniqueResult(); 
	}

	@SuppressWarnings("unchecked")
	@Override
	public IdentifiedList listFileLot(IdentifiedList identifiedList, Integer lotId) {
		SQLQuery query = null;
		
		StringBuilder sql = new StringBuilder("SELECT f.fileLotID, FileLotTypeOption.fieldDescription AS typeDescription, ");
		sql.append("f.fileName, f.recordsAmount, f.recordsQuantity, f.fileLotStatus, FileLotStatusOption.fieldDescription AS statusDescription ");
		sql.append("FROM FileLot f LEFT JOIN DataOption FileLotStatusOption ON f.fileLotStatus = FileLotStatusOption.DataOptionID ");
		sql.append("LEFT JOIN DataOption FileLotTypeOption ON f.FileLotType = FileLotTypeOption.DataOptionID ");
		sql.append("WHERE f.lotID = " + lotId);
		sql.append(" ORDER BY f.FileLotID");
		
		query = getSession().createSQLQuery(sql.toString());
		
		PagingParameters prm = (PagingParameters) identifiedList.getValue();

		Paginacao paginacao = new Paginacao();
		paginacao.setTotalDados(query.list().size());
		
		if(prm.getTotalPage() > 0){
			query.setFirstResult(prm.getPage());
			query.setMaxResults(prm.getTotalPage());
		}
		
		query.setResultTransformer(Transformers.aliasToBean(FileLot.class));
				
		paginacao.setListaDados(query.list());
		
		identifiedList.setValue(paginacao);		
		
		return identifiedList;
	}
}