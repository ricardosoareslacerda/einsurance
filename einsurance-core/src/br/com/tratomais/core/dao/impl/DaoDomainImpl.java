package br.com.tratomais.core.dao.impl;

import java.util.List;

import br.com.tratomais.core.dao.IDaoDomain;
import br.com.tratomais.core.model.Domain;

@SuppressWarnings("unchecked")
public final class DaoDomainImpl extends DaoGenericImpl<Domain, Integer> implements IDaoDomain {

	public DaoDomainImpl() {
		super();
	}

	public List<Domain> findByName(String name) {
		return this.getHibernateTemplate().find("From Domain where name = ?", name);
	}

	public Domain findDomainByName(String name) {
		return super.findUniqueEntityByQuery("From Domain where name = ?", name);
	}
}