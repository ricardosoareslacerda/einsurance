package br.com.tratomais.core.dao.impl;

import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import br.com.tratomais.core.dao.IDaoLot;
import br.com.tratomais.core.model.interfaces.Exchange;
import br.com.tratomais.core.model.interfaces.Lot;
import br.com.tratomais.core.model.paging.Paginacao;
import br.com.tratomais.core.model.product.IdentifiedList;
import br.com.tratomais.core.util.DateUtil;
import br.com.tratomais.core.util.PagingParameters;

public class DaoLotImpl extends DaoGenericImpl< Lot, Integer > implements IDaoLot{

	@Override
	public Lot getLot(Integer lotId) {
		Criteria criteria = getSession().createCriteria(Lot.class);
		criteria.add(Restrictions.eq("lotID", lotId));
		
		return (Lot) criteria.uniqueResult();
	}
	
	@Override
	public Lot getLot(Integer lotId, Integer parentId, Integer exchangeId, Integer insurerId, Integer partnerId) {
		Criteria criteria = getSession().createCriteria(Lot.class);
		
		criteria.add(Restrictions.eq("lotId", lotId))
		.add(Restrictions.eq("parentID", parentId))
		.add(Restrictions.eq("exchangeID", exchangeId))
		.add(Restrictions.eq("insurerID", insurerId))
		.add(Restrictions.eq("partnerID", partnerId));
		
		return (Lot) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public IdentifiedList listLot(IdentifiedList identifiedList, Integer exchangeId, Integer lotStatus, Date rangeBefore, Date rangeAfter) {
		SQLQuery query = null;
		
		Criteria criteria = getSession().createCriteria(Exchange.class);
		criteria.add(Restrictions.eq("exchangeID", exchangeId));
		Exchange exchange = (Exchange)criteria.uniqueResult();
		
		StringBuilder strSql = new StringBuilder("SELECT l.lotID, l.lotCode, l.fileName, ");
		if (exchange != null) {
			if (exchange.getExchangeType() == Exchange.EXCHANGE_TYPE_RECEIVE) {
				strSql.append("l.recieveDate as sendDate, ");
			}
			else if (exchange.getExchangeType() == Exchange.EXCHANGE_TYPE_SEND) {
				strSql.append("l.sendDate, ");
			}
		}
		strSql.append("l.recordsAmount, l.recordsQuantity, ");
		strSql.append("l.errorMessage, l.lotStatus, d.fieldDescription AS statusDescription ");
		strSql.append("FROM Lot l LEFT JOIN DataOption d ON l.LotStatus = d.DataOptionID ");
		strSql.append("WHERE l.exchangeID = " + exchangeId);
		if(lotStatus > 0){
			strSql.append(" AND l.lotStatus = " + lotStatus);
		}
		if (exchange != null) {
			if (exchange.getExchangeType() == Exchange.EXCHANGE_TYPE_RECEIVE) {
				strSql.append(" AND TIMESTAMPDIFF(DAY, l.recieveDate, '" + DateUtil.getDateTime(rangeBefore, "yyyy-MM-dd") + "') <= 0 ");
				strSql.append(" AND TIMESTAMPDIFF(DAY, l.recieveDate, '" + DateUtil.getDateTime(rangeAfter, "yyyy-MM-dd") + "') >= 0");
			}
			else if (exchange.getExchangeType() == Exchange.EXCHANGE_TYPE_SEND) {
				strSql.append(" AND TIMESTAMPDIFF(DAY, l.sendDate, '" + DateUtil.getDateTime(rangeBefore, "yyyy-MM-dd") + "') <= 0 ");
				strSql.append(" AND TIMESTAMPDIFF(DAY, l.sendDate, '" + DateUtil.getDateTime(rangeAfter, "yyyy-MM-dd") + "') >= 0");
			}
		}
		strSql.append(" ORDER BY l.lotCode, l.lotID");		
		
		query = getSession().createSQLQuery(strSql.toString());
		
		PagingParameters prm = (PagingParameters) identifiedList.getValue();

		Paginacao paginacao = new Paginacao();
		paginacao.setTotalDados(query.list().size());
		
		if(prm.getTotalPage() > 0){
			query.setFirstResult(prm.getPage());
			query.setMaxResults(prm.getTotalPage());
		}
		
		query.setResultTransformer(Transformers.aliasToBean(Lot.class));
				
		paginacao.setListaDados(query.list());
		
		identifiedList.setValue(paginacao);
		
		return identifiedList;
	}
}