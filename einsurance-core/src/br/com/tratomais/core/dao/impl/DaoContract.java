package br.com.tratomais.core.dao.impl;

import java.math.BigInteger;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.policy.Contract;
import br.com.tratomais.core.model.policy.ItemSequence;
import br.com.tratomais.core.model.policy.MasterPolicy;

public class DaoContract extends DaoGenericImpl< Contract, Integer >{
	/**
	 * @param policyNumber
	 * @param certificateNumber
	 * @return
	 */
	public Contract getContract(BigInteger policyNumber, Long certificateNumber) {
		Criteria criteria = getSession().createCriteria(Contract.class);
		criteria.add(Restrictions.eq("policyNumber", policyNumber));
		criteria.add(Restrictions.eq("certificateNumber", certificateNumber));
		criteria.addOrder(Order.desc("contractId"));
		criteria.setFirstResult(0);
		criteria.setMaxResults(1);		
		return (Contract) criteria.uniqueResult();
	}
	
	/**
	 * @param contractID
	 * @param sequenceCode
	 * @return
	 */
	public ItemSequence getItemSequence(int contractID, int sequenceCode) {
		Criteria criteria = getSession().createCriteria(ItemSequence.class);
		criteria.add(Restrictions.eq("id.contractID", contractID));
		criteria.add(Restrictions.eq("id.sequenceCode", sequenceCode));
		return (ItemSequence) criteria.uniqueResult();
	}	
	
	public MasterPolicy getMasterPolicyById(int policyID) {
		Criteria criteria = getSession().createCriteria(MasterPolicy.class);
		criteria.add(Restrictions.eq("masterPolicyId", policyID));
		criteria.setFirstResult(0);
		criteria.setMaxResults(1);
		return (MasterPolicy) criteria.uniqueResult();
	}
	
	public Channel getChannelById(int partnerId, int channelId) {
		Criteria criteria = getSession().createCriteria(Channel.class);
		criteria.add(Restrictions.eq("partnerId", partnerId));
		criteria.add(Restrictions.eq("channelId", channelId));
		return (Channel) criteria.uniqueResult();
	}
}