package br.com.tratomais.core.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.model.claim.ClaimEffect;

public class DaoClaimEffect extends DaoGenericImpl<ClaimEffect, Integer>{
	/**
	 * @param objectID
	 * @param claimEffectCode
	 * @return
	 */
	public ClaimEffect getByClaimEfectCode(int objectID, String claimEffectCode) {
		Criteria criteria = getSession().createCriteria(ClaimEffect.class);
		criteria.add(Restrictions.eq("objectID", objectID));
		criteria.add(Restrictions.eq("claimEffectCode", claimEffectCode.trim()));
		return (ClaimEffect) criteria.uniqueResult();
	}
}