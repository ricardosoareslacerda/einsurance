package br.com.tratomais.core.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoExchange;
import br.com.tratomais.core.model.interfaces.Exchange;

public class DaoExchangeImpl extends DaoGenericImpl<Exchange, Integer> implements IDaoExchange{

	@Override
	public Exchange getExchange(Integer exchangeId) {
		Criteria criteria = getSession().createCriteria(Exchange.class);
		criteria.add(Restrictions.eq("exchangeID", exchangeId));
		
		return (Exchange) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Exchange> listExchange() {
		return getSession().createCriteria(Exchange.class).add(Restrictions.eq("active", true)).list();	
	}
}