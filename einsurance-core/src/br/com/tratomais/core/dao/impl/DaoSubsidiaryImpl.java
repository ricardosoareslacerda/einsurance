package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoSubsidiary;
import br.com.tratomais.core.model.customer.Subsidiary;
import br.com.tratomais.core.util.TransportationClass;

@SuppressWarnings("unchecked")
public class DaoSubsidiaryImpl extends DaoGenericImpl<Subsidiary, Integer>  implements IDaoSubsidiary{

	public List<Subsidiary> findByName(String name) {
		return super.findByCriteria(Restrictions.ilike("name", name, MatchMode.START));
	}

	public List<Subsidiary> findByNickName(String nickName) {
		return this.getHibernateTemplate().find("From Subsidiary where nickName = ?", nickName);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoSubsidiary#subsidiaryExternalCodeExists(int, int, java.lang.String)
	 */
	public boolean subsidiaryExternalCodeExists(int insurerId, int subsidiaryId, String externalCode){
		Query query = getSession().createQuery("select count(s.externalCode) from Subsidiary s where ( s.insurerId = :insurerId ) and (s.subsidiaryId <> :subsidiaryId ) and (s.externalCode = :externalCode)" );
		query.setInteger("insurerId", insurerId);
		query.setInteger("subsidiaryId", subsidiaryId);
		query.setString("externalCode", externalCode);
		Long retorno = ((Long) query.uniqueResult() ).longValue();
		return  retorno> 0;
	}
	
	/**
	 * Query to list the subsidiarys (used in gridview)
	 * 
	 * @param limit: Number of rows to return.  If this is not set all rows will be returned.
	 * @param page: The page number to return (if paginating).
	 * @param columnSorted: Field to sort the output by.
	 * @param typeSorted: Sort order.  Either 'asc' (ascending) or 'desc' (descending).
	 * @param search: True or False (whether or not this is a search). Default is False. 
	 * Also takes strings of 'true' and 'false'.
	 * @param searchField: The field we're searching on.
	 * @param searchString: The string to match for our search.
	 * @return List<Subsidiary>
	 */
	public TransportationClass<Subsidiary> listSubsidiaryToGrid(int limit, int page, String columnSorted, String typeSorted,
																	String search, String searchField, String searchString){	
		int total_pages;
		int start;
		int count;
		
		if(search.equalsIgnoreCase("true")){
			count = (Integer)getSession().createCriteria(Subsidiary.class)
			.add(Restrictions.ilike(searchField, searchString, MatchMode.ANYWHERE))
			.setProjection(Projections.count("subsidiaryId"))
			.uniqueResult();			
		} else {
			count = (Integer)getSession().createCriteria(Subsidiary.class)
			.setProjection(Projections.count("subsidiaryId"))
			.uniqueResult();			
		}
		
		// calculation of total pages for the query
		total_pages = count/limit + ((count%limit)!=0?1:0);
		
		// if for some reasons the requested page is greater than the total
		// set the requested page to total page
		if (page > total_pages) page = total_pages;

		// calculate the starting position of the rows
		start = limit*page - limit; // do not put $limit*($page - 1)
		
		// if for some reasons start position is negative set it to 0
		// typical case is that the user type 0 for the requested page
		if(start < 0) start = 0; 		
		
		Criteria criteria = getSession().createCriteria(Subsidiary.class);
		criteria.setFirstResult(start);
		criteria.setMaxResults(limit);
		
		if(search.equalsIgnoreCase("true")){
			criteria.add(Restrictions.ilike(searchField, searchString, MatchMode.ANYWHERE));
		}
		
		if ("asc".equals(typeSorted)){
			criteria.addOrder(Order.asc(columnSorted));
		} else {
			criteria.addOrder(Order.desc(columnSorted));
		}
		
		List<Subsidiary> subsidiarys = criteria.list();	
		
		return new TransportationClass<Subsidiary>(subsidiarys, count, total_pages);
	}

	@Override
	public List<Subsidiary> listSubsidiary(int insurerID, Date refDate){
		Criteria crit = getSession().createCriteria(Subsidiary.class);
		crit.add(Restrictions.eq("insurerId", insurerID));
		crit.add(Restrictions.eq("active", true));
		
		return crit.list();
	}	
}