package br.com.tratomais.core.dao.impl;

import java.io.Serializable;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import br.com.tratomais.core.dao.IDaoBean;
import br.com.tratomais.core.dao.PersistentEntity;

@SuppressWarnings("unchecked")
public class DaoBeanImpl extends HibernateDaoSupport implements IDaoBean {
//	private static final Logger logger = Logger.getLogger( DaoBeanImpl.class );

	public DaoBeanImpl() {
		super();
	}

	public PersistentEntity load(Class clazz, Serializable id) {
		return (PersistentEntity) super.getHibernateTemplate().load(clazz, id);
	}
}