package br.com.tratomais.core.dao.impl;

import br.com.tratomais.core.dao.IDaoInstallment;
import br.com.tratomais.core.model.policy.Installment;
import br.com.tratomais.core.model.policy.InstallmentId;

/** 
 * Implementa��o atrav�s do suporte do spring ao Hibernate
 * @author luiz.alberoni
 */
public class DaoInstallmentImpl extends DaoHibernateSupport<Installment, InstallmentId> implements IDaoInstallment {

	/** Construtor padr�o */
	public DaoInstallmentImpl() {
		super(Installment.class);
	}
	
	/** {@inheritDoc} */
	@Override
	public Installment mergeAndFlush(Installment installment){
		Installment toReturn = merge(installment);
		getSession().flush();
		return toReturn;
	}
}