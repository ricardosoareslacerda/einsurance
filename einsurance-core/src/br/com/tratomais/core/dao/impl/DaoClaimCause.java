package br.com.tratomais.core.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.model.claim.ClaimCause;

public class DaoClaimCause extends DaoGenericImpl<ClaimCause, Integer> {

	/**
	 * @param objectID
	 * @param claimCauseCode
	 * @return
	 */
	public ClaimCause getByClaimCauseCode(int objectID, String claimCauseCode) {
		Criteria criteria = getSession().createCriteria(ClaimCause.class);
		criteria.add(Restrictions.eq("objectID", objectID));
		criteria.add(Restrictions.eq("claimCauseCode", claimCauseCode.trim()));
		return (ClaimCause) criteria.uniqueResult();
	}
}