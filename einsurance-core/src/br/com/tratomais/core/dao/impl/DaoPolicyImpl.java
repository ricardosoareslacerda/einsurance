package br.com.tratomais.core.dao.impl;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.BeanUtils;

import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.infrastructure.AuthenticationHelper;
import br.com.tratomais.core.model.ApplicationProperty;
import br.com.tratomais.core.model.ApplicationPropertyID;
import br.com.tratomais.core.model.Inflow;
import br.com.tratomais.core.model.User;
import br.com.tratomais.core.model.claim.ClaimAction;
import br.com.tratomais.core.model.claim.ClaimActionLock;
import br.com.tratomais.core.model.claim.ClaimCause;
import br.com.tratomais.core.model.claim.ClaimEffect;
import br.com.tratomais.core.model.customer.Activity;
import br.com.tratomais.core.model.customer.Address;
import br.com.tratomais.core.model.customer.Channel;
import br.com.tratomais.core.model.customer.Customer;
import br.com.tratomais.core.model.customer.Occupation;
import br.com.tratomais.core.model.customer.Phone;
import br.com.tratomais.core.model.customer.Profession;
import br.com.tratomais.core.model.pendencies.LockPendency;
import br.com.tratomais.core.model.pendencies.LockPendencyHistory;
import br.com.tratomais.core.model.policy.Beneficiary;
import br.com.tratomais.core.model.policy.CalcStep;
import br.com.tratomais.core.model.policy.Contract;
import br.com.tratomais.core.model.policy.Endorsement;
import br.com.tratomais.core.model.policy.EndorsementId;
import br.com.tratomais.core.model.policy.Installment;
import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.policy.ItemCoverage;
import br.com.tratomais.core.model.policy.ItemPersonalRisk;
import br.com.tratomais.core.model.policy.ItemPersonalRiskGroup;
import br.com.tratomais.core.model.policy.MasterPolicy;
import br.com.tratomais.core.model.product.BillingMethod;
import br.com.tratomais.core.model.product.ConversionRate;
import br.com.tratomais.core.model.product.Coverage;
import br.com.tratomais.core.model.product.DataOption;
import br.com.tratomais.core.model.product.PaymentOption;
import br.com.tratomais.core.model.renewal.RenewalAlert;
import br.com.tratomais.core.util.DateUtilities;
import br.com.tratomais.esb.interfaces.model.response.ClaimResponse;

/**
 * @author eduardo.venancio
 *
 */
@SuppressWarnings("unchecked")
public class DaoPolicyImpl extends DaoGenericImpl<Beneficiary, Integer> implements IDaoPolicy {

	private static final Logger logger = Logger.getLogger( DaoPolicyImpl.class );
	
	public List<Occupation> listAllOccupation() {
		String hql = "From Occupation";
		return this.getHibernateTemplate().find(hql);
	}

	public List<Profession> listAllProfession() {
		String hql = "From Profession";
		return this.getHibernateTemplate().find(hql);
	}

	public List<Activity> listAllActivity() {
		String hql = "From Activity";
		return this.getHibernateTemplate().find(hql);
	}

	@Deprecated
	public Endorsement saveEndorsement_atual(Endorsement inputEndorsement){

		//Verifica se endosso tem contrato
		if (inputEndorsement.getContract() == null) {
			throw new IllegalArgumentException("Contract object can't be null");
		}
		//Guarda o numero do endosso
		printKeys(inputEndorsement);		
		//Salva Contrato
		Contract workContract = new Contract();
		String[] ignorePropertiesContract = { "endorsements" };
		BeanUtils.copyProperties(inputEndorsement.getContract(), workContract, ignorePropertiesContract);
		workContract = saveContract(workContract);
		
		//Copia Endosso
		Endorsement workEndorsement = new Endorsement();
		String[] ignorePropertiesEndorsemant = { "items" };
		BeanUtils.copyProperties( inputEndorsement, workEndorsement, ignorePropertiesEndorsemant );
		
		//Se novo endosso
		if (inputEndorsement.getId() == null || inputEndorsement.getId().getEndorsementId() == 0 ) {
			EndorsementId id = new EndorsementId();
			//id.setContractId( workContract.getContractId() );
			//Endorsement maxEnd = getMaxEndorsementByContract( workContract.getContractId() );
			// IG-> Seta endosso para 1 nesta fase do projeto
			id.setEndorsementId( 1 );
			// IG-> N�o Soma endosso neste fase do projeto rever quando tiver endosso
//			if ( maxEnd != null ) {
//				id.setEndorsementId( maxEnd.getId().getEndorsementId() + 1 );
//			} else {
//				id.setEndorsementId( 1 );
//			}
			workEndorsement.setId( id );
		}
		workEndorsement.updateKey(workContract);

		printKeys(workEndorsement);
		
		workEndorsement.setQuotationNumber(workEndorsement.getId().getContractId());
		
		workEndorsement = (Endorsement) this.getHibernateTemplate().merge(workEndorsement);
		
		//((PersistentCollection) workEndorsement.getContract().getEndorsements()).forceInitialization();

		workEndorsement.getItems().clear();
		
		for (Item workiItem : inputEndorsement.getItems()) {
			workEndorsement.attachItem(workiItem);
			//this.getHibernateTemplate().saveOrUpdate(workiItem);
//			for ( ItemCoverage cov : workiItem.getItemCoverages() ) {
//				for ( CalcStep step : cov.getCalcSteps() ) {
//					System.out.println("##CALC##" + step.getId().getItemId()+" " + step.getId().getCoverageId()+" " + step.getDescription()+" " + step.getRateFactor() );
//				}
//			}
			
			workiItem = (Item) this.getHibernateTemplate().merge(workiItem);
			
		}

		
		workEndorsement.getContract().getEndorsements().clear();
		workEndorsement.getContract().getEndorsements().add(workEndorsement);
		workEndorsement = (Endorsement) this.getHibernateTemplate().merge(workEndorsement);
		
//		//Verifica se contrato tem chave e atualiza ou inclui se chave = 0
//		if(inputEndorsement.getContract().getContractId() == 0){
//			Contract workContract = new Contract();
//			String[] ignoreProperties = { "endorsements" };
//			BeanUtils.copyProperties(inputEndorsement.getContract(), workContract, ignoreProperties);
//			workContract = saveContract(workContract);
//			inputEndorsement.updateKey( workContract );
//		}else{
//			inputEndorsement.updateKey( inputEndorsement.getContract() );
//		}
//		
//		Endorsement workEndorsement = (Endorsement) this.getHibernateTemplate().merge(inputEndorsement);
//		
//		workEndorsement.setQuotationNumber(workEndorsement.getId().getContractId());
		
		return workEndorsement;
	}


	public Endorsement saveEndorsement(Endorsement inputEndorsement){
		
		//Verifica se endosso tem contrato
		if (inputEndorsement.getContract() == null) {
			throw new IllegalArgumentException("Contract object can't be null");
		}
		printKeys(inputEndorsement);
		
		//Salva Contrato
		Contract workContract = new Contract();
		String[] ignorePropertiesContract = { "endorsements" };
		BeanUtils.copyProperties(inputEndorsement.getContract(), workContract, ignorePropertiesContract);
		workContract = saveContract(workContract);
		
		//Copia Endosso
		Endorsement workEndorsement = new Endorsement();
		BeanUtils.copyProperties( inputEndorsement, workEndorsement );
		
		//Se novo endosso
		if (inputEndorsement.getId() == null || inputEndorsement.getId().getEndorsementId() == 0 ) {
			EndorsementId id = new EndorsementId();
			// IG-> Seta endosso para 1 nesta fase do projeto
			id.setEndorsementId( 1 );
			workEndorsement.setId( id );
		}
		workEndorsement.updateKey(workContract);
		printKeys(workEndorsement);

		workEndorsement.setQuotationNumber(workEndorsement.getId().getContractId());

		for ( Endorsement tmpEndorsement : workContract.getEndorsements() ) {
			this.getHibernateTemplate().evict(tmpEndorsement);
		}
		workEndorsement.getContract().getEndorsements().clear();

		workEndorsement.getContract().getEndorsements().add(workEndorsement);
		
		workEndorsement = (Endorsement) this.getHibernateTemplate().merge(workEndorsement);
		
		return workEndorsement;
	}

	/** 
	 * adjustEndorsementToCalc - m�todo utilizado para ajustar os dados do objeto de endosso 
	 * 
	 * @param inputEndorsement - Ultima posi��o do endosement valido
	 * @param issuanceType - Tipo de endosso 
	 * @param AdjustmentsToEndorsement - parametros de ajuste
	 * @return Endosement - objeto endorsement com todas as chaves dos filhos e parametros ajustados
	 * @see Endorsement
	 */
	@Override
	public Endorsement adjustEndorsementToCalc(Endorsement inputEndorsement, int issuanceType, IDaoPolicy.AdjustmentsToEndorsement adjustments){
		//Salva Contrato
		Contract workContract = new Contract();
		String[] ignorePropertiesContract = { "endorsements" };
		Contract contract = listContract(inputEndorsement.getId().getContractId());
		BeanUtils.copyProperties(contract, workContract, ignorePropertiesContract);
		getSession().evict(inputEndorsement);
		
		//Copia Endosso
		Endorsement workEndorsement = new Endorsement();
		String[] ignorePropertiesEndorsement = { "installments" };
		BeanUtils.copyProperties( inputEndorsement, workEndorsement, ignorePropertiesEndorsement );		
		try {
			workEndorsement.setId((EndorsementId) inputEndorsement.getId().clone());
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (!adjustments.isRecalculation()) {
			//atualiza a chave do endorsement
			workEndorsement.getId().setEndorsementId(contract.getEndorsements().size()+1);
		
			//Ajusta os dados principais do endorsement
			workEndorsement.setQuotationNumber(workEndorsement.getId().getContractId());
			workEndorsement.setEndorsedId(inputEndorsement.getId().getEndorsementId());
			workEndorsement.setEndorsedNumber(inputEndorsement.getEndorsementNumber());
		}
		workEndorsement.setEndorsementNumber(null);
		workEndorsement.setEndorsementDate(null);
		workEndorsement.setQuotationDate(null);
		workEndorsement.setIssuanceDate(null);
		workEndorsement.setProposalNumber(null);
		workEndorsement.setProposalDate(null);
		workEndorsement.setTransmitted(false);
		workEndorsement.setChangeLocked(null);
		
		// data fill user for change
		User user = AuthenticationHelper.getLoggedUser();
		if (user != null)
			workEndorsement.setUserId(user.getUserId());
		
		switch (issuanceType) {
			case Endorsement.ISSUANCE_TYPE_NO_PAYMENT_CANCEL:
				ajustaDadosEndossoAnulacao(workContract, workEndorsement, issuanceType, adjustments);	
				break;
			case Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR:
				ajustaDadosEndossoAnulacaoErroEmissao(workContract, workEndorsement, issuanceType, adjustments);	
				break;				
			case Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_INSURED:
				ajustaDadosEndossoAnulacaoSegurado(workContract, workEndorsement, issuanceType, adjustments);	
				break;
			case Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_REGISTER:
				ajustaDadosEndossoAlteracaoRegistral(workContract, workEndorsement, issuanceType, adjustments);
				break;
			case Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL:
				ajustaDadosEndossoAlteracaoTecnica(workContract, workEndorsement, issuanceType, adjustments);
				break;
			case Endorsement.ISSUANCE_TYPE_POLICY_REACTIVATION:
				adjustments.setPaymentOption(getPaymentOptionReactivation(loadEndorsement(inputEndorsement.getEndorsedId(), inputEndorsement.getId().getContractId())));
				ajustaDadosReativacaoApolice(workContract, workEndorsement, issuanceType, adjustments);
				break;
			case Endorsement.ISSUANCE_TYPE_POLICY_CLAIM:
				ajustaDadosSinistroApolice(workContract, workEndorsement, issuanceType, adjustments);
				break;
		}
		
		workEndorsement.updateKey(workContract);
		
		//autaliza a chave para os itens
		for ( Item workiItem : workEndorsement.getItems() ) {
			workiItem.updateKey( workEndorsement );
			for(ItemCoverage cover : workiItem.getItemCoverages()){
				cover.setCalcSteps(new HashSet< CalcStep >( 0 ));
			}
		}
		
		//limpa o endorsement antigo para pendurar o novo
		workEndorsement.getContract().getEndorsements().clear();
		workEndorsement.getContract().getEndorsements().add(workEndorsement);		
		
		return workEndorsement;
	}
	
	public void evict(Endorsement endorsement){
		Hibernate.initialize(endorsement.getItems());
		getSession().evict(endorsement.getContract());
		getSession().evict(endorsement);
	}
	
	/**
	 * Ajuste de dados para o endosso de anula��o
	 * 
	 * @param {@link Contract} workContract 
	 * @param {@link Endosement} workEndorsement 
	 * @param int issuanceType
	 * @param {@link AdjustmentsToEndorsement} adjustments 
	 */
	private void ajustaDadosEndossoAnulacao(Contract workContract,
											Endorsement workEndorsement, 
											int issuanceType, 
											AdjustmentsToEndorsement adjustments){

		//set os novos parametros do contrato
		workContract.setCancelDate(adjustments.getCancelDate());
		workContract.setContractStatus(Contract.CONTRACT_STATUS_ABROGATED);
		workContract = saveContract(workContract);
		
		//set os novos parametros do endosso
		workEndorsement.setTermId(2); //Pro-Rata
		workEndorsement.setIssuanceType(issuanceType);
		workEndorsement.setEffectiveDate(adjustments.getEffectiveDate());
		workEndorsement.setExpiryDate(workContract.getExpiryDate());
		workEndorsement.setEndorsementType(Endorsement.ENDORSEMENT_TYPE_NOT);
		workEndorsement.setCancelDate(adjustments.getCancelDate());
		workEndorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_COTACAO);
		workEndorsement.setCancelReason(Endorsement.CANCEL_REASON_OTHERS);
		
		//autaliza os itens
		for ( Item workItem : workEndorsement.getItems() ) {
			if (workItem.getCancelDate() == null) {
				workItem.setCancelDate(adjustments.getCancelDate());
				workItem.setItemStatus(Item.ITEM_STATUS_INACTIVE);
			}

			switch (workItem.getObjectId()) {
				case Item.OBJECT_CICLO_VITAL:
					for(ItemPersonalRiskGroup riskGroup : ((ItemPersonalRisk)workItem).getItemPersonalRiskGroups()) {
						if (riskGroup.getExpiryDate() == null) {
							riskGroup.setExpiryDate(adjustments.getCancelDate());
						}
					}
					break;
			}

			for(ItemCoverage cover : workItem.getItemCoverages()){
				cover.setCoverageCancel(true);
			}
		}
		
		//limpa a lista de parcelas para que
		//dependendo do tipo de endosso a fun��o principal
		//vai verificar a necessidade de gerar ou n�o novas parcelas
		workEndorsement.getInstallments().clear();
	}

	/**
	 * Ajuste de dados para o endosso de anula��o por solita��o do segurado
	 * 
	 * @param {@link Contract} workContract 
	 * @param {@link Endosement} workEndorsement 
	 * @param int issuanceType
	 * @param {@link AdjustmentsToEndorsement} adjustments 
	 */
	private void ajustaDadosEndossoAnulacaoSegurado(Contract workContract,
													Endorsement workEndorsement, 
													int issuanceType, 
													AdjustmentsToEndorsement adjustments){

		//set os novos parametros do contrato
		workContract.setCancelDate(adjustments.getCancelDate());
		workContract.setContractStatus(Contract.CONTRACT_STATUS_ABROGATED);
		workContract = saveContract(workContract);
		
		//set os novos parametros do endosso
		workEndorsement.setTermId(2); //Pro-Rata
		workEndorsement.setIssuanceType(issuanceType);
		workEndorsement.setEffectiveDate(adjustments.getCancelDate());
		workEndorsement.setExpiryDate(workContract.getExpiryDate());
		workEndorsement.setEndorsementType(adjustments.getEndorsementType());
		workEndorsement.setCancelDate(adjustments.getCancelDate());
		workEndorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_COTACAO);
		workEndorsement.setCancelReason(adjustments.getCancelReason());
		
		//autaliza os itens
		for ( Item workItem : workEndorsement.getItems() ) {
			if (workItem.getCancelDate() == null) {
				workItem.setCancelDate(adjustments.getCancelDate());
				workItem.setItemStatus(Item.ITEM_STATUS_INACTIVE);
			}

			switch (workItem.getObjectId()) {
				case Item.OBJECT_CICLO_VITAL:
					for(ItemPersonalRiskGroup riskGroup : ((ItemPersonalRisk)workItem).getItemPersonalRiskGroups()) {
						if (riskGroup.getExpiryDate() == null) {
							riskGroup.setExpiryDate(adjustments.getCancelDate());
						}
					}
					break;
			}

			for(ItemCoverage cover : workItem.getItemCoverages()){
				cover.setCoverageCancel(true);
			}
		}
		
		//limpa a lista de parcelas para que
		//dependendo do tipo de endosso a fun��o principal
		//vai verificar a necessidade de gerar ou n�o novas parcelas
		workEndorsement.getInstallments().clear();		
	}

	/**
	 * Ajuste de dados para o endosso de anula��o por erro de emiss�o
	 * 
	 * @param {@link Contract} workContract 
	 * @param {@link Endosement} workEndorsement 
	 * @param int issuanceType
	 * @param {@link AdjustmentsToEndorsement} adjustments 
	 */
	private void ajustaDadosEndossoAnulacaoErroEmissao(Contract workContract,
													Endorsement workEndorsement, 
													int issuanceType, 
													AdjustmentsToEndorsement adjustments){

		//set os novos parametros do contrato
		workContract.setCancelDate(adjustments.getCancelDate());
		workContract.setContractStatus(Contract.CONTRACT_STATUS_ABROGATED);
		workContract = saveContract(workContract);
		
		//set os novos parametros do endosso
		workEndorsement.setIssuanceType(issuanceType);
		workEndorsement.setEndorsementType(adjustments.getEndorsementType());
		workEndorsement.setEffectiveDate(workContract.getEffectiveDate());
		workEndorsement.setExpiryDate(workContract.getExpiryDate());
		workEndorsement.setCancelDate(adjustments.getCancelDate());
		workEndorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_COTACAO);
		workEndorsement.setCancelReason(adjustments.getCancelReason());
		
		//autaliza os itens
		for ( Item workItem : workEndorsement.getItems() ) {
			if (workItem.getCancelDate() == null) {
				workItem.setCancelDate(adjustments.getCancelDate());
				workItem.setItemStatus(Item.ITEM_STATUS_INACTIVE);
			}

			switch (workItem.getObjectId()) {
				case Item.OBJECT_CICLO_VITAL:
					for(ItemPersonalRiskGroup riskGroup : ((ItemPersonalRisk)workItem).getItemPersonalRiskGroups()) {
						if (riskGroup.getExpiryDate() == null) {
							riskGroup.setExpiryDate(adjustments.getCancelDate());
						}
					}
					break;
			}

			for(ItemCoverage cover : workItem.getItemCoverages()){
				cover.setCoverageCancel(true);
			}
		}
		
		//limpa a lista de parcelas para que
		//dependendo do tipo de endosso a fun��o principal
		//vai verificar a necessidade de gerar ou n�o novas parcelas
		workEndorsement.getInstallments().clear();		
	}
	
	/**
	 * Ajuste de dados para o endosso de altera��o registral
	 * 
	 * @param {@link Contract} workContract 
	 * @param {@link Endosement} workEndorsement 
	 * @param int issuanceType
	 * @param {@link AdjustmentsToEndorsement} adjustments 
	 */
	private void ajustaDadosEndossoAlteracaoRegistral(Contract workContract,
													Endorsement workEndorsement, 
													int issuanceType, 
													AdjustmentsToEndorsement adjustments){

		//set os novos parametros do contrato
		workContract.setContractStatus(Contract.CONTRACT_STATUS_ACTIVE);
		workContract = saveContract(workContract);

		//set os novos parametros do endosso
		workEndorsement.setTermId(2); //Pro-Rata
		workEndorsement.setIssuanceType(issuanceType);
		workEndorsement.setEffectiveDate(adjustments.getEffectiveDate());
		workEndorsement.setExpiryDate(workContract.getExpiryDate());
		workEndorsement.setEndorsementType(Endorsement.ENDORSEMENT_TYPE_NOT);
		workEndorsement.setQuotationDate(new Date());
		workEndorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_COTACAO);
		
		//set novos codigos de Customer, Address e Phone para o Segurado/Tomador
		this.ajustaDadosSeguradoTomador(workEndorsement);
		
		//limpa os premios e valores das coberturas
		for ( Item workItem : workEndorsement.getItems() ) {
			for(ItemCoverage cover : workItem.getItemCoverages()) {
				cover.limpaTaxas();
				cover.limpaPremios();
			}
		}
		
		//limpa a lista de parcelas para que
		//dependendo do tipo de endosso a fun��o principal
		//vai verificar a necessidade de gerar ou n�o novas parcelas
		workEndorsement.getInstallments().clear();		

		//limpa os premios e valores
		workEndorsement.flush();
	}

	/**
	 * Ajuste de dados para o endosso de altera��o t�cnica
	 * 
	 * @param {@link Contract} workContract 
	 * @param {@link Endosement} workEndorsement 
	 * @param int issuanceType
	 * @param {@link AdjustmentsToEndorsement} adjustments 
	 */
	private void ajustaDadosEndossoAlteracaoTecnica(Contract workContract,
													Endorsement workEndorsement, 
													int issuanceType, 
													AdjustmentsToEndorsement adjustments){

		//set os novos parametros do contrato
		workContract.setContractStatus(Contract.CONTRACT_STATUS_ACTIVE);
		workContract = saveContract(workContract);

		//set os novos parametros do endosso
		workEndorsement.setTermId(2); //Pro-Rata
		workEndorsement.setIssuanceType(issuanceType);
		workEndorsement.setEffectiveDate(adjustments.getEffectiveDate());
		workEndorsement.setExpiryDate(workContract.getExpiryDate());
		workEndorsement.setEndorsementType(Endorsement.ENDORSEMENT_TYPE_COLLECTION);
		workEndorsement.setQuotationDate(new Date());
		workEndorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_COTACAO);
		
		//limpa os premios e valores das coberturas
		for ( Item workItem : workEndorsement.getItems() ) {
			for(ItemCoverage cover : workItem.getItemCoverages()) {
				cover.limpaTaxas();
				cover.limpaPremios();
			}
		}
		
		//limpa a lista de parcelas para que
		//dependendo do tipo de endosso a fun��o principal
		//vai verificar a necessidade de gerar ou n�o novas parcelas
		workEndorsement.getInstallments().clear();
		
		//limpa os premios e valores
		workEndorsement.flush();
	}
	
	/**
	 * Ajuste de dados de cliente para o endosso de Altera��o Registral/T�cnica
	 * 
	 * @param {@link Endosement} workEndorsement 
	 */	
	private void ajustaDadosSeguradoTomador(Endorsement workEndorsement) {
		
		//set novos codigos de Customer, Address e Phone para o Segurado
		inactivationCustomerbyCustomerId(workEndorsement.getCustomerByInsuredId().getCustomerId());
		workEndorsement.getCustomerByInsuredId().setCustomerId(0);
		for ( Address workAddress : workEndorsement.getCustomerByInsuredId().getAddresses() ) {
			inactivationAddressbyAddressId(workAddress.getAddressId());
			inactivationPhonebyAddressId(workAddress.getAddressId());
			workAddress.setAddressId(0);
			Iterator<Phone> iterator = workAddress.getPhones().iterator();
			while (iterator.hasNext()) {
				Phone workPhone = iterator.next();
				if ((workPhone.getPhoneNumber() == null) || ("".equals(workPhone.getPhoneNumber().trim())))
					iterator.remove();
				else
					workPhone.setPhoneId(0);
			}
		}
		
		//set novos codigos de Customer, Address e Phone para o Tomador
		inactivationCustomerbyCustomerId(workEndorsement.getCustomerByPolicyHolderId().getCustomerId());
		workEndorsement.getCustomerByPolicyHolderId().setCustomerId(0);
		for ( Address workAddress : workEndorsement.getCustomerByPolicyHolderId().getAddresses() ) {
			inactivationAddressbyAddressId(workAddress.getAddressId());
			inactivationPhonebyAddressId(workAddress.getAddressId());
			workAddress.setAddressId(0);
			Iterator<Phone> iterator = workAddress.getPhones().iterator();
			while (iterator.hasNext()) {
				Phone workPhone = iterator.next();
				if ((workPhone.getPhoneNumber() == null) || ("".equals(workPhone.getPhoneNumber().trim())))
					iterator.remove();
				else
					workPhone.setPhoneId(0);
			}
		}
	}
	
	private PaymentOption getPaymentOptionReactivation(Endorsement endorsement){
		PaymentOption paymentOption = new PaymentOption();
		if (endorsement != null) {
			for (Installment installment : endorsement.getInstallments()) {
				paymentOption.setBillingMethodId(installment.getBillingMethodId());
				paymentOption.setPaymentId(endorsement.getPaymentTermId());
				paymentOption.setBankNumber(installment.getBankNumber());
				paymentOption.setBankAgencyNumber(installment.getBankAgencyNumber());
				paymentOption.setBankAccountNumber(installment.getBankAccountNumber());
				paymentOption.setBankCheckNumber(null);
				paymentOption.setCardType(installment.getCardType());
				paymentOption.setCardBrandType(installment.getCardBrandType());
				paymentOption.setCardNumber(installment.getCardNumber());
				paymentOption.setCardExpirationDate(installment.getCardExpirationDate());
				paymentOption.setCardHolderName(installment.getCardHolderName());
				paymentOption.setOtherAccountNumber(installment.getOtherAccountNumber());
				break;
			}
		}
		return paymentOption;
	}
	
	/**
	 * Ajuste de dados para o endosso de Reativa��o de Apolice
	 * 
	 * @param {@link Contract} workContract 
	 * @param {@link Endosement} workEndorsement 
	 * @param int issuanceType
	 * @param {@link AdjustmentsToEndorsement} adjustments 
	 */
	private void ajustaDadosReativacaoApolice(Contract workContract,
											  Endorsement workEndorsement, 
											  int issuanceType, 
											  AdjustmentsToEndorsement adjustments){
		
		//set os novos parametros do contrato
		workContract.setCancelDate(null);
		workContract.setContractStatus(Contract.CONTRACT_STATUS_ACTIVE);
		workContract = saveContract(workContract);
		
		//set os novos parametros do endosso
		workEndorsement.setTermId(2); //Pro-Rata
		workEndorsement.setIssuanceType(issuanceType);
		workEndorsement.setEffectiveDate(adjustments.getEffectiveDate());
		workEndorsement.setExpiryDate(workContract.getExpiryDate());
		workEndorsement.setEndorsementType(Endorsement.ENDORSEMENT_TYPE_COLLECTION);
		workEndorsement.setCancelDate(null);
		workEndorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_COTACAO);
		workEndorsement.setCancelReason(null);
		
		//autaliza os itens
		for ( Item workItem : workEndorsement.getItems() ) {
			if (workItem.getClaimDate() == null) {
				workItem.setCancelDate(null);
				workItem.setItemStatus(Item.ITEM_STATUS_ACTIVE);
			}

			switch (workItem.getObjectId()) {
				case Item.OBJECT_CICLO_VITAL:
					for(ItemPersonalRiskGroup riskGroup : ((ItemPersonalRisk)workItem).getItemPersonalRiskGroups()) {
						if (riskGroup.getClaimDate() == null) {
							riskGroup.setExpiryDate(null);
						}
					}
					break;
			}

			for(ItemCoverage cover : workItem.getItemCoverages()){
				cover.setCoverageCancel(false);
			}
		}
		
		//limpa a lista de parcelas para que
		//dependendo do tipo de endosso a fun��o principal
		//vai verificar a necessidade de gerar ou n�o novas parcelas
		workEndorsement.getInstallments().clear();
	}

	/**
	 * Ajuste de dados para o endosso de Sinistro de Apolice
	 * 
	 * @param {@link Contract} workContract 
	 * @param {@link Endosement} workEndorsement 
	 * @param int issuanceType
	 * @param {@link AdjustmentsToEndorsement} adjustments 
	 */
	private void ajustaDadosSinistroApolice(Contract workContract,
											Endorsement workEndorsement,
											int issuanceType,
											AdjustmentsToEndorsement adjustments){
		
		//set os novos parametros do contrato
		workContract.setCancelDate(adjustments.getCancelDate());
		workContract.setContractStatus(Contract.CONTRACT_STATUS_ABROGATED);
		workContract = saveContract(workContract);
		
		//set os novos parametros do endosso
		workEndorsement.setTermId(2); //Pro-Rata
		workEndorsement.setIssuanceType(issuanceType);
		workEndorsement.setEffectiveDate(adjustments.getEffectiveDate());
		workEndorsement.setExpiryDate(workContract.getExpiryDate());
		workEndorsement.setEndorsementType(adjustments.getEndorsementType());
		workEndorsement.setCancelDate(adjustments.getCancelDate());
		workEndorsement.setIssuingStatus(Endorsement.ISSUING_STATUS_COTACAO);
		workEndorsement.setCancelReason(Endorsement.CANCEL_REASON_OTHERS);
		workEndorsement.setPaymentTermId(Endorsement.PAYMENT_TERM_UNIQUE);
		workEndorsement.setClaimNumber(adjustments.getClaimNumber());
		workEndorsement.setClaimDate(adjustments.getClaimDate());
		workEndorsement.setClaimNotification(adjustments.getClaimNotification());
		
		//autaliza os itens
		for ( Item workItem : workEndorsement.getItems() ) {
			if (workItem.getCancelDate() == null) {
				workItem.setCancelDate(adjustments.getCancelDate());
				workItem.setItemStatus(Item.ITEM_STATUS_INACTIVE);
			}

			switch (workItem.getObjectId()) {
				case Item.OBJECT_CICLO_VITAL:
					for(ItemPersonalRiskGroup riskGroup : ((ItemPersonalRisk)workItem).getItemPersonalRiskGroups()) {
						if (riskGroup.getExpiryDate() == null) {
							riskGroup.setExpiryDate(adjustments.getCancelDate());
						}
					}
					break;
			}

			for(ItemCoverage cover : workItem.getItemCoverages()){
				cover.setCoverageCancel(true);
			}
		}
		
		//limpa a lista de parcelas para que
		//dependendo do tipo de endosso a fun��o principal
		//vai verificar a necessidade de gerar ou n�o novas parcelas
		workEndorsement.getInstallments().clear();
	}

	
	@Deprecated
	public Endorsement saveEndorsement2(Endorsement inputEndorsement) {
		Endorsement workEndorsement = new Endorsement();
		if (inputEndorsement.getId() == null ||inputEndorsement.getId().getContractId() == 0 || inputEndorsement.getId().getEndorsementId() == 0 ) {
			String[] ignoreProperties = { "items" };
			BeanUtils.copyProperties( inputEndorsement, workEndorsement, ignoreProperties );
			Contract contract = workEndorsement.getContract();
			contract.getEndorsements().clear();
			workEndorsement.setContract( saveContract( contract ) );
			EndorsementId id = new EndorsementId();
			id.setContractId( workEndorsement.getContract().getContractId() );
//			Endorsement maxEnd = getMaxEndorsementByContract( id.getContractId() );
			// IG-> Seta endosso para 1 nesta fase do projeto
			id.setEndorsementId( 1 );
			// IG-> N�o Soma endosso neste fase do projeto rever quando tiver endosso
//			if ( maxEnd != null ) {
//				id.setEndorsementId( maxEnd.getId().getEndorsementId() + 1 );
//			} else {
//				id.setEndorsementId( 1 );
//			}
			workEndorsement.setId( id );
			
		} else {
			workEndorsement = inputEndorsement;
			Contract workContract = new Contract();
			String[] ignoreProperties = { "endorsements" };
			BeanUtils.copyProperties(workEndorsement.getContract(), workContract, ignoreProperties);
			workContract = saveContract(workContract);
			workEndorsement.updateKey(workContract);
		}
		if ( workEndorsement.getItems() != null ) {
			for ( Item workiItem : workEndorsement.getItems() ) {
				workiItem.updateKey( workEndorsement );
			}
		}
		
		if ( workEndorsement.getInstallments() == null ) {
			workEndorsement.setInstallments(new HashSet<Installment>());
		}
			
			for ( Installment workInstallment : workEndorsement.getInstallments() ) {
				workInstallment.updateKey( workEndorsement );
			}
		
		workEndorsement = (Endorsement) this.getHibernateTemplate().merge(workEndorsement);
		
		workEndorsement.setQuotationNumber(workEndorsement.getId().getContractId());
		
		if (inputEndorsement.getId() == null ||inputEndorsement.getId().getContractId() == 0 || inputEndorsement.getId().getEndorsementId() == 0 ){		
		for ( Item item : inputEndorsement.getItems() ) {
			Item workItem = null;
			switch ( item.getObjectId() ) {
			case Item.OBJECT_VIDA:
			case Item.OBJECT_ACCIDENTES_PERSONALES:
				workItem = new ItemPersonalRisk();
				break;

			default:
				throw new IllegalArgumentException();
			}

		
			String[] ignoreItemProperties = {"itemClauses"
											,"itemProfiles"
											,"itemPersonalRiskGroups"};
			BeanUtils.copyProperties( item, workItem, ignoreItemProperties );
			
			//vincula item ao endosso
			workEndorsement.attachItem( workItem );
			
			for ( ItemCoverage workCoverage : workItem.getItemCoverages() ) {
				//workItem.attachCoverage( workCoverage );
				workCoverage.updateKey( workItem );
				//workCoverage = (ItemCoverage) this.getHibernateTemplate().merge(workCoverage);
			}
			
			//workItem = (Item) this.getHibernateTemplate().merge(workItem);
			this.getHibernateTemplate().save(workItem);
			//this.getHibernateTemplate().merge(workItem);

		}
		
		workEndorsement = (Endorsement) this.getHibernateTemplate().merge(workEndorsement);
		}		
		return workEndorsement;
	}
	
	/*
	private Endorsement getMaxEndorsementByContract(int contractId){
		Endorsement maxEnd = null;
		
		List endors = this.getHibernateTemplate().findByNamedQueryAndNamedParam( "getMaxEndorsement", "contractId", Integer.valueOf( contractId ) );
		if ( !endors.isEmpty() ) {
			maxEnd = ( Endorsement ) endors.get( 0 );
		}		
		return maxEnd;
	}
	*/
	
	public List<Inflow> listAllInflow(String inflowType) {

		//call de hibernate criteria
		Criteria criteria = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(Inflow.class);
		criteria.add(Restrictions.eq("inflowType", inflowType));
		criteria.add(Restrictions.eq("active", true));
		
		//declare variables
		Inflow inflow = new Inflow();
		inflow.setActive(true);
		inflow.setDescription("");
		inflow.setDisplayOrder(1);
		
		List<Inflow> inflowList = criteria.list();
		List<Inflow> inflowNewList = new ArrayList<Inflow>(); 
		
		inflowNewList.add(inflow);
		MessageFormat messageFormat = null;
		
		//foreach object, change description
		for (Inflow inflowIntermediary : inflowList) {
			
			inflow = new Inflow();
			BeanUtils.copyProperties(inflowIntermediary, inflow);
			
			messageFormat = new MessageFormat(inflow.getDescription());
			
			inflow.setDisplayOrder(inflow.getDisplayOrder()+1);
			
			ConversionRate conversionRate = findConversionRateByRefDate(inflow.getCurrencyId(), null);
			
			NumberFormat formatter = new DecimalFormat("#,###.00");
			
			Object[] inflowArgs = {formatter.format((inflow.getInflowOf() * conversionRate.getMultipleRateBy())),
								   formatter.format((inflow.getInflowUntil() * conversionRate.getMultipleRateBy()))};

			inflow.setDescription(messageFormat.format(inflowArgs));
			
			inflowNewList.add(inflow);
		}
		
		return inflowNewList;
	}	

	public ConversionRate findConversionRateByRefDate(Integer currencyId, Date refDate) {
		if(refDate==null) {
			refDate = new Date();
		}
		Criteria criteria = getSession().createCriteria(ConversionRate.class);
		criteria.add(Restrictions.eq("currencyId", currencyId));
		criteria.add(Restrictions.le("currencyDate", refDate));
		criteria.addOrder(Order.desc("currencyDate"));
		criteria.setMaxResults(1);
		return (ConversionRate) criteria.uniqueResult();
	}
	
	public MasterPolicy findMasterPolicyById(int masterPolicyId) {
		return (MasterPolicy) this.getHibernateTemplate().load(MasterPolicy.class, new Integer(masterPolicyId));
	}

	public Contract saveContract(Contract contract) {
		Object retorno = this.getHibernateTemplate().merge(contract);
		return (Contract)retorno; 
	}

	public Customer saveCustomer(Customer customer) {
		return (Customer) this.getHibernateTemplate().merge(customer);
	}

	public Contract listContract(int contractId) {
		return (Contract) this.getHibernateTemplate().load( Contract.class, contractId );
	}

	private void printKeys(Endorsement endorsement){
		String tabKey = "\t";
		int level = 0;
		logger.info( "###PrintKeys-Start---------------###");
		logger.info( "Endorsement: " + endorsement.getId().toString());
		level++;
		for ( Item item : endorsement.getItems() ) {
			logger.info( tabKey + "Item: " + item.getId().toString());
			level++;
			for ( ItemCoverage coverage : item.getItemCoverages() ) {
				logger.info( tabKey + tabKey + "Coverage: " + coverage.getId().toString());
				level++;
				for ( CalcStep calcStep : coverage.getCalcSteps() ) {
					logger.info( tabKey + tabKey + tabKey + "CalcStep: " + calcStep.getId().toString());
				}
				level--;
			}
			level--;
		}
		level--;
		logger.info( "###PrintKeys-End-----------------###");
	}

	/**
	 * 
	 * @author leo.costa
	 * @param endorsementId 
	 * @param contractId
	 * 
	 * @return {@link Endorsement} endorsement 
	 * 
	 * **/
	public Endorsement loadEndorsement(int endorsementId, int contractId) {
		//Criteria criteria = super.getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(Endorsement.class);
		Criteria criteria = getSession().createCriteria(Endorsement.class);
		criteria.add(Restrictions.eq("id.endorsementId", endorsementId));
		criteria.add(Restrictions.eq("id.contractId", contractId));
		return (Endorsement) criteria.uniqueResult();
	}

	/**
	 * 
	 * @author leo.costa
	 * @param endorsementId 
	 * @param contractId
	 * @param reason
	 * @param issuingStatus
	 * @param cancelDate
	 * 
	 * @return {@link Integer}
	 * 
	 * **/
	public int cancellationEndorsement(int endorsementId, int contractId, int reason, int issuingStatus, Date cancelDate ) {
		Query query = super.getNamedQuery("cancellationEndorsement");
		query.setDate("cancelDate", cancelDate);
		query.setInteger("cancelReason", reason);
		query.setInteger("issuingStatus",issuingStatus);
		query.setInteger("endorsementId", endorsementId);
		query.setInteger("contractId", contractId);
		int executeUpdate = query.executeUpdate();
		return executeUpdate;
	}

	/**
	 * 
	 * @author leo.costa
	 * @param contractId
	 * @param contractStatus
	 * @param cancelDate
	 * 
	 * @return {@link Integer}
	 * 
	 * **/
	public int cancellationContract(int contractId, int contractStatus, Date cancelDate) {
		Query query = super.getNamedQuery("cancellationContract");
		query.setDate("cancelDate", cancelDate);
		query.setInteger("contractStatus", contractStatus);
		query.setInteger("contractId", contractId);
		int executeUpdate = query.executeUpdate();
		return executeUpdate;
	}

	/**
	 * 
	 * @author leo.costa
	 * @param contractId
	 * @param endorsementId
	 * @param installmentStatus
	 * @param cancelDate
	 * 
	 * @return {@link Integer}
	 * 
	 * **/
	public int cancellationInstallment(int endorsementId, int contractId, int installmentStatus, Date cancelDate) {
		Query query = super.getNamedQuery("cancellationInstallment");
		query.setInteger("contractId", contractId);
		query.setInteger("endorsementId", endorsementId);
		query.setDate("cancelDate", cancelDate);
		query.setInteger("installmentStatus", installmentStatus);
		query.setInteger("installmentStatusOnly",  Installment.INSTALLMENT_STATUS_PENDING);
		int executeUpdate = query.executeUpdate();
		return executeUpdate;
	}

	/**
	 * 
	 * @author leo.costa
	 * @param contractId
	 * @param endorsementId
	 * @param itemStatus
	 * @param cancelDate
	 * 
	 * @return {@link Integer} qtde
	 * 
	 * **/
	public int cancellationItem(int endorsementId, int contractId, int itemStatus, Date cancelDate) {
		Query query = super.getNamedQuery("cancellationItem");
		query.setInteger("contractId", contractId);
		query.setInteger("endorsementId", endorsementId);
		query.setDate("cancelDate", cancelDate);
		query.setInteger("itemStatus", itemStatus);
		int executeUpdate = query.executeUpdate();
		return executeUpdate;
	} 
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int cancellationItem(int endorsementId, int contractId, int itemId, int itemStatus, Date cancelDate) {
		Query query = super.getNamedQuery("cancellationItemId");
		query.setInteger("contractId", contractId);
		query.setInteger("endorsementId", endorsementId);
		query.setInteger("itemId", itemId);
		query.setDate("cancelDate", cancelDate);
		query.setInteger("itemStatus", itemStatus);
		
		return query.executeUpdate();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int cancellationClaimItem(int endorsementId, int contractId, int itemId, int itemStatus, Date cancelDate, String claimNumber, Date claimDate, Date claimNotification) {
		Query query = super.getNamedQuery("cancellationClaimItemId");
		query.setInteger("contractId", contractId);
		query.setInteger("endorsementId", endorsementId);
		query.setInteger("itemId", itemId);
		query.setDate("cancelDate", cancelDate);
		query.setInteger("itemStatus", itemStatus);
		query.setString("claimNumber", claimNumber.trim());
		query.setDate("claimDate", claimDate);
		query.setDate("claimNotification", claimNotification);
		
		return query.executeUpdate();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int cancellationClaimItemRiskGroup(int endorsementId, int contractId, int itemId, int riskGroupId, Date cancelDate, String claimNumber, Date claimDate, Date claimNotification) {
		Query query = super.getNamedQuery("cancellationClaimItemRiskGroupId");
		query.setInteger("contractId", contractId);
		query.setInteger("endorsementId", endorsementId);
		query.setInteger("itemId", itemId);
		query.setInteger("riskGroupId", riskGroupId);
		query.setDate("cancelDate", cancelDate);
		query.setString("claimNumber", claimNumber.trim());
		query.setDate("claimDate", claimDate);
		query.setDate("claimNotification", claimNotification);
		
		return query.executeUpdate();
	}
	
	/**
	 * m�todo de desativa��o dos dados antigos do cliente
	 * 
	 * @author wellington.barbosa
	 * @param customerId
	 * @return {@link Integer} qtde
	 * **/
	public int inactivationCustomerbyCustomerId(int customerId) {
		Query query = super.getNamedQuery("inactivationCustomerbyCustomerId");
		query.setInteger("customerId", customerId);
		
		return query.executeUpdate();
	}
	
	/**
	 * m�todo de desativa��o do endere�o antigo do cliente
	 * 
	 * @author wellington.barbosa
	 * @param addressId
	 * @return {@link Integer} qtde
	 * **/
	public int inactivationAddressbyAddressId(int addressId) {
		Query query = super.getNamedQuery("inactivationAddressbyAddressId");
		query.setInteger("addressId", addressId);
		
		return query.executeUpdate();
	}
	
	/**
	 * m�todo de desativa��o dos telefone(s) antigo(s) do cliente
	 * 
	 * @author wellington.barbosa
	 * @param addressId
	 * @return {@link Integer} qtde
	 * **/
	public int inactivationPhonebyAddressId(int addressId) {
		Query query = super.getNamedQuery("inactivationPhonebyAddressId");
		query.setInteger("addressId", addressId);
		
		return query.executeUpdate();
	}
	
	/**
	 * m�todo de exclus�o dos telefone(s) antigo(s) do cliente
	 * 
	 * @author wellington.barbosa
	 * @param addressId
	 * @return {@link Integer} qtde
	 * **/
	public int removePhonebyAddressId(int addressId) {
		Query query = super.getNamedQuery("removePhonebyAddressId");
		query.setInteger("addressId", addressId);
		
		return query.executeUpdate();
	}
	
	/**
	 * @author leo.costa
	 * @param productId
	 * @param referenceDate 
	 * 
	 * @return {@link ArrayList} of {@link BillingMethod}
	 * **/
	public List<BillingMethod> listPaymentType(int productId, Date referenceDate){
		Query query = super.getNamedQuery("listPaymentType");
		query.setInteger("productId", productId);
		query.setDate("referenceDate", referenceDate);
	 	
		query.setResultTransformer( Transformers.aliasToBean( BillingMethod.class ) );
		List<BillingMethod> myList = query.list();
		
		return myList;		
	}

	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#deleteInstallment(br.com.tratomais.core.model.policy.Installment)
	 */
	public void deleteInstallment(Installment installment) {
		this.getHibernateTemplate().delete(installment);
	}

	@Override
	public ApplicationProperty loadApplicationProperty(int applicationId, String propertyName){
		ApplicationPropertyID applicationPropertyID = new ApplicationPropertyID(applicationId, propertyName);
		return (ApplicationProperty) getSession().get(ApplicationProperty.class, applicationPropertyID) ;
	}
	
	public Long getNextApplicationPropertyValue(int applicationId, String propertyName){
		Criteria crit = getSession().createCriteria(ApplicationProperty.class);
		
		crit.add( Restrictions.eq( "id.applicationId", applicationId ) );		
		crit.add( Restrictions.eq( "id.name", propertyName ) );
		
		ApplicationProperty appProperty = (ApplicationProperty) crit.uniqueResult();
		
		Integer newPropertyValue = appProperty.getValue() == null?1:Integer.parseInt(appProperty.getValue()) + 1;
		
		return newPropertyValue.longValue();
	}
	
	public void updateApplicationPropertyValue(ApplicationProperty appProperty){
		this.getHibernateTemplate().saveOrUpdate(appProperty);
	}

	@Override
	public void adjustEndorsementToEffectivate(Endorsement endorsement, IDaoPolicy.AdjustmentsToEndorsement adjustments) {
		// Define default value for Proposal
		adjustments.setProposal(true);
		
		// Check if define new PaymentOption
		if ((endorsement.getIssuanceType() != Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL) &&
			(endorsement.getIssuanceType() != Endorsement.ISSUANCE_TYPE_POLICY_REACTIVATION)){
			adjustments.setPaymentOption(new PaymentOption());
		}
		PaymentOption paymentOption = adjustments.getPaymentOption();
		
		if (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL) {
			//set novos codigos de Customer, Address e Phone para o Segurado/Tomador
			this.ajustaDadosSeguradoTomador(endorsement);
			
			// Set unique Installment
			if (endorsement.getEndorsementType() == Endorsement.ENDORSEMENT_TYPE_REFUND) {
				adjustments.setPaymentOption(new PaymentOption());
				paymentOption = adjustments.getPaymentOption();
				paymentOption.setQuantityInstallment(1);
				paymentOption.setFirstInstallmentValue(endorsement.getTotalPremium());
				paymentOption.setNextInstallmentValue(0.0);
				paymentOption.setPaymentId(Endorsement.PAYMENT_TERM_UNIQUE);
				paymentOption.setBillingMethodId(Endorsement.BILLING_METHOD_ZURICH);
				paymentOption.setPaymentType(Installment.PAYMENT_TYPE_OTHERS);
				paymentOption.setPaymentTermName("�nica");
				paymentOption.setFractioningAdditional(0.0);
				paymentOption.setTotalPremium(endorsement.getTotalPremium());
				paymentOption.setTaxRate(0.0);
			}
			if (endorsement.getEndorsementType() != Endorsement.ENDORSEMENT_TYPE_NOT)
				endorsement.setPaymentTermId(paymentOption.getPaymentId());
			else
				endorsement.setPaymentTermId(null);
		}
		else if (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CLAIM) {
			if (paymentOption != null) {
				if (endorsement.getEndorsementType() != Endorsement.ENDORSEMENT_TYPE_NOT) {
					paymentOption.setQuantityInstallment(1);
					paymentOption.setFirstInstallmentValue(endorsement.getTotalPremium());
					paymentOption.setNextInstallmentValue(0.0);
					paymentOption.setPaymentId(Endorsement.PAYMENT_TERM_UNIQUE);
					paymentOption.setBillingMethodId(Endorsement.BILLING_METHOD_ZURICH);
					paymentOption.setPaymentType(Installment.PAYMENT_TYPE_OTHERS);
					paymentOption.setPaymentTermName("�nica");
					paymentOption.setFractioningAdditional(0.0);
					paymentOption.setTotalPremium(endorsement.getTotalPremium());
					paymentOption.setTaxRate(0.0);
					endorsement.setPaymentTermId(paymentOption.getPaymentId());
				} else
					endorsement.setPaymentTermId(null);
			}
		}
		else if ((endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_EMISSION_ERROR) ||
				 (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CANCELLATION_INSURED) ||
				 (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_REGISTER) ||
				 (endorsement.getIssuanceType() == Endorsement.ISSUANCE_TYPE_NO_PAYMENT_CANCEL)) {
			if (paymentOption != null) {
				if (endorsement.getEndorsementType() == Endorsement.ENDORSEMENT_TYPE_REFUND) {
					paymentOption.setQuantityInstallment(1);
					paymentOption.setFirstInstallmentValue(endorsement.getTotalPremium());
					paymentOption.setNextInstallmentValue(0.0);
					paymentOption.setPaymentId(Endorsement.PAYMENT_TERM_UNIQUE);
					paymentOption.setBillingMethodId(Endorsement.BILLING_METHOD_ZURICH);
					paymentOption.setPaymentType(Installment.PAYMENT_TYPE_OTHERS);
					paymentOption.setFractioningAdditional(0.0);
					paymentOption.setTotalPremium(endorsement.getTotalPremium());
					paymentOption.setTaxRate(0.0);
				}
				if (endorsement.getEndorsementType() != Endorsement.ENDORSEMENT_TYPE_NOT)
					endorsement.setPaymentTermId(paymentOption.getPaymentId());
				else
					endorsement.setPaymentTermId(null);
			}
		}
	}

	@Override
	public Installment getInstallment(int contractId, int endorsementId, int intallmentId) {
		Criteria criteria = getSession().createCriteria(Installment.class);
		criteria.add(Restrictions.eq("id.contractId", contractId));
		criteria.add(Restrictions.eq("id.endorsementId", endorsementId));
		criteria.add(Restrictions.eq("id.intallmentId", intallmentId));
		criteria.setMaxResults(1);
		return (Installment) criteria.uniqueResult();		
	}

	/** {@author luiz.alberoni} */
	@Override
	public List<Installment> getInstallmentForEndorsementId(int contractId, int endorsementId){
		return (List<Installment>)
			getSession().createCriteria(Installment.class)
				.add(Restrictions.eq("id.contractId", contractId))
				.add(Restrictions.eq("id.endorsementId", endorsementId))
				.addOrder(Order.asc("id.installmentId"))
				.list();
	}
	
	/**
	 * Returns the last valid entrance of Endorsement, for a contract
	 * @param numContrato
	 * @return
	 * @since M1
	 * @date 2010.10.25
	 */
	public Endorsement lastValidEndosementForContractNumber(Integer numContrato) {
		Criteria criteria = getSession().createCriteria(Endorsement.class);
		criteria.add(Restrictions.eq("contract.contractId", numContrato));
		criteria.add(Restrictions.eq("issuingStatus", Endorsement.ISSUING_STATUS_EMITIDA));
		criteria.addOrder(Order.desc("issuanceDate"));
		criteria.addOrder(Order.desc("id.endorsementId"));
		criteria.setMaxResults(1);

		return (Endorsement) criteria.uniqueResult();
	}
	
	public Endorsement lastValidEndorsementByEvict(Integer contractId) {
		Endorsement lastEndorsement = this.lastValidEndosementForContractNumber(contractId);
		if (lastEndorsement == null) 
			return null;

		// evict Endorsement
		getSession().evict(lastEndorsement);
		
		// copy Contract
		Contract returnContract = new Contract();
		String[] ignoreProperties = {"endorsements"};
		BeanUtils.copyProperties(lastEndorsement.getContract(), returnContract, ignoreProperties);
		
		// copy Endorsement
		Endorsement returnEndorsement = new Endorsement();
		BeanUtils.copyProperties(lastEndorsement, returnEndorsement);
		
		// updateKey Endorsement
		returnEndorsement.updateKey(returnContract);
		
		// attach Endorsement
		returnEndorsement.getContract().getEndorsements().clear();
		returnEndorsement.getContract().getEndorsements().add(returnEndorsement);
		
		return returnEndorsement;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Endorsement lastValidEndosementForContractNumber(Integer contractNumber, Date effectiveDate) {
		Criteria criteria = getSession().createCriteria(Endorsement.class);
		criteria.add(Restrictions.eq("contract.contractId", contractNumber));
		criteria.add(Restrictions.eq("issuingStatus", Endorsement.ISSUING_STATUS_EMITIDA));
		criteria.add(Restrictions.le("effectiveDate", effectiveDate));
		criteria.add(Restrictions.ge("expiryDate", effectiveDate));
		criteria.addOrder(Order.desc("issuanceDate"));
		criteria.addOrder(Order.desc("id.endorsementId"));		
		criteria.setMaxResults(1);

		return (Endorsement) criteria.uniqueResult();
	}
	
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#getContractNumber(BigInteger, Long, Date)
	 */
	public Contract getContractNumber(BigInteger policyNumber, Long certificateNumber, Date effectiveDate) {
		Criteria criteria = getSession().createCriteria(Contract.class);
		criteria.add(Restrictions.eq("policyNumber", policyNumber));
		criteria.add(Restrictions.eq("certificateNumber", certificateNumber));
		criteria.add(Restrictions.le("effectiveDate", effectiveDate));
		criteria.add(Restrictions.ge("expiryDate", effectiveDate));		
		return (Contract) criteria.uniqueResult();
	}

	@Override
	public Endorsement getEndosement(int contractId, int endorsementId) {
		return (Endorsement) getSession().get(Endorsement.class, new EndorsementId(endorsementId,contractId));
	}
	
	//WBB: 2011-01-11
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#findLockPendencies(int, int, Integer, int)
	 */
	public List<LockPendency> findLockPendencies(int endorsementId, int contractId, Integer itemId, int pendencyId) {
		Criteria criteria = getSession().createCriteria(LockPendency.class);
		criteria.add(Restrictions.eq("id.endorsementID", endorsementId));
		criteria.add(Restrictions.eq("id.contractID", contractId));
		if (itemId != null)
			criteria.add(Restrictions.eq("itemID", itemId));
		criteria.add(Restrictions.eq("pendencyID", pendencyId));	
		return criteria.list();
	}
	
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#findLockPendencies(int, int, int)
	 */
	public List<LockPendency> findLockPendencies(int endorsementId, int contractId, int pendencyId) {
		Criteria criteria = getSession().createCriteria(LockPendency.class);
		criteria.add(Restrictions.eq("id.endorsementID", endorsementId));
		criteria.add(Restrictions.eq("id.contractID", contractId));
		criteria.add(Restrictions.eq("pendencyID", pendencyId));	
		return criteria.list();
	}
	
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#findLockPendencies(int, int)
	 */
	public List<LockPendency> findLockPendencies(int endorsementId, int contractId) {
		Criteria criteria = getSession().createCriteria(LockPendency.class);
		criteria.add(Restrictions.eq("id.endorsementID", endorsementId));
		criteria.add(Restrictions.eq("id.contractID", contractId));
		return criteria.list();
	} 
	
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#findInstallment(int, int)
	 */
	public Installment findInstallment(int contractId, int endorsementId, int installmentId) {
		Criteria criteria = getSession().createCriteria(Installment.class);
		criteria.add(Restrictions.eq("id.contractId", contractId));
		criteria.add(Restrictions.eq("id.endorsementId", endorsementId));
		criteria.add(Restrictions.eq("id.installmentId", installmentId));	
		return (Installment) criteria.uniqueResult();
	}
	
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#findInstallment(int, int)
	 */
	public Installment findInstallment(long proposalNumber, int installmentID) {
		Query query = getSession().createQuery("from Installment i where ( i.endorsement.proposalNumber = :proposalNumber) and (i.id.installmentId = :installmentId)");
		query.setLong("proposalNumber",proposalNumber);
		query.setInteger("installmentId",installmentID);
		return (Installment) query.uniqueResult();
	}
	
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#salvaHistorico(br.com.tratomais.core.model.pendencies.LockPendency, java.lang.String, int, java.util.Date, java.lang.Integer, java.lang.Integer)
	 */
	public void salvaHistorico(LockPendency lockPendency, String descricao, int historyStatus, Date issueDate, Integer userID, Integer lockPendencyStatus) {
		LockPendencyHistory historyPendency = new LockPendencyHistory();
		historyPendency.setContractID(lockPendency.getId().getContractID());
		historyPendency.setEndorsementID(lockPendency.getId().getEndorsementID());
		historyPendency.setLockID(lockPendency.getId().getLockID());
		historyPendency.setDescription(descricao);
		historyPendency.setHistoryType(historyStatus);
		historyPendency.setIssueDate(issueDate==null?new Date():issueDate);
		historyPendency.setUserID(userID==null?1:userID);
		historyPendency.setLockPendencyStatus(lockPendencyStatus);
		getSession().save(historyPendency);
	}
	
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#historico(br.com.tratomais.core.model.pendencies.LockPendency, java.lang.String, int, java.util.Date, java.lang.Integer, java.lang.Integer)
	 */
	public void historico(LockPendency lockPendency, String descricao, int historyStatus, Date issueDate, Integer userID, Integer lockPendencyStatus) {
		salvaHistorico(lockPendency, descricao, historyStatus, issueDate, userID, lockPendencyStatus);
	}
	
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#getDataOption(int)
	 */
	public DataOption getDataOption(int DataOptionId) {
		return (DataOption) getSession().get(DataOption.class, DataOptionId);
	}
	
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#findPendency(int, int, java.lang.Integer, int)
	 */
	public LockPendency findPendency(int contractId, int endorsementId, Integer itemId, int pendencyId) {
		Criteria criteria = getSession().createCriteria(LockPendency.class);
		criteria.add(Restrictions.eq("id.contractID", contractId));
		criteria.add(Restrictions.eq("id.endorsementID", endorsementId));
		if (itemId != null)
			criteria.add(Restrictions.eq("itemID", itemId));
		criteria.add(Restrictions.eq("pendencyID", pendencyId));
		return (LockPendency) criteria.uniqueResult();
	}
	
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#findItem(int, int, int)
	 */
	public Item findItem(int contractId, int endorsementId, int itemId) {
		Criteria criteria = getSession().createCriteria(Item.class);
		criteria.add(Restrictions.eq("id.contractId", contractId));
		criteria.add(Restrictions.eq("id.endorsementId", endorsementId));
		criteria.add(Restrictions.eq("id.itemId", itemId));
		return (Item) criteria.uniqueResult(); 
	}
	
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#findItemPersonalRiskGroup(int, int, int, int)
	 */
	public ItemPersonalRiskGroup findItemPersonalRiskGroup(int contractId, int endorsementId, int itemId, int riskGroupId) {
		Criteria criteria = getSession().createCriteria(ItemPersonalRiskGroup.class);
		criteria.add(Restrictions.eq("id.contractId", contractId));
		criteria.add(Restrictions.eq("id.endorsementId", endorsementId));
		criteria.add(Restrictions.eq("id.itemId", itemId));
		criteria.add(Restrictions.eq("id.riskGroupId", riskGroupId));
		return (ItemPersonalRiskGroup) criteria.uniqueResult(); 
	}
	
	/**
	 * Return action to the claim
	 * 
	 * @param {@link ClaimResponse}
	 * @return {@link ClaimAction}
	 */
	public ClaimAction getClaimAction(ClaimResponse claimResponse) {
		Criteria criteria = getSession().createCriteria(ClaimAction.class);
		criteria.add(Restrictions.eq("id.claimEffectID", claimResponse.getClaimEffectID()));
		criteria.add(Restrictions.eq("id.coverageID", claimResponse.getCoverageId()));
		criteria.add(Restrictions.eq("id.insuredMain", ((claimResponse.getItemId() == 1) && ((claimResponse.getRiskGroupId()==null?0:claimResponse.getRiskGroupId()) == 0)) ));
		criteria.add(Restrictions.eq("id.claimStatus", claimResponse.getClaimStatus()));
		criteria.add(Restrictions.le("id.effectiveDate", claimResponse.getClaimDate()));
		criteria.add(Restrictions.ge("expiryDate", claimResponse.getClaimDate()));
		return (ClaimAction) criteria.uniqueResult();
	}
	
	public ClaimActionLock getClaimActionLock(ClaimResponse claimResponse, int issuanceType){
		Criteria criteria = getSession().createCriteria(ClaimActionLock.class);
		criteria.add(Restrictions.eq("id.claimEffectId", claimResponse.getClaimEffectID()));
		criteria.add(Restrictions.eq("id.coverageId", claimResponse.getCoverageId()));
		criteria.add(Restrictions.eq("id.insuredMain", ((claimResponse.getItemId() == 1) && ((claimResponse.getRiskGroupId()==null?0:claimResponse.getRiskGroupId()) == 0)) ));
		criteria.add(Restrictions.eq("id.claimStatus", claimResponse.getClaimStatus()));
		criteria.add(Restrictions.eq("id.issuanceType", issuanceType));
		criteria.add(Restrictions.le("id.effectiveDate", claimResponse.getClaimDate()));
		criteria.add(Restrictions.ge("expiryDate", claimResponse.getClaimDate()));
		return (ClaimActionLock) criteria.uniqueResult();		
	}
	
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#getClaimCause(java.lang.Integer)
	 */
	public ClaimCause getClaimCause(Integer claimCauseID) {
		return (ClaimCause) getSession().get(ClaimCause.class, claimCauseID);
	}
	
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#getClaimEffect(java.lang.Integer)
	 */
	public ClaimEffect getClaimEffect(Integer claimEffectID) {
		return (ClaimEffect) getSession().get(ClaimEffect.class, claimEffectID);
	}
	
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#getCoverage(java.lang.Integer)
	 */
	public Coverage getCoverage(Integer coverageId) {
		return (Coverage) getSession().get(Coverage.class, coverageId);
	}
	
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#getChannel(java.lang.Integer)
	 */
	public Channel getChannel(Integer channelId) {
		return (Channel) getSession().get(Channel.class, channelId);
	}
	
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#save(java.lang.Object)
	 */
	public Object save(Object obj){
		return super.getHibernateTemplate().merge(obj);
	}
	
	/* (non-Javadoc)
	 * @see br.com.tratomais.core.dao.IDaoPolicy#saveOrUpdate(java.lang.Object)
	 */
	public void saveOrUpdate(Object obj) {
		super.getHibernateTemplate().saveOrUpdate(obj);
	}
	
	@Override
	public List<Contract> listContractsPerMasterPolicy(MasterPolicy masterPolicy){
		Query sqlQuery = getSession().getNamedQuery("listContractsPerMasterPolicy");
		sqlQuery.setLong("masterPolicyId", masterPolicy.getMasterPolicyId());
		sqlQuery.setInteger("contractStatus", Contract.CONTRACT_STATUS_ACTIVE);
		sqlQuery.setInteger("issuingStatus", Endorsement.ISSUING_STATUS_EMITIDA);
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, 0 - masterPolicy.getRenewalBeforeDay());
		sqlQuery.setDate("expiryDate", calendar.getTime());
		return sqlQuery.list();
	}

	@Override
	public List<Endorsement> listEndorsementForEmission() {
		Session session = getSession();
		Query query = session.createQuery("select distinct e from Endorsement as e inner join e.installments as i " +
				" where  (i.shippingCollection = :shippingCollection) " +
				"   and ((e.issuanceType = :issuanceTypeEmission) or (e.issuanceType = :issuanceTypeChangeTechnical)) " +
				"   and ((e.issuingStatus = :statusProposta) or (e.issuingStatus = :statusEmitida) or (e.issuingStatus = :statusAnulada)) " +
				"   and  (e.endorsementType = :endorsementType)");
		query.setBoolean("shippingCollection", false);
		query.setInteger("issuanceTypeEmission", Endorsement.ISSUANCE_TYPE_POLICY_EMISSION);
		query.setInteger("issuanceTypeChangeTechnical", Endorsement.ISSUANCE_TYPE_POLICY_CHANGE_TECHNICAL);
		query.setInteger("statusProposta", Endorsement.ISSUING_STATUS_PROPOSTA);
		query.setInteger("statusEmitida", Endorsement.ISSUING_STATUS_EMITIDA);
		query.setInteger("statusAnulada", Endorsement.ISSUING_STATUS_ANULADA);
		query.setInteger("endorsementType", Endorsement.ENDORSEMENT_TYPE_COLLECTION);
		return query.list();
	}

	/** {@inheritDoc} */
	public Contract getContract(int contractId){
		return (Contract) getSession().get(Contract.class, contractId);		
	}

	@Override
	public Endorsement getLastValidEndosementWithPaymentOptions(int contractId) {
		Criteria criteria = getSession().createCriteria(Endorsement.class);
		criteria.add(Restrictions.eq("id.contractId", contractId));
		criteria.add(Restrictions.eq("endorsementType", Endorsement.ENDORSEMENT_TYPE_COLLECTION));
		criteria.addOrder(Order.desc("id.endorsementId"));
		criteria.setMaxResults(1);
		return (Endorsement) criteria.uniqueResult();
	}

	@Override
	public void flush() {
		getSession().flush();
	}

	@Override
	public Contract updateContract(Contract inputContract) {
		//Salva Contrato
		Contract workContract = new Contract();
		String[] ignorePropertiesContract = { "endorsements" };
		BeanUtils.copyProperties(inputContract, workContract, ignorePropertiesContract);
		workContract = saveContract(workContract);
		return workContract;
	}

	@Override
	public Transaction beginTransaction() {
		//return getHibernateTemplate().getSessionFactory().openSession().beginTransaction();
		return getSession().beginTransaction();
	}

	@Override
	public void saveRenewalAlert(List<RenewalAlert> renewalAlerts) {	
		for(RenewalAlert renewalAlert: renewalAlerts) {
			saveRenewalAlert(renewalAlert);
		}
	}

	@Override
	public RenewalAlert saveRenewalAlert(RenewalAlert renewalAlert) {
		return (RenewalAlert) this.getHibernateTemplate().merge(renewalAlert);
	}

	@Override
	public List<RenewalAlert> listRenewalAlert(EndorsementId endorsement) {
		Criteria criteria = getSession().createCriteria(RenewalAlert.class);
		criteria.add(Restrictions.eq("contractId", endorsement.getContractId()));
		criteria.add(Restrictions.eq("endorsementId", endorsement.getEndorsementId()));
		return criteria.list();
	}

	@Override
	public List<RenewalAlert> listRenewalAlert(EndorsementId endorsement, boolean resolved) {
		Criteria criteria = getSession().createCriteria(RenewalAlert.class);
		criteria.add(Restrictions.eq("contractId", endorsement.getContractId()));
		criteria.add(Restrictions.eq("endorsementId", endorsement.getEndorsementId()));
		criteria.add(Restrictions.eq("resolved", resolved));
		return criteria.list();
	}

	@Override
	public List<RenewalAlert> listRenewalAlert(int contractId, int endorsementId) {
		return listRenewalAlert(new EndorsementId(endorsementId, contractId));
	}

	@Override
	public List<RenewalAlert> listRenewalAlert(int contractId, int endorsementId, boolean resolved) {
		return listRenewalAlert(new EndorsementId(endorsementId, contractId), resolved);
	}
	
	@Override
	public List<Contract> listEndorsementRenewed() {
		Query sqlQuery = getSession().getNamedQuery("listEndorsementRenewed");
		sqlQuery.setInteger("contractStatus", Contract.CONTRACT_STATUS_ACTIVE);
		sqlQuery.setInteger("issuingStatus", Endorsement.ISSUING_STATUS_EMITIDA);
		sqlQuery.setDate("expiryDate", new Date());
		return sqlQuery.list();
	}
	
	@Override
	public List<Contract> listEndorsementNotRenewed() {
		Query sqlQuery = getSession().getNamedQuery("listEndorsementNotRenewed");
		sqlQuery.setInteger("contractStatus", Contract.CONTRACT_STATUS_ACTIVE);
		sqlQuery.setInteger("issuingStatus", Endorsement.ISSUING_STATUS_EMITIDA);
		ApplicationProperty applicationProperty = this.loadApplicationProperty(1, "DaysToContractFinalize");
		int numDias = - Integer.parseInt(applicationProperty.getValue());
		sqlQuery.setDate("expiryDate", DateUtilities.addDays(new Date(), numDias));
		return sqlQuery.list();
	}

	@Override
	public List<Contract> listContractRenewed(BigInteger policyNumber, Long certificateNumber) {
		Query sqlQuery = getSession().getNamedQuery("listContractRenewed");
		sqlQuery.setBigInteger("policyNumber", policyNumber);
		sqlQuery.setLong("certificateNumber", certificateNumber);
		return sqlQuery.list();
	}

	@Override
	public List<Profession> listProfessionByRefDate(Date refDate) {
		Criteria crit = getSession().createCriteria(Profession.class);
		return crit.list();
	}

	@Override
	public List<Occupation> listOccupationByRefDate(Date refDate) {
		Criteria crit = getSession().createCriteria(Occupation.class);
		return crit.list();
	}

	@Override
	public List<Activity> listActivityByRefDate(Date refDate) {
		Criteria crit = getSession().createCriteria(Activity.class);
		return crit.list();
	}

	@Override
	public List<Inflow> listInflowByRefDate(Date refDate) {
		Criteria crit = getSession().createCriteria(Inflow.class);
		
		List<Inflow> inflowList = crit.list();
		List<Inflow> inflowNewList = new ArrayList<Inflow>(); 
		
		MessageFormat messageFormat = null;
		
		Inflow inflow = null;
		//foreach object, change description
		for (Inflow inflowIntermediary : inflowList) {
			
			inflow = new Inflow();
			BeanUtils.copyProperties(inflowIntermediary, inflow);
			
			messageFormat = new MessageFormat(inflow.getDescription());
			
			inflow.setDisplayOrder(inflow.getDisplayOrder()+1);
			
			ConversionRate conversionRate = findConversionRateByRefDate(inflow.getCurrencyId(), null);
			
			NumberFormat formatter = new DecimalFormat("#,###.00");
			
			Object[] inflowArgs = {formatter.format((inflow.getInflowOf() * conversionRate.getMultipleRateBy())),
								   formatter.format((inflow.getInflowUntil() * conversionRate.getMultipleRateBy()))};
			
			inflow.setDescription(messageFormat.format(inflowArgs));

			formatter = new DecimalFormat("####.00");

			String inflowOf = formatter.format(inflow.getInflowOf() * conversionRate.getMultipleRateBy()).replace(",",".");
			String inflowUntil = formatter.format((inflow.getInflowUntil() * conversionRate.getMultipleRateBy())).replace(",",".");
			
			inflow.setInflowOf(new Double(inflowOf));
			inflow.setInflowUntil(new Double(inflowUntil));

			
			inflowNewList.add(inflow);
		}
		
		return inflowNewList;
	}
}
