/**
 * 
 */
package br.com.tratomais.core.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.tratomais.core.dao.IDaoCoveragePlan;
import br.com.tratomais.core.model.product.CoveragePlan;

/**
 * @author eduardo.venancio
 *
 */
public class DaoCoveragePlanImpl extends DaoGenericImpl<CoveragePlan, Integer> implements IDaoCoveragePlan {
	
	public DaoCoveragePlanImpl(){
		super();
	}
	
	public List<CoveragePlan> findByName(String name) {
		return super.findByCriteria(Restrictions.ilike("name", name, MatchMode.START));
	}
	
	@SuppressWarnings("unchecked")
	public CoveragePlan getCoveragePlanByRefDate(int productId, int planId, int coverageId, Date refDate){
		CoveragePlan newCoveragePlan = null;
		Criteria crit = getSession().createCriteria(CoveragePlan.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", planId ) );
		crit.add( Restrictions.eq( "id.coverageId", coverageId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		
		List <CoveragePlan> list = crit.list();
		for ( CoveragePlan coveragePlan : list ) {
			newCoveragePlan = coveragePlan;
			break;
		}
		
		return newCoveragePlan;
	}

	@SuppressWarnings("unchecked")
	public List<CoveragePlan> getCoveragePlanByRefDate(int productId, int planId, Date refDate){
		Criteria crit = getSession().createCriteria(CoveragePlan.class);
		
		crit.add( Restrictions.eq( "id.productId", productId ) );
		crit.add( Restrictions.eq( "id.planId", planId ) );
		crit.add( Restrictions.le( "id.effectiveDate", refDate ) );
		crit.add( Restrictions.ge( "expiryDate", refDate ) );
		
		return crit.list();		
	}

	@Override
	public CoveragePlan loadCoveragePlan(int coveragePlanId) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CoveragePlan> listCoveragePlan(int productID, Date versionDate) {
		Criteria crit = getSession().createCriteria(CoveragePlan.class);
		
		crit.add( Restrictions.eq( "id.productId", productID ) );
		crit.add( Restrictions.le( "id.effectiveDate", versionDate ) );
		crit.add( Restrictions.ge( "expiryDate", versionDate ) );
		crit.addOrder(Order.asc("displayOrder"));

		return crit.list();
	}	
}