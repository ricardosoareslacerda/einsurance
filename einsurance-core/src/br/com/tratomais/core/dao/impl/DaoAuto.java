package br.com.tratomais.core.dao.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import br.com.tratomais.core.dao.IDaoAuto;
import br.com.tratomais.core.model.auto.AutoBrand;
import br.com.tratomais.core.model.auto.AutoModel;
import br.com.tratomais.core.model.auto.AutoVersion;
import br.com.tratomais.core.model.auto.AutoVersionID;
/**
 * 
 * @author luiz.alberoni
 *
 */
public class DaoAuto extends HibernateDaoSupport implements IDaoAuto {

	@Override
	@SuppressWarnings("unchecked")
	public List<AutoBrand> listBrands() {
		Criteria criteria = getSession().createCriteria(AutoBrand.class);
		criteria.add(Restrictions.eq("active", true));
		return criteria.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<AutoModel> listModels(int brandId) {
		Criteria criteria = getSession().createCriteria(AutoModel.class);
		criteria.add(Restrictions.eq("autoMark.autoMarkId", brandId));
		criteria.add(Restrictions.eq("active", true));
		return criteria.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Integer> listModelYear(int autoModelId, Date referDate) {
		Query query = getSession().getNamedQuery("selectAutoYearPerAutoModelIdAndReferDate");
		query.setInteger("autoModelId", autoModelId);
		query.setDate("referDate", referDate);
		return query.list();
	}
 
	@Override
	public Double getAutoCost(int autoModelId, Integer year, Date referenceDate) {
		Query query = getSession().getNamedQuery("selectAutoValueReferPerAutoModelIdAndReferDateAndYearRefer");
		query.setInteger("autoModelId", autoModelId);
		query.setInteger("yearRefer", year);
		query.setDate("referDate", referenceDate);
		BigDecimal valueRefer = (BigDecimal) query.uniqueResult();
		if (valueRefer != null)
			return (Double)valueRefer.doubleValue();
		return null;
	}

	@Override
	public AutoVersion findAutoVersion(int autoModelId, Date referenceDate) {
		Criteria criteria = getSession().createCriteria(AutoVersion.class);
		criteria.add(Restrictions.eq("id.autoModelID", autoModelId));
		criteria.add(Restrictions.le("id.effectiveDate", referenceDate));
		criteria.add(Restrictions.ge("expiryDate", referenceDate));
		criteria.setMaxResults(1);
		return (AutoVersion) criteria.uniqueResult();
	}

	@Override
	public AutoVersion getAutoVersion(AutoVersionID autoVersionID) {
		return (AutoVersion) getSession().load(AutoVersion.class, autoVersionID);
	}

	@Override
	public AutoVersion getAutoVersion(Integer autoModeID, Date effectiveDate) {
		AutoVersionID autoVersionID = new AutoVersionID();
		autoVersionID.setAutoModelID(autoModeID);
		autoVersionID.setEffectiveDate(effectiveDate);
		return getAutoVersion(autoVersionID);
	}
}