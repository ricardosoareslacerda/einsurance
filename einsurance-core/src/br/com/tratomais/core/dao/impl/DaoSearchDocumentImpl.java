package br.com.tratomais.core.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.transform.Transformers;

import br.com.tratomais.core.dao.IDaoSearchDocument;
import br.com.tratomais.core.infrastructure.AuthenticationHelper;
import br.com.tratomais.core.model.search.InstallmentParameters;
import br.com.tratomais.core.model.search.InstallmentResult;
import br.com.tratomais.core.model.search.SearchDocumentParameters;
import br.com.tratomais.core.model.search.SearchDocumentResult;

@SuppressWarnings("unchecked")
public class DaoSearchDocumentImpl extends DaoGenericImpl<SearchDocumentResult, Integer> implements IDaoSearchDocument {

	public List<SearchDocumentResult> listSearchDocument(SearchDocumentParameters searchDocumentParameters) {
		return listSearchDocument(searchDocumentParameters, true);
	}
	
	public List<SearchDocumentResult> listSearchDocument(SearchDocumentParameters searchDocumentParameters, boolean isAuthentication){
		
		List<SearchDocumentResult> returnList=null;
		
		//get the original query and set parameters values
		Query query =  getSession().getNamedQuery("listSearchDocument");
		StringBuilder strSql = new StringBuilder();
		
		if(isAuthentication){
			strSql.append("  LEFT JOIN UserPartner ");
			strSql.append("    ON UserPartner.PartnerID = MasterPolicy.PartnerID "); 
			strSql.append(" WHERE UserPartner.UserID = ");
			strSql.append(AuthenticationHelper.getLoggedUser().getUserId());
		} else {
			strSql.append(" WHERE ");
		}
		
		//conditions to the new query
		if(searchDocumentParameters.getContractId() != 0){
			if(strSql.toString().trim().length() > 5){
				strSql.append(" AND ");
			}
			
			strSql.append("endorsement.contractId = ");
			strSql.append(searchDocumentParameters.getContractId());
		}
		
		if(searchDocumentParameters.getCertificateNumber() != null && !"".equals(searchDocumentParameters.getCertificateNumber().trim())){
			if(strSql.toString().trim().length() > 5){
				strSql.append(" AND ");
			}
			
			strSql.append("contract.certificateNumber = ");
			strSql.append(searchDocumentParameters.getCertificateNumber().trim());
		}		

		if(searchDocumentParameters.getPolicyNumber() != null && !"".equals(searchDocumentParameters.getPolicyNumber().trim())){
			if(strSql.toString().trim().length() > 5){
				strSql.append(" AND ");
			}
			
			strSql.append("contract.policyNumber = ");
			strSql.append(searchDocumentParameters.getPolicyNumber().trim());
		}
		
		if(searchDocumentParameters.getName() != null && !"".equals(searchDocumentParameters.getName().trim())){
			if(strSql.toString().trim().length() > 5){
				strSql.append(" AND ");
			}
			
			strSql.append("UPPER(customer.name) like UPPER('%");
			strSql.append(searchDocumentParameters.getName().concat("%')")); 
		}
		
		if(searchDocumentParameters.getDocumentType() != null && !searchDocumentParameters.getDocumentType().equals(0)){
			if(strSql.toString().trim().length() > 5){
				strSql.append(" AND ");
			}
			
			strSql.append("customer.documentType = ");
			strSql.append(searchDocumentParameters.getDocumentType()); 
		}
		
		if(searchDocumentParameters.getDocumentNumber() != null && !"".equals(searchDocumentParameters.getDocumentNumber().trim())){
			if(strSql.toString().trim().length() > 5){
				strSql.append(" AND ");
			}
			
			strSql.append("customer.documentNumber = '");
			strSql.append(searchDocumentParameters.getDocumentNumber().trim().concat("'")); 
		}
		
		if(searchDocumentParameters.getRenewedOnly() != null && searchDocumentParameters.getRenewedOnly()){
			if(strSql.toString().trim().length() > 5){
				strSql.append(" AND ");
			}
			
			strSql.append("contract.Renewed = ");
			strSql.append(searchDocumentParameters.getRenewedOnly() ? 1 : 0); 
		}
		
		strSql.append(" ORDER BY policyNumber, certificateNumber, contractId, endorsementId");

		query = getSession().createSQLQuery(query.getQueryString().concat(strSql.toString()));
				
		if(searchDocumentParameters.getTotalPage() > 0){
			query.setFirstResult(searchDocumentParameters.getPage());
			query.setMaxResults(searchDocumentParameters.getTotalPage());
		}
		
		query.setResultTransformer(Transformers.aliasToBean(SearchDocumentResult.class));
		returnList = query.list();
		
		return returnList;
	}
	
	public int listSearchDocumentTotalRecords(SearchDocumentParameters searchDocumentParameters) {
		return listSearchDocumentTotalRecords(searchDocumentParameters, true);
	}
	
	public int listSearchDocumentTotalRecords(SearchDocumentParameters searchDocumentParameters, boolean isAuthentication) {
	
		//get the original query and set parameters values
		Query query =  getSession().getNamedQuery("listSearchDocument");
		StringBuilder strSql = new StringBuilder();
		
		if(isAuthentication){
			strSql.append("  LEFT JOIN UserPartner ");
			strSql.append("    ON UserPartner.PartnerID = MasterPolicy.PartnerID "); 
			strSql.append(" WHERE UserPartner.UserID = ");
			strSql.append(AuthenticationHelper.getLoggedUser().getUserId());
		} else {
			strSql.append(" WHERE ");
		}
		
		//conditions to the new query
		if(searchDocumentParameters.getContractId() != 0){
			if(strSql.toString().trim().length() > 5){
				strSql.append(" AND ");
			}
			
			strSql.append("endorsement.contractId = ");
			strSql.append(searchDocumentParameters.getContractId());
		}
		
		if(searchDocumentParameters.getCertificateNumber() != null && !"".equals(searchDocumentParameters.getCertificateNumber().trim())){
			if(strSql.toString().trim().length() > 5){
				strSql.append(" AND ");
			}
			
			strSql.append("contract.certificateNumber = ");
			strSql.append(searchDocumentParameters.getCertificateNumber().trim());
		}		

		if(searchDocumentParameters.getPolicyNumber() != null && !"".equals(searchDocumentParameters.getPolicyNumber().trim())){
			if(strSql.toString().trim().length() > 5){
				strSql.append(" AND ");
			}
			
			strSql.append("contract.policyNumber = ");
			strSql.append(searchDocumentParameters.getPolicyNumber().trim());
		}
		
		if(searchDocumentParameters.getName() != null && !"".equals(searchDocumentParameters.getName().trim())){
			if(strSql.toString().trim().length() > 5){
				strSql.append(" AND ");
			}
			
			strSql.append("UPPER(customer.name) like UPPER('%");
			strSql.append(searchDocumentParameters.getName().trim().concat("%')")); 
		}
		
		if(searchDocumentParameters.getDocumentType() != null && !searchDocumentParameters.getDocumentType().equals(0)){
			if(strSql.toString().trim().length() > 5){
				strSql.append(" AND ");
			}
			
			strSql.append("customer.documentType = ");
			strSql.append(searchDocumentParameters.getDocumentType()); 
		}
		
		if(searchDocumentParameters.getDocumentNumber() != null && !"".equals(searchDocumentParameters.getDocumentNumber().trim())){
			if(strSql.toString().trim().length() > 5){
				strSql.append(" AND ");
			}
			
			strSql.append("customer.documentNumber = '");
			strSql.append(searchDocumentParameters.getDocumentNumber().trim().concat("'")); 
		}
		
		if(searchDocumentParameters.getRenewedOnly() != null && searchDocumentParameters.getRenewedOnly()){
			if(strSql.toString().trim().length() > 5){
				strSql.append(" AND ");
			}
			
			strSql.append("contract.Renewed = ");
			strSql.append(searchDocumentParameters.getRenewedOnly() ? 1 : 0); 
		}
		
		strSql.append(" ORDER BY policyNumber, certificateNumber, contractId, endorsementId");

		query = getSession().createSQLQuery(query.getQueryString().concat(strSql.toString()));

		query.setResultTransformer(Transformers.aliasToBean(SearchDocumentResult.class));
		
		return query.list().size();
	}

	@Override
	public List<InstallmentResult> listInstallment(InstallmentParameters installmentParameters) {
		List<InstallmentResult> returnList = null;
		
		Query query =  getSession().getNamedQuery("listInstallment");
		StringBuilder sb = new StringBuilder();
		
		if(installmentParameters.getContractId() > 0){
			sb.append(" WHERE Contract.ContractID = "); 
			sb.append(installmentParameters.getContractId());
		}
		
		if(installmentParameters.getPolicyNumber() != null && !"".equals(installmentParameters.getPolicyNumber().trim())){
			if(sb.length() == 0)
				sb.append(" WHERE ");
			else
				sb.append(" AND ");
			
			sb.append("Contract.policyNumber = ");
			sb.append(installmentParameters.getPolicyNumber().trim());
		}

		if(installmentParameters.getCertificateNumber() != null && !"".equals(installmentParameters.getCertificateNumber().trim())){
			if(sb.length() == 0)
				sb.append(" WHERE ");
			else
				sb.append(" AND ");
			
			sb.append("Contract.certificateNumber = ");
			sb.append(installmentParameters.getCertificateNumber().trim());
		}
		
		if(installmentParameters.getDocumentTypeID() != 0){
			if(sb.length() == 0)
				sb.append(" WHERE ");
			else
				sb.append(" AND ");
			
			sb.append("customer.documentType = ");
			sb.append(installmentParameters.getDocumentTypeID());
		}
		
		if(installmentParameters.getDocumentNumber() != null && !"".equals(installmentParameters.getDocumentNumber().trim())){
			if(sb.length() == 0)
				sb.append(" WHERE ");
			else
				sb.append(" AND ");
			
			sb.append("customer.documentNumber = '");
			sb.append(installmentParameters.getDocumentNumber().trim().concat("'"));
		}
		
		if(installmentParameters.getStatusId() != 0){
			if(sb.length() == 0)
				sb.append(" WHERE ");
			else
				sb.append(" AND ");
			
			sb.append("installment.installmentStatus = ");
			sb.append(installmentParameters.getStatusId());
		}
		
		if(installmentParameters.isInArrears()){
			if(sb.length() == 0)
				sb.append(" WHERE ");
			else
				sb.append(" AND ");
			sb.append("getdate() > installment.issueDate");
			sb.append(" AND installment.installmentStatus = 150 ");
		}
		
		sb.append(" ORDER BY contract.policyNumber, contract.certificateNumber, contract.contractId, endorsement.endorsementId, installmentId");
		
		query = getSession().createSQLQuery(query.getQueryString().concat(sb.toString()));
		
		if(installmentParameters.getTotalPage() > 0){
			query.setFirstResult(installmentParameters.getPage());
			query.setMaxResults(installmentParameters.getTotalPage());
		}
		
		query.setResultTransformer(Transformers.aliasToBean(InstallmentResult.class));
		returnList = query.list();
		
		return returnList;
	}
}
