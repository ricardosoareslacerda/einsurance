package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.customer.Broker;

public interface IDaoBroker extends IDaoGeneric<Broker, Integer> {

	public List<Broker> findByNickName(String userName);
	public List<Broker> findByName(String name);
	/**
	 * Checks for Broker's external code existence
	 * @param insurerId 
	 * @param brokerId
	 * @param externalCode
	 * @return true if exists 
	 */	
	public boolean brokerExternalCodeExists(int insurerId, int brokerId, String externalCode);
    
	/**
     * list all Broker from the user
     * @param partnerId
     * @param productId
     * @param brokerId
     * @param userId
     * @return List<Broker>
     */
	public List<Broker> listBrokerPerProductOrPartner(Integer partnerId, Integer productId, Integer brokerId, Integer userId);
	public List<Broker> listBroker(int insurerID, String regulatoryCode, Date refDate);
	
}
