package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.PersonalRiskOption;

public interface IDaoPersonalRiskOption extends IDaoGeneric<PersonalRiskOption, Integer> {

	public List<PersonalRiskOption> findPersonalRiskOptions(int productId, int riskPlanId, Date referenceDate);

	public List<PersonalRiskOption> findPersonalRiskOptions();

}
