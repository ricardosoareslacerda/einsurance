package br.com.tratomais.core.dao;

import java.util.List;

import br.com.tratomais.core.model.search.InstallmentParameters;
import br.com.tratomais.core.model.search.InstallmentResult;
import br.com.tratomais.core.model.search.SearchDocumentParameters;
import br.com.tratomais.core.model.search.SearchDocumentResult;

public interface IDaoSearchDocument extends IDaoGeneric<SearchDocumentResult, Integer>{

	public List<SearchDocumentResult> listSearchDocument(SearchDocumentParameters searchDocumentParameters);
	public int listSearchDocumentTotalRecords(SearchDocumentParameters searchDocumentParameters);
	
	public List<InstallmentResult> listInstallment(InstallmentParameters installmentParameters);
	public List<SearchDocumentResult> listSearchDocument(SearchDocumentParameters parameters, boolean isAuthentication);
	
}
