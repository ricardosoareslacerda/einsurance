package br.com.tratomais.core.dao;

import java.util.Date;

import br.com.tratomais.core.model.product.ConversionRate;

public interface IDaoConversionRate extends IDaoGeneric< ConversionRate, Integer >{

	public ConversionRate findConversionRateByRefDate(Integer currencyId, Date refDate);

}
