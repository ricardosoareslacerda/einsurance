package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.product.Deductible;

public interface IDaoDeductible extends IDaoGeneric<Deductible, Integer> {
	public List<Deductible> findByCoverageIdReferenceDate(int coverageId, Date refDate);
}
