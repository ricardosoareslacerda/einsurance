package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.policy.Item;
import br.com.tratomais.core.model.product.CoverageRule;
import br.com.tratomais.core.model.product.CoverageRuleValue;

public interface IDaoCoverageRule extends IDaoGeneric<CoverageRule, Integer> {
	
	/**
	 * Find the coverage rule list by product, coverage and rule type.
	 * 
	 * @param productId product id
	 * @param coverageId coverage id
	 * @param ruleType rule type
	 * @param refDate reference date
	 * @return list of coverage rules
	 */
	public List<CoverageRule> getCoverageRuleList(int productId, int coverageId, int ruleType, Date refDate);

	/**
	 * Find the coverage rule value by rule, item and reference date.
	 * 
	 * @param coverageRule coverage rule
	 * @param item risk item
	 * @param refDate reference date
	 * @return coverage rule value
	 */
	public CoverageRuleValue getValueByCoverageRule(CoverageRule coverageRule, Item item, Date refDate );
}
