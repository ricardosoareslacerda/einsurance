package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.City;
import br.com.tratomais.core.model.State;
import br.com.tratomais.core.model.product.IdentifiedList;

public interface IDaoCity extends IDaoGeneric< City, Integer > {

	public List<City> findByName(String name); 
	
	public List<City> findByName(Integer id); 
	
	public List<City> listCity(Integer stateId);
	
	public IdentifiedList listCityByState(IdentifiedList identifiedList, int stateId);

	public IdentifiedList listIdentifiedListCity(IdentifiedList identifiedList);

	public List<City> listCityByRefDate(int countryID, int stateID, Date refDate);
	
	/**
	 * Procura por cidade, ignorando mai�sculo/min�sculo e acentos
	 * @param state Estado
	 * @param name Nome do munic�pio
	 * @return Lista de cidades
	 */
	public List<City> findByNameNoAccents(State state, String name);
}
