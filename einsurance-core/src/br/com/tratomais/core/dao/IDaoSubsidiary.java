package br.com.tratomais.core.dao;

import java.util.Date;
import java.util.List;

import br.com.tratomais.core.model.customer.Subsidiary;
import br.com.tratomais.core.util.TransportationClass;

public interface IDaoSubsidiary extends IDaoGeneric<Subsidiary, Integer> {

	public List<Subsidiary> findByNickName(String userName);
	public List<Subsidiary> findByName(String name);
	/**
	 * Checks for Subsidiarry's external code existence
	 * @param insurerId 
	 * @param subsidiaryId
	 * @param externalCode
	 * @return true if exists 
	 */
	public boolean subsidiaryExternalCodeExists(int insurerId, int subsidiaryId, String externalCode);	
	/**
	 * Query to list the subsidiarys (used in gridview)
	 * 
	 * @param limit: Number of rows to return.  If this is not set all rows will be returned.
	 * @param page: The page number to return (if paginating).
	 * @param columnSorted: Field to sort the output by.
	 * @param typeSorted: Sort order.  Either 'asc' (ascending) or 'desc' (descending).
	 * @param search: True or False (whether or not this is a search). Default is False. 
	 * Also takes strings of 'true' and 'false'.
	 * @param searchField: The field we're searching on.
	 * @param searchString: The string to match for our search.
	 * @return List<Subsidiary>
	 */
	public TransportationClass<Subsidiary> listSubsidiaryToGrid(int limit, int page, String columnSorted, String typeSorted,
																	String search, String searchField, String searchString);
	public List<Subsidiary> listSubsidiary(int insurerID, Date refDate);	
}
