package br.com.tratomais.core.dao;

import java.util.Date;

import br.com.tratomais.core.model.interfaces.Lot;
import br.com.tratomais.core.model.product.IdentifiedList;

public interface IDaoLot extends IDaoGeneric<Lot, Integer>{
	
	public Lot getLot(Integer lotId);
	
	public Lot getLot(Integer lotId, Integer parentId, Integer exchangeId, Integer insurerId, Integer partnerId);

	public IdentifiedList listLot(IdentifiedList identifiedList, Integer exchangeId, Integer lotStatus, Date rangeBefore, Date rangeAfter);

}
