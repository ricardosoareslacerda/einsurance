package br.com.tratomais.core.dao;

import java.util.List;

import br.com.tratomais.core.model.Login;

public interface IDaoLogin extends IDaoGeneric<Login, Integer> {

	public Login findByLogin(String userName, String domain);

	public List<Login> findByNameDomain(String domain);
}
