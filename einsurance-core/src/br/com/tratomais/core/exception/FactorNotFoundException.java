package br.com.tratomais.core.exception;

public class FactorNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1330578135186253609L;

	public FactorNotFoundException() {
	}

	public FactorNotFoundException(String message) {
		super(message);
	}

	public FactorNotFoundException(Throwable cause) {
		super(cause);
	}

	public FactorNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

}
