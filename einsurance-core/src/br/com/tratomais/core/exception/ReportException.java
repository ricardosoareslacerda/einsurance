package br.com.tratomais.core.exception;

public class ReportException extends Exception {
	private static final long serialVersionUID = 4265489072197148540L;
	ReportException(String msg) {
		super(msg);
	}
	ReportException(Exception e) {
		super(e);
	}
}
