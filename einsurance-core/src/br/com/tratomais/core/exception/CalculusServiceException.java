/**
 * 
 */
package br.com.tratomais.core.exception;

/**
 * @author luiz.alberoni
 *
 */
public class CalculusServiceException extends ServiceException {

	/**
	 * Eclipse generated
	 */
	private static final long serialVersionUID = 8071809651045162070L;

	/**
	 * @param message
	 */
	public CalculusServiceException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public CalculusServiceException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CalculusServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
