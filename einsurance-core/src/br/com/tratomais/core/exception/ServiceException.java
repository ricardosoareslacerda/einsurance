/**
 * 
 */
package br.com.tratomais.core.exception;

/**
 * Represents all Service Exceptions
 * @author luiz.alberoni
 */
public class ServiceException extends Exception {

	/**
	 * Eclipse generated
	 */
	private static final long serialVersionUID = 2121404984925752695L;

	/**
	 * @param message
	 */
	public ServiceException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ServiceException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
