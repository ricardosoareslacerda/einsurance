package br.com.tratomais.core.claim;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.axis2.AxisFault;
import org.springframework.beans.factory.annotation.Autowired;

import ve.com.zurich.as400.siniestro.SiniestroServiceStub;
import ve.com.zurich.as400.siniestro.SiniestroServiceStub.Agencia_type1;
import ve.com.zurich.as400.siniestro.SiniestroServiceStub.Certificado_type1;
import ve.com.zurich.as400.siniestro.SiniestroServiceStub.Consulta;
import ve.com.zurich.as400.siniestro.SiniestroServiceStub.ConsultaRespuesta;
import ve.com.zurich.as400.siniestro.SiniestroServiceStub.ConsultaSiniestro;
import ve.com.zurich.as400.siniestro.SiniestroServiceStub.FechaConsulta_type1;
import ve.com.zurich.as400.siniestro.SiniestroServiceStub.Poliza_type1;
import br.com.tratomais.core.dao.IDaoCoverage;
import br.com.tratomais.core.dao.IDaoDataOption;
import br.com.tratomais.core.dao.IDaoPolicy;
import br.com.tratomais.core.dao.IDaoProduct;
import br.com.tratomais.core.dao.impl.DaoClaimCause;
import br.com.tratomais.core.dao.impl.DaoClaimEffect;
import br.com.tratomais.core.dao.impl.DaoContract;
import br.com.tratomais.core.model.claim.ClaimActionLock;
import br.com.tratomais.core.model.claim.ClaimCause;
import br.com.tratomais.core.model.claim.ClaimEffect;
import br.com.tratomais.core.model.policy.Contract;
import br.com.tratomais.core.model.policy.ItemSequence;
import br.com.tratomais.core.model.product.Coverage;
import br.com.tratomais.core.model.product.DataGroup;
import br.com.tratomais.core.model.product.DataOption;
import br.com.tratomais.core.model.product.Product;
import br.com.tratomais.esb.interfaces.model.response.ClaimResponse;
import br.com.tratomais.general.utilities.NumberUtilities;

/**
 * AS400 implementation.
 * 
 * @see br.com.tratomais.core.claim.CheckStateClaim
 */
public class CheckStateClaimAS400 implements CheckStateClaim {

	private static final Integer CONTRACT_NOT_FOUND = 1;
	private static final Integer SEQUENCE_ITEM_NOT_FOUND_ERROR = 2;
	private static final Integer COVERAGE_NOT_FOUND = 3;
	private static final Integer CLAIM_CAUSE_NOT_FOUND = 4;
	private static final Integer CLAIM_EFFECT_NOT_FOUND = 5;
	private static final Integer CLAIM_STATUS_NOT_FOUND = 6;

	public CheckStateClaimAS400() {
		super();
	}
	@Autowired
	private DaoContract daoContract;
	public void setDaoContract(DaoContract daoContract) {
		this.daoContract = daoContract;
	}

	@Autowired
	private IDaoProduct daoProduct;
	public void setDaoProduct(IDaoProduct daoProduct) {
		this.daoProduct = daoProduct;
	}

	@Autowired
	private IDaoCoverage daoCoverage;
	public void setDaoCoverage(IDaoCoverage daoCoverage) {
		this.daoCoverage = daoCoverage;
	}

	@Autowired
	private DaoClaimCause daoClaimCause;
	public void setDaoClaimCause(DaoClaimCause daoClaimCause) {
		this.daoClaimCause = daoClaimCause;
	}

	@Autowired
	private DaoClaimEffect daoClaimEffect;
	public void setDaoClaimEffect(DaoClaimEffect daoClaimEffect) {
		this.daoClaimEffect = daoClaimEffect;
	}

	@Autowired
	private IDaoDataOption daoDataOption;
	public void setDaoDataOption(IDaoDataOption daoDataOption) {
		this.daoDataOption = daoDataOption;
	}

	@Autowired
	private IDaoPolicy daoPolicy;
	private String endpoint;
	
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public void setDaoPolicy(IDaoPolicy daoPolicy) {
		this.daoPolicy = daoPolicy;
	}

	/** {@inheritDoc} */
	@Override
	public Map<String, Object> check(Map<String, Object> dados) {
		int issuanceType = (Integer) dados.get(PARAM_ISSUANCE_TYPE);
		HashMap<String, Object> mapa = new HashMap<String, Object>();
		List<String> erros = new LinkedList<String>();
		boolean comErro = false;
		try {
			SiniestroServiceStub siniestroService;
			if ((endpoint == null) || ("".equals(endpoint)))
				siniestroService = new SiniestroServiceStub();
			else
				siniestroService = new SiniestroServiceStub(endpoint);
			Consulta consulta = new Consulta();
			NumberFormat instance = NumberFormat.getNumberInstance();
			instance.setMinimumIntegerDigits(13);
			instance.setMaximumIntegerDigits(13);
			instance.setGroupingUsed(false);

			Poliza_type1 poliza = new Poliza_type1();
			BigInteger polizaBI = (BigInteger) dados.get(PARAM_POLICE);
			String polizaStr = instance.format(polizaBI);
			poliza.setPoliza_type0(polizaStr);
			consulta.setPoliza(poliza);

			instance.setMinimumIntegerDigits(6);
			instance.setMaximumIntegerDigits(6);
			Certificado_type1 certificado = new Certificado_type1();
			certificado.setCertificado_type0(instance.format((Long) dados.get(PARAM_CERTIFICATE)));
			consulta.setCertificado(certificado);

			Agencia_type1 agencia = new Agencia_type1();
			agencia.setAgencia_type0((String) dados.get(PARAM_AGENCY));
			consulta.setAgencia(agencia);

			FechaConsulta_type1 fecha = new FechaConsulta_type1();
			fecha.setFechaConsulta_type0(dateInIntFormat((Date) dados.get(PARAM_QUERY_DATE)));
			consulta.setFechaConsulta(fecha);

			ConsultaRespuesta consultaRespuesta = siniestroService.consulta(consulta);
			ConsultaSiniestro[] siniestros = consultaRespuesta.get_return();

			List<ClaimResponse> list = processResquest(siniestros);
			for (ClaimResponse claim : list) {
				if (claim.getErrorID() != null) {
					comErro = true;
					erros.add(claim.getErrorMessage());
				}
			}
			if (!comErro)
				for (ClaimResponse claim : list) {
					ClaimActionLock lock = daoPolicy.getClaimActionLock(claim, issuanceType);
					if (lock != null) {
						if (lock.getClaimActionLockType().equals(ClaimActionLock.LOCK_TYPE_TOTAL_BLOCK)) {
							comErro = true;
							erros.add(String.format("No es permitido realizar esta acci�n, debido la existencia del siniestro '%06d' !", claim.getClaimNumber()));
							break;
						}
					}
				}
			if (comErro) {
				mapa.put(CheckStateClaim.RESULT_ERRORS_MESSAGES, erros);
			}
			mapa.put(CheckStateClaim.RESULT_VAR, !comErro);
		} catch (AxisFault e) {
			mapa.put(CheckStateClaim.RESULT_ERRORS_MESSAGES, e.getMessage());
			mapa.put(CheckStateClaim.RESULT_VAR, Boolean.FALSE);
			e.printStackTrace();
		} catch (RemoteException e) {
			mapa.put(CheckStateClaim.RESULT_ERRORS_MESSAGES, e.getMessage());
			mapa.put(CheckStateClaim.RESULT_VAR, Boolean.FALSE);
			e.printStackTrace();
		}
		return mapa;
	}

	private String dateInIntFormat(Date date) {
		SimpleDateFormat fmt = new SimpleDateFormat();
		fmt.applyPattern("ddMMyyyy");
		return fmt.format(date);
	}

	private List<ClaimResponse> processResquest(ConsultaSiniestro[] siniestros) {
		List<ClaimResponse> claimRequests = new LinkedList<ClaimResponse>();
		if (siniestros != null) {
			for (ConsultaSiniestro consulta : siniestros) {
				ClaimResponse response = new ClaimResponse();
				BigInteger policyNumber = new BigInteger(consulta.getPoliza());
				Long certificateNumber = new Long(consulta.getCertificado());

				Contract contract = daoContract.getContract(policyNumber, certificateNumber);
				if (contract == null) {
					String err = String.format("Certificado '%09d' de la p�liza '%013d' resultante del siniestro, no localizado o inexistente!", certificateNumber, policyNumber);
					response.setErrorMessage(err);
					response.setErrorID(CONTRACT_NOT_FOUND);
				} else {
					Integer sequenceCode = new Integer(consulta.getItemAfectado());
					ItemSequence itemSequence = daoContract.getItemSequence(contract.getContractId(), sequenceCode);
					if (itemSequence == null) {
						String err = String.format("Item '%02d' resultante del siniestro, no localizado o inexistente!", sequenceCode);
						response.setErrorMessage(err);
						response.setErrorID(SEQUENCE_ITEM_NOT_FOUND_ERROR);
					} else {
						Product product = daoProduct.findById(contract.getProductId());
						String coverageCode = consulta.getCobertura().trim(), 
							   bien = consulta.getBien().trim();

						Coverage coverage = daoCoverage.getByCoverageCode(product.getObjectId(), coverageCode, bien);
						if (coverage == null) {
							String err = String.format("Cobertura '%s' y bien '%s' resultantes del siniestro, no localizados o inexistentes!", coverageCode, bien);
							response.setErrorMessage(err);
							response.setErrorID(COVERAGE_NOT_FOUND);
						} else {
							String causa = consulta.getCausa().trim(), 
								   efecto = consulta.getEfecto().trim(), 
								   estatus = consulta.getEstatus().trim();
							ClaimCause claimCause = daoClaimCause.getByClaimCauseCode(product.getObjectId(), causa);
							ClaimEffect claimEffect = daoClaimEffect.getByClaimEfectCode(product.getObjectId(), efecto);
							DataOption claimStatus = daoDataOption.getByExternalCode(DataGroup.GROUP_CLAIM_STATUS, estatus);

							if (claimCause == null) {
								String err = String.format("Causa '%s' resultante del siniestro, no localizada o inexistente!", causa);
								response.setErrorMessage(err);
								response.setErrorID(CLAIM_CAUSE_NOT_FOUND);
							} else if (claimEffect == null) {
								String err = String.format("Efecto '%s' resultante del siniestro, no localizado o inexistente!", efecto);
								response.setErrorMessage(err);
								response.setErrorID(CLAIM_EFFECT_NOT_FOUND);
							} else if (claimStatus == null) {
								String err = String.format("Estatus '%s' resultante del siniestro, no localizado o inexistente!", estatus);
								response.setErrorMessage(err);
								response.setErrorID(CLAIM_STATUS_NOT_FOUND);
							} else {
								response.setPolicyNumber(contract.getPolicyNumber());
								response.setCertificateNumber(certificateNumber);
								response.setChannelCode(consulta.getAgencia().trim());
								response.setClaimCauseID(claimCause.getClaimCauseID());
								response.setClaimDate(toDate(consulta.getFechaSiniestro()));
								response.setClaimEffectID(claimEffect.getClaimEffectID());
								response.setClaimNotification(toDate(consulta.getFechaNotificacion()));
								response.setClaimNumber(new Long(consulta.getNumeroSiniestro()));
								response.setClaimStatus(claimStatus.getDataOptionId());
								response.setClaimValue(NumberUtilities.roundDecimalPlaces(consulta.getMontoIndeminizacion().doubleValue(), 2));
								response.setCoverageId(coverage.getCoverageId());
								response.setItemId(itemSequence.getItemID());
								response.setRiskGroupId(itemSequence.getRiskGroupID() == 0 ? null : itemSequence.getRiskGroupID());
							}
						}
					}
				}
				claimRequests.add(response);
			}
		}
		return claimRequests;
	}

	/**
	 * Transforma a string para data
	 * 
	 * @param dateStr
	 * @return
	 */
	private Date toDate(String dateStr) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(dateStr.substring(
				0, 2)));
		calendar.set(Calendar.MONTH, Integer.valueOf(dateStr.substring(2, 4)));
		calendar.set(Calendar.YEAR, Integer.valueOf(dateStr.substring(4, 8)));
		return calendar.getTime();
	}

}
