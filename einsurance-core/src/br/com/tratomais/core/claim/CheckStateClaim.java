package br.com.tratomais.core.claim;

import java.util.Map;

/**
 * Checks for impeditive claim
 * @author luiz.alberoni
 */
public interface CheckStateClaim {
	/** Should be stored the result (java.lang.Boolean) of the check processing (True value denotes no pendencies) */
	static final String RESULT_VAR = "claim.result";
	/** To this 'variable', should be returned a list (java.util.List<String>) of error messages */
	static final String RESULT_ERRORS_MESSAGES = "claim.error.messages";
	/** Return endorsement to be used*/
	static final String PARAM_POLICE = "claim.police";
	/** Certificate's number */
	static final String PARAM_CERTIFICATE = "claim.certificate";
	/** Agency number */
	static final String PARAM_AGENCY = "claim.agency";
	/** Query's date */
	static final String PARAM_QUERY_DATE = "claim.query.date";
	/** Issuance Type */
	static final String PARAM_ISSUANCE_TYPE = "claim.issuranceType";
	
	/**
	 * Checks for impeditive claims
	 * @param args
	 * @return Map containing the processing results
	 */
	Map<String, Object> check(Map<String, Object> args);
}
