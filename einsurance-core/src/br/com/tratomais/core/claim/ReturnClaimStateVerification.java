package br.com.tratomais.core.claim;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Returns the state of a claim state verification
 * @author luiz.alberoni
 */
public class ReturnClaimStateVerification implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer erros;
	private List<String> errorMessages = new LinkedList<String>();
	private boolean result;
	public Integer getErros() {
		return erros;
	}
	public void setErros(Integer erros) {
		this.erros = erros;
	}
	public List<String> getErrorMessages() {
		return errorMessages;
	}
	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
}
