package br.com.tratomais.core.claim;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * AS400 implementation.
 * 
 * @see br.com.tratomais.core.claim.CheckStateClaim
 */
public class CheckStateClaimHelper implements CheckStateClaim {
	/** {@inheritDoc} */
	@Override
	public Map<String, Object> check(Map<String, Object> args) {
		HashMap<String, Object> mapa = new HashMap<String, Object>();
		mapa.put(CheckStateClaim.RESULT_VAR, this.returnedValue);
		mapa.put(CheckStateClaim.RESULT_ERRORS_MESSAGES, this.messages);
		return mapa;
	}

	private boolean returnedValue = false;
	private List<String> messages = new LinkedList<String>();

	public boolean isReturnedValue() {
		return returnedValue;
	}

	public void setReturnedValue(boolean returnedValue) {
		this.returnedValue = returnedValue;
	}

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
}
