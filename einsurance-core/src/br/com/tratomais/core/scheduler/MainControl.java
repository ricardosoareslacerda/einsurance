package br.com.tratomais.core.scheduler;

import it.sauronsoftware.cron4j.Scheduler;
import it.sauronsoftware.cron4j.Task;


public final class MainControl {
	private MainControl(){}
	
	private static final Scheduler scheduler = new Scheduler();
	
	public static String schedule(String schedulePattern, Runnable action){
		return scheduler.schedule(schedulePattern, action);
	}
	
	public static String schedule(String schedulePattern, Task task){
		return scheduler.schedule(schedulePattern, task);
	}
	
	public static Task getTaskForId(String taskId) {
		return scheduler.getTask(taskId);
	}
	
	/**
	 * Reagenda
	 * @param id
	 * @param pattern
	 */
	public static void reschedule(String id, String pattern){
		scheduler.reschedule(id, pattern);
	}
	
	public static void deschedule(String id){
		scheduler.deschedule(id);
	}
	
	public static void startScheduler() {
		scheduler.start();
	}
	
	public static void stopScheduler() {
		scheduler.stop();
	}
}
