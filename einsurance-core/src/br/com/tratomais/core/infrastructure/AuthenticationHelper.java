package br.com.tratomais.core.infrastructure;

import java.sql.Timestamp;

import org.springframework.context.ApplicationContext;
import org.springframework.security.Authentication;
import org.springframework.security.AuthenticationManager;
import org.springframework.security.BadCredentialsException;
import org.springframework.security.context.SecurityContextHolder;
import org.springframework.security.providers.UsernamePasswordAuthenticationToken;

import br.com.tratomais.core.model.User;
import br.com.tratomais.core.util.AppContextHelper;

public class AuthenticationHelper {

 	public static User getLoggedUser() {
		Authentication authentication = (Authentication) SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null && authentication.getPrincipal() != null && authentication.getPrincipal() instanceof User) {
			return (User) authentication.getPrincipal();
		}
		return null;
	}

	public static void setSelectedPartnerInLoggedUser(int partnerId) {
		User loggedUser = getLoggedUser();
		loggedUser.setTransientSelectedPartner(partnerId);
	}

	/**
	 * Takes the username and password as provided and checks the validaty of the credentials. Spring security is used to check the credentielas and
	 * to return the authenticated principal with it's authorized roles. An exception is thrown if the authentication failes.
	 * 
	 * @param username
	 *            String containing the username of the principal to login
	 * @param password
	 *            String containing the password used to identify the current user
	 * @return AuthorizationData object containing the name of the principal and the authorized roles.
	 */
	public User authenticatePrincipal(String username, String password) {

		User loggedUser = null;
		try {
			ApplicationContext appContext = AppContextHelper.getApplicationContext();
			AuthenticationManager manager = (AuthenticationManager) appContext.getBean("_authenticationManager");
			UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(username, password);

			Authentication authentication = manager.authenticate(usernamePasswordAuthenticationToken);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			loggedUser = (User) authentication.getPrincipal();
			loggedUser.setTimeValidation(new Timestamp(System.currentTimeMillis()));
		//} catch (BadCredentialsException c) {
		//	throw c;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return loggedUser;
	}

	public static boolean isSessionExpired(Timestamp timeNow) {
		if (getLoggedUser() != null) {
			long diferencaMilisegundos = timeNow.getTime() - getLoggedUser().getTimeValidation().getTime();
			int minutos = (int) (diferencaMilisegundos / 1000) / 60;
			if (minutos > getLoggedUser().getTimeLimit()) {
				return true;
			} else {
				getLoggedUser().setTimeValidation(timeNow);
				return false;
			}
		}
		return true;
	}

	public static void logout() {
		Authentication authentication = (Authentication) SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null)
			authentication.setAuthenticated(false);
	}
	public User authenticatePrincipalException(String username, String password) {
		User loggedUser = null;
		try {
			ApplicationContext appContext = AppContextHelper.getApplicationContext();
			AuthenticationManager manager = (AuthenticationManager) appContext.getBean("_authenticationManager");
			UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(username, password);

			Authentication authentication = manager.authenticate(usernamePasswordAuthenticationToken);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			loggedUser = (User) authentication.getPrincipal();
			loggedUser.setTimeValidation(new Timestamp(System.currentTimeMillis()));
		} catch (BadCredentialsException c) {
			throw c;
		} catch (RuntimeException c) {
			throw c;
		}
		return loggedUser;
	}
}
